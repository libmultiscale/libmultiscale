#include "atom_model_list.hh"
#include "compute_list.hh"
#include "continuum_model_list.hh"
#include "coupler_list.hh"
#include "dd_model_list.hh"
#include "dumper_list.hh"
#include "filter_list.hh"
#include "geometry_list.hh"
#include "stimulation_list.hh"
/* -------------------------------------------------------------------------- */
#include <boost/preprocessor.hpp>
#include <map>
#include <string>
#include <vector>
#include <string>
/* -------------------------------------------------------------------------- */
#include <boost/preprocessor.hpp>
#define stringify_macro(s) stringify_macro_1(s)
#define stringify_macro_1(s) #s
/* -------------------------------------------------------------------------- */
#define APPEND_MODEL_KEY_TO_LIST(r, data, x)                                   \
  data[stringify_macro(BOOST_PP_TUPLE_ELEM(3, 2, x))].resize(1);               \
  data[stringify_macro(BOOST_PP_TUPLE_ELEM(3, 2, x))][0] =                     \
      stringify_macro(BOOST_PP_TUPLE_ELEM(3, 0, x));

#define APPEND_COUPLER_KEY_TO_LIST(r, data, x)                                 \
  data[stringify_macro(BOOST_PP_TUPLE_ELEM(4, 1, x))].push_back(               \
      stringify_macro(BOOST_PP_TUPLE_ELEM(4, 0, x)));

#define APPEND_NORMAL_KEY_TO_LIST(r, data, x)                                  \
  data[stringify_macro(BOOST_PP_TUPLE_ELEM(2, 1, x))].push_back(               \
      stringify_macro(BOOST_PP_TUPLE_ELEM(2, 0, x)));

/* -------------------------------------------------------------------------- */
auto getKeyWordList() {
  std::map<std::string, std::vector<std::string>> key_list;
  BOOST_PP_SEQ_FOR_EACH(APPEND_MODEL_KEY_TO_LIST, key_list, LIST_ATOM_MODEL);
  BOOST_PP_SEQ_FOR_EACH(APPEND_MODEL_KEY_TO_LIST, key_list, LIST_DD_MODEL);
  BOOST_PP_SEQ_FOR_EACH(APPEND_MODEL_KEY_TO_LIST, key_list,
                        LIST_CONTINUUM_MODEL);

  BOOST_PP_SEQ_FOR_EACH(APPEND_COUPLER_KEY_TO_LIST, key_list, LIST_COUPLER);
  BOOST_PP_SEQ_FOR_EACH(APPEND_NORMAL_KEY_TO_LIST, key_list, LIST_GEOMETRY);

  BOOST_PP_SEQ_FOR_EACH(APPEND_NORMAL_KEY_TO_LIST, key_list, LIST_STIMULATION);
  BOOST_PP_SEQ_FOR_EACH(APPEND_NORMAL_KEY_TO_LIST, key_list, LIST_DUMPER);
  BOOST_PP_SEQ_FOR_EACH(APPEND_NORMAL_KEY_TO_LIST, key_list, LIST_COMPUTE);
  BOOST_PP_SEQ_FOR_EACH(APPEND_NORMAL_KEY_TO_LIST, key_list, LIST_FILTER);

  return key_list;
}
/* -------------------------------------------------------------------------- */
