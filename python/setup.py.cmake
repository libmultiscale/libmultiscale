import os
from setuptools import setup

name = 'libmultiscale'

if os.path.isfile("commit_hash"):
  with open("commit_hash") as f:
    commit_hash = f.read()[:8]
else:
  commit_hash = "need uuencode for revision information"

version = f'@LIBMULTISCALE_MAJOR_VERSION@.@LIBMULTISCALE_MINOR_VERSION@.@LIBMULTISCALE_BUILD_VERSION@-dirty{commit_hash}'

setup(name=name,
  version=version,
  author="Guillaume Anciaux",
  author_email="guillaume.anciaux@epfl.ch",
  url="https://gitlab.com/libmultiscale/libmultiscale",
  description="LibMultiScale Pypi package",
  long_description="""
In simulations, particle approaches can be helpful when the
discreteness of matter needs to be taken into account. Multiscale
coupling methods allow to reduce the prohibitive computational costs
of discrete methods. For instance, with these approaches one can
couple an atomic description with a macroscopic model of continuum
mechanics.

LibMultiScale is a C++ parallel framework for the multiscale
coupling methods dedicated to material simulations. This framework is
designed as a library providing an API which makes it possible to
program coupled simulations. The coupled parts can be provided by
existing projects. In such a manner, the API gives C++ templated
interfaces to reduce to the maximum the cost of integration taking the
form of plugins or alike. LAMMPS (Sandia laboratories) and Akantu
(LSMS) have been integrated to provide a functional framework.

The LibMultiScale is now distributed with a joint CECILL-C and GPL
open-source licence with a shared copyrights between INRIA, CEA and
EPFL. 

[Documentation can be found here](https://libmultiscale.gitlab.io/libmultiscale)
""",
  long_description_content_type="text/markdown",
  packages=['.'],
  package_data={
  "": ["*.so"],
  },
  classifiers=[
  "Operating System :: POSIX :: Linux",
  "License :: CeCILL-C Free Software License Agreement (CECILL-C)",
  "License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",
  "Programming Language :: C++",
  "Programming Language :: Python :: 3"
  ],
  # scripts=['clients/AMEL.py'],
  license="""
CeCILL-C
========

Copyright INRIA and CEA

The LibMultiScale is a C++ parallel framework for the multiscale
coupling methods dedicated to material simulations. This framework
provides an API which makes it possible to program coupled simulations
and integration of already existing codes.

This Project was initiated in a collaboration between INRIA Futurs Bordeaux
within ScAlApplix team and CEA/DPTA Ile de France.
The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
within the LSMS/ENAC laboratory.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.

L-GPL
=====

Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)

LibMultiScale is free  software: you can redistribute it and/or  modify it under the
terms  of the  GNU Lesser  General Public  License as  published by  the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
details.

You should  have received  a copy  of the GNU  Lesser General  Public License
along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.

""")
