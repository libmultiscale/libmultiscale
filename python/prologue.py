class DynamicalAttribute:
    def __init__(self, obj):
        self.obj = obj

    def __getattribute__(self, name):
        if name == 'obj':
            return super(DynamicalAttribute, self).__getattribute__(name)
        if name == '__dir__':
            return super(DynamicalAttribute, self).__getattribute__(name)
        elif hasattr(self.obj, 'getattribute'):
            return self.obj.getattribute(name)

    def __setattr__(self, name, val):
        if name == 'obj':
            super(DynamicalAttribute, self).__setattr__(name, val)
            return
        self.obj.setattr(name, val)

    def __dir__(self):
        return self.obj.dir()
