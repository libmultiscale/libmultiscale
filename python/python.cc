#include "lm_common.hh"
#include "lm_python_bindings.hh"
#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/iostream.h>

namespace py = pybind11;
using namespace libmultiscale;

std::vector<std::string> fake_argv;
std::vector<char *> c_argv;

extern std::map<std::string, std::vector<std::string>> getKeyWordList();

void loadModules() {
  // auto sys = py::module::import("sys");
  // auto argv = sys.attr("argv");
  // fake_argv = py::cast<std::vector<std::string>>(argv);
  // std::cout << "AAAA " << fake_argv.size() << std::endl;
  // for (auto &&v : fake_argv) {
  //   std::cout << "AAAA " << &v[0] << std::endl;
  //   c_argv.push_back(&v[0]);
  // }
  // loadModules(c_argv.size(), c_argv.data());
  loadModules(0, nullptr);
  create_seg_fault = false;
}

extern std::string lm_release_info;

template <typename T2, typename T>
void ModuleProperty(py::module &m, const char *n, T &var) {
  // setting a property
  // import the builtins and get the "property" python class

  auto property = py::module::import("builtins").attr("property");
  m.attr(n) = property(py::cpp_function([&]() { return var; }),
                       py::cpp_function([&](T2 &a) { var = a; }));
}

PYBIND11_MODULE(pylibmultiscale, m) {
  m.doc() = "LibMultiScale python module";

  ModuleProperty<int>(m, "current_step", current_step);
  ModuleProperty<int>(m, "lm_my_proc_id", lm_my_proc_id);
  ModuleProperty<IntegrationSchemeStage>(m, "current_stage", current_stage);
  ModuleProperty<UInt>(m, "spatial_dimension", spatial_dimension);
  ModuleProperty<bool>(m, "print_trace", print_trace);

  m.def("loadModules", &::loadModules,
        "Load all the module dependencies of libmultiscale");

  m.def("closeModules", &closeModules,
        "close all the module dependencies of libmultiscale");

  m.def("setCodeUnitSystem", &setCodeUnitSystem,
  	"Allows to set the used unit system to the specified one.");
  m.def("printCodeUnitSystem", [](const bool toStdout) -> void {
  	if(toStdout) {
  	  py::scoped_ostream_redirect output; printCodeUnitSystem(toStdout);
  	} else {
  	  printCodeUnitSystem(toStdout);
  	}}, py::arg("toStdout") = false,
  	"Prints the currently used unit system to the DUMP."
  	" In case the optional argument is set to `True`, it will be written to `stdout`.");
  m.def("isCodeUnitSystemSetTo", &isCodeUnitSystemSetTo,
  	"Tests if the currently used code system is set to the specified one.");

  py::class_<Parser>(m, "Parser")
      .def_static("getAlgebraicVariables", &Parser::getAlgebraicVariables)
      .def_static("setAlgebraicVariable",
                  [](const std::string &name, Real value) {
                    Parser::getAlgebraicVariables()[name] = value;
                  });

  m.attr("release_info") = py::cast(lm_release_info);

  m.def("getKeyWordList", &getKeyWordList);
  makeBindings(m);

  static py::exception<LibMultiScaleException> lm_exception(
      m, "LibMultiScaleException");
  py::register_exception_translator([](std::exception_ptr p) {
    try {
      if (p)
        std::rethrow_exception(p);
    } catch (LibMultiScaleException &e) {
      if (print_trace) {
        lm_exception(e.messageWithTrace().c_str());
      } else {
        std::string mess("\n");
        mess += "-----------------------------------------";
        mess += "-----------------------------------------\n";
        mess += e.what();
        lm_exception(mess.c_str());
      }
    }
  });
}
