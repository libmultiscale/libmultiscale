/**
 * @file   stimulation_enforce_compatibility.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @brief  Asks a domain to enforce (parallel) compatibility of its DOFS
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_STIMULATION_ENFORCE_COMPATIBILITY_HH__
#define __LIBMULTISCALE_STIMULATION_ENFORCE_COMPATIBILITY_HH__
/* -------------------------------------------------------------------------- */
#include "stimulation_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class StimulationEnforceCompatibility : public StimulationInterface {

public:
  DECLARE_STIMULATION(StimulationEnforceCompatibility,
                      _or<domMD, domCONTINUUM>);

  //! most generic stimulate function
  template <typename Domain> void stimulate(Domain &dom);
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_STIMULATION_FIELD_ENFORCE_COMPATIBILITY_HH__ */
