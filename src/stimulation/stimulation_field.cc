/**
 * @file   stimulation_field.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Fri Jul 25 11:42:31 2014
 *
 * @brief  Impose a field to a set of degree of freedom
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "stimulation_field.hh"
#include "compute_interface.hh"
#include "factory_multiscale.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
/* LMDESC FIELD

   Impose a field to a set of degree of freedom\			\
   This stimulator set displacements/velocities/forces based on the content of a
   compute
*/
/* LMHERITANCE action_interface */

/* LMEXAMPLE
STIMULATION fd FIELD INPUT dof:md FIELD position INPUT field:mycompute
 */

void StimulationField::declareParams() {

  StimulationInterface::declareParams();

  /* LMKEYWORD FIELD
     Set the field which should be set by the stimulator
     The possible values are:

       - position0
       - position
       - displacement
       - velocity
       - force
    */
  this->parseKeyword("FIELD", ftype);
  /* LMKEYWORD ADDITIVE
   Flag whether the COMPUTE should be added to the existing field or replace
   it. defaults to false.
  */
  this->parseTag("ADDITIVE", additive_flag, false);
}
/* -------------------------------------------------------------------------- */

StimulationField::StimulationField(const std::string &name)
    : LMObject(name), ftype(ft_uninitialised), additive_flag(false) {
  this->createInput("dofs");
  this->createInput("field");
  this->removeInput("input1");
}

/* -------------------------------------------------------------------------- */

StimulationField::~StimulationField() {}

/* -------------------------------------------------------------------------- */

template <typename Cont>
void StimulationField::stimulate(Cont &cont, ContainerArray<Real> &field) {

  constexpr UInt Dim = Cont::Dim;

  switch (ftype) {
  case _position0:
    stimulateField<Dim, _position0>(cont, field);
    break;
  case _position:
    stimulateField<Dim, _position>(cont, field);
    break;
  case _displacement:
    stimulateField<Dim, _displacement>(cont, field);
    break;
  case _velocity:
    stimulateField<Dim, _velocity>(cont, field);
    break;
  case _force:
    stimulateField<Dim, _force>(cont, field);
    break;
  case ft_uninitialised:
    LM_FATAL("You forgot to set the field type for the stimulator \""
             << this->getID() << "\"");
    break;
  default:
    LM_FATAL("cannot treat field type \"" << ftype << "\"" << std::endl);
  }
}

/* -------------------------------------------------------------------------- */

template <UInt Dim, FieldType type, typename Cont>
void StimulationField::stimulateField(Cont &cont, ContainerArray<Real> &field) {

  if (cont.size() == 0)
    return;

  UInt dim_compute = field.cols();

  if (dim_compute != Dim)
    LM_FATAL(this->getID() << ": incompatible dimensions " << dim_compute
                           << " != " << Dim << " cont size: " << cont.size()
                           << " compute size: " << field.rows());
  if (field.rows() != cont.size())
    LM_FATAL(this->getID() << ": compute or filter contains " << field.rows()
                           << " entries"
                           << " while container have " << cont.size()
                           << " : incompatible match");

  // ComputeInterface &comp = ptr;
  // auto itCompute = comp.evalArrayOutput().begin();

  if (this->additive_flag) {
    for (auto &&[at, field] : zip(cont, field.rowwise())) {
      Vector<Dim> val;
      auto f = at.template field<type>();
      for (UInt i = 0; i < Dim; ++i) {
        val[i] = field[i] + f[i];
        //++itCompute;
      }
      at.template field<type>() = val;
    }
  } else {
    for (auto &&[at, field] : zip(cont, field.rowwise())) {
      at.template field<type>() = Vector<Dim>(field);
    }
  }
}

/* -------------------------------------------------------------------------- */

// template <typename Cont>
// void StimulationField::stimulate(Cont &cont) {

//   constexpr UInt Dim = Cont::Dim;

//   switch (ftype) {
//   case _position0:
//     stimulateField<Dim, _position0>(cont);
//     break;
//   case _position:
//     stimulateField<Dim, _position>(cont);
//     break;
//   case _displacement:
//     stimulateField<Dim, _displacement>(cont);
//     break;
//   case _velocity:
//     stimulateField<Dim, _velocity>(cont);
//     break;
//   case _force:
//     stimulateField<Dim, _force>(cont);
//     break;
//   case _temperature:
//     stimulateField<1, _temperature>(cont);
//     break;
//   default:
//     LM_FATAL("cannot treat field type \"" << ftype << "\"");
//   }
// }

// /* --------------------------------------------------------------------------
// */

// template <UInt Dim, FieldType type, typename Cont>
// void StimulationField::stimulateField(Cont &cont) {

//   if (cont.size() == 0)
//     return;

//   ComputeInterface *ptr = dynamic_cast<ComputeInterface *>(
//       FilterManager::getManager().getObject(compute_id));
//   if (!ptr)
//     LM_FATAL("invalid compute id: " << compute_id);

//   ptr->build();
//   UInt dim_compute = ptr->getDim();

//   if (dim_compute != Dim)
//     LM_FATAL(this->getID() << ": incompatible dimensions " << dim_compute
//                            << " != " << Dim << " cont size: " << cont.size()
//                            << " compute size: " << ptr->size());
//   if (ptr->size() / dim_compute != cont.size())
//     LM_FATAL(this->getID() << ": compute or filter contains "
//                            << ptr->size() / dim_compute << " entries"
//                            << " while container have " << cont.size()
//                            << " : incompatible match");

//   ComputeInterface &comp = *ptr;
//   auto itCompute = comp.begin();

//   if (this->additive_flag) {
//     for (auto &&at : cont) {
//       Vector<Dim> val;
//       Vector<Dim> f = at.template field<type>();
//       for (UInt i = 0; i < Dim; ++i) {
//         val[i] = *itCompute + f[i];
//         ++itCompute;
//       }
//       at.template field<type>() = val;
//     }
//   } else {
//     for (auto &&at : cont) {
//       for (UInt i = 0; i < Dim; ++i) {
//         auto val = *itCompute;
//         at.template field<type>()[i] = val;
//         ++itCompute;
//       }
//     }
//   }
// }
// /* --------------------------------------------------------------------------
// */

void StimulationField::compute_make_call() {
  try {
    this->stimulate<dispatch>(this->getInput("dofs"), this->getInput("field"));
  } catch (Component::UnconnectedInput &e) {
    LM_FATAL("Unconnected input for component " << this->getID() << std::endl
                                                << e.what());
  }
}

__END_LIBMULTISCALE__
