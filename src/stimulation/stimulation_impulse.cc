/**
 * @file   stimulation_impulse.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This stimulator sets an initial impulse displacement field generating
 * a wave
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "stimulation_impulse.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

StimulationImpulse::StimulationImpulse(const std::string &name)
    : LMObject(name), impulse_compute("computeImpulse:" + name) {}

/* -------------------------------------------------------------------------- */

StimulationImpulse::~StimulationImpulse() {}

/* -------------------------------------------------------------------------- */

void StimulationImpulse::init() { impulse_compute.init(); }

/* -------------------------------------------------------------------------- */

template <typename Cont> void StimulationImpulse::stimulate(Cont &cont) {
  constexpr UInt Dim = Cont::Dim;

  impulse_compute.compute(cont);

  auto &imposed_field = impulse_compute.evalArrayOutput();

  if (imposed_field.cols() == Dim) {
    for (auto &&[at, field] : zip(cont, imposed_field.rowwise())) {
      at.displacement() = Vector<Dim>(field);
    }
  } else {
    const int direction = impulse_compute.direction;
    LM_ASSERT(cont.size() == imposed_field.size(),
              "The impossible happened " << cont.size()
                                         << " != " << imposed_field.size());
    for (auto &&[at, field] : zip(cont, imposed_field)) {
      at.displacement()[direction] = field;
    }
  }
}
/* -------------------------------------------------------------------------- */

/* LMDESC IMPULSE
   This stimulator sets an initial impulse displacement field that will generate
   a wave. It uses the IMPULSE compute to generate a displacement field. 
   See keywords/filter/impulse for more information on the generated wave.
*/
/* LMHERITANCE compute_impulse */

/* LMEXAMPLE STIMULATION impulse IMPULSE INPUT central_md LWAVE WaveLength*r0
   DIRECTION 0 INTENSITY 1.1e-3 ONESHOT 0 */

void StimulationImpulse::declareParams() {
  StimulationInterface::declareParams();
  this->addSubParsableObject(impulse_compute);
}

/* -------------------------------------------------------------------------- */

DECLARE_STIMULATION_MAKE_CALL(StimulationImpulse, )

__END_LIBMULTISCALE__
