/**
 * @file   stimulation_dislo.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 *
 * @date   Wed Dec 04 12:14:29 2013
 *
 * @brief  This stimulation command imposes the Volterra displacement fields of
 * straight screw edge or mixed dislocations
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_STIMULATION_VOLTERRA_HH__
#define __LIBMULTISCALE_STIMULATION_VOLTERRA_HH__
/* -------------------------------------------------------------------------- */
#include "stimulation_interface.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class StimulationVolterra : public StimulationInterface {
public:
  DECLARE_STIMULATION(StimulationVolterra, _or<MD>);

  //! init function
  void init() override;

  template <typename _Input>
  std::enable_if_t<_Input::Dim == 3> stimulate(_Input &cont);

  template <typename _Input>
  std::enable_if_t<_Input::Dim != 3> stimulate(_Input &cont);

  //!  compute displacement
  Vector<3> computeVolterraDisplacement(Vector<3> &burgers, Vector<3> &X);
  //! to revise
  Real evalArctan(Real X, Real Y);

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

private:
  //! burgers vector magnitude
  Quantity<Length> burgers;
  //! direction of burgers vector (norm = 1)
  Vector<3> burgers_direction;
  //! normal to the slip plane (norm = 1)
  Vector<3> slip_plane_normal;
  //! normal to the slip plane  (norm = 1)
  Vector<3> line_direction;
  //! one point crossing the dislocation line
  Quantity<Length, 3> pos;
  //! poisson ratio
  Real poisson;

  //! width on which the voleterra field should be applied
  Quantity<Length> width;
  //! number of small dislocation to add
  int resolution;
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_STIMULATION_VOLTERRA_HH__ */
