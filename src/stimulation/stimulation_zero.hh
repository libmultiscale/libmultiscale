/**
 * @file   stimulation_zero.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Mon Nov 25 15:05:56 2013
 *
 * @brief  Remise a zero (reset to zero) stimulator
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef STIMULATION_ZERO_H
#define STIMULATION_ZERO_H

/* -------------------------------------------------------------------------- */
#include "stimulation_interface.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class StimulationZero : public StimulationInterface {

public:
  DECLARE_STIMULATION(StimulationZero, _or<MD, CONTINUUM>);

  //! most generic stimulate function
  template <typename Cont> enable_if_md<Cont> stimulate(Cont &cont);
  template <typename Cont> enable_if_not_md<Cont> stimulate(Cont &cont);

  
private:
  std::vector<FieldType> field;
  Real COEF;
  bool average_flag;
  //* direction in which to apply the stimulation
  int direction;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif
