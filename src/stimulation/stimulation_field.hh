/**
 * @file   stimulation_field.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  Impose a field to a set of degree of freedom
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_STIMULATION_FIELD_HH__
#define __LIBMULTISCALE_STIMULATION_FIELD_HH__
/* -------------------------------------------------------------------------- */
#include "stimulation_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class StimulationField : public StimulationInterface {

public:
  DECLARE_STIMULATION(StimulationField, _or<MD, CONTINUUM>,
                      ContainerArray<Real>);

  //! most generic stimulate function
  template <typename Cont>
  void stimulate(Cont &cont, ContainerArray<Real> &field);
  //! templated with the type of field to be set
  template <UInt Dim, FieldType type, typename Cont>
  void stimulateField(Cont &cont, ContainerArray<Real> &field);

private:
  //! the field to impose from provided array
  FieldType ftype;
  //! wether it should be cumulative
  bool additive_flag;
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_STIMULATION_FIELD_HH__ */
