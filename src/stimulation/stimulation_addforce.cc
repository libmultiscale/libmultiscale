/**
 * @file   stimulation_addforce.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  Add a force to a set of atoms/nodes/points
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "stimulation_addforce.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

StimulationAddForce::StimulationAddForce(const std::string &name)
    : LMObject(name) {
  for (UInt i = 0; i < 3; ++i) {
    total_force[i] = 0.0;
    per_atom_force[i] = 0.0;
  }
}
/* -------------------------------------------------------------------------- */

StimulationAddForce::~StimulationAddForce() {}

/* -------------------------------------------------------------------------- */

template <typename Cont> void StimulationAddForce::stimulate(Cont &cont) {

  constexpr UInt Dim = Cont::Dim;

  UInt cpt = cont.size();

  CommGroup group = cont.getCommGroup();
  UInt rank = group.getMyRank();

  group.allReduce(&cpt, 1, "reduce total number of points concerned", OP_SUM);
  if (rank == 0) {
    for (UInt i = 0; i < Dim; ++i)
      DUMP("In direction " << i << " add an additional force "
                           << total_force[i] / (Real)cpt + per_atom_force[i],
           DBG_INFO);
  }

  for (auto &&at : cont) {
    for (UInt i = 0; i < Dim; ++i) {
      at.force()[i] += total_force[i] / Real(cpt) + per_atom_force[i];
    }
  }
}
/* -------------------------------------------------------------------------- */

/* LMDESC ADDFORCE

   Add a force to a set of atoms/nodes/points

   The stimulator is provided a force :math:`f^{req}` and for each
   atoms/nodes/points in the input the stimulator do:

   .. math::
     f += \frac{1}{\#points} f^{total} + f^{per-atom-force}
*/

/* LMEXAMPLE
   .. code-block::

     STIMULATION force ADDFORCE TOTAL_FORCE 1
     STIMULATION force ADDFORCE PER_ATOM_FORCE 0.001
*/

void StimulationAddForce::declareParams() {
  StimulationInterface::declareParams();

  this->changeDefault("STAGE", PRE_STEP3);

  /* LMKEYWORD TOTAL_FORCE
     Give the total force vector requested to be added. This quantity will
     be divided by the number of atoms in the container.
  */
  this->parseVectorKeyword("TOTAL_FORCE", spatial_dimension, total_force,
                           VEC_DEFAULTS(0., 0., 0.));

  /* LMKEYWORD PER_ATOM_FORCE
     Give the force vector requested to be added on each atom.
  */
  this->parseVectorKeyword("PER_ATOM_FORCE", spatial_dimension, per_atom_force,
                           VEC_DEFAULTS(0., 0., 0.));
}

/* -------------------------------------------------------------------------- */

DECLARE_STIMULATION_MAKE_CALL(StimulationAddForce, )

__END_LIBMULTISCALE__
