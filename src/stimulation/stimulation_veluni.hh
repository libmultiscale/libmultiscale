/**
 * @file   stimulation_veluni.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Thu Nov 21 22:11:13 2013
 *
 * @brief  This stimulator allows to enforce a constant velocity motion
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef STIMULATION_VELUNI_H
#define STIMULATION_VELUNI_H

/* -------------------------------------------------------------------------- */
#include "stimulation_interface.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class StimulationVelUni : public StimulationInterface {

public:
  DECLARE_STIMULATION(StimulationVelUni, _or<MD, CONTINUUM>);

  //! most generic stimulate function
  template <typename _Input> void stimulate(_Input &cont);

private:
  //! vector coordinates that gives displacement orientation
  Quantity<Length, 3u> vec;
  //! direction of the constraint -1 is all, 0 is X, 1 is Y, 2 is Z
  std::vector<int> directions;
};
__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_STIMULATION_VELUNI_HH__ */
