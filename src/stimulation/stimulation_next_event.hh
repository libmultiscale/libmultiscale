/**
 * @file   stimulation_next_event.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 *
 * @date   Mon Jun 30 12:36:27 2014
 *
 * @brief  allows to explicitly set the global variable nb_step_next_event
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_STIMULATION_NEXT_EVENT_HH__
#define __LIBMULTISCALE_STIMULATION_NEXT_EVENT_HH__
/* -------------------------------------------------------------------------- */
#include "stimulation_interface.hh"
#include <limits>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class StimulationNextEvent : public StimulationInterface {

public:
  DECLARE_STIMULATION(StimulationNextEvent, _or<MD>);

  void init() override;

  //! most generic stimulate function
  void stimulate();

private:
  UInt val;
  UInt incr;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_STIMULATION_NEXT_EVENT_HH__ */
