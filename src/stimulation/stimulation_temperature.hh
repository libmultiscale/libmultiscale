/**
 * @file   stimulation_temperature.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Mon Nov 25 15:05:56 2013
 *
 * @brief  Set a given temperature to a set of DOFs
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef STIMULATION_TEMPERATURE_H
#define STIMULATION_TEMPERATURE_H

/* -------------------------------------------------------------------------- */
#include "stimulation_interface.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class StimulationTemperature : public StimulationInterface {

public:
  DECLARE_STIMULATION(StimulationTemperature, _or<MD, CONTINUUM>);

  //! stimulate function for meshes
  template <typename _Input> enable_if_mesh<_Input> stimulate(_Input &cont);
  //! stimulate function for md
  template <typename _Input> enable_if_md<_Input> stimulate(_Input &cont);

private:
  //! desired temperature
  Quantity<Temperature> temperature;
  //! seed for random number generator (used only for atomistics)
  long seed;
  //! heat rate per node (used only for finite elements)
  Real heat_rate_per_node;
  //! flux flag to apply heat rate per node
  bool flux_flag;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif
