/**
 * @file   stimulation_temperature.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  Set a given temperature to a set of DOFs
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
#include "stimulation_temperature.hh"
#include "compute_ekin.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include <random>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
/* LMDESC TEMPERATURE

   This stimulator sets initial velocity to input a temperature.
*/
/* LMEXAMPLE STIMULATION TEMPERATURE TEMP 100 SEED 32 */
/* LMEXAMPLE STIMULATION TEMPERATURE FLUX_FLAG FLUX 1 SEED 32 */
/* LMHERITANCE action_interface */

inline void StimulationTemperature::declareParams() {
  StimulationInterface::declareParams();

  /* LMKEYWORD TEMP
     Set the temperature desired to be set
  */
  this->parseKeyword("TEMP", temperature);
  /* LMKEYWORD SEED
     Set the seed for the random generator
  */
  this->parseKeyword("SEED", seed);
  /* LMKEYWORD HEAT_RATE
     Set the heat rate per node (only for finite elements)
  */
  this->parseKeyword("HEAT_RATE", heat_rate_per_node, 0.);
  /* LMKEYWORD FLUX_FLAG
     Set the flux per node (only for finite elements)
  */
  this->parseTag("FLUX_FLAG", flux_flag, false);
}

/* -------------------------------------------------------------------------- */

StimulationTemperature::StimulationTemperature(const std::string &name)
    : LMObject(name), flux_flag(false) {}

/* -------------------------------------------------------------------------- */

StimulationTemperature::~StimulationTemperature() {}

/* -------------------------------------------------------------------------- */

template <typename Cont>
enable_if_md<Cont> StimulationTemperature::stimulate(Cont &cont) {

  constexpr UInt Dim = Cont::Dim;

  for (auto &&at : cont) {
    at.velocity() = Vector<Cont::Dim>::Random();
  }

  // remove rigid body motion
  Vector<Dim> average_v = Vector<Dim>::Zero();
  for (auto &&at : cont) {
    average_v += at.velocity();
  }
  average_v /= cont.size();

  for (auto &&at : cont) {
    at.velocity() -= average_v;
  }

  ComputeEKin compute_ekin("ComputeEkin:" + this->getID());
  compute_ekin.compute("domain"_input = cont);
  Real T_ = compute_ekin.evalArrayOutput("Temperature").get(0);

  Real alpha = sqrt(temperature / T_);

  for (auto &&at : cont) {
    at.velocity() *= alpha;
  }
  compute_ekin.build(cont);
  T_ = compute_ekin.evalArrayOutput("Temperature").get(0);
  DUMP("temperature measured after: " << T_, DBG_MESSAGE);
}
/* -------------------------------------------------------------------------- */

template <typename Cont>
enable_if_mesh<Cont> StimulationTemperature::stimulate(Cont & // cont
) {

  LM_TOIMPLEMENT;
  // if (!flux_flag) {
  //   for (auto &&n : cont) {
  //     n.temperature() = this->temperature;
  //   }
  // } else {
  //   for (auto &&n : cont) {
  //     n.externalHeatRate() = this->heat_rate_per_node;
  //   }
  // }
}

/* -------------------------------------------------------------------------- */

DECLARE_STIMULATION_MAKE_CALL(StimulationTemperature, )

__END_LIBMULTISCALE__
