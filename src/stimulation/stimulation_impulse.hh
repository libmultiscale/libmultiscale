/**
 * @file   stimulation_impulse.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Dec 10 16:00:49 2013
 *
 * @brief  This stimulator sets an initial impulse displacement field generating
 * a wave
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef STIMULATION_IMPULSE_H
#define STIMULATION_IMPULSE_H

/* -------------------------------------------------------------------------- */
#include "compute_impulse.hh"
#include "stimulation_interface.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class StimulationImpulse : public StimulationInterface {

public:
  DECLARE_STIMULATION(StimulationImpulse, _or<MD, CONTINUUM>);

  //! initialization function
  void init() override;
  //! most generic stimulate function
  template <typename _Input> void stimulate(_Input &cont);
  //! return scale deformation impulse provided a scale position
  Real computeScaleImpulse(Real d);

private:
  ComputeImpulse impulse_compute;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif
