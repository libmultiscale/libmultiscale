/**
 * @file   stimulation_dislo.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 *
 * @date   Wed Dec 04 12:14:29 2013
 *
 * @brief  This stmulation command imposes the Volterra displacement fields of
 * strainght screw edge or mixed dislocations
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_STIMULATION_DISLO_HH__
#define __LIBMULTISCALE_STIMULATION_DISLO_HH__
/* -------------------------------------------------------------------------- */
#include "stimulation_interface.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class StimulationDislo : public StimulationInterface {
public:
  DECLARE_STIMULATION(StimulationDislo, _or<MD>);

  //! init function
  void init() override;

  //! most generic stimulate function
  template <typename _Input> void stimulate(_Input &cont);
  /** We define a coordinate system for the dislocation in which the normal n to
      the slip plane forms the y axis, the line vector l the z axis and
      Be= cross(x,l) is the x direction
  **/
  //!  displacement of a Volterra in its coordinate system's direction Be,
  // b_edge is the edge component of the burgers vector
  template <typename _Input>
  Real computeBeDisplacement(Real b_edge, Vector<_Input::Dim> &X);
  //!  displacement of a Volterra in its coordinate system's direction n,
  template <typename _Input>
  Real computeNDisplacement(Real b_edge, Vector<_Input::Dim> &X);
  //!  displacement of a Volterra in its coordinate system's direction l,
  // b_screw is the screw component of the burgers vector
  template <typename _Input>
  Real computeLDisplacement(Real b_screw, Vector<_Input::Dim> &X);

  //!  displacement of an anisotropy dislocation in its coordinate system's
  //!  direction Be,
  // b_edge is the edge component of the burgers vector
  template <typename _Input>
  Real computeBeDisplacementAniso(Real b_edge, Vector<_Input::Dim> &X);
  //!  displacement of an anisotropy dislocation in its coordinate system's
  //!  direction n,
  template <typename _Input>
  Real computeNDisplacementAniso(Real b_edge, Vector<_Input::Dim> &X);
  //!  displacement of an anisotropy dislocation in its coordinate system's
  //!  direction l,
  // b_screw is the screw component of the burgers vector
  template <typename _Input>
  Real computeLDisplacementAniso(Real b_screw, Vector<_Input::Dim> &X);

  //!  displacement of an isotropy dislocation in its coordinate system's
  //!  direction Be,
  // b_edge is the edge component of the burgers vector
  template <typename _Input>
  Real computeBeDisplacementIso(Real b_edge, Vector<_Input::Dim> &X);
  //!  displacement of an isotropy dislocation in its coordinate system's
  //!  direction n,
  template <typename _Input>
  Real computeNDisplacementIso(Real b_edge, Vector<_Input::Dim> &X);
  //!  displacement of an isotropy dislocation in its coordinate system's
  //!  direction l,
  // b_screw is the screw component of the burgers vector
  template <typename _Input>
  Real computeLDisplacementIso(Real b_screw, Vector<_Input::Dim> &X);

  //!  linearly distributed displacement
  Real computeLinearDisplacement(Real burg, Real *X, Real xlo, Real xhi);

  template <typename _Input> Real eval_t_coef(Vector<_Input::Dim> &X);
  template <typename _Input> Real eval_q_coef(Vector<_Input::Dim> &X);
  Real evalArctan(Real X, Real Y);

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

private:
  template <typename _Input>
  void fix_boundary(typename _Input::Ref &atom, const Vector<_Input::Dim> &xmin,
                    const Vector<_Input::Dim> &xmax);
  template <typename _Input>
  inline void stimulate_to_clean(_Input &cont, UInt stage);
  Quantity<Length> burgers;
  Real *burg_edge, *normal, *line_dir;
  // Quantity<Length, 3> normal_quant;
  int normal_quant[3];
  int burg_edge_quant[3];
  //  Quantity<Length, 3> line_dir_quant;
  int line_dir_quant[3];
  Real rotation_matrix[9];
  // Quantity<Length, 3> pos;
  Real pos[3];
  // Quantity<Length>   width;
  int nb_replica;
  char dir_replica_char;
  UInt dir_replica;
  // int sign[2];
  Real nu;
  LMID geom;
  Real theta;
  Quantity<Length> fix_border_tol;
  int dipole[3];
  bool linear;

  // anisotropy
  bool aniso;
  bool iso;
  Real elastic_coefs[13];
  Real lambda;
  Real phi;
  Real c11bar;
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_STIMULATION_DISLO_HH__ */
