/**
 * @file   stimulation_linearmomentum.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This stimulator zeros the linear momentum of a group of atoms
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "stimulation_linearmomentum.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "ref_point_data.hh"
#include <mpi.h>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

StimulationLinearMomentum::StimulationLinearMomentum(const std::string &name)
    : LMObject(name) {}

/* -------------------------------------------------------------------------- */

StimulationLinearMomentum::~StimulationLinearMomentum() {}

/* -------------------------------------------------------------------------- */

template <typename Cont> void StimulationLinearMomentum::stimulate(Cont &cont) {
  constexpr UInt Dim = Cont::Dim;

  UInt cpt = cont.size(), cpt_tot = 0;
  Vector<Dim> local_mean_v =
      Vector<Dim>::Zero(); //, local_mean_vy = 0.0, local_mean_vz = 0.0;
  Vector<Dim> mean_v = Vector<Dim>::Zero(); //, mean_vy = 0.0, mean_vz = 0.0;

  for (auto &&at : cont) {
    local_mean_v += at.velocity();
  }

  MPI_Allreduce(&local_mean_v, &mean_v, 3, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(&cpt, &cpt_tot, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

  mean_v /= cpt_tot;

  for (auto &&at : cont) {
    at.velocity() -= mean_v;
  }
}
/* -------------------------------------------------------------------------- */

/* LMDESC LINEAR_MOMENTUM
   This stimulator zeros the linear momentum of a group of atoms
*/

/* LMHERITATE action_interface */

/* LMEXAMPLE STIMULATION lm LINEAR_MOMENTUM INPUT md FREQ 1 */

void StimulationLinearMomentum::declareParams() {
  StimulationInterface::declareParams();
}

/* -------------------------------------------------------------------------- */

DECLARE_STIMULATION_MAKE_CALL(StimulationLinearMomentum, )

__END_LIBMULTISCALE__
