/**
 * @file   stimulation_dislo.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This stmulation command imposes the Volterra displacement fields of
 * strainght screw edge or mixed dislocations
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "stimulation_volterra.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

StimulationVolterra::StimulationVolterra(const std::string &name)
    : LMObject(name) {}

/* -------------------------------------------------------------------------- */

StimulationVolterra::~StimulationVolterra() {}

/* -------------------------------------------------------------------------- */

void StimulationVolterra::init() {

  burgers_direction.normalize();
  slip_plane_normal.normalize();
  line_direction.normalize();
}

/* -------------------------------------------------------------------------- */

/*
Volterra tools:

everything is computed for
- the line direction to be X
- the pack plane direction to be Y
- the edge direction to be Z
*/

/* -------------------------------------------------------------------------- */

Real computeVolterraZDisplacement(Vector<3> &X, Vector<3> &burgers, Real pois) {

  auto b_edge = burgers[2];
  auto x = -X[2];
  auto y = X[1];
  auto r2 = x * x + y * y;
  auto epsilon = 1e-15;
  auto res = b_edge / (2. * M_PI) *
             (std::atan2(y, x) + x * y / (2. * (1 - pois) * (r2 + epsilon)));
  return res;
}

/* -------------------------------------------------------------------------- */

Real computeVolterraYDisplacement(Vector<3> &X, Vector<3> &burgers, Real pois) {

  auto x = -X[2];
  auto y = X[1];

  auto x_2 = x * x;
  auto y_2 = y * y;
  auto b_edge = burgers[2];
  auto b_edge2 = b_edge * b_edge;
  auto r2 = x_2 + y_2;
  Real epsilon = 1e-15;
  auto c1 = (1 - 2. * pois);
  auto c2 = 4. * (1. - pois);
  auto res = -b_edge / (2. * M_PI) *
             (c1 / c2 * std::log((r2 + epsilon) / b_edge2) +
              (x_2 - y_2) / c2 / (r2 + epsilon));

  return res;
}

/* ------------------------------------------------------------------------ */

Real computeVolterraXDisplacement(Vector<3> &X, Vector<3> &burgers) {
  auto x = X[2];
  auto y = X[1];

  auto b_screw = burgers[0];
  auto res = b_screw / (2. * M_PI) * (std::atan2(y, x));
  return res;
}

/* ------------------------------------------------------------------------ */
inline Vector<3>
StimulationVolterra::computeVolterraDisplacement(Vector<3> &burgers,
                                                 Vector<3> &X) {

  Vector<3> res;
  res[0] = computeVolterraXDisplacement(X, burgers);
  res[1] = computeVolterraYDisplacement(X, burgers, this->poisson);
  res[2] = computeVolterraZDisplacement(X, burgers, this->poisson);
  return res;
}

/* ------------------------------------------------------------------------ */

template <typename Cont>
std::enable_if_t<Cont::Dim != 3> StimulationVolterra::stimulate(Cont &) {
  LM_FATAL("This stimulator cannot work with a spatial dimension lower than 3");
}

/* ------------------------------------------------------------------------ */

template <typename Cont>
std::enable_if_t<Cont::Dim == 3> StimulationVolterra::stimulate(Cont &cont) {

  Vector<3> slip_direction = line_direction.cross(slip_plane_normal);
  Matrix<3> rotation_matrix;
  rotation_matrix.col(0) = line_direction;
  rotation_matrix.col(1) = slip_plane_normal;
  rotation_matrix.col(2) = slip_direction;

  for (auto &&at : cont) {
    auto X0 = at.position0();    // position in physical space
    Vector<3> X = at.position(); // position in physical space

    X = X0;
    X -= pos.template cast<Real>();

    // apply rotation point to the dislocation coordinate system
    Vector<3> x = rotation_matrix * X;
    Vector<3> rotated_burgers = rotation_matrix * (burgers * burgers_direction);

    // compute isotropy volterra displacement
    auto disp = computeVolterraDisplacement(rotated_burgers, x);

    // rotate back
    Vector<3> Disp = rotation_matrix.transpose() * disp;
    at.position() += Disp;
  }
}

/* --------------------------------------------------------------------------
 */

/* LMDESC VOLTERRA

   This stimulation command imposes the Volterra displacement
   field of straight screw, edge, or combination of these two last\\\\

   Displacement field  of Screw dislocation:

   \begin{equation}
   u_{z}(x, y) = \frac{b}{2\pi} \cdot atan2(y,x)
   \end{equation}

   Displacement fields of Edge dislocation:

   \begin{equation}
   u_{x}(x, y) = \frac{b}{2\pi} \cdot
   \left(
   atan2(y,x) + \frac{xy}{2 (1-\nu) (x^2 + y^2)} \right)
   \end{equation}

   \begin{equation}
   u_{y}(x, y) = -\frac{b}{2\pi} \cdot
   \left(
   \frac{1-2\nu}{4(1 - \nu)}\ln(x^2 + y^2)
   + \frac{x^2 - y^2}{4(1-\nu)(x^2 + y^2)}
   \right)
   \end{equation}

   are introduced.
*/
/* LMEXAMPLE STIMULATION myVolterra DISLO INPUT md STAGE PRE_DUMP BURGERS
 * burgers
 * POS 0 0 0  REPLICA 10 0 POISSON 0.3468 THETA 180 ONESHOT 0 */
/* LMHERITANCE action_interface */

void StimulationVolterra::declareParams() {
  StimulationInterface::declareParams();

  /* LMKEYWORD BURGERS
     Specify the magnitude of Burger's vector
  */
  this->parseKeyword("BURGERS", this->burgers);

  /* LMKEYWORD BURGERS
     Specify the direction of the Burger's vector
  */
  this->parseKeyword("BURGERS_DIR", this->burgers_direction);

  /* LMKEYWORD POS
     Specify the coordinates of a point on the dislocation line
  */
  this->parseKeyword("POS", this->pos);

  /* LMKEYWORD SLIP_PLANE
     Specifies the normal vector to the slip plane
  */
  this->parseKeyword("SLIP_PLANE", this->slip_plane_normal);

  /* LMKEYWORD LINE_DIR
     Specifies the normal vector to the slip plane
  */
  this->parseKeyword("LINE_DIR", this->line_direction);

  /* LMKEYWORD POISSON
     Specify Poisson's ratio of the material
  */
  this->parseKeyword("POISSON", this->poisson);

  /* LMKEYWORD RESOLUTION
     Specify the number of increments in the spread of dislocation density in
     Z direction
  */
  // this->parseKeyword("RESOLUTION", this->resolution);

  /* LMKEYWORD WIDTH
     Specify the width of the core dislocation in Z direction
  */
  // this->parseKeyword("WIDTH", this->width);
}

/* --------------------------------------------------------------------------
 */
DECLARE_STIMULATION_MAKE_CALL(StimulationVolterra, )

__END_LIBMULTISCALE__
