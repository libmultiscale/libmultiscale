/**
 * @file   stimulation_block.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Tue Jul 29 17:03:26 2014
 *
 * @brief  blocks nodes of a fem mesh, thus eliminating equations
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "stimulation_block.hh"
#include "container_mesh.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "ref_point_data.hh"

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

StimulationBlock::StimulationBlock(const std::string &name) : LMObject(name) {
  block = Vector<3, bool>::Zero();
}

/* -------------------------------------------------------------------------- */

StimulationBlock::~StimulationBlock() {}

/* -------------------------------------------------------------------------- */

template <typename _Input> void StimulationBlock::stimulate(_Input &input) {
  if (input.size() == 0)
    return;

  constexpr UInt Dim = _Input::Dim;
  std::vector<UInt> directions;

  for (UInt i = 0; i < Dim; ++i) {
    if (this->block[i])
      directions.push_back(i);
  }

  typedef std::vector<UInt>::iterator dir_iterator;

  DUMP("Stimulation BLOCK "
           << " Dim = " << Dim << " block_x = " << this->block[0]
           << " block_y = " << this->block[1]
           << " block_z = " << this->block[2],
       DBG_INFO);

  for (__attribute__((unused)) auto &&nd : input) {
    for (dir_iterator dir = directions.begin(); dir != directions.end();
         ++dir) {
      LM_TOIMPLEMENT;
      // nd.boundary(*dir) = true;
    }
  }
}

/* -------------------------------------------------------------------------- */
/* LMDESC BLOCK
   blocks nodes of a fem mesh, thus eliminating equations
*/

/* LMEXAMPLE STIMULATION boundary BLOCK INPUT femdom DIR 1 0 1 */

/* LMHERITANCE action_interface */

void StimulationBlock::declareParams() {
  StimulationInterface::declareParams();

  /* LMKEYWORD DIR
     Whether to block in x direction*/
  this->parseVectorKeyword("DIR", spatial_dimension, this->block,
                           VEC_DEFAULTS(false, false, false));
}

/* -------------------------------------------------------------------------- */

DECLARE_STIMULATION_MAKE_CALL(StimulationBlock, )

__END_LIBMULTISCALE__
