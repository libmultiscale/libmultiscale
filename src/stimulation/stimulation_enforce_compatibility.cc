/**
 * @file   stimulation_enforce_compatibility.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @brief  Asks a domain to enforce (parallel) compatibility of its DOFS
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "stimulation_enforce_compatibility.hh"
#include "compute_interface.hh"
#include "factory_multiscale.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
/* LMDESC ENFORCE_COMPATIBILITY

   Asks a domain to enforce compatibility of its DOFS
     - checks for geometry/processor box consistency (out of box atoms)
     - checks for PBCs
*/
/* LMHERITANCE action_interface */

/* LMEXAMPLE
STIMULATION compat ENFORCE_COMPATIBILITY INPUT md
 */

/* -------------------------------------------------------------------------- */

StimulationEnforceCompatibility::StimulationEnforceCompatibility(
    const std::string &name)
    : LMObject(name) {
  // this->createInput("domain");
  // this->removeInput("input1");
}

/* -------------------------------------------------------------------------- */

StimulationEnforceCompatibility::~StimulationEnforceCompatibility() {}

/* -------------------------------------------------------------------------- */

template <typename Domain>
void StimulationEnforceCompatibility::stimulate(Domain &dom) {
  dom.enforceCompatibility();
}

/* -------------------------------------------------------------------------- */

void StimulationEnforceCompatibility::declareParams() {
  StimulationInterface::declareParams();
}
/* -------------------------------------------------------------------------- */

DECLARE_STIMULATION_MAKE_CALL(StimulationEnforceCompatibility, )

__END_LIBMULTISCALE__
