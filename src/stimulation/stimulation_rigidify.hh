/**
 * @file   stimulation_rigidify.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Nov 25 17:17:19 2013
 *
 * @brief  This stimulator allows to reduce the force to let a group act like a
 * rigid body
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef STIMULATION_RIGIDIFY_H
#define STIMULATION_RIGIDIFY_H

/* -------------------------------------------------------------------------- */
#include "stimulation_interface.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class StimulationRigidify : public StimulationInterface {

public:
  DECLARE_STIMULATION(StimulationRigidify, _or<MD>);

  template <typename _Input> void stimulate(_Input &cont);

private:
  Quantity<Force, 3> total_force;
  Real factor;
};
__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_RIGIDIFY_HH__ */
