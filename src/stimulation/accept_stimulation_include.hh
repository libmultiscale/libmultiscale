/**
 * @file   accept_stimulation_include.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 30 12:13:51 2013
 *
 * @brief  This is a convenient file for the visitor mechanism
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
__MY_TEMPLATE_DECL__
inline void __MY_CLASS__::accept(StimulationInterface &v) {
  if (1 == 2) {
  }

#define BOOST_TYPE_CAST_STIMULATION(r, data, x)                                \
  TYPE_CAST_STIMULATION(BOOST_PP_TUPLE_ELEM(2, 0, x))

#define TYPE_CAST_STIMULATION(stimulation_type)                                \
  else if (stimulation_type *_ptr = dynamic_cast<stimulation_type *>(&v)) {    \
    __ROUTINE__                                                                \
  }

  BOOST_PP_SEQ_FOR_EACH(BOOST_TYPE_CAST_STIMULATION, obj, __LIST__)

//#undef DECLARE_ACCEPT
#undef TYPE_CAST_STIMULATION
#undef BOOST_TYPE_CAST_STIMULATION
  //#undef BOOST_DECLARE_ACCEPT

  else {
    LM_FATAL("unrecognized stimulation");
  };
}
