/**
 * @file   stimulation_next_event.cc
 *
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 *
 * @date   Thu Sep 18 16:13:19 2014
 *
 * @brief  allows to explicitly set the global variable nb_step_next_event
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "stimulation_next_event.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* LMDESC NEXT_EVENT
   allows to explicitly set the global variable nb\_step\_next\_event
*/

/* LMHERITATE action_interface */

/* LMEXAMPLE STIMULATION gts NEXT_EVENT INPUT NULL FREQ 1200 */

void StimulationNextEvent::declareParams() {

  StimulationInterface::declareParams();
  /* LMKEYWORD INCR
     This specifies by how much to increment nb\_step\_next\_event
  */
  this->parseKeyword("INCR", this->incr, lm_uint_max);

  /* LMKEYWORD VAL
     This specifies value to which nb\_step\_next\_event is to be set
  */
  this->parseKeyword("VAL", this->val, lm_uint_max);
}

/* -------------------------------------------------------------------------- */

void StimulationNextEvent::stimulate() {
  if (this->incr != lm_uint_max) {
    nb_step_next_event += incr;
  } else {
    nb_step_next_event = val;
  }
  // DUMP("nb_step_next_event has been set to " << nb_step_next_event,
  // DBG_MESSAGE);
}

/* -------------------------------------------------------------------------- */

StimulationNextEvent::StimulationNextEvent(const std::string &name)
    : LMObject(name), val(lm_uint_max), incr(lm_uint_max){}

/* -------------------------------------------------------------------------- */

StimulationNextEvent::~StimulationNextEvent(){}

/* -------------------------------------------------------------------------- */

void StimulationNextEvent::init() {
  bool val_set = this->val != lm_uint_max;
  bool incr_set = this->incr != lm_uint_max;
  if (val_set && incr_set) {
    LM_FATAL("only VAL or INCR can be set, not both");
  } else if ((!val_set && !incr_set) && this->frequency == 1) {
    LM_FATAL("if neither VAL nor INCR are set, the FREQ keyword "
             << "has to be used");
  }
  if (this->frequency != 1) {
    this->incr = this->frequency;
  }
}

/* -------------------------------------------------------------------------- */

void StimulationNextEvent::compute_make_call() { this->stimulate(); }

__END_LIBMULTISCALE__
