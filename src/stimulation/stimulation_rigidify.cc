/**
 * @file   stimulation_rigidify.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This stimulator allows to reduce the force to let a group act like a
 * rigid body
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "stimulation_rigidify.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

StimulationRigidify::StimulationRigidify(const std::string &name)
    : LMObject(name) {
  for (UInt i = 0; i < 3; ++i) {
    total_force[i] = 0.;
  }
  factor = 1.;
}

/* -------------------------------------------------------------------------- */

StimulationRigidify::~StimulationRigidify() {}

/* -------------------------------------------------------------------------- */

template <typename Cont> void StimulationRigidify::stimulate(Cont &cont) {

  constexpr UInt Dim = Cont::Dim;

  UInt cpt = cont.size();
  Vector<Dim> f = Vector<Dim>::Zero();

  for (auto &&at : cont) {
    f += at.force();
  }

  // then reduce for multiple processors
  CommGroup group = cont.getCommGroup();
  group.allReduce(f.data(), Dim, "reduce force", OP_SUM);
  group.allReduce(&cpt, 1, "reduce number atoms", OP_SUM);

  f += total_force.cast<Real>().block<Dim, 1u>(0, 0);
  f /= cpt;

  for (auto &&at : cont) {
    at.force() = f / factor;
  }
}
/* -------------------------------------------------------------------------- */

/* LMDESC RIGIDIFY
   This stimulator allows to reduce the force acting over a set of degrees of
   freedom to a single scalar and assign this last to all forces of that set.
   Such a set will then act like a rigid body. More precisely the algorithm
   is:\\

   \begin{equation}
   f_{global} = \sum_i f_i + f^{ext}
   \end{equation}
   \begin{equation}
   f_i := \frac{f_{global}}{\#dof \cdot factor}
   \end{equation}

   where $\#dof$ is the number of dof in the subset $f^{ext}$ (GLOBAL\_FORCE) is
   an external force
   applied onto the rigid body and $factor$ (FACTOR) is a flexibility factor to
   accelerate
   linear motion.
*/

/* LMHERITATE action_interface */

/* LMEXAMPLE STIMULATION rigid RIGIDIFY INPUT md GLOBAL_FORCE 1 0 0 MASS_FACTOR
 * 1e-3 */

//! most generic stimulate function

void StimulationRigidify::declareParams() {
  StimulationInterface::declareParams();

  this->changeDefault("STAGE", PRE_STEP3);

  /* LMKEYWORD TOTAL_FORCE
     Specifiy the external force to be applied to the rigid body.
  */
  this->parseVectorKeyword("TOTAL_FORCE", spatial_dimension, total_force);
  /* LMKEYWORD FACTOR
     Correction factor to augment/reduce the forces applied to the rigid body.
  */
  this->parseKeyword("FACTOR", factor, 1.);
}

/* -------------------------------------------------------------------------- */

DECLARE_STIMULATION_MAKE_CALL(StimulationRigidify, )

__END_LIBMULTISCALE__
