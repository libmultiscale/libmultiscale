/**
 * @file   stimulation_berendsen.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This stimulator applies a berendsen thermostat to maintain the
 * temperature of atoms
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "stimulation_berendsen.hh"
#include "compute_ekin.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

template <typename Cont> void StimulationBerendsen::stimulate(Cont &cont) {

  // constexpr UInt Dim = Cont::Dim;

  ComputeEKin compute_ekin("ComputeEkin:" + this->getID());
  compute_ekin.build(cont);
  Real T_ = compute_ekin.evalArrayOutput().get(1);

  Real alpha = sqrt(1. + 1. / tau * (temperature / T_ - 1));
  if (alpha > 1.5)
    alpha = 1.5;

  for (auto &&at : cont) {
    DUMP("procede a la stimulation de l'atome a la position " << at, DBG_INFO);
    at.velocity() *= alpha;
  }
}

/* -------------------------------------------------------------------------- */

/* LMDESC BERENDSEN
   This stimulator applies a berendsen thermostat to maintain the temperature of
   atoms
*/

/* LMHERITATE action_interface */

/* LMEXAMPLE STIMULATION reset BERENDSEN INPUT md TEMP 150 DAMP 100 */

void StimulationBerendsen::declareParams() {
  StimulationInterface::declareParams();
  /* LMKEYWORD TEMP
     Sets the desired temperature
  */
  this->parseKeyword("TEMP", temperature);
  /* LMKEYWORD DAMP
     Sets the scaling strength (expressed in the units of time)
  */
  this->parseKeyword("DAMP", tau);
}

/* -------------------------------------------------------------------------- */

StimulationBerendsen::StimulationBerendsen(const std::string &name)
    : LMObject(name) {}

/* -------------------------------------------------------------------------- */

StimulationBerendsen::~StimulationBerendsen() {}

/* -------------------------------------------------------------------------- */

DECLARE_STIMULATION_MAKE_CALL(StimulationBerendsen, )

__END_LIBMULTISCALE__
