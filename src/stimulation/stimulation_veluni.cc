/**
 * @file   stimulation_veluni.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This stimulator allows to enforce a constant velocity motion
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "stimulation_veluni.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

template <UInt Dim, int Dir, typename Cont>
void apply_vel(Cont &cont, Real vel) {

  for (auto &&at : cont) {

    DUMP("proceed stimulation of atom " << at, DBG_DETAIL);
    at.velocity()[Dir] = vel;
    at.force()[Dir] = 0.0;
  }
}

/* -------------------------------------------------------------------------- */

template <UInt Dim, typename Cont> void apply_vel(Cont &cont, Vector<Dim> vec) {

  for (auto &&at : cont) {

    DUMP("proceed stimulation of atom " << at, DBG_DETAIL);
    for (UInt i = 0; i < Dim; ++i) {
      at.velocity()[i] = vec[i];
    }
    at.force().setZero();
  }
}

/* -------------------------------------------------------------------------- */
StimulationVelUni::StimulationVelUni(const std::string &name) : LMObject(name) {
  for (UInt i = 0; i < 3; ++i)
    vec[i] = 0.;
}

/* -------------------------------------------------------------------------- */

StimulationVelUni::~StimulationVelUni() {}

/* -------------------------------------------------------------------------- */

template <typename Cont> void StimulationVelUni::stimulate(Cont &cont) {

  constexpr UInt Dim = Cont::Dim;

  if (this->directions.size() == 0)
    apply_vel<Dim>(cont, this->vec.block<Dim, 1>(0, 0).template cast<Real>());
  else {
    for (UInt i = 0; i < directions.size(); ++i) {
      int dir = directions[i];
      if (dir >= 0 && dir < int(Dim)) {
        switch (dir) {
        case 0:
          apply_vel<Dim, 0>(cont, this->vec[0]);
          break;
        case 1:
          apply_vel<Dim, 1>(cont, this->vec[1]);
          break;
        case 2:
          apply_vel<Dim, 2>(cont, this->vec[2]);
          break;
        default:
          LM_FATAL("unknown dimension");
        }
      } else
        LM_FATAL("unknown dimension");
    }
  }
}

/* -------------------------------------------------------------------------- */

/* LMDESC VELUNI
   This stimulator allows to enforce a constant velocity motion.
   The way chosen to enforce this is by cancelling the force
   and imposing a constant velocity field as provided by keyword VEC. \ \
   The algorithm is:\                                                   \
   \begin{align*}
   \forall i \in dir, \quad f_i = 0 \quad v_i = VEC
   \end{align*}
*/

/* LMEXAMPLE STIMULATION uni VELUNI INPUT md VEC 1e-5 0 0 */

void StimulationVelUni::declareParams() {
  StimulationInterface::declareParams();
  /* LMKEYWORD VEC
     Specifies the vector to be imposed to the velocity field.
  */
  this->parseVectorKeyword("VEC", spatial_dimension, vec);

  /* LMKEYWORD DIR
     Specifies the direction of the constraint
     0 is X, 1 is Y, 2 is Z and -1(default) is all directions
  */
  this->parseKeyword("DIR", directions, std::vector<int>{});
}

/* -------------------------------------------------------------------------- */

DECLARE_STIMULATION_MAKE_CALL(StimulationVelUni, )

__END_LIBMULTISCALE__
