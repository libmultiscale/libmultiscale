/**
 * @file   stimulation_addforce.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Jul 07 11:25:00 2014
 *
 * @brief  Add a force to a set of atoms/nodes/points
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_STIMULATION_ADDFORCE_HH__
#define __LIBMULTISCALE_STIMULATION_ADDFORCE_HH__
/* -------------------------------------------------------------------------- */
#include "stimulation_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class StimulationAddForce : public StimulationInterface {

public:
  DECLARE_STIMULATION(StimulationAddForce, _or<MD, CONTINUUM>);

  //! most generic stimulate function
  template <typename _Input> void stimulate(_Input &cont);

private:
  Quantity<Force, 3> total_force;
  Quantity<Force, 3> per_atom_force;
};

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_STIMULATION_ADDFORCE_HH__ */
