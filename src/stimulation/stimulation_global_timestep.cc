/**
 * @file   stimulation_global_timestep.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This stimulator allows to change the integration timestep of a model
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "stimulation_global_timestep.hh"
#include "domain_interface.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_filter.hh"
#include "lib_md.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

StimulationGlobalTimeStep::StimulationGlobalTimeStep(const std::string &name)
    : LMObject(name) {}

/* -------------------------------------------------------------------------- */

StimulationGlobalTimeStep::~StimulationGlobalTimeStep() {}

/* -------------------------------------------------------------------------- */

template <typename Cont> void StimulationGlobalTimeStep::stimulate(Cont &) {
  LM_TOIMPLEMENT;
  // if (DomainInterface *ptr = dynamic_cast<DomainInterface *>(this->input)) {
  //   ptr->setTimeStep(globaltimestep);
  // } else
  //   LM_FATAL("invalid input");
}

/* -------------------------------------------------------------------------- */
/* LMDESC GLOBAL_TIMESTEP
   This stimulator allows to
   change the integration timestep of a model.
   Thus the input can only be a model plugin
   and cannot be a filter/compute.
*/

/* LMHERITATE action_interface */

/* LMEXAMPLE STIMULATION gts GLOBAL_TIMESTEP INPUT md TS 5 */

void StimulationGlobalTimeStep::declareParams() {
  StimulationInterface::declareParams();
  /* LMKEYWORD TS
     This specifies the required timestep.
  */
  this->parseKeyword("TS", globaltimestep);
}

/* -------------------------------------------------------------------------- */

DECLARE_STIMULATION_MAKE_CALL(StimulationGlobalTimeStep, )

__END_LIBMULTISCALE__
