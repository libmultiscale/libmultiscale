/**
 * @file   stimulation_block.hh
 *
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Wed Jan 22 17:41:05 2014
 *
 * @brief  blocks nodes of a fem mesh, thus eliminating equations
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_STIMULATION_BLOCK_HH__
#define __LIBMULTISCALE_STIMULATION_BLOCK_HH__

/* -------------------------------------------------------------------------- */
#include "stimulation_interface.hh"
__BEGIN_LIBMULTISCALE__

class StimulationBlock : public StimulationInterface {

public:
  DECLARE_STIMULATION(StimulationBlock, _or<MD>);

  template <typename _Input> void stimulate(_Input &cont);

private:
  // directions in which blockage should be applied
  Vector<3, bool> block;
};

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_STIMULATION_BLOCK_HH__ */
