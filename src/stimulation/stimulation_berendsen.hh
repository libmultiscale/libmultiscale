/**
 * @file   stimulation_berendsen.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Dec 10 16:00:49 2013
 *
 * @brief  This stimulator applies a berendsen thermostat to maintain the
 * temperature of atoms
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_STIMULATION_BERENDSEN_HH__
#define __LIBMULTISCALE_STIMULATION_BERENDSEN_HH__
/* -------------------------------------------------------------------------- */
#include "stimulation_interface.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class StimulationBerendsen : public StimulationInterface {

public:
  DECLARE_STIMULATION(StimulationBerendsen, _or<MD>);

  //! most generic stimulate function
  template <typename _Input> void stimulate(_Input &cont);

private:
  Quantity<Temperature> temperature;
  Quantity<Time> tau;
};
__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_STIMULATION_BERENDSEN_HH__ */
