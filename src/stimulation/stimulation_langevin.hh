/**
 * @file   stimulation_langevin.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Fri Nov 08 12:27:05 2013
 *
 * @brief  This stimulator applies a Langevin thermostat
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef STIMULATION_LANGEVIN_H
#define STIMULATION_LANGEVIN_H

/* -------------------------------------------------------------------------- */
#include "math_tools.hh"
#include "stimulation_interface.hh"
#include "units.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class StimulationLangevin : public StimulationInterface {

public:
  DECLARE_STIMULATION(StimulationLangevin, _or<MD>);

  //! most generic stimulate function
  template <typename _Input> void stimulate(_Input &cont);

  template <bool computeWork, bool computeWorkPerAtom, typename _Input>
  void __attribute__((noinline)) addForce(_Input &cont);

  template <bool computeWork, bool computeWorkPerAtom, typename _Input>
  void frictionCorrection(_Input &cont);

  template <bool computeWork, bool computeWorkPerAtom, typename _Input>
  void restitutionCorrection(_Input &cont);

  // template <bool computeWork, bool computeWorkPerAtom>
  // void accountFrictionWork(Real friction_term, Real velocity, UInt index);
  // template <bool computeWork, bool computeWorkPerAtom>
  // void accountRestitutionWork(Real friction_term, Real velocity, UInt index);

  template <typename _Input> void resetOldWorkPerAtom(_Input &cont);
  template <typename _Input> void gatherWork(_Input &cont);

  Real computeEnergy(std::vector<Real> &force);

  void init() override;

  //! return per atom friction work
  auto &getFrictionWorkPerAtom() {
    return this->getOutputAsArray("friction_work_per_atom");
  };
  //! return per atom restitution work
  auto &getRestitutionWorkPerAtom() {
    return this->getOutputAsArray("restitution_work_per_atom");
  };
  //! return friction work
  Real getFrictionWork() { return friction_work; };
  //! return global restitution work
  Real getRestitutionWork() { return restitution_work; };
  // //! return temperature input
  Real getTemperature() { return this->temperature; };

protected:
  Quantity<Time> damp;
  Quantity<Temperature> temperature;
  Quantity<Time> timestep;
  long seed;

  //! compute information
  // ComputeCompatibility friction_restitution_work;
  //! compute for per-atom information
  // ComputeCompatibility old_friction_per_atom;
  // ComputeCompatibility old_restitution_per_atom;
  // ComputeCompatibility friction_work_per_atom;
  // ComputeCompatibility restitution_work_per_atom;

  //! current frictional work
  Real friction_work;
  Real restitution_work;

  //! cumulated frictional work
  Real cumulated_friction_work;
  Real cumulated_restitution_work;

  //! flag to decide if the work should be computed
  bool compute_work_flag;
  //! flag to decide if the work per atom should be computed
  bool compute_work_peratom_flag;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif
