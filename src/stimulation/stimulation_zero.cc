

/**
 * @file   stimulation_raz.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  Remise a zero (reset to zero) stimulator
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "stimulation_zero.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
#include "ref_point_data.hh"
#include <iomanip>

/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

StimulationZero::StimulationZero(const std::string &name)
    : LMObject(name), COEF(0), average_flag(false), direction(-1) {}

/* -------------------------------------------------------------------------- */

StimulationZero::~StimulationZero() {}

/* -------------------------------------------------------------------------- */

template <typename Cont>
enable_if_md<Cont> StimulationZero::stimulate(Cont &cont) {

  constexpr UInt Dim = Cont::Dim;
  std::vector<UInt> directions;
  if (this->direction < 0) {
    for (UInt i = 0; i < Dim; ++i) {
      directions.push_back(i);
    }
  } else {
    directions.push_back(this->direction);
  }

  UInt dim = Dim == 2 ? 1 : Dim;
  std::vector<UInt> directions2;
  for (UInt i = 0; i < dim; ++i) {
    directions2.push_back(i);
  }
 

  typedef std::vector<UInt>::iterator iter;

  DUMP("Stimulation ZERO fields "
           << " COEF : " << COEF << " pointer this " << this,
       DBG_INFO);

  for (UInt i = 0; i < field.size(); ++i) {
    FieldType f = field[i];

    for (auto &&at : cont) {
      if (f == _velocity) {
        for (iter i = directions.begin(); i != directions.end(); ++i) {
          at.velocity()[*i] += (COEF - 1) * at.velocity()[*i];
        }
      } else if (f == _displacement) {
        for (iter i = directions.begin(); i != directions.end(); ++i) {
          at.displacement()[*i] += (COEF - 1) * at.displacement()[*i];
        }
      } else if (f == _force) {
        for (iter i = directions.begin(); i != directions.end(); ++i) {
          at.force()[*i] += (COEF - 1) * at.force()[*i];
        }
      } else if (f == _position0) {
        for (iter i = directions.begin(); i != directions.end(); ++i) {
          at.position0()[*i] = at.position()[*i];
        }
      } else if (f == _torque) {
        for (iter i = directions2.begin(); i != directions2.end(); ++i) {
          at.torque()[*i] += (COEF - 1) * at.torque()[*i];
        }
      } else if (f == _angular_velocity) {
        for (iter i = directions2.begin(); i != directions2.end(); ++i) {
          at.angular_velocity()[*i] += (COEF - 1) * at.angular_velocity()[*i];
        }      
      }
    }
  }
}

template <typename Cont>
enable_if_not_md<Cont> StimulationZero::stimulate(Cont &cont) {

  constexpr UInt Dim = Cont::Dim;
  std::vector<UInt> directions;
  if (this->direction < 0) {
    for (UInt i = 0; i < Dim; ++i) {
      directions.push_back(i);
    }
  } else {
    directions.push_back(this->direction);
  }
  typedef std::vector<UInt>::iterator iter;

  DUMP("Stimulation ZERO fields "
           << " COEF : " << COEF << " pointer this " << this,
       DBG_INFO);

  for (UInt i = 0; i < field.size(); ++i) {
    FieldType f = field[i];

    for (auto &&at : cont) {
      if (f == _velocity) {
        for (iter i = directions.begin(); i != directions.end(); ++i) {
          at.velocity()[*i] += (COEF - 1) * at.velocity()[*i];
        }
      } else if (f == _displacement) {
        for (iter i = directions.begin(); i != directions.end(); ++i) {
          at.displacement()[*i] += (COEF - 1) * at.displacement()[*i];
        }
      } else if (f == _force) {
        for (iter i = directions.begin(); i != directions.end(); ++i) {
          at.force()[*i] += (COEF - 1) * at.force()[*i];
        }
      } else if (f == _position0) {
        for (iter i = directions.begin(); i != directions.end(); ++i) {
          at.position0()[*i] = at.position()[*i];
        }
      }
    }
  }
}

/* -------------------------------------------------------------------------- */

/* LMDESC ZERO

   Remise a zero (reset to zero) stimulator\\

   This stimulator reset some fields to zero or even rescale them according
   to the options provided.
*/
/* LMEXAMPLE STIMULATION reset ZERO INPUT md FIELD displacement FIELD velocity
 */
/* LMHERITANCE action_interface */

void StimulationZero::declareParams() {
  StimulationInterface::declareParams();

  /* LMKEYWORD FIELD
     Set the fields that should be altered.
     The value selected can be:

     - displacement : selects the displacement field
     - velocity : selects the velocity field
     - force : selects the force field
     - torque : selects the torque field (valid for DEM)
     - angular_velocity : selects the angular velocity field (valid for DEM)
     - position0 : selects the initial position field. Actually it is
       relevant for atomic systems assigning the initial positions to the
       current one. This is useful when a restart file needs to be generated
       with current positions and a zero displacement field.
     
     This command can be called several times for each field that need to be
     altered.
  */

  this->parseKeyword("FIELD", field);

  /* LMKEYWORD COEF
     This is effective only for displacement, velocity or force field.
     Instead of reset to zero it multiplies by the provided coefficient.
     It enables, for instance to perform velocity damping.
  */
  this->parseKeyword("COEF", COEF, 0.);

  /* LMKEYWORD COMPONENT
     Just apply stimulation in direction COMPONENT. By default all directions
     are stimulated.
  */
  this->parseKeyword("COMPONENT", direction, -1);
}

/* -------------------------------------------------------------------------- */

DECLARE_STIMULATION_MAKE_CALL(StimulationZero, )

__END_LIBMULTISCALE__
