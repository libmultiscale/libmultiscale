/**
 * @file   arlequin_template.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Nov 25 15:05:56 2013
 *
 * @brief  Internal class to factor code for the Arlequin kind methods
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_ARLEQUIN_TEMPLATE_HH__
#define __LIBMULTISCALE_ARLEQUIN_TEMPLATE_HH__
/* -------------------------------------------------------------------------- */
#include "bridging_atomic_continuum.hh"
#include "compute_arlequin_weight.hh"
#include "coupling_atomic_continuum.hh"
#include "lib_continuum.hh"
#include "lib_md.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class ArlequinTemplate : public CouplingAtomicContinuum {

public:
  ArlequinTemplate(const std::string &name);
  ~ArlequinTemplate() = default;
  void declareParams();

  //! The bridging zone
  BridgingAtomicContinuum bridging_zone;
  //! The bridge zone used for surface effects
  BridgingAtomicContinuum boundary_zone;

protected:
  DECORATE_FUNCTION_DISPATCH(computeAtomicWeights, subMD)
  //! Compute the weighting function of the coupled MD DOFs
  template <typename ContA> void computeAtomicWeights(ContA &pointList);
  DECORATE_FUNCTION_DISPATCH(computeContinuumWeights, subCONTINUUM)
  //! Compute the weighting function of the coupled MD DOFs
  template <typename ContC> void computeContinuumWeights(ContC &meshList);

protected:
  //! geometry id for the bridging zone
  LMID bridging_geom;
  //! geometry id for the boundary zone
  LMID boundary_geom;

  //! weighting vector for FE DOFs
  ComputeArlequinWeight weight_mesh;
  //! weighting vector for MD DOFs
  ComputeArlequinWeight weight_point;

  //! weighting vector for FE DOFs
  ContainerArray<Real> lambdas_mesh;
  //! weighting vector for MD DOFs
  ContainerArray<Real> lambdas_point;
};
/* ------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
#endif /* __LIBMULTISCALE_ARLEQUIN_TEMPLATE_HH__ */
