#include "coupling_atomic_continuum.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

CouplingAtomicContinuum::CouplingAtomicContinuum(const std::string &name)
    : LMObject(name), CouplingInterface(name) {}

CouplingAtomicContinuum::~CouplingAtomicContinuum() {}

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
