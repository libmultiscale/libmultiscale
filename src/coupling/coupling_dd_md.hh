/**
 * @file   coupling_dd_md.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed May 22 17:37:59 2013
 *
 * @brief  Coupling object between atomistic and dislocation dynamics
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COUPLING_DD_MD_HH__
#define __LIBMULTISCALE_COUPLING_DD_MD_HH__
/* -------------------------------------------------------------------------- */
#include "coupling_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/** this class represent a low-level coupling zone between dd and md.
    The Needleman Method for instance is based on it */

class CouplingDDMD : public CouplingInterface {

public:
  CouplingDDMD(const std::string &name);
  virtual ~CouplingDDMD() {}

  template <typename DomainA> auto getDomA() -> DomainA &;
  template <typename DomainDD> auto getDomDD() -> DomainDD &;

protected:
  //! group ID of the continuum side
  CommGroup comm_group_dd;
  //! group IF of the atomic side
  CommGroup comm_group_atomic;

  //! flag if in group continuum
  bool is_in_dd;
  //! flag if in group atomic
  bool is_in_atomic;
};
__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_COUPLING_DD_MD_HH__ */
