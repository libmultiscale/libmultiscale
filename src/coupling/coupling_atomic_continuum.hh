/**
 * @file   coupling_atomic_continuum.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed May 22 17:27:21 2013
 *
 * @brief  Coupling object between atomistic and dislocation dynamics
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#ifndef COUPLING_ATOMIC_CONTINUUM_H
#define COUPLING_ATOMIC_CONTINUUM_H
/* -------------------------------------------------------------------------- */
#include "coupling_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/** TODO : review doc */

class CouplingAtomicContinuum : public CouplingInterface {

public:
  CouplingAtomicContinuum(const std::string &name);
  virtual ~CouplingAtomicContinuum();

protected:
  //! flag if in group continuum
  bool is_in_continuum() { return comm_group_continuum().amIinGroup(); };
  //! flag if in group atomic
  bool is_in_atomic() { return comm_group_atomic().amIinGroup(); };
  //! group IF of the atomic side
  CommGroup &comm_group_atomic() { return this->comm_group_A; };
  //! group ID of the continuum side
  CommGroup &comm_group_continuum() { return this->comm_group_B; };
};
/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__

#endif // COUPLING_ATOMIC_CONTINUUM
