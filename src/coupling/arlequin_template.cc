/**
 * @file   arlequin_template.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Nov 25 15:05:56 2013
 *
 * @brief  Internal class to factor code for the Arlequin kind methods
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

/* -------------------------------------------------------------------------- */
#include "arlequin_template.hh"
#include "bridging.hh"
#include "bridging_atomic_continuum.hh"
#include "factory_multiscale.hh"
#include "lm_common.hh"
#include <fstream>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

ArlequinTemplate::ArlequinTemplate(const std::string &name)
    : LMObject(name), CouplingAtomicContinuum(name),
      bridging_zone(this->getID() + "-bridging"),
      boundary_zone(this->getID() + "-boundary"),
      weight_mesh("weight-mesh-" + name), weight_point("weight-point-" + name),
      lambdas_mesh("lambdas-mesh-" + name),
      lambdas_point("lambdas-point-" + name) {

  this->weight_mesh.setCommGroup(comm_group_continuum());
  this->lambdas_mesh.setCommGroup(comm_group_continuum());

  this->weight_point.setCommGroup(comm_group_atomic());
  this->lambdas_point.setCommGroup(comm_group_atomic());

  this->createOutput("weight-mesh") = this->weight_mesh;
  this->createOutput("lambdas-mesh") = this->lambdas_mesh;
  this->createOutput("weight-point") = this->weight_point;
  this->createOutput("lambdas-point") = this->lambdas_point;
}

/* -------------------------------------------------------------------------- */
INSTANCIATE_DISPATCH(ArlequinTemplate::computeAtomicWeights)
template <typename ContA>
void ArlequinTemplate::computeAtomicWeights(ContA &pointList) {

  // point weights
  this->weight_point.compute(pointList);
  bridging_zone.attachVector(this->weight_point.evalArrayOutput());

  lambdas_point.assign(weight_point.evalArrayOutput().size(), 0);
  this->bridging_zone.attachVector(lambdas_point);

  auto &&weights = weight_point.evalArrayOutput();

  for (auto &&[at, lbda, weight] : zip(pointList, lambdas_point, weights)) {
    lbda = weight * at.mass();
  }
}
/* -------------------------------------------------------------------------- */

INSTANCIATE_DISPATCH(ArlequinTemplate::computeContinuumWeights)
template <typename ContC>
void ArlequinTemplate::computeContinuumWeights(ContC &meshList) {

  // mesh weights

  this->weight_mesh.compute(meshList);
  lambdas_mesh.assign(weight_mesh.evalArrayOutput().size(), 0);

  for (auto &&[nd, lbda, weight] :
       zip(meshList.getContainerNodes(), lambdas_mesh,
           weight_mesh.evalArrayOutput())) {
    lbda = weight * nd.mass();
  }
}
/* -------------------------------------------------------------------------- */

/* LMDESC ArlequinTemplate
   This class is used internally.

   It is to be used
   while two zones are declared, one for the coupling
   and one for providing a stiff boundary condition to the atoms.
   In the coupling a linear weight function is built.
*/

/* LMHERITANCE dof_association compute_arlequin_weight */

void ArlequinTemplate::declareParams() {

  this->addSubParsableObject(bridging_zone);
  this->addSubParsableObject(boundary_zone);
  this->addSubParsableObject(weight_mesh);
  this->addSubParsableObject(weight_point);

  /* LMKEYWORD GEOMETRY
   Set the bridging/overlaping zone where the Lagrange multipliers are
   to be computed.
  */
  this->parseKeyword("GEOMETRY", bridging_geom);

  /* LMKEYWORD BOUNDARY
     Set the boundary geometry where the atom velocities are to be fixed from
     the interpolated finite element velocities.
  */
  this->parseKeyword("BOUNDARY", boundary_geom);
  
}

/* ------------------------------------------------------------------------ */

__END_LIBMULTISCALE__
