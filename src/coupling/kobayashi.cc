/**
 * @file   kobayashi.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Jul 11 15:47:44 2014
 *
 * @brief  Kobayashi's bridging method
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "kobayashi.hh"
#include "arlequin_template.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
Kobayashi::Kobayashi(const std::string &name)
    : LMObject(name), ArlequinTemplate(name) {

  // naming the bridging zones
  std::stringstream fname;
  fname << name << "-bridging";
}
/* -------------------------------------------------------------------------- */
Kobayashi::~Kobayashi() {}

/* -------------------------------------------------------------------------- */

template <typename DomainA, typename DomainC>
void Kobayashi::init(DomainA &domA, DomainC &domC) {
  if (lm_world_size > 1)
    LM_FATAL("this coupler works only in sequential");
  std::stringstream fname;

  // initialize the bridging_zone object
  bridging_zone.init(domA.getContainer(), domC.getContainer());

  // build weights
  this->computeAtomicWeights<dispatch>(this->bridging_zone.pointList);
  this->computeContinuumWeights<dispatch>(this->bridging_zone.meshList);

  DUMP("Computing initial least square constraint", DBG_INFO);
  buildCGMatrix();

  // treat now MD boundary zone
  boundary_zone.init(domA.getContainer(), domC.getContainer());

  DUMP("Init ... done", DBG_INFO_STARTUP);
}

/* -------------------------------------------------------------------------- */

template <typename DomainA, typename DomainC>
void Kobayashi::coupling(CouplingStage stage, DomainA &domA [[gnu::unused]],
                         DomainC &domC [[gnu::unused]]) {

  if (stage != COUPLING_STEP4)
    return;

  boundary_zone.projectAtomicFieldOnMesh(_velocity);
  boundary_zone.projectAtomicFieldOnMesh(_displacement);

  correctContinuum(_velocity);
  correctContinuum(_displacement);
  correctAtoms(_velocity);
  correctAtoms(_displacement);
}

/* -------------------------------------------------------------------------- */

void Kobayashi::buildCGMatrix() {

  auto &smatrix = bridging_zone.smatrix;
  this->A = smatrix * smatrix.transpose();
  this->solver.compute(this->A);
}

/* -------------------------------------------------------------------------- */

ContainerArray<Real> &Kobayashi::buildLeastSquareField(FieldType field_type) {
  auto &smatrix = bridging_zone.smatrix;
  ComputeExtract field(this->getID());
  field.setParam("FIELD", field_type);
  field.compute(this->bridging_zone.pointList);
  Eigen::VectorXd rhs = smatrix * field.evalArrayOutput().matrix();
  Eigen::VectorXd x = this->solver.solve(rhs.matrix());
  auto &ls_field = this->bridging_zone.buffer_for_nodes;
  ls_field.resize(x.rows(), x.cols());
  ls_field = x;
  return ls_field;
}

/* -------------------------------------------------------------------------- */

void Kobayashi::correctContinuum(FieldType field_type) {

  auto &LS_field = this->buildLeastSquareField(field_type);
  auto &weight_mesh = this->weight_mesh.evalArrayOutput();

  ComputeExtract field(this->getID());
  field.setParam("FIELD", field_type);
  field.compute(this->bridging_zone.meshList);

  for (auto &&[w, ls_f, f] : zip(weight_mesh, LS_field.rowwise(),
                                 field.evalArrayOutput().rowwise())) {
    if (w <= .5) {
      Real speed_projection = this->quality;
      if (w <= 1e-2)
        speed_projection = 1;
      ls_f = speed_projection * (ls_f - f);
    }
  }
  this->bridging_zone.addMeshField(field_type, LS_field);
}
/* -------------------------------------------------------------------------- */

void Kobayashi::correctAtoms(FieldType field_type) {

  ContainerArray<Real> LS_field;
  bridging_zone.projectAtomicFieldOnMesh(field_type, LS_field);
  auto &weightMD = this->weight_point.evalArrayOutput();

  ComputeExtract field(this->getID());
  field.setParam("FIELD", field_type);
  field.compute(this->bridging_zone.pointList);

  for (auto &&[w, ls_f, f] :
       zip(weightMD, LS_field.rowwise(), field.evalArrayOutput().rowwise())) {
    if (w <= .5) {
      Real speed_projection = this->quality;
      if (w <= 1e-2)
        speed_projection = 1;
      ls_f = speed_projection * (ls_f - f);
    }
  }
  this->bridging_zone.addPointField(field_type, LS_field);
}
/* -------------------------------------------------------------------------- */

/* LMDESC KOBAYASHI
   This keyword is used to set a coupling method
   which will implement the method of Kobayashi and co-workers
   *A simple dynamical scale-coupling method for concurrent simulation of
   hybridized atomistic/coarse-grained-particle system,*
   **Ryo Kobayashi, Takahide Nakamura, Shuji Ogata**,
   *International Journal for Numerical Methods in Engineering Volume 83,
   Issue 2, pages 249-268, 9 July 2010.*

   This coupling component works only in 1D and in sequential.
*/

/* LMHERITANCE arlequin_template */

/* LMEXAMPLE
   COUPLING_CODE md fe KOBAYASHI GEOMETRY 3 BOUNDARY 4 GRID_DIVISIONX 10 QUALITY
   1e-3 VERBOSE
 */

void Kobayashi::declareParams() { ArlequinTemplate::declareParams(); }
/* -------------------------------------------------------------------------- */

DECLARE_COUPLER_INIT_MAKE_CALL(Kobayashi, domA, domC)
DECLARE_COUPLING_MAKE_CALL(Kobayashi, domA, domC)

__END_LIBMULTISCALE__
