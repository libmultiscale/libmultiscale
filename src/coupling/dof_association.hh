/**
 * @file   dof_association.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Jan 21 12:37:46 2014
 *
 * @brief  Mother of all object which associates DOFs for coupling purpose
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_DOF_ASSOCIATION_INTERFACE_HH__
#define __LIBMULTISCALE_DOF_ASSOCIATION_INTERFACE_HH__
/* -------------------------------------------------------------------------- */
#include "compute_extract.hh"
#include "duo_distributed_vector.hh"
#include "lm_parsable.hh"
#include "spatial_grid_libmultiscale.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class DofAssociation : public Parsable, public Component {

  //****************************************************************
  // non templated functions
  //****************************************************************

public:
  DofAssociation(const std::string &name);
  virtual ~DofAssociation();

  //! return the local number of matched points (parallel safe)
  // virtual UInt getNumberLocalMatchedPoints();
  //! function used to synchronize vectors (by summing them)
  void synchronizeVectorBySum(const std::string &name_vec,
                              ContainerArray<Real> &vec);

  template <typename ContA, typename ContB>
  void init(ContA &contA, ContB &contB);

protected:
  //! function used to redistribute a vector from B to A
  void distributeVectorB2A(const std::string &name_vec,
                           ContainerArray<Real> &vec);
  //! function used to redistribute a vector from A to B
  void distributeVectorA2B(const std::string &name_vec,
                           ContainerArray<Real> &vec);
  //! create duo vector
  void createDuoVectorB(const std::string &name,
                        ContainerArray<UInt> &mappingA2B);
  //! function used update the duo vector for migrations
  void synchronizeMigration(CommGroup group1, CommGroup group2);
  void declareParams() override;
  //! get duo vector
  std::shared_ptr<DuoDistributedVector> &getDuoVector();
  //! exchange points positions following coarse description
  void exchangePositions(UInt Dim);
  //! exchange association information for full communication scheme description
  void exchangeAssociationInformation(ContainerArray<UInt> &managed,
                                      ContainerArray<UInt> &mappingA2B);
  //! missing ducomentation
  void createDuoVectorA(const std::string &name, ContainerArray<UInt> &managed,
                        ContainerArray<UInt> &mappingA2B,
                        std::vector<std::vector<UInt>> &unassociated_points);
  //! exchange duplicate points info from A processors to B ones
  void exchangeRejectedContinuumOwners(
      std::vector<std::vector<UInt>> &unassociated_points);
  //! filter associations so that removed DOF are acknowledged
  void filterMapping(ContainerArray<UInt> &mappingA2B);

  //! am i in group A
  bool in_group_A();
  //! am i in group B
  bool in_group_B();

private:
  void compute_make_call() override{};

  //! exchange geometries: for coarse desc of communication scheme
  void exchangeGeometries(Cube &cube);

  UInt exchangeNbPointsPerProc();

  //****************************************************************
  // templated functions
  //****************************************************************

protected:
  DECORATE_FUNCTION_DISPATCH(exchangeGeometries, _or<MD, CONTINUUM>)
  //! exchange geometries: for coarse desc of communication scheme
  template <typename Cont> void exchangeGeometries(Cont &sub);
  DECORATE_FUNCTION_DISPATCH(buildPositions, subMD)
  //! extract a position vector from a subset container
  template <typename ContA> void buildPositions(ContA &sub);

protected:
  // to parametrize the grid to speed search (decomposition in each direction)
  Vector<3, UInt> grid_division;
  //! the intersection geometry ID
  LMID geomID;
  //! parallel id of group A
  CommGroup comm_group_A;
  //! parallel id of group B
  CommGroup comm_group_B;
  //! array that specifies what processor I should communicate with
  std::vector<UInt> com_with;
  //! total number of points in the associated zone
  UInt total_points;
  //! number of successful associations
  UInt found_associations;
  //! array that specifies the number of points per proc
  std::vector<UInt> nb_points_per_proc;
  //! generated positions (by BuildPosition method)
  ContainerArray<Real> positions;

  //! flag to know wether we have to check for coherency
  bool check_coherency;

protected:
  //! duodistributed vector used to synchronize vectors/matrices
  std::shared_ptr<DuoDistributedVector> duo_vector;
};

/* -------------------------------------------------------------------------- */

template <typename Cont> void DofAssociation::exchangeGeometries(Cont &sub) {
  if (comm_group_A == comm_group_B)
    return;

  exchangeGeometries(sub.getBoundingBox());
}
/* -------------------------------------------------------------------------- */

template <typename ContA, typename ContB>
void DofAssociation::init(ContA &contA, ContB &contB) {

  comm_group_A = contA.getCommGroup();
  comm_group_B = contB.getCommGroup();

  UInt nb_zone_A = comm_group_A.size();
  UInt nb_zone_B = comm_group_B.size();

  DUMP(this->getID() << ":in group A = " << in_group_A(), DBG_DETAIL);
  DUMP(this->getID() << ":in group B = " << in_group_B(), DBG_DETAIL);

  if (in_group_A()) {
    com_with.resize(nb_zone_B);
    com_with.assign(false, nb_zone_B);
    // this->acquireContext(contA);
  }
  if (in_group_B()) {
    com_with.resize(nb_zone_A);
    com_with.assign(false, nb_zone_A);
    // this->acquireContext(contB);
  }
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_DOF_ASSOCIATION_INTERFACE_HH__ */
