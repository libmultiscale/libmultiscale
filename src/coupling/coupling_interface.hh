/**
 * @file   coupling_interface.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jan 15 17:00:43 2014
 *
 * @brief  Interface to all coupling
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COUPLING_INTERFACE_HH__
#define __LIBMULTISCALE_COUPLING_INTERFACE_HH__
/* -------------------------------------------------------------------------- */
#include "component_libmultiscale.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_communicator.hh"
#include "lm_parsable.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class CouplingInterface : public Parsable, public Component {

public:
  CouplingInterface(const std::string &name)
      : LMObject(name),
        comm_group_A(Communicator::getCommunicator().getGroup("all")),
        comm_group_B(Communicator::getCommunicator().getGroup("all")) {}
  virtual ~CouplingInterface() {}

  template <typename ContA, typename ContB>
  void init(ContA &contA, ContB &contB);

  //! function for applying the coupling
  virtual void coupling(CouplingStage stage) = 0;
  //! function for initiation
  virtual void init() { LM_TOIMPLEMENT; };

  virtual void compute_make_call() {
    DUMP("so far coupler action as a component does nothing", DBG_WARNING);
  }

protected:
  //! am i in group A
  bool in_group_A();
  //! am i in group B
  bool in_group_B();

  //! group ID of the continuum side
  CommGroup comm_group_A;
  //! group IF of the atomic side
  CommGroup comm_group_B;
};
/* -------------------------------------------------------------------------- */

inline bool CouplingInterface::in_group_A() {
  return comm_group_A.amIinGroup();
}
/* ------------------------------------------------------------------------ */

inline bool CouplingInterface::in_group_B() {
  return comm_group_B.amIinGroup();
}

/* ------------------------------------------------------------------------ */

template <typename ContA, typename ContB>
void CouplingInterface::init(ContA &contA, ContB &contB) {

  comm_group_A = contA.getCommGroup();
  comm_group_B = contB.getCommGroup();

  DUMP(this->getID() << ":in group A = " << in_group_A(), DBG_DETAIL);
  DUMP(this->getID() << ":in group B = " << in_group_B(), DBG_DETAIL);
}

__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
#define DECLARE_COUPLER(class_name, ...)                                       \
  class_name(const std::string &name);                                         \
  virtual ~class_name();                                                       \
  DECORATE_FUNCTION_DISPATCH(coupling, CouplingStage, __VA_ARGS__)             \
  DECORATE_FUNCTION_DISPATCH(init, __VA_ARGS__)                                \
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW                                              \
                                                                               \
  void connect(DomainInterface &, DomainInterface &);                          \
  void coupling(CouplingStage stage) override;                                 \
  void init() override;                                                        \
  void declareParams() override

#define DECLARE_COUPLER_INIT_MAKE_CALL(class_name, input1, input2)             \
  void class_name::connect(DomainInterface &domA, DomainInterface &domB) {     \
    this->createInput(stringify_macro(input1)) = domA;                         \
    this->createInput(stringify_macro(input2)) = domB;                         \
  }                                                                            \
                                                                               \
  void class_name::init() {                                                    \
    try {                                                                      \
      this->init<dispatch>(this->getInput(stringify_macro(input1)),            \
                           this->getInput(stringify_macro(input2)));           \
    } catch (Component::UnconnectedInput & e) {                                \
      LM_FATAL("Unconnected input for component " << this->getID()             \
                                                  << std::endl                 \
                                                  << e.what());                \
    }                                                                          \
  }
/* -------------------------------------------------------------------------- */
#define DECLARE_COUPLING_MAKE_CALL(class_name, input1, input2)                 \
                                                                               \
  void class_name::coupling(CouplingStage stage) {                             \
    try {                                                                      \
      this->coupling<dispatch>(stage, this->getInput(stringify_macro(input1)), \
                               this->getInput(stringify_macro(input2)));       \
    } catch (Component::UnconnectedInput & e) {                                \
      LM_FATAL("Unconnected input for component " << this->getID()             \
                                                  << std::endl                 \
                                                  << e.what());                \
    }                                                                          \
  }

/* -------------------------------------------------------------------------- */
// file generated by CMake
#include "coupler_list.hh"
/* -------------------------------------------------------------------------- */

#endif /* __LIBMULTISCALE_COUPLING_INTERFACE_HH__ */
