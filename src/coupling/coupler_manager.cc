/**
 * @file   coupler_manager.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Sep 09 15:15:46 2014
 *
 * @brief  Manager of coupling components
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* --------------------------------------------------------------------------
Copyright INRIA and CEA

author : Guillaume ANCIAUX (guillaume.anciaux@epfl.ch)

The LibMultiScale is a C++ parallel framework for the multiscale
coupling methods dedicated to material simulations. This framework
provides an API which makes it possible to program coupled simulations
and integration of already existing codes.

This Project is done in a collaboration between INRIA Futurs Bordeaux
within ScAlApplix team and CEA/DPTA Ile de France.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
----------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
#include "coupler_manager.hh"
#include "domain_multiscale.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
#include "lib_coupler.hh"
#include "lm_parser.hh"
/* -------------------------------------------------------------------------- */
#include <boost/preprocessor.hpp>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

CouplerManager::CouplerManager()
    : LMObject("CouplerManager"), CouplingInterface("CouplerManager") {}
/* -------------------------------------------------------------------------- */

CouplerManager::~CouplerManager() {}
/* -------------------------------------------------------------------------- */

void CouplerManager::coupling(CouplingStage stage) {

  for (auto &&[name, coupler] : objects) {
    coupler->coupling(stage);
  }
}
/* -------------------------------------------------------------------------- */
#define BOOST_PARSE_COUPLER_KEY(r, data, n, x)                                 \
  else if (key == stringify_macro(BOOST_PP_TUPLE_ELEM(4, 1, x)))               \
      coupler_type = n;

/* -------------------------------------------------------------------------- */

ParseResult CouplerManager::parseCouplingLine(std::stringstream &line,
                                              UInt Dim) {

  DUMP("parsing COUPLING_CODE commande = " << line.str(), DBG_INFO_STARTUP);
  std::string coupler_name;
  Parser::parse(coupler_name, line);
  if (objects.count(coupler_name))
    LM_FATAL("coupler name " << coupler_name << " was alread used");

  // retreive the domain to be coupled
  LMID IDA, IDC;
  Parser::parse(IDA, line);
  Parser::parse(IDC, line);
  auto &domA = DomainMultiScale::getManager().getObject(IDA);
  auto &domC = DomainMultiScale::getManager().getObject(IDC);

  // parsing the coupler code
  UInt coupler_type = UINT_MAX;
  std::string key;
  Parser::parse(key, line);

  if (2 == 1) {
  }
  BOOST_PP_SEQ_FOR_EACH_I(BOOST_PARSE_COUPLER_KEY, f, LIST_COUPLER)
  else LM_FATAL("Unknown coupler type "
                << key
                << " maybe you should compile support for this coupler ?");

  // create the coupler object
  std::shared_ptr<CouplingInterface> coupler =
      build(coupler_type, coupler_name, domA, domC, Dim);

  // forge a new string from truncated stringstream to set options
  std::string buffer = line.str().substr(line.tellg());
  ParseResult parsed = coupler->parseLine(buffer);
  Parser::shiftLine(parsed, line);
  // check that options are all set
  coupler->checkAllKeywordsAreParsed();
  // call the init function
  coupler->init();

  // append the new object to the list
  this->addObject(coupler);

  return true;
}
/* -------------------------------------------------------------------------- */
/* boost stuff for instanciation:                                             */
/* DO NOT TOUCH UNLESS YOU KNOW WHAT YOU ARE DOING                            */
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
#define GET_TYPE1(x) BOOST_PP_TUPLE_ELEM(4, 2, x)
#define GET_TYPE2(x) BOOST_PP_TUPLE_ELEM(4, 3, x)
#define GET_CLASS_COUPLER(x) BOOST_PP_TUPLE_ELEM(4, 0, x)
/* -------------------------------------------------------------------------- */

template <typename Coupler, typename DomA, typename DomB>
auto create_and_connect_coupler(const std::string &name, DomA &&domA,
                                DomB &&domB) {
  auto coupler = std::make_shared<Coupler>(name);
  coupler->connect(std::forward<DomA>(domA), std::forward<DomB>(domB));
  return coupler;
}

/* -------------------------------------------------------------------------- */

#define BOOST_SWITCH_CASE_COUPLER(r, data, n, x)                               \
  case n: {                                                                    \
    using classCoupling = GET_CLASS_COUPLER(x);                                \
    coupler =                                                                  \
        create_and_connect_coupler<classCoupling>(coupler_name, domA, domB);   \
    break;                                                                     \
  }

/* -------------------------------------------------------------------------- */

std::shared_ptr<CouplingInterface>
CouplerManager::build(UInt coupler_type, const std::string &coupler_name,
                      DomainInterface &domA, DomainInterface &domB, UInt Dim) {

  std::shared_ptr<CouplingInterface> coupler;

  switch (coupler_type) {
    BOOST_PP_SEQ_FOR_EACH_I(BOOST_SWITCH_CASE_COUPLER, ~, LIST_COUPLER)
  default:
    LM_FATAL("Internal Error");
  }
  if (coupler == NULL)
    LM_FATAL("Error : Internal error (allocation failed ?): "
             << coupler_name << ", " << domA.getID() << ", " << domB.getID()
             << ", " << Dim);

  return coupler;
}
/* -------------------------------------------------------------------------- */

void CouplerManager::init() { LM_TOIMPLEMENT; }

/* -------------------------------------------------------------------------- */

/* LMDESC CouplerManager
   This is the generic class which manages the collection of
   couplers instantiated for a simulation.
*/

void CouplerManager::declareParams() { LM_TOIMPLEMENT; }

/* -------------------------------------------------------------------------- */
CouplerManager &CouplerManager::getManager() {
  if (!IDManager<CouplingInterface>::static_pointer)
    IDManager<CouplingInterface>::static_pointer.reset(new CouplerManager());
  return dynamic_cast<CouplerManager &>(
      *IDManager<CouplingInterface>::static_pointer);
}

/* -------------------------------------------------------------------------- */

template <>
std::unique_ptr<IDManager<CouplingInterface>>
    IDManager<CouplingInterface>::static_pointer = nullptr;
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
