/**
 * @file   shape_matrix.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Jul 11 15:47:44 2014
 *
 * @brief  Data structure for the storage of shape function values on
 * atomic/point positions
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_SHAPE_MATRIX_HH__
#define __LIBMULTISCALE_SHAPE_MATRIX_HH__
/* -------------------------------------------------------------------------- */
#include "container_array.hh"
/* ------------------------------------------------------------------------ */
#include <Eigen/Sparse>
/* ------------------------------------------------------------------------ */

__BEGIN_LIBMULTISCALE__

/* ------------------------------------------------------------------------ */

/**
 Class ShapeMatrix

 This class is actually a sparse matrix used to store the
 shape functions of several mesh nodes evaluated on
 specific locations.

 The structure follows:

 $m_{Ii} = \varphi_I(X_i)$

 where $I$ is a node index, and $i$ an atom index.

 */

class ShapeMatrix
    : public Eigen::SparseMatrix<libmultiscale::Real, Eigen::RowMajor> {
public:
  using Eigen::SparseMatrix<libmultiscale::Real, Eigen::RowMajor>::SparseMatrix;
  ~ShapeMatrix();

  template <typename Cont> inline void removeAtoms(Cont &at_index_list);
};

/* ------------------------------------------------------------------------- */
inline ShapeMatrix::~ShapeMatrix() {}
/* ------------------------------------------------------------------------- */
template <typename Cont>
inline void ShapeMatrix::removeAtoms(Cont &at_indexes) {

  using Matrix = Eigen::SparseMatrix<libmultiscale::Real, Eigen::RowMajor>;

  std::set<UInt> at_indexes_remove(at_indexes.begin(), at_indexes.end());
  Matrix new_shape_matrix;
  UInt new_cols = this->cols() - at_indexes_remove.size();
  new_shape_matrix.resize(this->rows(), new_cols);

  std::vector<UInt> new_j(this->cols());
  UInt cpt = 0;
  for (UInt j = 0; j < this->cols(); ++j) {
    new_j[j] = -1;
    if (not at_indexes_remove.count(j)) {
      new_j[j] = cpt;
      ++cpt;
    }
  }

  for (int k = 0; k < this->outerSize(); ++k)
    for (Matrix::InnerIterator it(*this, k); it; ++it) {
      it.value();
      it.row();   // row index
      it.col();   // col index (here it is equal to k)
      it.index(); // inner index, here it is equal to it.row()
      new_shape_matrix.insert(it.row(), new_j[it.col()]) = it.value();
    }

  this->operator=(new_shape_matrix);
}
/* ------------------------------------------------------------------------ */
__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_SHAPE_MATRIX_HH__ */
