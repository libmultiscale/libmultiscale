/**
 * @file   duo_distributed_vector.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  Migration safe representation of array of references
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __DUO_DISTRIBUTED_VECTOR_H__
#define __DUO_DISTRIBUTED_VECTOR_H__
/* -------------------------------------------------------------------------- */
#include <list>
/* -------------------------------------------------------------------------- */
#include "comm_group.hh"
#include "container_array.hh"
#include "ref_subset.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

/** Class duodistributedvector allows redistribution of vectors,
    with possiblity to adapt the migration procedures (creating holes
    in the commuincation scheme) and eventually unfragmenting it */

class DuoDistributedVector : public AttachedObject {
public:
  using MapUIntToUIntList = std::map<UInt, std::vector<UInt>>;
  using MapUIntToUIntPairList =
      std::map<UInt, std::vector<std::pair<UInt, UInt>>>;
  using MapUIntToUInt = std::map<UInt, UInt>;
  using PairUIntList = std::vector<std::pair<UInt, UInt>>;

  ~DuoDistributedVector();
  DuoDistributedVector(UInt lsize, UInt tsize, const std::string &my_name);

  //! sets duo proc of an index (return false if index is already associated)
  inline bool setDuoProc(UInt index, UInt duo_proc);
  //! remove a com index : return the previously associated duo proc
  inline UInt removeIndex(UInt index);
  //! compress the indexes, by removing the unassociated/unmatched enties
  inline void compress();
  //! update both sides of the migrated operations
  void synchronizeMigration(CommGroup &groupAtomic, CommGroup &groupFE);
  //! function used to synchronize vectors (by summing them)
  void synchronizeVectorBySum(const std::string &name_vec,
                              ContainerArray<Real> &vec, CommGroup &group1,
                              CommGroup &group2);
  //! function used to redistribute a vector
  void distributeVector(const std::string &name_vec, ContainerArray<Real> &vec,
                        CommGroup &group1, CommGroup &group2);
  //! print to file the current redistribution scheme (with positions info)
  void print(const std::string &prefix);
  //! print to screen summary of com scheme
  void printSummary();

  inline void packData(UInt index, LMPackBuffer &buffer,
                       bool verbose = false) override;
  inline void unpackData(UInt index, LMPackBuffer &buffer,
                         bool verbose = false) override;
  inline void moveAttachedValues(UInt i_src, UInt i_dest) override;
  inline void resize(UInt sz) override;

  LMID getID() const override { return id; };

private:
  //! clear the duo (for reuse)
  void clear();
  //! set the size of the duo vector
  void setSize(UInt lsize, UInt tsize);
  //! find the real index given proc and com_index
  inline UInt findRealIndex(UInt com_index, UInt duo_proc);
  //! add an index for the communication to a given proc
  inline void addIndex(UInt index, UInt proc);
  //! update the communication list for the sent indexes
  void updateSent();
  //! update the communication list for the recv indexes
  void updateRecv();

private:
  ContainerArray<UInt> duo_proc_vector;
  AttachedVector<UInt> _duo_proc_vector;

  //! map index to communication indexes
  // MapUIntToUInt duo_index_map;
  //! map index to duo/fem proc
  // MapUIntToUInt duo_proc_per_index;
  // //! map duo/fem proc to communication indexes
  // MapUIntToUIntList indexes_per_duo_proc;
  // //! map duo/fem proc to sent indexes
  // MapUIntToUIntList sent_per_duo_proc;
  // // //! map duo/fem proc to new processor owners
  // // MapUIntToUIntList sent_procs;
  // //! map duo/fem proc to received indexes
  // MapUIntToUIntList received_per_duo_proc;
  //! total size of vectors that can be redistributed
  // UInt totalsize;
  //! actual local size of vecotr that can be redistributed
  // UInt localsize;
  //! flag used to say that migration have occured
  // bool something_changed;
  //! general id
  LMID id;
};

/* -------------------------------------------------------------------------- */
inline bool DuoDistributedVector::setDuoProc(UInt index, UInt proc) {
  if (duo_proc_vector(index, 0) != UInt(-1)) {
    DUMP("index " << index << " was already associated with duo proc "
                  << duo_proc_vector[index],
         DBG_INFO);
    return false;
  }
  addIndex(index, proc);
  return true;
}

/* -------------------------------------------------------------------------- */

inline UInt DuoDistributedVector::findRealIndex(UInt // com_index
                                                ,
                                                UInt // duo_proc
) {
#warning "to review"
  LM_TOIMPLEMENT;
  // LM_ASSERT(indexes_per_duo_proc.count(duo_proc),
  //           "processor " << duo_proc
  //                        << " not part of redistribution scheme: abort");
  // LM_ASSERT(com_index < indexes_per_duo_proc[duo_proc].size(),
  //           "invalid com_index: abort "
  //               << com_index << " " <<
  //               indexes_per_duo_proc[duo_proc].size());
  // return indexes_per_duo_proc[duo_proc][com_index];
}

/* -------------------------------------------------------------------------- */

inline UInt DuoDistributedVector::removeIndex(UInt index) {
  auto old_proc = duo_proc_vector(index, 0);
  duo_proc_vector(index, 0) = UINT_MAX;
  return old_proc;
}

/* -------------------------------------------------------------------------- */

inline void DuoDistributedVector::compress() {
  ContainerArray<UInt> compressed_duo_proc_vector(0, 2);

  for (auto &&[index, duo] : enumerate(duo_proc_vector.rowwise())) {
    auto proc = duo[0];
    if (proc == UINT_MAX)
      continue;
    compressed_duo_proc_vector.push_back(Vector<2, UInt>{proc, lm_my_proc_id});
  }
  this->duo_proc_vector = compressed_duo_proc_vector;
}

/* -------------------------------------------------------------------------- */

inline void DuoDistributedVector::addIndex(UInt index, UInt duo_proc) {
  LM_ASSERT(duo_proc_vector(index, 0) == UInt(-1),
            "index " << index << " already present in communication array"
                     << duo_proc_vector.row(index));

  duo_proc_vector(index, 0) = duo_proc;
  duo_proc_vector(index, 1) = lm_my_proc_id;

  DUMP("add index " << index << " from proc " << duo_proc << " duo is "
                    << this->getID(),
       DBG_DETAIL);
}
/* -------------------------------------------------------------------------- */

inline void DuoDistributedVector::packData(UInt index, LMPackBuffer &buffer,
                                           bool) {

  DUMP(this->getID() << ": packing index for duo " << index, DBG_MESSAGE);
  _duo_proc_vector.packData(index, buffer);

  // // LM_ASSERT(duo_index_map.count(index), "not found index : abort");
  // LM_ASSERT(duo_proc_per_index.count(index), "not found index : abort");
  // UInt fem_proc = duo_proc_per_index[index];
  // buffer << fem_proc;
  // buffer << lm_my_proc_id;
  // sent_per_duo_proc[fem_proc].push_back(index);
  // // sent_procs[fem_proc].push_back(proc);
  // DUMP("register index " << index << " to sent_list"
  //                        << " from duo/fem proc " << fem_proc << " duo is "
  //                        << this->getID(),
  //      DBG_DETAIL);
}
/* -------------------------------------------------------------------------- */
inline void DuoDistributedVector::unpackData(UInt index, LMPackBuffer &buffer,
                                             bool) {

  DUMP(this->getID() << ": unpacking index for duo " << index, DBG_MESSAGE);
  _duo_proc_vector.unpackData(index, buffer);

  // UInt fem_proc, old_proc;
  // buffer >> fem_proc;
  // buffer >> old_proc;
  // received_per_duo_proc[fem_proc].push_back(index);

  // DUMP("register index " << index << " to receive_list"
  //                        << " from proc " << fem_proc << " giving old owner
  //                        as "
  //                        << old_proc << " duo is " << getID(),
  //      DBG_DETAIL);
}
/* -------------------------------------------------------------------------- */
inline void DuoDistributedVector::moveAttachedValues(UInt i_src, UInt i_dest) {

  AttachedVector a_vec = (duo_proc_vector);
  a_vec.moveAttachedValues(i_src, i_dest);

  // UInt duo_proc = duo_proc_per_index[i_src];
  // // UInt com_index = duo_index_map[i_src];
  // // duo_index_map.erase(i_src);
  // duo_proc_per_index.erase(i_src);

  // LM_ASSERT(!duo_proc_per_index.count(i_dest), "inconstistency");
  // // LM_ASSERT(!duo_index_map.count(i_dest), "inconstistency");

  // DUMP("moving index " << i_src << " to " << i_dest << " for proc " <<
  // duo_proc
  //                      << " duo is " << this->getID(),
  //      DBG_DETAIL);

  // auto &indexes = indexes_per_duo_proc[duo_proc];
  // // LM_ASSERT(indexes[com_index] == i_src, "inconstistency");
  // coms[com_index] = i_dest;
  // duo_index_map[i_dest] = com_index;
  // duo_proc_map[i_dest] = duo_proc;

  // // std::pair<UInt, UInt> p(i_src, i_dest);
  // // moved.push_back(p);
}

/* -------------------------------------------------------------------------- */

inline void DuoDistributedVector::resize(UInt sz) {
  duo_proc_vector.resize(sz);
}

/* -------------------------------------------------------------------------- */

inline std::shared_ptr<AttachedObject>
make_attached(std::shared_ptr<DuoDistributedVector> &obj) {
  return obj;
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif /* __DUO_DISTRIBUTED_VECTOR_H__ */
