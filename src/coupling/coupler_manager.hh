/**
 * @file   coupler_manager.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jan 15 17:00:43 2014
 *
 * @brief  Manager of coupling components
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COUPLER_MANAGER_HH__
#define __LIBMULTISCALE_COUPLER_MANAGER_HH__
/* -------------------------------------------------------------------------- */
#include "coupling_interface.hh"
#include "domain_interface.hh"
#include "factory_multiscale.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class CouplerManager : public CouplingInterface,
                       public IDManager<CouplingInterface> {

private:
  CouplerManager();

public:
  virtual ~CouplerManager();
  static CouplerManager &getManager();
  //! function which parses the line
  ParseResult parseCouplingLine(std::stringstream &line, UInt Dim);
  void init() override;
  void coupling(CouplingStage stage) override;

  //! function that parses single keywords
  void declareParams() override;

private:
  //! creates the coupler based on couplerType and domain pointers
  std::shared_ptr<CouplingInterface> build(UInt couplerType,
                                           const std::string &coupler_name,
                                           DomainInterface &domA,
                                           DomainInterface &domB, UInt Dim);

  // private:
  //   std::map<std::string, std::shared_ptr<CouplingInterface>> &couplers;
};
/* ------------------------------------------------------------------------ */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_COUPLER_MANAGER_HH__ */
