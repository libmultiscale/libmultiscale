/**
 * @file   kobayashi.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Jul 11 15:47:44 2014
 *
 * @brief  Kobayashi's bridging method
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#ifndef __LIBMULTISCALE_KOBAYASHI_HH__
#define __LIBMULTISCALE_KOBAYASHI_HH__
/* -------------------------------------------------------------------------- */
#include "arlequin_template.hh"
#include "lib_continuum.hh"
#include "lib_md.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class Kobayashi : public ArlequinTemplate {

public:
  DECLARE_COUPLER(Kobayashi, domMD, domCONTINUUM);

  template <typename DomainA, typename DomainC>
  void init(DomainA &domA, DomainC &domC);
  template <typename DomainA, typename DomainC>
  void coupling(CouplingStage stage, DomainA &domA, DomainC &domC);

protected:
  void buildCGMatrix();
  void correctContinuum(FieldType field_type);
  void correctAtoms(FieldType field_type);

  ContainerArray<Real> &buildLeastSquareField(FieldType type);

  Eigen::SparseMatrix<Real> A;
  Eigen::SparseLU<Eigen::SparseMatrix<Real>> solver;

  //! quality factor
  Real quality;
};
__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_KOBAYASHI_HH__ */
