/**
 * @file   quasi_continuum.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Tue Sep 09 15:15:46 2014
 *
 * @brief  Quasicontinuum point to point coupling
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the terms  of the  GNU Lesser  General Public  License as  published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public
 * License  for more details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
/* ./coupling/quasi_continuum.hh
**********************************
Copyright INRIA and CEA

The LibMultiScale is a C++ parallel framework for the multiscale
coupling methods dedicated to material simulations. This framework
provides an API which makes it possible to program coupled simulations
and integration of already existing codes.

This Project is done in a collaboration between INRIA Futurs Bordeaux
within ScAlApplix team and CEA/DPTA Ile de France.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
***********************************/

#ifndef __LIBMULTISCALE_QUASI_CONTINUUM_HH__
#define __LIBMULTISCALE_QUASI_CONTINUUM_HH__

#include "coupling_atomic_continuum.hh"
#include "point_association.hh"
#include "string"

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
class QuasiContinuum : public CouplingAtomicContinuum {
public:
  DECLARE_COUPLER(QuasiContinuum, domMD, domCONTINUUM);

  template <typename DomainA, typename DomainC>
  void init(DomainA &domA, DomainC &domC);

  template <typename DomainA, typename DomainC>
  void coupling(CouplingStage stage, DomainA &domA, DomainC &domC);

  /// one coupling function per coupling direction: md=>fem
  template <FieldType ftype> void enforceInterface();
  /// one coupling function per coupling direction: fem=>md
  template <FieldType sync_type> void enforcePad();
  /// set all pad forces to zero to avoid annoying effects in postprocessing
  void clearPadForces();
  void clearInterfaceForces();
  virtual void clearAll();

  void updateForMigration();

private:
  void setBoundary();
  //! associates interface atoms to fem nodes
  PointAssociation interface_association;
  //! path to interface atom file
  std::string interface_path;
  //! associates fem nodes to pad atoms
  PointAssociation pad_association;
  //! path to pad atom file
  std::string pad_path;
  //! weighting python script
  std::string weight_path;

  //! the frequency at which to reset the postions to match
  UInt reset_position_freq;

  //! flag to set the arlequin style
  bool arlequin_flag;

  //! coupler name as declared in config file
  std::string name;
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_QUASI_CONTINUUM_HH__ */
