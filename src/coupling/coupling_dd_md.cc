#include "coupling_dd_md.hh"

/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
CouplingDDMD::CouplingDDMD(const std::string &name)
    : LMObject(name), CouplingInterface(name),
      comm_group_dd(this->comm_group_A), comm_group_atomic(this->comm_group_B) {

  is_in_dd = comm_group_dd.amIinGroup();
  is_in_atomic = comm_group_atomic.amIinGroup();
}

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
