/**
 * @file   bridging_atomic_continuum.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Thu Oct 03 16:50:49 2013
 *
 * @brief  Bridging object between atomistic and finite elements
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#ifndef BRIDGING_ATOMIC_CONTINUUM_H
#define BRIDGING_ATOMIC_CONTINUUM_H
/* -------------------------------------------------------------------------- */
#include "bridging.hh"
#include "lib_continuum.hh"
#include "lib_md.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/** TODO: review doc */

class BridgingAtomicContinuum : public Bridging {

  //****************************************************************
  // non templated functions
  //****************************************************************

public:
  BridgingAtomicContinuum(const std::string &name);
  virtual ~BridgingAtomicContinuum();

  //! sync migrated data with atoms
  void updateForMigration() override;

private:
  //! average atomic velocities per elements
  // virtual void averageDOFsOnElements(ContainerArray<Real> &data_mesh,
  //                                    FieldType field);

  //! checks migration did not affect positions
  void checkPositionCoherency();
  //! Perform a leastsquare on a set of atoms returning and nodal array
  void leastSquareAtomicDOFs(ContainerArray<Real> &data_mesh, FieldType field);
  //! Perform a leastsquare on a set of atoms returning and nodal array
  void leastSquareAtomicData(ContainerArray<Real> &data_mesh,
                             ContainerArray<Real> &data_atom);
  //! build atomic DOFs vector noise (u-sum_I \varphi_I u_I)
  void buildAtomicDOFsNoiseVector(std::vector<Real> &v_out, FieldType field);

  //****************************************************************
  // templated functions
  //****************************************************************

public:
  DECORATE_FUNCTION_DISPATCH(init, MD, CONTINUUM)
  //! generic initializing function
  template <typename DomainA, typename DomainC>
  void init(DomainA &contA, DomainC &contC);

protected:
  DECORATE_FUNCTION_DISPATCH(checkPositionCoherency, MD, CONTINUUM)
  //! checks coherency of DOFs redistribution based on atomic positions
  template <typename DomainA, typename DomainC>
  void checkPositionCoherency(DomainA &domA, DomainC &domC);
  //! search a given atom by coordinates
  template <typename Cont, typename Vec>
  void searchAtomInLocalPool(Cont &container, Vec &X);

protected:
};

__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

#endif // BRIDGING
