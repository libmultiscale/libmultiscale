/**
 * @file   xiao.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jan 15 17:00:43 2014
 *
 * @brief  Bridging Domain/Arlequin method
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_XIAO_HH__
#define __LIBMULTISCALE_XIAO_HH__
/* -------------------------------------------------------------------------- */
#include "arlequin_template.hh"
#include "bridging_atomic_continuum.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class Xiao : public virtual ArlequinTemplate {

public:
  DECLARE_COUPLER(Xiao, domMD, domCONTINUUM);

  template <typename DomainA, typename DomainC>
  void init(DomainA &domA, DomainC &domC);

  //! build constraint matrix
  virtual void buildConstraintMatrix();
  //! Build the right hand side contribution from FE
  virtual void buildRHS(FieldType field_type);
  //! apply correction to local FE Dofs
  virtual void applyCorrection(FieldType field_type);
  //! solve the constraint system
  virtual void solveConstraint();
  //! Substract initial force
  DECORATE_FUNCTION_DISPATCH(substractInitialForce, subCONTINUUM)
  template <typename ContC> void substractInitialForce(ContC &meshList);
  
protected:
  //! array to store matrix
  ContainerArray<Real> A;
  //! array to store rhs/multipliers data
  ContainerArray<Real> rhs;
  //! Array of the initial forces in the bridging
  ContainerArray<double> force_initial_bridging;

  bool centroid_flag;
  bool substract_initial_force;
  Real quality;

  //! flag to know wether we have to check for coherency
  bool check_coherency;
  
};
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_XIAO_HH__ */
