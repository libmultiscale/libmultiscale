#include "coupling_dd_continuum.hh"

/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
CouplingDDContinuum::CouplingDDContinuum(const std::string &name)
    : LMObject(name), CouplingInterface(name) {}

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
