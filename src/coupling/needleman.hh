/**
 * @file   needleman.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Wed Jan 15 17:00:43 2014
 *
 * @brief  Needleman style discrete dislocation coupler between DD and FEM
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the terms  of the  GNU Lesser  General Public  License as  published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public
 * License  for more details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef NEEDLEMAN_H
#define NEEDLEMAN_H
/* -------------------------------------------------------------------------- */
//#define CONTINUFLAG 1
//#define DISLOFLAG 2
/* -------------------------------------------------------------------------- */
#include "bridging.hh"
#include "coupling_dd_continuum.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class Needleman : public CouplingDDContinuum {

public:
  DECLARE_COUPLER(Needleman, domDD, domCONTINUUM);

  virtual void clearAll() {}

  template <typename DomainDD, typename DomainC>
  void coupling(CouplingStage stage, DomainDD &domDD, DomainC &domC);
  template <typename DomainDD, typename DomainC>
  void init(DomainDD &domDD, DomainC &domC);

protected:
  template <typename ContC> void applyStressesOnNodes(ContC &meshList);
  void applyStressesOnSources();
  void recomputeBridgingInformationOnNodes();
  template <typename DomC, typename DomA>
  void applyBoundaryConditionToFEM(DomC &domC, DomA &domA);

  //! The bridging zone
  Bridging bridging_nodes;
  // Bridging<ContainerDDSources,ContainerMesh,Dim> bridging_sources;
};
/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
#endif // NEEDLEMAN
