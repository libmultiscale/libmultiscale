/**
 * @file   bridging_atomic_continuum.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  Bridging object between atomistic and finite elements
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
//#define TIMER
/* -------------------------------------------------------------------------- */
#include "bridging_atomic_continuum.hh"
#include "compute_extract.hh"
#include "lm_common.hh"
#include "reference_manager.hh"
#include "trace_atom.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

BridgingAtomicContinuum::BridgingAtomicContinuum(const std::string &name)
    : LMObject(name), Bridging(name) {}
/* -------------------------------------------------------------------------- */

BridgingAtomicContinuum::~BridgingAtomicContinuum() {}

/* -------------------------------------------------------------------------- */

void BridgingAtomicContinuum::updateForMigration() {

  Bridging::updateForMigration();
  if (this->check_coherency) {
    checkPositionCoherency();
  }
}
/* -------------------------------------------------------------------------- */
INSTANCIATE_DISPATCH(BridgingAtomicContinuum::init)
template <typename ContA, typename ContC>
void BridgingAtomicContinuum::init(ContA &contA, ContC &contC) {

  auto all_group = Communicator::getCommunicator().getGroup("all");

  Bridging::init(contA, contC);

  if (in_group_A()) {
    auto &pointList = this->pointList.get<typename ContA::ContainerSubset>();
    contA.addSubSet(pointList);
    DUMP(this->getID() << " : attach duo vector", DBG_MESSAGE);
    pointList.attachObject(this->getDuoVector());
    DUMP(this->getID() << " : attaching Parent::duo_vector", DBG_INFO);
  }
  all_group.barrier();

  this->checkPositionCoherency();
}
/* -------------------------------------------------------------------------- */

// void BridgingAtomicContinuum::averageDOFsOnElements(
//     ContainerArray<Real> &data_mesh, FieldType field) {

//   LM_FATAL("to adapt");

//   if (comm_group_atomic == comm_group_continuum) {

//     ComputeExtract extracted_field("ComputeExtract:" + this->getID());
//     extracted_field.setParam("FIELD", field);
//     extracted_field.buildManual(*this->pointList);
//     this->averageOnElements<dispatch>(extracted_field.getArray(), data_mesh,
//                                       meshList);
//     return;
//   }

//   ComputeExtract extracted_field("ComputeExtract:" + this->getID());
//   extracted_field.setParam("FIELD", field);
//   if (this->is_in_atomic)
//     extracted_field.buildManual(*this->pointList);

//   LM_TOIMPLEMENT;
//   // extracted_field.resize(Parent::local_points * Dim);

//   /* do communication */
//   this->communicateBufferAtom2Continuum(extracted_field.getArray());

//   if (this->is_in_continuum)
//     this->averageOnElements<dispatch>(extracted_field.getArray(), data_mesh,
//                                       meshList);
// }
/* -------------------------------------------------------------------------- */

void BridgingAtomicContinuum::leastSquareAtomicData(
    ContainerArray<Real> &data_mesh, ContainerArray<Real> &data_atom) {

  if (this->comm_group_atomic() == this->comm_group_continuum()) {
    LM_TOIMPLEMENT;
    // this->solveLeastSquare<dispatch>(data_mesh, data_atom,
    //                                  this->evalOutput("meshList"));
    return;
  }

  /* do communication */
  this->commPoint2Continuum(data_atom);

  if (this->is_in_continuum()) {
    LM_TOIMPLEMENT;
    // this->solveLeastSquare<dispatch>(data_mesh, data_atom,
    //                                  this->getOutput("meshList"));
  }
}

/* -------------------------------------------------------------------------- */

void BridgingAtomicContinuum::leastSquareAtomicDOFs(
    ContainerArray<Real> &data_mesh, FieldType field) {

  LM_FATAL("to adapt");

  if (this->comm_group_atomic() == this->comm_group_continuum()) {
    ComputeExtract extracted_field("ComputeExtract:" + this->getID());
    extracted_field.setParam("FIELD", field);
    extracted_field.compute(this->pointList);
    LM_TOIMPLEMENT;
    // this->solveLeastSquare<dispatch>(
    //     data_mesh, extracted_field.evalArrayOutput(), this->meshList);
    return;
  }

  ComputeExtract extracted_field("ComputeExtract:" + this->getID());
  extracted_field.setParam("FIELD", field);
  if (this->is_in_atomic())
    extracted_field.compute(this->pointList);

  LM_TOIMPLEMENT;
  // extracted_field.resize(Parent::local_points * Dim);

  this->leastSquareAtomicData(data_mesh, extracted_field.evalArrayOutput());
}

/* -------------------------------------------------------------------------- */

void BridgingAtomicContinuum::buildAtomicDOFsNoiseVector(std::vector<Real> &,
                                                         FieldType field) {

  ComputeExtract extracted_field("ComputeExtract:" + this->getID());
  extracted_field.setParam("FIELD", field);
  if (this->is_in_atomic())
    extracted_field.compute(this->pointList);

  if (this->is_in_continuum()) {

    LM_TOIMPLEMENT;
    // auto &field = extracted_field.evalArrayOutput();
    // this->interpolatePointData(field, ??);
    // for (UInt i = 0; i < this->local_points * spatial_dimension; ++i)
    // LM_TOIMPLEMENT;
    // extracted_field[i] *= -1;
  }
  /* do communication */
  this->synchSumBuffer(extracted_field.evalArrayOutput());
}

/* -------------------------------------------------------------------------- */

template <typename ContA, typename ContC>
void BridgingAtomicContinuum::checkPositionCoherency(ContA &pointList,
                                                     ContC &meshList) {

  ContainerArray<Real> interpolated_x0;
  this->projectAtomicFieldOnMesh(_position0, interpolated_x0);

  constexpr UInt Dim = ContA::ContainerSubset::Dim;
  bool error_flag = false;

  if (this->is_in_atomic()) {

    UInt i = 0;
    for (auto &&[pos, at] : zip(interpolated_x0.rowwise(), pointList)) {
      UInt local_flag = 0;
      Vector<Dim> at_pos = at.position0();
      Real dist = (Vector<Dim>(pos) - at_pos).norm();
      if (dist > 1e-5) {
        DUMP("mismatch atom interpolation: " << i << " diff is " << dist,
             DBG_MESSAGE);
        DUMP("ref atom is " << at << " interpolated position is "
                            << Vector<Dim>(pos),
             DBG_MESSAGE);
        DUMP("associated_element_index: " << this->pointToElement[i],
             DBG_MESSAGE);
        auto &&elem = meshList.getContainerElems()[this->pointToElement[i]];
        DUMP("associated_element: " << elem, DBG_MESSAGE);

        for (UInt i = 0; i < elem.nNodes(); ++i) {
          DUMP("associated_node: " << i << " " << elem.getNode(i), DBG_MESSAGE);
        }
        DUMP("shapes: " << this->smatrix.col(i), DBG_MESSAGE);
        searchAtomInLocalPool(pointList, pos);
        error_flag = 1;
      }
      ++i;
    }

    if (error_flag) {
      //     this->getDuoVector().print("buggy-redistrib");
      LM_FATAL("abort due to migration problem");
    }
  }
  DUMP("position coherency OK", DBG_INFO);
}

/* -------------------------------------------------------------------------- */

template <typename Cont, typename Vec>
void BridgingAtomicContinuum::searchAtomInLocalPool(Cont &container, Vec &x) {

  UInt found_index = container.size();

  for (auto &&[i, at] : enumerate(container)) {
    // #ifdef TRACE_ATOM
    //     auto pos = at.position0();
    // #endif
    IF_COORD_EQUALS(pos, x) {
      DUMP("actually searched atom is at ref " << at << " bridge index " << i,
           DBG_MESSAGE);
      found_index = i;
      break;
    }
  }
  if (found_index == container.size())
    DUMP("atom " << x << " "
                 << " not found locally : probably has migrated"
                 << " or is simply lost !"
                 << " NOT GOOD",
         DBG_MESSAGE);

  //   i = 0;
  //   for (auto &&at : domA.getContainer()) {
  // #ifdef TRACE_ATOM
  //     auto pos = at.position0();
  // #endif
  //     IF_COORD_EQUALS(pos, x) {
  //       DUMP("actually searched atom is at ref " << at << " bridge index
  //       "
  //       << i,
  //            DBG_MESSAGE);
  //       break;
  //     }
  //     ++i;
  //   }
  //   if (i == domA.getContainer().size())
  //     DUMP("atom not found locally : probably has migrated"
  //              << "or is simply lost !"
  //              << " NOT GOOD",
  //          DBG_MESSAGE);
}
/* --------------------------------------------------------------------------
 */

void BridgingAtomicContinuum::checkPositionCoherency() {
  if (is_in_continuum() and is_in_atomic())
    this->checkPositionCoherency<dispatch>(this->pointList, this->meshList);
}

__END_LIBMULTISCALE__
