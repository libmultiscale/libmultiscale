/**
 * @file   needleman.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Wed Jan 15 17:00:43 2014
 *
 * @brief  Needleman style discrete dislocation coupler between DD and FEM
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the terms  of the  GNU Lesser  General Public  License as  published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public
 * License  for more details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "needleman.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

Needleman::Needleman(const std::string &name)
    : LMObject(name), CouplingDDContinuum(name),
      bridging_nodes("bridging:" + name)
//,
//    bridging_sources(this->domA.getContainerNodes(),this->domC.getContainer())
{}
/* -------------------------------------------------------------------------- */
Needleman::~Needleman() { clearAll(); }
/* -------------------------------------------------------------------------- */

template <typename DomainDD, typename DomainC>
void Needleman::init(DomainDD &domDD, DomainC &domC) {

  // initialize the bridging_zone object
  bridging_nodes.init(domDD, domC);

  // //forward name
  // sprintf(temp,"%s-bridging",Parent::name.c_str());
  // bridging_sources.setName(temp);
  // // initialize the bridging_zone object
  // bridging_sources.init();
}
/* -------------------------------------------------------------------------- */

template <typename DomainDD, typename DomainC>
void Needleman::coupling(CouplingStage stage, DomainDD &domDD, DomainC &domC) {
  if (stage != COUPLING_STEP3)
    return;

  // since dislocations move I must recompute their positions and associations
  // with elements
  recomputeBridgingInformationOnNodes();
  LM_TOIMPLEMENT;
  // applyStressesOnNodes(domDD, domC);
  applyStressesOnSources();

  // apply boundary conditions to the FEM zone from image stresses of the
  // dislocation stress fields
  applyBoundaryConditionToFEM(domC, domDD);
}

/* -------------------------------------------------------------------------- */

void Needleman::recomputeBridgingInformationOnNodes() {

  LM_FATAL("to adapt");
  // construct sublist of coupled nodes
  // bridging_nodes.buildPointList();
  // // build positions of atom vector
  // bridging_nodes.buildPositions();
  // // build shape matrix
  // bridging_nodes.buildShapeMatrix(bridging_nodes->pointList->nbElem());
  // //filter unmatched atomic DOFs
  // bridging_nodes.filterPointListForUnmatched();
  // //filter assoc vector
  // bridging_nodes.filterAssoc();
}

/* -------------------------------------------------------------------------- */

template <typename ContC>
void Needleman::applyStressesOnNodes(ContC &meshList) {
  LM_TOIMPLEMENT;
  // assign the stress to nodes
  // auto &elems = meshList.getContainerElems();

  // for (auto &&[nd, assoc] :
  //      zip(bridging_nodes.pointList, bridging_nodes.getAssoc())) {
  //   // assoc return the element index in which point i resides
  //   // elementList is a container of elements
  //   auto &&el = elems.get(assoc);
  //   // conversion from 2D to 3D (temporary ?)
  //   nd.stress(0) = el.stress(0);
  //   nd.stress(1) = el.stress(1);
  //   nd.stress(2) = 0;
  //   nd.stress(3) = el.stress(2);
  //   nd.stress(4) = 0;
  //   nd.stress(5) = 0;
  // }
}

/* -------------------------------------------------------------------------- */

void Needleman::applyStressesOnSources() {
  // // assign the stress to sources
  // IteratorDDSourcesSubset itsources =
  // bridging_sources->pointList->getIterator(); RefDDSource nd =
  // itsources.getFirst(); UInt i = 0; RefElem el; while (!itsources.end()){
  //   // assoc return the element index in which poUInt i resides
  //   // elementList is a conatiner of elements
  //   el = bridging_sources->elementList.get(bridging_sources->getAssoc(i));
  //   // conversion from 2D to 3D (temporary ?)
  //   nd.stress(0) = el.stress(0);
  //   nd.stress(1) = el.stress(1);
  //   nd.stress(2) = 0;
  //   nd.stress(3) = el.stress(2);
  //   nd.stress(4) = 0;
  //   nd.stress(5) = 0;
  //   ++i;
  //   nd = itsources.getNext();
  // }
}

/* -------------------------------------------------------------------------- */

template <typename DomC, typename DomA>
void Needleman::applyBoundaryConditionToFEM(DomC &domC,
                                            DomA &domA [[gnu::unused]]) {
  auto &contElem = domC.getContainerElems();
  LM_FATAL("what was that ?");
  // contElem.boundarycontainerSwitch(1);
  auto contNodes = domC.getContainerNodes();

  for (auto &&el [[gnu::unused]] : contElem) {
    std::vector<UInt> nodes;
    LM_TOIMPLEMENT;
    // el.globalIndexes(nodes);
    // Real nx, ny, nz;
    // el.normal(nx, ny, nz);
    // Real normals[3];
    // normals[0] = nx;
    // normals[1] = ny;
    // normals[2] = 0;
    UInt nnodes = nodes.size();
    Real quadrature_point[2] = {0., 0.};
    LM_TOIMPLEMENT;
    // Real h  = el.length();
    for (UInt n = 0; n < nnodes; ++n) {
      RefNode nd = contNodes.get(n);
      LM_TOIMPLEMENT;
      // quadrature_point[0] += nd.position0(0);
      // quadrature_point[1] += nd.position0(1);
    }
    quadrature_point[0] /= 2;
    quadrature_point[1] /= 2;
    LM_TOIMPLEMENT;
    // Real tractions[3];
    // domA.imageForce(quadrature_point, normals, tractions);
    for (UInt n = 0; n < nnodes; ++n) {
      RefNode nd = contNodes.get(n);

      LM_TOIMPLEMENT;
      // Real fx = nd.force()[0] * h;
      // Real fy = nd.force()[1] * h;
      LM_TOIMPLEMENT;
      // nd.setForce(0, fx + 0.5 * tractions[0]);
      // nd.setForce(0, fy + 0.5 * tractions[1]);
    }
  }
  LM_FATAL("what was that");
  //  contElem.boundarycontainerSwitch(0);
}
/* -------------------------------------------------------------------------- */

/* LMDESC NEEDLEMAN
   Needleman style discrete dislocation coupler between DD and FEM
*/

/* LMEXAMPLE
   Will be added
 */

void Needleman::declareParams() {}

/* -------------------------------------------------------------------------- */
DECLARE_COUPLER_INIT_MAKE_CALL(Needleman, domDD, domCONTINUUM)
DECLARE_COUPLING_MAKE_CALL(Needleman, domDD, domCONTINUUM)
/* ------------------------------------------------------------------------ */

__END_LIBMULTISCALE__
