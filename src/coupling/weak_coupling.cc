/**
 * @file   weak_coupling.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jan 15 17:00:43 2014
 *
 * @brief  Bridging Domain/Weak coupling method
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#define TIMER
/* -------------------------------------------------------------------------- */
#include "weak_coupling.hh"
#include "lm_common.hh"
#include "stimulation_zero.hh"
#include <Eigen/Dense>
#include <iostream>
#include <sstream>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

WeakCoupling::WeakCoupling(const std::string &name)
    : LMObject(name), ArlequinTemplate(name), Xiao(name) {}

/* -------------------------------------------------------------------------- */

WeakCoupling::~WeakCoupling() {}

/* -------------------------------------------------------------------------- */

void WeakCoupling::coupling(CouplingStage stage) { Xiao::coupling(stage); }

/* -------------------------------------------------------------------------- */

template <typename DomainA, typename DomainC>
void WeakCoupling::init(DomainA &domA, DomainC &domC) {
  Xiao::init(domA, domC);
}

/* -------------------------------------------------------------------------- */

void WeakCoupling::buildConstraintMatrix() {

  if (this->is_in_continuum()) {
    auto &shp = this->bridging_zone.smatrix;

    Eigen::MatrixXd mass_node(shp.rows(), shp.rows());
    mass_node.setZero();

    for (int I = 0; I < mass_node.rows(); ++I) {
      mass_node(I, I) = 1. / lambdas_mesh[I];
    }

    Amatrix =
        shp * shp.transpose() * mass_node.matrix() * shp * shp.transpose();
  }
  if (this->is_in_atomic()) {
    if (not this->is_in_continuum())
      Amatrix = Array::Zero(lambdas_point.rows(), lambdas_point.cols());

    auto &shp = this->bridging_zone.smatrix;

    Eigen::MatrixXd mass_particle(shp.cols(), shp.cols());
    mass_particle.setZero();

    for (int i = 0; i < mass_particle.rows(); ++i) {
      mass_particle(i, i) = 1. / lambdas_point[i];
    }

    Amatrix += shp * mass_particle.matrix() * shp.transpose();
  }

  for (int I = 0; I < Amatrix.rows(); ++I) {
    double line = Amatrix.row(I).sum();
    if (line == 0) {
      Amatrix(I, I) = 1;
    }
  }

  this->facto = Amatrix.fullPivLu();

  // bridging_zone.synchronizeVectorBySum("A", this->Amatrix);
}

/* -------------------------------------------------------------------------- */

void WeakCoupling::buildRHS(FieldType field_type) {
  auto &shp = this->bridging_zone.smatrix;
  Xiao::buildRHS(field_type);
  this->rhsWeak = shp * rhs.matrix();
  //  bridging_zone.synchronizeVectorBySum("rhs", this->rhsWeak);
}

/* -------------------------------------------------------------------------- */

void WeakCoupling::solveConstraint() {
  lagrange_multiplier = facto.solve(this->rhsWeak);
}

/* -------------------------------------------------------------------------- */

void WeakCoupling::applyCorrection(FieldType field_type) {

  auto &shp = this->bridging_zone.smatrix;

  if (this->is_in_continuum()) {

    auto &correction = this->bridging_zone.buffer_for_nodes;

    correction = -shp * shp.transpose() * lagrange_multiplier.matrix();

    for (auto &&[corr, lbda] : zip(correction.rowwise(), this->lambdas_mesh)) {
      corr = corr / lbda;
    }
    this->bridging_zone.setPBCSlaveFromMaster(correction);
    this->bridging_zone.addMeshField(field_type, correction);
    correction.clear();
  }

  if (this->is_in_atomic()) {
    auto &correction = this->bridging_zone.buffer_for_points;
    correction = shp.transpose() * lagrange_multiplier.matrix();

    for (auto &&[cor, lbda] : zip(correction.rowwise(), lambdas_point)) {
      LM_ASSERT(lbda, "weight associated with atom is zero : abort");
      cor = cor / lbda;
    }

    this->bridging_zone.addPointField(field_type, correction);
    correction.clear();
  }
  lagrange_multiplier.setZero();
}

/* -------------------------------------------------------------------------- */

void WeakCoupling::declareParams() { Xiao::declareParams(); }

/* -------------------------------------------------------------------------- */

/* LMDESC WEAKCOUPLING

   TO DO
*/

/* LMHERITANCE xiao */

/* -------------------------------------------------------------------------- */

DECLARE_COUPLER_INIT_MAKE_CALL(WeakCoupling, domA, domC)
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
