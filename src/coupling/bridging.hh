/**
 * @file   bridging.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Jul 11 15:47:44 2014
 *
 * @brief  Bridging object between atomistic and finite elements
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_BRIDGING_HH__
#define __LIBMULTISCALE_BRIDGING_HH__
/* -------------------------------------------------------------------------- */
#include "container_array.hh"
#include "dof_association.hh"
#include "filter_geometry.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "shape_matrix.hh"
#include "spatial_grid_libmultiscale.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/** TODO : review doc */

class Bridging : public DofAssociation {

public:
  Bridging(const std::string &name);
  virtual ~Bridging();

  //! extract identical nodes from the pbc pair lists
  void setPBCPairs(std::vector<std::pair<UInt, UInt>> &pairs);
  //! attach a vector to bridging zone (sync with point migrations)
  void attachVector(ContainerArray<Real> &tab);
  //! fill an array with interpolated field on point positions
  void mesh2Point(const ContainerArray<Real> &data_node,
                  ContainerArray<Real> &data_points, bool apply_pbc = true);
  //! fill an array with field interpolated(on points) from mesh
  void mesh2Point(FieldType field_type, ContainerArray<Real> &data_node);
  //! set a point field from an array
  void setPointField(FieldType field, Real val, UInt dim);
  //! set a point field from an array
  void setPointField(FieldType field, ContainerArray<Real> &buffer);
  //! add an array to a point field
  void addPointField(FieldType field, ContainerArray<Real> &buffer);
  //! set a point field uniformly from a value
  void setPointField(FieldType field, Eigen::VectorXd val);
  //! set a mesh field from an array
  void setMeshField(FieldType field, ContainerArray<Real> &buffer);
  //! add a mesh field from an array
  void addMeshField(FieldType field, ContainerArray<Real> &buffer);
  //! set a mesh field uniformly from a value
  void setMeshField(FieldType field, Eigen::VectorXd val);

  //! TODO review
  void projectAtomicFieldOnMesh(FieldType field);
  //! TODO review
  void projectAtomicFieldOnMesh(FieldType field, ContainerArray<Real> &array);

protected:
  //! function to call to synch migrated data along with atoms
  virtual void updateForMigration();
  //! sends the buffer array of atomic DOFs: GROUP_FROM -> GROUP_DEST
  void commPoint2Continuum(ContainerArray<Real> &buf);
  //! internal function that synch the buffer array of atomic DOFs by sum
  void synchSumBuffer(ContainerArray<Real> &buf);

private:
  //! sends the buffer array of atomic DOFs: GROUP_DEST -> GROUP_FROM
  void commContinuum2Point(ContainerArray<Real> &buf);

  void declareParams();

  //! filter duplicate owners info from A processors
  void filterRejectedContinuumOwners(
      std::vector<std::vector<UInt>> &unassociated_points);

  //! unfragmenting an attached vector
  virtual void UnfragmentArray(std::vector<Real> &, UInt, const char *,
                               UInt = 1){};

  //! project atomic values on mesh interpolated solution
  void projectPointDataOnMesh(ContainerArray<Real> &point_data,
                              ContainerArray<Real> &continuum_field,
                              const std::string &message);

  //! project atomic values on mesh interpolated solution
  void projectPointDataOnMesh(FieldType field, ContainerArray<Real> &buffer);

  //! filter an array associated of detected particles
  void filterArray(ContainerArray<Real> &array);

public:
  DECORATE_FUNCTION_DISPATCH(init, MD, CONTINUUM)
  //! Init this object : select DOFs, associate them and compute shapematrix
  template <typename ContA, typename ContC>
  void init(ContA &contA, ContC &contC);

  DECORATE_FUNCTION_DISPATCH(buildLocalPBCPairs, CONTINUUM)
  //! extract identical nodes from the pbc pair lists
  template <typename ContC> void buildLocalPBCPairs(ContC &meshList);

  //! impose slave nodes to the master value
  template <typename Vec> void setPBCSlaveFromMaster(Vec &mat);

  DECORATE_FUNCTION_DISPATCH(buildPointList, MD)
  //! internal function that filter the interesting MD DOFs
  template <typename ContA> void buildPointList(ContA &contA);

  DECORATE_FUNCTION_DISPATCH(buildShapeMatrix, subCONTINUUM)
  //! compute the association vector and smatrix
  template <typename ContMesh>
  void buildShapeMatrix(ContMesh &unmatchedMeshList);

  DECORATE_FUNCTION_DISPATCH(filterPointListForUnmatched, subMD)
  //! filter an atomic container using unmatched points
  template <typename ContA> void filterPointListForUnmatched(ContA &pointList);

protected:
  //! internal function that filter the interesting FE DOFs
  template <typename ContC> void buildContinuumDOFsList(ContC &contC);

  DECORATE_FUNCTION_DISPATCH(filterContainerElems, std::vector<UInt>, CONTINUUM)
  //! filter a container (elems_rec or nodes_rec for instance) in order not to
  //! take into account unmatched elements/nodes from BuildShapeMatrix
  template <typename ContMesh>
  void filterContainerElems(std::vector<UInt> &nb_atome_par_element,
                            ContMesh &contMesh);

  //! flag if in group continuum
  bool is_in_continuum() { return comm_group_continuum().amIinGroup(); };
  //! flag if in group atomic
  bool is_in_atomic() { return comm_group_atomic().amIinGroup(); };

  //! group IF of the atomic side
  CommGroup comm_group_atomic() { return this->comm_group_A; };
  //! group ID of the continuum side
  CommGroup comm_group_continuum() { return this->comm_group_B; };

public:
  //! contains the atoms filtered by geometry
  FilterGeometry unmatchedPointList;
  //! contains the nodes&elements filtered by geometry
  FilterGeometry unmatchedMeshList;
  //! contains the atoms to be coupled
  OutputContainer &pointList;
  //! contains the nodes&elements to be coupled
  OutputContainer &meshList;
  //! shape matrix (containing interpolation values of the initial atomic sites)
  ShapeMatrix smatrix;
  //! buffer array to store point quantities
  ContainerArray<Real> buffer_for_points;
  //! buffer array to store node quantities
  ContainerArray<Real> buffer_for_nodes;

protected:
  //! association vector
  ContainerArray<UInt> pointToElement;
  //! Container of all elements
  OutputContainer &contMesh;
  //! grid used to search point <-> association
  OutputContainer &grid;

  //****************************************************************
  //  TO REVIEW
  //****************************************************************

  // node fusion info for PBC in mesh (temporary)
  std::vector<std::pair<UInt, UInt>> local_pbc_pairs;
  std::map<UInt, UInt> reverse_local_pbc_pairs;

  //! pbc pairs object
  std::vector<std::pair<UInt, UInt>> pbc_pairs;

  // DECORATE_FUNCTION_DISPATCH(averageOnElements, ContainerArray<Real>,
  //                            ContainerArray<Real>, CONTINUUM)
  // //! average point data per elements
  // template <typename ContC>
  // void averageOnElements(ContainerArray<Real> &data_point,
  //                        ContainerArray<Real> &data_mesh, ContC &meshList);

  //! cumulate values due to PBC conditions
  // virtual void cumulPBC(ContainerArray<Real> &data);

  //! attach a vector to the bridging zone (coherency will thus be maintained
  //! through atomic migrations)
  // void attachVector(ContainerArray<Real> &tab, const std::string &name);
};

__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
#include "bridging_inline_impl.hh"
/* -------------------------------------------------------------------------- */

#endif /* __LIBMULTISCALE_BRIDGING_HH__ */
