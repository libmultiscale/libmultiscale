/**
 * @file   coupling_atomic_continuum_python.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @brief  AtC coupler wrapper for python
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COUPLING_ATOMIC_CONTINUUM_PYTHON_HH__
#define __LIBMULTISCALE_COUPLING_ATOMIC_CONTINUUM_PYTHON_HH__
/* -------------------------------------------------------------------------- */
#include "coupling_atomic_continuum.hh"
#include "lib_continuum.hh"
#include "lib_md.hh"
/* -------------------------------------------------------------------------- */
#include <pybind11/operators.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
/* -------------------------------------------------------------------------- */
namespace py = pybind11;
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

class CouplingAtomicContinuumPython : public virtual CouplingAtomicContinuum {

public:
  DECLARE_COUPLER(CouplingAtomicContinuumPython, domMD, domCONTINUUM);

  void init(DomainInterface &domA, DomainInterface &domC);

  bool is_in_atomic() { return CouplingAtomicContinuum::is_in_atomic(); };
  bool is_in_continuum() { return CouplingAtomicContinuum::is_in_continuum(); };

protected:
};

/* LMDESC ATC_PYTHON

   This is an internal class, for the purpose of wiring with a python
   class allowing to implement a generic Atomic-to-continuum coupler in python.

 */

inline void CouplingAtomicContinuumPython::declareParams(){};

inline CouplingAtomicContinuumPython::CouplingAtomicContinuumPython(
    const std::string &name)
    : LMObject(name), CouplingAtomicContinuum(name){};

inline CouplingAtomicContinuumPython::~CouplingAtomicContinuumPython(){};

inline void CouplingAtomicContinuumPython::init() {
  try {
    this->init<dispatch>(this->getInput(stringify_macro(input1)),
                         this->getInput(stringify_macro(input2)));
  } catch (Component::UnconnectedInput &e) {
    LM_FATAL("Unconnected input for component " << this->getID() << std::endl
                                                << e.what());
  }
}

inline void CouplingAtomicContinuumPython::init(DomainInterface &domA,
                                                DomainInterface &domC) {
  PYBIND11_OVERRIDE(void, CouplingAtomicContinuum, init, domA, domC);
}

inline void CouplingAtomicContinuumPython::coupling(CouplingStage stage) {
  PYBIND11_OVERRIDE_PURE(void, CouplingAtomicContinuumPython, coupling, stage);
}

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
#endif /* __LIBMULTISCALE_COUPLING_ATOMIC_CONTINUUM_PYTHON_HH__ */
