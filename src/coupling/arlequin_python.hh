/**
 * @file   arlequin_python.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Nov 25 15:05:56 2013
 *
 * @brief  Arlequin bridging to be defined in python
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_ARLEQUIN_PYTHON_HH__
#define __LIBMULTISCALE_ARLEQUIN_PYTHON_HH__
/* -------------------------------------------------------------------------- */
#include "arlequin_template.hh"
#include "lib_continuum.hh"
#include "lib_md.hh"
/* -------------------------------------------------------------------------- */
#include <pybind11/operators.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
/* -------------------------------------------------------------------------- */
namespace py = pybind11;
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

class ArlequinPython : public virtual ArlequinTemplate {

public:
  DECLARE_COUPLER(ArlequinPython, domMD, domCONTINUUM);

  void init(DomainInterface &domA, DomainInterface &domC);

  LMID get_bridging_geom() { return bridging_geom; }
  LMID get_boundary_geom() { return boundary_geom; }

  bool is_in_atomic() { return ArlequinTemplate::is_in_atomic(); };
  bool is_in_continuum() { return ArlequinTemplate::is_in_continuum(); };

  void computeAtomicWeights(ContainerInterface &pointList) {
    ArlequinTemplate::computeAtomicWeights<dispatch>(pointList);
  };
  void computeContinuumWeights(ContainerInterface &meshList) {
    ArlequinTemplate::computeContinuumWeights<dispatch>(meshList);
  }
  void computeAtomicWeights(OutputContainer &pointList) {
    ArlequinTemplate::computeAtomicWeights<dispatch>(pointList);
  };
  void computeContinuumWeights(OutputContainer &meshList) {
    ArlequinTemplate::computeContinuumWeights<dispatch>(meshList);
  }

  ContainerArray<Real> &get_lambdas_mesh() { return lambdas_mesh; };
  ContainerArray<Real> &get_lambdas_point() { return lambdas_point; };

protected:
};

/* LMDESC ARLEQUIN_PYTHON

   This is an internal class, for the purpose of wiring with a python
   class allowing to implement the an Arlequin Coupler in python.

 */
inline void ArlequinPython::declareParams() {
  PYBIND11_OVERRIDE(void, ArlequinTemplate, declareParams, );
};

inline ArlequinPython::ArlequinPython(const std::string &name)
    : LMObject(name), ArlequinTemplate(name){};

inline ArlequinPython::~ArlequinPython(){};

inline void ArlequinPython::init() {
  try {
    this->init<dispatch>(this->getInput(stringify_macro(input1)),
                         this->getInput(stringify_macro(input2)));
  } catch (Component::UnconnectedInput &e) {
    LM_FATAL("Unconnected input for component " << this->getID() << std::endl
                                                << e.what());
  }
}

inline void ArlequinPython::init(DomainInterface &domA, DomainInterface &domC) {
  PYBIND11_OVERRIDE(void, ArlequinTemplate, init, domA, domC);
}

inline void ArlequinPython::coupling(CouplingStage stage) {
  PYBIND11_OVERRIDE_PURE(void, ArlequinPython, coupling, stage);
}

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
#endif /* __LIBMULTISCALE_ARLEQUIN_PYTHON_HH__ */
