/**
 * @file   bridging.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Jul 11 15:47:44 2014
 *
 * @brief  Bridging object between atomistic and finite elements
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

/* -------------------------------------------------------------------------- */
#include "bridging.hh"
#include "compute_extract.hh"
#include "factory_multiscale.hh"
#include "filter_geometry.hh"
#include "geometry_manager.hh"
#include "lm_common.hh"
#include "stimulation_field.hh"
#include "trace_atom.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

template <typename ContA, typename ContC>
void Bridging::init(ContA &contA, ContC &contC) {

  auto all_group = Communicator::getCommunicator().getGroup("all");

  DofAssociation::init(contA, contC);
  const UInt Dim = spatial_dimension;

  contMesh = contC;
  // unmatchedPointList.setCommGroup(this->comm_group_A);
  // unmatchedMeshList.setCommGroup(this->comm_group_B);
  unmatchedPointList.acquireContext(contA);
  unmatchedMeshList.acquireContext(contC);

  if (in_group_A()) {
    pointList.alloc<typename ContA::ContainerSubset>("pointList-" +
                                                     this->getID());
    pointList->acquireContext(contA);
    unmatchedPointList.acquireContext(contA);
  }

  if (in_group_B()) {
    meshList.alloc<typename ContC::ContainerSubset>("meshList-" +
                                                    this->getID());
    meshList->acquireContext(contC);
    unmatchedMeshList.acquireContext(contC);
  }

  buffer_for_points.acquireContext(contA);
  buffer_for_nodes.acquireContext(contC);

  DUMPBYPROC("selecting DOFs in bridging zone", DBG_INFO_STARTUP, 0);
  if (in_group_A()) {
    this->buildPointList(contA);
    this->buildPositions<dispatch>(unmatchedPointList.evalOutput());
  }
  if (in_group_B())
    this->buildContinuumDOFsList(contC);

  DUMP(this->getID() << " : Generating Communication Scheme", DBG_INFO_STARTUP);
  all_group.barrier();
  DUMP(this->getID() << " : exchange coarse geometries", DBG_MESSAGE);
  if (this->in_group_A()) {
    this->exchangeGeometries<dispatch>(this->unmatchedPointList.evalOutput());
  }
  if (in_group_B()) {
    this->exchangeGeometries<dispatch>(this->unmatchedMeshList.evalOutput());
  }
  all_group.barrier();

  DUMP(this->getID() << " : exchange points positions", DBG_MESSAGE);
  this->exchangePositions(Dim);
  all_group.barrier();

  DUMP(this->getID() << " : build shape matrix", DBG_MESSAGE);
  if (in_group_B()) {
    DUMP(this->getID() << " : build shape matrix (parsing "
                       << this->positions.rows() << " points by position)",
         DBG_INFO);
    this->buildShapeMatrix<dispatch>(this->unmatchedMeshList.evalOutput());
    DUMP(this->getID() << " : I will manage " << this->found_associations
                       << " points",
         DBG_INFO);
  }
  all_group.barrier();

  DUMP(this->getID() << " : ExchangeAssociation", DBG_MESSAGE);
  ContainerArray<UInt> managed;
  this->exchangeAssociationInformation(managed, pointToElement);

  for (auto &&[i, c_w] : enumerate(this->com_with))
    DUMP("second test Do I communicate with " << i << " : " << c_w, DBG_INFO);

  all_group.barrier();

  DUMP(this->getID()
           << " : generate duo vector and detect double point assignement",
       DBG_MESSAGE);
  std::vector<std::vector<UInt>> unassociated;
  if (this->in_group_A())
    this->createDuoVectorA("Point", managed, pointToElement, unassociated);

  DUMP(this->getID() << " : doing exchange rejected", DBG_MESSAGE);
  this->exchangeRejectedContinuumOwners(unassociated);

  all_group.barrier();

  DUMP(this->getID() << " : doing filter rejected", DBG_MESSAGE);
  if (this->in_group_B())
    this->filterRejectedContinuumOwners(unassociated);

  all_group.barrier();

  DUMP(this->getID() << " : filtering position & association vector",
       DBG_MESSAGE);

  if (this->in_group_A()) {
    filterPointListForUnmatched<dispatch>(pointList);
  }
  this->filterArray(this->positions);

  if (this->in_group_B()) {

    this->filterMapping(pointToElement);
    // this->buildNodeShape<dispatch>(this->meshList);
  }

  DUMP(this->getID() << " : all done for scheme generation", DBG_MESSAGE);

  all_group.barrier();
}
/* -------------------------------------------------------------------------- */

template <typename ContC>
void Bridging::buildContinuumDOFsList(ContC &contMesh) {

  constexpr UInt Dim = ContC::ContainerSubset::Dim;

  unmatchedMeshList.setParam("GEOMETRY", this->geomID);
  unmatchedMeshList.compute(contMesh);

  auto &unmatched_mesh =
      unmatchedMeshList.evalOutput<typename ContC::ContainerSubset>();
  DUMP("we have found " << unmatched_mesh.getContainerElems().size()
                        << " concerned elements",
       DBG_INFO_STARTUP);

  STARTTIMER("Filling spatial-grid");

  auto &contElems = unmatched_mesh.getContainerElems();
  // auto &contNodes = unmatched_mesh.getContainerNodes();

  std::vector<UInt> nodes;
  Geometry &geom = *GeometryManager::getManager().getGeometry(this->geomID);
  Cube cube = geom.getBoundingBox();
  DUMP("geometry of the grid => \n" << cube, DBG_INFO_STARTUP);

  auto &grid =
      this->grid.alloc<SpatialGridElem<UInt, Dim>>(cube, this->grid_division);

  UInt nb_elem = 0;
  for (auto &&el : contElems) {
    std::vector<Vector<Dim>> node_coords;
    auto &&nodes = el.globalIndexes();
    UInt nb = nodes.size();

    for (UInt i = 0; i < nb; ++i) {
      auto nd = el.getNode(i);

#ifndef LM_OPTIMIZED
      Real mass_node = nd.mass();
      LM_ASSERT(mass_node > 0, "invalid mass" << nodes[i] << " " << mass_node);
#endif

      node_coords.push_back(nd.position0());
    }

    grid.addFiniteElement(nb_elem, node_coords);
    ++nb_elem;
  }
  STOPTIMER("Filling spatial-grid");

  DUMP("we have found " << contElems.size() << " concerned elements",
       DBG_INFO_STARTUP);
  DUMP("in average " << grid.getAverageEltByBlock() << " elements per block",
       DBG_INFO_STARTUP);
}
/* -------------------------------------------------------------------------- */

template <typename ContA> void Bridging::buildPointList(ContA &contPoints) {

  unmatchedPointList.setParam("GEOMETRY", this->geomID);
  unmatchedPointList.compute(contPoints);
  UInt local_points = unmatchedPointList.size();

  if (!local_points)
    DUMP("We found no point in the bridging zone", DBG_INFO);

  DUMP("We found " << local_points << " point in the bridging zone",
       DBG_INFO_STARTUP);
}
/* -------------------------------------------------------------------------- */

template <typename ContMesh>
void Bridging::buildShapeMatrix(ContMesh &unmatchedMeshList) {

  UInt nb_points = this->positions.rows();
  pointToElement.assign(nb_points, UINT_MAX);

  if (unmatchedMeshList.getContainerElems().size() == 0) {
    // need to free the output produced by the component
    DUMP("elem_rec is empty!", DBG_WARNING);
    this->getOutput("grid").reset();
    this->removeOutput("grid");
    return;
  }

  constexpr UInt Dim = ContMesh::ContainerSubset::Dim;
  auto &grid = this->grid.get<SpatialGridElem<UInt, Dim> &>();

  //****************************************************************
  /* building the association array points <-> elements */
  //****************************************************************
  STARTTIMER("Construction association");

  for (UInt i = 0; i < nb_points; ++i) {

#ifndef TIMER
    if (i % 100000 == 0)
      DUMP("building association: point " << i << "/" << nb_points, DBG_INFO);
#endif

    Vector<Dim> x = this->positions(i);

    pointToElement[i] = UINT_MAX;

    for (auto &&j : grid.findSet(x)) {
      auto el = unmatchedMeshList.getContainerElems().get(j);
      DUMP("is point " << i << " within element " << j << "?", DBG_ALL);

      if (el.contains(x)) {
        LM_ASSERT(pointToElement[i] == UINT_MAX,
                  "this should not happen. "
                      << "I found twice the same point while filtering");
        DUMP("associating point " << i << " and element " << j, DBG_ALL);
        pointToElement[i] = j;
        ++found_associations;
        break;
      }
    }
  }

  STOPTIMER("Construction association");
  std::vector<UInt> nb_points_per_element(
      unmatchedMeshList.getContainerElems().size());

  nb_points_per_element.assign(unmatchedMeshList.getContainerElems().size(), 0);

  /* build number of atoms per element */
  for (UInt i = 0; i < nb_points; ++i) {
    if (pointToElement[i] != UINT_MAX)
      ++nb_points_per_element[pointToElement[i]];
  }

  /* filter element&node list for the unassociated nodes&elements
       - it needs the global meshlist
       - it sets the filtered mesh in meshList output
  */
  filterContainerElems<dispatch>(nb_points_per_element, this->contMesh);

  auto &meshList =
      this->getOutput<typename ContMesh::ContainerSubset>("meshList");

  auto &elements = meshList.getContainerElems();
  auto &nodes = meshList.getContainerNodes();

  this->buildLocalPBCPairs(meshList);

  smatrix.resize(nodes.size(), found_associations);

  /* set the indirection (alloc tab) plus the shapes */
  std::vector<Real> shapes;
  UInt atom_index = 0;

  for (UInt i = 0; i < nb_points; ++i) {
    if (pointToElement[i] == UINT_MAX)
      continue;

    auto &&X = this->positions(i);
    auto el = elements.get(pointToElement[i]);

    el.computeShapes(shapes, X);

    auto &&node_indices = el.getAlteredConnectivity();
    IF_TRACED(X, "checking for atom in trouble");

    std::vector<UInt> local_nodes(shapes.size());

    for (UInt nd = 0; nd < shapes.size(); ++nd) {
      UInt node_index = node_indices[nd];
      smatrix.coeffRef(node_index, atom_index) = shapes[nd];
    }
    ++atom_index;
  }
}

/* -------------------------------------------------------------------------- */
template <typename ContC> void Bridging::buildLocalPBCPairs(ContC &meshList) {

  for (auto &&[i_master, i_slave] : pbc_pairs) {
    UInt j_master = meshList.subIndex2Index(i_master);
    UInt j_slave = meshList.subIndex2Index(i_slave);
    if (j_master == UINT_MAX || j_slave == UINT_MAX)
      continue;

    local_pbc_pairs.push_back(std::make_pair(j_master, j_slave));
    reverse_local_pbc_pairs.insert(std::make_pair(j_slave, j_master));
  }
  DUMP("Detected " << local_pbc_pairs.size() << " local pairs, from "
                   << pbc_pairs.size() << " global pairs",
       DBG_MESSAGE);
}
/* -------------------------------------------------------------------------- */

template <typename Vec> void Bridging::setPBCSlaveFromMaster(Vec &mat) {

  for (auto &&[i_master, i_slave] : local_pbc_pairs) {
    mat.row(i_slave) = mat.row(i_master);
  }
}

/* -------------------------------------------------------------------------- */
template <typename ContMesh>
void Bridging::filterContainerElems(std::vector<UInt> &nb_atom_per_element,
                                    ContMesh &contMesh) {

  using ContainerSubset = typename ContMesh::ContainerSubset;
  typename ContainerSubset::ContainerElems new_t(this->getID() + ":elems");
  std::vector<int> new_index;

  auto &unmatchedMeshList =
      this->unmatchedMeshList.evalOutput<ContainerSubset>();
  auto &meshList = this->meshList.get<ContainerSubset>();

  for (UInt el = 0; el < nb_atom_per_element.size(); ++el) {
    if (nb_atom_per_element[el] > 0) {

      DUMP("elem " << el << " becomes " << new_t.size(), DBG_ALL);
      new_index.push_back(new_t.size());

      nb_atom_per_element[new_t.size()] = nb_atom_per_element[el];
      new_t.push_back(unmatchedMeshList.getContainerElems().get(el));
    } else
      new_index.push_back(new_t.size());
  }
  nb_atom_per_element.resize(new_t.size());
  meshList.clear();
  // rebuild the element container
  meshList.getContainerElems() = new_t;
  // rebuild the association list
  for (UInt i = 0; i < pointToElement.size(); ++i) {
    if (pointToElement[i] != UINT_MAX) {
      pointToElement[i] = new_index[pointToElement[i]];
    }
  }

  meshList.computeAlteredConnectivity(contMesh.getContainerNodes());
  UInt nb = meshList.size();
  if (!nb) {
    DUMP("We found no nodes in bridging zone", DBG_INFO_STARTUP);
    return;
  }

  DUMP("We found " << nb << " nodes concerned into "
                   << meshList.getContainerElems().size() << " elements",
       DBG_INFO_STARTUP);
}
/* -------------------------------------------------------------------------- */
template <typename ContA>
void Bridging::filterPointListForUnmatched(ContA &pointList) {

  auto &unmatchedPointList =
      this->unmatchedPointList.evalOutput<typename ContA::ContainerSubset>();

  pointList.clear();
  // positions.clear();

  for (UInt i = 0; i < pointToElement.size(); ++i) {
    // std::cerr << "pointToElement2[" << i << "] = " << pointToElement[i]
    //           << std::endl;
    if (pointToElement[i] != UINT_MAX) {
      DUMP("for container, atom " << i << " becomes " << pointList.size(),
           DBG_ALL);
      pointList.push_back(unmatchedPointList.get(i));
      // positions.push_back(unmatchedPointList.get(i).position0());
    } else {
      this->duo_vector->removeIndex(i);
      DUMP("atom filtered " << i << " " << unmatchedPointList.get(i),
           DBG_DETAIL);
    }
  }
  this->duo_vector->compress();
  pointList.acquireContext(unmatchedPointList);
}
/* -------------------------------------------------------------------------- */

// template <typename ContC>
// void Bridging::averageOnElements(ContainerArray<Real> &data_atomic,
//                                  ContainerArray<Real> &data_mesh,
//                                  ContC &meshList [[gnu::unused]]) {

//   LM_TOIMPLEMENT;
//   //  data_mesh.assign(meshList.getContainerElems().rows(),
//   //                  meshList.getContainerElems().cols());
//   // smatrix->averageOnElements(data_atomic, data_mesh);
// }
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
