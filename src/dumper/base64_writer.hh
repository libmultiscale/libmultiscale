/**
 * @file   base64_writer.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Mar 01 14:17:56 2013
 *
 * @brief  Class enabling writting of base64 encoding
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_BASE64_WRITER_HH__
#define __LIBMULTISCALE_BASE64_WRITER_HH__
/* -------------------------------------------------------------------------- */
#include "lm_file.hh"
#include "lm_log.hh"
#include <cstring>
#include <vector>
/* -------------------------------------------------------------------------- */
#ifdef LIBMULTISCALE_USE_ZLIB
#include <zlib.h>
#endif
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/** Class that allow to push binary data
    in base64 format to any file.
    This class is mainly used by the paraview helper
    to create binary XML VTK files.
    The conversion is a 4/3 size conversion. */

class Base64Writer {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  Base64Writer() {
    linelength = 0;
    initBase64Stuff();
    n = 0;
    start = -1;
  };

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  //! for any packet in base64 a little header is used that is an UInt = nbBytes
  //! written
  void writeHeader();
  //! this is used to allocate the memory for the final count of bytes
  void createHeader();
  //! when all stream is ready buffer is sent to file
  void dumpToFile();
  //! empty temporary buffer
  void clearBuffer();

  //! push to file a string
  void pushStrInBase64(char *c);
  //! push to file a Real
  void pushRealInBase64(Real c);
  //! push to file an integer
  void pushIntegerInBase64(UInt c);
  //! push to file a Byte
  void pushByteInBase64(unsigned char c);
  //! notify that we don't want to add any data. Closing the current buffer
  void finish();
  //! decode 3 bytes from 4 Base64 bytes (4/3 ratio)
  UInt decode(char c0, char c1, char c2, char c3, char *r0, char *r1, char *r2);

  //! set the output file to use
  void setOutputFile(LMFile &f) { file = f; };

private:
  //! when 4 bytes are ready they are dumped to buffer by this function
  void dumpToBuffer();
  //! initialisation process
  void initBase64Stuff();
  //! primitive function to push bytes to file
  // void ochar(UInt c);

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

  //! decoding table
  char dtable[256];
  //! encoding table
  char etable[256];
  //! stage in conversion process(1,2 or 3)
  UInt n;
  //! used to code/decode
  unsigned char igroup[3], ogroup[4];

  //! unused
  UInt linelength;
  //! unused
  // UInt maxlinelength;

  //! LM file descriptor
  LMFile file;

  //! buffer to cache data
  std::vector<unsigned char> buffer;
  //! number of bytes written to buffer
  long nbBytes;
  //! for rewind need o floating index
  int start;
};

/* -------------------------------------------------------------------------- */

inline void Base64Writer::initBase64Stuff() {
  memset(dtable, 0xFF, 256);
  memset(etable, 0xFF, 256);

  for (UInt i = 0; i < 9; i++) {
    etable[i] = (char)('A' + i);
    dtable[0 + etable[i]] = (char)(i);
    etable[i + 9] = (char)('J' + i);
    dtable[0 + etable[i + 9]] = (char)(i + 9);
    etable[26 + i] = (char)('a' + i);
    dtable[0 + etable[26 + i]] = (char)(i + 26);
    etable[26 + i + 9] = (char)('j' + i);
    dtable[0 + etable[26 + i + 9]] = (char)(i + 26 + 9);
  }
  for (UInt i = 0; i < 8; i++) {
    etable[i + 18] = (char)('S' + i);
    dtable[0 + etable[i + 18]] = (char)(i + 18);
    etable[26 + i + 18] = (char)('s' + i);
    dtable[0 + etable[26 + i + 18]] = (char)(26 + i + 18);
  }
  for (UInt i = 0; i < 10; i++) {
    etable[52 + i] = (char)('0' + i);
    dtable[0 + etable[i + 52]] = (char)(i + 52);
  }
  etable[62] = '+';
  dtable[0 + etable[62]] = 62;
  etable[63] = '/';
  dtable[0 + etable[63]] = 63;
}

/* -------------------------------------------------------------------------- */

inline void Base64Writer::pushIntegerInBase64(UInt d) {
  DUMP("pushing " << d << " ( n = " << n << " )", DBG_ALL);
  unsigned char *c = (unsigned char *)&d;
  for (UInt i = 0; i < sizeof(int); ++i) {
    pushByteInBase64(c[i]);
  }
}

/* -------------------------------------------------------------------------- */

inline void Base64Writer::pushStrInBase64(char *str) {

  unsigned char *c = (unsigned char *)str;
  for (UInt i = 0; i < 512; ++i) {
    if (str[i] == '\0')
      break;
    pushByteInBase64(c[i]);
  }
}
/* -------------------------------------------------------------------------- */

inline void Base64Writer::pushRealInBase64(Real d) {
  DUMP("pushing Real " << d << " as " << sizeof(Real) << " bytes", DBG_ALL);

  unsigned char *c = (unsigned char *)&d;
  for (UInt i = 0; i < sizeof(Real); ++i) {
    pushByteInBase64(c[i]);
  }
}
/* -------------------------------------------------------------------------- */

inline void Base64Writer::pushByteInBase64(unsigned char c) {
  // initialise les blocs
  DUMP("pushing byte " << (int)c << " at position " << n, DBG_ALL);

  if (n == 0) {
    igroup[0] = igroup[1] = igroup[2] = 0;
  }
  igroup[n] = c;
  ++n;

  if (n == 3) {
    dumpToBuffer();
  }
  nbBytes += 1;
}
/* -------------------------------------------------------------------------- */

inline void Base64Writer::finish() {
  if (n == 0)
    return;

  dumpToBuffer();
  linelength = 0;
}
/* -------------------------------------------------------------------------- */

inline void Base64Writer::dumpToBuffer() {
  if (n < 3) {
    igroup[2] = 0;
    if (n < 2) {
      igroup[1] = 0;
    }
  }

  DUMP("premiere partie en base 64 : " << (igroup[0] >> 2), DBG_ALL);
  ogroup[0] = etable[igroup[0] >> 2];
  DUMP("deuxieme partie en base 64 : "
           << (((igroup[0] & 3) << 4) | (igroup[1] >> 4)),
       DBG_ALL);
  ogroup[1] = etable[((igroup[0] & 3) << 4) | (igroup[1] >> 4)];
  DUMP("troisieme partie en base 64 : "
           << (((igroup[1] & 0xF) << 2) | (igroup[2] >> 6)),
       DBG_ALL);
  ogroup[2] = etable[((igroup[1] & 0xF) << 2) | (igroup[2] >> 6)];
  DUMP("last partie en base 64 : " << (igroup[2] & 0x3F), DBG_ALL);
  ogroup[3] = etable[igroup[2] & 0x3F];

  if (n < 3) {
    ogroup[3] = '=';
    if (n < 2) {
      ogroup[2] = '=';
    }
  }

  for (UInt i = 0; i < 4; i++) {
    DUMP("dumped to buffer " << ogroup[i], DBG_ALL);
    if (start == -1)
      buffer.push_back(ogroup[i]);
    else {
      buffer[start] = ogroup[i];
      ++start;
    }
  }

  // remise a zero du compteur
  n = 0;
}

/* inline void Base64Writer::ochar(UInt c) */
/* { */
/*   if(file.dumpchar(c)==-1){ */
/*       FATAL("error while writing to file (in compressed mode) ! no more space
 * ?"); */
/*   } */
/*   linelength++; */
/* } */

/* -------------------------------------------------------------------------- */

inline UInt Base64Writer::decode(char c0, char c1, char c2, char c3, char *r0,
                                 char *r1, char *r2) {

  unsigned char d0, d1, d2, d3;

  d0 = dtable[0 + c0];
  d1 = dtable[0 + c1];
  d2 = dtable[0 + c2];
  d3 = dtable[0 + c3];

  DUMP("d0 " << (int)d0 << " d1 " << (int)d1 << " d2 " << (int)d2 << " d3 "
             << (int)d3,
       DBG_ALL);

  // Decode the 3 bytes

  *r0 = (char)(((d0 << 2) & 0xFC) | ((d1 >> 4) & 0x03));
  *r1 = (char)(((d1 << 4) & 0xF0) | ((d2 >> 2) & 0x0F));
  *r2 = (char)(((d2 << 6) & 0xC0) | ((d3 >> 0) & 0x3F));

  DUMP("r0 " << (int)*r0 << " r1 " << (int)*r1 << " r2 " << (int)*r2, DBG_ALL);

  // Return the number of bytes actually decoded

  if (c2 == '=') {
    return 1;
  }
  if (c3 == '=') {
    return 2;
  }
  return 3;
}

/* -------------------------------------------------------------------------- */

inline void Base64Writer::writeHeader() {

  //  if (bflag == BASE64){
  //  char byteC[8];
  char byte[6];

  finish();

  /*   long save_offset; */

  /*   save_offset = file.tell(); */
  /*   file.seek(header_offset,SEEK_SET); */
  /*   //reread the 4 bytes precedently written */

  /*   file.read(byteC,sizeof(char),4); */

  /*   DUMP("la chaine saisie " << byteC[0] << " " << byteC[1] << " " <<
   * byteC[2] << " " << byteC[3]); */
  /*   b64.Decode(byteC[0],byteC[0],byteC[0],byteC[0], */
  /* 	     byte,byte+1,byte+2); */
  DUMP("la chaine saisie " << buffer[0] << " " << buffer[1] << " " << buffer[2]
                           << " " << buffer[3],
       DBG_ALL);
  decode(buffer[0], buffer[0], buffer[0], buffer[0], byte, byte + 1, byte + 2);

  /*   file.read(byteC+4,sizeof(char),4); */
  /*   DUMP("la chaine saisie " << byteC[4] << " " << byteC[5] << " " <<
   * byteC[6] << " " << byteC[7]); */
  /*   UInt nb = b64.Decode(byteC[4],byteC[5],byteC[6],byteC[7], */
  /* 		      byte+3,byte+4,byte+5); */

  DUMP("la chaine saisie " << buffer[4] << " " << buffer[5] << " " << buffer[6]
                           << " " << buffer[7],
       DBG_ALL);
  UInt nb = decode(buffer[4], buffer[5], buffer[6], buffer[7], byte + 3,
                   byte + 4, byte + 5);

  /*   //je me replace au debut du header */
  /*   file.seek(header_offset,SEEK_SET); */
  /*   //je viens de relire 6 octets */
  /*   //les quatres premiers seulement sont a changer */

  DUMP("placing number of writen bytes : " << nbBytes << " " << buffer.size(),
       DBG_ALL);

  UInt temp = nbBytes;
  start = 0;
  pushIntegerInBase64(temp);
  if (nb > 1)
    pushByteInBase64(byte[4]);
  if (nb > 2)
    pushByteInBase64(byte[5]);

  start = -1;
  nbBytes = temp;

  finish();
  dumpToFile();
}
/* -------------------------------------------------------------------------- */

inline void Base64Writer::dumpToFile() {
  file.write(&buffer[0], buffer.size(), 1);
}
/* -------------------------------------------------------------------------- */

inline void Base64Writer::clearBuffer() {
  buffer.clear();
  nbBytes = 0;
}
/* -------------------------------------------------------------------------- */

inline void Base64Writer::createHeader() {
  clearBuffer();
  pushIntegerInBase64(0);
}
/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_BASE64_WRITER_HH__ */
