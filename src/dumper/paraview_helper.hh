/**
 * @file   paraview_helper.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Jan 11 13:30:17 2013
 *
 * @brief  An helper class to generate VTK headers
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_PARAVIEW_HELPER_HH__
#define __LIBMULTISCALE_PARAVIEW_HELPER_HH__
/* -------------------------------------------------------------------------- */
#include "base64_writer.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class ParaviewHelper {

public:
  /* ------------------------------------------------------------------------ */
  /* Typedefs                                                                 */
  /* ------------------------------------------------------------------------ */

  enum ParaHelperOutType { TEXT = 0, BASE64 = 1, COMPRESSED = 2 };

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  ParaviewHelper(LMFile &f) {
    file = f;
    setOutputFile(file);
    compteur = 0;
  };

  ParaviewHelper() {
    bflag = BASE64;
    compteur = 0;
  };
  virtual ~ParaviewHelper(){};

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  void setOutputFile(LMFile &file);
  void write_header(UInt nb_nodes, UInt nb_elems);
  void startDofList();
  void endDofList();
  void startCellsConnectivityList();
  void endCellsConnectivityList();
  void startCellsoffsetsList();
  void endCellsoffsetsList();
  void startCellstypesList();
  void endCellstypesList();
  void startPointDataList();
  void endPointDataList();
  void startCellDataList();
  void endCellDataList();
  void startData(const std::string &name, UInt nb_components);
  void PDataArray(const std::string &name, UInt nb_components);
  void endData();
  void write_conclusion();

  template <typename V> void push(V &&x, UInt dim);

  void pushReal(const Real &x);
  void pushInteger(const UInt &i);

  void setMode(UInt mode) { bflag = BASE64 & mode; }

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

protected:
  Base64Writer b64;
  UInt bflag;

  LMFile file;
  long header_offset;

  UInt compteur;
};

/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::setOutputFile(LMFile &f) {
  file = f;
  b64.setOutputFile(file);
}

/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::write_header(UInt nb_nodes, UInt nb_elems) {

  file.printf("<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" ");
  /* #ifdef BIG_ENDIAN */
  /*   file.printf("byte_order=\"BigEndian\">\n"); */
  /* #else */
  file.printf("byte_order=\"LittleEndian\">\n");
  /* #endif */
  file.printf("<UnstructuredGrid>\n<Piece NumberOfPoints= \"%d\" "
              "NumberOfCells=\"%d\">\n",
              nb_nodes, nb_elems);
}

/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::startDofList() {

  file.printf(
      "<Points><DataArray type = \"Float64\" NumberOfComponents=\"%d\" ", 3);
  if (bflag == BASE64)
    file.printf("format=\"binary\">\n");
  else
    file.printf("format=\"ascii\">\n");

  if (bflag == BASE64)
    b64.createHeader();
}

/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::endDofList() {
  if (bflag == BASE64)
    b64.writeHeader();

  file.printf("</DataArray>\n");
  file.printf("</Points>\n");
}

/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::startCellsConnectivityList() {

  file.printf("<Cells>\n");
  file.printf("<DataArray type=\"Int32\" Name=\"connectivity\" ");
  if (bflag == BASE64)
    file.printf("format=\"binary\">\n");
  else
    file.printf("format=\"ascii\">\n");

  if (bflag == BASE64)
    b64.createHeader();
}

/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::endCellsConnectivityList() {
  if (bflag == BASE64)
    b64.writeHeader();

  file.printf("</DataArray>\n");
}

/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::startCellsoffsetsList() {

  file.printf("<DataArray type=\"Int32\" Name=\"offsets\" ");
  if (bflag == BASE64)
    file.printf("format=\"binary\">\n");
  else
    file.printf("format=\"ascii\">\n");
  if (bflag == BASE64)
    b64.createHeader();
}

/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::endCellsoffsetsList() {
  if (bflag == BASE64)
    b64.writeHeader();

  file.printf("</DataArray>\n");
}

/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::startCellstypesList() {
  file.printf("<DataArray type=\"Int32\" Name=\"types\" ");
  if (bflag == BASE64)
    file.printf("format=\"binary\">\n");
  else
    file.printf("format=\"ascii\">\n");
  if (bflag == BASE64)
    b64.createHeader();
}

/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::endCellstypesList() {
  if (bflag == BASE64)
    b64.writeHeader();

  file.printf("</DataArray>\n");
  file.printf("</Cells>\n");
}

/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::startPointDataList() {
  file.printf("<PointData>\n");
}
/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::endPointDataList() {
  file.printf("</PointData>\n");
}
/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::startCellDataList() { file.printf("<CellData>\n"); }
/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::endCellDataList() { file.printf("</CellData>\n"); }
/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::startData(const std::string &name,
                                      UInt nb_components) {
  file.printf(
      "<DataArray type=\"Float64\" NumberOfComponents=\"%d\" Name=\"%s\" ",
      nb_components, name.c_str());
  if (bflag == BASE64)
    file.printf("format=\"binary\">\n");
  else
    file.printf("format=\"ascii\">\n");
  if (bflag == BASE64)
    b64.createHeader();
}
/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::endData() {
  if (bflag == BASE64)
    b64.writeHeader();

  file.printf("</DataArray>\n");
}

/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::PDataArray(const std::string &name,
                                       UInt nb_components) {
  file.printf(
      "<PDataArray type=\"Float64\" NumberOfComponents=\"%d\" Name=\"%s\" ",
      nb_components, name.c_str());
  if (bflag == BASE64)
    file.printf("format=\"binary\"></PDataArray>\n");
  else
    file.printf("format=\"ascii\"></PDataArray>\n");
}

/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::write_conclusion() {
  file.printf("</Piece>\n");
  file.printf("</UnstructuredGrid></VTKFile>");
}

/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::pushReal(const Real &d) {
  if (bflag == BASE64)
    b64.pushRealInBase64(d);
  else {
    file.printf("%.15e ", d);
    ++compteur;
    if (compteur % 3 == 0)
      file.printf("\n");
  }
}

/* -------------------------------------------------------------------------- */

template <typename V> void ParaviewHelper::push(V &&x, UInt dim) {
  UInt Dim = x.size();
  if (Dim > dim)
    LM_FATAL("not compatible vector");

  for (UInt i = 0; i < Dim; ++i) {
    this->pushReal(x[i]);
  }

  for (UInt i = 0; i < dim - Dim; ++i) {
    this->pushReal(0.);
  }
}

/* -------------------------------------------------------------------------- */

inline void ParaviewHelper::pushInteger(const UInt &i) {
  if (bflag == BASE64)
    b64.pushIntegerInBase64(i);
  else {
    file.printf("%d ", i);
  }
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_PARAVIEW_HELPER_HH__ */
