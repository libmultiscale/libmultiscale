/**
 * @file   dumper_ekin.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This dmuper generates a text file with the kinetic energy
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#include "dumper_ekin.hh"
#include "compute_ekin.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
#include "ref_point_data.hh"
#include <fstream>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

template <typename Cont> void DumperEKin::dump(Cont &cont) {

  ComputeEKin compute("ComputeEkin:" + this->getID());
  compute.build(cont);

  CommGroup group = cont.getCommGroup();
  UInt myrank = group.getMyRank();

  if (myrank == 0) {
    this->file << current_step << " " << compute.evalArrayOutput()[2] << " "
               << compute.evalArrayOutput()[1] << std::endl;
  }
}
/* -------------------------------------------------------------------------- */

void DumperEKin::init() {
  std::string fname = this->getBaseName() + "_ekin.plot";
  this->file.open(fname.c_str());
  if (this->file.is_open() == false)
    LM_FATAL("Cannot open file " << fname << " for dumper epot : exit");
}

/* -------------------------------------------------------------------------- */

/* LMDESC EKIN
   This dumper outputs to a single file kinetic energy informations. \\ \ \
   The file takes the column-like format: \\ \				\
   TimeStep \hspace{1cm} EKin \hspace{1cm} Temperature
*/

/* LMEXAMPLE DUMPER ekinmd EKIN INPUT md FREQ 100 PREFIX ./ */
/* LMHERITANCE dumper_interface */

void DumperEKin::declareParams() { DumperInterface::declareParams(); }

/* -------------------------------------------------------------------------- */

DumperEKin::DumperEKin(const std::string &name) : LMObject(name) {}

/* -------------------------------------------------------------------------- */

DumperEKin::~DumperEKin() {}

/* -------------------------------------------------------------------------- */

DECLARE_DUMPER_MAKE_CALL(DumperEKin)

__END_LIBMULTISCALE__
