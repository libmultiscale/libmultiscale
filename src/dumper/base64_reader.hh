/**
 * @file   base64_reader.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Mar 01 23:56:16 2013
 *
 * @brief  Class enabling reading of base64 encoding
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_BASE64_READER_HH__
#define __LIBMULTISCALE_BASE64_READER_HH__
/* -------------------------------------------------------------------------- */
#include "cstring"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

struct InputStream {
  //! check if possible to read some bytes
  virtual bool canRead(UInt len) = 0;

  template <typename T> bool canRead() { return this->canRead(sizeof(T)); };

  //! extract a real
  template <typename T> InputStream &operator>>(T &c);

protected:
  //! extract a Byte of data
  virtual void readByte(char &c) = 0;
};

/* ------------------------------------------------------------------------ */

template <typename T> InputStream &InputStream::operator>>(T &d) {
  char *c = reinterpret_cast<char *>(&d);
  UInt i;
  for (i = 0; i < sizeof(T); ++i) {
    this->readByte(c[i]);
    DUMP("read character " << (int)c[i], DBG_DETAIL);
  }
  return *this;
}
/* ------------------------------------------------------------------------ */

/** Class that allow to push binary data
    in base64 format to any file.
    This class is mainly used by the paraview helper
    to create binary XML VTK files.
    The conversion is a 4/3 size conversion. */

class Base64Reader : public InputStream {

public:
  Base64Reader();

  //! set the reading stream
  void str(std::string &str);
  //! reset internal reading variables
  void reset();
  //! check if possible to read some bytes
  bool canRead(UInt len) override;

private:
  //! extract a bytes
  void readByte(char &c) override;
  //! initialisation process
  void init();
  //! decode 3 bytes from 4 Base64 bytes (4/3 ratio)
  void decode();
  //! decode all bytes from coded_bytes buffer
  void decodeAll();

  //! decoding table
  char dtable[256];
  //! encoding table
  char etable[256];
  //! already read encoded bytes
  std::list<char> coded_bytes;
  //! stream of decoded bytes
  std::list<char> decoded_bytes;
};

/* ------------------------------------------------------------------------ */

inline void Base64Reader::init() {
  memset(dtable, 0xFF, 256);
  memset(etable, 0xFF, 256);

  for (UInt i = 0; i < 9; i++) {
    etable[i] = (char)('A' + i);
    dtable[0 + etable[i]] = (char)i;
    etable[i + 9] = (char)('J' + i);
    dtable[0 + etable[i + 9]] = (char)(i + 9);
    etable[26 + i] = (char)('a' + i);
    dtable[0 + etable[26 + i]] = (char)(i + 26);
    etable[26 + i + 9] = (char)('j' + i);
    dtable[0 + etable[26 + i + 9]] = (char)(i + 26 + 9);
  }
  for (UInt i = 0; i < 8; i++) {
    etable[i + 18] = (char)('S' + i);
    dtable[0 + etable[i + 18]] = (char)(i + 18);
    etable[26 + i + 18] = (char)('s' + i);
    dtable[0 + etable[26 + i + 18]] = (char)(26 + i + 18);
  }
  for (UInt i = 0; i < 10; i++) {
    etable[52 + i] = (char)('0' + i);
    dtable[0 + etable[i + 52]] = (char)(i + 52);
  }
  etable[62] = '+';
  dtable[0 + etable[62]] = 62;
  etable[63] = '/';
  dtable[0 + etable[63]] = 63;
}

/* --------------------------------------------------------------------------
 */

inline Base64Reader::Base64Reader() {
  init();
  reset();
}
/* --------------------------------------------------------------------------
 */

inline void Base64Reader::reset() {
  this->coded_bytes.clear();
  this->decoded_bytes.clear();
}
/* --------------------------------------------------------------------------
 */

inline void Base64Reader::str(std::string &str) {
  for (UInt i = 0; i < str.size(); ++i)
    this->coded_bytes.push_back(str[i]);

  this->decodeAll();
}
/* --------------------------------------------------------------------------
 */

inline void Base64Reader::decode() {
  assert(coded_bytes.size() > 3);

  std::array<char, 4> coded;

  for (UInt i = 0; i < 4; ++i) {
    coded[i] = this->coded_bytes.front();
    this->coded_bytes.pop_front();
  }

  unsigned char r0, r1, r2;
  unsigned char d0, d1, d2, d3;
  d0 = dtable[0 + coded[0]];
  d1 = dtable[0 + coded[1]];
  d2 = dtable[0 + coded[2]];
  d3 = dtable[0 + coded[3]];

  DUMP("code " << coded[0] << " " << coded[1] << " " << coded[2] << " "
               << coded[3],
       DBG_DETAIL);

  DUMP("code " << (int)d0 << " " << (int)d1 << " " << (int)d2 << " " << (int)d3,
       DBG_DETAIL);

  // Decode the 3 bytes
  r0 = (char)(((d0 << 2) & 0xFC) | ((d1 >> 4) & 0x03));
  r1 = (char)(((d1 << 4) & 0xF0) | ((d2 >> 2) & 0x0F));
  r2 = (char)(((d2 << 6) & 0xC0) | ((d3 >> 0) & 0x3F));

  DUMP("decode " << (int)r0 << " " << (int)r1 << " " << (int)r2, DBG_DETAIL);

  decoded_bytes.push_back(r0);
  // treat incomplete data
  if (coded[2] != '=') {
    decoded_bytes.push_back(r1);
  }
  if (coded[3] != '=') {
    decoded_bytes.push_back(r2);
  }
}

/* ------------------------------------------------------------------------ */
inline void Base64Reader::decodeAll() {
  DUMP("decoding Base64", DBG_INFO);
  while (coded_bytes.size() >= 4) {
    this->decode();
  }
}

/* ------------------------------------------------------------------------ */

inline void Base64Reader::readByte(char &c) {

  assert(this->decoded_bytes.size() > 0);

  // pop the character to be read
  c = this->decoded_bytes.front();
  this->decoded_bytes.pop_front();
}

/* ------------------------------------------------------------------------ */

inline bool Base64Reader::canRead(UInt len) {

  return this->decoded_bytes.size() >= len;
}

/* ------------------------------------------------------------------------ */

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_BASE64_READER_HH__ */
