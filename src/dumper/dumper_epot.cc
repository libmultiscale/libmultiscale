/**
 * @file   dumper_epot.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This dumper outputs to a single file potential energy informations
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "dumper_epot.hh"
#include "compute_epot.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
#include "ref_point_data.hh"
#include <fstream>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

template <typename Cont> void DumperEPot::dump(Cont &cont) {

  ComputeEPot compute("computeEpot:" + this->getID());
  compute.build(cont);

  CommGroup group = cont.getCommGroup();
  UInt myrank = group.getMyRank();

  if (myrank == 0) {

    std::string filename = this->getBaseName() + "_epot.plot";
    std::ofstream file(filename.c_str(), std::ios_base::app);

    file << current_step << " " << compute.evalArrayOutput()[0] << std::endl;
  }
}
/* -------------------------------------------------------------------------- */
void DumperEPot::init() {
  std::string fname = this->getBaseName() + "_epot.plot";
  std::ofstream file(fname.c_str());
  if (file.is_open() == false)
    LM_FATAL("Cannot open file " << fname << " for dumper epot : exit");
}

/* -------------------------------------------------------------------------- */

/* LMDESC EPOT
   This dumper outputs to a single file potential energy informations. \\ \ \
   The file takes the column-like format: \\ \				\
   TimeStep \hspace{1cm} EPot
*/

/* LMEXAMPLE DUMPER epotmd EPOT INPUT md FREQ 100 PREFIX ./ */
/* LMHERITANCE dumper_interface */

void DumperEPot::declareParams() { DumperInterface::declareParams(); }

/* -------------------------------------------------------------------------- */

DumperEPot::DumperEPot(const std::string &name) : LMObject(name) {}

/* -------------------------------------------------------------------------- */

DumperEPot::~DumperEPot() {}

/* -------------------------------------------------------------------------- */
DECLARE_DUMPER_MAKE_CALL(DumperEPot)

__END_LIBMULTISCALE__
