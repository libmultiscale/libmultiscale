/**
 * @file   dumper_paraview.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Jul 04 21:55:04 2014
 *
 * @brief  This dumper allows to generate paraview files for vizualization
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_DUMPER_PARAVIEW_HH__
#define __LIBMULTISCALE_DUMPER_PARAVIEW_HH__
/* -------------------------------------------------------------------------- */
#include "dumper_interface.hh"
#include "paraview_helper.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/** Class DumperParaview
 * Implementation of a dumper to paraview vtu files
 */

class DumperParaview : public DumperInterface {

public:
  DECLARE_DUMPER(DumperParaview,
                 _or<MD, CONTINUUM, DD, POINT, MESH, COMPONENT>);

  //! most generic dump function
  template <typename Cont> enable_if_component<Cont> dump(Cont &cont);
  template <typename Cont> enable_if_point<Cont> dump(Cont &cont);
  template <typename Cont> enable_if_mesh<Cont> dump(Cont &cont);

  //! initialisation of the dumper
  void init() override;

protected:
  template <typename _Input>
  void dumpField(const std::string &name_field, _Input &cont,
                 ContainerArray<Real> &contField, const UInt Dim);

  template <UInt Dim, typename _Input>
  void dumpField(const std::string &name_field, _Input &cont,
                 ContainerArray<Real> &contField);

  void openPVTU();
  void openVTU();

  template <typename F> void foreach_additional_field(F &&f);
  template <typename Cont, typename Func>
  decltype(auto) foreach_activated_fields(Cont &cont, Func &&func);

private:
  //! flag for displacement field
  bool dep_field;
  //! flag for initial position field
  bool p0_field;
  //! flag for velocity field
  bool vel_field;
  //! flag for acceleration field
  bool force_field;
  //! flag for external force field
  bool external_force_field;
  //! flag for internal force field
  bool internal_force_field;
  //! flag for force field
  bool acceleration_field;
  //! flag for distribution field
  bool proc_field;
  //! flag for constraUInt field
  bool stress_field;
  //! flag for deformation field
  bool strain_field;
  //! flag for potential energy field
  bool epot_field;
  //! flag for mass field
  bool mass_field;
  //! flag for charge field
  bool charge_field;
  //! flag for id field
  bool id_field;
  //! flag to produce zipped files
  bool flag_compressed;
  //! flag to produce text files
  bool flag_text;
  //! flag to produce base64 files
  bool flag_base64;
  //! flag for electronic density
  bool e_density_field;
  //! flag for barycenter computation
  bool barycenters_field;
  //! flag for temperature field
  bool temperature_field;
  //! flag for temperature variation field
  bool temperatureVar_field;
  //! flag for heat capacity field
  bool heatCapacity_field;
  //! flag for heat capacity field
  bool heatRate_field;
  //! flag for external heat rate field
  bool external_heatRate_field;
  //! flag for tag field
  bool tag_field;
  //! flag for output of boundary information
  bool boundary_field;
  //! flag for output of burgers information
  bool burgers_field;
  //! flag for output of normals information
  bool normal_field;
  //! flag for output of torques
  bool torque_field;
  //! flag for output of angular velocity
  bool angular_velocity_field;
  //! flag for output of angular acceleration
  bool angular_acceleration_field;
  //! flag for output of radius
  bool radius_field;

  //! additional field
  std::vector<LMID> additional_fields;

  //! pointer to paraview helper facilities
  ParaviewHelper paraHelper;

  //! the file where to write
  LMFile file;

  //! list of activated fields
  std::vector<FieldType> activated_fields;
};
/* --------------------------------------------------------------------------
 */

template <UInt Dim, typename _Input>
void DumperParaview::dumpField(const std::string &name_field, _Input &cont,
                               ContainerArray<Real> &contField) {
  if (contField.rows() != cont.size())
    LM_FATAL("compute/filter " << name_field << " contains "
                               << contField.size() / Dim << " entries"
                               << " while container has " << cont.size()
                               << " : incompatible match");

  paraHelper.startData(name_field, contField.getDim());

  UInt cpt = 0;
  for (auto &&value : contField) {
    paraHelper.pushReal(value);
    ++cpt;
  }
  paraHelper.endData();
}
/* --------------------------------------------------------------------------
 */

template <typename _Input>
void DumperParaview::dumpField(const std::string &name_field, _Input &cont,
                               ContainerArray<Real> &contField,
                               const UInt Dim) {

  if (Dim == 0) {
    DUMP(name_field << " was not yet computed: cannot dump in paraview",
         DBG_WARNING);
    return;
  }
  switch (Dim) {
  case 1:
    dumpField<1>(name_field, cont, contField);
    break;
  case 2:
    dumpField<2>(name_field, cont, contField);
    break;
  case 3:
    dumpField<3>(name_field, cont, contField);
    break;
  case 4:
    dumpField<3>(name_field, cont, contField);
    break;
  case 5:
    dumpField<3>(name_field, cont, contField);
    break;
  case 6:
    dumpField<3>(name_field, cont, contField);
    break;
  case 7:
    dumpField<3>(name_field, cont, contField);
    break;
  case 8:
    dumpField<3>(name_field, cont, contField);
    break;
  case 9:
    dumpField<3>(name_field, cont, contField);
    break;
  case 10:
    dumpField<3>(name_field, cont, contField);
    break;
  default:
    LM_FATAL("internal error " << Dim << " " << name_field);
  }
}
/* --------------------------------------------------------------------------
 */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_DUMPER_PARAVIEW_HH__ */
