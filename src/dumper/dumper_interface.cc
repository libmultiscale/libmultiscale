/**
 * @file   dumper_interface.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Thu Jul 24 14:21:58 2014
 *
 * @brief  Mother of all dumpers
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "dumper_interface.hh"
#include "compute_interface.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
#include "ref_point_data.hh"
#include <fstream>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

const std::string &DumperInterface::getBaseName() {
  buildNameFile();
  return base_name;
}

/* -------------------------------------------------------------------------- */

DumperInterface::DumperInterface() {
  nothing_to_do_flag = false;
  dump_unit_system = code_unit_system;
  prefix = "./";
  this->createInput("input");
}
/* -------------------------------------------------------------------------- */

void DumperInterface::buildNameFile() {
  std::stringstream sbase_name;
  sbase_name << prefix << "/" << getID();
  base_name = sbase_name.str();
}
/* -------------------------------------------------------------------------- */
void DumperInterface::init() {
  buildNameFile();
  if (lm_my_proc_id == 0) {
    std::stringstream fname;
    fname << prefix << "/release.info";
    std::ofstream file_release(fname.str().c_str());
    file_release << lm_release_info;
    file_release.close();
  }
}
/* -------------------------------------------------------------------------- */

/* LMDESC Dumper
   This describe the generic keywords which all dumper
   inheritate from.
*/

/* LMHERITANCE action_interface */

void DumperInterface::declareParams() {
  ActionInterface::declareParams();

  /* LMKEYWORD UNITS
     specify custom output unit system can take values RealUnits, SIUnits,
     or MetalUnits. By default uses the code units specified for LibMultiscale
  */
  this->parseKeyword("UNITS", dump_unit_system, code_unit_system);

  /* LMKEYWORD PREFIX
     Allows to specify the directory where the dumped files should be created
  */

  this->parseKeyword("PREFIX", prefix, "./");
}
/* -------------------------------------------------------------------------- */

void DumperInterface::dump() { this->action(); }

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
