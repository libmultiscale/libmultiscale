/**
 * @file   dumper_blackdynamite.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Jul 29 11:44:58 2014
 *
 * @brief  This allows to push the result of computes to
 * BlackDynamite/PostgreSQL database
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_DUMPER_BLACK_DYNAMITE_HH__
#define __LIBMULTISCALE_DUMPER_BLACK_DYNAMITE_HH__
/* -------------------------------------------------------------------------- */
#include "compute_concat.hh"
#include "domain_interface.hh"
#include "dumper.hh"
#include <blackdynamite.hh>
#include <fstream>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

template <typename _Input> class DumperBlackDynamite : public Dumper<_Input> {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  DumperBlackDynamite(const std::string &name, ComponentLMInterface &d);

  ~DumperBlackDynamite();

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  void dump(_Input &cont);
  void declareParams();
  void init();

protected:
  ComputeConcat<_Input> compute_concat;

  std::ofstream my_file;

  UInt rank;

  static BlackDynamite::RunManager *bd_run_manager;
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_DUMPER_BLACK_DYNAMITE_HH__ */
