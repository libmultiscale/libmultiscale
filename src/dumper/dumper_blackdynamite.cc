/**
 * @file   dumper_blackdynamite.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Jul 29 11:44:58 2014
 *
 * @brief  This allows to push the result of computes to
 * BlackDynamite/PostgreSQL database
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "dumper_blackdynamite.hh"
#include "communicator.hh"
#include "compute_interface.hh"
#include "filter_manager.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include <iomanip>
/* -------------------------------------------------------------------------- */
extern std::string lm_release_info;
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

template <typename Cont> void DumperBlackDynamite<Cont>::init() {
  if (this->bd_run_manager == NULL)
    this->bd_run_manager = new BlackDynamite::RunManager;
}
/* -------------------------------------------------------------------------- */

template <typename Cont> void DumperBlackDynamite<Cont>::dump(Cont &cont) {

  compute_concat.build(cont);

  Communicator &comm = Communicator::getCommunicator();
  CommGroup group = cont.getCommGroup();

  // only root node will dump the result of this compute
  UInt my_rank = comm.groupRank(lm_my_proc_id, group);

  if (my_rank != this->rank)
    return;

  bd_run_manager->startRun();
  bd_run_manager->push(lm_release_info, "lm_release_info");

  UInt dim = compute_concat.getDim();

  LM_ASSERT(compute_concat.size() % dim == 0, "invalid container");

  UInt n_entries = compute_concat.size() / dim;
  if (n_entries != 1)
    LM_FATAL("BlackDynamite dumper can only push single entry computes");

  if (compute_concat.name_computed.size() != dim)
    LM_FATAL(
        "cannot retreive the quantity name of the quantities to be pushed ");

  for (UInt i = 0; i < dim; ++i) {
    Real value = compute_concat[i];
    std::string quantity_name = this->getID();
    quantity_name += ":" + compute_concat.name_computed[i];
    DUMP("pushing " << quantity_name << " with value " << value, DBG_INFO);
    bd_run_manager->push(value, quantity_name, current_step);
  }
}

/* -------------------------------------------------------------------------- */

/* LMDESC BLACKDYNAMITE
   This allows to push the result of computes to BlackDynamite/PostgreSQL
   database.
*/

/* LMHERITANCE compute_concat dumper */
/* LMEXAMPLE DUMPER bd BLACKDYNAMITE INPUT md FREQ 100 */

template <typename _Input> void DumperBlackDynamite<_Input>::declareParams() {
  Dumper<_Input>::declareParams();

  this->addSubParsableObject(compute_concat);

  /* LMKEYWORD RANK
     The rank of processor which should push to database
  */
  this->parseKeyword("RANK", rank, 0u);
}

/* -------------------------------------------------------------------------- */

template <typename _Input>
DumperBlackDynamite<_Input>::DumperBlackDynamite(const std::string &name,
                                                 ComponentLMInterface &d)
    : Dumper<_Input>(name, d), compute_concat("", d) {
  rank = 0;
}

/* -------------------------------------------------------------------------- */

template <typename _Input> DumperBlackDynamite<_Input>::~DumperBlackDynamite() {
  if (bd_run_manager->getInstanceCounter() == 1) {
    bd_run_manager->changeState("FINISHED");
  }
  delete bd_run_manager;
}
/* -------------------------------------------------------------------------- */

template <typename _Input>
BlackDynamite::RunManager *DumperBlackDynamite<_Input>::bd_run_manager = NULL;
/* -------------------------------------------------------------------------- */

DECLARE_DUMPER_COMPUTE(DumperBlackDynamite);

__END_LIBMULTISCALE__
