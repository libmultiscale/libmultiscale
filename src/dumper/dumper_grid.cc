/**
 * @file   dumper_grid.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This dumper allows to use a grid so as to reduce on a per-cell basis
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "dumper_grid.hh"
#include "cube.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "lm_file.hh"
#include "ref_point_data.hh"
#include <iomanip>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

template <typename Cont> void DumperGrid::dump(Cont &cont) {

  switch (field) {
  case _displacement:
    FillGrid<_displacement>(cont);
    break;
  case _velocity:
    FillGrid<_velocity>(cont);
    break;
  case _force:
    FillGrid<_force>(cont);
    break;
  default:
    LM_FATAL("cannot treat this field");
  }

  std::stringstream fname;
  fname << this->getBaseName() << "_grid." << std::setfill('0') << std::setw(4)
        << this->action_step << ".txt";
  LMFile file(fname.str());
  // std::vector<std::vector<Tensor> *> &blocks = grid->getBlocks();

  LM_TOIMPLEMENT;
  // Real x = 0, y = 0, z = 0;
  // std::vector<Real> force_x;
  // for (UInt i = 0; i < grid->getSize(); ++i)
  //   if (blocks[i]) {
  //     grid->index2Coordinates(i, x, y, z);
  //     Tensor t;
  //     t = grid->extractBoxValue(i, op);

  //     file.printf(
  //         "%d\t%.15e\t%.15e\t%.15e\t%.15e\t%.15e\t%.15e\t%.15e\t%.15e\t%.15e\n",
  //         i, x, y, z, t[0], t[1], t[2], t[3], t[4], t[5]);
  //   }
  // file.close();
  // delete (grid);
  // grid = NULL;
}
/* -------------------------------------------------------------------------- */

template <FieldType type, typename Cont> void DumperGrid::FillGrid(Cont &cont) {

  if (cont.size() == 0)
    return;

  LM_TOIMPLEMENT;

  // Cube &cube = cont.getBoundingBox();
  // constexpr UInt Dim = Cont::Dim;

  // for (UInt i = 0; i < Dim; ++i) {
  //   if (gridspace[i] == -1 && griddivision[i] == UINT_MAX)
  //     LM_FATAL("GRIDSPACE or GRIDDIVISION keyword should be used for
  //     direction "
  //              << i);
  // }

  // Vector<Dim> gridspace2 = gridspace.block<Dim,1>(0,0);
  // Vector<Dim> griddivision2 = griddivision.block<Dim,1>(0,0);

  // grid =
  //     new SpatialGridPoint<Tensor, Cont::Dim>(cube, gridspace2,
  //     griddivision2);

  // for (auto &&at : cont) {
  //   Vector<Cont::Dim> x = at.position0();
  //   if (current_flag)
  //     x = at.position();

  //   auto tmp = at.template field<type>();
  //   Tensor t;
  //   for (UInt i = 0; i < Dim; ++i)
  //     t[i] = tmp[i];
  //   grid->addElement(t, x);
  // }

  // DUMP("we have as average " << grid->getAverageEltByBlock()
  //                            << " points per block",
  //      DBG_MESSAGE);
}

/* -------------------------------------------------------------------------- */

/* LMDESC GRID
   This dumper allows to use a grid so as to reduce on a per-cell basis.
   A point/nodes based sorting into a spatial grid is used.
   It admits several options that could be model dependent.
*/
/* LMEXAMPLE DUMPER GRID GRIDSPACE 10 10 10 PREFIX /home/titeuf */
/* LMHERITANCE dumper_interface */

inline void DumperGrid::declareParams() {
  DumperInterface::declareParams();

  /* LMKEYWORD GRID_DIVISIONX
     Set the number of grid cells in x direction
  */
  this->parseKeyword("GRID_DIVISIONX", griddivision[0]);
  /* LMKEYWORD GRID_DIVISIONY
     Set the number of grid cells in y direction
  */
  this->parseKeyword("GRID_DIVISIONY", griddivision[1]);
  /* LMKEYWORD GRID_DIVISIONZ
     Set the number of grid cells in x direction
  */
  this->parseKeyword("GRID_DIVISIONZ", griddivision[2]);
  /* LMKEYWORD GRID_DIVISION
     Allows to set the number of grid cells in all directions of
     space, see GRID\_DIVISION\_X/Y/Z
  */
  this->parseVectorKeyword("GRID_DIVISION", 3, griddivision);

  /* LMKEYWORD GRIDSPACEX
     Set the cell size in the X direction for the grid generation
  */
  this->parseKeyword("GRIDSPACEX", gridspace[0]);
  /* LMKEYWORD GRIDSPACEY
     Set the cell size in the Y direction for the grid generation
  */
  this->parseKeyword("GRIDSPACEY", gridspace[1]);
  /* LMKEYWORD GRIDSPACEZ
     Set the cell size in the Z direction for the grid generation
  */
  this->parseKeyword("GRIDSPACEZ", gridspace[2]);
  /* LMKEYWORD GRIDSPACE
     The implementation of the Bridging domain method needs a match
     between elements and atoms to be made at initialisation of the
     bridging zones. This could be a very costful operation. \\
     In order to break the complexity of such a stage, the
     elements are first stored in a grid for which
     number of cells (in each direction) are specified by this keyword.
   */
  this->parseVectorKeyword("GRIDSPACE", 3, gridspace);
  /* LMKEYWORD OPERATOR
     Specify the operator to reduce the information per cell \\
     The possible operators are for now:

     - SUM: perform a sum per cell :math:`\sum_{i \in cell} v_i`
     - AVERAGE: perform an average per cell :math:`\frac{1}{\#cell}\sum_{i \in
       cell} v_i`
     - DENSITY: perform the volumetric average :math:`\overline{v} =
       \frac{1}{vol(cell)}\sum_{i \in cell} v_i`
     - DEVIATION: compute the standard deviation :math:`\frac{1}{\#cell}\sum_{i
       \in cell} (v_i - \overline{v} )^2`
     - MAX: compute the max value
     - MIN: compute the min value
  */
  this->parseKeyword("OPERATOR", op);

  /* LMKEYWORD CURRENT_POSITION
     All the points/atoms/nodes are sorted in a spatial grid. By default the
     initial position of
     these points/atoms/nodes are considered. This flag allows to consider
     current positions instead.
   */
  this->parseTag("CURRENT_POSITION", current_flag);

  /* LMKEYWORD FIELD
     Request to perform the grid reduction on the provided field
  */
  this->parseKeyword("FIELD", field);
}

/* -------------------------------------------------------------------------- */

DumperGrid::DumperGrid(const std::string &name) : LMObject(name) {
  current_flag = 0;
  for (UInt i = 0; i < 3; ++i)
    gridspace[i] = -1.;
  for (UInt i = 0; i < 3; ++i)
    griddivision[i] = UINT_MAX;

  op = OP_AVERAGE;
  // grid = NULL;
}

/* -------------------------------------------------------------------------- */

DumperGrid::~DumperGrid() {}

/* -------------------------------------------------------------------------- */

DECLARE_DUMPER_MAKE_CALL(DumperGrid)

__END_LIBMULTISCALE__
