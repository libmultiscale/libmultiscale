/**
 * @file   dumper_epot.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Nov 25 15:05:56 2013
 *
 * @brief  This dumper outputs to a single file potential energy informations
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_DUMPER_EPOT_HH__
#define __LIBMULTISCALE_DUMPER_EPOT_HH__
/* -------------------------------------------------------------------------- */
#include "dumper_interface.hh"
// #include "dumper_helper_sql.hh"
// #include "../common/domain_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/** Class DumperEPot
 * Implementation of a dumper for kinetik energy
 */

class DumperEPot : public DumperInterface {

public:
  DECLARE_DUMPER(DumperEPot, _or<MD>);

  //! most generic dump function
  template <typename _Input> void dump(_Input &cont);
  //! initialisation of the dumper (do nothing)
  void init() override;
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_DUMPER_EPOT_HH__ */
