/**
 * @file   dumper_1d.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  Dumps the per point information for 1D domains
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "dumper_1d.hh"
#include "compute_interface.hh"
#include "factory_multiscale.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "ref_point_data.hh"
#include <iomanip>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

template <typename Cont> void Dumper1D::dump(Cont &cont) {

  constexpr UInt Dim = Cont::Dim;

  if (Dim != 1)
    LM_FATAL("cannot apply this dumper to model of dimension greater than 1 "
             << Dim);

  if (!file.is_open()) {
    std::stringstream str;
    str << this->getBaseName() << "-" << lm_my_proc_id << ".plot";
    file.open(str.str().c_str());
  }

  if (filter != invalidFilter) {
    auto &comp =
        FilterManager::getManager().getCastedObject<ComputeInterface>(filter);
    if (!do_not_rebuild_flag)
      comp.compute();
    LM_TOIMPLEMENT;
    // if (ptr->size() / ptr->getDim() != cont.size())
    //   LM_FATAL("compute and container mismatch of number of components "
    //            << ptr->size() / ptr->getDim() << " " << cont.size());
    dumpCompute<Dim>(comp, cont);
  } else
    switch (field_type) {
    case _displacement:
      dumpField<_displacement, Dim>(cont);
      break;
    case _position:
      dumpField<_position, Dim>(cont);
      break;
    case _velocity:
      dumpField<_velocity, Dim>(cont);
      break;
    case _force:
      dumpField<_force, Dim>(cont);
      break;
    default:
      LM_FATAL("field not eligible " << field_type);
    }
}
/* -------------------------------------------------------------------------- */

template <UInt Dim, typename ContCompute, typename Cont>
void Dumper1D::dumpCompute(ContCompute &, Cont &) {

  LM_TOIMPLEMENT;
  // if (contCompute.getDim() > 2)
  //   LM_FATAL("result of compute must have < 3 columns "
  //            << contCompute.getDim());

  // if (contCompute.getDim() == 1) {
  //   for (UInt i = 0; i < contCompute.size(); ++i) {
  //     file << std::scientific << std::setprecision(precision)
  //          << contCompute.get(i) << "\t" << cont.get(i).position0() << "\t";
  //   }
  // } else if (contCompute.getDim() == 2) {
  //   for (UInt i = 0; i < contCompute.size() / 2; ++i) {
  //     file << std::scientific << std::setprecision(precision)
  //          << contCompute.get(2 * i) << "\t" << contCompute.get(2 * i + 1)
  //          << "\t";
  //   }
  // }
  // file << std::endl;
}

/* -------------------------------------------------------------------------- */

template <FieldType ftype, UInt Dim, typename Cont>
void Dumper1D::dumpField(Cont &cont) {

  for (auto &&at : cont) {
    auto f = at.template field<ftype>();
    file << std::scientific << std::setprecision(precision) << f << "\t"
         << at.position0() << "\t";
  }
  file << std::endl;
}

/* -------------------------------------------------------------------------- */

/* LMDESC DUMPER1D
   Dumps the per point information such as position,displacement,velocity and
   force
   for 1D domains.
*/

/* LMEXAMPLE DUMPER dmd1d DUMPER1D INPUT md FREQ 100 PREFIX ./ FIELD position */

/* LMHERITANCE dumper_interface */

void Dumper1D::declareParams() {

  DumperInterface::declareParams();

  /* LMKEYWORD FIELD
     Specify the field to extract
  */
  this->parseKeyword("FIELD", field_type);

  /* LMKEYWORD FILTER
     specify what to put with positions
  */
  this->parseKeyword("FILTER", filter, invalidFilter);

  /* LMKEYWORD PRECISION
     The generated file will use the provided precision.
  */
  this->parseKeyword("PRECISION", precision, 15u);

  /* LMKEYWORD DONOTREBUILD
     the default is to recompute
     You can ask not to recompute the attached filter so that
     when another instance ask for the compute at another time the release
     number will be ignored
  */
  this->parseTag("DONOTREBUILD", do_not_rebuild_flag, false);
}

/* -------------------------------------------------------------------------- */

Dumper1D::Dumper1D(const std::string &name) : LMObject(name) {
  filter = invalidFilter;
  precision = 15;
  do_not_rebuild_flag = false;
}

/* -------------------------------------------------------------------------- */

Dumper1D::~Dumper1D() {}

/* -------------------------------------------------------------------------- */

DECLARE_DUMPER_MAKE_CALL(Dumper1D)

__END_LIBMULTISCALE__
