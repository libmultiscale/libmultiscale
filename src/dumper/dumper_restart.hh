/**
 * @file   dumper_restart.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Oct 29 22:09:05 2013
 *
 * @brief  This dumper saves the state of the simulation
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_DUMPER_RESTART_HH__
#define __LIBMULTISCALE_DUMPER_RESTART_HH__
/* -------------------------------------------------------------------------- */
#include "base64_writer.hh"
#include "dumper_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class DumperRestart : public DumperInterface {

public:
  DECLARE_DUMPER(DumperRestart, _or<MD, CONTINUUM>);

  //! dump function for meshes
  template <typename _Input> enable_if_mesh<_Input> dump(_Input &cont);
  //! dump function for md
  template <typename _Input> enable_if_md<_Input> dump(_Input &cont);

  void initVectors(CommGroup group);

protected:
  //! prUInt the headers in a given file for the xml restart format
  void printHeaders(UInt nbDofs, const UInt Dim, LMFile &file);

  //! prUInt the headers in a given file for the xml restart format
  void printTail(LMFile &file);

  //! make coms and dump to file with little communication overlapping
  template <UInt Dim>
  void DumpField(const std::string &fieldName, LMFile &file, CommGroup group);

protected:
  //! local number of dofs
  UInt nb_local_dofs;
  using BufferType = std::vector<Real>;
  //! local data
  BufferType data;
  //! local number of Dofs per proc. Only valid for root (rank = 0) processor
  std::vector<UInt> nb_local_per_proc;
  //!  buffer data used to gather information on the one that will write to
  //!  disk. Only valid for root
  std::vector<BufferType> data_per_proc;

  //! flag to know if a binary or text dump will be done
  bool text_flag;
  //! flag to know if the angular velocity must be dumped
  bool angular_velocity;
  //! flag to know if the timestep must be dumped
  bool timestep0;

  //! helper for base64
  Base64Writer b64;
};
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_DUMPER_RESTART_HH__ */
