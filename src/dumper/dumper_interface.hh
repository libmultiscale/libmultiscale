/**
 * @file   dumper_interface.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Apr 03 23:50:25 2013
 *
 * @brief  The interface shared by all dumpers
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_DUMPER_INTERFACE_HH__
#define __LIBMULTISCALE_DUMPER_INTERFACE_HH__
/* -------------------------------------------------------------------------- */
#include "action_interface.hh"
extern std::string lm_release_info;
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/** Class DumperInterface.
 * Interface class to all dumpers
 */

class DumperInterface : public ActionInterface {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  DumperInterface();

  virtual ~DumperInterface(){};

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  //! set parameters
  virtual void declareParams();
  //! construct the name of the files to be generated
  void buildNameFile();
  //! init stuff
  virtual void init();
  const std::string &getBaseName();
  //! call for immediate dump
  void dump();

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */
protected:
  //! flag to set if no action required from dumper
  bool nothing_to_do_flag;
  //! data stored by step
  std::map<std::string, std::vector<Real>> by_step_data;
  //! unit system in which to output the dump data, not used by all dumpers
  UnitSystem dump_unit_system;
  //! domain id to apply stimulator on
  UInt domain_id;

  //! prefix for directory
  std::string prefix;
  //! base name of the filename
  std::string base_name;
};
/* -------------------------------------------------------------------------- */

#define DECLARE_DUMPER(class_name, ...)                                        \
  class_name(const std::string &name);                                         \
  virtual ~class_name();                                                       \
  DECORATE_FUNCTION_DISPATCH(dump, __VA_ARGS__)                                \
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW                                              \
                                                                               \
  void declareParams() override;                                               \
  void compute_make_call() override

#define DECLARE_DUMPER_MAKE_CALL(class_name)                                   \
  void class_name::compute_make_call() {                                       \
    this->dump<dispatch>(this->getInput());                                    \
  }

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_DUMPER_INTERFACE_HH__ */
