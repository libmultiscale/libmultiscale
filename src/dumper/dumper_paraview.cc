/**
 * @file   dumper_paraview.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This dumper allows to generate paraview files for vizualization
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "dumper_paraview.hh"
#include "compute_interface.hh"
#include "factory_multiscale.hh"
#include "field.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */
#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <iomanip>
#include <sys/stat.h>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

DumperParaview::DumperParaview(const std::string &name)
    : LMObject(name), dep_field(false), p0_field(false), vel_field(false),
      force_field(false), external_force_field(false),
      internal_force_field(false), acceleration_field(false), proc_field(false),
      stress_field(false), strain_field(false), epot_field(false),
      mass_field(false), charge_field(false), id_field(false),
      flag_compressed(false), flag_text(false), flag_base64(false),
      e_density_field(false), barycenters_field(false),
      temperature_field(false), temperatureVar_field(false),
      heatCapacity_field(false), heatRate_field(false),
      external_heatRate_field(false), tag_field(false), boundary_field(false),
      burgers_field(false), normal_field(false), torque_field(false),
      angular_velocity_field(false), angular_acceleration_field(false),
      radius_field(false) {}

/* -------------------------------------------------------------------------- */

DumperParaview::~DumperParaview() {}

/* -------------------------------------------------------------------------- */
void DumperParaview::init() {
  DumperInterface::init();
  if (flag_compressed)
    paraHelper.setMode(ParaviewHelper::COMPRESSED);
  if (flag_text)
    paraHelper.setMode(ParaviewHelper::TEXT);
  if (flag_base64)
    paraHelper.setMode(ParaviewHelper::BASE64);

  if (dep_field == true)
    this->activated_fields.push_back(_displacement);
  if (p0_field == true)
    this->activated_fields.push_back(_position0);
  if (vel_field == true)
    this->activated_fields.push_back(_velocity);
  if (force_field == true)
    this->activated_fields.push_back(_force);
  if (internal_force_field == true)
    this->activated_fields.push_back(_internal_force);
  if (external_force_field == true)
    this->activated_fields.push_back(_external_force);
  if (acceleration_field == true)
    this->activated_fields.push_back(_acceleration);
  if (stress_field == true)
    this->activated_fields.push_back(_stress);
  if (strain_field == true)
    this->activated_fields.push_back(_strain);
  if (epot_field == true)
    this->activated_fields.push_back(_epot);
  if (mass_field == true)
    this->activated_fields.push_back(_mass);
  if (charge_field == true)
    this->activated_fields.push_back(_charge);
  if (id_field == true)
    this->activated_fields.push_back(_id);
  if (temperature_field == true)
    this->activated_fields.push_back(_temperature);
  if (tag_field == true)
    this->activated_fields.push_back(_tag);
  if (boundary_field == true)
    this->activated_fields.push_back(_boundary);
  if (burgers_field == true)
    this->activated_fields.push_back(_burgers);
  if (normal_field == true)
    this->activated_fields.push_back(_normal);
  if (torque_field == true)
    this->activated_fields.push_back(_torque);
  if (angular_velocity_field == true)
    this->activated_fields.push_back(_angular_velocity);
  if (angular_acceleration_field == true)
    this->activated_fields.push_back(_angular_acceleration);
  if (radius_field == true)
    this->activated_fields.push_back(_radius);
}

/* -------------------------------------------------------------------------- */
/* LMDESC PARAVIEW
   This dumper ouputs paraview files from any domain description.
   It admits several options that could be model dependent.
*/
/* LMEXAMPLE DUMPER para PARAVIEW INPUT md PROC DISP PREFIX /home/titeuf */
/* LMHERITANCE dumper_interface */

void DumperParaview::declareParams() {
  DumperInterface::declareParams();

  /* LMKEYWORD FIELD
     Request to activate a field
   */
  this->parseKeyword("FIELD", activated_fields, std::vector<FieldType>{});
  /* LMKEYWORD PROC
     Flag to request processor number to each DOF
   */
  this->parseTag("PROC", proc_field, false);
  /* LMKEYWORD DISP
     Flag to request displacements stored at each DOF
  */
  this->parseTag("DISP", dep_field, false);
  /* LMKEYWORD ELECTRONIC_DENSITY
     Flag to request electronic density (for EAM systems) stored at each atom
  */
  this->parseTag("ELECTRONIC_DENSITY", e_density_field, false);
  /* LMKEYWORD P0
     Flag to request output of initial position stored at each DOFs
  */
  this->parseTag("P0", p0_field, false);
  /* LMKEYWORD TEMPERATURE
     Only for continuum domains.
     Flag to request output of temperature stored at each DOFs.
  */
  this->parseTag("TEMPERATURE", temperature_field, false);
  /* LMKEYWORD TEMPERATURE_VARIATION
     Only for continuum domains. Flag to request the output of the temperature
     variation
     at each DOFs.
  */
  this->parseTag("TEMPERATURE_VARIATION", temperatureVar_field, false);
  /* LMKEYWORD HEAT_CAPACITY
     return the heat capacity associated with nodal DOF. This
     is applicable only for FE domains.
  */
  this->parseTag("HEAT_CAPACITY", heatCapacity_field, false);
  /* LMKEYWORD HEAT_RATE
     return the heat rate (residual) associated with nodal DOF. This
     is applicable only for FE domains.
  */
  this->parseTag("HEAT_RATE", heatRate_field, false);
  /* LMKEYWORD EXTERNAL_HEAT_RATE
     return the external applied heat rate associated with nodal DOF. This
     is applicable only for FE domains.
  */
  this->parseTag("EXTERNAL_HEAT_RATE", external_heatRate_field, false);

  /* LMKEYWORD VEL
     Flag to request output of velocity stored at each DOFs
  */
  this->parseTag("VEL", vel_field, false);
  /* LMKEYWORD FORCE
     Flag to request output of force stored at each DOFs
  */
  this->parseTag("FORCE", force_field, false);
  /* LMKEYWORD ACCELERATION
   Flag to request output of force stored at each DOFs
  */
  this->parseTag("ACCELERATION", acceleration_field, false);
  /* LMKEYWORD INTERNAL_FORCE
     Flag to request output of applied force stored at each DOFs
  */
  this->parseTag("INTERNAL_FORCE", internal_force_field, false);
  /* LMKEYWORD EXTERNAL_FORCE
   Flag to request output of applied force stored at each DOFs
*/
  this->parseTag("EXTERNAL_FORCE", external_force_field, false);
  /* LMKEYWORD STRESS
     Flag to request output of stress field. This is implemented only for atomic
     systems where the stress is computed using LAMMPS compute\_stress object.
  */
  this->parseTag("STRESS", stress_field, false);
  /* LMKEYWORD STRAIN
     Flag to request output of strain field. This is implemented only for FE
     systems.
  */
  this->parseTag("STRAIN", strain_field, false);
  /* LMKEYWORD EPOT
     Flag to request the output of the potential energy associated to each
     degree of freedom. Only for MD domains.
  */
  this->parseTag("EPOT", epot_field, false);
  /* LMKEYWORD MASS
     Flag to request output of mass stored to each degree of freedom. Only
     applicable
     to MD and FE systems.
  */
  this->parseTag("MASS", mass_field, false);
  /* LMKEYWORD CHARGE
     Flag to request the output of the charge associated to each degree of
     freedom.
     Only for MD domains
  */
  this->parseTag("CHARGE", charge_field, false);
  /* LMKEYWORD ID
     Flag to request output of internal id associated to each degree of freedom.
  */
  this->parseTag("ID", id_field, false);

  /* LMKEYWORD COMPRESSED
     Flag to request output in compressed mode.
  */
  this->parseTag("COMPRESSED", flag_compressed, false);

  /* LMKEYWORD TEXT
     The sections in XML paraview files are set to be written in simple TEXT
     encoding.
  */
  this->parseTag("TEXT", flag_text, false);

  /* LMKEYWORD BASE64
     The sections in XML paraview files are set to be written in base64
     encoding.
  */
  this->parseTag("BASE64", flag_base64, false);

  /* LMKEYWORD TAG
     provide the tag of the degree of freedom (for usage with lammps)
  */

  this->parseTag("TAG", tag_field, false);

  /* LMKEYWORD BOUNDARY
     flag to request output of nodal blockage
  */

  this->parseKeyword("BOUNDARY", boundary_field, false);

  /* LMKEYWORD ADDFIELD
     provide the id of compute as an additional field
  */

  this->parseKeyword("ADDFIELD", additional_fields, std::vector<LMID>{});

  /* LMKEYWORD BURGERS
     Flag to request the output of the BURGERS to each degree of
     freedom.
     Only for DD domains
  */

  this->parseKeyword("BURGERS", burgers_field, false);

  /* LMKEYWORD NORMAL
     Flag to request the output of the NORMALS to each degree of
     freedom.
  */

  this->parseKeyword("NORMAL", normal_field, false);

  /* LMKEYWORD ANGULAR_VELOCITY
     Flag to request the output of the angular velocity to each degree of
     freedom.
  */

  this->parseKeyword("ANGULAR_VELOCITY", angular_velocity_field, false);

  /* LMKEYWORD TORQUE
     Flag to request the output of the torque to each degree of
     freedom.
  */

  this->parseKeyword("TORQUE", torque_field, false);

  /* LMKEYWORD RADIUS
     Flag to request the output of the radius to each degree of
     freedom.
  */

  this->parseKeyword("RADIUS", radius_field, false);
}

/* -------------------------------------------------------------------------- */

void DumperParaview::openPVTU() {
  std::stringstream fname;
  fname << this->getBaseName() << "_pvf" << std::setfill('0') << std::setw(4)
        << this->action_step << ".pvtu";
  /* ouverture du fichier parallele vtk */
  file.open(fname.str());
  paraHelper.setOutputFile(file);
}
/* -------------------------------------------------------------------------- */

void DumperParaview::openVTU() {
  std::string dirname;
  dirname += this->prefix + "/" + this->getID() + "-VTUs";
  mkdir(dirname.c_str(), 0755);
  std::stringstream fname;
  fname << dirname << "/" << this->getID() << "_pvf" << std::setfill('0')
        << std::setw(4) << this->action_step << ".proc" << std::setfill('0')
        << std::setw(4) << lm_my_proc_id << ".vtu";

  file.open(fname.str(), "wb+", flag_compressed);
  paraHelper.setOutputFile(file);
}

/* -------------------------------------------------------------------------- */
template <typename Cont, typename Func>
decltype(auto) DumperParaview::foreach_activated_fields(Cont &cont,
                                                        Func &&func) {
  std::vector<FieldType> treated_fields;
  Cont::Ref::fields::foreach ([&](auto &&a) {
    constexpr auto field_type = std::decay_t<decltype(a)>::value;
    auto it = std::find(this->activated_fields.begin(),
                        this->activated_fields.end(), field_type);
    if (it != this->activated_fields.end()) {
      auto _field = make_field<field_type>(cont);
      func(_field);
      treated_fields.push_back(field_type);
    }
  });

  for (auto &&_f : this->activated_fields) {
    auto it = std::find(treated_fields.begin(), treated_fields.end(), _f);
    if (it == treated_fields.end()) {
      DUMP("Cannot use field " << _f
                               << " as it is not accessible from component "
                               << cont.getID() << " of type "
                               << demangle(typeid(typename Cont::Ref).name()),
           DBG_WARNING);
    }
  }
  return treated_fields;
}

/* -------------------------------------------------------------------------- */

template <typename ActivatedFields, typename Cont, typename Func>
auto apply_functor_on_activated_elemental_fields(ActivatedFields &afields,
                                                 Cont &cont, Func &&func) {
  std::vector<FieldType> treated_fields;
  Cont::RefElem::fields::foreach ([&](auto &&a) {
    constexpr auto field_type = std::decay_t<decltype(a)>::value;
    auto it = std::find(afields.begin(), afields.end(), field_type);
    if (it != afields.end()) {
      auto _field = make_field<field_type>(cont.getContainerElems());
      func(_field);
      treated_fields.push_back(field_type);
    }
  });
  return treated_fields;
}

/* -------------------------------------------------------------------------- */

template <typename Cont>
enable_if_component<Cont> DumperParaview::dump(Cont &cont) {
  this->dump<dispatch>(cont.evalOutput());
}

/* -------------------------------------------------------------------------- */

template <typename Cont>
enable_if_point<Cont> DumperParaview::dump(Cont &cont) {

  constexpr UInt Dim = Cont::Dim;
  UInt nb = cont.size();

  CommGroup group = cont.getCommGroup();
  if (!group.amIinGroup())
    return;

  UInt my_rank = group.getMyRank();
  UInt group_size = group.size();

  if (my_rank == 0) {

    openPVTU();

    file.printf("<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" ");
    file.printf("byte_order=\"LittleEndian\">\n");
    file.printf("<PUnstructuredGrid GhostLevel=\"0\">\n");

    file.printf("<PPointData>\n");

    foreach_activated_fields(cont, [&](auto field) {
      paraHelper.PDataArray(field.getName(), field.getDim());
    });

    foreach_additional_field(
        [&](const std::string &name, ContainerArray<Real> &array) {
          paraHelper.PDataArray(name, array.getDim());
        });

    file.printf("</PPointData>");
    file.printf("<PPoints><PDataArray type=\"Float64\" "
                "NumberOfComponents=\"3\" format=\"ascii\"/></PPoints>");

    if (group_size > 1)
      for (UInt l = 0; l < group_size; ++l) {
        file.printf("<Piece Source=\"%s-VTUs/%s_pvf%.4d.proc%.4d.vtu\"/>\n",
                    this->getID().c_str(), this->getID().c_str(),
                    this->action_step, group[l].getAbsoluteRank());
      }
    else
      file.printf("<Piece Source=\"%s-VTUs/%s_pvf%.4d.proc%.4d.vtu\"/>\n",
                  this->getID().c_str(), this->getID().c_str(),
                  this->action_step, lm_my_proc_id);

    file.printf("</PUnstructuredGrid></VTKFile>");
    file.close();
  }

  openVTU();

  if (nb)
    paraHelper.write_header(nb, 1);
  else
    paraHelper.write_header(nb, 0);

  UnitsConverter unit;
  unit.setInUnits(code_unit_system);
  unit.setOutUnits(this->dump_unit_system);
  unit.computeConversions();

  paraHelper.startDofList();

  for (auto &&at : cont) {
    auto X = at.position0();
    for (UInt i = 0; i < Dim; ++i)
      paraHelper.pushReal(X[i] * unit.getConversion<Length>());
    for (UInt i = Dim; i < 3; ++i)
      paraHelper.pushReal(0.0);
  }

  paraHelper.endDofList();
  paraHelper.startCellsConnectivityList();

  for (UInt i = 0; i < nb; ++i) {
    paraHelper.pushInteger(i);
  }

  paraHelper.endCellsConnectivityList();
  paraHelper.startCellsoffsetsList();

  paraHelper.pushInteger(nb);

  paraHelper.endCellsoffsetsList();
  paraHelper.startCellstypesList();

  if (nb) {
    paraHelper.pushInteger(2);
  }

  paraHelper.endCellstypesList();

  paraHelper.startPointDataList();

  foreach_activated_fields(cont, [&](auto field) {
    auto dim = field.getDim();
    auto name = field.getName();
    paraHelper.startData(name, dim);

    for (auto &&v : field) {
      if (dim != v.size())
        LM_FATAL("not compatible vector");
      auto view = Eigen::Map<Eigen::VectorXd>(v.data(), dim);
      paraHelper.push(view, dim);
    }

    paraHelper.endData();
  });

  foreach_additional_field(
      [&](const std::string &name, ContainerArray<Real> &array) {
        dumpField(name, cont, array, array.getDim());
      });

  paraHelper.endPointDataList();
  paraHelper.write_conclusion();

  file.close();
}
/* -------------------------------------------------------------------------- */

template <typename Cont> enable_if_mesh<Cont> DumperParaview::dump(Cont &cont) {

  constexpr UInt Dim = Cont::Dim;

  foreach_additional_field([&](const std::string &, ContainerArray<Real> &) {});

  CommGroup group = cont.getCommGroup();
  if (!group.amIinGroup())
    throw;

  UInt my_rank = group.getMyRank();
  UInt group_size = group.size();

  if (my_rank == 0) {

    openPVTU();

    file.printf("<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" ");
    file.printf("byte_order=\"LittleEndian\">\n");

    file.printf("<PUnstructuredGrid GhostLevel=\"1\">\n");

    file.printf("<PPointData>\n");

    auto treated_fields = foreach_activated_fields(cont, [&](auto field) {
      paraHelper.PDataArray(field.getName(), field.getDim());
    });

    foreach_additional_field(
        [&](const std::string &name, ContainerArray<Real> &array) {
          paraHelper.PDataArray(name, array.getDim());
        });

    file.printf("</PPointData>");

    file.printf("<PCellData>\n");
    auto treated_fields_elems = apply_functor_on_activated_elemental_fields(
        activated_fields, cont, [&](auto field) {
          paraHelper.PDataArray(field.getName(), field.getDim());
        });

    treated_fields.insert(treated_fields.end(), treated_fields_elems.begin(),
                          treated_fields_elems.end());

    for (auto &&f : activated_fields) {
      auto it = std::find(treated_fields.begin(), treated_fields.end(), f);
      if (it == treated_fields.end())
        LM_FATAL("Field " << f
                          << " is not produced by container: " << cont.getID());
    }

    file.printf("</PCellData>\n");
    file.printf("<PPoints><PDataArray type=\"Float64\" "
                "NumberOfComponents=\"3\" format=\"ascii\"/></PPoints>");

    if (group_size > 1)
      for (UInt l = 0; l < group_size; ++l) {
        file.printf("<Piece Source=\"%s-VTUs/%s_pvf%.4d.proc%.4d.vtu\"/>\n",
                    this->getID().c_str(), this->getID().c_str(),
                    this->action_step, l);
      }
    else
      file.printf("<Piece Source=\"%s-VTUs/%s_pvf%.4d.proc%.4d.vtu\"/>\n",
                  this->getID().c_str(), this->getID().c_str(),
                  this->action_step, lm_my_proc_id);

    file.printf("</PUnstructuredGrid></VTKFile>");
    file.close();
  }

  openVTU();

  auto &contElems = cont.getContainerElems();
  auto &contNodes = cont.getContainerNodes();

  UInt nb = contNodes.size();
  UInt sizes = contElems.size();

  paraHelper.write_header(nb, sizes);

  UnitsConverter unit;
  unit.setInUnits(code_unit_system);
  unit.setOutUnits(this->dump_unit_system);
  unit.computeConversions();

  paraHelper.startDofList();

  for (auto &&n : contNodes) {
    Vector<Dim> X = n.position0();
    for (UInt i = 0; i < Dim; ++i)
      paraHelper.pushReal(X[i] * unit.getConversion<Length>());
    for (UInt i = Dim; i < 3; ++i)
      paraHelper.pushReal(0.0);
  }

  paraHelper.endDofList();

  paraHelper.startCellsConnectivityList();

  std::vector<UInt> nodes;

  for (auto &&el : contElems) {
    if (el.isAltered())
      nodes = el.getAlteredConnectivity();
    else
      nodes = el.localIndexes();

    UInt nd_sz = nodes.size();
    if (nd_sz == 10)
      nd_sz = 4;
    for (UInt j = 0; j < nd_sz; ++j) {
      paraHelper.pushInteger(nodes[j]);
    }
  }

  paraHelper.endCellsConnectivityList();
  paraHelper.startCellsoffsetsList();

  UInt counter = 0;

  for (auto &&el : contElems) {
    nodes = el.localIndexes();

    if (nodes.size() == 10)
      counter += 4;
    else
      counter += nodes.size();
    paraHelper.pushInteger(counter);
  }

  paraHelper.endCellsoffsetsList();
  paraHelper.startCellstypesList();

  UInt code_type = UINT_MAX;

  for (auto &&el : contElems) {
    nodes = el.localIndexes();
    switch (nodes.size()) {
    case 2:
      code_type = 3;
      break;
    case 3:
      code_type = 5;
      break;
    case 8:
      code_type = 12;
      break;
    case 4:
      code_type = 10;
      break;
    case 10:
      code_type = 10;
      break;
    default:
      LM_FATAL("unable to dump for VTK : " << nodes.size());
    }

    paraHelper.pushInteger(code_type);
  }

  paraHelper.endCellstypesList();

  paraHelper.startPointDataList();

  foreach_activated_fields(cont, [&](auto field) {
    auto dim = field.getDim();
    auto name = field.getName();
    paraHelper.startData(name, dim);

    for (auto &&v : field) {
      if (dim != v.size())
        LM_FATAL("not compatible vector");
      paraHelper.push(v, dim);
    }

    paraHelper.endData();
  });

  foreach_additional_field(
      [&](const std::string &name, ContainerArray<Real> &array) {
        dumpField(name, cont, array, array.getDim());
      });

  paraHelper.endPointDataList();

  paraHelper.startCellDataList();

  apply_functor_on_activated_elemental_fields(
      this->activated_fields, cont, [&](auto field) {
        auto dim = field.getDim();
        auto name = field.getName();
        paraHelper.startData(name, dim);

        for (auto &&v : field) {
          if (dim != v.size())
            LM_FATAL("not compatible vector");
          auto view = Eigen::Map<Eigen::VectorXd>(v.data(), dim);
          paraHelper.push(view, dim);
        }

        paraHelper.endData();
      });

  paraHelper.endCellDataList();
  paraHelper.write_conclusion();

  file.close();
}

/* ----------------------------------------------------------------------- */

// template <typename Cont> enable_if_dd<Cont> DumperParaview::dump(Cont &cont)
// {

//   constexpr UInt Dim = Cont::Dim;

//   auto &cNodes = cont;

//   CommGroup group = cont.getCommGroup();
//   UInt my_rank = group.getMyRank();
//   UInt group_size = group.size();

//   if (my_rank == 0) {

//     openPVTU();

//     file.printf("<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\"");
//     file.printf("byte_order=\"LittleEndian\">\n");
//     file.printf("<PUnstructuredGrid GhostLevel=\"0\">\n");

//     file.printf("<PPointData>\n");

//     APPLY_FUNCTOR_ON_ACTIVATED_FIELDS([&](auto field) {
//       paraHelper.PDataArray(field.getName(), field.getDim());
//     });

//     file.printf("</PPointData>");
//     file.printf("<PPoints><PDataArray type=\"Float64\" "
//                 "NumberOfComponents=\"3\" format =\"ascii\"/></PPoints>");

//     if (group_size > 1)
//       for (UInt l = 0; l < group_size; ++l) {
//         file.printf("<Piece Source =\"%s-VTUs/%s_pvf%.4d.proc%.4d.vtu\"/>\n",
//                     this->getID().c_str(), this->getID().c_str(),
//                     this->action_step, l);
//       }
//     else
//       file.printf("<Piece Source =\"%s-VTUs/%s_pvf%.4d.proc%.4d.vtu\"/>\n",
//                   this->getID().c_str(), this->getID().c_str(),
//                   this->action_step, lm_my_proc_id);

//     file.printf("</PUnstructuredGrid></VTKFile>");
//     file.close();
//   }

//   openVTU();

//   UInt nbrNodes = cNodes.size();
//   paraHelper.write_header(nbrNodes, 1);

//   UnitsConverter unit;
//   unit.setInUnits(code_unit_system);
//   unit.setOutUnits(code_unit_system);
//   unit.computeConversions();

//   std::vector<UInt> considered_indices;
//   std::vector<UInt> offsets;

//   paraHelper.startDofList();
//   std::vector<UInt> constraints;

//   for (auto &&nd : cNodes) {
//     auto nextNode = nd;
//     // UInt cnst = nd.getConstraint();
//     UInt idx = nd.getIndex();
//     considered_indices.push_back(idx);

//     Vector<Dim> X = nextNode.position() * unit.getConversion<Length>();
//     paraHelper.push(X, Dim);
//   }
//   paraHelper.endDofList();

//   paraHelper.startCellsConnectivityList();
//   for (UInt i = 0; i < nbrNodes; ++i) {
//     paraHelper.pushInteger(i);
//   }
//   if (offsets.size() == 0)
//     paraHelper.pushInteger(0);
//   paraHelper.endCellsConnectivityList();

//   paraHelper.startCellsoffsetsList();
//   paraHelper.pushInteger(nbrNodes + 1);
//   paraHelper.endCellsoffsetsList();

//   paraHelper.startCellstypesList();
//   if (nbrNodes) {
//     paraHelper.pushInteger(4);
//   }
//   paraHelper.endCellstypesList();

//   paraHelper.startPointDataList();

//   APPLY_FUNCTOR_ON_ACTIVATED_FIELDS([&](auto field) {
//     auto dim = field.getDim();
//     auto name = field.getName();
//     paraHelper.startData(name, dim);

//     for (auto &&v : field) {
//       paraHelper.push(v, dim);
//     }

//     paraHelper.endData();

//   });

//   // paraHelper.startData("kind", 1);
//   // UInt count = 0;
//   // for (auto &&nd : cNodes) {
//   //   if (nbrElems == 0) {
//   //     paraHelper.pushInteger(nd.getConstraint());
//   //   } else {
//   //     paraHelper.pushInteger(constraints[count]);
//   //     ++count;
//   //   }
//   // }
//   // paraHelper.endData();

//   paraHelper.endPointDataList();
//   paraHelper.write_conclusion();

//   file.close();
// }

/* ------------------------------------------------------------------------ */

template <typename F> void DumperParaview::foreach_additional_field(F &&f) {
  for (UInt i = 0; i < additional_fields.size(); ++i) {
    LMID additional_field = additional_fields[i];
    if (additional_field != invalidFilter) {
      auto out = getRegisteredOutput(additional_field);
      ContainerArray<Real> *ptr = nullptr;
      if (out.is_type<ComputeInterface>())
        ptr = &out.get<ComputeInterface>().evalArrayOutput();
      else if (out.is_type<ContainerArray<Real>>())
        ptr = &out.get<ContainerArray<Real>>();
      else
        LM_FATAL("This is a wrong additional field");
      f(additional_field, *ptr);
    }
  }
}

/* ------------------------------------------------------------------------ */
DECLARE_DUMPER_MAKE_CALL(DumperParaview)

/* --------------------------------------------------------------------------
 */

__END_LIBMULTISCALE__
