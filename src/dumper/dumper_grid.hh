/**
 * @file   dumper_grid.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Mon Nov 25 15:05:56 2013
 *
 * @brief  This dumper allows to use a grid so as to reduce on a per-cell basis
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_DUMPER_GRID_HH__
#define __LIBMULTISCALE_DUMPER_GRID_HH__
/* -------------------------------------------------------------------------- */
#include "dumper_interface.hh"
#include "spatial_grid_libmultiscale.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/** Class DumperGrid
 * Implementation of a dumper to pcontact vtu files
 */

class DumperGrid : public DumperInterface {

public:
  DECLARE_DUMPER(DumperGrid, _or<MD>);

  template <typename _Input> void dump(_Input &cont);
  template <FieldType type, typename _Input> void FillGrid(_Input &cont);

private:
  Quantity<Length, 3> gridspace;
  Vector<3, UInt> griddivision;
  FieldType field;
  bool current_flag;
  Operator op;

  //! lagrangian min value for the atoms
  // Real xmin[3];
  // //! lagrangian max value for the atoms
  // Real xmax[3];
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_DUMPER_GRID_HH__ */
