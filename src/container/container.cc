#include "container.hh"
#include "domain_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

//! copy the container info
void ContainerInterface::acquireContext(const LMObject &obj) {
  LMObject::acquireContext(obj);
  if (auto *ptr = dynamic_cast<const ContainerInterface *>(&obj)) {
    bounding_box = ptr->getBoundingBox();
    if (ptr->hasRefManager())
      this->setRefManager(ptr->getRefManager());
  }
  if (auto *ptr = dynamic_cast<const DomainInterface *>(&obj)) {
    bounding_box = ptr->getDomainBoundingBox();
  }
}
/* -------------------------------------------------------------------------- */

//! return the ReferenceManager
std::shared_ptr<ReferenceManagerInterface>
ContainerInterface::getRefManager() const {
  if (refmanager.expired())
    LM_THROW("cannot access undefined Reference Manager");
  return refmanager.lock();
}

/* -------------------------------------------------------------------------- */

bool ContainerInterface::hasRefManager() const { return !refmanager.expired(); }
/* -------------------------------------------------------------------------- */

void ContainerInterface::setRefManager(
    std::weak_ptr<ReferenceManagerInterface> refMgr) {
  refmanager = refMgr;
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
