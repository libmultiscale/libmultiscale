/**
 * @file   container.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Jul 22 14:47:56 2014
 *
 * @brief  This is the generic container class
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_CONTAINER_HH__
#define __LIBMULTISCALE_CONTAINER_HH__
/* -------------------------------------------------------------------------- */
#include "cube.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
class ReferenceManagerInterface;
template <typename T> class ContainerArray;
/* -------------------------------------------------------------------------- */

template <typename It> class FilteredIterator {
public:
  inline FilteredIterator(int dt, It it, It end) : dt(dt), it(it), end(end) {
    if (it == end)
      return;

    while (it != end and not((*it).getDOFType() & dt)) {
      ++it;
    }
  }

  void operator++() {
    if (it == end)
      return;
    ++it;
    while (it != end and not((*it).getDOFType() & dt)) {
      ++it;
    }
  };
  inline decltype(auto) operator*() { return *it; };
  inline bool operator!=(FilteredIterator &other) { return it != other.it; };

private:
  int dt;

  It it;
  It end;
};

template <typename Cont> class FilteredContainer {
public:
  inline FilteredContainer(int dt, Cont &cont) : dt(dt), cont(cont) {}

  inline decltype(auto) begin() { return make_it(cont.begin(), cont.end()); }
  inline decltype(auto) end() { return make_it(cont.end(), cont.end()); }

private:
  template <typename It> inline decltype(auto) make_it(It it, It end) {
    return FilteredIterator<It>(dt, it, end);
  }

  int dt;
  Cont &cont;
};
/* -------------------------------------------------------------------------- */

class ContainerInterface : public virtual LMObject {

public:
  ContainerInterface() : LMObject("anonymous"){};
  virtual ~ContainerInterface(){};

  //! return bounding box
  const Cube &getBoundingBox() const { return bounding_box; };
  //! return bounding box
  Cube &getBoundingBox() { return bounding_box; };
  //! ste bounding box
  void setBoundingBox(Cube &c) { bounding_box = c; };
  //! copy the container info
  virtual void acquireContext(const LMObject &obj) override;
  //! return the size of the container
  virtual UInt size() const = 0;
  //! return the number of items/rows of the container
  virtual UInt rows() const { return this->size(); };
  //! declare a subset of the current container
  template <typename T> inline void addSubSet(T &cont_subset);
  //! attach an object to the current container
  template <typename T> inline void attachObject(T &object);
  //! set the ReferenceManager
  void setRefManager(std::weak_ptr<ReferenceManagerInterface> refMgr);
  //! return the ReferenceManager
  std::shared_ptr<ReferenceManagerInterface> getRefManager() const;

  template <typename Daughter> decltype(auto) filter(int dt) {
    return FilteredContainer<Daughter>(dt, dynamic_cast<Daughter &>(*this));
  }

protected:
  //! checks if the container has a ref_manager
  bool hasRefManager() const;

  std::weak_ptr<ReferenceManagerInterface> refmanager;

private:
  Cube bounding_box;
};

/* -------------------------------------------------------------------------- */

/**
 * Interface Container<T>.
 * Generic container
 */

template <typename T> class Container_base : public ContainerInterface {

public:
  typedef ContainerArray<T> ContainerSubset;
  typedef T Ref;

  Container_base(){};
  virtual ~Container_base(){};

  //! sort contained items
  virtual void sort() { LM_TOIMPLEMENT; };
  //! search for a given item index
  virtual UInt search(T &) { LM_TOIMPLEMENT; };
  //! add a given item
  virtual void push_back(const T &) { LM_TOIMPLEMENT; };
  //! get an item from its index
  virtual T &get(UInt index) = 0;
  //! get an item from its index
  virtual T &operator[](UInt index) { return this->get(index); };
  //! remove an item from its index
  virtual void remove(UInt) { LM_TOIMPLEMENT; };
  //! return the number of items
  virtual UInt size() const = 0;
};

/* -------------------------------------------------------------------------- */

template <typename T> class iterator_base {

public:
  using value_type = T;
  using iterator_category = std::forward_iterator_tag;
  using difference_type = int;
  using pointer = T *;
  using reference = T &;

  iterator_base(Container_base<T> &cont) : iterator_base() {
    this->cont = &cont;
  }

  iterator_base() : cont(nullptr){};

  virtual ~iterator_base(){};

  virtual iterator_base &operator++() = 0;
  virtual T &operator*() = 0;

  virtual bool operator!=(const iterator_base &) const = 0;
  virtual bool operator==(const iterator_base &) const = 0;

protected:
  Container_base<T> *cont;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
#include "reference_manager_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
template <typename T> void ContainerInterface::addSubSet(T &cont_subset) {
  if (not this->hasRefManager())
    return;

  this->getRefManager()->addSubset(cont_subset);
}

/* -------------------------------------------------------------------------- */
template <typename T> void ContainerInterface::attachObject(T &object) {
  if (not this->hasRefManager())
    return;

  this->getRefManager()->attachObject(object, *this);
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_CONTAINER_HH__ */
