/**
 * @file   iterator_array_base_type.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Feb 19 20:58:22 2013
 *
 * @brief  Iterator over contiguous arrays
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_ITERATOR_ARRAY_BASE_TYPE_HH__
#define __LIBMULTISCALE_ITERATOR_ARRAY_BASE_TYPE_HH__
/* -------------------------------------------------------------------------- */

template <typename T> class ContainerArrayBaseType;

/**
 * Class IteratorArray
 * Generic iterator over array containers
 */
template <typename T> class IteratorArrayBaseType : public Iterator<T> {
public:
  //! type of associated container
  typedef Container<T, Iterator<T>> myContainer;
  //! type of parent in heritage
  typedef Iterator<T> Parent;

  IteratorArrayBaseType(ContainerArrayBaseType<T> &c)
      : Iterator<T>(c), index_courant(0), index_debut(0) {
    // TODO am�liorer les classe de container et iterator en d�finissant le type
    // T
    ContainerArrayBaseType<T> &cont =
        static_cast<ContainerArrayBaseType<T> &>(this->my_cont);
    index_fin = cont.size();
  };

  //! get fist item
  T &getFirst() {
    //this->end_flag = false;
    index_courant = 0;
    tab_parcours =
        &(static_cast<ContainerArrayBaseType<T> &>(this->my_cont).array[0]);
    if (static_cast<ContainerArrayBaseType<T> &>(this->my_cont)
            .array.size() == 0) {
      // this->end_flag = true;
      return (
          static_cast<ContainerArrayBaseType<T> &>(this->my_cont).NULL_ELEM);
    } else
      return tab_parcours[index_courant];
  }
  //! get next item in course
  T &getNext() {
    index_courant++;

    if (index_courant >= index_fin) {
      --index_courant;
      // this->end_flag = true;
    }
    return tab_parcours[index_courant];
  }

private:
  //! current index in course
  UInt index_courant;
  //! first index in course
  UInt index_debut;
  //! last index in course
  UInt index_fin;

  //! reference to invalid item
  T invalid_ref;
  //! pointer to avoid over-cost in using STL vector
  T *tab_parcours;
};

#endif /* __LIBMULTISCALE_ITERATOR_ARRAY_BASE_TYPE_HH__ */
