#ifndef __LIBMULTISCALE_FIELD_HH__
#define __LIBMULTISCALE_FIELD_HH__
/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

template <FieldType fieldtype, typename T> class Field {
public:
  static constexpr FieldType type = fieldtype;

  Field(T &cont) : cont(cont){};

  UInt getDim() {
    if (not cont.size())
      return 0;
    auto &&ref = *this->cont.begin();
    // ref.test();
    auto f = ref.template field<fieldtype>();
    return f.size();
  };

  std::string getName() {
    std::stringstream sstr;
    sstr << fieldtype;
    return sstr.str();
  };

  template <typename it_type> struct iterator {

    iterator(it_type it) : it(std::move(it)) {}
    bool operator!=(const iterator &it) const { return this->it != it.it; };
    void operator++() { ++this->it; };
    auto operator*() { return (*it).template field<fieldtype>(); };

    it_type it;
  };

  decltype(auto) begin() {
    return iterator<decltype(cont.begin())>(cont.begin());
  };
  decltype(auto) end() { return iterator<decltype(cont.begin())>(cont.end()); };

  T &cont;
};

template <FieldType fieldtype, typename T>
Field<fieldtype, T> make_field(T &cont) {
  return Field<fieldtype, T>(cont);
}

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

#endif //__LIBMULTISCALE_FIELD_HH__
