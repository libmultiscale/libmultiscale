/**
 * @file   iterator_array.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Thu Sep 06 20:05:31 2012
 *
 * @brief  Iterator over contiguous arrays
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_ITERATOR_ARRAY_HH__
#define __LIBMULTISCALE_ITERATOR_ARRAY_HH__
/* -------------------------------------------------------------------------- */
#include "container_array.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/**
 * Class IteratorArray
 * Generic iterator over array containers
 */

template <typename T> class ContainerArrayIterator : public iterator_base<T> {

public:
  using iterator_category = std::random_access_iterator_tag;
  using value_type = T;
  using iterator = ContainerArrayIterator<T>;

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  ContainerArrayIterator(T *ptr);

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  //! get fist item
  T &operator*() { return *ptr; }
  //! get next item in course
  iterator_base<T> &operator++() override;
  iterator_base<T> &operator--();
  bool operator!=(const iterator_base<T> &) const override;
  bool operator==(const iterator_base<T> &) const override;

  iterator operator+(UInt n) { return iterator(ptr + n); };
  iterator operator-(UInt n) { return ptr - n; };
  int operator-(const iterator &it) { return ptr - it.ptr; };
  bool operator<(const iterator &it) { return ptr < it.ptr; };
  bool operator>(const iterator &it) { return ptr > it.ptr; };
  iterator operator[](UInt n) { return ptr[n]; };

private:
  T *ptr;
};

/* -------------------------------------------------------------------------- */

template <typename T>
ContainerArrayIterator<T>::ContainerArrayIterator(T *ptr) {
  this->ptr = ptr;
}

/* -------------------------------------------------------------------------- */

template <typename T>
iterator_base<T> &ContainerArrayIterator<T>::operator++() {
  ++ptr;
  return *this;
}

/* -------------------------------------------------------------------------- */

template <typename T>
iterator_base<T> &ContainerArrayIterator<T>::operator--() {
  --ptr;
  return *this;
}

/* -------------------------------------------------------------------------- */

template <typename T>
bool ContainerArrayIterator<T>::operator!=(const iterator_base<T> &it) const {
  return (this->ptr != static_cast<const iterator &>(it).ptr);
}

/* -------------------------------------------------------------------------- */
template <typename T>
bool ContainerArrayIterator<T>::operator==(const iterator_base<T> &it) const {
  return (this->ptr == static_cast<const iterator &>(it).ptr);
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

namespace std {
template <typename T>
struct iterator_traits<::libmultiscale::ContainerArrayIterator<T>> {
  using value_type = T;
  using iterator_category = random_access_iterator_tag;
  using difference_type = int;
  using pointer = T *;
  using reference = T &;
};
} // namespace std

#endif /* __LIBMULTISCALE_ITERATOR_ARRAY_HH__ */
