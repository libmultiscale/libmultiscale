#ifndef __LIBMULTISCALE_ACCESSOR_DOF_HH__
#define __LIBMULTISCALE_ACCESSOR_DOF_HH__
/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

template <typename Ref, FieldType ftype> class AccessorDof {
public:
  using TensorType = typename field_getter<ftype, Ref>::TensorType;
  static constexpr UInt TensorDim = field_getter<ftype, Ref>::TensorDim;

  template <typename T>
  using enable_if_scalar =
      std::enable_if_t<std::is_arithmetic_v<std::decay_t<T>>> *;
  template <typename T>
  using enable_if_not_scalar =
      std::enable_if_t<not std::is_arithmetic_v<std::decay_t<T>>> *;

private:
  TensorType field;

public:
  template <typename T = TensorType, enable_if_not_scalar<T> = nullptr>
  decltype(auto) operator[](UInt index) {
    return field(index);
  }
  template <typename T = TensorType, enable_if_scalar<T> = nullptr>
  decltype(auto) operator[](UInt index) {
    if (index > 0)
      LM_FATAL("invalid index");
    return field;
  }

  using value_type =
      std::decay_t<decltype(std::declval<AccessorDof<Ref, ftype> &>()[0])>;
  AccessorDof(Ref &nd) : field(field_getter<ftype, Ref>::get(nd)){};

  template <typename T>
  inline TensorType &operator=(const Vector<TensorDim> &val) {
    field = val;
    return field;
  }
  template <typename T>
  inline TensorType &operator=(const VectorView<TensorDim> val) {
    field = val;
    return field;
  }

  template <typename T> inline TensorType &operator=(const T &val) {
    field = val;
    return field;
  }

  template <typename T> inline T operator=(const T val[]) {
    field = Eigen::Map<TensorType>(val);
    return field;
  }

  template <typename T> inline T operator+=(const T &val) {
    field += val;
    return field;
  }

  template <typename T = TensorType, enable_if_not_scalar<T> = nullptr>
  UInt size() const {
    return field.size();
  }

  template <typename T = TensorType, enable_if_scalar<T> = nullptr>
  UInt size() const {
    return 1;
  }

  template <typename T = TensorType, enable_if_not_scalar<T> = nullptr>
  decltype(auto) data() {
    return field.data();
  }
  template <typename T = TensorType, enable_if_scalar<T> = nullptr>
  decltype(auto) data() {
    return &field;
  }

  template <typename T = TensorType, enable_if_not_scalar<T> = nullptr>
  operator ArrayView() {
    return ArrayView(field.data(), 1, field.size());
  }

  template <typename T = TensorType, enable_if_not_scalar<T> = nullptr>
  operator Vector<TensorDim, value_type>() {
    return field;
  }

  // template <typename T = TensorType,
  //           std::enable_if_t<std::is_arithmetic_v<std::decay_t<T>> and
  //                            (field_getter<ftype, Ref>::Dim > 1)> * =
  //                            nullptr>
  // operator Vector<field_getter<ftype, Ref>::Dim, value_type>() {
  //   LM_FATAL("cannot call this conversion");
  // }

  template <typename T = TensorType, enable_if_scalar<T> = nullptr>
  operator T() {
    return field;
  }

  template <typename T = TensorType, enable_if_scalar<T> = nullptr>
  operator Vector<1, value_type>() {
    return field;
  }

  void printself(std::ostream &os) { os << field; }
};

__END_LIBMULTISCALE__

#endif
