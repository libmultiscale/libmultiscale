#ifndef __LM_PYTHON_TYPES_HH__
#define __LM_PYTHON_TYPES_HH__
/* -------------------------------------------------------------------------- */
#include "lm_communicator.hh"
#include "lm_timer.hh"
#include "lm_types.hh"
/* -------------------------------------------------------------------------- */
#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
/* -------------------------------------------------------------------------- */

namespace py = pybind11;

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

inline void declare_types(py::module &m) {

  py::enum_<dbgLevel>(m, "dbgLevel")
      .value("DBG_MESSAGE", dbg_message)
      .value("DBG_WARNING", dbg_warning)
      .value("DBG_INFO_STARTUP", dbg_info_startup)
      .value("DBG_INFO", dbg_info)
      .value("DBG_DETAIL", dbg_detail)
      .value("DBG_ALL", dbg_all)
      .export_values();

  m.def("DUMP", [](const std::string &mesg, dbgLevel level) {
    switch (level) {
    case dbg_message:
      DUMP(mesg, DBG_MESSAGE);
      break;
    case dbg_warning:
      DUMP(mesg, DBG_WARNING);
      break;
    case dbg_info_startup:
      DUMP(mesg, DBG_INFO_STARTUP);
      break;
    case dbg_info:
      DUMP(mesg, DBG_INFO);
      break;
    case dbg_detail:
      DUMP(mesg, DBG_DETAIL);
      break;
    case dbg_all:
      DUMP(mesg, DBG_ALL);
      break;
    }
  });

  m.def("STARTTIMER", [](const std::string &mesg) { STARTTIMER(mesg); });
  m.def("STOPTIMER", [](const std::string &mesg) { STOPTIMER(mesg); });

  py::enum_<IntegrationSchemeStage>(m, "IntegrationSchemeStage")
      .value("NONE_STEP", NONE_STEP)
      .value("PRE_DUMP", PRE_DUMP)
      .value("PRE_STEP1", PRE_STEP1)
      .value("PRE_STEP2", PRE_STEP2)
      .value("PRE_STEP3", PRE_STEP3)
      .value("PRE_STEP4", PRE_STEP4)
      .value("PRE_STEP5", PRE_STEP5)
      .value("PRE_STEP6", PRE_STEP6)
      .value("PRE_STEP7", PRE_STEP7)
      .value("PRE_FATAL", PRE_FATAL)
      .value("ALL_STEP", ALL_STEP)
      .export_values();

  py::enum_<CouplingStage>(m, "CouplingStage")
      .value("COUPLING_STEP1", COUPLING_STEP1)
      .value("COUPLING_STEP2", COUPLING_STEP2)
      .value("COUPLING_STEP3", COUPLING_STEP3)
      .value("COUPLING_STEP4", COUPLING_STEP4)
      .value("COUPLING_STEP5", COUPLING_STEP5)
      .value("COUPLING_STEP6", COUPLING_STEP6)
      .value("COUPLING_STEP7", COUPLING_STEP7)
      .export_values();

  py::enum_<DOFType>(m, "DOFType")
      .value("dt_master", dt_master)
      .value("dt_slave", dt_slave)
      .value("dt_pbc_master", dt_pbc_master)
      .value("dt_pbc_slave", dt_pbc_slave)
      .value("dt_all", dt_all)
      .export_values();

  py::enum_<FieldType>(m, "FieldType")
      .value("position0", _position0)
      .value("position", _position)
      .value("displacement", _displacement)
      .value("velocity", _velocity)
      .value("force", _force)
      .value("stress", _stress)
      .value("temperature", _temperature)
      .value("grouprank", _grouprank)
      .value("strain", _strain)
      .value("epot", _epot)
      .value("applied_force", _applied_force)
      .value("mass", _mass)
      .value("tag", _tag)
      .value("id", _id)
      .value("proc", _proc)
      .value("charge", _charge)
      .value("burgers", _burgers)
      .value("normal", _normal)
      .value("boundary", _boundary)
      .value("torque", _torque)
      .value("angular_velocity", _angular_velocity)
      .value("angular_acceleration", _angular_acceleration)
      .export_values();

  py::class_<CommGroup>(m, "CommGroup");

  py::class_<Communicator>(m, "Communicator")
      .def("getCommunicator", &Communicator::getCommunicator,
           py::return_value_policy::reference)
      .def("getNumberFreeProcs", &Communicator::getNumberFreeProcs)
      .def("addGroup", &Communicator::addGroup)
      .def("getGroup", &Communicator::getGroup)
      .def("reset", &Communicator::reset);
}
/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__

#endif //__LM_PYTHON_TYPES_HH__
