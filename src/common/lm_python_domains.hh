#ifndef __LM_PYTHON_DOMAINS_HH__
#define __LM_PYTHON_DOMAINS_HH__
/* -------------------------------------------------------------------------- */
#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
/* -------------------------------------------------------------------------- */
#include "domain_multiscale.hh"
#include "lm_python_containers.hh"
/* -------------------------------------------------------------------------- */

namespace py = pybind11;

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

template <class T> struct _type_traits { using type = T; };

template <typename Class, typename Func>
inline auto declare_domain(py::module &m, const char *class_name,
                           Func &&dec_func) {

  auto &dec = py::class_<Class, DomainInterface, std::shared_ptr<Class>>(
                  m, class_name, py::multiple_inheritance())
                  .def(py::init<LMID, CommGroup>())
                  .def("init",
                       [](Class &self) {
                         self.checkAllKeywordsAreParsed();
                         self.init();
                       })
                  .def("getContainer", &Class::getContainer,
                       py::return_value_policy::reference)
                  .def("updateGradient", &Class::updateGradient)
                  .def("updateAcceleration", &Class::updateAcceleration)
                  .def("potentialEnergy", &Class::potentialEnergy)
                  .def("primal", &Class::primal)
                  .def("primalTimeDerivative", &Class::primalTimeDerivative)
                  .def("gradient", &Class::gradient)
                  .def("acceleration", &Class::acceleration)
                  .def("getField", &Class::getField)
                  .def("enforceCompatibility", &Class::enforceCompatibility);

  dec_func(dec, _type_traits<Class>{});

  using container_class =
      std::decay_t<decltype(std::declval<Class>().getContainer())>;

  declare_container<container_class>::exec(m, class_name);
}

/* ------------------------------------------------------------------------ */
inline void declare_domains(py::module &m) {

  auto clean_class_name = [](const std::string &arg) {
    std::string res = arg;

    res.erase(std::remove(res.begin(), res.end(), '<'), res.end());
    res.erase(std::remove(res.begin(), res.end(), '>'), res.end());
    return res;
  };

  py::class_<DomainInterface, Parsable, Component,
             std::shared_ptr<DomainInterface>>(m, "DomainInterface",
                                               py::multiple_inheritance());

  py::class_<DomainMultiScale>(m, "DomainMultiScale")
      .def_static("getManager", &DomainMultiScale::getManager,
                  py::return_value_policy::reference)
      .def("build", &DomainMultiScale::build)
      .def("getObject", &DomainMultiScale::getObject,
           py::return_value_policy::reference)
      .def("coupling", &DomainMultiScale::coupling)
      .def("destroy", &DomainMultiScale::destroy)
      .def("addObject", py::overload_cast<std::shared_ptr<DomainInterface>>(
                            &DomainMultiScale::addObject))
      .def("for_each", [&](DomainMultiScale &self, py::object callable) {
        self.for_each([&](DomainInterface &d) {
          auto py_d = py::cast(d, py::return_value_policy::reference);
          callable(py_d);
        });
      });

#define PYTHON_DOMAIN(n, func, obj)                                            \
  {                                                                            \
    using _class = BOOST_PP_TUPLE_ELEM(3, 0, obj);                             \
    std::string domain_name =                                                  \
        clean_class_name(BOOST_PP_STRINGIZE(BOOST_PP_TUPLE_ELEM(3, 0, obj)));  \
    declare_domain<_class>(m, domain_name.c_str(), func);                      \
  }

  auto dec_atomic_domain = [](auto &dec [[gnu::unused]],
                              auto &&obj [[gnu::unused]]) {};
  auto dec_continuum_domain = [](auto &dec, auto &&obj) {
    using T = typename std::decay_t<decltype(obj)>::type;
    dec.def("getPBCpairs", &T::getPBCpairs);
  };
  auto dec_dd_domain [[gnu::unused]] = [](auto &dec [[gnu::unused]],
                                          auto &&obj [[gnu::unused]]) {};

  BOOST_PP_SEQ_FOR_EACH(PYTHON_DOMAIN, dec_atomic_domain, LIST_ATOM_MODEL);
  BOOST_PP_SEQ_FOR_EACH(PYTHON_DOMAIN, dec_dd_domain, LIST_DD_MODEL);
  BOOST_PP_SEQ_FOR_EACH(PYTHON_DOMAIN, dec_continuum_domain,
                        LIST_CONTINUUM_MODEL);

#undef PYTHON_DOMAIN
}
/* ------------------------------------------------------------------------ */

__END_LIBMULTISCALE__

#endif //__LM_PYTHON_DOMAINS_HH__
