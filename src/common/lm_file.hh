/**
 * @file   lm_file.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  File object useful to open/close read/write files in plain/compressed
 * mode
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_LM_FILE_HH__
#define __LIBMULTISCALE_LM_FILE_HH__
/* -------------------------------------------------------------------------- */
#include <zlib.h>
#include <cstdio>
#include <string>
#include "lm_types.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class LMFile {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  LMFile();
  LMFile(const std::string &fname, const std::string &mode = "",
         UInt compr = false);

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  void printf(const char *, ...);
  void close();
  void open(const std::string &fname, const std::string &mode = "",
            UInt compr = false);
  void flush();

  UInt tell();
  UInt seek(UInt offset, UInt set);
  int dumpchar(int c);
  UInt read(void *buffer, UInt size, UInt number);
  UInt write(void *buffer, UInt size, UInt number);
  char *gets(char *buf, UInt len);

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

private:
  FILE *_file;
  UInt fd;
#ifdef LIBMULTISCALE_USE_ZLIB
  gzFile gzfile;
#endif

  UInt opened;
  UInt compressed;

  std::string name;
};
/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

#endif /* __LIBMULTISCALE_LM_FILE_HH__ */
