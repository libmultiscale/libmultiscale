/**
 * @file   lm_functions.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  This file contains the global scope functions
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef LM_FUNCTIONS_HH
#define LM_FUNCTIONS_HH
/* -------------------------------------------------------------------------- */
#include "lm_macros.hh"
#include <iostream>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

// c++ typeinfo
std::string demangle(const char *name);

template <typename T> inline std::string typeinfo() {
  return demangle(typeid(T).name());
}

// LM libc functions wrappers
#ifdef SWIG
extern void lm_exit(int __status) throw();
#else
extern void lm_exit(int __status) throw() __attribute__((__noreturn__));
#endif

extern char *lm_getenv(const char *);
extern int lm_getpid();
extern int lm_atoi(const std::string &n);

// init functions
extern void loadModules(int argc, char **argv);
extern void closeModules();

__END_LIBMULTISCALE__
#endif // LM_FUNCTIONS_HH
