/**
 * @file   attached_object.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  This object is used to describe objects attached to containers of
 * references
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_ATTCHED_OBJECT_HH__
#define __LIBMULTISCALE_ATTCHED_OBJECT_HH__
/* -------------------------------------------------------------------------- */
#include "auto_argument.hh"
#include "lm_common.hh"
#include "lm_pack_buffer.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
template <typename T> class ContainerArray;

class AttachedObject : public AutoDispatch::ArgumentAny {

public:
  virtual ~AttachedObject(){};

  virtual void packData(UInt index, LMPackBuffer &buffer,
                        bool verbose = false) = 0;
  virtual void unpackData(UInt index, LMPackBuffer &buffer,
                          bool verbose = false) = 0;

  virtual void moveAttachedValues(UInt i_src, UInt i_dest) = 0;
  virtual void resize(UInt) { LM_TOIMPLEMENT; }
  virtual LMID getID() const = 0;
};

/* -------------------------------------------------------------------------- */

template <typename T> class AttachedVector : public AttachedObject {

public:
  AttachedVector(ContainerArray<T> &vector) { *this = vector; };
  virtual ~AttachedVector() = default;

  using AutoDispatch::ArgumentAny::operator=;

  void packData(UInt index, LMPackBuffer &buffer,
                bool verbose = false) override {
    auto &v = this->getV();
    for (UInt i = 0; i < v.cols(); ++i) {
      buffer << v(index, i);
    }
    if (verbose) {
      DUMP("packing: " << v.row(index), DBG_MESSAGE);
    }
  }
  void unpackData(UInt index, LMPackBuffer &buffer,
                  bool verbose = false) override {
    auto &v = this->getV();
    if (index >= v.rows())
      v.resize(index + 1);
    for (UInt i = 0; i < v.cols(); ++i) {
      buffer >> v(index, i);
    }
    if (verbose) {
      DUMP("unpacking: " << v.row(index), DBG_MESSAGE);
    }
  }
  void moveAttachedValues(UInt i_src, UInt i_dest) {
    auto &v = this->getV();
    v.row(i_dest) = v.row(i_src);
  }

  void resize(UInt sz) {
    auto &v = this->getV();
    DUMP(&v << " resizes from " << v.rows() << " to " << sz, DBG_DETAIL);
    v.resize(sz);
  }

  LMID getID() const override { return this->getV().getID(); };

  ContainerArray<T> &getV() { return this->get<ContainerArray<T>>(); };
  const ContainerArray<T> &getV() const {
    return this->get<ContainerArray<T>>();
  };
};
/* -------------------------------------------------------------------------- */

template <typename T>
std::shared_ptr<AttachedObject> make_attached(ContainerArray<T> &obj) {
  return std::make_shared<AttachedVector<T>>(obj);
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_ATTCHED_OBJECT_HH__ */
