/**
 * @file   ref_point_data.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  This is the class for reference that are simple points
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_REF_POINT_DATA_HH__
#define __LIBMULTISCALE_REF_POINT_DATA_HH__
/* -------------------------------------------------------------------------- */
#include "container_array.hh"
#include "cube.hh"
#include "ref_point.hh"

/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

template <UInt Dim> class RefPointData;
/* -------------------------------------------------------------------------- */

template <UInt Dim> class UnusableDomain {

public:
  typedef ContainerArray<RefPointData<Dim>> ContainerPoints;

public:
  ContainerArray<RefPointData<Dim>> &getContainer() { LM_TOIMPLEMENT; }
};
/* -------------------------------------------------------------------------- */

#define TO_IMPLEMENT_ACCESSOR(type_ret, name, params)                          \
  virtual type_ret name(params) { LM_TOIMPLEMENT; }

template <UInt _Dim> class RefPointData {

public:
  static constexpr UInt Dim = _Dim;
  using Domain = UnusableDomain<Dim>;

  using fields = field_list<_position0, _position, _displacement, _velocity,
                            _force, _stress, _mass>;

  RefPointData() : pos(Vector<Dim>::Zero()){};
  virtual ~RefPointData(){};

  Vector<Dim> &position0() { return pos; };
  Vector<Dim> &position() { return pos; };
  TO_IMPLEMENT_ACCESSOR(Vector<Dim> &, velocity, );
  TO_IMPLEMENT_ACCESSOR(Vector<Dim> &, displacement, );
  TO_IMPLEMENT_ACCESSOR(Vector<Dim> &, force, );
  TO_IMPLEMENT_ACCESSOR(Real &, stress, );
  TO_IMPLEMENT_ACCESSOR(Real &, mass, );
  TO_IMPLEMENT_ACCESSOR(Real &, getEPot, );
  TO_IMPLEMENT_ACCESSOR(Real &, charge, );
  TO_IMPLEMENT_ACCESSOR(Real &, alpha, );
  TO_IMPLEMENT_ACCESSOR(Real &, electronic_density, );
  TO_IMPLEMENT_ACCESSOR(UInt &, tag, );

  UInt &id() { return index; };
  UInt &getIndex() { return index; };

  virtual Real &appliedForce(UInt) { LM_TOIMPLEMENT; }
  virtual Real &temperature() { LM_TOIMPLEMENT; };
  virtual Real &temperatureVar() { LM_TOIMPLEMENT; };
  virtual Real &capacity() { LM_TOIMPLEMENT; };
  virtual Real &heatRate() { LM_TOIMPLEMENT; };
  virtual Real &externalHeatRate() { LM_TOIMPLEMENT; };
  virtual bool &boundary(UInt) { LM_TOIMPLEMENT; };

  template <FieldType ftype> Vector<Dim> &field() {
    if (ftype == _position) {
      return this->pos;
    } else {
      LM_TOIMPLEMENT;
    }
  }

  template <bool pbc_x, bool pbc_y, bool pbc_z>
  inline Real computeInitialDistance(const Real *, Cube &) {
    LM_TOIMPLEMENT;
  }

  template <bool pbc_x, bool pbc_y, bool pbc_z>
  inline Real computeDistance(const Real *, Cube &) {
    LM_TOIMPLEMENT;
  }

  inline Real computeInitialDistance(const Real *) { LM_TOIMPLEMENT; };
  inline bool operator==(RefPointData &ref) { return (ref.index == index); };

  template <typename Ref> RefPointData &operator=(Ref &r) {
    this->pos = r.position();
    return *this;
  }

  template <UInt Dim> RefPointData &operator=(VectorView<Dim> r) {
    this->pos = r;
    return *this;
  }

  //! return the dof_type
  virtual DOFType getDOFType() { return dt_master; };

  virtual void test(){};

private:
  UInt index;
  Vector<Dim> pos;
};

/* -------------------------------------------------------------------------- */

template <UInt Dim>
inline std::ostream &operator<<(std::ostream &os, RefPointData<Dim> &ref) {
  UInt n = ref.id();

  os << "Point Data reference : " << n << " [P] " << ref.position();
  return os;
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_REF_POINT_DATA_HH__ */
