#include "lm_object.hh"
#include "lm_communicator.hh"
/* -------------------------------------------------------------------------- */
#include <ctime>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

int LMObject::random_release() {
  //  std::srand(std::time(nullptr));
  return std::rand();
}
/* -------------------------------------------------------------------------- */

LMObject::LMObject(const LMID &id)
    : id(id), comm_group(Communicator::getGroup("self")) {
  release = random_release();
}
/* -------------------------------------------------------------------------- */

const LMID &LMObject::getID() const { return id; }
/* -------------------------------------------------------------------------- */

void LMObject::setID(const LMID &id) { this->id = id; }
/* -------------------------------------------------------------------------- */

void LMObject::setCommGroup(CommGroup group) { comm_group = group; }
/* -------------------------------------------------------------------------- */

CommGroup LMObject::getCommGroup() const { return comm_group; }
/* -------------------------------------------------------------------------- */

void LMObject::acquireContext(const LMObject &obj) {
  if (this->getCommGroup() == Communicator::getGroup("self"))
    this->setCommGroup(obj.getCommGroup());
  void *ptr = this;
  UInt low = ((UInt *)ptr)[0];
  UInt big = ((UInt *)ptr)[1];
  this->setRelease(hash_releases(low, big, obj.release));
}

/* -------------------------------------------------------------------------- */
UInt LMObject::getRelease() const { return release; }
/* -------------------------------------------------------------------------- */

void LMObject::setRelease(UInt r) { release = r; }
/* -------------------------------------------------------------------------- */

// void LMObject::incRelease() { ++release; }

/* -------------------------------------------------------------------------- */

void LMObject::changeRelease() { release = random_release(); }

__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
