/**
 * @file   lm_timer.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  Timer system for LibMultiScale
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LM_TIMER_HH__
#define __LM_TIMER_HH__
/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
#include <string>
/* -------------------------------------------------------------------------- */
#if defined(LIBMULTISCALE_TIMER)
/* -------------------------------------------------------------------------- */
#include <chrono>
#include <fstream>
#include <map>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
using time_mark = std::chrono::time_point<std::chrono::high_resolution_clock>;
extern std::map<std::string, std::chrono::microseconds> mesT;
extern std::map<std::string, UInt> nmes;
extern std::map<std::string, time_mark> tstart;
extern std::map<std::string, time_mark> tstop;
extern UInt header;
/* -------------------------------------------------------------------------- */
#define SEC_IN_NANOSEC 1000000000
/* -------------------------------------------------------------------------- */

inline void STARTTIMER(const std::string &x) {
  if (!mesT.count(x)) {
    mesT[x] = std::chrono::microseconds::zero();
  }
  auto start = std::chrono::high_resolution_clock::now();
  tstart[x] = start;
}
/* -------------------------------------------------------------------------- */

inline void STOPTIMER(const std::string &x) {
  auto stop = std::chrono::high_resolution_clock::now();
  tstop[x] = stop;

  mesT[x] += std::chrono::duration_cast<std::chrono::microseconds>(tstop[x] -
                                                                   tstart[x]);
  ++nmes[x];
}
/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
#else
inline void STARTTIMER(const std::string &) {}
inline void STOPTIMER(const std::string &) {}
#endif

__BEGIN_LIBMULTISCALE__
void dumpTimes(UInt proc_number);
void dumpTimesInColumn(UInt proc_number, UInt step = 0);
__END_LIBMULTISCALE__
#endif // __LM_TIMER_HH__
