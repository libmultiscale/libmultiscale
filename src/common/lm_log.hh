/**
 * @file   lm_log.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  This is the file containing all the macros for the log/debug system
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_LOG_HH__
#define __LIBMULTISCALE_LOG_HH__
/* -------------------------------------------------------------------------- */
#include "lm_functions.hh"
#include "lm_globals.hh"
#include "lm_macros.hh"
#include "lm_types.hh"
#include <list>
#include <map>
#include <memory>
#include <sstream>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
#define DBG_MESSAGE 0
#define DBG_WARNING 1
#define DBG_INFO_STARTUP 2
#define DBG_INFO 3
#define DBG_DETAIL 4
#define DBG_ALL 5
/* -------------------------------------------------------------------------- */
void openGlobal();
/* -------------------------------------------------------------------------- */
void doWait(bool condition);
void doWaitOnFatal();
void doWaitAtStartup();
/* -------------------------------------------------------------------------- */
#define DOWAIT_ON_FATAL doWaitOnFatal();
#define DOWAIT_AT_STARTUP doWaitAtStartup();
/* -------------------------------------------------------------------------- */
enum dbgLevel {
  dbg_message = 1,
  dbg_warning = 2,
  dbg_info_startup = 3,
  dbg_info = 4,
  dbg_detail = 5,
  dbg_all = 6
};
/* -------------------------------------------------------------------------- */

#define DUMP(x, level) DUMP##level(x)
#define DUMPBYPROC(x, level, p)                                                \
  if (lm_my_proc_id == p || global_proc == lm_my_proc_id ||                    \
      global_proc1 == lm_my_proc_id || global_proc2 == lm_my_proc_id)          \
  DUMP##level(x)
#define DUMPFILE(f, x)                                                         \
  if (lm_my_proc_id == 0) {                                                    \
    f << __FILE__ << "," << __LINE__ << ">" << x << std::endl;                 \
  }

/* -------------------------------------------------------------------------- */
#define PRINTOUT(x, level)                                                     \
  {                                                                            \
    if (global_level >= level) {                                               \
      std::stringstream sstr;                                                  \
      sstr << x;                                                               \
      lmPrintOut(sstr, level, __LINE__, __FILE__, __func__,                    \
                 __PRETTY_FUNCTION__);                                         \
    }                                                                          \
  }
void lmPrintOut(std::stringstream &x, dbgLevel level, UInt line,
                const std::string &file, const std::string &function,
                const std::string &pretty_function);
/* -------------------------------------------------------------------------- */

#ifdef LM_OPTIMIZED
#define LEVEL_MAX 1
#else
#define LEVEL_MAX 10
#endif

#if LEVEL_MAX > 0
#define DUMPDBG_MESSAGE(x)                                                     \
  { PRINTOUT(x, dbg_message); }
#else
#define DUMPDBG_MESSAGE(x)                                                     \
  {}
#endif

#if LEVEL_MAX > 1
#define DUMPDBG_WARNING(x)                                                     \
  { PRINTOUT(x, dbg_warning); }
#else
#define DUMPDBG_WARNING(x)                                                     \
  {}
#endif

#if LEVEL_MAX > 2
#define DUMPDBG_INFO_STARTUP(x)                                                \
  { PRINTOUT(x, dbg_info_startup); }
#else
#define DUMPDBG_INFO_STARTUP(x)                                                \
  {}
#endif

#if LEVEL_MAX > 3
#define DUMPDBG_INFO(x)                                                        \
  { PRINTOUT(x, dbg_info); }
#else
#define DUMPDBG_INFO(x)                                                        \
  {}
#endif

#if LEVEL_MAX > 4
#define DUMPDBG_DETAIL(x)                                                      \
  { PRINTOUT(x, dbg_detail); }
#else
#define DUMPDBG_DETAIL(x)                                                      \
  {}
#endif

#if LEVEL_MAX > 5
#define DUMPDBG_ALL(x)                                                         \
  { PRINTOUT(x, dbg_all); }
#else
#define DUMPDBG_ALL(x)                                                         \
  {}
#endif

/* -------------------------------------------------------------------------- */

struct LMStackTrace {
  LMStackTrace(std::list<std::string> stack,
               std::map<std::string, size_t> addr_map);

  std::list<std::string> stack;
  std::map<std::string, size_t> addr_map;
};

/* -------------------------------------------------------------------------- */

#define LM_THROW(x)                                                            \
  {                                                                            \
    std::stringstream sstr;                                                    \
    sstr << x;                                                                 \
    throw LibMultiScaleException(sstr.str());                                  \
  }

/* -------------------------------------------------------------------------- */

#ifndef LM_OPTIMIZED
#define LM_ASSERT(cond, message)                                               \
  if (!(cond)) {                                                               \
    LM_FATAL(message);                                                         \
  }
#else
#define LM_ASSERT(cond, message)                                               \
  {}
#endif

/* -------------------------------------------------------------------------- */

struct LibMultiScaleException : public std::exception {
public:
  LibMultiScaleException(const std::string &mess) throw();
  LibMultiScaleException(const LibMultiScaleException &e,
                         const std::string &mess) throw();

  ~LibMultiScaleException() throw(){};

  virtual const char *what() const throw();

  std::string messageWithTrace();
  std::string recursiveWhat();

  std::string message;
  std::string recursive_message;
  LMStackTrace stack_trace;
  std::shared_ptr<LibMultiScaleException> previous_exception;
};

/* -------------------------------------------------------------------------- */

void lmFatal(std::stringstream &x, UInt line, const std::string &file,
             const std::string &function, const std::string &pretty_function);

#ifndef LM_FATAL
#define LM_FATAL(x)                                                            \
  {                                                                            \
    std::stringstream sstr;                                                    \
    sstr << x;                                                                 \
    lmFatal(sstr, __LINE__, __FILE__, __func__, __PRETTY_FUNCTION__);          \
    ::libmultiscale::lm_exit(LM_EXIT_FAILURE);                                 \
  }
#endif

void lmFatal_rethrow(LibMultiScaleException &e, std::stringstream &x, UInt line,
                     const std::string &file, const std::string &function,
                     const std::string &pretty_function);

#ifndef LM_FATAL_RE
#define LM_FATAL_RE(e, x)                                                      \
  {                                                                            \
    std::stringstream sstr;                                                    \
    sstr << x;                                                                 \
    lmFatal_rethrow(e, sstr, __LINE__, __FILE__, __func__,                     \
                    __PRETTY_FUNCTION__);                                      \
    ::libmultiscale::lm_exit(LM_EXIT_FAILURE);                                 \
  }
#endif

/* -------------------------------------------------------------------------- */

#define LM_TOIMPLEMENT                                                         \
  { LM_FATAL("Function " << __PRETTY_FUNCTION__ << " not implemented "); }

/* -------------------------------------------------------------------------- */

// backward declaration and type constraints
// scalars
template <typename T> using detectScalar = std::is_arithmetic<std::decay_t<T>>;

// AccessorAtomDof
template <typename Ref, FieldType ftype> class AccessorAtomDof;
template <typename Ref, FieldType ftype> class AccessorNodalDof;

template <typename T> struct detectAccessorDof {};
template <typename Ref, FieldType ftype>
struct detectAccessorDof<AccessorAtomDof<Ref, ftype>> : std::true_type {};
template <typename Ref, FieldType ftype>
struct detectAccessorDof<AccessorNodalDof<Ref, ftype>> : std::true_type {};

template <typename T> struct detectVector {};
template <UInt Dim, typename T>
struct detectVector<Vector<Dim, T>> : std::true_type {};
template <UInt Dim, typename T>
struct detectVector<VectorView<Dim, T>> : std::true_type {};

template <typename T> struct detectMatrix {};
template <UInt Dim> struct detectMatrix<Matrix<Dim>> : std::true_type {};

#if __cplusplus > 202002L
// concepts
template <typename T>
concept VectorType = detectVector<T>::value;

template <typename T>
concept AccessorDofType = detectAccessorDof<T>::value;

template <typename T>
concept MatrixType = detectMatrix<T>::value;

template <typename T>
concept TensorType = detectMatrix<T>::value or detectVector<T>::value;

template <typename T> struct tensor_type_traits {};

template <ScalarType T> struct tensor_type_traits<T> {
  using TensorType = std::decay_t<T>;
  using ScalarType = TensorType;
  static constexpr UInt TensorDim = 1;
};

template <AccessorDofType T> struct tensor_type_traits<T> {
  using TensorType = typename T::TensorType;
  using ScalarType = typename T::Scalar;
  static constexpr UInt TensorDim = T::TensorDim;
};

template <TensorType T> struct tensor_type_traits<T> {
  using TensorType = T;
  using ScalarType = typename T::Scalar;
  static constexpr UInt TensorDim = T::Dim;
};
#else

template <typename T> struct tensor_scalar_traits {
  using TensorType = std::decay_t<T>;
  using ScalarType = TensorType;
  static constexpr UInt TensorDim = 1;
};

template <typename T> struct tensor_accessor_traits {
  using TensorType = typename T::TensorType;
  using ScalarType = typename T::Scalar;
  static constexpr UInt TensorDim = T::TensorDim;
};

template <typename T> struct tensor_vector_traits {
  using TensorType = T;
  using ScalarType = typename T::Scalar;
  static constexpr UInt TensorDim = T::Dim;
};

template <typename T> struct tensor_type_traits {};

template <UInt Dim, typename T>
struct tensor_type_traits<VectorView<Dim, T>>
    : public tensor_vector_traits<VectorView<Dim, T>> {};

template <UInt Dim, typename T>
struct tensor_type_traits<Vector<Dim, T>>
    : public tensor_vector_traits<Vector<Dim, T>> {};

template <UInt Dim>
struct tensor_type_traits<Matrix<Dim>>
    : public tensor_vector_traits<Matrix<Dim>> {};

template <typename Ref, FieldType ftype>
struct tensor_type_traits<AccessorAtomDof<Ref, ftype>>
    : public tensor_accessor_traits<AccessorAtomDof<Ref, ftype>> {};

template <typename Ref, FieldType ftype>
struct tensor_type_traits<AccessorNodalDof<Ref, ftype>>
    : public tensor_accessor_traits<AccessorNodalDof<Ref, ftype>> {};

template <>
struct tensor_type_traits<Real> : public tensor_scalar_traits<Real> {};

template <>
struct tensor_type_traits<Real &> : public tensor_scalar_traits<Real &> {};

#endif

/* -------------------------------------------------------------------------- */

template <FieldType ftype, typename Ref> struct field_getter {};

#define DECLARE_FIELD_GETTER(_field)                                           \
  template <typename Ref> struct field_getter<_##_field, Ref> {                \
                                                                               \
    using raw_type = std::invoke_result_t<decltype(&Ref::_field), Ref &>;      \
    using TensorTraits = tensor_type_traits<raw_type>;                         \
    using TensorType = typename TensorTraits::TensorType;                      \
    using ScalarType = typename TensorTraits::ScalarType;                      \
    static constexpr UInt TensorDim = TensorTraits::TensorDim;                 \
                                                                               \
    template <typename R> static decltype(auto) get(R &&ref) {                 \
      return ref._field();                                                     \
    }                                                                          \
  }

DECLARE_FIELD_GETTER(position);
DECLARE_FIELD_GETTER(velocity);
DECLARE_FIELD_GETTER(position0);
DECLARE_FIELD_GETTER(force);
DECLARE_FIELD_GETTER(displacement);
DECLARE_FIELD_GETTER(stress);
DECLARE_FIELD_GETTER(strain);
DECLARE_FIELD_GETTER(mass);
DECLARE_FIELD_GETTER(burgers);
DECLARE_FIELD_GETTER(normal);
DECLARE_FIELD_GETTER(acceleration);
DECLARE_FIELD_GETTER(boundary);
DECLARE_FIELD_GETTER(internal_force);
DECLARE_FIELD_GETTER(external_force);
DECLARE_FIELD_GETTER(angular_velocity);
DECLARE_FIELD_GETTER(angular_acceleration);
DECLARE_FIELD_GETTER(torque);
DECLARE_FIELD_GETTER(radius);
DECLARE_FIELD_GETTER(epot);

#undef DECLARE_FIELD_GETTER
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_LOG_HH__ */
