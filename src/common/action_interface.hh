/**
 * @file   action_interface.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  This is the interface to all action objects
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_ACTION_INTERFACE_HH__
#define __LIBMULTISCALE_ACTION_INTERFACE_HH__
/* -------------------------------------------------------------------------- */
#include "comm_group.hh"
#include "component_libmultiscale.hh"
#include "container_array.hh"
#include "lm_parsable.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

template <typename T> class FactoryMultiScale;

class ActionInterface : public Parsable, public Component {

public:
  ActionInterface();
  virtual ~ActionInterface(){};

public:
  //! this function holds to possible restriction to action calls
  virtual bool shouldMakeAction();
  //! do the action effectively
  virtual void action();
  //! initialisation of the action internal data
  virtual void init(){};
  //! set parameters
  virtual void declareParams();

  static FactoryMultiScale<ActionInterface> &getManager();

protected:
  //! this function check for the stage mask
  bool doesStageMaskMatch();
  //! set start and end step when onestep is declared
  void setOneStep();

public:
  //! set default values
  void setDefaults();
  //! function to print the contain of the class
  virtual void printself(std::ostream &stream) const;

protected:
  //! actual dump step
  UInt action_step;
  //! start step
  UInt start_step;
  //! end step
  UInt end_step;
  //! one shot
  UInt one_shot;
  //! frequency of the dump
  UInt frequency;

private:
  //! stage mask to explicitly specify at what place in the scheme to do the
  //! action
  IntegrationSchemeMask stage_mask;
};

/* -------------------------------------------------------------------------- */

inline std::ostream &operator<<(std::ostream &os, ActionInterface &obj) {
  obj.printself(os);
  return os;
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_ACTION_INTERFACE_HH__ */
