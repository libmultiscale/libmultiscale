/**
 * @file   reference_manager_interface.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  This is the interface for managers of reference and coherency with
 * migrations
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __AKANTU_REFERENCE_MANAGER_INTERFACE_HH__
#define __AKANTU_REFERENCE_MANAGER_INTERFACE_HH__
/* -------------------------------------------------------------------------- */
#include "attached_object.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
class ContainerInterface;
class LMObject;
template <typename T> class ContainerArray;
template <typename Ref> class ReferenceManager;
/* -------------------------------------------------------------------------- */

class ReferenceManagerInterface {

public:
  ReferenceManagerInterface() : flag_need_check_movements(false){};

  virtual ~ReferenceManagerInterface(){};

  //! set the Activate/UnActivate state of the reference manager
  virtual void setState(bool state) { flag_need_check_movements = state; };
  //! request to manage a subset
  virtual void addSubset(ContainerInterface &cont) = 0;
  // //! request remove a subset from the manager
  virtual void removeSubset(const std::string &name) = 0;
  //! request attaching a generic AttachedObject with  a given container
  template <typename T> void attachObject(T &obj, ContainerInterface &c);
  //! request detaching a generic AttachedObject with  a given container
  template <typename T> void detachObject(T &obj, ContainerInterface &c);

  virtual void acquireContext(const LMObject &obj) = 0;

protected:
  virtual void attachObject(std::shared_ptr<AttachedObject> obj,
                            ContainerInterface &c) = 0;
  virtual void detachObject(std::shared_ptr<AttachedObject> obj,
                            ContainerInterface &c) = 0;

  bool flag_need_check_movements;
};

/* -------------------------------------------------------------------------- */

template <typename T>
void ReferenceManagerInterface::attachObject(T &obj, ContainerInterface &c) {
  this->attachObject(make_attached(obj), c);
}

/* -------------------------------------------------------------------------- */

template <typename T>
void ReferenceManagerInterface::detachObject(T &obj, ContainerInterface &c) {
  this->detachObject(make_attached(obj), c);
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __AKANTU_REFERENCE_MANAGER_INTERFACE_HH__ */
