/**
 * @file   domain_interface.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  This is the interface to all domain (model/plugins)
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

/* -------------------------------------------------------------------------- */
#ifndef DOMAIN_INTERFACE_H
#define DOMAIN_INTERFACE_H
/* -------------------------------------------------------------------------- */
#include "comm_group.hh"
#include "component_libmultiscale.hh"
#include "lm_parsable.hh"
/* -------------------------------------------------------------------------- */
#include <Eigen/Dense>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
template <typename IT> class FactoryMultiScale;
class FilterInterface;
class ActionInterface;
class Cube;
/* -------------------------------------------------------------------------- */

/**
 * Class DomainInterface
 * is the most generic interface to domains
 */

class DomainInterface : public Parsable, public Component {

public:
  DomainInterface(CommGroup group_id);
  virtual ~DomainInterface();

  //! access a field given by field type
  virtual ArrayView getField(FieldType field_type) = 0;
  //! initialization method
  virtual void init() = 0;
  //! updates the gradient of energy
  virtual void updateGradient() = 0;
  //! updates the acceleration
  virtual void updateAcceleration() = 0;
  //! returns the acceleration
  virtual ArrayView acceleration() = 0;
  //! returns the gradient of energy
  virtual ArrayView gradient() = 0;
  //! returns the primal degrees of freedom
  virtual ArrayView primal() = 0;
  //! returns the time derivative of the primal degrees of freedom
  virtual ArrayView primalTimeDerivative() = 0;
  //! returns the time derivative of the primal degrees of freedom
  virtual ArrayView mass() = 0;
  //! returns the potential energy
  virtual Real potentialEnergy() = 0;
  //! restart from xml file function
  virtual void readXMLFile(const std::string &filename) = 0;
  //! return the size of the domain ususefull to deal with pbc
  virtual Cube getDomainBoundingBox() const = 0;
  //! return the size of the box associated to current processor
  virtual Cube getSubDomainBoundingBox() const = 0;
  //! return the size of the box associated to current processor
  virtual void setDomainBoundingBox(const Cube &cube) = 0;
  //! return the size of the box associated to current processor
  virtual void setSubDomainBoundingBox(const Cube &cube) = 0;
  //! set the parameters common to all domains
  virtual void declareParams() = 0;
  //! perform processor migration
  virtual void enforceCompatibility() = 0;
  //! returns whether an md domain is perdiodic in a given direction
  virtual bool isPeriodic(UInt dir) = 0;
  //! calls for domain specific output if any
  virtual void output(){};

  // ------ implemented in this interface class

  //! function to print the contain of the class
  virtual void printself(std::ostream &stream) const;
  //! return a reference to the geometry
  Geometry &getGeom();

protected:
  //! domain geometry
  LMID geom;
  // timestep
  Quantity<Time> timeStep;
};
/* -------------------------------------------------------------------------- */

inline std::ostream &operator<<(std::ostream &os, DomainInterface &obj) {
  obj.printself(os);
  return os;
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif // DOMAIN_INTERFACE_H
