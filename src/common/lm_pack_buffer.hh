/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifndef __LIBMULTISCALE_LM_PACK_BUFFER_H__
#define __LIBMULTISCALE_LM_PACK_BUFFER_H__

#include <memory>
#include <mpi.h>
#include <stdexcept>
#include <vector>

#ifdef LIBMULTISCALE_LAMMPS_PLUGIN
// In case the LAMMPS plugin is activate, load its pack buffer instance
// We will later make a using declaration.
#include "pack_buffer.h"	//Goes into the LAMMPS Tree
#endif

namespace libmultiscale {


#ifndef LIBMULTISCALE_LAMMPS_PLUGIN
/*
 * In case the LAMMPS Plugin is deactivated, we will use our own version of the LMPackBuffer,
 * which is in essenca a copy of the one inside LAMMPS.
 */
class LMPackBuffer {

public:
  LMPackBuffer() {
    _buf = std::make_shared<std::vector<char>>();
    read_index_ptr = std::make_shared<int>(0);
    write_index_ptr = std::make_shared<int>(0);
  };

  template <typename T> inline LMPackBuffer(T *ptr, int size = -1) {
    this->wrapped_buf = reinterpret_cast<char *>(ptr);
    read_index_ptr = std::make_shared<int>(0);
    write_index_ptr = std::make_shared<int>(0);
    wrapped_size = size * sizeof(T);
  }

  void resize(int sz) {
    if (_buf)
      _buf->resize(sz);
    else if (wrapped_buf and wrapped_size < sz)
      throw std::runtime_error("cannot resize a wrapped buffer");
  }

  template <typename T> void operator<<(T &&obj) {

    auto &write_index = *write_index_ptr;
    if (this->_buf) {
      char *ptr = reinterpret_cast<char *>(&obj);
      for (unsigned long i = 0; i < sizeof(std::decay_t<T>);
           ++i, ++write_index) {
        _buf->push_back(ptr[i]);
      }
    } else if (this->wrapped_buf) {
      char *ptr = reinterpret_cast<char *>(&obj);
      for (unsigned long i = 0; i < sizeof(std::decay_t<T>);
           ++i, ++write_index) {
        wrapped_buf[write_index] = ptr[i];
      }
    }
  }

  int read_index() { return *read_index_ptr; }
  int write_index() { return *write_index_ptr; }

  template <typename T> void operator>>(T &obj) {
    auto &read_index = *read_index_ptr;

    T res;
    if (this->_buf) {
      char *ptr = reinterpret_cast<char *>(&res);
      for (unsigned long i = 0; i < sizeof(std::decay_t<T>);
           ++i, ++read_index) {
        ptr[i] = (*_buf).at(read_index);
      }
    } else if (this->wrapped_buf) {
      char *ptr = reinterpret_cast<char *>(&res);
      for (unsigned long i = 0; i < sizeof(std::decay_t<T>);
           ++i, ++read_index) {
        ptr[i] = wrapped_buf[read_index];
      }
    } else
      throw std::runtime_error("should not happen");
    obj = res;
  }

  int size() {
    if (_buf)
      return _buf->size();
    if (wrapped_buf and wrapped_size >= 0)
      return wrapped_size;
    throw std::runtime_error("not allocated buffer");
  }

  void clear() {
    if (_buf)
      _buf->clear();
    *read_index_ptr = 0;
    *write_index_ptr = 0;
  }

  explicit operator char *() {
    if (this->_buf)
      return (char *)&(*this->_buf)[0];
    else if (this->wrapped_buf)
      return (char *)this->wrapped_buf;
    return nullptr;
  }

  int to_int() {
    int obj;
    *this >> obj;
    return obj;
  };
  int to_double() {
    double obj;
    *this >> obj;
    return obj;
  };

  LMPackBuffer operator[](int i) {
    if (this->_buf) {
      LMPackBuffer shifted(&(*this->_buf)[i]);
      return shifted;
    } else if (this->wrapped_buf) {
      LMPackBuffer shifted(this->wrapped_buf + i);
      return shifted;
    }
    throw;
  }

  template <typename T> T pop() {
    T res;
    res << *this;
    return res;
  }

  void push_bytes(void *ptr, int nbytes) {
    LMPackBuffer &buf = *this;
    char *src = static_cast<char *>(ptr);
    for (int i = 0; i < nbytes; ++i) {
      buf << src[i];
    }
  }
  void pop_bytes(void *ptr, int nbytes) {
    LMPackBuffer &buf = *this;
    char *dst = static_cast<char *>(ptr);
    for (int i = 0; i < nbytes; ++i) {
      buf >> dst[i];
    }
  }

  template <typename T = char> void seek(int n) {
    *read_index_ptr += sizeof(T) * n;
  }

private:
  std::shared_ptr<std::vector<char>> _buf;
  char *wrapped_buf = nullptr;
  int wrapped_size = 0;
  std::shared_ptr<int> read_index_ptr;
  std::shared_ptr<int> write_index_ptr;
};

template <typename T> void operator<<(T &&scalar, LMPackBuffer &buf) {
  buf >> scalar;
}

inline void communicateBuffers(LMPackBuffer send, int to, LMPackBuffer recv,
                               int from, int tag, MPI_Comm comm) {

  MPI_Status status;
  MPI_Request req;
  int nb_recv;

  MPI_Isend((char *)send, send.size(), MPI_CHAR, to, tag, comm, &req);
  MPI_Probe(from, tag, comm, &status);
  MPI_Get_count(&status, MPI_CHAR, &nb_recv);
  recv.clear();
  recv.resize(nb_recv);
  MPI_Recv((char *)recv, nb_recv, MPI_CHAR, from, tag, comm, MPI_STATUS_IGNORE);
  MPI_Wait(&req, MPI_STATUS_IGNORE);
}

#else
/*
 * Case the LAMMPS Plugin is present we make an alias
 */
using LMPackBuffer = LAMMPS_NS::PackBuffer;
#endif


/* ---------------------------------------------------------------------- */

} // namespace libmultiscale

#endif  /* __LIBMULTISCALE_LM_PACK_BUFFER_H__ */
