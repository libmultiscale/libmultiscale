/**
 * @file   lm_timer.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  Timer system for LibMultiScale
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "lm_common.hh"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
#ifdef LIBMULTISCALE_TIMER
/* -------------------------------------------------------------------------- */
using time_mark = std::chrono::time_point<std::chrono::high_resolution_clock>;
std::map<std::string, std::chrono::microseconds> mesT;
std::map<std::string, UInt> nmes;
std::map<std::string, time_mark> tstart;
std::map<std::string, time_mark> tstop;
UInt header = false;
/* -------------------------------------------------------------------------- */
void dumpTimes(UInt proc_number) {
  std::stringstream fname;
  fname << "perf-" << proc_number;
  std::ofstream out(fname.str().c_str());
  out << std::setw(30) << "Operation" << std::setw(5) << " " << std::setw(20)
      << "Mean time(s)" << std::setw(5) << " " << std::setw(20)
      << "Total time(s)" << std::setw(5) << " " << std::setw(20)
      << "Call number" << std::endl;
  std::map<std::chrono::microseconds, std::string> inv;
  for (auto &&[name, duration] : mesT) {
    inv[duration] = name;
  }

  for (auto &&[duration, name] : inv) {
    std::string &key = name;
    auto t = duration;
    UInt n = nmes[key];
    auto t_moy = t / n;
    out << std::scientific << std::setprecision(10) << std::setw(30) << key
        << std::setw(5) << " " << std::setw(20) << t_moy.count() << std::setw(5)
        << " " << std::setw(20) << t.count() << std::setw(5) << " "
        << std::setw(20) << n << std::endl;
  }
}
/* -------------------------------------------------------------------------- */
/* add by jcho */
void dumpTimesInColumn(UInt proc_number, UInt step) {
  std::map<std::chrono::microseconds, std::string> inv;
  for (auto &&[name, duration] : mesT) {
    inv[duration] = name;
  }

  std::stringstream fname;
  fname << "times-" << proc_number;

  if (!header) {
    header = true;
    std::ofstream myfile;
    myfile.open(fname.str().c_str());
    myfile << "step" << std::setw(30);

    for (auto &&[duration, name] : inv) {
      std::string &key = name;
      myfile << key << std::setw(30);
    }
    myfile << std::endl;
    myfile.close();
  } else {
    // analysis header
    std::ifstream myfile0(fname.str().c_str());
    std::string head;
    std::getline(myfile0, head);
    std::vector<std::string> keys;
    std::istringstream stm(head);
    std::string token;
    while (stm >> token)
      keys.push_back(token);
    UInt data_length = keys.size();

    // append data
    UInt count = 0;
    std::ofstream myfile;
    myfile.open(fname.str().c_str(), std::ios::app);
    myfile << step << std::setw(30);
    for (UInt i = 1; i < data_length; ++i) {
      for (auto &&[duration, name] : inv) {
        std::string &key = name;
        std::chrono::microseconds t = duration;
        if (key == keys[i]) {
          myfile << std::scientific << std::setprecision(10) << t.count()
                 << std::setw(30);
          count += 1;
          break;
        }
      }
    }
    if (count != data_length - 1) {
      LM_FATAL("non matched number of data " << count << " and "
                                             << data_length - 1);
    }
    myfile << std::endl;
    myfile.close();
  }
}

#else
void dumpTimes(UInt) {}
void dumpTimesInColumn(UInt, UInt) {}
#endif
/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
