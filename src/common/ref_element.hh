/**
 * @file   ref_element.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @brief  Mother of all reference to element
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_REF_ELEMENT_HH__
#define __LIBMULTISCALE_REF_ELEMENT_HH__
/* -------------------------------------------------------------------------- */
#include "ref_node_continuum.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

/**
 * Class RefElement
 * Generic reference over elements and connectivities
 */

template <typename DaughterClass> class RefElement {

public:
  RefElement() { this->is_altered = false; }
  virtual ~RefElement(){};

  //! return the local indexes (in DOF vector) for connected nodes
  virtual std::vector<UInt> localIndexes() = 0;
  //! return the global indexes (in DOF vector) for connected nodes
  virtual std::vector<UInt> globalIndexes() = 0;
  //! return a ref to the nodes
  auto nodes() { return localIndexes(); };

  inline void setAlteredConnectivity(std::vector<UInt> &altered_connectivity);
  inline std::vector<UInt> &getAlteredConnectivity();
  inline bool isAltered();

private:
  std::vector<UInt> altered_connectivity;
  bool is_altered;
};

/* -------------------------------------------------------------------------- */

template <typename DaughterClass>
inline void RefElement<DaughterClass>::setAlteredConnectivity(
    std::vector<UInt> &altered_connectivity) {
  this->altered_connectivity = altered_connectivity;
  this->is_altered = true;
}
/* -------------------------------------------------------------------------- */

template <typename DaughterClass>
inline std::vector<UInt> &RefElement<DaughterClass>::getAlteredConnectivity() {
  LM_ASSERT(this->is_altered == true,
            "the connectivity was not altered: cannot proceed");
  return this->altered_connectivity;
}
/* -------------------------------------------------------------------------- */

template <typename DaughterClass>
inline bool RefElement<DaughterClass>::isAltered() {
  return this->is_altered;
}

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_REF_ELEMENT_HH__ */
