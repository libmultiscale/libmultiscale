/**
 * @file   container_mesh.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  This is the abstract container for mesh data
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_CONTAINER_MESH_HH__
#define __LIBMULTISCALE_CONTAINER_MESH_HH__
/* -------------------------------------------------------------------------- */
#include "container.hh"
#include "container_array.hh"
#include "ref_element.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

template <typename ContNodes, typename ContElems>
class ContainerMesh : public virtual LMObject,
                      public Container_base<typename ContNodes::Ref> {
public:
  using ContainerNodes = ContNodes;
  using ContainerElems = ContElems;
  using iterator = typename ContNodes::iterator;
  using Ref = typename ContNodes::Ref;
  using RefElem = typename ContElems::Ref;

  using ContainerSubset =
      ContainerMesh<ContainerArray<Ref>, ContainerArray<RefElem>>;

  static constexpr UInt Dim = ContainerNodes::Dim;

  ContainerMesh(const LMID &id);
  ContainerMesh(ContainerMesh &&id) = default;
  ContainerMesh(ContainerMesh &id) = default;
  virtual ~ContainerMesh();

  decltype(auto) filter(int dt) {
    return Container_base<Ref>::template filter<ContainerMesh>(dt);
  }

  operator ContainerNodes &() {
    LM_FATAL(
        "silent conversion from ContainerMesh to ContainerNode is not allowed");
    return container_nodes;
  }

  iterator begin() { return container_nodes.begin(); };
  iterator end() { return container_nodes.end(); };

  Ref &get(UInt index) { return container_nodes.get(index); }

  void clear() {
    container_nodes.clear();
    container_elems.clear();
  }

  UInt size() const { return container_nodes.size(); };

  template <typename ContNodesPristine>
  void computeAlteredConnectivity(ContNodesPristine &contNodes);

  void setCommGroup(CommGroup group) override;

  ContNodes &getArray() { return this->getContainerNodes(); };
  ContNodes &getContainerNodes() { return container_nodes; };
  ContElems &getContainerElems() { return container_elems; };

  UInt subIndex2Index(UInt index) {
    LM_ASSERT(nodeIndexList.size() > 0,
              "nodeIndexList was not initialized: did you call "
                  << "computeAlteredConnectivity method ?");

    auto it = std::find(nodeIndexList.begin(), nodeIndexList.end(), index);
    if (it == nodeIndexList.end())
      return UINT_MAX;
    return it - nodeIndexList.begin();
  };

private:
  ContNodes container_nodes;
  ContElems container_elems;

  std::vector<UInt> nodeIndexList;
};

/* -------------------------------------------------------------------------- */
template <typename ContNodes, typename ContElems>
ContainerMesh<ContNodes, ContElems>::ContainerMesh(const LMID &id)
    : LMObject(id), container_nodes(id + ":nodes"),
      container_elems(id + ":elems") {}
/* -------------------------------------------------------------------------- */

template <typename ContNodes, typename ContElems>
ContainerMesh<ContNodes, ContElems>::~ContainerMesh() {}
/* -------------------------------------------------------------------------- */

template <typename ContNodes, typename ContElems>
template <typename ContNodesPristine>
void ContainerMesh<ContNodes, ContElems>::computeAlteredConnectivity(
    ContNodesPristine &contNodes) {

  std::set<UInt> nodeIndexList_set;
  for (auto &&el : container_elems) {
    auto &&connectivity = el.globalIndexes();
    for (UInt n = 0; n < connectivity.size(); ++n)
      nodeIndexList_set.insert(connectivity[n]);
  }
  
  nodeIndexList.clear();

  std::copy(nodeIndexList_set.begin(), nodeIndexList_set.end(),
            std::back_inserter(nodeIndexList));

  for (auto &&n_index : nodeIndexList) {
    container_nodes.push_back(contNodes.get(n_index));
  }

  // recompute the connectivity
  std::vector<UInt> altered_connectivity;

  for (UInt i = 0; i < container_elems.size(); ++i) {
    auto &el = container_elems.get(i);
    auto &&connectivity = el.globalIndexes();
    altered_connectivity.resize(connectivity.size());
    for (UInt n = 0; n < connectivity.size(); ++n) {
      auto it = std::find(nodeIndexList.begin(), nodeIndexList.end(),
                          connectivity[n]);
      altered_connectivity[n] = it - nodeIndexList.begin();
    }
    el.setAlteredConnectivity(altered_connectivity);
  }

  this->acquireContext(contNodes);
}
/* -------------------------------------------------------------------------- */

template <typename ContNodes, typename ContElems>
void ContainerMesh<ContNodes, ContElems>::setCommGroup(CommGroup group) {
  Container_base<typename ContNodes::Ref>::setCommGroup(group);
  container_nodes.setCommGroup(group);
  container_elems.setCommGroup(group);
}

/* -------------------------------------------------------------------------- */

template <UInt _Dim>
class RefGenericElem : public RefElement<RefGenericElem<_Dim>> {

public:
  static constexpr UInt Dim = _Dim;

  using fields = field_list<>;

  template <typename Vec> void setConnectivity(Vec &&conn) {
    auto n_nodes = conn.rows() * conn.cols();
    this->conn.clear();
    for (UInt i = 0; i < n_nodes; ++i)
      this->conn.push_back(conn[i]);
  }

  bool operator==(RefGenericElem &) {
    LM_TOIMPLEMENT;
    return true;
  };

  //! return the local indexes (in DOF vector) for connected nodes
  inline std::vector<UInt> localIndexes() { return this->conn; }
  //! return the global indexes (in DOF vector) for connected nodes
  virtual std::vector<UInt> globalIndexes() { return this->localIndexes(); };

  inline void setAlteredConnectivity(std::vector<UInt> &) { LM_TOIMPLEMENT; };
  inline std::vector<UInt> &getAlteredConnectivity() { LM_TOIMPLEMENT; };
  inline bool isAltered() { return false; };
  Real stress(UInt) { LM_TOIMPLEMENT; };
  Real strain(UInt) { LM_TOIMPLEMENT; };

  std::vector<UInt> conn;
};

template <UInt Dim>
using ContainerGenericMesh = ContainerMesh<ContainerArray<RefPointData<Dim>>,
                                           ContainerArray<RefGenericElem<Dim>>>;

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_CONTAINER_MESH_HH__ */
