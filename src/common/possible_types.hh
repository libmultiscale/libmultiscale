#include "auto_arguments.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
#include <boost/preprocessor.hpp>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
template <typename T, typename... Ts>
using enable_if_type = AutoDispatch::enable_if_type<T, Ts...>;
template <typename T, typename... Ts>
using enable_if_not_type = AutoDispatch::enable_if_not_type<T, Ts...>;
template <typename... Ts> using _or = AutoDispatch::_or<Ts...>;
template <typename... Ts> using _params = AutoDispatch::_params<Ts...>;

/* -------------------------------------------------------------------------- */

template <typename T> class ContainerArray;
template <typename ContNode, typename ContElem> class ContainerMesh;

/* -------------------------------------------------------------------------- */

// TODO : need to boost these declarations

/* -------------------------------------------------------------------------- */
// MD
/* -------------------------------------------------------------------------- */

// possible containers of MD
#ifdef LIBMULTISCALE_MD1D
class ContainerMD1D;
class RefMD1D;
class DomainMD1D;
#endif

#ifdef LIBMULTISCALE_LAMMPS_PLUGIN
template <UInt Dim> class ContainerLammps;
template <UInt Dim> class RefLammps;
template <UInt Dim> class DomainLammps;
#endif

using md_sub_containers = _or<
#ifdef LIBMULTISCALE_MD1D
    ContainerArray<RefMD1D>
#endif
#if defined(LIBMULTISCALE_LAMMPS_PLUGIN) && defined(LIBMULTISCALE_MD1D)
    ,
#endif
#ifdef LIBMULTISCALE_LAMMPS_PLUGIN
    ContainerArray<RefLammps<2>>, ContainerArray<RefLammps<3>>
#endif
    >;

using md_containers = _or<_or<
#ifdef LIBMULTISCALE_MD1D
                              ContainerMD1D
#endif
#if defined(LIBMULTISCALE_LAMMPS_PLUGIN) && defined(LIBMULTISCALE_MD1D)
                              ,
#endif
#ifdef LIBMULTISCALE_LAMMPS_PLUGIN
                              ContainerLammps<2>, ContainerLammps<3>
#endif
                              >,
                          md_sub_containers>;

using md_domains = _or<
#ifdef LIBMULTISCALE_MD1D
    DomainMD1D
#endif
#if defined(LIBMULTISCALE_LAMMPS_PLUGIN) && defined(LIBMULTISCALE_MD1D)
    ,
#endif
#ifdef LIBMULTISCALE_LAMMPS_PLUGIN
    DomainLammps<2>, DomainLammps<3>
#endif
    >;

using subMD = md_sub_containers;
using MD = md_containers;
using domMD = md_domains;

/* -------------------------------------------------------------------------- */
// DD
/* -------------------------------------------------------------------------- */

#ifdef LIBMULTISCALE_PARADIS_PLUGIN
class ContainerNodesParaDiS;
class ContainerElemsParaDiS;
class RefNodeParaDiS;
class RefElemParaDiS;
class DomainParaDiS;
#endif

// possible containers of DD
using dd_sub_containers = _or<
#ifdef LIBMULTISCALE_PARADIS_PLUGIN
    ContainerMesh<ContainerArray<RefNodeParaDiS>,
                  ContainerArray<RefElemParaDiS>>
#endif
    >;

using dd_containers = _or<
#ifdef LIBMULTISCALE_PARADIS_PLUGIN
    ContainerMesh<ContainerNodesParaDiS, ContainerElemsParaDiS>
#endif
    >;

using dd_domains = _or<
#ifdef LIBMULTISCALE_PARADIS_PLUGIN
    DomainParaDiS
#endif
    >;

using subDD = dd_sub_containers;
using DD = dd_containers;
using domDD = dd_domains;

/* -------------------------------------------------------------------------- */
// CONTINUUM
/* -------------------------------------------------------------------------- */

// possible containers of CONTINUUM
#ifdef LIBMULTISCALE_MECA1D
class ContainerElemsMeca1D;
class ContainerNodesMeca1D;
class RefNodeMeca1D;
class RefElemMeca1D;
class DomainMeca1D;
#endif

#ifdef LIBMULTISCALE_AKANTU_PLUGIN
template <UInt Dim> class ContainerElemsAkantu;
template <UInt Dim> class ContainerNodesAkantu;
template <UInt Dim> class RefNodeAkantu;
template <UInt Dim> class RefElemAkantu;
template <UInt Dim> class DomainAkantu;
#endif

using continuum_sub_containers = std::tuple<
#ifdef LIBMULTISCALE_MECA1D
    ContainerMesh<ContainerArray<RefNodeMeca1D>, ContainerArray<RefElemMeca1D>>
#endif
#if defined(LIBMULTISCALE_MECA1D) && defined(LIBMULTISCALE_AKANTU_PLUGIN)
    ,
#endif
#ifdef LIBMULTISCALE_AKANTU_PLUGIN
    ContainerMesh<ContainerArray<RefNodeAkantu<1>>,
                  ContainerArray<RefElemAkantu<1>>>,
    ContainerMesh<ContainerArray<RefNodeAkantu<2>>,
                  ContainerArray<RefElemAkantu<2>>>,
    ContainerMesh<ContainerArray<RefNodeAkantu<3>>,
                  ContainerArray<RefElemAkantu<3>>>
#endif
    >;

using continuum_containers =
    _or<std::tuple<
#ifdef LIBMULTISCALE_MECA1D
            ContainerMesh<ContainerNodesMeca1D, ContainerElemsMeca1D>
#endif
#if defined(LIBMULTISCALE_MECA1D) && defined(LIBMULTISCALE_AKANTU_PLUGIN)
            ,
#endif
#ifdef LIBMULTISCALE_AKANTU_PLUGIN
            ContainerMesh<ContainerNodesAkantu<1>, ContainerElemsAkantu<1>>,
            ContainerMesh<ContainerNodesAkantu<2>, ContainerElemsAkantu<2>>,
            ContainerMesh<ContainerNodesAkantu<3>, ContainerElemsAkantu<3>>
#endif
            >,
        continuum_sub_containers>;

using continuum_domains = std::tuple<
#ifdef LIBMULTISCALE_MECA1D
    DomainMeca1D
#endif
#if defined(LIBMULTISCALE_MECA1D) && defined(LIBMULTISCALE_AKANTU_PLUGIN)
    ,
#endif
#ifdef LIBMULTISCALE_AKANTU_PLUGIN
    DomainAkantu<1>, DomainAkantu<2>, DomainAkantu<3>
#endif
    >;

using subCONTINUUM = continuum_sub_containers;
using CONTINUUM = continuum_containers;
using domCONTINUUM = continuum_domains;

/* -------------------------------------------------------------------------- */
// GenericContainers
/* -------------------------------------------------------------------------- */

template <UInt Dim> class RefPoint;
template <UInt Dim> class RefPointData;
template <UInt Dim> class RefGenericElem;

template <UInt Dim>
using ContainerGenericMesh = ContainerMesh<ContainerArray<RefPointData<Dim>>,
                                           ContainerArray<RefGenericElem<Dim>>>;

template <UInt Dim> class RefGenericDDElem;
template <UInt Dim> class RefGenericDDNode;
template <UInt Dim>
using ContainerGenericDDMesh =
    ContainerMesh<ContainerArray<RefGenericDDNode<Dim>>,
                  ContainerArray<RefGenericDDElem<Dim>>>;

// possible containers of points
using point_containers =
    _or<ContainerArray<RefPointData<1u>>, ContainerArray<RefPointData<2u>>,
        ContainerArray<RefPointData<3u>>>;

using POINT = point_containers;

// possible containers of meshes
using mesh_containers = _or<ContainerGenericMesh<1u>, ContainerGenericMesh<2u>,
                            ContainerGenericMesh<3u>>;

using MESH = mesh_containers;

// possible containers of DD
using dd_mesh_containers = _or<ContainerGenericDDMesh<3u>>;
using DDMESH = dd_mesh_containers;

// possible computes as input
class Component;
using COMPONENT = _or<Component>;
using ARRAY = _or<ContainerArray<Real>>;

/* -------------------------------------------------------------------------- */

template <typename T> using enable_if_md = enable_if_type<_params<T>, MD>;
template <typename T>
using enable_if_not_md = enable_if_not_type<_params<T>, MD>;
template <typename T>
using enable_if_mesh =
    enable_if_type<_params<T>, _or<mesh_containers, CONTINUUM, DD>>;

template <typename T>
using enable_if_continuum = enable_if_type<_params<T>, CONTINUUM>;

template <typename T>
using enable_if_point = enable_if_type<_params<T>, _or<MD, POINT>>;

template <typename T> using enable_if_dd = enable_if_type<_params<T>, DD>;

template <typename T>
using enable_if_component = enable_if_type<_params<T>, COMPONENT>;

template <typename T>
using enable_if_not_component = enable_if_not_type<_params<T>, COMPONENT>;

/* --------------------------------------------------------------------- */

__END_LIBMULTISCALE__
/* --------------------------------------------------------------------------
 */
