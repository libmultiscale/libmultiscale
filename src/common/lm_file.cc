/**
 * @file   lm_file.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  File object useful to open/close read/write files in plain/compressed
 * mode
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "lm_log.hh"
#include "lm_file.hh"
#include <zlib.h>
#include <cstdio>
#include <cstdarg>
#include "lm_globals.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

void LMFile::printf(const char *formated, ...) {
  if (!opened) {
    LM_FATAL("Warning : file not opened " << name);
  }

  //  if (!opened) return;

  if (_file == NULL)
    LM_FATAL("file not opened");

  char buf[512];

  va_list list;
  va_start(list, formated);
  UInt len = vsprintf(buf, formated, list);
  va_end(list);

#ifdef LIBMULTISCALE_USE_ZLIB
  if (compressed) {
    gzwrite(gzfile, buf, len);
    return;
  } else
#endif
    fwrite(buf, len, 1, _file);
}
/* -------------------------------------------------------------------------- */

char *LMFile::gets(char *buf, UInt len) {
  char *ret;

  if (!opened) {
    LM_FATAL("Warning : file not opened " << name);
  }
  if (_file == NULL)
    LM_FATAL("file not opened");

  DUMP("read to buf (" << buf << ") at most " << len << " characters",
       DBG_DETAIL);
#ifdef LIBMULTISCALE_USE_ZLIB
  if (compressed) {
    ret = gzgets(gzfile, buf, len);
  } else
#endif
    ret = fgets(buf, len, _file);

  DUMP("read to buf (" << buf << ") at most " << len << " characters",
       DBG_DETAIL);
  return ret;
}

/* -------------------------------------------------------------------------- */

void LMFile::close() {
  //  if (!opened) FATAL("file not opened");
  if (!opened) {
    LM_FATAL("Warning : file not opened " << name);
  }

#ifdef LIBMULTISCALE_USE_ZLIB
  if (compressed) {
    gzclose(gzfile);
    gzfile = NULL;
  } else
#endif
    fclose(_file);

  _file = NULL;
  fd = UINT_MAX;
  opened = false;

  DUMP("file " << name << " closed", DBG_DETAIL);
}
/* -------------------------------------------------------------------------- */

void LMFile::open(const std::string &fname, const std::string &mode,
                  UInt compr) {
  std::string _mode = "wb";

  if (mode != "")
    _mode = mode;

  if (opened) {
    close();
  }

  compressed = compr;
  if (compressed) {
#ifdef LIBMULTISCALE_USE_ZLIB
    char buffer[256];
    sprintf(buffer, "%s.gz", fname.c_str());
    _file = fopen(buffer, _mode.c_str());
    if (_file == NULL)
      LM_THROW("Could not open file " << buffer);
    name = buffer;
#else
    LM_FATAL("cannot open a compressed file without ZLIB support: activate in "
             "CMake menu");
#endif
    // DUMP("file opened in compressed form " << name);
  } else {
    _file = fopen(fname.c_str(), _mode.c_str());
    if (_file == NULL)
      LM_THROW("Could not open file " << fname);
    name = fname;
  }

  opened = true;
  fd = fileno(_file);

#ifdef LIBMULTISCALE_USE_ZLIB
  if (compressed) {
    char temp[10];
    sprintf(temp, "%s%d", _mode.c_str(), 9);
    gzfile = gzdopen(fd, temp);
    if (gzfile == NULL)
      LM_FATAL("Could not open file descriptor using gzdopen(" << fd << ","
                                                               << temp << ")");
  }
#endif

  DUMP("file " << name << " opened , compressed = " << compressed, DBG_INFO);
}
/* -------------------------------------------------------------------------- */

int LMFile::dumpchar(int c) {
  if (!opened) {
    LM_FATAL("Warning : file not opened " << name);
  }

//  if (!opened) return EOF;

#ifdef LIBMULTISCALE_USE_ZLIB
  if (compressed) {
    int res = gzputc(gzfile, c);
    // DUMP("file opened in compressed form " << name);
    if (c != res)
      LM_FATAL("I did not write what I wanted (compressed) " << res);
    return res;
  } else
#endif
      if (c != putc(c, _file))
    LM_FATAL("I did not write what I wanted");
  return c;
}

/* -------------------------------------------------------------------------- */

UInt LMFile::seek(UInt offset, UInt set) {
  if (!opened) {
    LM_FATAL("Warning : file not opened " << name);
  }

#ifdef LIBMULTISCALE_USE_ZLIB
  if (compressed) {
    return gzseek(gzfile, offset, set);
  } else
#endif
    return fseek(_file, offset, set);
}

/* -------------------------------------------------------------------------- */

UInt LMFile::tell() {
  if (!opened) {
    LM_FATAL("Warning : file not opened " << name);
  }

#ifdef LIBMULTISCALE_USE_ZLIB
  if (compressed) {
    return gztell(gzfile);
  } else
#endif

    return ftell(_file);
}
/* -------------------------------------------------------------------------- */

UInt LMFile::read(void *buffer, UInt size, UInt number) {
  if (!opened) {
    LM_FATAL("Warning : file not opened " << name);
  }

#ifdef LIBMULTISCALE_USE_ZLIB
  if (compressed) {
    return gzread(gzfile, buffer, size * number);
  } else
#endif
    return fread(buffer, size, number, _file);
}

/* -------------------------------------------------------------------------- */

UInt LMFile::write(void *buffer, UInt size, UInt number) {
  if (!opened) {
    LM_FATAL("Warning : file not opened " << name);
  }

#ifdef LIBMULTISCALE_USE_ZLIB
  if (compressed) {
    return gzwrite(gzfile, buffer, size * number);
  } else
#endif
    return fwrite(buffer, size, number, _file);
}

/* -------------------------------------------------------------------------- */

void LMFile::flush() {
  if (!opened) {
    LM_FATAL("Warning : file not opened " << name);
  }

#ifdef LIBMULTISCALE_USE_ZLIB
  if (compressed) {
    gzflush(gzfile, Z_SYNC_FLUSH);
  } else
#endif
    fflush(_file);
}

/* -------------------------------------------------------------------------- */

LMFile::LMFile() {
  compressed = 0;
  _file = NULL;
  fd = UINT_MAX;
#ifdef LIBMULTISCALE_USE_ZLIB
  gzfile = NULL;
#endif
  opened = false;
}
/* -------------------------------------------------------------------------- */

LMFile::LMFile(const std::string &fname, const std::string &mode, UInt compr) {
  compressed = compr;
  _file = NULL;
  fd = UINT_MAX;
#ifdef LIBMULTISCALE_USE_ZLIB
  gzfile = NULL;
#endif
  opened = false;
  open(fname, mode, compressed);
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
