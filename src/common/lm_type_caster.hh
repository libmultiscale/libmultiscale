#ifndef __LIBMULTISCALE_TYPE_CASTER_HH__
#define __LIBMULTISCALE_TYPE_CASTER_HH__
/* -------------------------------------------------------------------------- */
#include "quantity.hh"
/* -------------------------------------------------------------------------- */
#include <map>
#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <vector>
/* -------------------------------------------------------------------------- */
namespace pybind11 {
namespace detail {

// template <libmultiscale::UInt Dim, typename T, typename Cast>
// std::enable_if_t<Dim == 1 and not std::is_same_v<T, bool>, bool>
// vector_load(Cast &value, PyObject *source) {
//   value = cast<T>(source);
//   return !PyErr_Occurred();
// }

// template <libmultiscale::UInt Dim, typename T, typename Cast>
// std::enable_if_t<Dim != 1 and not std::is_same_v<T, bool>, bool>
// vector_load(Cast &value, PyObject *source) {
//   auto tmp = cast<list>(source);
//   for (unsigned int i = 0; i < tmp.size(); ++i) {
//     value[i] = cast<T>(tmp[i]);
//   }
//   return !PyErr_Occurred();
// }

// // pybind cannot cast bool variables: needs a specialization
// template <libmultiscale::UInt Dim, typename T, typename Cast>
// std::enable_if_t<Dim == 1 and std::is_same_v<T, bool>, bool>
// vector_load(Cast &value, PyObject *source) {
//   value = cast<int>(source);
//   return !PyErr_Occurred();
// }

// template <libmultiscale::UInt Dim, typename T, typename Cast>
// std::enable_if_t<Dim != 1 and std::is_same_v<T, bool>, bool>
// vector_load(Cast &value, PyObject *source) {
//   auto tmp = cast<list>(source);
//   for (unsigned int i = 0; i < tmp.size(); ++i) {
//     value[i] = cast<int>(tmp[i]);
//   }
//   return !PyErr_Occurred();
// }

template <libmultiscale::PhysicalQuantity q, typename T>
struct type_caster<libmultiscale::PhysicalScalar<q, T>> {

  using type = libmultiscale::PhysicalScalar<q, T>;
  PYBIND11_TYPE_CASTER(type, _("physical_scalar"));

  bool load(handle src, bool) {
    this->value = ::pybind11::cast<T>(src);
    return !PyErr_Occurred();
  }

  static handle cast(type src, return_value_policy, handle) {
    return PyFloat_FromDouble(src);
  }
};

template <libmultiscale::PhysicalQuantity q, typename T>
struct npy_format_descriptor<libmultiscale::PhysicalScalar<q, T>>
    : public npy_format_descriptor<T> {};

template <libmultiscale::PhysicalQuantity q, libmultiscale::UInt Dim,
          typename T>
struct intrinsic_type<libmultiscale::Quantity<q, Dim, T>> {
  using type = typename Eigen::Matrix<libmultiscale::PhysicalScalar<q, T>, Dim,
                                      1u>::Matrix;
};

template <libmultiscale::PhysicalQuantity q, typename T>
struct intrinsic_type<libmultiscale::Quantity<q, 1, T>> {
  using type = typename libmultiscale::PhysicalScalar<q, T>;
};

template <libmultiscale::UInt Dim, typename T>
struct intrinsic_type<libmultiscale::Vector<Dim, T>> {
  using type = typename Eigen::Matrix<T, Dim, 1u>::Matrix;
};

// // pybind cannot cast bool variables: needs a specialization
// template <libmultiscale::UInt Dim, typename T, typename Cast>
// std::enable_if_t<Dim == 1 and std::is_same_v<T, bool>, bool>
// vector_load(Cast &value, PyObject *source) {
//   value = cast<int>(source);
//   return !PyErr_Occurred();
// }

// template <libmultiscale::UInt Dim, typename T, typename Cast>
// std::enable_if_t<Dim != 1 and std::is_same_v<T, bool>, bool>
// vector_load(Cast &value, PyObject *source) {
//   auto tmp = cast<list>(source);
//   for (unsigned int i = 0; i < tmp.size(); ++i) {
//     value[i] = cast<int>(tmp[i]);
//   }
//   return !PyErr_Occurred();
// }

// template <libmultiscale::PhysicalQuantity q, libmultiscale::UInt Dim,
//           typename T>
// struct type_caster<libmultiscale::Quantity<q, Dim, T>> {

//   using type = libmultiscale::Quantity<q, Dim, T>;
//   PYBIND11_TYPE_CASTER(type, _("quantity"));

//   bool load(handle src, bool) {
//     return vector_load<Dim, T>(this->value, src.ptr());
//   }

//   static handle cast(type src, return_value_policy, handle) {
//     return PyFloat_FromDouble(src);
//   }
// };

// template <libmultiscale::UInt Dim, typename T>
// struct type_caster<libmultiscale::Vector<Dim, T>> {

//   using type = libmultiscale::Vector<Dim, T>;
//   PYBIND11_TYPE_CASTER(type, _("vector"));

//   bool load(handle src, bool) {
//     return vector_load<Dim, T>(this->value, src.ptr());
//   }

//   static handle cast(type src, return_value_policy, handle) {
//     throw;

//     // return cast(src);
//   }
// };
} // namespace detail
} // namespace pybind11
/* -------------------------------------------------------------------------- */

#endif /* __LIBMULTISCALE_TYPE_CASTER_HH__ */
