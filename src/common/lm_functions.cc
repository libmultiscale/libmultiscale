/**
 * @file   lm_functions.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  This file contains the global scope functions
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "geometry.hh"
#include "geometry_manager.hh"
#include "lm_common.hh"
#include "lm_parser.hh"
#include <mpi.h>
#include <memory>
#include <cxxabi.h>
#include <regex>
#include <pybind11/pybind11.h>
#include <pybind11/embed.h>

__BEGIN_LIBMULTISCALE__

namespace py = pybind11;

/* -------------------------------------------------------------------------- */

[[noreturn]] void onTerminate() noexcept {

  std::string trace;
  if (auto exc = std::current_exception()) {
    // we have an exception
    try {
      rethrow_exception(exc); // throw to recognize the type
    } catch (LibMultiScaleException &e) {
      if(print_trace)
        trace = e.messageWithTrace();
      else
        trace = e.recursiveWhat();
    } catch (std::exception &ex) {
      auto e = LibMultiScaleException(ex.what());
      for (UInt i = 0; i < 4; ++i)
        e.stack_trace.stack.pop_front();

      if(print_trace)
        trace = e.messageWithTrace();
      else
        trace = e.what();
    } catch (...) {
      auto e = LibMultiScaleException(
          "LibMultiScale failed with an unknown exception");

      for (UInt i = 0; i < 4; ++i)
        e.stack_trace.stack.pop_front();
      if(print_trace)
        trace = e.messageWithTrace();
      else
        trace = e.what();
    }
  }
  std::cerr << std::endl
            << "LibMultiScale failed with the following message:" << std::endl
            << std::endl << trace;

  std::_Exit(EXIT_FAILURE);
}

/* -------------------------------------------------------------------------- */
// c++ typeinfo
/* -------------------------------------------------------------------------- */

std::string demangle(const char* name) {

    int status = -4; // some arbitrary value to eliminate the compiler warning
    std::unique_ptr<char, void(*)(void*)> res {
        abi::__cxa_demangle(name, NULL, NULL, &status),
        std::free
    };

    std::regex r("libmultiscale::");
    auto demangled_name = res.get();
    
    return (status==0) ? std::regex_replace(demangled_name,r,"") : name ;
}

/* -------------------------------------------------------------------------- */
// MPI macros
/* -------------------------------------------------------------------------- */
void loadMPI(int argc, char **argv) {
  int myrank, world, is_initialized;
  MPI_Initialized(&is_initialized);
  if (!is_initialized) {
    MPI_Init(&argc, &argv);
  }
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  lm_my_proc_id = (UInt)myrank;
  MPI_Comm_size(MPI_COMM_WORLD, &world);
  lm_world_size = (UInt)world;
}

void closeMPI() {
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
}

/* -------------------------------------------------------------------------- */
// environment macro
/* -------------------------------------------------------------------------- */

void loadENV() {
  char *varenv = lm_getenv("DEBUG_LEVEL");
  if (varenv)
    global_level = lm_atoi(varenv);
  else
    global_level = DBG_WARNING;
  varenv = lm_getenv("DEBUG_PROC");
  if (varenv)
    global_proc = lm_atoi(varenv);
  else
    global_proc = UINT_MAX;
  varenv = lm_getenv("DEBUG_PROC1");
  if (varenv)
    global_proc1 = lm_atoi(varenv);
  else
    global_proc1 = UINT_MAX;
  varenv = lm_getenv("DEBUG_PROC2");
  if (varenv)
    global_proc2 = lm_atoi(varenv);
  else
    global_proc2 = UINT_MAX;
  varenv = lm_getenv("DEBUG_START");
  if (varenv)
    start_dump = lm_atoi(varenv);
  else
    start_dump = 0;
  varenv = lm_getenv("DEBUG_END");
  if (varenv)
    end_dump = lm_atoi(varenv);
  else
    end_dump = UINT_MAX;
  varenv = lm_getenv("HOSTNAME");
  if (varenv)
    my_hostname = varenv;
  else
    my_hostname = "";
  varenv = lm_getenv("JOB_ID");
  if (varenv)
    lm_job_id = lm_atoi(varenv);
  else
    lm_job_id = lm_world_size;
  varenv = lm_getenv("PRINT_CRAP_TO_FILE");
  if (varenv)
    print_crap_to_file = true;
  else
    print_crap_to_file = false;
  varenv = lm_getenv("CREATE_SEG_FAULT");
  if (varenv)
    create_seg_fault = true;
  else
    create_seg_fault = false;
  varenv = lm_getenv("WAIT_ON_FATAL");
  if (varenv)
    wait_on_fatal = true;
  else
    wait_on_fatal = false;
  varenv = lm_getenv("WAIT_AT_STARTUP");
  if (varenv)
    wait_at_startup = lm_atoi(varenv);
  else
    wait_at_startup = UINT_MAX;
  varenv = lm_getenv("PRINT_TRACE");
  if (varenv)
    print_trace = true;
  else
    print_trace = false;
  openGlobal();
}

/* -------------------------------------------------------------------------- */
/// EPSN init macro
/* -------------------------------------------------------------------------- */
#ifdef LIBMULTISCALE_USE_EPSN
#include "Epsn..hh"
#include "RedSYM..hh"
EXTERN char xmlepsnfile[255];
EXTERN EPSN::Simulation::Interface *epsn_itfc;

inline void loadEPSN() {
  epsn_itfc = NULL;
  epsn_itfc = new EPSN::Simulation::Interface;
  epsn_itfc->initORB(argc, argv);
}

inline closeEPSN() {
  epsn_itfc->finalize();
  epsn_itfc->killORB();
  delete epsn_itfc;
}
#else
void loadEPSN() {}
void closeEPSN() {}
#endif

/* -------------------------------------------------------------------------- */
/// Python init macro
/* -------------------------------------------------------------------------- */
bool embedded_interpreter = false;

void loadPython(){
  if (not Py_IsInitialized()){
    py::initialize_interpreter();
    embedded_interpreter = true;
  }
}

void closePython(){
  if (embedded_interpreter) {
    //py::finalize_interpreter();
    DUMP("WARNING: Python interpreter will not be closed. This is a temporary fix, see #18.",
         DBG_WARNING);
  }
}

/* -------------------------------------------------------------------------- */
// init functions
/* -------------------------------------------------------------------------- */
void loadModules(int argc, char **argv) {
  std::set_terminate(&onTerminate);
  loadMPI(argc, argv);
  loadEPSN();
  loadENV();
  loadPython();
}

void closeModules() {
  GeometryManager::destroy();
  Parser::freeMemory();
  closeEPSN();
#ifdef LIBMULTISCALE_TIMER
  dumpTimes(lm_my_proc_id);
#endif
  closeMPI();
  closePython();
  (*global_out).flush();
}

/* -------------------------------------------------------------------------- */
// terminate exception function
/* -------------------------------------------------------------------------- */


__END_LIBMULTISCALE__
