#ifndef __LM_PYTHON_BINDINGS_HH__
#define __LM_PYTHON_BINDINGS_HH__
/* -------------------------------------------------------------------------- */
#include "lm_type_caster.hh"
#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
/* -------------------------------------------------------------------------- */
#include "lm_python_actions.hh"
#include "lm_python_components.hh"
#include "lm_python_containers.hh"
#include "lm_python_couplers.hh"
#include "lm_python_domains.hh"
#include "lm_python_geometries.hh"
#include "lm_python_types.hh"
/* -------------------------------------------------------------------------- */

namespace py = pybind11;

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

inline void makeBindings(py::module &m) {
  m.attr("prologue") = py::module::import("prologue");

  ////////////////////////////////////////////////////////////////

  declare_types(m);
  declare_components(m);
  declare_containers(m);
  declare_actions(m);
  declare_geometries(m);
  declare_domains(m);
  declare_couplers(m);

  ////////////////////////////////////////////////////////////////
}

__END_LIBMULTISCALE__

#endif //__LM_PYTHON_BINDINGS_HH__
