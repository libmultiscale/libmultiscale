/**
 * @file   action_interface.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  This is the interface to all action objects
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "action_interface.hh"
#include "factory_multiscale.hh"
#include "filter_interface.hh"
#include "lm_common.hh"

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
ActionInterface::ActionInterface() { setDefaults(); }
/* -------------------------------------------------------------------------- */

void ActionInterface::printself(std::ostream &stream) const {
  stream << "Action " << this->getID() << " " << typeid(*this).name()
         << "action_step " << action_step << " "
         << "start_step " << start_step << " "
         << "end_step " << end_step << " "
         << "frequency " << frequency << std::endl;
}

/* -------------------------------------------------------------------------- */

void ActionInterface::setDefaults() {
  action_step = 0;
  frequency = 1;
  start_step = 0;
  end_step = UINT_MAX;
  one_shot = UINT_MAX;
}
/* -------------------------------------------------------------------------- */

bool ActionInterface::shouldMakeAction() {
  bool amIinGroup = comm_group.amIinGroup();
  if (not amIinGroup)
    return false;

  if ((current_stage == PRE_FATAL) && (stage_mask & PRE_FATAL))
    return true;
  bool sM = doesStageMaskMatch();
  if (!sM)
    return false;

  setOneStep();

  DUMP("for action " << this->getID() << " freq " << frequency << " step "
                     << action_step,
       DBG_INFO);
  if (end_step >= current_step && start_step <= current_step)
    if (frequency != UINT_MAX && current_step % frequency == 0) {
      DUMP("should proceed action "
               << this->getID() << " current_step = " << current_step
               << " start " << start_step << " end " << end_step,
           DBG_INFO);
      return true;
    }
  return false;
}

/* -------------------------------------------------------------------------- */

void ActionInterface::setOneStep() {

  if (one_shot != UINT_MAX)
    end_step = start_step = one_shot;
}

/* -------------------------------------------------------------------------- */

bool ActionInterface::doesStageMaskMatch() {
  if (stage_mask.isNone())
    stage_mask = PRE_STEP1;
  if (!(stage_mask & current_stage))
    return false;
  return true;
}

/* -------------------------------------------------------------------------- */

void ActionInterface::action() {
  this->compute();
  ++action_step;
}
/* -------------------------------------------------------------------------- */
FactoryMultiScale<ActionInterface> &ActionInterface::getManager() {
  return FactoryMultiScale<ActionInterface>::getManager();
}

/* -------------------------------------------------------------------------- */

/* LMDESC ActionInterface
   This describes the generic keywords which all actions
   inheritate from.
*/

void ActionInterface::declareParams() {

  /* LMKEYWORD INPUT
     Specify the input(s) of the Action component
  */
  this->parseKeyword("INPUT", static_cast<Component &>(*this));
  this->makeItOptional("INPUT");

  /* LMKEYWORD FREQ
     Fix the frequency at which action should be called
  */
  this->parseKeyword("FREQ", frequency, 1u);

  /* LMKEYWORD START
     Fix the first step at which action should start
  */
  this->parseKeyword("START", start_step, 0u);
  /* LMKEYWORD END
     Fix the last step at which action should be performed
  */
  this->parseKeyword("END", end_step, lm_uint_max);
  /* LMKEYWORD ONESHOT
     Set the action to be called only at a given step.
  */
  this->parseKeyword("ONESHOT", one_shot, lm_uint_max);

  /* LMKEYWORD STAGE
     Set the stage within generic explicit integration scheme at
     which the action should be called. The possible values are:

     - PRE_DUMP
     - PRE_STEP1
     - PRE_STEP2
     - PRE_STEP3
     - PRE_STEP4
     - PRE_STEP5
     - PRE_STEP6
     - PRE_STEP7

     The keyword STAGE can be called more than once and the result will be a
     bit-mask value
     so that an action can be called at several stages within a given
     timestep.

     These stages are identified depending on the implemented integration
     scheme.
     For instance the classical Verlet integration scheme is coded in a loop in
     the AMEL executable as follows:

     .. code-block:: cpp

        for (int current_step = 0; current_step < nb_step ; ++current_step,
        current_time += dt){

          stimulator.stimulate(PRE_DUMP);
          dumper.dump();
          stimulator.stimulate(PRE_STEP1);
          dom.performStep1();
          dom.coupling(COUPLING_STEP1);

          stimulator.stimulate(PRE_STEP2);
          dom.performStep2();
          dom.coupling(COUPLING_STEP2);

          stimulator.stimulate(PRE_STEP3);
          dom.performStep3();

          dom.coupling(COUPLING_STEP3);
          stimulator.stimulate(PRE_STEP4);
          dom.coupling(COUPLING_STEP4);
        }

     For a classical Verlet integration scheme performStep1() is the initial
     velocity and
     position update, performStep2() is the force computation, and
     performStep3() is the
     velocity correction.

     If the stage value is kept to NONE, the default of PRE\_STEP1 will apply
     automatically
  */

  this->parseKeyword("STAGE", stage_mask, NONE_STEP);
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
