/**
 * @file   ref_point.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  This is the class for reference that are any reference using a
 * point-like description should inheritate from
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_REF_POINT_HH__
#define __LIBMULTISCALE_REF_POINT_HH__
/* -------------------------------------------------------------------------- */
#include "cube.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

template <UInt _Dim> class RefPoint {

public:
  static constexpr UInt Dim = _Dim;

  RefPoint(){};
  virtual ~RefPoint(){};

  //! get the distance between the current reference and a point, pbc corrected
  template <bool pbc_x, bool pbc_y, bool pbc_z, typename Vec1, typename Vec2>
  static inline Real computeDistance(Vec1 &&pos1, Vec2 &&pos2,
                                     Cube &bounding_box);

  //! return the dof_type
  virtual int getDOFType() { return dt_master; };

protected:
  //! compute the distance between points pbc corrected (internal)
  template <bool pbc_x, bool pbc_y, bool pbc_z, typename Vec1, typename Vec2,
            typename Vec3, typename Vec4>
  static inline Real computeDistance(Vec1 &&pos1, Vec2 &&pos2, Vec3 &&d_size,
                                     Vec4 &&d_size_half);
};

/* -------------------------------------------------------------------------- */

template <UInt Dim>
template <bool pbc_x, bool pbc_y, bool pbc_z, typename Vec1, typename Vec2,
          typename Vec3, typename Vec4>
inline Real RefPoint<Dim>::computeDistance(Vec1 &&pos1, Vec2 &&pos2,
                                           Vec3 &&d_size,
                                           Vec4 &&d_inv_size_half) {
  auto &x = pos1;
  Real dx[Dim];
  Real d2 = 0.0;
  for (UInt i = 0; i < Dim; ++i) {
    dx[i] = x[i] - pos2[i];
    if ((pbc_x && i == 0) || (pbc_y && i == 1) || (pbc_z && i == 2)) {
      int offset = static_cast<int>(dx[i] * d_inv_size_half[i]);
      dx[i] -= offset * d_size[i];
    }
    d2 += dx[i] * dx[i];
  }
  return d2;
}
/* ------------------------------------------------------------------------ */

template <UInt Dim>
template <bool pbc_x, bool pbc_y, bool pbc_z, typename Vec1, typename Vec2>
inline Real RefPoint<Dim>::computeDistance(Vec1 &&pos1, Vec2 &&pos2,
                                           Cube &bounding_box) {
  return computeDistance<pbc_x, pbc_y, pbc_z>(
      pos1, pos2, bounding_box.getSize(), bounding_box.getInvSizeHalf());
}

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_REF_POINT_HH__ */
