#ifndef __LM_PYTHON_ACTIONS_HH__
#define __LM_PYTHON_ACTIONS_HH__
/* -------------------------------------------------------------------------- */
#include "action_interface.hh"
#include "factory_multiscale.hh"
#include "filter_interface.hh"
#include "lib_dumper.hh"
#include "lib_filter.hh"
#include "lib_stimulation.hh"
#include "stimulation_interface.hh"
/* -------------------------------------------------------------------------- */
#include "compute_list.hh"
#include "dumper_list.hh"
#include "filter_list.hh"
#include "stimulation_list.hh"
/* -------------------------------------------------------------------------- */
#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
/* -------------------------------------------------------------------------- */

namespace py = pybind11;

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

template <typename Class, typename Mother>
inline void declare_action(py::module &m, const char *class_name) {
  py::class_<Class, Mother, std::shared_ptr<Class>>(m, class_name,
                                                    py::multiple_inheritance())
      .def(py::init<LMID>())
      .def("init", [](Class &self) {
        self.checkAllKeywordsAreParsed();
        self.init();
      });
}
/* -------------------------------------------------------------------------- */

inline void declare_actions(py::module &m) {

  py::class_<ActionInterface, Parsable, Component,
             std::shared_ptr<ActionInterface>>(m, "ActionInterface",
                                               py::multiple_inheritance())
      .def("action", &ActionInterface::action);

  py::class_<StimulationInterface, ActionInterface,
             std::shared_ptr<StimulationInterface>>(m, "StimulationInterface",
                                                    py::multiple_inheritance())
      .def("stimulate", &StimulationInterface::stimulate);

  py::class_<FilterInterface, ActionInterface,
             std::shared_ptr<FilterInterface>>(m, "FilterInterface",
                                               py::multiple_inheritance());

  py::class_<DumperInterface, ActionInterface,
             std::shared_ptr<DumperInterface>>(m, "DumperInterface",
                                               py::multiple_inheritance())
      .def("dump", &DumperInterface::dump);

  py::class_<ComputeInterface, FilterInterface,
             std::shared_ptr<ComputeInterface>>(m, "ComputeInterface",
                                                py::multiple_inheritance());

  auto clean_class_name = [](const std::string &arg) {
    std::string res = arg;

    res.erase(std::remove(res.begin(), res.end(), '<'), res.end());
    res.erase(std::remove(res.begin(), res.end(), '>'), res.end());
    return res;
  };

#define PYTHON_STIMULATION(n, data, obj)                                       \
  {                                                                            \
    using _class = BOOST_PP_TUPLE_ELEM(2, 0, obj);                             \
    std::string stimulation_name =                                             \
        clean_class_name(BOOST_PP_STRINGIZE(BOOST_PP_TUPLE_ELEM(2, 0, obj)));  \
                                                                               \
    declare_action<_class, StimulationInterface>(m, stimulation_name.c_str()); \
  }

  BOOST_PP_SEQ_FOR_EACH(PYTHON_STIMULATION, f, LIST_STIMULATION);
#undef PYTHON_STIMULATION

#define PYTHON_COMPUTE(n, data, obj)                                           \
  {                                                                            \
    using _class = BOOST_PP_TUPLE_ELEM(2, 0, obj);                             \
    std::string compute_name =                                                 \
        clean_class_name(BOOST_PP_STRINGIZE(BOOST_PP_TUPLE_ELEM(2, 0, obj)));  \
                                                                               \
    declare_action<_class, ComputeInterface>(m, compute_name.c_str());         \
  }
  BOOST_PP_SEQ_FOR_EACH(PYTHON_COMPUTE, f, LIST_COMPUTE);
#undef PYTHON_COMPUTE

#define PYTHON_FILTER(n, data, obj)                                            \
  {                                                                            \
    using _class = BOOST_PP_TUPLE_ELEM(2, 0, obj);                             \
    std::string filter_name =                                                  \
        clean_class_name(BOOST_PP_STRINGIZE(BOOST_PP_TUPLE_ELEM(2, 0, obj)));  \
                                                                               \
    declare_action<_class, FilterInterface>(m, filter_name.c_str());           \
  }
  BOOST_PP_SEQ_FOR_EACH(PYTHON_FILTER, f, LIST_FILTER);
#undef PYTHON_FILTER

#define PYTHON_DUMPER(n, data, obj)                                            \
  {                                                                            \
    using _class = BOOST_PP_TUPLE_ELEM(2, 0, obj);                             \
    std::string dumper_name =                                                  \
        clean_class_name(BOOST_PP_STRINGIZE(BOOST_PP_TUPLE_ELEM(2, 0, obj)));  \
                                                                               \
    declare_action<_class, DumperInterface>(m, dumper_name.c_str());           \
  }
  BOOST_PP_SEQ_FOR_EACH(PYTHON_DUMPER, f, LIST_DUMPER);
#undef PYTHON_DUMPER

  py::class_<ActionManager, std::shared_ptr<ActionManager>>(m, "ActionManager")
      .def_static("getManager", py::overload_cast<>(&ActionManager::getManager),
                  py::return_value_policy::reference)
      .def_static("getManager", &ActionManager::getManager,
                  py::return_value_policy::reference)
      .def("action", &ActionManager::action)
      .def("destroy", &ActionManager::destroy)
      .def("addObject", py::overload_cast<std::shared_ptr<ActionInterface>>(
                            &ActionManager::addObject));

  py::class_<FilterManager, std::shared_ptr<FilterManager>>(m, "FilterManager")
      .def_static("getManager", py::overload_cast<>(&FilterManager::getManager),
                  py::return_value_policy::reference)
      .def_static("getManager", &FilterManager::getManager,
                  py::return_value_policy::reference)
      .def("action", &FilterManager::action)
      .def("destroy", &FilterManager::destroy)
      .def("addObject", py::overload_cast<std::shared_ptr<FilterInterface>>(
                            &FilterManager::addObject));
}

__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

#endif //__LM_PYTHON_ACTIONS_HH__
