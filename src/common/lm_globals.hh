/**
 * @file   lm_globals.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  This file contains all the global variables of LM
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_LM_GLOBALS_HH__
#define __LIBMULTISCALE_LM_GLOBALS_HH__
/* -------------------------------------------------------------------------- */
#include "lm_macros.hh"
#include "lm_types.hh"
#include <iostream>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

static const std::string invalidFilter = "invalid filter";
static const std::string invalidGeom = "invalid geometry";
static const std::string invalidDomain = "invalid domain";
static const std::string invalidDumper = "invalid dumper";
static const std::string invalidStimulator = "invalid stimulator";

using LMID = std::string;
/* -------------------------------------------------------------------------- */
// global variables
EXTERN UInt current_step;
EXTERN IntegrationSchemeStage current_stage;
EXTERN Real current_time;
EXTERN UInt nb_step;
EXTERN UInt nb_step_next_event;
EXTERN UInt global_level;
EXTERN UInt global_mod;
EXTERN UInt global_proc;
EXTERN UInt global_proc1;
EXTERN UInt global_proc2;
EXTERN UInt start_dump;
EXTERN UInt end_dump;
EXTERN std::string my_hostname;
EXTERN UInt print_crap_to_file;
EXTERN std::ostream *global_out;
EXTERN std::ofstream file_out;
EXTERN UInt wait_on_fatal;
EXTERN UInt wait_at_startup;
EXTERN UInt create_seg_fault;
EXTERN UInt lm_my_proc_id;
EXTERN UInt lm_world_size;
EXTERN UInt lm_job_id;
EXTERN UInt spatial_dimension;
EXTERN UInt lm_uint_max;
EXTERN Real lm_real_max;
/// used to avoid infinite FATAL recursion in stage PRE_FATAL
EXTERN bool fatal_loop;
EXTERN bool print_trace;

/* -------------------------------------------------------------------------- */
EXTERN UInt getGlobalCurrentRelease();
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_LM_GLOBALS_HH__ */
