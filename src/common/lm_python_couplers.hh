#ifndef __LM_PYTHON_COUPLERS_HH__
#define __LM_PYTHON_COUPLERS_HH__
/* -------------------------------------------------------------------------- */
#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
/* -------------------------------------------------------------------------- */
#include "arlequin_python.hh"
#include "bridging.hh"
#include "coupler_manager.hh"
#include "coupling_atomic_continuum_python.hh"
#include "xiao.hh"
/* -------------------------------------------------------------------------- */

namespace py = pybind11;

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

inline void declare_couplers(py::module &m) {
  py::class_<DofAssociation, Parsable, Component,
             std::shared_ptr<DofAssociation>>(m, "DofAssociation",
                                              py::multiple_inheritance())
      .def("synchronizeVectorBySum", &DofAssociation::synchronizeVectorBySum);

  py::class_<Bridging, DofAssociation, std::shared_ptr<Bridging>>(
      m, "Bridging", py::multiple_inheritance())
      .def(py::init<const std::string &>())
      .def("setPBCPairs", &Bridging::setPBCPairs)
      .def("init",
           [](Bridging &self, ContainerInterface &domA,
              ContainerInterface &domB) {
             self.checkAllKeywordsAreParsed();
             self.init<dispatch>(domA, domB);
           })
      .def("mesh2Point", py::overload_cast<FieldType, ContainerArray<Real> &>(
                             &Bridging::mesh2Point))
      .def("mesh2Point", py::overload_cast<const ContainerArray<Real> &,
                                           ContainerArray<Real> &, bool>(
                             &Bridging::mesh2Point))
      .def_property_readonly(
          "pointList", [](Bridging &self) { return self.pointList; },
          py::return_value_policy::reference)
      .def_property_readonly(
          "meshList", [](Bridging &self) { return self.meshList; },
          py::return_value_policy::reference)
      .def_readwrite("buffer_for_nodes", &Bridging::buffer_for_nodes,
                     py::return_value_policy::reference)
      .def_readwrite("buffer_for_points", &Bridging::buffer_for_points,
                     py::return_value_policy::reference)
      .def_property_readonly(
          "unmatchedPointList",
          [](Bridging &self) { return self.unmatchedPointList; },
          py::return_value_policy::reference)
      .def_property_readonly(
          "unmatchedMeshList",
          [](Bridging &self) { return self.unmatchedMeshList; },
          py::return_value_policy::reference)
      .def_readonly("smatrix", &Bridging::smatrix)
      .def("attachVector", &Bridging::attachVector)
      .def("setPBCSlaveFromMaster",
           [](Bridging &self, Eigen::ArrayXXd &mat) {
             self.setPBCSlaveFromMaster(mat);
           })
      .def("setPBCSlaveFromMaster",
           [](Bridging &self, ContainerArray<Real> &mat) {
             self.setPBCSlaveFromMaster(mat);
           })
      .def("setPointField",
           py::overload_cast<FieldType, Real, UInt>(&Bridging::setPointField))
      .def("setPointField",
           py::overload_cast<FieldType, ContainerArray<Real> &>(
               &Bridging::setPointField))
      .def("addPointField",
           py::overload_cast<FieldType, ContainerArray<Real> &>(
               &Bridging::addPointField))
      .def("setPointField", py::overload_cast<FieldType, Eigen::VectorXd>(
                                &Bridging::setPointField))
      .def("setMeshField", py::overload_cast<FieldType, ContainerArray<Real> &>(
                               &Bridging::setMeshField))
      .def("addMeshField", py::overload_cast<FieldType, ContainerArray<Real> &>(
                               &Bridging::addMeshField))
      .def("setMeshField", py::overload_cast<FieldType, Eigen::VectorXd>(
                               &Bridging::setMeshField));

  py::class_<BridgingAtomicContinuum, Bridging,
             std::shared_ptr<BridgingAtomicContinuum>>(
      m, "BridgingAtomicContinuum", py::multiple_inheritance())
      .def("init",
           [](BridgingAtomicContinuum &self, ContainerInterface &domA,
              ContainerInterface &domB) {
             self.checkAllKeywordsAreParsed();
             self.init<dispatch>(domA, domB);
           })
      .def("projectAtomicFieldOnMesh",
           py::overload_cast<FieldType>(
               &BridgingAtomicContinuum::projectAtomicFieldOnMesh))
      .def("projectAtomicFieldOnMesh",
           py::overload_cast<FieldType, ContainerArray<Real> &>(
               &BridgingAtomicContinuum::projectAtomicFieldOnMesh))
      .def("updateForMigration", &BridgingAtomicContinuum::updateForMigration);

  /* --------------------------------------------------------------------- */

  py::class_<CouplingInterface, Parsable, Component,
             std::shared_ptr<CouplingInterface>>(m, "CouplingInterface",
                                                 py::multiple_inheritance());
  py::class_<CouplingAtomicContinuum, CouplingInterface,
             std::shared_ptr<CouplingAtomicContinuum>>(
      m, "CouplingAtomicContinuum_internal", py::multiple_inheritance());

  py::class_<ArlequinTemplate, CouplingAtomicContinuum,
             std::shared_ptr<ArlequinTemplate>>(m, "ArlequinTemplate",
                                                py::multiple_inheritance())
      .def_property_readonly(
          "bridging_zone",
          [](ArlequinTemplate & self) -> auto & { return self.bridging_zone; },
          py::return_value_policy::reference)
      .def_property_readonly(
          "boundary_zone",
          [](ArlequinTemplate & self) -> auto & { return self.boundary_zone; },
          py::return_value_policy::reference);

  py::class_<Xiao, ArlequinTemplate, std::shared_ptr<Xiao>>(
      m, "Xiao", py::multiple_inheritance())
      .def(py::init<const std::string &>())
      .def("init",
           [](Xiao &self, DomainInterface &domA, DomainInterface &domC) {
             self.checkAllKeywordsAreParsed();
             self.init<dispatch>(domA, domC);
           })
      .def("buildRHS", &Xiao::buildRHS)
      .def("buildConstraintMatrix", &Xiao::buildConstraintMatrix);

  py::class_<ArlequinPython, ArlequinTemplate, std::shared_ptr<ArlequinPython>>(
      m, "ArlequinPython", py::multiple_inheritance())
      .def(py::init<const std::string &>())
      .def("coupling", [](ArlequinPython &self,
                          CouplingStage stage) { self.coupling(stage); })
      .def("init",
           [](ArlequinPython &self, DomainInterface &domA,
              DomainInterface &domC) {
             self.checkAllKeywordsAreParsed();
             self.init<dispatch>(domA, domC);
           })
      .def("init",
           static_cast<void (ArlequinPython::*)()>(&ArlequinPython::init))
      .def_property_readonly(
          "bridging_zone",
          [](ArlequinTemplate & self) -> auto & { return self.bridging_zone; },
          py::return_value_policy::reference)
      .def_property_readonly(
          "boundary_zone",
          [](ArlequinTemplate & self) -> auto & { return self.boundary_zone; },
          py::return_value_policy::reference)
      .def_property_readonly("boundary_geom",
                             &ArlequinPython::get_boundary_geom)
      .def_property_readonly("bridging_geom",
                             &ArlequinPython::get_bridging_geom)
      .def("is_in_atomic", &ArlequinPython::is_in_atomic)
      .def("is_in_continuum", &ArlequinPython::is_in_continuum)
      .def("computeAtomicWeights", py::overload_cast<ContainerInterface &>(
                                       &ArlequinPython::computeAtomicWeights))
      .def("computeContinuumWeights",
           py::overload_cast<ContainerInterface &>(
               &ArlequinPython::computeContinuumWeights))
      .def("computeAtomicWeights", py::overload_cast<OutputContainer &>(
                                       &ArlequinPython::computeAtomicWeights))
      .def("computeContinuumWeights",
           py::overload_cast<OutputContainer &>(
               &ArlequinPython::computeContinuumWeights))
      .def_property_readonly("lambdas_mesh", &ArlequinPython::get_lambdas_mesh)
      .def_property_readonly("lambdas_point",
                             &ArlequinPython::get_lambdas_point);

  py::class_<CouplingAtomicContinuumPython, CouplingAtomicContinuum,
             std::shared_ptr<CouplingAtomicContinuumPython>>(
      m, "CouplingAtomicContinuum", py::multiple_inheritance())
      .def(py::init<const std::string &>())
      .def("coupling", [](CouplingAtomicContinuumPython &self,
                          CouplingStage stage) { self.coupling(stage); })
      .def("init", static_cast<void (CouplingAtomicContinuumPython::*)()>(
                       &CouplingAtomicContinuumPython::init))
      .def("init",
           [](CouplingAtomicContinuumPython &self, DomainInterface &domA,
              DomainInterface &domC) {
             self.checkAllKeywordsAreParsed();
             self.init<dispatch>(domA, domC);
           })
      .def("is_in_atomic", &CouplingAtomicContinuumPython::is_in_atomic)
      .def("is_in_continuum", &CouplingAtomicContinuumPython::is_in_continuum);

  py::class_<CouplerManager, std::shared_ptr<CouplerManager>>(m,
                                                              "CouplerManager")
      .def_static("getManager", &CouplerManager::getManager,
                  py::return_value_policy::reference)
      .def("getObject", &CouplerManager::getObject,
           py::return_value_policy::reference)
      .def("addObject", py::overload_cast<std::shared_ptr<CouplingInterface>>(
                            &CouplerManager::addObject));
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif //__LM_PYTHON_COUPLERS_HH__
