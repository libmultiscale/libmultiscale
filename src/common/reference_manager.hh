/**
 * @file   reference_manager.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  This is the manager of reference and coherency with migrations
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_REFERENCE_MANAGER_HH__
#define __LIBMULTISCALE_REFERENCE_MANAGER_HH__
/* -------------------------------------------------------------------------- */
#include "auto_arguments.hh"
#include "possible_types.hh"
#include "ref_subset.hh"
#include "lm_pack_buffer.hh"
#include "reference_manager_interface.hh"
/* -------------------------------------------------------------------------- */
#include <map>
#include <mpi.h>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

template <typename Ref>
class ReferenceManager : public ReferenceManagerInterface {

public:
  using BufferMap = std::map<UInt, LMPackBuffer>;
  using MapRefToUInt = std::map<Ref, UInt, typename Ref::RefComparator>;
  using MapRefToRef = std::map<Ref, Ref, typename Ref::RefComparator>;
  using MapUIntToRefList = std::map<UInt, std::vector<Ref>>;

  ReferenceManager(typename Ref::Domain::ContainerPoints &global_container);
  virtual ~ReferenceManager() = default;

  //! function that start the updating process of all attached structures
  void updateRefSubSets();

  //! request to manage a subset
  DECORATE_FUNCTION_DISPATCH(addSubset, _or<subMD>)
  template <typename Cont>
  std::enable_if_t<std::is_same_v<typename Cont::Ref, Ref>>
  addSubset(Cont &cont);

  template <typename Cont>
  std::enable_if_t<not std::is_same_v<typename Cont::Ref, Ref>>
  addSubset(Cont &) {
    LM_FATAL("wrong reference type";)
  }

  void addSubset(ContainerInterface &cont) override;

  //! request to remove a subset
  void removeSubset(const LMID &name) override;

  void attachObject(std::shared_ptr<AttachedObject> obj,
                    ContainerInterface &c) override;
  //! request detaching a generic AttachedObject with  a given container c
  void detachObject(std::shared_ptr<AttachedObject> obj,
                    ContainerInterface &c) override;

  //! print bilan for concerned container : used only to debug
  // void printBilan();
  //! retreive the mpi context
  void acquireContext(const LMObject &obj) override;
  //! compute the masks of every reference attached with subsets
  void computeMasks();

protected:
  //! set the mpi communicator
  void setCommGroup(CommGroup comm);
  //! generic communication routine to send/recv a bunch of buffers
  // void exchangeBuffers(BufferMap &toSend, BufferMap &toRecv);

  //! translate the references for moved refs
  // void translateMovingReferences();
  //! pack masks to a BufferMap
  // void packMasks(MapRefToUInt &masks);
  //! unpack masks to a BufferMap
  // void unpackMasks(MapRefToUInt &masks);
  //! clear the buffers and/or create one entry per proc I should com with
  // void clearPackBuffers();

protected:
  //! global set
  // RefSet<GlobalContainer> global_set;
  //! subset array
  std::map<LMID, std::shared_ptr<RefSubsetInterface<Ref>>> subsets;
  //! mapping between sent ref ref and new proc owner
  // MapRefToUInt sent;
  //! inverse mapping : sent refs sorted by receiving processors
  // MapUIntToRefList sent_byproc;
  //! new refs generated by migration (received)
  // MapUIntToRefList newrefs;
  //! communication buffers to be sent
  // BufferMap buffers_tosend;
  //! communication buffers to be received
  // BufferMap buffers_torecv;
  //! mapping between moved refs old ref and new ref
  // MapRefToRef moved;
  //! flag to notify if updating of attached references needed
  // bool have_changed = false;
  //! for debug purpose
  // void printPackBuffersStatus();
  typename ReferenceManager<Ref>::MapRefToUInt masks;

private:
  CommGroup *comm_group;
};
/* -------------------------------------------------------------------------- */

template <UInt Dim> class RefPointData;

template <UInt Dim>
struct ReferenceManager<RefPointData<Dim>> : public ReferenceManagerInterface {
  void removeSubSet(const LMID &){};
};
/* -------------------------------------------------------------------------- */

template <UInt Dim> class RefGenericElem;

template <UInt Dim>
struct ReferenceManager<RefGenericElem<Dim>>
    : public ReferenceManagerInterface {
  void removeSubSet(const LMID &){};
};
/* -------------------------------------------------------------------------- */

template <UInt Dim> class RefGenericDDElem;

template <UInt Dim>
struct ReferenceManager<RefGenericDDElem<Dim>>
    : public ReferenceManagerInterface {
  void removeSubSet(const LMID &){};
};
/* -------------------------------------------------------------------------- */

template <UInt Dim> class RefGenericDDNode;

template <UInt Dim>
struct ReferenceManager<RefGenericDDNode<Dim>>
    : public ReferenceManagerInterface {
  void removeSubSet(const LMID &){};
};
/* -------------------------------------------------------------------------- */

template <> struct ReferenceManager<double> : public ReferenceManagerInterface {
  void removeSubSet(const LMID &) {}
};
/* -------------------------------------------------------------------------- */
template <> struct ReferenceManager<UInt> : public ReferenceManagerInterface {
  void removeSubSet(const LMID &) {}
};
/* -------------------------------------------------------------------------- */
class RefElemMeca1D;
template <>
struct ReferenceManager<RefElemMeca1D> : public ReferenceManagerInterface {
  void removeSubSet(const LMID &) {}
};
/* -------------------------------------------------------------------------- */

template <UInt Dim> class RefElemAkantu;
template <UInt Dim>
struct ReferenceManager<RefElemAkantu<Dim>> : public ReferenceManagerInterface {
  void removeSubSet(const LMID &) {}
};
/* -------------------------------------------------------------------------- */

class RefElemParaDiS;
template <>
struct ReferenceManager<RefElemParaDiS> : public ReferenceManagerInterface {
  void removeSubSet(const LMID &) {}
};
/* -------------------------------------------------------------------------- */
template <typename Ref>
void ReferenceManager<Ref>::attachObject(std::shared_ptr<AttachedObject> obj,
                                         ContainerInterface &cont) {

  bool found = false;
  for (auto &&[name, subset] : subsets) {
    if (&subset->getContainer() == &cont) {
      DUMP("attaching object " << &obj << " to subset " << cont.getID(),
           DBG_INFO);
      subset->attachData(obj);
      found = true;
      break;
    }
  }
  if (not found)
    LM_FATAL("attaching object failed : did not find its subset");
}
/* ------------------------------------------------------------------------ */
template <typename Ref>
void ReferenceManager<Ref>::detachObject(std::shared_ptr<AttachedObject> obj,
                                         ContainerInterface &cont) {

  bool found = false;
  for (auto &&[name, subset] : subsets) {
    if (&subset->getContainer() == &cont) {
      DUMP("detaching object " << &obj << " from subset " << cont.getID(),
           DBG_INFO);
      subset->detachData(obj);
      found = true;
      break;
    }
  }
  if (not found)
    LM_FATAL("detaching object failed : did not find its subset");
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_REFERENCE_MANAGER_HH__ */
