#ifndef __LM_PYTHON_COMPONENTS_HH__
#define __LM_PYTHON_COMPONENTS_HH__
/* -------------------------------------------------------------------------- */
#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
/* -------------------------------------------------------------------------- */
#include "action_interface.hh"
#include "component_libmultiscale.hh"
#include "domain_multiscale.hh"
#include "factory_multiscale.hh"
#include "filter_interface.hh"
#include "geometry_manager.hh"
/* -------------------------------------------------------------------------- */

namespace py = pybind11;

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

template <typename indexes> struct ApplyFromVector;
template <int... n> struct ApplyFromVector<std::integer_sequence<int, n...>> {
  template <typename V> static decltype(auto) from_vector(V &&v) {
    auto it = v.begin();
    return std::forward_as_tuple(it[n]...);
  }
  template <typename V, typename F> static void apply(F &&f, V &&vec) {
    std::vector<InputConnection> _input;
    for (auto &&[k, v] : vec) {
      try {
        auto &vec = py::cast<LMObject &>(v);
        _input.push_back(InputConnection(py::cast<std::string>(k)) = vec);
        continue;
      } catch (std::exception &e) {
      }
      try {
        _input.push_back(InputConnection(py::cast<std::string>(k)) =
                             py::cast<OutputContainer>(v));
        continue;
      } catch (std::exception &e) {
      }
      auto vec = py::cast<ContainerArray<Real>::EigenArray>(v);
      ContainerArray<Real> out;
      out = std::forward<ContainerArray<Real>::EigenArray>(vec);

      _input.push_back(InputConnection(py::cast<std::string>(k)) =
                           std::forward<ContainerArray<Real>>(out));
    }
    std::apply(f,
               from_vector(std::forward<std::vector<InputConnection>>(_input)));
  }
};

template <int N, typename V, typename F> void apply_from_vector(F &&f, V &v) {
  ApplyFromVector<std::make_integer_sequence<int, N>>::apply(f, v);
}

template <int N> struct call_compute_impl {
  template <typename F, typename V> static void doit(F &&f, V &&v) {
    int sz = v.size();

    if (sz == N)
      apply_from_vector<N>(f, std::forward<V>(v));
    else
      call_compute_impl<N - 1>::doit(std::forward<F>(f), std::forward<V>(v));
  }
};

template <> struct call_compute_impl<0> {
  template <typename F, typename V> static void doit(F &&f, V &&) { f(); }
};

template <typename F, typename V> void call_compute(F &&f, V &&v) {
  call_compute_impl<6>::doit(f, v);
}
/* -------------------------------------------------------------------------- */

template <typename Comp> struct LMProxyParameters {

  LMProxyParameters(Comp &comp) : _component(comp) {}

  template <typename T> void createMapKeys(T &&keys) {
    for (auto &&k : keys) {
      std::string res = k;
      std::transform(res.begin(), res.end(), res.begin(), ::tolower);
      this->_keywords[res] = k;
    }
  }

  std::string check_key(std::string name) {
    std::string key = name;
    std::transform(key.begin(), key.end(), key.begin(), ::tolower);
    if (not this->_keywords.count(key)) {
      std::stringstream sstr;
      for (auto &&[k, v] : this->_keywords)
        sstr << k << ", ";
      LM_FATAL(this->_component.getID()
               << ": Unknown attribute " << key << ", should be one of ["
               << sstr.str() << "]");
    }
    return key;
  }

  template <typename T> std::shared_ptr<T> tryCast(py::object val) {
    try {
      return py::cast<std::shared_ptr<T>>(val);
    } catch (py::cast_error &e) {
      return nullptr;
    }
    return nullptr;
  }

  auto dir() {
    std::vector<std::string> dir;
    for (auto &&[k, v] : this->_keywords)
      dir.push_back(k);
    return dir;
  }

  Comp &_component;
  std::map<std::string, std::string> _keywords;
};

struct LMParameters : public LMProxyParameters<Parsable> {

  LMParameters(Parsable &component) : LMProxyParameters(component) {
    createMapKeys(component.getPossibleKeywords());
  }

  py::object getattribute(std::string &name) {
    std::string key = this->check_key(name);
    return this->_component.getParam<py::object>(_keywords[key]);
  }

  template <typename T>
  void setattrManaged(std::string name, std::shared_ptr<T> ptr) {
    try {
      ptr->getManager().addObject(ptr);
    } catch (LibMultiScaleException &e) {
    }
    ptr->init();
    this->_component.setParam(this->_keywords[name], ptr->getID());
  }

  void setattr(std::string name, py::object val) {
    name = this->check_key(name);

    if (auto ptr = this->tryCast<ActionInterface>(val)) {
      this->setattrManaged(name, ptr);
      return;
    }
    if (auto ptr = this->tryCast<Geometry>(val)) {
      this->setattrManaged(name, ptr);
      return;
    }

    this->_component.setParam(this->_keywords[name], val);
  }
};

struct LMInputs : public LMProxyParameters<Component> {
  LMInputs(Component &component) : LMProxyParameters(component) {
    createMapKeys(component.getPossibleInputs());
  }

  template <typename T> void setattr(std::string name, T &val) {
    name = this->check_key(name);
    this->_component.connect(this->_keywords[name], val);
  }
};
/* --------------------------------------------------------------------------
 */

inline void declare_components(py::module &m) {

  py::class_<LMParameters>(m, "LMParameters")
      .def("getattribute", &LMParameters::getattribute)
      .def("setattr", &LMParameters::setattr)
      .def("dir", &LMParameters::dir);

  py::class_<LMInputs>(m, "LMInputs")
      .def("setattr", &LMInputs::setattr<LMObject>)
      .def("dir", &LMInputs::dir);

  auto DynamicalAttribute = m.attr("prologue").attr("DynamicalAttribute");

  py::class_<LMObject, std::shared_ptr<LMObject>>(m, "LMObject")
      .def("getID", &LMObject::getID);

  py::class_<OutputContainer, std::shared_ptr<OutputContainer>>(
      m, "OutputContainer")
      .def(
          "castContainer",
          [](OutputContainer &self) -> decltype(auto) {
            return self.get<ContainerInterface &>();
          },
          py::return_value_policy::reference);

  py::class_<Component, LMObject, std::shared_ptr<Component>>(
      m, "Component", py::multiple_inheritance())
      .def(
          "evalOutput",
          [&](Component &self, const std::string output_name)
              -> decltype(auto) { return self.evalOutput(output_name).get(); },
          py::arg("name") = "", py::return_value_policy::reference)
      .def(
          "evalArrayOutput",
          [&](Component &self, const std::string output_name)
              -> decltype(auto) { return self.evalArrayOutput(output_name); },
          py::arg("name") = "", py::return_value_policy::reference)
      .def_property_readonly("inputs",
                             [DynamicalAttribute](Component &self) {
                               return DynamicalAttribute(LMInputs(self));
                             })
      .def("compute", [](Component &self) { self.compute(); })
      .def("compute",
           [](Component &self, py::kwargs kwargs) {
             call_compute([&](auto &&...a) { self.compute(a...); }, kwargs);
           })
      .def("compute",
           [](Component &self, OutputContainer arg) {
             self.compute(std::forward<OutputContainer>(arg));
           })
      .def("compute",
           [](Component &self, LMObject &obj) { //
             self.compute(obj);
           })
      .def("changeRelease", &Component::changeRelease);

  py::class_<Parsable, std::shared_ptr<Parsable>>(m, "Parsable")
      .def("getPossibleKeywords", &Parsable::getPossibleKeywords)
      .def("parseTag", &Parsable::parseTag)
      .def("getParam", &Parsable::getParam<py::object>)
      .def("setParam", &Parsable::setParam<py::object>)
      .def_property_readonly("params", [DynamicalAttribute](Parsable &self) {
        return DynamicalAttribute(LMParameters(self));
      });
}
/* --------------------------------------------------------------------------
 */

__END_LIBMULTISCALE__

#endif //__LM_PYTHON_COMPONENTS_HH__
