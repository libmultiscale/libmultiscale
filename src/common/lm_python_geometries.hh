#ifndef __LM_PYTHON_GEOMETRIES_HH__
#define __LM_PYTHON_GEOMETRIES_HH__

/* -------------------------------------------------------------------------- */
#include "geometry_list.hh"
#include "geometry_manager.hh"
#include "lib_geometry.hh"
/* -------------------------------------------------------------------------- */
#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
/* -------------------------------------------------------------------------- */

namespace py = pybind11;

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

template <typename Class>
inline void declare_geometry(py::module &m, const char *class_name) {

  py::class_<Class, Geometry, std::shared_ptr<Class>>(
      m, class_name, py::multiple_inheritance())
      .def(py::init<UInt, LMID>())
      .def("getID", [](Class &self) { return self.getID(); })
      .def("init",
           [](Class &self) {
             self.checkAllKeywordsAreParsed();
             self.init();
           })
      .def("__str__", [](Class &self) {
        std::stringstream sstr;
        sstr << self;
        return sstr.str();
      });
}

#define PYTHON_GEOMETRY(n, data, obj)                                          \
  {                                                                            \
    using _class = BOOST_PP_TUPLE_ELEM(2, 0, obj);                             \
    std::string geometry_name =                                                \
        clean_class_name(BOOST_PP_STRINGIZE(BOOST_PP_TUPLE_ELEM(2, 0, obj)));  \
                                                                               \
    declare_geometry<_class>(m, geometry_name.c_str());                        \
  }

inline void declare_geometries(py::module &m) {

  auto clean_class_name = [](const std::string &arg) {
    std::string res = arg;

    res.erase(std::remove(res.begin(), res.end(), '<'), res.end());
    res.erase(std::remove(res.begin(), res.end(), '>'), res.end());
    return res;
  };

  py::class_<GeometryManager>(m, "GeometryManager")
      .def_static("getManager", &GeometryManager::getManager,
                  py::return_value_policy::reference)
      .def("addGeometry", &GeometryManager::addGeometry)
      .def("addObject", &GeometryManager::addGeometry)
      .def("destroy", &GeometryManager::destroy)
      .def("reset", &GeometryManager::reset);

  py::class_<Geometry, Parsable, std::shared_ptr<Geometry>>(
      m, "Geometry", py::multiple_inheritance());

  BOOST_PP_SEQ_FOR_EACH(PYTHON_GEOMETRY, f, LIST_GEOMETRY);
}
#undef PYTHON_GEOMETRY

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif //__LM_PYTHON_GEOMETRIES_HH__
