/**
 * @file   lm_ostream_types.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 * @author Moseley Philip Arthur <philip.moseley@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  Definition of the printing features of the principal enums of
 * LibMultiScale
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_LM_OSTREAM_TYPES_HH__
#define __LIBMULTISCALE_LM_OSTREAM_TYPES_HH__
/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

inline std::ostream &operator<<(std::ostream &os, FieldType q) {
  switch (q) {
  case _position0:
    os << "position0";
    break;
  case _position:
    os << "position";
    break;
  case _displacement:
    os << "displacement";
    break;
  case _velocity:
    os << "velocity";
    break;
  case _force:
    os << "force";
    break;
  case _acceleration:
    os << "acceleration";
    break;
  case _temperature:
    os << "temperature";
    break;
  case _grouprank:
    os << "grouprank";
    break;
  case _stress:
    os << "stress";
    break;
  case _strain:
    os << "strain";
    break;
  case _epot:
    os << "pe/atom";
    break;
  case _mass:
    os << "mass";
    break;
  case _burgers:
    os << "burgers";
    break;
  case _normal:
    os << "normal";
    break;
  case _boundary:
    os << "boundary";
    break;
  case _internal_force:
    os << "internal_force";
    break;
  case _external_force:
    os << "external_force";
    break;
  case _angular_velocity:
    os << "angular_velocity";
    break;
  case _angular_acceleration:
    os << "angular_acceleration";
    break;
  case _torque:
    os << "torque";
    break;
  case _radius:
    os << "radius";
    break;    
  default:
    LM_FATAL("unknown field type");
  }
  return os;
}

/* -------------------------------------------------------------------------- */

inline std::ostream &operator<<(std::ostream &os, Operator op) {
  switch (op) {
  case OP_NONE:
    os << "NONE";
    break;
  case OP_AVERAGE:
    os << "AVERAGE";
    break;
  case OP_DENSITY:
    os << "DENSITY";
    break;
  case OP_DEVIATION:
    os << "DEVIATION";
    break;
  case OP_MAX:
    os << "MAX";
    break;
  case OP_MIN:
    os << "MIN";
    break;
  case OP_SUM:
    os << "SUM";
    break;
  default:
    LM_FATAL("unknown operator");
  }
  return os;
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_LM_OSTREAM_TYPES_HH__ */
