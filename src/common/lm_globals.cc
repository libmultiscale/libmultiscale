/**
 * @file   lm_globals.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  This file contains all the global variables of LM
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "lm_common.hh"
#include <fstream>
#include <limits>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
// global variables
UInt current_step;
IntegrationSchemeStage current_stage;
Real current_time;
UInt nb_step;
UInt nb_step_next_event;
UInt global_level;
UInt global_mod;
UInt global_proc;
UInt global_proc1;
UInt global_proc2;
UInt start_dump;
UInt end_dump;
std::string my_hostname;
UInt print_crap_to_file;
std::ostream *global_out;
std::ofstream file_out;
UInt wait_on_fatal;
UInt wait_at_startup;
UInt create_seg_fault;
UInt lm_my_proc_id = 0;
UInt lm_world_size = 1;
UInt lm_job_id;
UInt spatial_dimension;
UInt lm_uint_max = std::numeric_limits<UInt>::max();
Real lm_real_max = std::numeric_limits<Real>::max();
/// used to avoid infinite FATAL recursion in stage PRE_FATAL
bool fatal_loop = false;
bool print_trace;

#if defined(LIBMULTISCALE_USE_TIMER)
std::map<std::string, struct timespec> mesT;
std::map<std::string, UInt> nmes;
std::map<std::string, struct timespec> tstart;
std::map<std::string, struct timespec> tstop;
#endif
/* -------------------------------------------------------------------------- */

UInt getGlobalCurrentRelease() {
  UInt k = 0;
  switch (current_stage) {
  case PRE_DUMP:
    k = 0;
    break;
  case PRE_STEP1:
    k = 1;
    break;
  case PRE_STEP2:
    k = 2;
    break;
  case PRE_STEP3:
    k = 3;
    break;
  case PRE_STEP4:
    k = 4;
    break;
  case PRE_STEP5:
    k = 5;
    break;
  case PRE_STEP6:
    k = 6;
    break;
  case PRE_STEP7:
    k = 7;
    break;
  case PRE_FATAL:
    k = 8;
    break;
  default:
    LM_FATAL("error: unknown stage " << current_stage);
  }
  return (8 * current_step + k);
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
