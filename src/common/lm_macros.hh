/**
 * @file   lm_macros.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  This is where global macros are defined
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_LM_MACROS_HH__
#define __LIBMULTISCALE_LM_MACROS_HH__

/* -------------------------------------------------------------------------- */
// MACRO DEFINITIONS
/* -------------------------------------------------------------------------- */
#ifndef LIBMULTISCALE_USE_MPI
#error "LibMultiScale current version cannot be compiled without MPI support"
#endif

#define EXTERN extern
/* -------------------------------------------------------------------------- */
#ifndef UINT_MAX
#define UINT_MAX lm_uint_max
#endif

#define __BEGIN_LIBMULTISCALE__ namespace libmultiscale {

#define __END_LIBMULTISCALE__ }
/* -------------------------------------------------------------------------- */
#define CONTINUUMTYPE 0
#define ATOMTYPE 1
#define DDTYPE 2
/* -------------------------------------------------------------------------- */
// stuff to stringify a macro after expansion
#define stringify_macro(s) stringify_macro_1(s)
#define stringify_macro_1(s) #s

#endif /* __LIBMULTISCALE_LM_MACROS_HH__ */
