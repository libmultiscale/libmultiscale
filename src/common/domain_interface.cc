/**
 * @file   domain_interface.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:45:56 2014
 *
 * @brief  This is the interface to all domain (model/plugins)
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "domain_interface.hh"
#include "cube.hh"
#include "geometry_manager.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

DomainInterface::DomainInterface(CommGroup group) {
  this->setCommGroup(group);
  // restartfile = "";
}
/* -------------------------------------------------------------------------- */
DomainInterface::~DomainInterface() {}

/* -------------------------------------------------------------------------- */

void DomainInterface::printself(std::ostream &os) const {
  os << "Domain " << this->getID();
}

/* -------------------------------------------------------------------------- */
Geometry &DomainInterface::getGeom() {
  if (geom == invalidGeom)
    LM_FATAL("domain geometry has not been specified");

  return *GeometryManager::getManager().getGeometry(this->geom);
}
/* -------------------------------------------------------------------------- */

/* LMDESC DomainInterface
   This section describe the section that is associated
   with every model.
*/

void DomainInterface::declareParams() {}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
