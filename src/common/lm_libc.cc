/**
 * @file   lm_libc.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 08 23:40:22 2014
 *
 * @brief  Wrapper of libc functions
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cstdlib>
#include <mpi.h>
#include "lm_functions.hh"
#include "lm_types.hh"
#include <sys/types.h>
#include <unistd.h>

/* -------------------------------------------------------------------------- */

void ::libmultiscale::lm_exit(int __status) throw() {
  switch (__status) {
  case LM_EXIT_FAILURE:
    MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    exit(EXIT_FAILURE);
    break;

  case LM_EXIT_SUCCESS:
    exit(EXIT_SUCCESS);
    break;
  default:
    exit(__status);
  }
}

/* -------------------------------------------------------------------------- */

char * ::libmultiscale::lm_getenv(const char *vname) { return getenv(vname); }

/* -------------------------------------------------------------------------- */

int ::libmultiscale::lm_getpid() { return getpid(); }

/* -------------------------------------------------------------------------- */

int ::libmultiscale::lm_atoi(const std::string &n) {
  int res;
  std::stringstream sstr(n);
  sstr >> res;
  return res;
}
