#ifndef __LIBMULTISCALE_LM_OBJECT_HH__
#define __LIBMULTISCALE_LM_OBJECT_HH__
/* -------------------------------------------------------------------------- */
#include "auto_arguments.hh"
#include "comm_group.hh"
#include "lm_common.hh"
#include "lm_globals.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
using dispatch = AutoDispatch::dispatch;
template <typename T, typename... Ts>
using enable_if_type = AutoDispatch::enable_if_type<T, Ts...>;
/* -------------------------------------------------------------------------- */

inline UInt hash_vector(std::vector<UInt> const &vec) {
  std::size_t seed = vec.size();
  for (auto &i : vec) {
    seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  }
  return seed;
}
template <typename... Ts> inline UInt hash_releases(Ts... rel) {
  return hash_vector(std::vector<UInt>{rel...});
}

/* -------------------------------------------------------------------------- */

/**
 * Class LMObject
 * type for objects having a context: string ID, release hash and MPI group.
 */

class LMObject : public AutoDispatch::Argument {

public:
  friend class Component;

  LMObject(const LMID &id);

  static int random_release();

  virtual const LMID &getID() const;
  virtual void setID(const LMID &id);

  //! set the communication group
  virtual void setCommGroup(CommGroup group);
  //! return the communication group
  CommGroup getCommGroup() const;

  virtual void acquireContext(const LMObject &obj);

  //! return actual release
  UInt getRelease() const;

protected:
  //! return actual release
  void setRelease(UInt r);
  // //! increment the release
  // virtual void incRelease();
  //! invalidate the object (if component->forces recompute)
  virtual void changeRelease();

private:
  //! general id
  LMID id;
  //! general release number
  UInt release;

protected:
  CommGroup comm_group;
};

__END_LIBMULTISCALE__

#endif // __LIBMULTISCALE_LM_OBJECT_HH__
