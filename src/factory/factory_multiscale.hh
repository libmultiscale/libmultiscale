/**
 * @file   factory_multiscale.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Dec 04 12:14:29 2013
 *
 * @brief  This is the central factory for all the objects created through the
 * parser
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_FACTORY_MULTISCALE_HH__
#define __LIBMULTISCALE_FACTORY_MULTISCALE_HH__

/* -------------------------------------------------------------------------- */
#include "domain_interface.hh"
#include "domain_multiscale.hh"
/* -------------------------------------------------------------------------- */
#include <typeinfo>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

template <typename T> class FactoryMultiScale : public T, public IDManager<T> {

public:
  friend class IDManager<T>;
  typedef std::string ID;

protected:
  FactoryMultiScale();

public:
  ~FactoryMultiScale() { this->destroy(); }

  //! public function creating a component
  Component &createObject();
  //! public function that create an factory from a reference code
  void connectObject(Component &obj, std::stringstream &line);
  //! call init onto all factorys stored
  void init();
  //! action
  void action();
  //! routines which truly creates a new object connected to obj parameter
  template <typename Obj> void create(Obj &obj);
  //! parse a config line
  virtual UInt parseActionLine(std::stringstream &line, ActionType at);

  //! request the singleton
  // static FactoryMultiScale<T> &getManager(UInt dim);
  static FactoryMultiScale<T> &getManager();

  void compute_make_call() { LM_TOIMPLEMENT; }

protected:
  // //! dimension for all the built objects
  // UInt dim;

  std::string obj_id_being_created;
  std::string obj_type_being_created;
  ActionType obj_kind_being_created;
};
/* -------------------------------------------------------------------------- */
template <typename T> FactoryMultiScale<T> &FactoryMultiScale<T>::getManager() {
  return IDManager<T>::template getManager<FactoryMultiScale<T>>();
  // LM_ASSERT(!(IDManager<T>::static_pointer), "manager already constructed");
  // IDManager<T>::static_pointer.reset(new FactoryMultiScale<T>(dim));
  // return dynamic_cast<FactoryMultiScale<T> &>(*IDManager<T>::static_pointer);
}

/* ------------------------------------------------------------------------ */
// template <typename T> FactoryMultiScale<T>
// &FactoryMultiScale<T>::getManager() {
//   if (!IDManager<T>::static_pointer)
//     LM_THROW("Manager " << typeid(T).name() << " was not created");

//   return dynamic_cast<FactoryMultiScale<T> &>(*IDManager<T>::static_pointer);
// }

/* ------------------------------------------------------------------------ */

using ActionManager = FactoryMultiScale<ActionInterface>;
using FilterManager = FactoryMultiScale<FilterInterface>;

/* ------------------------------------------------------------------------ */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_FACTORY_MULTISCALE_HH__ */
