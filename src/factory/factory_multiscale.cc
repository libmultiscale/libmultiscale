/**
 * @file   factory_multiscale.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon May 05 17:54:16 2014
 *
 * @brief  This is the central factory for all the objects created through the
 * parser
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define TIMER
#include "factory_multiscale.hh"
#include "compute_list.hh"
#include "dumper_list.hh"
#include "filter_list.hh"
#include "lib_dumper.hh"
#include "lib_filter.hh"
#include "lib_md.hh"
#include "lib_stimulation.hh"
#include "lm_common.hh"
#include "lm_parser.hh"
#include "stimulation_list.hh"
#include <boost/preprocessor.hpp>
#include <typeinfo>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

#define BOOST_IF_ELSE(r, data, x)                                              \
  else if (obj_type == stringify_macro(BOOST_PP_TUPLE_ELEM(2, 1, x))) {        \
    typedef BOOST_PP_TUPLE_ELEM(2, 0, x) NewObj;                               \
    std::shared_ptr<NewObj> _ptr = std::make_shared<NewObj>(obj_id);           \
    factory.addObject(_ptr);                                                   \
    return *_ptr;                                                              \
  }

/* -------------------------------------------------------------------------- */

#define BOOST_FILL_SSTR(r, sstr, x)                                            \
  sstr << stringify_macro(BOOST_PP_TUPLE_ELEM(2, 1, x)) << std::endl;

/* -------------------------------------------------------------------------- */

template <ActionType action_type, typename FactoryType>
inline Component &create_object(FactoryType &factory, const std::string &obj_id,
                                const std::string &obj_type);

template <>
Component &create_object<at_filter, FactoryMultiScale<ActionInterface>>(
    FactoryMultiScale<ActionInterface> &, const std::string &,
    const std::string &) {
  LM_TOIMPLEMENT;
}

template <>
Component &create_object<at_compute, FactoryMultiScale<ActionInterface>>(
    FactoryMultiScale<ActionInterface> &, const std::string &,
    const std::string &) {
  LM_TOIMPLEMENT;
}

template <>
Component &create_object<at_dumper, FactoryMultiScale<FilterInterface>>(
    FactoryMultiScale<FilterInterface> &, const std::string &,
    const std::string &) {
  LM_TOIMPLEMENT;
}

template <>
Component &create_object<at_stimulator, FactoryMultiScale<FilterInterface>>(
    FactoryMultiScale<FilterInterface> &, const std::string &,
    const std::string &) {
  LM_TOIMPLEMENT;
}

template <>
Component &create_object<at_stimulator, FactoryMultiScale<ActionInterface>>(
    FactoryMultiScale<ActionInterface> &factory, const std::string &obj_id,
    const std::string &obj_type) {

  if (1 == 2) {
  }
#ifndef LIST_STIMULATION
#error "missing include"
#endif

  BOOST_PP_SEQ_FOR_EACH(BOOST_IF_ELSE, ~, LIST_STIMULATION)
  else {
    std::stringstream possibilities;
    BOOST_PP_SEQ_FOR_EACH(BOOST_FILL_SSTR, possibilities, LIST_STIMULATION);

    LM_FATAL("Unknown/incompatible stimulator "
             << obj_type << "\n\npossible stimulations are:\n"
             << possibilities.str());
  }
}

/* -------------------------------------------------------------------------- */

template <>
Component &create_object<at_filter, FactoryMultiScale<FilterInterface>>(
    FactoryMultiScale<FilterInterface> &factory, const std::string &obj_id,
    const std::string &obj_type) {

  if (1 == 2) {
  }

#ifndef LIST_FILTER
#error "missing include"
#endif

  BOOST_PP_SEQ_FOR_EACH(BOOST_IF_ELSE, ~, LIST_FILTER)
  else {
    std::stringstream possibilities;
    BOOST_PP_SEQ_FOR_EACH(BOOST_FILL_SSTR, possibilities, LIST_FILTER);

    LM_FATAL("Unknown/incompatible filter " << obj_id << ":" << obj_type
                                            << "\n\npossible filters are:\n"
                                            << possibilities.str());
  }
}

/* -------------------------------------------------------------------------- */

template <>
Component &create_object<at_compute, FactoryMultiScale<FilterInterface>>(
    FactoryMultiScale<FilterInterface> &factory, const std::string &obj_id,
    const std::string &obj_type) {

  if (1 == 2) {
  }

#ifndef LIST_COMPUTE
#error "missing include"
#endif

  BOOST_PP_SEQ_FOR_EACH(BOOST_IF_ELSE, ~, LIST_COMPUTE)
  else {
    std::stringstream possibilities;
    BOOST_PP_SEQ_FOR_EACH(BOOST_FILL_SSTR, possibilities, LIST_COMPUTE);

    LM_FATAL("Unknown/incompatible compute " << obj_id << ":" << obj_type
                                             << "\n\npossible computes are:\n"
                                             << possibilities.str());
  }
}

/* -------------------------------------------------------------------------- */

template <>
Component &create_object<at_dumper, FactoryMultiScale<ActionInterface>>(
    FactoryMultiScale<ActionInterface> &factory, const std::string &obj_id,
    const std::string &obj_type) {

  if (1 == 2) {
  }

#ifndef LIST_DUMPER
#error "missing include"
#endif

  BOOST_PP_SEQ_FOR_EACH(BOOST_IF_ELSE, ~, LIST_DUMPER)
  else {
    std::stringstream possibilities;
    BOOST_PP_SEQ_FOR_EACH(BOOST_FILL_SSTR, possibilities, LIST_DUMPER);

    LM_FATAL("Unknown/incompatible compute " << obj_type
                                             << "\n\npossible dumpers are:\n"
                                             << possibilities.str());
  }
}

/* -------------------------------------------------------------------------- */

template <typename T> Component &FactoryMultiScale<T>::createObject() {

  if (obj_kind_being_created == at_stimulator)
    return create_object<at_stimulator>(*this, obj_id_being_created,
                                        obj_type_being_created);
  if (obj_kind_being_created == at_filter)
    return create_object<at_filter>(*this, obj_id_being_created,
                                    obj_type_being_created);
  if (obj_kind_being_created == at_compute)
    return create_object<at_compute>(*this, obj_id_being_created,
                                     obj_type_being_created);
  if (obj_kind_being_created == at_dumper)
    return create_object<at_dumper>(*this, obj_id_being_created,
                                    obj_type_being_created);

  DUMP(obj_kind_being_created, DBG_MESSAGE);
  LM_TOIMPLEMENT;
}

/* -------------------------------------------------------------------------- */

template <typename T>
void FactoryMultiScale<T>::connectObject(Component &obj,
                                         std::stringstream &line) {

  std::string mot;
  UInt pos = line.tellg();
  DUMP(line.str() << " tellg: " << pos, DBG_DETAIL);
  std::string buffer = "";
  if (pos < line.str().length())
    buffer = line.str().substr(pos);

  DUMPFILE(Parser::fout, "Parsing Filter " << buffer);

  if (&obj != &(this->getObject(obj_id_being_created)))
    LM_FATAL("internal error: " << &obj << " != "
                                << &(this->getObject(obj_id_being_created)));

  ParseResult parsed = this->getObject(obj_id_being_created).parseLine(buffer);
  if (!parsed)
    LM_FATAL("Filter parameter "
             << line.str()
             << " was not parsed !! check config file for a syntax problem");
  Parser::shiftLine(parsed, line);
  this->getObject(obj_id_being_created).checkAllKeywordsAreParsed();
  this->getObject(obj_id_being_created).init();

  DUMP("filter configuration parsed ... ", DBG_INFO);
}
/* -------------------------------------------------------------------------- */

template <typename T>
FactoryMultiScale<T>::FactoryMultiScale()
    : LMObject(std::string("Manager:") + typeid(T).name()) // , dim(Dim)
{

  LM_ASSERT(!this->static_pointer, "There should not be more than one instance"
                                       << " of this object "
                                       << stringify_macro(T));
}

/* -------------------------------------------------------------------------- */

template <typename T> void FactoryMultiScale<T>::action() {
  for (auto actionID : this->insert_order) {
    DUMP("testing action " << actionID << " at stage " << current_stage,
         DBG_INFO);
    auto ptr = this->objects[actionID];
    if (ptr == NULL)
      LM_FATAL("NULL pointer registered!");
    if (ptr->shouldMakeAction()) {
      DUMP("calling action " << actionID << " at stage " << current_stage,
           DBG_INFO);
      STARTTIMER(actionID);
      try {
        ptr->action();
      } catch (Component::NotInCommGroup &e) {
        DUMP(ptr->getID() << ": This processor cannot not do anything as one "
                             "input is generated "
                             "by another group of processors",
             DBG_WARNING);
      } catch (Component::UnconnectedInput &e) {
        DUMP(ptr->getID() << ": This action cannot not do anything as some "
                             "input is not created/computed/connected",
             DBG_WARNING);
      }

      STOPTIMER(actionID);
    }
  }
  ++this->action_step;
}

/* -------------------------------------------------------------------------- */
template <typename T> void FactoryMultiScale<T>::init() {

  LM_THROW("deprecated method should not be called anymore");

  for (auto &&obj : this->objects) {
    obj.second->init();
  }
}
/* -------------------------------------------------------------------------- */

template <typename T>
UInt FactoryMultiScale<T>::parseActionLine(std::stringstream &line,
                                           ActionType atype) {
  std::string mot;
  obj_type_being_created = "NULL";
  obj_kind_being_created = atype;

  Parser::parse(obj_id_being_created, line);
  Parser::parse(obj_type_being_created, line);

  auto &obj = createObject();
  try {
    connectObject(obj, line);
  } catch (LibMultiScaleException &e) {
    LM_FATAL_RE(e, line.str() << " was not parsed !! " << std::endl
                              << "\t\tcheck config file for a syntax problem"
                              << std::endl);
  }

  return true;
}

template <>
std::unique_ptr<IDManager<ActionInterface>>
    IDManager<ActionInterface>::static_pointer = nullptr;
template <>
std::unique_ptr<IDManager<FilterInterface>>
    IDManager<FilterInterface>::static_pointer = nullptr;
/* -------------------------------------------------------------------------- */
template class FactoryMultiScale<ActionInterface>;
template class FactoryMultiScale<FilterInterface>;
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
