/**
 * @file   domain_multiscale.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jan 15 17:00:43 2014
 *
 * @brief  This is a meta domain whose purpose is to handle other domains
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_DOMAIN_MULTISCALE_HH__
#define __LIBMULTISCALE_DOMAIN_MULTISCALE_HH__
/* -------------------------------------------------------------------------- */
#include "domain_interface.hh"
#include "id_manager.hh"
/* -------------------------------------------------------------------------- */
#include <algorithm>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

// backward declaration
// class FilterManager;
class CouplerManager;
// class ActionManager;
class GeometryManager;

/* -------------------------------------------------------------------------- */

/**
 * Class DomainMultiScale
 * domaine generique multiechelle
 */

class DomainMultiScale : public IDManager<DomainInterface>, Parsable {

public:
  friend class Parser;
  friend class IDManager<DomainInterface>;

protected:
  DomainMultiScale();

public:
  virtual ~DomainMultiScale();

public:
  static inline DomainMultiScale &getManager();

  //! initialization method
  void init(){};
  //! parse line for creation of a model
  UInt parseLineModel(std::stringstream &line);
  //! function that calls all the stored coupling objects
  void coupling(CouplingStage stage);
  //! restart from xml file function
  void readXMLFile(const std::string &){};
  //! This method build a full multiscale domain from a config file
  UInt build(const std::string &config);
  //! return a model from its registering order index
  std::shared_ptr<DomainInterface> getModelByNumber(UInt i);
  //! return the number of registered models
  UInt getNbModels();
  //! return the domain dimensions for this processor
  Cube getSubDomainBoundingBox() { LM_TOIMPLEMENT; };
  //! return the entire domain dimensions
  Cube getDomainBoundingBox() { LM_TOIMPLEMENT; };
  //! return the entire domain dimensions
  void setSubDomainBoundingBox(const Cube &) { LM_TOIMPLEMENT; };
  //! return the entire domain dimensions
  void setDomainBoundingBox(const Cube &) { LM_TOIMPLEMENT; };
  //! return the dimension of the domains
  UInt getDim() { return Dim; };

  void declareParams() override{};

  template <typename Func> void for_each(Func &&func) {
    std::for_each(this->objects.begin(), this->objects.end(), [&](auto &&pair) {
      auto &&d = *pair.second;
      if (d.getCommGroup().amIinGroup())
        func(d);
    });
  }

private:
  //! generic parser method that is called by the parser object on each red line
  ParseResult setParam(const std::string &key,
                       std::stringstream &line) override;

  //! dimension of the multiscale material (should be unified)
  UInt Dim;
};

/* -------------------------------------------------------------------------- */

std::pair<std::string, std::string>
split_component_output(const std::string name);
Component &getRegisteredComponent(const std::string &component_name);
OutputContainer getRegisteredOutput(const std::string &output_name);

template <typename T> T &getRegisteredOutput(const std::string &output_name) {
  try {
    auto &res = getRegisteredOutput(output_name).get<T>();
    return res;
  } catch (std::bad_cast &e) {
    auto &out = getRegisteredOutput(output_name).get();
    LM_FATAL("output " << output_name << "(" << AutoDispatch::obj_info(out)
                       << ") is not of the right type: "
                       << AutoDispatch::demangle(typeid(T).name()));
  }
}
/* -------------------------------------------------------------------------- */

inline DomainMultiScale &DomainMultiScale::getManager() {
  return IDManager<DomainInterface>::getManager<DomainMultiScale>();
}

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_DOMAIN_MULTISCALE_HH__ */
