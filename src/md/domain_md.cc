/**
 * @file   domain_md.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Thu Nov 14 00:49:01 2013
 *
 * @brief  Mother to all molecular domains
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

/* -------------------------------------------------------------------------- */
#include "domain_md.hh"
#include "cube.hh"
#include "geometry_manager.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
#include "trace_atom.hh"
#include "xml_parser_restart.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

template <typename ContAtoms, UInt Dim>
inline void
DomainAtomic<ContAtoms, Dim>::readXMLFile(const std::string &filename) {

  XMLRestartParser<Dim> parser;

  //  UInt flag_memorysave_onrestart = material.mem_save_on_off;
  auto group = this->getCommGroup();
  int group_size = group.size();
  int myrankingroup = group.getMyRank();
  for (int i = 0; i < group_size; ++i) {
    if (myrankingroup != i)
      continue;

    DUMP("Setting DOFs simulation from libMultiScale XML Data format "
             << filename,
         DBG_INFO);

    if (parser.parseFile(filename)) {
      LM_FATAL("restart was not performed because file was not found"
               << filename);
    }

    // this->setCurrentStep(current_step);

    DUMP("read done " << filename, DBG_INFO);

    UnitsConverter unit;
    unit.setInUnits(si_unit_system);

    UInt cpt = 0;
    UInt cpt2 = 0;
#ifndef LM_OPTIMIZED
    UInt total = atoms.size();
#endif // LM_OPTIMIZED

    for (auto &&at : atoms) {

      VectorView<Dim> X = at.position0();

      if (cpt2 % 100000 == 0)
        DUMP("passing atom number " << cpt2 << "/" << total << " reloaded",
             DBG_INFO_STARTUP);
      ++cpt2;

      IF_TRACED(X, "i see it");

      if (!parser.isDofRegistered(X)) {
        DUMP("this atom " << X << ", was not reloaded: was not in restart file",
             DBG_DETAIL);
        IF_TRACED(X, "traced atom was not reloaded: was not in restart file");
        continue;
      }

      Vector<Dim> U;
      parser.template assignField<_displacement>(X, U);
      DUMP("reloading atom at position " << X << " where disp = " << U,
           DBG_DETAIL);
      at.displacement() = U;

      Vector<Dim> V;
      parser.template assignField<_velocity>(X, V);
      at.velocity() = V;
      IF_TRACED(X, "reloading atom force " << at.force());
      if (cpt % 100 == 0)
        DUMP("atom number " << cpt << "/" << total << " reloaded", DBG_INFO);
      ++cpt;

      // Vector<Dim> V;
      // parser.assignVelField(X, V);
      // at.velocity() = V;
      // Vector<Dim> W;
      // parser.assignVelField(X, W);
      // at.angular_velocity() = W;
      // IF_TRACED(X, "reloading atom force " << at.force());
      // if (cpt % 100 == 0)
      //   DUMP("atom number " << cpt << "/" << total << " reloaded", DBG_INFO);
      // ++cpt;
    }

    parser.cleanMemory();

    DUMP("associated " << cpt << " over " << atoms.size(), DBG_MESSAGE);
  }
  DUMP("perform migration to have coherent stress and force values",
       DBG_INFO_STARTUP);

  // performMigration(); // perform migration in upper step
  // (domain_lammps.cc:initDOFs())
  // performStep2();
}

/* -------------------------------------------------------------------------- */

template <typename ContAtoms, UInt Dim>
typename DomainAtomic<ContAtoms, Dim>::ContainerAtoms &
DomainAtomic<ContAtoms, Dim>::getContainer() {

  atoms.acquireContext(*this);
  return atoms;
}

/* -------------------------------------------------------------------------- */

/* LMDESC DomainAtomic
   This section describe the section that is associated
   with molecular dynamics models. It is the starting
   point of every MD plugin.
*/

/* LMHERITANCE domain_interface */

template <typename ContAtoms, UInt Dim>
void DomainAtomic<ContAtoms, Dim>::declareParams() {

  DomainInterface::declareParams();

  /* LMKEYWORD DOMAIN_GEOMETRY
     This describe the geometry where all
     degrees of freedom are located. LAMMPS and MD1D
     plugins enforce this constraint by refusing the
     creation of atoms outside of this geometry. Thus
     carving, selection, etc, are possible by means of this
     keyword.
  */
  this->parseKeyword("DOMAIN_GEOMETRY", geom);

  /* LMKEYWORD TIMESTEP
     Sets the timestep
  */
  this->parseKeyword("TIMESTEP", this->timeStep);
}
/* -------------------------------------------------------------------------- */

#ifdef LIBMULTISCALE_STAMP
template class DomainAtomic<ContainerStamp, 3>;
#endif
#ifdef LIBMULTISCALE_LAMMPS_PLUGIN
template class DomainAtomic<ContainerLammps<2>, 2>;
template class DomainAtomic<ContainerLammps<3>, 3>;
#endif
#ifdef LIBMULTISCALE_MD1D
template class DomainAtomic<ContainerMD1D, 1>;
#endif

__END_LIBMULTISCALE__
