/**
 * @file   trace_atom.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  Set of macros to trace an atom through processor migrations
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_TRACE_ATOM_H__
#define __LIBMULTISCALE_TRACE_ATOM_H__
/* -------------------------------------------------------------------------- */
//#define LIBMULTISCALE_TRACE_ATOM
#ifdef LIBMULTISCALE_TRACE_ATOM
/* -------------------------------------------------------------------------- */
#include "attached_object.hh"
#include "lm_common.hh"

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

extern Real trace_ref[3];

extern Real trace_tolerance;
extern UInt internal_index;

/* -------------------------------------------------------------------------- */
template <typename V> bool CHECK_TRACED(V &&X) {
  // return true;
  return (trace_ref[0] - trace_tolerance <= X[0] &&
          X[0] <= trace_ref[0] + trace_tolerance &&
          trace_ref[1] - trace_tolerance <= X[1] &&
          X[1] <= trace_ref[1] + trace_tolerance &&
          trace_ref[2] - trace_tolerance <= X[2] &&
          X[2] <= trace_ref[2] + trace_tolerance);
}
/* -------------------------------------------------------------------------- */
template <typename Ref> bool CHECK_REF_TRACED(Ref &&ref) {
  return CHECK_TRACED(ref.position0());
}
/* -------------------------------------------------------------------------- */
template <typename T> class ContainerArray;
template <UInt dim> class RefLammps;

#define checkAttachedValue(mess, ref, subsets)

// #define checkAttachedValue(mess, ref, subsets)
//   {
//     auto &s = subsets["half-subset"];
//     auto &container =
//         s->template getContainer<ContainerArray<RefLammps<3>>>();
//     int index = s->getRefIndex(ref);
//     if (index != -1) {
//       auto &att_obj = dynamic_cast<AttachedVector<double> &>(
//           *s->attached_objects["v3_p0"]);
//
//       bool is_hole = s->is_hole(index);
//
//       DUMP(container.getID() << "[" << index << "] = " << ref
//                              << " attached:" << att_obj.getID() << ": "
//                              << att_obj.getV().row(index)
//                              << " hole=" << is_hole << " ### " << mess,
//            DBG_MESSAGE);
//       if ((not is_hole)) {
//         if constexpr (decltype(ref)::Dim == 3) {
//           Vector<3> pos = ref.position0() * 2;
//           Vector<2> data = att_obj.getV().row(index);
//           if ((data - pos.block<2, 1>(0, 0)).norm() > 1e-5)
//             LM_FATAL("aaaaa");
//         }
//       }
//     }
//   }

__END_LIBMULTISCALE__

#define ATOM_OUT(X) "(" << X[0] << "," << X[1] << "," << X[2] << ")"
/* --------------------------------------------------------------------------
 */

#define IF_COORD_EQUALS(x1, x2)                                                \
  if (x1[0] - trace_tolerance <= x2[0] && x2[0] <= x1[0] + trace_tolerance &&  \
      x1[1] - trace_tolerance <= x2[1] && x2[1] <= x1[1] + trace_tolerance &&  \
      x1[2] - trace_tolerance <= x2[2] && x2[2] <= x1[2] + trace_tolerance)
/* -------------------------------------------------------------------------- */
#define IF_TRACED(X, message)                                                  \
  if (CHECK_TRACED(X)) {                                                       \
    DUMP(ATOM_OUT(X) << message, DBG_MESSAGE);                                 \
  }
/* -------------------------------------------------------------------------- */
#define IF_REF_TRACED(ref, message)                                            \
  if (CHECK_REF_TRACED(ref)) {                                                 \
    DUMP(ref << " " << message, DBG_MESSAGE);                                  \
  }
/* -------------------------------------------------------------------------- */
#define VIEW_ATOM(T)                                                           \
  {                                                                            \
    if (internal_index != UINT_MAX) {                                          \
      T ref(internal_index);                                                   \
      DUMP("how is my atom now ? " << ref, DBG_MESSAGE);                       \
    } else                                                                     \
      DUMP("my atom is not here ", DBG_MESSAGE);                               \
  };
/* -------------------------------------------------------------------------- */
#define SET_INTERNAL_TRACE_INDEX(X, index)                                     \
  if (CHECK_TRACED(X))                                                         \
    internal_index = index;
/* -------------------------------------------------------------------------- */
#define SET_REF_INTERNAL_TRACE_INDEX(ref, index)                               \
  {                                                                            \
    if (CHECK_REF_TRACED(ref))                                                 \
      internal_index = index;                                                  \
  }
/* ----------------------------------------------------------------------- */
#define TRACE_BREAKPOINT(ref)                                                  \
  if (CHECK_REF_TRACED(ref)) {                                                 \
    int a = 1;                                                                 \
    while (a) {                                                                \
    }                                                                          \
  }
/* ----------------------------------------------------------------------- */
#else
#define IF_TRACED(X, message)
#define IF_REF_TRACED(ref, message)
#define VIEW_ATOM(T)
#define CHECK_TRACED(X) (1 == 2)
#define CHECK_REF_TRACED(X) (1 == 2)
#define IF_COORD_EQUALS(x1, x2)
#define SET_INTERNAL_TRACE_INDEX(X, index)
#define SET_REF_INTERNAL_TRACE_INDEX(ref, index)
#define TRACE_BREAKPOINT(ref)
#define checkAttachedValue(mess, ref, subsets)
#endif
/* --------------------------------------------------------------------------
 */

#endif /* __LIBMULTISCALE_TRACE_ATOM_H__ */
