/**
 * @file   domain_md1d.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Fri Jan 10 20:47:45 2014
 *
 * @brief  This is the domain for 1d atomic chains
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_DOMAIN_MD1D_HH__
#define __LIBMULTISCALE_DOMAIN_MD1D_HH__
/* -------------------------------------------------------------------------- */
#include "container_md1d.hh"
#include "domain_md.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/**
 * Class DomainAtomic
 * domaine atomique
 */

class DomainMD1D : public DomainAtomic<ContainerMD1D, 1> {

public:
  using RefDof = RefMD1D;
  using ContainerDofs = ContainerMD1D;
  using IteratorDofs = ContainerMD1D::iterator;

  DomainMD1D(const LMID &ID, CommGroup group);
  ~DomainMD1D();

  ArrayView getField(FieldType field_type) override;
  void updateGradient() override;
  void updateAcceleration() override;
  ArrayView acceleration() override;
  ArrayView gradient() override;
  ArrayView primal() override;
  ArrayView primalTimeDerivative() override;
  ArrayView mass() override;

  void init() override;
  void declareParams() override;
  Cube getDomainBoundingBox() const override;
  Cube getSubDomainBoundingBox() const override { LM_TOIMPLEMENT; };
  void setDomainBoundingBox(const Cube &) override { LM_TOIMPLEMENT; };
  void setSubDomainBoundingBox(const Cube &) override { LM_TOIMPLEMENT; };
  Real potentialEnergy() override { LM_TOIMPLEMENT; };
  void enforceCompatibility() override;

  bool isPeriodic(UInt dir) override;

protected:
  //! get the elastic stiffness constant
  Real getElasticConstant();
  //! get R0
  Real getR0();
  int getNbDofs();
  void p1();
  void p3();
  //! compute linear force (quadratic potential)
  void linearForceComputation();
  inline Real computeDistance(const Real &xi, const Real &xj);
  void correctPBCPositions();

  UInt getDim();
  void getNeighborList(RefAtom &atom, UInt count,
                       std::vector<RefAtom> &neigbhor_list);

  void setPeriodicFlag(bool flag);
  void setReferenceManagerState(bool state);

  Real getEcin() { LM_TOIMPLEMENT; };

  Real getDomainSize();
  void computeNeighborLists();
  Real ljForce(Real rij);
  Real ljBondEnergy(Real rij);

private:
  //! the neighbor list
  std::vector<std::vector<UInt>> neighbor_lists;
  //! array for constraints
  // std::vector<Real> stress;
  //! interatomic distance
  Quantity<Length> r0;
  //! cutoff radius
  Quantity<Length> rcut;
  //! epsilon parameter for lennard jones
  Quantity<Energy> epsilon;
  //! sigma parameter for lennard jones
  Quantity<Length> sigma;
  //! equivalent stiffness
  Real stiffness_parameter;
  //! flag to activate periodic boundary conditions
  bool periodic_flag;

  /* ------------------------------------------------------------------------ */
  // old material members
  /* --------------------------------------------------------------------------
   */

  //! per atom mass
  Quantity<Mass> atomic_mass;
  //! flag to activate elasticity (linearize LJ)
  bool elastic_flag;
  //! if a shift is requested for the entire set of atoms
  Quantity<Length> shift;

  //! the file to read the atomic points from
  std::string atom_file;
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

#endif /* __LIBMULTISCALE_DOMAIN_MD1D_HH__ */
