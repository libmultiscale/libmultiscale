/**
 * @file   iterator_md1d.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Jan 07 16:36:30 2013
 *
 * @brief  This is the generic iterator over atoms of the md1d domain
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_ITERATOR_MD1D_HH__
#define __LIBMULTISCALE_ITERATOR_MD1D_HH__
/* -------------------------------------------------------------------------- */
#include "container_md1d.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/**
 * Class IteratorMD1D
 *
 */

class ContainerMD1D::iterator : public iterator_base<RefMD1D> {

public:
  iterator(ContainerMD1D &c);

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  RefMD1D &operator*();
  iterator_base<RefMD1D> &operator++();

  virtual bool operator!=(const iterator_base &) const;
  virtual bool operator==(const iterator_base &) const;

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

  friend ContainerMD1D;

private:
  UInt index_atom;
  RefMD1D atom;
};

/* -------------------------------------------------------------------------- */

inline ContainerMD1D::iterator::iterator(ContainerMD1D &c)
    : iterator_base(c), atom(c) {}

/* -------------------------------------------------------------------------- */

inline RefMD1D &ContainerMD1D::iterator::operator*() { return atom; }

/* -------------------------------------------------------------------------- */
inline iterator_base<RefMD1D> &ContainerMD1D::iterator::operator++() {
  ++index_atom;
  atom.index_atom = index_atom;
  return *this;
}
/* -------------------------------------------------------------------------- */
inline bool ContainerMD1D::iterator::operator!=(const iterator_base &it) const {
  return index_atom !=
         static_cast<const ContainerMD1D::iterator &>(it).index_atom;
}
/* -------------------------------------------------------------------------- */
inline bool ContainerMD1D::iterator::operator==(const iterator_base &it) const {
  return index_atom ==
         static_cast<const ContainerMD1D::iterator &>(it).index_atom;
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

namespace std {
template <typename T> struct iterator_traits<libmultiscale::iterator_base<T>> {
private:
  using iterator_type = libmultiscale::iterator_base<T>;

public:
  using iterator_category = typename iterator_type::iterator_category;
  using value_type = typename iterator_type::value_type;
  using difference_type = typename iterator_type::difference_type;
  using pointer = typename iterator_type::pointer;
  using reference = typename iterator_type::reference;
};

template <>
struct iterator_traits<libmultiscale::ContainerMD1D::iterator>
    : public iterator_traits<
          libmultiscale::iterator_base<libmultiscale::RefMD1D>> {};

} // namespace std

#endif /* __LIBMULTISCALE_ITERATOR_MD1D_HH__ */
