/**
 * @file   md_builder.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Fri Jan 10 23:04:26 2014
 *
 * @brief  This class constructs the 1D atomic chains
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

/* -------------------------------------------------------------------------- */
#include "md_builder.hh"
#include "lm_common.hh"
#include "lm_parser.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

void ZoneMDBuilder::buildMD1D(const Cube &b, const Real &r0, const Real &mass,
                              const Real &shift, bool periodic_flag,
                              ContainerMD1D &atoms) {

  Real range = b.getXmax()[0] - b.getXmin()[0];
  int nb_atoms = (int)(range / r0);

  Real center = b.getCenter(0) + shift;
  //  this->buildMD1D(nb_atoms,b.getCenter(0)+shift,r0,mass,periodic_flag,atoms);

  int center_index = 0;

  if (periodic_flag) {
    --nb_atoms;
    center_index = nb_atoms / 2;
  } else
    center_index = (nb_atoms - 1) / 2;

  DUMP("Requesting build of a md zone of size "
           << (nb_atoms - 1) * r0 << " center index = " << center_index,
       DBG_INFO_STARTUP);

  atoms.resize(nb_atoms);

  // higher than center part
  for (int i = center_index; i < nb_atoms; ++i) {

    atoms.p[i] = center + (i - center_index) * r0;
    atoms.p0[i] = center + (i - center_index) * r0;
    DUMP("creation of atom " << i << " at position " << atoms.p0[i],
         DBG_DETAIL);
    atoms.v[i] = 0.0f;
    LM_ASSERT(mass != 0,
              "used mass cannot be 0 -> have you used MASS keyword ?");
    atoms.m[i] = mass;
    atoms.a[i] = 0.0f;
    atoms.f[i] = 0.0f;
    // atoms[i].alpha = 1.0;
    atoms.pe[i] = 0.0;
  }
  // lower than center part
  for (int i = center_index - 1; i > -1; --i) {

    atoms.p[i] = center + (i - center_index) * r0;
    atoms.p0[i] = center + (i - center_index) * r0;
    DUMP("creation of atom " << i << " at position " << atoms.p0[i],
         DBG_DETAIL);
    atoms.v[i] = 0.0f;
    LM_ASSERT(mass != 0,
              "used mass cannot be 0 -> have you used MASS keyword ?");
    atoms.m[i] = mass;
    atoms.a[i] = 0.0f;
    atoms.f[i] = 0.0f;
    // atoms[i].alpha = 1.0;
    atoms.pe[i] = 0.0;
  }

  DUMP("Number of generated atoms " << atoms.size(), DBG_MESSAGE);
}
/* -------------------------------------------------------------------------- */

void ZoneMDBuilder::readMD1D(const std::string &filename, const Cube &,
                             const Real &, const Real &mass,
                             ContainerMD1D &atoms) {

  std::ifstream f;
  f.open(filename.c_str());

  if (!f.is_open()) {
    LM_FATAL("could not open config file " << filename);
  }

  UInt line_count = 0;
  while (f.good()) {
    std::string buffer;
    std::getline(f, buffer);
    ++line_count;

    unsigned long found = buffer.find_first_of("#");
    std::string buffer_comment_free = buffer.substr(0, found - 1);
    buffer = buffer_comment_free;

    // if it is a comment, then do nothing
    if (buffer[0] == '#' || buffer == "") {
      continue;
    }

    std::stringstream line(buffer);

    Real x;
    try {
      Parser::parse(x, line);
    } catch (LibMultiScaleException &e) {
      LM_THROW(filename << ":" << line_count << ":"
                        << " unable to read coordinate: " << buffer);
    }

    atoms.add(x, x, 0., 0., 0., mass);
  }

  DUMP("Number of generated atoms " << atoms.size(), DBG_MESSAGE);
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
