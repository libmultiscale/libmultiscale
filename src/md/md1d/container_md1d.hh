/**
 * @file   container_md1d.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Thu Jul 24 14:21:58 2014
 *
 * @brief  Generic container of references for the MD1D model
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_CONTAINER_MD1D_HH__
#define __LIBMULTISCALE_CONTAINER_MD1D_HH__
/* -------------------------------------------------------------------------- */
#include "container_array.hh"
#include "ref_atom_md1d.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
class ReferenceManagerMD1D;
/* -------------------------------------------------------------------------- */

/**
 * Class ContainerMD1D
 *
 */
class ContainerMD1D : public Container_base<RefMD1D> {

public:
  friend class DomainMD1D;

  class iterator;
  static constexpr UInt Dim = 1;

  ContainerMD1D(const LMID &id);
  ~ContainerMD1D(){};

  iterator begin();
  iterator end();
  RefMD1D &get(UInt index) override;
  UInt size() const override;
  void resize(UInt size);
  void add(Real _p, Real _p0, Real _v, Real _f, Real _a, Real _m);

  ContainerArray<Real> p;
  ContainerArray<Real> p0;
  ContainerArray<Real> v;
  ContainerArray<Real> f;
  ContainerArray<Real> a;
  ContainerArray<Real> m;
  ContainerArray<Real> pe;
  ContainerArray<Real> acceleration;

  RefMD1D ref;
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
#include "iterator_md1d.hh"
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

inline ContainerMD1D::ContainerMD1D(const LMID &id)
    : LMObject(id), Container_base<RefMD1D>(), ref(*this, 0) {}

/* -------------------------------------------------------------------------- */

inline UInt ContainerMD1D::size() const { return p.size(); }

/* -------------------------------------------------------------------------- */

inline void ContainerMD1D::resize(UInt sz) {
  p.resize(sz);
  p0.resize(sz);
  v.resize(sz);
  f.resize(sz);
  a.resize(sz);
  m.resize(sz);
  pe.resize(sz);
  acceleration.resize(sz);
}

/* -------------------------------------------------------------------------- */

inline ContainerMD1D::iterator ContainerMD1D::begin() {
  ContainerMD1D::iterator it(*this);
  it.index_atom = 0;
  it.atom.index_atom = it.index_atom;
  return it;
}
/* -------------------------------------------------------------------------- */

inline ContainerMD1D::iterator ContainerMD1D::end() {
  ContainerMD1D::iterator it(*this);
  it.index_atom = this->size();
  it.atom.index_atom = it.index_atom;
  return it;
}

/* -------------------------------------------------------------------------- */

inline RefMD1D &ContainerMD1D::get(UInt index) {
  LM_ASSERT(p.size(), "dof vector was not initialized");
  ref.setIndex(index);
  return ref;
}

/* -------------------------------------------------------------------------- */
inline void ContainerMD1D::add(Real _p, Real _p0, Real _v, Real _f, Real _a,
                               Real _m) {
  p.push_back(_p);
  p0.push_back(_p0);
  v.push_back(_v);
  f.push_back(_f);
  a.push_back(_a);
  m.push_back(_m);
  pe.push_back(0.);
  acceleration.push_back(0.);
}

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_CONTAINER_MD1D_HH__ */
