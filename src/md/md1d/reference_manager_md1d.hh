/**
 * @file   reference_manager_md1d.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Mar 13 16:53:59 2013
 *
 * @brief  This is the reference manager of the md1d model which handles
 * migrations
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_REFERENCE_MANAGER_MD1D_HH__
#define __LIBMULTISCALE_REFERENCE_MANAGER_MD1D_HH__
/* -------------------------------------------------------------------------- */
#include "ref_atom_md1d.hh"
#include "reference_manager.hh"
#include "container_md1d.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class ReferenceManagerMD1D : public ReferenceManager<RefMD1D> {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  ReferenceManagerMD1D(ContainerMD1D &global_cont)
      : ReferenceManager<RefMD1D>(global_cont){};

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

public:
  UInt haveChangedRef(RefMD1D &ref);
  bool haveChanged();
  RefMD1D &NewRef(RefMD1D &ref);
};

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_REFERENCE_MANAGER_MD1D_HH__ */
