/**
 * @file   domain_md1d.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Fri Jan 10 20:47:45 2014
 *
 * @brief  This is the domain for 1d atomic chains
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

/* -------------------------------------------------------------------------- */
#include "domain_md1d.hh"
#include "lm_common.hh"
#include "md_builder.hh"
#include "reference_manager.hh"
#include "reference_manager_md1d.hh"
#include "spatial_filter_atomic.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

void DomainMD1D::correctPBCPositions() {

  int nb_atoms = this->getNbDofs();
  Real domainSize = this->getDomainSize();

  for (int i = 0; i < nb_atoms; ++i) {
    atoms.p[i] -= domainSize * nearbyint(atoms.p[i] / domainSize);
  }
}
/* -------------------------------------------------------------------------- */

inline Real DomainMD1D::computeDistance(const Real &xi, const Real &xj) {
  Real rij = xj - xi;
  Real domainSize = this->getDomainSize();

  if (this->periodic_flag)
    rij -= domainSize * nearbyint(rij / domainSize);
  return rij;
}
/* -------------------------------------------------------------------------- */

Real DomainMD1D::getDomainSize() {
  if (this->periodic_flag)
    return this->getNbDofs() * r0;
  return (this->getNbDofs() - 1) * r0;
}

/* -------------------------------------------------------------------------- */

void DomainMD1D::init() {
  DomainAtomic::init();

  if (!this->getCommGroup().amIinGroup())
    return;

  ZoneMDBuilder z;

  Cube b = this->getGeom().getBoundingBox();

  if (this->atom_file == "")
    z.buildMD1D(b, this->r0, this->atomic_mass, this->shift,
                this->periodic_flag, this->atoms);

  else
    z.readMD1D(this->atom_file, b, this->r0, this->atomic_mass, this->atoms);

  DUMP("Size of domain " << this->getDomainSize(), DBG_MESSAGE);
  DUMP("position offset " << -1.0 * this->getDomainSize() / 2 << " Center "
                          << b.getCenter(0) << " Shift " << shift,
       DBG_MESSAGE);

  DUMP(" MD1D  R0 " << r0 << " RCUT " << rcut << " Rcut/r0 "
                    << (Real)(rcut / r0),
       DBG_MESSAGE);

  // int nb_atoms = this->getNbDofs();
  // stress.resize(nb_atoms);
  stiffness_parameter = 36. * epsilon * pow(2., 2. / 3.) / sigma / sigma;
  DUMP("Stiffness parameter " << stiffness_parameter << " epsilon " << epsilon
                              << " sigma " << sigma,
       DBG_MESSAGE);

  this->computeNeighborLists();
}
/* -------------------------------------------------------------------------- */
DomainMD1D::DomainMD1D(const LMID &ID, CommGroup group)
    : LMObject(ID), DomainAtomic<ContainerMD1D, 1>(ID, group),
      periodic_flag(false), elastic_flag(false), shift(0.0) {}
/* -------------------------------------------------------------------------- */
DomainMD1D::~DomainMD1D() {}
/* -------------------------------------------------------------------------- */

ArrayView DomainMD1D::gradient() { return getField(_force); }
ArrayView DomainMD1D::primal() { return getField(_position); }
ArrayView DomainMD1D::primalTimeDerivative() { return getField(_velocity); }
ArrayView DomainMD1D::mass() { return getField(_mass); }
ArrayView DomainMD1D::acceleration() { return getField(_acceleration); }
/* -------------------------------------------------------------------------- */

auto DomainMD1D::getField(FieldType field_type) -> ArrayView {
  switch (field_type) {
  case _position0:
    return ArrayView(atoms.p0.data(), int(atoms.p0.size()), 1);
  case _velocity:
    return ArrayView(atoms.v.data(), int(atoms.v.size()), 1);
  case _position:
    return ArrayView(atoms.p.data(), int(atoms.p.size()), 1);
  case _force:
    return ArrayView(atoms.f.data(), int(atoms.f.size()), 1);
  case _mass:
    return ArrayView(atoms.m.data(), int(atoms.m.size()), 1);
  case _acceleration:
    return ArrayView(atoms.acceleration.data(), int(atoms.acceleration.size()),
                     1);
  default:
    LM_FATAL("Not accessible field: " << field_type);
  }
}

/* -------------------------------------------------------------------------- */

void DomainMD1D::enforceCompatibility() {

  if (periodic_flag)
    this->correctPBCPositions();

  this->changeRelease();
}

/* -------------------------------------------------------------------------- */

inline void DomainMD1D::computeNeighborLists() {

  UInt nb_atoms = atoms.size();
  neighbor_lists.resize(nb_atoms);
  for (UInt i = 0; i < nb_atoms; ++i) {
    std::vector<UInt> neighbors;

    for (UInt j = i + 1; j < nb_atoms; ++j) {
      Real rij = std::fabs(this->computeDistance(atoms.p[j], atoms.p[i]));
      LM_ASSERT(rij / sigma > 1e-6,
                "problem in neighbor list: " << i << " " << j);

      if (rij <= rcut * 1.5)
        neighbors.push_back(j);
    }
    neighbor_lists[i] = neighbors;
  }
}

/* -------------------------------------------------------------------------- */

inline Real DomainMD1D::ljForce(Real rij) {

  LM_ASSERT(std::fabs(rij) / sigma > 1e-6, "problem in neighbor list");
  Real contribution = -24.0 * epsilon *
                      (2.0 * MathTools::ipow(sigma / rij, 12) -
                       1.0 * MathTools::ipow(sigma / rij, 6)) /
                      rij;

  return contribution;
}
/* -------------------------------------------------------------------------- */

inline Real DomainMD1D::ljBondEnergy(Real rij) {

  LM_ASSERT(std::fabs(rij) / sigma > 1e-6, "problem in neighbor list");
  Real contribution =
      4.0 * epsilon *
      (MathTools::ipow(sigma / rij, 12) - MathTools::ipow(sigma / rij, 6));

  return contribution;
}
/* -------------------------------------------------------------------------- */
void DomainMD1D::updateAcceleration() {
  this->acceleration() =
      code_unit_system.ft_m2v * this->gradient() / this->mass();
}

/* -------------------------------------------------------------------------- */

void DomainMD1D::updateGradient() {

  atoms.f.setZero();
  atoms.a.setZero();
  atoms.pe.setZero();

  // loop over atoms to compute their force
  UInt nb_atoms = atoms.size();
  for (UInt i = 0; i < nb_atoms; ++i) {

    // loop over right neighbors
    for (auto &&j : neighbor_lists[i]) {
      Real rij = this->computeDistance(atoms.p[j], atoms.p[i]);
      if (std::fabs(rij) <= rcut) {
        auto f = ljForce(rij);
        atoms.f[i] -= f;
        atoms.f[j] += f;
        auto epot = ljBondEnergy(rij);
        atoms.pe[i] += epot / 2;
        atoms.pe[j] += epot / 2;
      }
    }
  }

  this->changeRelease();
}
/* -------------------------------------------------------------------------- */
// Added by Srinivasa Babu Ramisetti
void DomainMD1D::linearForceComputation() {

  double contribution;
  double rij;
  double pos_right = 0, pos_left = 0;
  int i;

  int nb_atoms = atoms.size();

  for (i = 0; i < nb_atoms; ++i) {
    atoms.pe[i] = 0.0;
    double pos_i = atoms.p[i];
    int left = (i - 1 + nb_atoms) % nb_atoms;
    int right = (i + 1) % nb_atoms;

    if (periodic_flag != 1) {
      left = i - 1;
      right = i + 1;
    }

    if (right < nb_atoms) {
      pos_right = atoms.p[right];
      rij = computeDistance(pos_i, pos_right);
      contribution = (rij - r0) * stiffness_parameter;
      atoms.f[i] += contribution;
      atoms.pe[i] += (rij - r0) * contribution / 4.0;
    }
    if (left >= 0) {
      pos_left = atoms.p[left];
      rij = computeDistance(pos_i, pos_left);
      contribution = (rij - r0) * stiffness_parameter;
      atoms.f[i] -= contribution;
      atoms.pe[i] += (rij - r0) * contribution / 4.0;
    }
    atoms.a[i] = atoms.f[i] / atoms.m[i];
  }
}

/* -------------------------------------------------------------------------- */
void DomainMD1D::getNeighborList(RefAtom &atom, UInt count,
                                 std::vector<RefAtom> &neigbhor_list) {
  UInt index_count = 0;
  neigbhor_list.clear();
  RefMD1D r1d(atoms);
  int current_id = atom.id();
  int nb_atoms = atoms.size();

  if (periodic_flag == 1) {
    // DUMP(" Current atom id "<< atom.id() << " pos0 " << atom.position0(0) <<
    // " nb_coefficients " << count << " nb_atoms " << nb_atoms, DBG_MESSAGE);
    int id = 1;
    int i_id1 = (current_id + id);
    int i_id2 = (current_id - id);
    //    Real d = fabs(atom.position0(0) - dofs[i_id1].p0);
    // if(current_id == (nb_atoms - 1))
    //   d = fabs(atom.position0(0) - dofs[i_id2].p0);

    // while(d < distance){
    while (index_count < count) {
      i_id1 = (current_id + id);
      if (i_id1 < 0 || i_id1 >= nb_atoms) {
        i_id1 = (current_id + id) % nb_atoms;
        // d = fabs(atom.position0(0) - dofs[current_id - id].p0);
      }
      r1d.setIndex(i_id1);
      neigbhor_list.push_back(r1d);

      i_id2 = (current_id - id);
      if (i_id2 < 0 || i_id2 > nb_atoms) {
        i_id2 = (current_id - id + nb_atoms) % nb_atoms;
        // d = fabs(atom.position0(0) - dofs[current_id + id].p0);
      }
      r1d.setIndex(i_id2);
      neigbhor_list.push_back(r1d);

      id++;
      index_count++;
      // DUMP(" l_i "<< i_id2 << " r_i " << i_id1 << " vel[l] " << v2 << "
      // vel[r] " << v1 , DBG_MESSAGE);
    }
  } else {
    DUMP(" Current atom id " << atom.id() << " pos0 " << atom.position0()
                             << " nb_atoms " << nb_atoms,
         DBG_DETAIL);
    int id = 1;
    // Real d = fabs(atom.position0(0) - dofs[current_id + 1].p0);
    // while(d < distance){
    while (index_count < count) {
      if ((current_id + id) < 0 || (current_id + id) > nb_atoms)
        LM_FATAL("Neighbor id's are out of bound!");
      r1d.setIndex(current_id + id);
      neigbhor_list.push_back(r1d);
      if ((current_id - id) < 0 || (current_id - id) > nb_atoms)
        LM_FATAL("Neighbor id's are out of bound!");
      r1d.setIndex(current_id - id);
      neigbhor_list.push_back(r1d);

      DUMP(" neigbhor id on left : " << current_id - id
                                     << " neigbhor id on right : "
                                     << current_id + id,
           DBG_DETAIL);

      id++;
      // d = fabs(atom.position0(0) - dofs[current_id + id].p0);
      // count = count + 2;
      index_count++;
    }
  }

  DUMP("Number of neighbor's found " << index_count * 2, DBG_DETAIL);
}

Real DomainMD1D::getElasticConstant() { return this->stiffness_parameter; }

/* -------------------------------------------------------------------------- */

Real DomainMD1D::getR0() { return this->r0; }

/* -------------------------------------------------------------------------- */

int DomainMD1D::getNbDofs() { return this->atoms.size(); }

/* -------------------------------------------------------------------------- */

UInt DomainMD1D::getDim() { return 1; }

/* -------------------------------------------------------------------------- */

Cube DomainMD1D::getDomainBoundingBox() const {
  Cube c;
  c.getXmin()[0] = *std::min_element(atoms.p0.begin(), atoms.p0.end());
  c.getXmax()[0] = *std::max_element(atoms.p0.begin(), atoms.p0.end());
  return c;
}

/* -------------------------------------------------------------------------- */

void DomainMD1D::setPeriodicFlag(bool flag) { periodic_flag = flag; }

/* -------------------------------------------------------------------------- */

void DomainMD1D::setReferenceManagerState(bool) {}

/* -------------------------------------------------------------------------- */

bool DomainMD1D::isPeriodic(UInt dir) { return (periodic_flag && dir == 1); }

/* -------------------------------------------------------------------------- */

/* LMDESC MD1D
   This plugin is the implementation of a very simple
   one dimensional molecular dynamics class.
*/

/* LMHERITANCE domain_md */

/* LMEXAMPLE

   .. code-block::

     Section MultiScale RealUnits \\
     ...\\
     GEOMETRY myGeom CUBE BBOX -1 1 -1 1 -1 1\\
     MODEL MD1D md\\
     ...\\
     endSection\\ \\


     Section MD1D:md RealUnits
     RCUT rcut
     R0 r0
     MASS 39.95e-3
     TIMESTEP 1
     EPSILON 1.6567944e-21/6.94769e-21
     SIGMA 1.1

     DOMAIN_GEOMETRY 1
     endSection

 */

void DomainMD1D::declareParams() {

  DomainAtomic<ContainerMD1D, 1>::declareParams();

  /* LMKEYWORD RCUT
     Sets the cutoff radius for force calculation
   */
  this->parseKeyword("RCUT", rcut);
  /* LMKEYWORD R0
     Sets the interatomic distance
   */
  this->parseKeyword("R0", r0);
  /* LMKEYWORD SHIFT
     used in geometry creation
  */
  this->parseKeyword("SHIFT", shift, 0.);
  /* LMKEYWORD MASS
     Sets the per atom mass
  */
  this->parseKeyword("MASS", atomic_mass);
  /* LMKEYWORD EPSILON
     Sets the LJ energy parameter
  */
  this->parseKeyword("EPSILON", epsilon);
  /* LMKEYWORD SIGMA
     Sets the LJ distance parameter
  */
  this->parseKeyword("SIGMA", sigma);
  /* LMKEYWORD PERIODIC
     Sets the periodic boundary condition
  */
  this->parseTag("PERIODIC", periodic_flag, false);
  /* LMKEYWORD ELASTIC
     Sets the flag to switch elastic interactions
  */
  this->parseTag("ELASTIC", elastic_flag, false);

  /* LMKEYWORD ATOM_FILE
     Sets the file to read the atomic positions
  */
  this->parseKeyword("ATOM_FILE", atom_file, "");
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
