/**
 * @file   md_builder.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Jan 10 20:47:45 2014
 *
 * @brief  This class constructs the 1D atomic chains
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

/* -------------------------------------------------------------------------- */
#ifndef __LIBMULTISCALE_MD_BUILDER_HH__
#define __LIBMULTISCALE_MD_BUILDER_HH__
/* -------------------------------------------------------------------------- */
#include "lib_geometry.hh"
#include "ref_atom_md1d.hh"
#include "container_md1d.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class ZoneMDBuilder {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  ZoneMDBuilder(){};
  ~ZoneMDBuilder(){};

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  void buildMD1D(const Cube &b, const Real &r0, const Real &mass,
                 const Real &shift, bool periodic_flag, ContainerMD1D &dom);

  void readMD1D(const std::string &filename, const Cube &b, const Real &r0,
                const Real &mass, ContainerMD1D &dom);
};

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_MD_BUILDER_HH__ */
