/**
 * @file   ref_atom_md1d.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Fri Jan 10 20:47:45 2014
 *
 * @brief  This is the reference over atoms handled by the domainMD1D
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_REF_ATOM_MD1D_HH__
#define __LIBMULTISCALE_REF_ATOM_MD1D_HH__
/* -------------------------------------------------------------------------- */
#include "ref_atom.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
class ContainerMD1D;
class ComparatorRefMD1D;
class DomainMD1D;
/* -------------------------------------------------------------------------- */

/**
 * Class RefMD1D
 *
 */

class RefMD1D : public RefAtom<1, RefMD1D> {

public:
  typedef ComparatorRefMD1D RefComparator;
  typedef DomainMD1D Domain;

  friend class IteratorMD1D;
  friend class ContainerMD1D;
  friend class ComparatorRefMD1D;

  static constexpr UInt Dim = 1;

  using fields = field_list<_position0, _position, _displacement, _velocity,
                            _force, _mass, _acceleration>;

  RefMD1D();
  RefMD1D(ContainerMD1D &dom, UInt index = 0);

  bool operator==(RefMD1D &ref) { return (ref.index_atom == index_atom); };

  VectorView<Dim> position0();
  VectorView<Dim> position();
  AccessorAtomDof<RefMD1D, _displacement> displacement();
  VectorView<Dim> velocity();
  VectorView<Dim> force();
  VectorView<Dim> acceleration();
  VectorView<Dim> torque();
  VectorView<Dim> angular_velocity();
  Real NormContrainte() { return 0; };
  Real &mass();
  void setMass(Real mass);
  UInt typeID() { return 0; };
  Real &charge();
  UInt id();
  UInt tag();
  void setIndex(UInt n);
  UInt getIndex();
  Real epot();
  Real stress() { LM_TOIMPLEMENT; };

private:
  Real *_m;
  Real *_charge;
  Real *_p0;
  Real *_p;
  Real *_v;
  Real *_f;
  Real *_a;
  Real *_pe;
  // currently pointed atom
  UInt index_atom;
};

/* -------------------------------------------------------------------------- */

inline RefMD1D::RefMD1D()
    : _m(nullptr), _charge(nullptr), _p0(nullptr), _p(nullptr), _v(nullptr),
      _f(nullptr), _a(nullptr), _pe(nullptr), index_atom(0) {}

/* -------------------------------------------------------------------------- */

inline void RefMD1D::setIndex(UInt n) { index_atom = n; }
/* -------------------------------------------------------------------------- */

inline UInt RefMD1D::getIndex() { return index_atom; }
/* -------------------------------------------------------------------------- */

inline UInt RefMD1D::id() { return index_atom; }
/* -------------------------------------------------------------------------- */

inline void RefMD1D::setMass(Real mass) { _m[index_atom] = mass; }
/* -------------------------------------------------------------------------- */

inline Real &RefMD1D::charge() { return _charge[index_atom]; }

/* -------------------------------------------------------------------------- */

inline UInt RefMD1D::tag() { return index_atom; }
/* -------------------------------------------------------------------------- */

inline VectorView<1> RefMD1D::position0() {
  return VectorView<1>(_p0 + index_atom);
}
/* -------------------------------------------------------------------------- */

inline VectorView<1> RefMD1D::position() {
  return VectorView<1>(_p + index_atom);
}
/* -------------------------------------------------------------------------- */

inline AccessorAtomDof<RefMD1D, _displacement> RefMD1D::displacement() {
  return AccessorAtomDof<RefMD1D, _displacement>(*this);
}

/* -------------------------------------------------------------------------- */

inline VectorView<1> RefMD1D::velocity() {
  return VectorView<1>(_v + index_atom);
}
/* -------------------------------------------------------------------------- */

inline VectorView<1> RefMD1D::torque() { LM_TOIMPLEMENT; }
/* -------------------------------------------------------------------------- */

inline VectorView<1> RefMD1D::angular_velocity() { LM_TOIMPLEMENT; }

/* -------------------------------------------------------------------------- */

inline VectorView<1> RefMD1D::force() { return VectorView<1>(_f + index_atom); }
/* -------------------------------------------------------------------------- */
inline VectorView<1> RefMD1D::acceleration() {
  return VectorView<1>(_a + index_atom);
}
/* -------------------------------------------------------------------------- */

inline Real &RefMD1D::mass() { return _m[index_atom]; }
/* -------------------------------------------------------------------------- */

inline Real RefMD1D::epot() { return _pe[index_atom]; }

/* -------------------------------------------------------------------------- */

class ComparatorRefMD1D {
public:
  bool operator()(RefMD1D &r1, RefMD1D &r2) const {
    return r1.index_atom < r2.index_atom;
  }

  bool operator()(const RefMD1D &r1, const RefMD1D &r2) const {
    return r1.index_atom < r2.index_atom;
  }
};

/* -------------------------------------------------------------------------- */

inline std::ostream &operator<<(std::ostream &os, RefMD1D &ref) {
  UInt n = ref.getIndex();

  os << "MD1D reference : " << n;
  return os;
}

__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

#endif /* __LIBMULTISCALE_REF_ATOM_MD1D_HH__ */
