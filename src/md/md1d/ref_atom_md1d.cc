#include "ref_atom_md1d.hh"
#include "container_md1d.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

RefMD1D::RefMD1D(ContainerMD1D &c, UInt index) : index_atom(index) {
  _m = c.m.data();
  //  _charge = c.charge.data();
  _p0 = c.p0.data();
  _p = c.p.data();
  _v = c.v.data();
  _f = c.f.data();
  _a = c.acceleration.data();
  _pe = c.pe.data();
}
/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
