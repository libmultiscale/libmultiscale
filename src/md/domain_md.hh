/**
 * @file   domain_md.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Oct 29 22:09:05 2013
 *
 * @brief  Mother to all molecular domains
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_DOMAIN_MD_HH__
#define __LIBMULTISCALE_DOMAIN_MD_HH__
/* -------------------------------------------------------------------------- */
#include "domain_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/**
 * Class DomainAtomic
 *
 */

template <typename ContAtoms, UInt Dim>
class DomainAtomic : public DomainInterface {

public:
  using ContainerAtoms = ContAtoms;
  using IteratorAtoms = typename ContainerAtoms::iterator;
  using RefAtom = typename ContainerAtoms::Ref;
  using ContainerPoints = ContainerAtoms;
  using Output = ContainerPoints;
  using IteratorPoints = typename ContainerAtoms::iterator;
  using RefPoint = typename ContainerAtoms::Ref;
  using ContainerSubset = typename ContainerAtoms::ContainerSubset;

  DomainAtomic(const LMID &id, CommGroup group)
      : DomainInterface(group), atoms(id){};

  void init() override { createOutput("atoms") = atoms; }

  virtual ~DomainAtomic() {}

  //! component compute does nothing for md domains
  void compute_make_call(){};
  //! read XMLFile to relaod a situation
  void readXMLFile(const std::string &filename);
  //! implementation of the parsing method
  virtual void declareParams();
  //! connect and build the adapted object
  void connect(FactoryMultiScale<FilterInterface> &f);
  //! connect and build the adapted object
  void connect(FactoryMultiScale<ActionInterface> &f);
  //! return the container of atoms
  ContainerAtoms &getContainer();

  // possibly deadcode
  // //! get the elastic constant
  // virtual Real getElasticConstant() = 0;
  // //! get the R0
  // virtual Real getR0() = 0;
  // //! get the nb of dofs
  // virtual int getNbDofs() = 0;
  //! return value of arlequin flag
  // bool arlequin() { return arlequin_flag; }

protected:
  //! flag to active arlequin energy weigthing
  // bool arlequin_flag;
  //! atoms container
  ContainerAtoms atoms;
};
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_DOMAIN_MD_HH__ */
