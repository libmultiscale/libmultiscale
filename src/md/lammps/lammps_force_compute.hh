/**
 * @file   lammps_force_compute.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  LAMMPS hook to recompute stress
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_LAMMPS_FORCE_COMPUTE_HH__
#define __LIBMULTISCALE_LAMMPS_FORCE_COMPUTE_HH__
/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
// LAMMPS includes
#include <angle.h>
#include <atom.h>
#include <bond.h>
#include <comm.h>
#include <compute_stress_atom.h>
#include <dihedral.h>
#include <force.h>
#include <improper.h>
#include <kspace.h>
#include <modify.h>
#include <pair.h>
#include <update.h>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class LammpsForceCompute {

public:
  ~LammpsForceCompute(){};
  LammpsForceCompute(LAMMPS_NS::LAMMPS *lmp);

  bool computed();
  void compute(int eflag, int vflag);
  void forceBackup();
  void forceRestore();
  void force_clear(UInt vflag);

protected:
  bool _computed;
  UInt time;
  Array force_backup;
  int vflag_backup;
  int eflag_backup;

  LAMMPS_NS::LAMMPS *lammps_object;
};
/* -------------------------------------------------------------------------- */

inline LammpsForceCompute::LammpsForceCompute(LAMMPS_NS::LAMMPS *lmp) {
  _computed = false;
  time = 0;
  lammps_object = lmp;
}
/* -------------------------------------------------------------------------- */
inline bool LammpsForceCompute::computed() {
  return ((time == current_step) && _computed == true);
}

/* -------------------------------------------------------------------------- */
inline void LammpsForceCompute::force_clear(UInt) {
  // clear global force array
  // nall includes ghosts only if either newton flag is set

  UInt nall = lammps_object->atom->nlocal;
  if (lammps_object->force->newton)
    nall += lammps_object->atom->nghost;

  Real **f = lammps_object->atom->f;
  for (UInt i = 0; i < nall; i++) {
    f[i][0] = 0.0;
    f[i][1] = 0.0;
    f[i][2] = 0.0;
  }

  if (lammps_object->atom->torque) {
    Real **torque = lammps_object->atom->torque;
    for (UInt i = 0; i < nall; i++) {
      torque[i][0] = 0.0;
      torque[i][1] = 0.0;
      torque[i][2] = 0.0;
    }
  }

  //   if (granflag) {
  //     Real **phia = atom->phia;
  //     for (i = 0; i < nall; i++) {
  //       phia[i][0] = 0.0;
  //       phia[i][1] = 0.0;
  //       phia[i][2] = 0.0;
  //     }
  //   }

  // clear pair force array if using it this timestep to compute virial

  //   if (vflag == 2 && pairflag) {
  //     if (atom->nmax > maxpair) {
  //       maxpair = atom->nmax;
  //       memory->destroy_2d_Real_array(f_pair);
  //       f_pair = memory->create_2d_Real_array(maxpair,3,"verlet:f_pair");
  //     }
  //     for (i = 0; i < nall; i++) {
  //       f_pair[i][0] = 0.0;
  //       f_pair[i][1] = 0.0;
  //       f_pair[i][2] = 0.0;
  //     }
  //   }
}

/* -------------------------------------------------------------------------- */

inline void LammpsForceCompute::compute(int eflag, int vflag) {

  this->force_clear(vflag);

  if (lammps_object->modify->n_pre_force) {
    lammps_object->modify->pre_force(vflag);
  }

  if (lammps_object->force->pair && lammps_object->force->pair->compute_flag) {
    lammps_object->force->pair->compute(eflag, vflag);
  }

  if (lammps_object->atom->molecular) {
    if (lammps_object->force->bond)
      lammps_object->force->bond->compute(eflag, vflag);
    if (lammps_object->force->angle)
      lammps_object->force->angle->compute(eflag, vflag);
    if (lammps_object->force->dihedral)
      lammps_object->force->dihedral->compute(eflag, vflag);
    if (lammps_object->force->improper)
      lammps_object->force->improper->compute(eflag, vflag);
  }

  if (lammps_object->force->kspace) {
    lammps_object->force->kspace->compute(eflag, vflag);
  }

  if (lammps_object->modify->n_pre_reverse) {
    lammps_object->modify->pre_reverse(eflag, vflag);
  }

  if (lammps_object->force->newton) {
    lammps_object->comm->reverse_comm();
  }

  lammps_object->modify->post_force(vflag);

  _computed = true;
  time = current_step;
}

/* -------------------------------------------------------------------------- */

inline void LammpsForceCompute::forceBackup() {

  UInt nlocal = lammps_object->atom->nlocal;
  Real **f = lammps_object->atom->f;

  force_backup.resize(nlocal, 3);
  force_backup = ArrayView(f[0], nlocal, 3);

  this->vflag_backup = lammps_object->update->vflag_atom;
  this->eflag_backup = lammps_object->update->eflag_atom;

  lammps_object->update->vflag_atom = lammps_object->update->ntimestep;
  lammps_object->update->eflag_atom = lammps_object->update->ntimestep;

  // for (UInt i = 0; i < nlocal; ++i) {
  //   for (UInt k = 0; k < 3; ++k) {
  //     force_backup[i * 3 + k] = f[i][k];
  //   }
  // }
}

/* -------------------------------------------------------------------------- */

inline void LammpsForceCompute::forceRestore() {
  UInt nlocal = lammps_object->atom->nlocal;
  Real **f = lammps_object->atom->f;

  ArrayView(f[0], nlocal, 3) = force_backup;

  lammps_object->update->vflag_atom = this->vflag_backup;
  lammps_object->update->eflag_atom = this->eflag_backup;

  // for (UInt i = 0; i < nlocal; ++i) {
  //   for (UInt k = 0; k < 3; ++k) {
  //     f[i][k] = force_backup[i * 3 + k];
  //   }
  // }
}

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_LAMMPS_FORCE_COMPUTE_HH__ */
