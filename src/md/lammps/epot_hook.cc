/**
 * @file   epot_hook.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri May 10 14:59:01 2013
 *
 * @brief  LAMMPS hook to recompute potential energy
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "epot_hook.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
// LAMMPS includes
#include <angle.h>
#include <atom.h>
#include <atom_vec.h>
#include <bond.h>
#include <comm.h>
#include <dihedral.h>
#include <force.h>
#include <improper.h>
#include <kspace.h>
#include <lammps.h>
#include <modify.h>
#include <pair.h>
/* -------------------------------------------------------------------------- */
#include <vector>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

EpotHookLammps::EpotHookLammps(LAMMPS_NS::LAMMPS *lmp)
    : LammpsForceCompute(lmp) {
  char arg0[10] = "2";
  char arg1[10] = "all";
  char arg2[20] = "pe/atom";
  char *args[] = {arg0, arg1, arg2};

  lammps_epot =
      std::make_shared<LAMMPS_NS::ComputePEAtom>(lammps_object, 3, args);
}

/* -------------------------------------------------------------------------- */
EpotHookLammps::~EpotHookLammps() {}
/* -------------------------------------------------------------------------- */

void EpotHookLammps::computeEpot() {
  forceBackup();
  this->compute(2, 0);
  this->lammps_epot->compute_peratom();
  forceRestore();
}

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
