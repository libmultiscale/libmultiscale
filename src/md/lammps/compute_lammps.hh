/**
 * @file   compute_lammps.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  Generic compute which actually uses a LAMMPS compute
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_LAMMPS_HH__
#define __LIBMULTISCALE_COMPUTE_LAMMPS_HH__
/* -------------------------------------------------------------------------- */
#include "compute_interface.hh"
#include "domain_interface.hh"
#include "lammps.h"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

template <typename Comp> class ComputeLammps : public ComputeInterface {

public:
  ComputeLammps(Comp &c, DomainInterface &domain, LAMMPS_NS::LAMMPS &lmp);
  virtual ~ComputeLammps();

  void build();

  void copyVector();
  void copyScalar();
  void copyArray();
  void copyPerAtom();
  void _copyPerAtom();

  void compute_make_call();

protected:
  Comp &comp_obj;
  LAMMPS_NS::LAMMPS &lmp;
};

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_COMPUTE_LAMMPS_HH__ */
