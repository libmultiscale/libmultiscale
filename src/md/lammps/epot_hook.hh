/**
 * @file   epot_hook.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Apr 29 18:04:29 2013
 *
 * @brief  LAMMPS hook to recompute potential energy
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_EPOT_HOOK_HH__
#define __LIBMULTISCALE_EPOT_HOOK_HH__
/* -------------------------------------------------------------------------- */
#include "lammps_force_compute.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
#include <compute_pe_atom.h>
#include <lammps.h>
#include <vector>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

class EpotHookLammps : public LammpsForceCompute {

public:
  EpotHookLammps(LAMMPS_NS::LAMMPS *lmp);
  ~EpotHookLammps();

  Real epot(UInt i);

private:
  void computeEpot();
  std::shared_ptr<LAMMPS_NS::ComputePEAtom> lammps_epot;
};

/* -------------------------------------------------------------------------- */

inline Real EpotHookLammps::epot(UInt i) {
  if (!this->computed())
    this->computeEpot();

  return lammps_epot->energy[i];
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_EPOT_HOOK_HH__ */
