/**
 * @file   stress_hook.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  LAMMPS hook to recompute stress
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_STRESS_HOOK_HH__
#define __LIBMULTISCALE_STRESS_HOOK_HH__
/* -------------------------------------------------------------------------- */
#include "lammps_force_compute.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
// LAMMPS includes
#include <comm.h>
#include <compute_stress_atom.h>
#include <pair.h>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class StressHookLammps : public LammpsForceCompute {

public:
  ~StressHookLammps();
  StressHookLammps(LAMMPS_NS::LAMMPS *lmp);

  inline VectorView<6> stress(UInt i);

private:
  void computeStress();
  std::shared_ptr<LAMMPS_NS::ComputeStressAtom> lammps_stress;
};
/* -------------------------------------------------------------------------- */

VectorView<6> StressHookLammps::stress(UInt i) {
  if (!this->computed())
    this->computeStress();

  return VectorView<6>(lammps_stress->stress[i]);
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_STRESS_HOOK_HH__ */
