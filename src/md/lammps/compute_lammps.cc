/**
 * @file   compute_lammps.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  Generic compute which actually uses a LAMMPS compute
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "compute_lammps.hh"
#include "fix.h"
#include "lib_md.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
template <typename Comp>
ComputeLammps<Comp>::ComputeLammps(Comp &c, DomainInterface &domain,
                                   LAMMPS_NS::LAMMPS &lmp)
    : LMObject(std::string(c.id)), comp_obj(c), lmp(lmp) {
  createInput("input") = domain;
  this->createArrayOutput("output");
}

/* -------------------------------------------------------------------------- */
template <typename Comp> ComputeLammps<Comp>::~ComputeLammps() {}

/* -------------------------------------------------------------------------- */

template <typename Comp> void ComputeLammps<Comp>::copyScalar() {

  Real res = this->comp_obj.compute_scalar();
  this->getOutputAsArray().push_back(res);
  // this->name_computed.push_back(this->comp_obj.id);

  if (this->comp_obj.vector_flag)
    copyArray();
}
/* -------------------------------------------------------------------------- */

template <typename Comp> void ComputeLammps<Comp>::copyArray() {
  UInt size = this->comp_obj.size_vector;
  this->comp_obj.compute_vector();
  Real *vector = this->comp_obj.vector;
  for (UInt i = 0; i < size; i++) {
    this->getOutputAsArray().push_back(vector[i]);
    std::stringstream sstr;
    sstr << this->comp_obj.id << "[" << i << "]";
    // this->name_computed.push_back(sstr.str());
  }
}

/* -------------------------------------------------------------------------- */

template <typename Comp> void ComputeLammps<Comp>::copyVector() {
  LM_TOIMPLEMENT;
}

/* -------------------------------------------------------------------------- */

template <> void ComputeLammps<LAMMPS_NS::Fix>::copyArray() {
  UInt m = this->comp_obj.size_array_rows;
  UInt n = this->comp_obj.size_array_cols;

  for (UInt i = 0; i < m; ++i) {
    for (UInt j = 0; j < n; ++j) {
      Real res = this->comp_obj.compute_array(i, j);
      this->getOutputAsArray().push_back(res);
    }
  }

  for (UInt i = 0; i < n; ++i) {
    std::stringstream sstr;
    sstr << this->comp_obj.id << "[" << i << "]";
    // this->name_computed.push_back(sstr.str());
  }
}

/* -------------------------------------------------------------------------- */

template <typename Comp> void ComputeLammps<Comp>::copyPerAtom() {
  LM_TOIMPLEMENT;
}

/* -------------------------------------------------------------------------- */

template <> void ComputeLammps<LAMMPS_NS::Compute>::copyPerAtom() {
  this->comp_obj.compute_peratom();
  this->_copyPerAtom();
}

/* -------------------------------------------------------------------------- */

template <> void ComputeLammps<LAMMPS_NS::Fix>::copyPerAtom() {
  this->_copyPerAtom();
}

/* -------------------------------------------------------------------------- */

template <typename Comp> void ComputeLammps<Comp>::_copyPerAtom() {
  UInt ncol = this->comp_obj.size_peratom_cols;
  Real *_ptr = nullptr;
  if (ncol == 0) {
    ncol = 1;
    _ptr = &this->comp_obj.vector_atom[0];
  } else {
    _ptr = &this->comp_obj.array_atom[0][0];
  }
  UInt sz = lmp.atom->nlocal;
  using Arr =
      Eigen::Array<Real, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
  auto &array = this->getOutputAsArray();
  array = Eigen::Map<Arr>(_ptr, sz, ncol);
  std::cerr << array << std::endl;
  // if (ncol > 1)
  //   for (UInt i = 0; i < sz; ++i)
  //     for (UInt j = 0; j < ncol; ++j)
  //       std::cerr << i << "," << j << " = "
  //                 << this->comp_obj.array_atom[i][j] - array(i, j) <<
  //                 std::endl;
  //   this->getOutputAsArray()[i] = _ptr[i];
}

/* -------------------------------------------------------------------------- */

template <typename Comp> void ComputeLammps<Comp>::build() {

  this->clear();
  // this->name_computed.clear();

  if (this->comp_obj.scalar_flag)
    copyScalar();
  else if (this->comp_obj.peratom_flag)
    copyPerAtom();
  else if (this->comp_obj.array_flag)
    copyArray();
  else if (this->comp_obj.vector_flag)
    copyVector();
  else if (this->comp_obj.local_flag) {
    LM_TOIMPLEMENT;
  } else
    LM_FATAL(this->getID() + ": This Lammps compute does not produce"
                             " anything: cannot be used as input for LM");
}

/* -------------------------------------------------------------------------- */

template <typename Comp> void ComputeLammps<Comp>::compute_make_call() {
  DUMP(this->getID() << ": Compute", DBG_INFO);
  this->build();
}

/* -------------------------------------------------------------------------- */

template class ComputeLammps<LAMMPS_NS::Compute>;
template class ComputeLammps<LAMMPS_NS::Fix>;

__END_LIBMULTISCALE__
