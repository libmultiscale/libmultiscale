/**
 * @file   atom_vec_lm.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @brief  Atomic vector allowing to add libmultiscale bonus to atom packs
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_ATOM_VEC_LM_HH__
#define __LIBMULTISCALE_ATOM_VEC_LM_HH__
/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
#include "reference_manager_lammps.hh"
/* -------------------------------------------------------------------------- */
#include "atom_vec.h"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

using AtomIndexes = std::vector<int>;
using LMPPackBuffer = LAMMPS_NS::PackBuffer;

template <typename AtomVecLAMMPS, UInt Dim>
class AtomVecLM : public AtomVecLAMMPS {
public:
  AtomVecLM(const AtomVecLAMMPS &);
  ~AtomVecLM();

  void data_atom(double *, LAMMPS_NS::imageint,
                 const std::vector<std::string> &) override;
  void copy(int, int, int) override;
  void pack_exchange(int, LMPPackBuffer) override;
  void unpack_exchange(LMPPackBuffer) override;
  void pack_border(int n, AtomIndexes &list, LMPPackBuffer buf, int pbc_flag,
                   int *pbc) override;
  void unpack_border(int n, int first, LMPPackBuffer buf) override;
  void create_atom(int, Real *coord) override;
  void grow(int n) override;
  void alloc_x0();

  bool hasGeom(UInt itype = 0) { return geom.count(itype) > 0; }

  Geometry &getGeom(UInt itype = 0) {
    if (not this->hasGeom(itype))
      LM_FATAL("not known geometry with index: " << itype);
    return *geom[itype];
  };

  void setGeom(Geometry *g, UInt itype = 0) { this->geom[itype] = g; };

  //! set the ReferenceManager
  void setRefManager(std::shared_ptr<ReferenceManagerLammps<Dim>> refMgr) {
    ref_manager = refMgr;
  }

private:
  //! shared pointer to the reference manager
  std::shared_ptr<ReferenceManagerLammps<Dim>> ref_manager;
  std::map<UInt, Geometry *> geom;
};

/* ---------------------------------------------------------------------- */

template <typename AtomVecLAMMPS, UInt Dim>
AtomVecLM<AtomVecLAMMPS, Dim>::AtomVecLM(const AtomVecLAMMPS &avec)
    : AtomVecLAMMPS(avec) {}

/* ---------------------------------------------------------------------- */
template <typename AtomVecLAMMPS, UInt Dim>
AtomVecLM<AtomVecLAMMPS, Dim>::~AtomVecLM() {}

/* ----------------------------------------------------------------------
   copy atom I bonus info to atom J
------------------------------------------------------------------------- */

template <typename AtomVecLAMMPS, UInt Dim>
void AtomVecLM<AtomVecLAMMPS, Dim>::copy(int i, int j, int delflag) {
  AtomVecLAMMPS::copy(i, j, delflag);
  this->x0[j][0] = this->x0[i][0];
  this->x0[j][1] = this->x0[i][1];
  this->x0[j][2] = this->x0[i][2];

  ref_manager->moveAtom(i, j);
}

/* ----------------------------------------------------------------------
   pack data for atom I for sending to another proc
   xyz must be 1st 3 values, so comm::exchange() can test on them
------------------------------------------------------------------------- */

template <typename AtomVecLAMMPS, UInt Dim>
void AtomVecLM<AtomVecLAMMPS, Dim>::pack_exchange(int i, LMPPackBuffer buf) {
  int offset = buf.write_index();
  AtomVecLAMMPS::pack_exchange(i, buf);
  buf << this->x0[i][0];
  buf << this->x0[i][1];
  buf << this->x0[i][2];
  ref_manager->pack_subset_data(i, buf);
  buf[offset] << buf.write_index() - offset;
}

/* ---------------------------------------------------------------------- */

template <typename AtomVecLAMMPS, UInt Dim>
void AtomVecLM<AtomVecLAMMPS, Dim>::unpack_exchange(LMPPackBuffer buf) {
  int new_atom_index = this->atom->nlocal;
  AtomVecLAMMPS::unpack_exchange(buf);
  buf >> this->x0[new_atom_index][0];
  buf >> this->x0[new_atom_index][1];
  buf >> this->x0[new_atom_index][2];
  ref_manager->unpack_subset_data(new_atom_index, buf);
}

/* ---------------------------------------------------------------------- */

template <typename AtomVecLAMMPS, UInt Dim>
void AtomVecLM<AtomVecLAMMPS, Dim>::pack_border(int n, AtomIndexes &list,
                                                LMPPackBuffer buf, int pbc_flag,
                                                int *pbc) {

  double dx, dy, dz;
  AtomVecLAMMPS::pack_border(n, list, buf, pbc_flag, pbc);

  if (pbc_flag == 0) {
    for (int i = 0; i < n; i++) {
      int j = list[i];
      buf << this->x0[j][0];
      buf << this->x0[j][1];
      buf << this->x0[j][2];
    }
  } else {
    if (this->domain->triclinic == 0) {
      dx = pbc[0] * this->domain->xprd;
      dy = pbc[1] * this->domain->yprd;
      dz = pbc[2] * this->domain->zprd;
    } else {
      dx = pbc[0];
      dy = pbc[1];
      dz = pbc[2];
    }
    for (int i = 0; i < n; i++) {
      int j = list[i];
      buf << this->x0[j][0] + dx;
      buf << this->x0[j][1] + dy;
      buf << this->x0[j][2] + dz;
    }
  }
}

/* ---------------------------------------------------------------------- */

template <typename AtomVecLAMMPS, UInt Dim>
void AtomVecLM<AtomVecLAMMPS, Dim>::unpack_border(int n, int first,
                                                  LMPPackBuffer buf) {

  AtomVecLAMMPS::unpack_border(n, first, buf);
  for (int i = first; i < first + n; i++) {
    buf >> this->x0[i][0];
    buf >> this->x0[i][1];
    buf >> this->x0[i][2];
  }
}

/* ---------------------------------------------------------------------- */
template <typename AtomVecLAMMPS, UInt Dim>
void AtomVecLM<AtomVecLAMMPS, Dim>::data_atom(
    double *coord, LAMMPS_NS::imageint imagetmp,
    const std::vector<std::string> &values) {
  this->x0[this->atom->nlocal][0] = coord[0];
  this->x0[this->atom->nlocal][1] = coord[1];
  this->x0[this->atom->nlocal][2] = coord[2];
  AtomVecLAMMPS::data_atom(coord, imagetmp, values);
}

/* ---------------------------------------------------------------------- */

template <typename AtomVecLAMMPS, UInt Dim>
void AtomVecLM<AtomVecLAMMPS, Dim>::create_atom(int itype, Real *coord) {

  // add the atom to my list of atoms
  if (this->hasGeom(itype) and
      !getGeom(itype).contains(coord[0], coord[1], coord[2])) {
    return;
  }
  this->x0[this->atom->nlocal][0] = coord[0];
  this->x0[this->atom->nlocal][1] = coord[1];
  this->x0[this->atom->nlocal][2] = coord[2];
  AtomVecLAMMPS::create_atom(itype, coord);
}
/* ---------------------------------------------------------------------- */

template <typename AtomVecLAMMPS, UInt Dim>
void AtomVecLM<AtomVecLAMMPS, Dim>::grow(int n) {
  AtomVecLAMMPS::grow(n);
  this->alloc_x0();
}

/* ---------------------------------------------------------------------- */

template <typename AtomVecLAMMPS, UInt Dim>
void AtomVecLM<AtomVecLAMMPS, Dim>::alloc_x0() {
  this->x0 = this->memory->grow(this->atom->x0, this->nmax, 3, "atom:x0");
}

__END_LIBMULTISCALE__

#endif //__LIBMULTISCALE_ATOM_VEC_LM_HH__
