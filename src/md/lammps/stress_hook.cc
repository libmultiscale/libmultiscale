/**
 * @file   stress_hook.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  LAMMPS hook to recompute stress
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "stress_hook.hh"
#include "lammps_common.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

StressHookLammps::~StressHookLammps() {}

/* -------------------------------------------------------------------------- */

StressHookLammps::StressHookLammps(LAMMPS_NS::LAMMPS *lmp)
    : LammpsForceCompute(lmp) {

  char arg0[10] = "1";
  char arg1[10] = "all";
  char arg2[20] = "stress/atom";
  char arg3[10] = "NULL";
  char arg4[10] = "pair";
  char *args[] = {arg0, arg1, arg2, arg3, arg4};

  lammps_stress =
      std::make_shared<LAMMPS_NS::ComputeStressAtom>(lammps_object, 5, args);
}

/* -------------------------------------------------------------------------- */

void StressHookLammps::computeStress() {
  forceBackup();
  this->compute(0, 4);
  this->lammps_stress->compute_peratom();
  forceRestore();
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
