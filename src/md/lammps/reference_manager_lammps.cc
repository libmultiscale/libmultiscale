/**
 * @file   reference_manager_lammps.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Mar 13 16:53:59 2013
 *
 * @brief  This is the reference manager of the LAMMPS model which handles
 * migrations
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

/* -------------------------------------------------------------------------- */
#include "reference_manager_lammps.hh"
#include "domain_lammps.hh"
#include "lm_common.hh"
#include "trace_atom.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

template <UInt Dim>
ReferenceManagerLammps<Dim>::ReferenceManagerLammps(
    ContainerLammps<Dim> &global_container)
    : ReferenceManager<RefLammps<Dim>>(global_container) {}

/* -------------------------------------------------------------------------- */

// template <UInt Dim>
// void ReferenceManagerLammps<Dim>::sendAtom(UInt i, UInt proc) {
//   if (!this->flag_need_check_movements)
//     return;

//   RefLammps<Dim> refold(i);

//   IF_REF_TRACED(refold, "sending atom(" << i << ") to proc " << proc);
//   SET_REF_INTERNAL_TRACE_INDEX(refold, -1);

//   for (auto [temp, temp2] : this->moved) {
//     if (refold == temp2) {
//       DUMP("The atom to be sent was already moved away from ref"
//                << temp << " vers " << temp2,
//            DBG_DETAIL);
//       refold = temp;
//       this->moved.erase(temp);
//       LM_ASSERT(this->moved.count(temp) == 0, "pb avec la STL");
//       LM_ASSERT(refold.index_atom == temp.index_atom, "pb avec la STL");
//       break;
//     }
//   }

//   for (auto [temp, proc2] : this->sent) {
//     if (refold == temp) {
//       LM_FATAL(
//           "the atom " << refold
//                       << " that is requested to be sent was already sent to "
//                       << proc2 << " and now it is to be sent to " << proc);
//     }
//   }

//   this->sent[refold] = proc;
//   this->sent_byproc[proc].push_back(refold);

//   this->have_changed = true;
// }
/* -------------------------------------------------------------------------- */

template <UInt Dim> void ReferenceManagerLammps<Dim>::moveAtom(UInt i, UInt j) {

  if (!this->flag_need_check_movements)
    return;

  // pas la peine de faire un traitement si le deplacement ne fait rien
  if (i == j) {
    return;
  }

  RefLammps<Dim> refold(i);
  RefLammps<Dim> refnew(j);

  IF_REF_TRACED(refnew,
                "atom moved in memory from " << i << " to " << j << " index");
  SET_REF_INTERNAL_TRACE_INDEX(refnew, j);

  // TRACE_BREAKPOINT(refnew)

  DUMP("I move atom " << refold << " to " << refnew, DBG_INFO);

  LM_ASSERT(
      !isNaN(refnew.position0()),
      "the atom of reference "
          << refnew
          << " is out of control : initial position is not-a-number (NaN)");

  checkAttachedValue("before moving ", refold, this->subsets);
  auto mask = this->masks[refold];

  for (auto &&[name, s] : this->subsets) {
    if (mask & s->mask()) {
      s->translateMovingReference(refold, refnew);
      this->masks[refnew] = mask;
    }
  }
  this->masks.erase(refold);

  checkAttachedValue("after moving ", refnew, this->subsets);
}

/* ---------------------------------------------------------------------- */

template <UInt Dim>
void ReferenceManagerLammps<Dim>::pack_subset_data(UInt i, LMPPackBuffer buf) {
  // build a reflammps
  RefLammps<Dim> ref(i);

  checkAttachedValue("before packing", ref, this->subsets);

  IF_REF_TRACED(ref, "sending atom(" << ref.getIndex() << ")");
  // first of all pack the masks
  int mask = this->masks[ref];
  buf << mask;
  // loop over each subset
  for (auto &&[name, s] : this->subsets) {
    if (mask & s->mask()) {
      s->packData(ref, buf);
      mask = mask & ~s->mask();
      this->masks[ref] = mask;
    }
  }
  SET_REF_INTERNAL_TRACE_INDEX(ref, -1);
  checkAttachedValue("after packing", ref, this->subsets);
}

/* ---------------------------------------------------------------------- */

template <UInt Dim>
void ReferenceManagerLammps<Dim>::unpack_subset_data(UInt i,
                                                     LMPPackBuffer buf) {
  // build a reflammps
  RefLammps<Dim> ref(i);

  // checkAttachedValue("before unpacking", ref, this->subsets);

  IF_REF_TRACED(ref, "receiving atom(" << ref.getIndex() << ")");
  // first of all pack the masks
  int mask;
  mask << buf;
  // loop over each subset
  for (auto &&[name, s] : this->subsets) {
    if (mask & s->mask()) {
      s->unpackData(ref, buf);
      this->masks[ref] = mask;
    }
  }
  SET_REF_INTERNAL_TRACE_INDEX(ref, ref.getIndex());
  checkAttachedValue("after unpacking", ref, this->subsets);
}

// template <UInt Dim>
// void ReferenceManagerLammps<Dim>::acceptAtom(UInt i, UInt fromProc) {
//   if (!this->flag_need_check_movements)
//     return;

//   RefLammps<Dim> newref(i);
//   IF_REF_TRACED(newref, "atom received from proc " << fromProc);
//   SET_REF_INTERNAL_TRACE_INDEX(newref, i);

//   this->newrefs[fromProc].push_back(newref);

//   DUMP("I accept atom " << newref << " from " << fromProc, DBG_ALL);

//   LM_ASSERT(!isNaN(newref.position0()),
//             "l'atome de reference "
//                 << newref
//                 << "fait de la merde : pb dans la gestion des migrations "
//                 << "pour atome recu de " << fromProc);

//   for (auto &&it : this->moved) {
//     RefLammps<Dim> temp = it.first;
//     RefLammps<Dim> temp2 = it.second;
//     LM_ASSERT(!(newref == temp2),
//               "recois un atome a un emplacement deja utilise "
//                   << "par un deplacement depuis  " << temp << " vers "
//                   << temp2);
//   }

//   LM_ASSERT(!isNaN(newref.position0()),
//             "l'atome de reference "
//                 << newref
//                 << "fait de la merde : pb dans la gestion des migrations "
//                 << "pour atome recu de " << fromProc);

//   this->have_changed = true;
// }
/* --------------------------------------------------------------------------
 */
template <UInt Dim> void ReferenceManagerLammps<Dim>::compactSubsets() {
  for (auto &&[name, s] : this->subsets) {
    s->fillRemainingHoles();
  }
}
/* -------------------------------------------------------------------------- */

template class ReferenceManagerLammps<2>;
template class ReferenceManagerLammps<3>;
__END_LIBMULTISCALE__
