/**
 * @file   container_lammps.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Thu Jul 24 14:21:58 2014
 *
 * @brief  This is the container of lammps atomic references
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_CONTAINER_LAMMPS_HH__
#define __LIBMULTISCALE_CONTAINER_LAMMPS_HH__
/* -------------------------------------------------------------------------- */
#include "container.hh"
#include "ref_atom_lammps.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/**
 * Container for lammps atoms
 *
 */

template <UInt _Dim>
class ContainerLammps : public Container_base<RefLammps<_Dim>> {

public:
  class iterator;

  static constexpr UInt Dim = _Dim;

public:
  ContainerLammps(const LMID &id)
      : LMObject(id), Container_base<RefLammps<Dim>>(), stress_hook(nullptr),
        epot_hook(nullptr){};

  ~ContainerLammps(){};

  iterator begin();
  iterator end();

  UInt size() const override;

  RefLammps<_Dim> &get(UInt index);
  void init() {
    if (!stress_hook)
      stress_hook = std::make_shared<StressHookLammps>(lammps_main_object);
    if (!epot_hook)
      epot_hook = std::make_shared<EpotHookLammps>(lammps_main_object);
  };

  //! array of atom structures
  std::shared_ptr<StressHookLammps> stress_hook;
  std::shared_ptr<EpotHookLammps> epot_hook;
  RefLammps<Dim> at;
};

__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
#include "iterator_lammps.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

template <UInt _Dim>
inline typename ContainerLammps<_Dim>::iterator ContainerLammps<_Dim>::begin() {
  iterator it(*this);
  return it;
}

/* -------------------------------------------------------------------------- */

template <UInt Dim>
inline typename ContainerLammps<Dim>::iterator ContainerLammps<Dim>::end() {

  // UInt index_atom;
  // if (dt & dt_ghost)
  //   index_atom =
  //       lammps_main_object->atom->nlocal + lammps_main_object->atom->nghost;
  // else
  UInt index_atom = this->size();

  iterator it(*this, index_atom);
  return it;
}

/* -------------------------------------------------------------------------- */

template <UInt _Dim>
inline RefLammps<_Dim> &ContainerLammps<_Dim>::get(UInt index) {
  at.index_atom = index;
  at.stress_hook = this->stress_hook;
  at.epot_hook = this->epot_hook;
  return at;
}

/* -------------------------------------------------------------------------- */

template <UInt _Dim> inline UInt ContainerLammps<_Dim>::size() const {
  if (!lammps_main_object)
    return 0;
  if (!lammps_main_object->atom)
    return 0;
  // if (dt & dt_ghost)
  //   return lammps_main_object->atom->nlocal +
  //   lammps_main_object->atom->nghost;
  return lammps_main_object->atom->nlocal;
}

/* -------------------------------------------------------------------------- */

template <UInt Dim>
class ContainerLammpsMinimize : public ContainerLammps<Dim> {};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_CONTAINER_LAMMPS_HH__ */
