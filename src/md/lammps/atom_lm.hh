/**
 * @file   atom_vec_lm.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @brief  Atomic vector allowing to add libmultiscale bonus to atom packs
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_ATOM_LM_HH__
#define __LIBMULTISCALE_ATOM_LM_HH__
/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
#include "reference_manager_lammps.hh"
/* -------------------------------------------------------------------------- */
#include "atom.h"
#include "atom_vec_atomic.h"
#include "atom_vec_lm.hh"
#include "atom_vec_sphere.h"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

template <UInt Dim> class AtomLM : public LAMMPS_NS::Atom {
public:
  AtomLM(const LAMMPS_NS::Atom &avec,
         std::shared_ptr<ReferenceManagerLammps<Dim>> &ref_manager);

  LAMMPS_NS::AtomVec *new_avec(const std::string &, int, int &) override;

  std::shared_ptr<ReferenceManagerLammps<Dim>> ref_manager;
};

/* ---------------------------------------------------------------------- */

template <UInt Dim>
AtomLM<Dim>::AtomLM(const LAMMPS_NS::Atom &avec,
                    std::shared_ptr<ReferenceManagerLammps<Dim>> &ref_manager)
    : Atom(avec), ref_manager(ref_manager) {}

/* ---------------------------------------------------------------------- */
template <UInt Dim>
LAMMPS_NS::AtomVec *AtomLM<Dim>::new_avec(const std::string &style,
                                          int trysuffix, int &sflag) {

  auto *avec = LAMMPS_NS::Atom::new_avec(style, trysuffix, sflag);
  if (auto *ptr = dynamic_cast<LAMMPS_NS::AtomVecAtomic *>(avec)) {
    auto *avec = new AtomVecLM<LAMMPS_NS::AtomVecAtomic, Dim>(*ptr);
    avec->setRefManager(ref_manager);
    avec->alloc_x0();
    return avec;
  } else if (auto *ptr = dynamic_cast<LAMMPS_NS::AtomVecSphere *>(avec)) {
    auto *avec = new AtomVecLM<LAMMPS_NS::AtomVecSphere, Dim>(*ptr);
    avec->setRefManager(ref_manager);
    avec->alloc_x0();
    return avec;
  }
  LM_FATAL("need to extend here");
}

__END_LIBMULTISCALE__

#endif //__LIBMULTISCALE_ATOM_VEC_LM_HH__
