/**
 * @file   domain_lammps.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Thu Jul 31 22:41:23 2014
 *
 * @brief  This is the model wrapping LAMMPS
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_DOMAIN_LAMMPS_HH__
#define __LIBMULTISCALE_DOMAIN_LAMMPS_HH__
/* -------------------------------------------------------------------------- */
#include "container_lammps.hh"
#include "domain_md.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/**
 * Class DomainAtomic
 * domaine atomique
 */

template <UInt Dim>
class DomainLammps : public DomainAtomic<ContainerLammps<Dim>, Dim>,
                     public LAMMPS_NS::LAMMPS {

public:
  using InternalArray =
      Eigen::Array<Real, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

  DomainLammps(const LMID &ID, CommGroup group);
  ~DomainLammps();

  ArrayView getField(FieldType field_type) override;
  Real potentialEnergy() override;
  void enforceCompatibility() override;
  Cube getSubDomainBoundingBox() const override;
  Cube getDomainBoundingBox() const override;
  void setDomainBoundingBox(const Cube &cube) override;
  void setSubDomainBoundingBox(const Cube &cube) override;
  void init() override;
  void updateGradient() override;
  void updateAcceleration() override;
  ArrayView gradient() override;
  ArrayView primal() override;
  ArrayView primalTimeDerivative() override;
  ArrayView mass() override;
  ArrayView acceleration() override;

  void newmarkPredictor();
  void newmarkCorrector();

  bool isPeriodic(UInt dir) override;
  void readXMLFile(const std::string &filename) override;

protected:
  void setCurrentStep(UInt ts);
  void setTimeStep(Real ts);
  UInt getCurrentStep();
  Real getTimeStep();

  Real getLatticeConstant();
  int getOrientValue(UInt axis, UInt comp);
  //! get the elastic stiffness constant
  Real getElasticConstant() { LM_TOIMPLEMENT; };
  //! clear the force before a computation
  void force_clear(UInt vflag);
  //! migration and boundary conditions are applied by this method
  void exchangeAtoms();
  //! Initialize degree of freedom values (reload, initial positions,velocities,
  //! etc...)
  void initDOFs();
  //! read material structure set constants units etc...
  void initModel();
  //! function for keyword parsing
  void declareParams();
  //! load lammps conf file
  void loadLammpsConfigFile();
  //! perform change box size
  void changeBox();
  //! reset the ghost atoms etc...
  void resetBox();
  //! create a lammps header for details LM knows
  std::string createLammpsHeader();
  //! calls for domain specific output if any
  void output() override;

protected:
  //! array to store the acceleration
  InternalArray acceleration_field;
  //! array to store the angular acceleration
  InternalArray angular_acceleration_field;
  //! lammps configuration file name
  std::string lammpsfile;
  //! flag to unactivate every unit conversion
  bool flag_units;
  //! the requested box geometry
  LMID newGeomBox;
  //! the requested time when to change the box
  UInt when_change_box;
  //! flag to decide when to change the box
  UInt change_box;
  LAMMPS_NS::NeighList *neig_list;
  //! lattice name to be used in lammps config file generation
  std::string lattice;
  //! lattice length
  Quantity<Length> lattice_size;
  //! lattice origin
  Quantity<Length, 3> lattice_origin;
  //! lattice spacing
  Real lattice_spacing[3];
  //! orientations of lattice
  int lattice_orient_x[3];
  int lattice_orient_y[3];
  int lattice_orient_z[3];
  //! boundary selection
  std::string boundaries[3];
  //! replica in each direction
  int replica[6];
  //! change to triclinic box
  int triclinic;
  //! amount of tilting
  Quantity<Length, 2> tilt;
  //! flag to ask for automatic generation of the header
  bool create_header_flag;
  //! flag to inform the simulation is started from restart file
  // bool restart_start;
  //! shared pointer to the reference manager
  std::shared_ptr<ReferenceManagerLammps<Dim>> ref_manager;
  //! flag to say if masses are per type or per particles
  bool is_mass_per_particle = false;
};
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_DOMAIN_LAMMPS_HH__ */
