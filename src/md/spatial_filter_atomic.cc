/**
 * @file   spatial_filter_atomic.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Wed Dec 04 12:14:29 2013
 *
 * @brief  This class allows to perform spatial filtering of atomic fields
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define TIMER
/* -------------------------------------------------------------------------- */
#include "spatial_filter_atomic.hh"
#include "container_array.hh"
#include "lm_common.hh"
#include <fstream>
#include <sstream>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

void SpatialFilterAtomic::readKernelFromFile(const std::string &filename) {
  std::ifstream fs(filename.c_str());
  std::string line;
  std::vector<double> x;
  if (fs.is_open()) {
    while (!fs.eof()) {
      getline(fs, line);
      if (line == "")
        continue;
      if (line[0] == '#')
        continue;
      DUMP("read line in kernel " << line, DBG_DETAIL);
      std::stringstream ss;
      ss << line;
      Real j = 0;
      ss >> j;
      x.push_back(j);
      Real k_value;
      ss >> k_value;
      memory_kernel.push_back(k_value);
    }
    fs.close();
    DUMP("MEMORY KERNEL SIZE IS " << memory_kernel.size(), DBG_DETAIL);
  } else
    LM_FATAL(
        "cannot open the file to read the memory kernel file: " << filename);

  buildCSpline(x, memory_kernel);
}

/* -------------------------------------------------------------------------- */

void SpatialFilterAtomic::buildCSpline(std::vector<double> &x,
                                       std::vector<double> &memory_kernel) {

  kernel_length = memory_kernel.size();
  // kernel_distance_first_neigh = x[1];
  kernel_distance = x[kernel_length - 1];
  kernel_distance_sq = kernel_distance * kernel_distance;

  // Real r0 = 4.032*sqrt(2)/2;
  // std::ofstream fout("spatial_filter_kernel.txt");
  // for (UInt i = 0; i < kernel_length; ++i) {
  //   fout << std::scientific << x[i]/r0 << " " <<
  //   r0*r0*sqrt(3)/2*memory_kernel[i] << std::endl;
  // }
  // fout.close();

  std::vector<double> x_, f_;
  x_.reserve(x.size() - 1);
  f_.reserve(x.size() - 1);
  copy(x.begin() + 1, x.end(), back_inserter(x_));
  copy(memory_kernel.begin() + 1, memory_kernel.end(), back_inserter(f_));

  double spacing = ((x[kernel_length - 1]) - (x[kernel_length - 2]));
  // build the spline coefficients
  this->cSpline->buildSplineCoefficients(x_, f_, spacing);

  DUMP("Maximum distance between two atoms for filtering "
           << kernel_distance << " square of distance " << kernel_distance_sq
           << " spacing " << spacing,
       DBG_MESSAGE);
}
/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
