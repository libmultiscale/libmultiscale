/**
 * @file   trace_atom.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Sep 30 12:13:51 2013
 *
 * @brief  Set of macros to trace an atom through processor migrations
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "trace_atom.hh"
#ifdef LIBMULTISCALE_TRACE_ATOM
#include "lm_common.hh"
#include <map>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

typedef unsigned int UInt;
typedef double Real;

Real trace_ref[3] = {-3.6288, 26.6112, -1.6128};

Real trace_tolerance = 0.01;
UInt internal_index = -1;
__END_LIBMULTISCALE__
#endif
