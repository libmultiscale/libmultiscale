/**
 * @file   container_neighbor_atoms_interface.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Dec 06 13:47:13 2013
 *
 * @brief  This class virtualize a container of neighbors for a collection of
 * atoms
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_CONTAINER_NEIGHBOR_ATOMS_INTERFACE_HH__
#define __LIBMULTISCALE_CONTAINER_NEIGHBOR_ATOMS_INTERFACE_HH__
/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
#include <typeinfo>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class ContainerNeighborAtomsInterface {
  /* ------------------------------------------------------------------------ */
  /* Typedefs                                                                 */
  /* ------------------------------------------------------------------------ */
public:
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  ContainerNeighborAtomsInterface(){};
  virtual ~ContainerNeighborAtomsInterface(){};

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  template <typename T> void castPointer(T *&ptr) {
    LM_ASSERT(this, "This object is a NULL pointer");
    ptr = dynamic_cast<T *>(this);
    LM_ASSERT(ptr,
              "dynamic cast failed. True type is " << typeid(*this).name());
  }

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */
private:
};

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_CONTAINER_NEIGHBOR_ATOMS_INTERFACE_HH__ */
