/**
 * @file   ref_atom.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Nov 15 14:49:04 2013
 *
 * @brief  This is the mother class of all atomic reference
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_REF_ATOM_HH__
#define __LIBMULTISCALE_REF_ATOM_HH__
/* -------------------------------------------------------------------------- */
#include "accessor_dof.hh"
#include "cube.hh"
#include "lm_common.hh"
#include "ref_point.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
template <UInt Dim, typename DaugtherClass> class RefAtom;
/* -------------------------------------------------------------------------- */
template <typename Ref, FieldType ftype>
struct AccessorAtomDof : public AccessorDof<Ref, ftype> {
  using AccessorDof<Ref, ftype>::AccessorDof;
  using AccessorDof<Ref, ftype>::operator=;
};
/* -------------------------------------------------------------------------- */

/**
 * Class RefAtom
 *
 */

template <UInt Dim, typename DaughterClass>
class RefAtom : public RefPoint<Dim> {

public:
#define declare_accessor(_name)                                                \
  decltype(auto) _name() { return static_cast<DaughterClass *>(this)->_name(); }

  declare_accessor(position0);
  declare_accessor(position);
  declare_accessor(velocity);
  declare_accessor(force);
  declare_accessor(acceleration);
  declare_accessor(displacement);
  declare_accessor(mass);
  declare_accessor(charge);
  declare_accessor(angular_velocity);
  declare_accessor(torque);
  declare_accessor(radius);
  declare_accessor(id);
  declare_accessor(stress);
  declare_accessor(epot);

#undef declare_accessor

  virtual UInt typeID() { LM_TOIMPLEMENT; };
  virtual MatrixView<Dim> kinetic_stress() { LM_TOIMPLEMENT; };
  virtual UInt tag() { LM_TOIMPLEMENT; };
  virtual Real electronic_density() { LM_TOIMPLEMENT; };

  template <FieldType ftype> AccessorAtomDof<RefAtom, ftype> field() {
    AccessorAtomDof<RefAtom, ftype> acc(*this);
    return acc;
  }

public:
  static constexpr UInt dim = Dim;
};

/* -------------------------------------------------------------------------- */

template <typename Ref> class AccessorAtomDof<Ref, _displacement> {

public:
  static constexpr FieldType ftype = _displacement;
  using TensorType = Vector<Ref::Dim>;
  using Scalar = Real;
  static constexpr UInt TensorDim = Ref::Dim;

  AccessorAtomDof(Ref &at) : at(at) { disp = at.position() - at.position0(); };

  template <typename T> inline TensorType &operator=(const T &val) {
    at.position() = at.position0() + val;
    disp = val;
    return disp;
  }

  template <typename T>
  inline AccessorAtomDof<Ref, _displacement> &operator+=(const T &val) {
    disp += val;
    at.position() = at.position0() + disp;
    return *this;
  }

  UInt size() { return Ref::Dim; }

  class AccessorDisplacementDof {

  public:
    AccessorDisplacementDof(Ref &at, UInt index) : at(at), index(index) {}

    operator Real() { return (at.position() - at.position0())[index]; }

    template <typename T>
    inline AccessorDisplacementDof &operator=(const T &val) {
      at.position()[index] = val + at.position0()[index];
      return *this;
    }

    template <typename T>
    inline AccessorDisplacementDof &operator+=(const T &val) {
      at.position()[index] += val;
      return *this;
    }

    template <typename T>
    inline AccessorDisplacementDof &operator-=(const T &val) {
      at.position()[index] -= val;
      return *this;
    }

  private:
    Ref &at;
    UInt index;
  };

  inline AccessorDisplacementDof operator[](UInt i) {
    return AccessorDisplacementDof(at, i);
  }

  operator Vector<Ref::Dim>() { return Vector<Ref::Dim>(disp); }
  decltype(auto) data() { return disp.data(); };

  void printself(std::ostream &os) { os << disp; }

  Ref &at;
  TensorType disp;
};

/* -------------------------------------------------------------------------- */
template <typename Ref, FieldType ftype>
std::ostream &operator<<(std::ostream &os, AccessorAtomDof<Ref, ftype> &acc) {
  acc.printself(os);
  return os;
}
/* -------------------------------------------------------------------------- */
template <typename Ref, FieldType ftype>
std::ostream &operator<<(std::ostream &os, AccessorAtomDof<Ref, ftype> &&acc) {
  acc.printself(os);
  return os;
}
/* -------------------------------------------------------------------------- */

#undef DECLARE_ACCESSOR

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_REF_ATOM_HH__ */
