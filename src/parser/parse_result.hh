/**
 * @file   parse_result.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  Small object which encapsulate a parsing operation result
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_PARSE_RESULT_HH__
#define __LIBMULTISCALE_PARSE_RESULT_HH__
/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class ParseResult {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  ParseResult() {
    parsed = false;
    nb_word_parsed = 0;
  };

  ParseResult(bool res) {
    parsed = res;
    nb_word_parsed = 1;
  };
  ParseResult(bool res, UInt n) {
    parsed = res;
    nb_word_parsed = n;
  };

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  void max(ParseResult &p) {
    nb_word_parsed = std::max(nb_word_parsed, p.nb_word_parsed);
    parsed |= p.parsed;
  };

  void operator+=(ParseResult &p) {
    parsed = p.parsed;
    nb_word_parsed += p.nb_word_parsed;
  };

  void operator++() { ++nb_word_parsed; };

  operator bool() const { return parsed; }

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

  bool parsed;
  UInt nb_word_parsed;
};
/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_PARSE_RESULT_HH__ */
