/**
 * @file   lm_parser_inline_impl.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Thu Jul 24 14:21:58 2014
 *
 * @brief  This is the central parser for LM
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "lm_parser.hh"
#include <memory>

// backward declaration
namespace pybind11 {
class object;
class dict;
} // namespace pybind11

namespace py = pybind11;

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

template <typename T> inline UInt parseNumber(T &d, std::stringstream &line) {
  std::string buffer;
  UInt nb = Parser::strNext(buffer, line);
  Real res;
  bool alg_parsed = false;
  try {
    alg_parsed = Parser::math_parser.parse(buffer, res);
  } catch (std::string mess) {
    LM_FATAL(Parser::getParserState() << std::endl
                                      << "\"" << line.str() << "\"" << std::endl
                                      << mess);
  }
  if (!alg_parsed) {
    std::stringstream sbuf(buffer);
    sbuf >> res;
    if (sbuf.fail())
      LM_FATAL(Parser::getParserState()
               << std::endl
               << "\"" << line.str() << "\"" << std::endl
               << "could not parse " << line.str().substr(line.tellg())
               << " or " << buffer);
  }
  d = static_cast<T>(res);
  return nb;
}

/* -------------------------------------------------------------------------- */

template <> inline UInt parseNumber<bool>(bool &d, std::stringstream &line) {
  try {
    std::string buffer;
    UInt nb = Parser::strNext(buffer, line);
    std::transform(buffer.begin(), buffer.end(), buffer.begin(), ::tolower);
    if (buffer == "true")
      d = true;
    else if (buffer == "false")
      d = false;
    else if (buffer == "1")
      d = true;
    else if (buffer == "0")
      d = false;
    else {
      d = !d;
      return 0;
    }
    return nb;
  } catch (...) {
    d = !d;
  }
  return 0;
}

/* -------------------------------------------------------------------------- */

inline UInt _parse(std::string &val, std::stringstream &line, UInt = 0) {
  return Parser::strNext(val, line);
}
/* -------------------------------------------------------------------------- */

template <typename T>
std::enable_if_t<not std::is_arithmetic<T>::value, UInt>
_parse(T &val, std::stringstream &line, UInt n_entries = 0);

/* -------------------------------------------------------------------------- */

template <typename T>
inline std::enable_if_t<
    std::is_arithmetic<T>::value and not std::is_same<T, char>::value, UInt>
_parse(T &val, std::stringstream &line, UInt = 0) {
  return parseNumber<T>(val, line);
}

/* -------------------------------------------------------------------------- */
class Component;

UInt _parse(Component &comp, std::stringstream &line, UInt n_entries = 0);

/* -------------------------------------------------------------------------- */

UInt _parse(std::map<std::string, std::shared_ptr<pybind11::object>> val,
            std::stringstream &line, UInt n_entries = 0);

/* -------------------------------------------------------------------------- */

inline UInt _parse(char &val, std::stringstream &line, UInt = 0) {

  std::string buffer;
  UInt nb = Parser::strNext(buffer, line);
  std::stringstream sbuf(buffer);
  sbuf >> val;
  if (sbuf.fail())
    LM_FATAL(Parser::getParserState()
             << std::endl
             << "\"" << line.str() << "\"" << std::endl
             << "could not parse " << line.str().substr(line.tellg()) << " or "
             << buffer);
  return nb;
}

/* -------------------------------------------------------------------------- */

template <typename T>
inline UInt _parse(std::vector<T> &vec, std::stringstream &line, UInt = 0) {
  T tmp;
  UInt nb_word = _parse(tmp, line);
  vec.push_back(tmp);
  return nb_word;
}

/* -------------------------------------------------------------------------- */

template <UInt nb, typename T>
inline UInt _parse_vector(T &val, std::stringstream &line, UInt n_entries) {
  UInt nb_word = 0;
  using type = typename std::remove_reference<decltype(val[0])>::type;
  type tmp = type();

  if (n_entries > int(nb))
    LM_FATAL("invalid vector size: " << n_entries << " > " << nb);

  UInt size = nb;
  if (n_entries > 0)
    size = n_entries;

  for (UInt i = 0; i < size; ++i) {
    nb_word += _parse(tmp, line);
    val[i] = tmp;
  }
  return nb_word;
}

/* -------------------------------------------------------------------------- */

template <typename T, UInt nb>
inline UInt _parse(Vector<nb, T> &val, std::stringstream &line,
                   UInt n_entries = 0) {
  return _parse_vector<nb>(val, line, n_entries);
}

/* -------------------------------------------------------------------------- */

template <PhysicalQuantity q, typename T, UInt nb>
inline UInt _parse(Quantity<q, nb, T> &val, std::stringstream &line,
                   UInt n_entries = 0) {
  return _parse_vector<nb>(val, line, n_entries);
}

/* -------------------------------------------------------------------------- */

template <PhysicalQuantity q, typename T>
inline UInt _parse(PhysicalScalar<q, T> &val, std::stringstream &line,
                   UInt = 0) {
  Real tmp;
  UInt nb = _parse(tmp, line);
  val = tmp * Parser::units_converter.getConversion<q>();
  return nb;
}

/* -------------------------------------------------------------------------- */

inline UInt _parse(IntegrationSchemeMask &mask, std::stringstream &line,
                   UInt = 0) {

  IntegrationSchemeMask value;
  std::string read_stage;
  UInt nb = Parser::strNext(read_stage, line);

  if (read_stage == "PRE_DUMP")
    value = PRE_DUMP;
  else if (read_stage == "PRE_STEP1")
    value |= PRE_STEP1;
  else if (read_stage == "PRE_STEP2")
    value |= PRE_STEP2;
  else if (read_stage == "PRE_STEP3")
    value |= PRE_STEP3;
  else if (read_stage == "PRE_STEP4")
    value |= PRE_STEP4;
  else if (read_stage == "PRE_STEP5")
    value |= PRE_STEP5;
  else if (read_stage == "PRE_STEP6")
    value |= PRE_STEP6;
  else if (read_stage == "PRE_STEP7")
    value |= PRE_STEP7;
  else if (read_stage == "PRE_FATAL")
    value |= PRE_FATAL;
  else {
    std::stringstream sstr;
    sstr << read_stage;
    nb += parseNumber(value, sstr);
  }

  mask |= value;

  return nb;
}

/* -------------------------------------------------------------------------- */

inline UInt _parse(UnitSystem &usys, std::stringstream &line, UInt = 0) {
  std::string name;
  UInt nb = Parser::strNext(name, line);
  usys = UnitSystem::parseUnitSystemName(name);

  return nb;
}

/* -------------------------------------------------------------------------- */

template <typename T, UInt nb>
inline UInt _parse(T (&val)[nb], std::stringstream &line, UInt n_entries = 0) {
  return _parse_vector<nb>(val, line, n_entries);
}
/* -------------------------------------------------------------------------- */

inline UInt _parse(FieldType &ftype, std::stringstream &line, UInt = 0) {
  std::string name;
  UInt nb = Parser::strNext(name, line);
  if (name == "position0")
    ftype = _position0;
  else if (name == "position")
    ftype = _position;
  else if (name == "displacement")
    ftype = _displacement;
  else if (name == "velocity")
    ftype = _velocity;
  else if (name == "force")
    ftype = _force;
  else if (name == "temperature")
    ftype = _temperature;
  else if (name == "grouprank")
    ftype = _grouprank;
  else if (name == "stress")
    ftype = _stress;
  else if (name == "strain")
    ftype = _strain;
  else if (name == "epot")
    ftype = _epot;
  else if (name == "burgers")
    ftype = _burgers;
  else if (name == "normal")
    ftype = _normal;
  else if (name == "torque")
    ftype = _torque;
  else if (name == "angular_velocity")
    ftype = _angular_velocity;
  else if (name == "angular_acceleration")
    ftype = _angular_acceleration;
  else
    LM_FATAL("unknown field type " << name);

  return nb;
}
/* -------------------------------------------------------------------------- */

inline UInt _parse(Operator &op, std::stringstream &line, UInt = 0) {
  std::string name;
  UInt nb = Parser::strNext(name, line);
  if (name == "NONE")
    op = OP_NONE;
  else if (name == "AVERAGE")
    op = OP_AVERAGE;
  else if (name == "DENSITY")
    op = OP_DENSITY;
  else if (name == "DEVIATION")
    op = OP_DEVIATION;
  else if (name == "MAX")
    op = OP_MAX;
  else if (name == "MIN")
    op = OP_MIN;
  else if (name == "SUM")
    op = OP_SUM;
  else
    LM_FATAL("unknown operator " << name);

  return nb;
}
/* -------------------------------------------------------------------------- */

inline UInt _parse(LatticeType &ltype, std::stringstream &line, UInt = 0) {
  std::string name;
  UInt nb = Parser::strNext(name, line);
  if (name == "mono_atom_1d")
    ltype = mono_atom_1d;
  else if (name == "hex_2d")
    ltype = hex_2d;
  else if (name == "hex_2d_radial")
    ltype = hex_2d_radial;
  else if (name == "fcc_3d")
    ltype = fcc_3d;
  else
    LM_FATAL("unknown lattice type " << name);

  return nb;
}
/* -------------------------------------------------------------------------- */

inline UInt _parse(DOFType &dt, std::stringstream &line, UInt = 0) {
  std::string name;
  UInt nb = Parser::strNext(name, line);

  if (name == "dt_master")
    dt = dt_master;
  else if (name == "dt_slave")
    dt = dt_slave;
  else if (name == "dt_pbc_master")
    dt = dt_pbc_master;
  else if (name == "dt_pbc_slave")
    dt = dt_pbc_slave;
  else if (name == "dt_all")
    dt = dt_all;

  else
    LM_FATAL("unknown DOFType " << name);

  return nb;
}
/* -------------------------------------------------------------------------- */

// inline UInt _parse(char &val, std::stringstream &line) {
//   std::string val_str;
//   UInt nb = Parser::strNext(val_str, line);
//   if (val_str.size() != 1) {
//     LM_THROW("takes only one character");
//   }
//   val = val_str[0];
//   return nb;
// }

/* -------------------------------------------------------------------------- */

template <typename T>
inline UInt Parser::parse(T &var, std::stringstream &line, UInt n_entries) {
  return _parse(var, line, n_entries);
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
