/**
 * @file   lm_parser.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 02 16:25:38 2014
 *
 * @brief  This is the central parser for LM
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

/* -------------------------------------------------------------------------- */
#ifndef __LIBMULTISCALE_LM_PARSER_HH__
#define __LIBMULTISCALE_LM_PARSER_HH__
/* -------------------------------------------------------------------------- */
#include "algebraic_parser.hh"
#include "parse_result.hh"
#include "quantity.hh"
#include "units.hh"
#include "units_converter.hh"
#include <fstream>
#include <list>
#include <vector>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
// backward declaration
class Parsable;
/* -------------------------------------------------------------------------- */

struct UnableToParseWord : public LibMultiScaleException {

  UnableToParseWord() throw()
      : LibMultiScaleException("unable to parse word"){};
};

/* -------------------------------------------------------------------------- */

class Parser {
  /* ------------------------------------------------------------------------ */
  /* Typedefs                                                                 */
  /* ------------------------------------------------------------------------ */

  friend class Parsable;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  //! generic parse function of any quantity
  template <typename T>
  static UInt parse(T &val, std::stringstream &line, UInt n_entries = 0);
  //! parse a configuration file
  static void parseConfigFile(const std::string &file,
                              const std::string &section_name,
                              const std::string &section_id, Parsable &obj);
  //! substitute simple/environment variables in string
  static void substituteVariables(std::string &str);
  //! parse for generic keywords
  static UInt parseGenericKeywords(std::string &linebuffer, Parsable &obj);
  //! ask to free memory allocated
  static void freeMemory();
  //! ask to unstack words declared as parsed by the ParseResult argument
  static void shiftLine(const ParseResult &parsed, std::stringstream &line);
  //! return the current file being read
  static std::string getCurrentConfigFile();
  //! return the current line
  static UInt getCurrentLine();
  //! return a string with the parser state ready to print
  static std::string getParserState();
  //! rewind to a precise line in the current file
  static void rewindToLine(UInt line);
  //! return map of algebraic variables
  static std::map<std::string, double> &getAlgebraicVariables();

  //! read a line in a file even if multiline
  static void getLine(std::ifstream &file, std::string &line,
                      UInt &current_line);
  //! read coming word
  static UInt strNext(std::string &str, std::stringstream &line);

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */
public:
  //! ostream to where to write parser logs
  static std::ofstream fout;
  //! units converter for the LM blocks in the parser
  static UnitsConverter units_converter;
  //! math parser object that allow using math expressions in the config files
  static AlgebraicExpressionParser math_parser;

private:
  friend class BoundaryType;

  //! stack of units system for the sections
  static std::vector<UnitSystem> section_units;
  //! stack of opened file for include system
  static std::vector<std::string> opened_files;
  //! stack of opened file current lines
  static std::vector<UInt> current_line;
  //! stack of opened file stream
  static std::vector<std::ifstream *> opened_ifstreams;

  //! stack of opened for sections
  static std::vector<UInt> for_starting_line;
  //! stack of items to iterate on
  static std::vector<std::list<std::string>> for_items;
  //! stack of variable to iterate in for loops
  static std::vector<std::string> for_var;
};

__END_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
#include "lm_parser_inline_impl.hh"
/* -------------------------------------------------------------------------- */

#endif /* __LIBMULTISCALE_LM_PARSER_HH__ */
