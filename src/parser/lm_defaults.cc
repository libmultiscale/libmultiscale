#include "lm_defaults.hh"
#include "compute_impulse.hh"
#include "cube_surface.hh"
#include "lm_type_caster.hh"
__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
#define PYOBJ std::shared_ptr<pybind11::object>
#define ARG(...) __VA_ARGS__

#define DECLARE_ASSIGN(T)                                                      \
  template <>                                                                  \
  std::enable_if_t<is_pybind_type<PYOBJ>> assign_value<T, PYOBJ>(              \
      T & to_set, PYOBJ && val) {                                              \
    to_set = pybind11::cast<T>(*val);                                          \
  }                                                                            \
  template <>                                                                  \
  std::enable_if_t<is_pybind_type<PYOBJ &>> assign_value<T, PYOBJ &>(          \
      T & to_set, PYOBJ & val) {                                               \
    to_set = pybind11::cast<T>(*val);                                          \
  }

DECLARE_ASSIGN(ARG(std::vector<FieldType>))

#define DECLARE_ASSIGN_PHYSICAL_QUANTITY(quant)                                \
  DECLARE_ASSIGN(ARG(PhysicalScalar<quant, Real>))                             \
  DECLARE_ASSIGN(ARG(Quantity<quant, 1u, Real>))                               \
  DECLARE_ASSIGN(ARG(Quantity<quant, 2u, Real>))                               \
  DECLARE_ASSIGN(ARG(Quantity<quant, 3u, Real>))                               \
  DECLARE_ASSIGN(ARG(Quantity<quant, 4u, Real>))                               \
  DECLARE_ASSIGN(ARG(Quantity<quant, 5u, Real>))                               \
  DECLARE_ASSIGN(ARG(Quantity<quant, 6u, Real>))                               \
  DECLARE_ASSIGN(ARG(Quantity<quant, 7u, Real>))                               \
  DECLARE_ASSIGN(ARG(Quantity<quant, 8u, Real>))                               \
  DECLARE_ASSIGN(ARG(Quantity<quant, 9u, Real>))                               \
  DECLARE_ASSIGN(ARG(Quantity<quant, 10u, Real>))

DECLARE_ASSIGN_PHYSICAL_QUANTITY(Length)
DECLARE_ASSIGN_PHYSICAL_QUANTITY(Mass)
DECLARE_ASSIGN_PHYSICAL_QUANTITY(Energy)
DECLARE_ASSIGN_PHYSICAL_QUANTITY(Time)
DECLARE_ASSIGN_PHYSICAL_QUANTITY(MassDensity)
DECLARE_ASSIGN_PHYSICAL_QUANTITY(Force)
DECLARE_ASSIGN_PHYSICAL_QUANTITY(Pressure)
DECLARE_ASSIGN_PHYSICAL_QUANTITY(Temperature)

DECLARE_ASSIGN(ARG(Vector<1u, UInt>))
DECLARE_ASSIGN(ARG(Vector<2u, UInt>))
DECLARE_ASSIGN(ARG(Vector<3u, UInt>))

DECLARE_ASSIGN(ARG(Vector<1u, bool>))
DECLARE_ASSIGN(ARG(Vector<2u, bool>))
DECLARE_ASSIGN(ARG(Vector<3u, bool>))

DECLARE_ASSIGN(ARG(Vector<1u>))
DECLARE_ASSIGN(ARG(Vector<2u>))
DECLARE_ASSIGN(ARG(Vector<3u>))

DECLARE_ASSIGN(CubeSurface::Face)
DECLARE_ASSIGN(Operator)
DECLARE_ASSIGN(DOFType)

DECLARE_ASSIGN(int)
DECLARE_ASSIGN(char)
DECLARE_ASSIGN(long)
DECLARE_ASSIGN(bool)
DECLARE_ASSIGN(UInt)
DECLARE_ASSIGN(std::string)
DECLARE_ASSIGN(Real)
DECLARE_ASSIGN(std::vector<int>)
DECLARE_ASSIGN(std::vector<UInt>)
DECLARE_ASSIGN(std::vector<std::string>)
DECLARE_ASSIGN(Impulse::ImpulseType)
DECLARE_ASSIGN(IntegrationSchemeMask)
DECLARE_ASSIGN(UnitSystem)
DECLARE_ASSIGN(FieldType)

#undef DECLARE_ASSIGN_PHYSICAL_QUANTITY
#undef DECLARE_ASSIGN

#define DECLARE_ASSIGN(T)                                                      \
  template <>                                                                  \
  std::enable_if_t<is_pybind_type<PYOBJ &>> assign_value<T, PYOBJ &>(          \
      T & to_set, PYOBJ & val)

DECLARE_ASSIGN(ARG(std::map<std::string, PYOBJ>)) {
  try {
    py::dict dict = py::cast<py::dict>(*val);
    for (auto &&key_val : dict) {
      auto key = pybind11::cast<std::string>(key_val.first);
      auto _val = pybind11::cast<pybind11::object>(key_val.second);
      to_set[key] = std::make_shared<pybind11::object>(_val);
    }
  } catch (...) {
    LM_FATAL("object not a dictionary");
  }
}

DECLARE_ASSIGN(Component) {
  std::string input = pybind11::cast<std::string>(*val);
  std::stringstream sstr(input);
  _parse(to_set, sstr, 1);
}

#undef DECLARE_ASSIGN

#define DECLARE_ASSIGN_VEC(T, Dim)                                             \
  template <>                                                                  \
  std::enable_if_t<is_pybind_type<PYOBJ &>> assign_value<T[Dim], PYOBJ &>(     \
      T(&to_set)[Dim], PYOBJ & val) {                                          \
    pybind11::tuple v = pybind11::cast<pybind11::tuple>(*val);                 \
    for (UInt i = 0; i < Dim; ++i) {                                           \
      to_set[i] = pybind11::cast<T>(v[i]);                                     \
    }                                                                          \
  }

#define DECLARE_ASSIGN(T)                                                      \
  DECLARE_ASSIGN_VEC(T, 1)                                                     \
  DECLARE_ASSIGN_VEC(T, 2)                                                     \
  DECLARE_ASSIGN_VEC(T, 3)                                                     \
  DECLARE_ASSIGN_VEC(T, 4)                                                     \
  DECLARE_ASSIGN_VEC(T, 5)                                                     \
  DECLARE_ASSIGN_VEC(T, 6)                                                     \
  DECLARE_ASSIGN_VEC(T, 7)                                                     \
  DECLARE_ASSIGN_VEC(T, 8)                                                     \
  DECLARE_ASSIGN_VEC(T, 9)                                                     \
  DECLARE_ASSIGN_VEC(T, 10)                                                    \
  DECLARE_ASSIGN_VEC(T, 11)                                                    \
  DECLARE_ASSIGN_VEC(T, 12)                                                    \
  DECLARE_ASSIGN_VEC(T, 13)                                                    \
  DECLARE_ASSIGN_VEC(T, 14)                                                    \
  DECLARE_ASSIGN_VEC(T, 15)

DECLARE_ASSIGN(Real)
DECLARE_ASSIGN(bool)
DECLARE_ASSIGN(UInt)
DECLARE_ASSIGN(int)
DECLARE_ASSIGN(std::string)

#undef DECLARE_ASSIGN
#undef DECLARE_ASSIGN_VEC

UInt _parse(std::map<std::string, PYOBJ>, std::stringstream &, UInt) {
  LM_TOIMPLEMENT;
}

#undef PYOBJ

__END_LIBMULTISCALE__
