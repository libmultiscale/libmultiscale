/**
 * @file   lm_parsable_inline_impl.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Thu Feb 06 11:42:03 2014
 *
 * @brief  Common mother for parsable objects
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_PARSABLE_INLINE_IMPL_HH__
#define __LIBMULTISCALE_PARSABLE_INLINE_IMPL_HH__
/* -------------------------------------------------------------------------- */
#include "lm_parameter.hh"
#include "lm_parsable.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

template <typename T>
void Parsable::parseKeyword(const std::string &keyword, T &var,
                            default_val<T> def) {
  this->declareParam<T>(keyword, var, def);
}

/* -------------------------------------------------------------------------- */

template <typename T, typename V>
void Parsable::parseKeyword(const std::string &keyword, T &var, V &&def) {
  this->parseKeyword(keyword, var, default_val<T>(std::forward<V>(def)));
}

/* -------------------------------------------------------------------------- */

template <typename T, typename V>
void Parsable::parseVectorKeyword(const std::string &keyword, UInt n, T &var,
                                  V &&def) {
  this->parseVectorKeyword(keyword, n, var,
                           default_val<T>(std::forward<V>(def)));
}

/* -------------------------------------------------------------------------- */

template <typename T>
void Parsable::parseVectorKeyword(const std::string &keyword, UInt n, T &var,
                                  default_val<T> def) {
  this->declareParam<T>(keyword, var, def, n);
}

/* -------------------------------------------------------------------------- */

template <typename T>
void Parsable::declareParam(const std::string &keyword, T &var,
                            default_val<T> def, UInt n_entries) {
  if (params.count(keyword) > 0)
    LM_FATAL("keyword " << keyword << " was already declared");

  ParameterTyped<T> *param =
      new ParameterTyped<T>(keyword, var, def, n_entries);
  params[keyword] = std::shared_ptr<Parameter>(param);
}

/* -------------------------------------------------------------------------- */

template <typename T> T Parsable::getParam(const std::string &keyword) {

  checkKeyword(keyword);

  auto p = params[keyword];
  return p->getParam<T>(keyword);
}

/* -------------------------------------------------------------------------- */

template <typename T>
void Parsable::changeDefault(const std::string &keyword, const T &val) {

  std::map<std::string, std::shared_ptr<Parameter>>::iterator it =
      params.find(keyword);
  std::map<std::string, std::shared_ptr<Parameter>>::iterator end =
      params.end();

  if (it == end)
    LM_THROW("not registered " << keyword);

  Parameter *p = it->second.get();
  p->changeDefault(keyword, val);
}

/* -------------------------------------------------------------------------- */

template <typename T>
void Parsable::setParam(const std::string &keyword, const T &value) {

  if (!are_parameters_declared) {
    declareParams();
    generateListKeywords();
    are_parameters_declared = true;
  }

  checkKeyword(keyword);

  for (UInt i = 0; i < forward_parsable.size(); ++i) {
    if (!forward_parsable[i]->doAcceptKeyword(keyword))
      continue;
    forward_parsable[i]->setParam(keyword, value);
  }

  if (!are_parameters_declared) {
    declareParams();
    are_parameters_declared = true;
  }

  if (params.count(keyword)) {
    params[keyword]->setParam(keyword, value);
  }
  this->changeRelease();
}

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_PARSABLE_INLINE_IMPL_HH__ */
