/**
 * @file   lm_defaults.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Oct 29 18:26:07 2013
 *
 * @brief  Handles the defaults for the parsing system
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_LM_DEFAULTS_HH__
#define __LIBMULTISCALE_LM_DEFAULTS_HH__
/* -------------------------------------------------------------------------- */
#include "quantity.hh"
#include <map>
#include <memory>
#include <type_traits>
#include <vector>
/* -------------------------------------------------------------------------- */
// backward declaration

namespace pybind11 {
class object;
class dict;
} // namespace pybind11

namespace py = pybind11;

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
template <typename V>
constexpr bool is_pybind_type =
    std::is_same<std::decay_t<V>, std::shared_ptr<pybind11::object>>::value;

template <typename T, typename V>
inline std::enable_if_t<not is_pybind_type<V>> assign_value(T &to_set, V &&val);

template <typename T, typename V>
std::enable_if_t<is_pybind_type<V>> assign_value(T &to_set, V &&val);

template <typename T, typename V> struct Assign {
  static void _assign_value(T &to_set, V &&val) { to_set = val; }
};

template <typename T, UInt Dim, typename V> struct Assign<T[Dim], V> {

  static void _assign_value(T (&to_set)[Dim], V &&val) {
    for (UInt i = 0; i < std::extent<T>::value; ++i)
      assign_value(to_set[i], val[i]);
  }
};

template <typename T, typename V>
inline std::enable_if_t<not is_pybind_type<V>> assign_value(T &to_set,
                                                            V &&val) {
  Assign<T, V>::_assign_value(to_set, std::forward<V>(val));
}

template <typename T, typename V>
std::enable_if_t<is_pybind_type<V>> assign_value(T &to_set, V &&val);

/* --------------------------------------------------------------- */

template <typename T> class default_val {

public:
  default_val() { is_set = false; };

  template <typename V>
  using not_another_default =
      std::enable_if<not std::is_same<std::decay_t<V>, default_val<T>>::value>;

  template <typename V>
  default_val(V &&v, typename not_another_default<V>::type * = 0) {
    is_set = true;
    assign_value(val, std::forward<V>(v));
  }

  template <typename V>
  default_val(const V &v, typename not_another_default<V>::type * = 0) {
    is_set = true;
    assign_value(val, v);
  }

  bool setDefault(T &to_set) const {
    if (!this->is_set)
      return false;
    assign_value(to_set, val);
    return true;
  }

  const T &getVal() const { return val; };

private:
  bool is_set;
  T val;
};

/* ----------------------------------------------------------------- */
class Component;
template <> class default_val<Component> {

public:
  //   default_val() { is_set = false; };

  //   template <typename V>
  //   using not_another_default =
  //       std::enable_if<not std::is_same<std::decay_t<V>,
  //       default_val<T>>::value>;

  //   template <typename V>
  //   default_val(V &&v, typename not_another_default<V>::type * = 0) {
  //     is_set = true;
  //     assign_value(val, std::forward<V>(v));
  //   }

  //   template <typename V>
  //   default_val(const V &v, typename not_another_default<V>::type * = 0) {
  //     is_set = true;
  //     assign_value(val, v);
  //   }

  bool setDefault(Component &) const { return true; }

  //   const T &getVal() const { return val; };

private:
  //  bool is_set;
};

/* ----------------------------------------------------------------- */

template <typename T> struct type_converter_helper { using type = T; };

template <> struct type_converter_helper<char *> { using type = std::string; };

template <> struct type_converter_helper<const char *> {
  using type = std::string;
};

template <typename T> auto VEC_DEFAULTS(std::initializer_list<T> l) {
  return Vector<l.size(), T>(l);
}

template <typename T> auto VEC_DEFAULTS(T arg) {
  using type = typename type_converter_helper<T>::type;
  std::vector<type> res;
  res.push_back(arg);
  return res;
}

template <typename T, typename... Args>
auto VEC_DEFAULTS(T arg1, Args... args) {

  using type = typename type_converter_helper<T>::type;
  std::vector<type> res;
  res.push_back(arg1);
  auto res2 = VEC_DEFAULTS(args...);
  res.insert(res.end(), res2.begin(), res2.end());
  return res;
}

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_LM_DEFAULTS_HH__ */
