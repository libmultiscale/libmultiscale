/**
 * @file   algebraic_parser.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Mar 13 16:53:59 2013
 *
 * @brief  The algebraic expression parser for LM config files
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_ALGEBRAIC_PARSER_HH__
#define __LIBMULTISCALE_ALGEBRAIC_PARSER_HH__
/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
#include <map>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

template <typename T> struct AlgebraicParser;

struct AlgebraicExpressionParser {
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  AlgebraicExpressionParser();
  ~AlgebraicExpressionParser();

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  //! parse an expression passed as a string
  bool parse(const std::string &str, double &result);
  //! access the value of a given variable
  Real &operator[](const std::string &var_name);
  //! erase a variable from the pool of variables
  void eraseVariable(const std::string &var_name);
  //! test if a variable is existing
  bool doesExist(const std::string &var_name);
  //! return map of variables
  std::map<std::string, double> &getVariables();

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

  AlgebraicParser<std::string::const_iterator> *my_parser;
};

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_ALGEBRAIC_PARSER_HH__ */
