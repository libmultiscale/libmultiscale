/**
 * @file   lm_parameter.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Nov 25 17:17:19 2013
 *
 * @brief  Encapsulate a parameter for the parsing system
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_LM_PARAMETERS_HH__
#define __LIBMULTISCALE_LM_PARAMETERS_HH__
/* -------------------------------------------------------------------------- */
#include "lm_defaults.hh"
#include "lm_parser.hh"
#include <memory>
#include <typeinfo>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
template <typename T> class ParameterTyped;
/* -------------------------------------------------------------------------- */

class Parameter {

public:
  Parameter(const std::string &name) : name(name), is_set(false){};

  virtual ~Parameter(){};

  virtual std::shared_ptr<pybind11::object>
  getParameterAsPyObject(const std::string &variable) = 0;

  virtual void
  setParameterFromPyObject(const std::string &variable,
                           std::shared_ptr<pybind11::object> val) = 0;

  template <typename T>
  std::enable_if_t<not std::is_same<T, pybind11::object>::value,
                   ParameterTyped<T> &>
  getParameterTyped() {

    try {
      ParameterTyped<T> &tmp = dynamic_cast<ParameterTyped<T> &>(*this);
      return tmp;
    } catch (...) {
      LM_FATAL("The parameter named "
               << name << " is of type " << typeid(*this).name()
               << " and not of type " << typeid(T).name());
    }
  }

  virtual ParseResult setParam(const std::string &variable,
                               std::stringstream &line) = 0;

  template <typename T>
  std::enable_if_t<not std::is_same<T, pybind11::object>::value>
  setParam(const std::string &variable, const T &value);

  template <typename T>
  std::enable_if_t<std::is_same<T, pybind11::object>::value>
  setParam(const std::string &variable, const T &value);

  template <typename T>
  std::enable_if_t<not std::is_same<T, pybind11::object>::value, T>
  getParam(const std::string &variable);

  template <typename T>
  std::enable_if_t<std::is_same<T, pybind11::object>::value, T>
  getParam(const std::string &variable);

  template <typename T>
  void changeDefault(const std::string &variable, const T &value);

  bool isSet() { return is_set; };
  void makeItAsSet() { is_set = true; };

protected:
  std::string name;

  bool is_set;
};

/* -------------------------------------------------------------------------- */

template <typename T>
std::enable_if_t<not std::is_same<T, pybind11::object>::value>
Parameter::setParam(const std::string &variable, const T &value) {

  this->getParameterTyped<T>().setParam(variable, value);
}
/* -------------------------------------------------------------------------- */

template <typename T>
std::enable_if_t<std::is_same<T, pybind11::object>::value>
Parameter::setParam(const std::string &variable, const T &value) {
  return this->setParameterFromPyObject(
      variable, std::make_shared<pybind11::object>(value));
}
/* -------------------------------------------------------------------------- */

template <typename T>
std::enable_if_t<not std::is_same<T, pybind11::object>::value, T>
Parameter::getParam(const std::string &variable) {
  T value;
  this->getParameterTyped<T>().getParam(variable, value);
  return value;
}
/* -------------------------------------------------------------------------- */

template <typename T>
std::enable_if_t<std::is_same<T, pybind11::object>::value, T>
Parameter::getParam(const std::string &variable) {

  return *this->getParameterAsPyObject(variable);
}

/* -------------------------------------------------------------------------- */

template <typename T>
void Parameter::changeDefault(const std::string &variable, const T &value) {
  this->getParameterTyped<T>().changeDefault(variable, value);
}

/* -------------------------------------------------------------------------- */

// template <typename T>
// std::shared_ptr<pybind11::object> my_cast(T &values);

using pyptr = std::shared_ptr<pybind11::object>;

template <typename T> struct MyCast { static pyptr cast(T &values); };

template <typename T> pyptr my_cast(T &values) {
  return MyCast<T>::cast(values);
}

/* -------------------------------------------------------------------------- */

template <typename T> class ParameterTyped : public Parameter {

public:
  ParameterTyped(const std::string n, T &v, const default_val<T> def,
                 UInt n_entries = 1)
      : Parameter(n), values(v), n_entries(n_entries) {
    this->is_set = def.setDefault(this->values);
    DUMPFILE(Parser::fout, "declare " << n << " 1 x " << typeid(T).name());
  };

  virtual ~ParameterTyped(){};

  virtual void setParam(const std::string &, const T &value) {
    assign_value(this->values, value);
    this->is_set = true;
  }

  virtual void getParam(const std::string &, T &value) {
    assign_value(value, this->values);
  }

  std::shared_ptr<pybind11::object>
  getParameterAsPyObject(const std::string &) override {
    return my_cast(this->values);
  };

  void
  setParameterFromPyObject(const std::string &,
                           std::shared_ptr<pybind11::object> val) override {
    assign_value(this->values, val);
    this->is_set = true;
  };

  virtual void changeDefault(const std::string &, const T &value) {
    assign_value(this->values, value);
    this->is_set = true;
  }

  ParseResult setParam(const std::string &variable [[gnu::unused]],
                       std::stringstream &line) {
    LM_ASSERT(variable == name, "internal keyword problem");
    UInt word_count = 0;
    word_count += Parser::parse(values, line, n_entries);
    this->is_set = true;
    return ParseResult(true, word_count);
  };

  virtual Parameter *createCopy() { LM_TOIMPLEMENT; };

protected:
  T &values;

  UInt n_entries;
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_LM_PARAMETERS_HH__ */
