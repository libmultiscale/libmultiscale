/**
 * @file   lm_parsable.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Thu Jul 24 14:21:58 2014
 *
 * @brief  Common mother for parsable objects
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "lm_parsable.hh"
#include "lm_common.hh"
#include "lm_parameter.hh"
#include "lm_parsable_inline_impl.hh"
#include "stimulation_impulse.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

Parsable::Parsable() : are_parameters_declared(false) {}

/* -------------------------------------------------------------------------- */

Parsable::~Parsable() {}

/* -------------------------------------------------------------------------- */

void Parsable::parseTag(const std::string &keyword, bool &var,
                        default_val<bool> def) {
  this->parseKeyword(keyword, var, def);
}

/* -------------------------------------------------------------------------- */

template <>
void Parsable::changeDefault<IntegrationSchemeStage>(
    const std::string &keyword, const IntegrationSchemeStage &val) {

  this->changeDefault(keyword, IntegrationSchemeMask(val));
}

/* -------------------------------------------------------------------------- */

void Parsable::makeItOptional(const std::string &keyword) {

  std::map<std::string, std::shared_ptr<Parameter>>::iterator it =
      params.find(keyword);
  std::map<std::string, std::shared_ptr<Parameter>>::iterator end =
      params.end();

  if (it == end)
    LM_THROW("not registered " << keyword);

  Parameter *p = it->second.get();
  p->makeItAsSet();
}

/* -------------------------------------------------------------------------- */

ParseResult Parsable::parseLine(const std::string &buffer) {

  if (!are_parameters_declared) {
    declareParams();
    generateListKeywords();
    are_parameters_declared = true;
  }

  std::stringstream line(buffer);
  std::string mot;
  ParseResult count(true, 0);
  DUMPFILE(Parser::fout, "Parsing line " << buffer);
  while (line.good()) {
    try {
      Parser::parse(mot, line);
    } catch (LibMultiScaleException &e) {
      break;
    }

    if (mot == "BLOCK_PARAMS") {
      ParseResult parsed(true, 0);
      std::string section_name;
      Parser::parse(section_name, line);
      ++parsed;
      std::string filename;
      try {
        Parser::parse(filename, line);
        Parser::parseConfigFile(filename, section_name, "", (*this));
        ++parsed;
      } catch (LibMultiScaleException &e) {
        Parser::parseConfigFile(Parser::getCurrentConfigFile(), section_name,
                                "", (*this));
      }
      count += parsed;
      ++count;
      return count;
    } else {
      ParseResult parsed;
      try {
        //        const std::string & ls = line.str(); // just for debugging
        parsed = setParam(mot, line);
      } catch (LibMultiScaleException &e) {
        LM_FATAL_RE(e, std::endl
                           << Parser::getParserState() << "parameter " << mot
                           << " was not parsed !! " << std::endl
                           << "check config file for a syntax problem: "
                           << "\"" << buffer << "\"");
      }
      if (!parsed)
        LM_FATAL("parameter "
                 << mot << " was not parsed !! " << std::endl
                 << "check config file for a syntax problem for line\n\t"
                 << Parser::getParserState() << std::endl
                 << "\t\t\"" << buffer << "\"" << std::endl);
      // count the pop parameters
      count += parsed;
      // count the keyword which have been removed
      ++count;
    }
  }
  return count;
}
/* -------------------------------------------------------------------------- */

void Parsable::checkAllKeywordsAreParsed() {

  if (!are_parameters_declared) {
    declareParams();
  }

  for (UInt i = 0; i < forward_parsable.size(); ++i)
    forward_parsable[i]->checkAllKeywordsAreParsed();

  std::vector<std::string> failed_keywords;
  for (auto &&it : params) {
    if (it.second->isSet() == false) {
      failed_keywords.push_back(it.first);
    }
  }
  std::string mystr;
  for (UInt i = 0; i < failed_keywords.size(); ++i) {
    mystr += "\t >> \t \"" + failed_keywords[i] + "\"\n";
  }
  std::string verb1 = "were";
  std::string verb2 = "have";

  if (failed_keywords.size() == 1) {
    verb1 = "was";
    verb2 = "has";
  }

  if (failed_keywords.size() > 0)
    LM_THROW("parameters " << std::endl
                           << std::endl
                           << mystr << std::endl
                           << " " << verb1 << " not parsed and"
                           << " " << verb2 << " no default value" << std::endl);
}

/* -------------------------------------------------------------------------- */
void Parsable::addSubParsableObject(Parsable &obj) {
  this->addSubParsableObject(&obj);
}

void Parsable::addSubParsableObject(Parsable *ptr) {
  forward_parsable.push_back(ptr);
  if (!ptr->are_parameters_declared) {
    ptr->declareParams();
    ptr->generateListKeywords();
    ptr->are_parameters_declared = true;
  }
}

/* -------------------------------------------------------------------------- */
ParseResult Parsable::setParam(const std::string &keyword,
                               std::stringstream &line) {

  checkKeyword(keyword);

  ParseResult parsed;

  std::string current_buffer;
  UInt pos = line.tellg();
  if (pos < line.str().length())
    current_buffer = line.str().substr(pos);

  for (UInt i = 0; i < forward_parsable.size(); ++i) {
    if (!forward_parsable[i]->doAcceptKeyword(keyword))
      continue;
    std::stringstream sub_line(current_buffer);
    ParseResult parsed_forward =
        forward_parsable[i]->setParam(keyword, sub_line);
    parsed.max(parsed_forward);
  }

  if (params.count(keyword)) {
    std::stringstream sub_line(current_buffer);
    Parameter *p = params[keyword].get();
    ParseResult parsed_main = p->setParam(keyword, sub_line);
    parsed.max(parsed_main);
  }

  Parser::shiftLine(parsed, line);
  return parsed;
}

/* -------------------------------------------------------------------------- */
std::set<std::string> Parsable::getPossibleKeywords() {
  if (!are_parameters_declared) {
    declareParams();
    generateListKeywords();
    are_parameters_declared = true;
  }
  return list_keywords;
}
/* -------------------------------------------------------------------------- */
void Parsable::checkKeyword(const std::string &keyword) {
  if (!doAcceptKeyword(keyword)) {
    std::stringstream message;
    message << "keyword " << keyword << " is not registered" << std::endl;
    message << " the registered keywords are:" << std::endl;
    message << listKeywordsToStr();
    message << std::endl;
    LM_THROW(message.str());
  }
}
/* -------------------------------------------------------------------------- */
bool Parsable::doAcceptKeyword(const std::string &keyword) {
  return (list_keywords.count(keyword));
}
/* -------------------------------------------------------------------------- */

std::string Parsable::listKeywordsToStr() {
  std::stringstream list_keys;
  std::set<std::string>::iterator it = list_keywords.begin();
  std::set<std::string>::iterator end = list_keywords.end();

  while (it != end) {
    list_keys << " " << (*it);
    ++it;
  }
  return list_keys.str();
}
/* -------------------------------------------------------------------------- */

void Parsable::generateListKeywords() {

  for (auto &&[k, v] : params) {
    list_keywords.insert(k);
  }

  for (UInt i = 0; i < forward_parsable.size(); ++i) {
    for (auto &&[k, v] : forward_parsable[i]->params) {
      list_keywords.insert(k);
    }
    forward_parsable[i]->generateListKeywords();
  }
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
