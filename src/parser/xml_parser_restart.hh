/**
 * @file   xml_parser_restart.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Sep 03 14:49:43 2014
 *
 * @brief  XML parser for restart files
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef XML_PARSER_RESTART_H
#define XML_PARSER_RESTART_H
/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
#include "xml_parser.hh"
#include <map>
#include <valarray>
#include <vector>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
static const Real epsilon = .000001;
static const Real epsilon2 = epsilon * epsilon;
/* -------------------------------------------------------------------------- */

template <UInt Dim> class Triplet {
public:
  Triplet(Real *X) : X(X){};

  template <typename T> Triplet(const T &X) : X(X) {}

  Triplet(){};

  Real &operator()(UInt i) {
    LM_ASSERT(i < Dim, "out of range access " << i);
    return this->X[i];
  }

  Real &operator[](UInt i) { return (*this)(i); }

private:
  Vector<Dim> X;
};

/* -------------------------------------------------------------------------- */

class CompareTriplet {
public:
  template <UInt Dim> bool operator()(Triplet<Dim> t1, Triplet<Dim> t2) const {
    Real d = 0.;
    for (UInt i = 0; i < Dim; ++i) {
      Real dist = t1(i) - t2(i);
      d += dist * dist;
    }

    if (d < epsilon2)
      return false;

    for (UInt i = 0; i < Dim; ++i) {
      if (fabs(t1(i) - t2(i)) > epsilon) {
        if (t1(i) < t2(i))
          return true;
        else
          return false;
      }
    }
    return false;
  }
};

/* -------------------------------------------------------------------------- */

template <UInt Dim> class XMLRestartParser : public XMLParser {

public:
  XMLRestartParser() {
    nbDofs = 0;
    cpt = 0;
  };

  ~XMLRestartParser() { cleanMemory(); };

  void cleanMemory() {
    map_fields.clear();
    mapP0.clear();
  }

  virtual UInt parseFile(const std::string &name) {

    if (UInt ret = XMLParser::parseFile(name)) {
      return ret;
    }

    return 0;
  }

  bool isDofRegistered(VectorView<Dim> &X);
  template <FieldType field_type, typename V1, typename V2>
  void assignField(const V1 &X, V2 &U);
  template <typename V> void assignInitialPosition(UInt index, V &X);

  UInt getReadDofsNumber() { return nbDofs; };

protected:
  void charHandler(InputStream &str) override;
  void startElement(const std::string &name,
                    const std::vector<std::string> &atts) override;
  void endElement(const std::string &name) override;

private:
  template <FieldType field_type> void manageField(InputStream &is);
  void manageP0(InputStream &s);

  //! temporary triplet (for incomlete lines)
  std::vector<Real> triplet;

  UInt nbDofs;
  FieldType current_field;

  UInt cpt;

  using ReloadedField = std::map<Triplet<Dim>, Triplet<Dim>, CompareTriplet>;

  std::map<FieldType, ReloadedField> map_fields;
  std::vector<Triplet<Dim>> mapP0;
};

/* -------------------------------------------------------------------------- */

template <UInt Dim>
inline std::ostream &operator<<(std::ostream &os, Triplet<Dim> &t) {
  os << "Triplet :";
  for (UInt i = 0; i < Dim; ++i)
    os << " " << t(i);

  os << std::endl;
  return os;
}

/* -------------------------------------------------------------------------- */

template <UInt Dim>
inline bool XMLRestartParser<Dim>::isDofRegistered(VectorView<Dim> &X) {
  Triplet<Dim> a(X);
  bool test = (map_fields[_displacement].count(a) != 0);

  return test;
}

/* -------------------------------------------------------------------------- */

template <UInt Dim>
template <typename V>
inline void XMLRestartParser<Dim>::assignInitialPosition(UInt index, V &X) {
  Triplet<Dim> &b = mapP0[index];
  for (UInt i = 0; i < Dim; ++i)
    X[i] = b(i);
}
/* -------------------------------------------------------------------------- */

template <UInt Dim>
template <FieldType field_type, typename V1, typename V2>
inline void XMLRestartParser<Dim>::assignField(const V1 &X, V2 &U) {

  Triplet<Dim> a(X);
  auto &b = map_fields[field_type][a];
  for (UInt i = 0; i < Dim; ++i)
    U[i] = b(i);
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif
