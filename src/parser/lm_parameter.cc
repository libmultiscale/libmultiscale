/* -------------------------------------------------------------------------- */
#include "compute_impulse.hh"
#include "cube_surface.hh"
#include "lm_type_caster.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

#define ARG(...) __VA_ARGS__

template <typename T> pyptr MyCast<T>::cast(T &values) {
  return std::make_shared<pybind11::object>(pybind11::cast(values));
}

template <>
pyptr MyCast<std::map<std::string, pyptr>>::cast(
    std::map<std::string, pyptr> &) {
  LM_FATAL("cannot get this parameter in read");
}

#define CAST(T) template struct MyCast<T>

#define CAST_PHYSICAL_QUANTITY(quant)                                          \
  CAST(ARG(PhysicalScalar<quant, Real>));                                      \
  CAST(ARG(Quantity<quant, 1u, Real>));                                        \
  CAST(ARG(Quantity<quant, 2u, Real>));                                        \
  CAST(ARG(Quantity<quant, 3u, Real>));                                        \
  CAST(ARG(Quantity<quant, 4u, Real>));                                        \
  CAST(ARG(Quantity<quant, 5u, Real>));                                        \
  CAST(ARG(Quantity<quant, 6u, Real>));                                        \
  CAST(ARG(Quantity<quant, 7u, Real>));                                        \
  CAST(ARG(Quantity<quant, 8u, Real>));                                        \
  CAST(ARG(Quantity<quant, 9u, Real>));                                        \
  CAST(ARG(Quantity<quant, 10u, Real>))

CAST(UInt);
CAST(Real);
CAST(bool);
CAST(IntegrationSchemeMask);
CAST(ARG(std::map<std::string, std::shared_ptr<pybind11::object>>));
CAST(ARG(std::map<std::string, std::string>));
CAST(CubeSurface::Face);
CAST(long);
CAST(int);
CAST(ARG(Vector<3, Real>));
CAST(ARG(Vector<2, Real>));
CAST(ARG(Vector<1, Real>));
CAST(ARG(Vector<3, bool>));
CAST(std::string);
CAST(std::vector<int>);
CAST(std::vector<Real>);
CAST(std::vector<UInt>);

CAST_PHYSICAL_QUANTITY(Length);
CAST_PHYSICAL_QUANTITY(Mass);
CAST_PHYSICAL_QUANTITY(Energy);
CAST_PHYSICAL_QUANTITY(Time);
CAST_PHYSICAL_QUANTITY(MassDensity);
CAST_PHYSICAL_QUANTITY(Force);
CAST_PHYSICAL_QUANTITY(Pressure);
CAST_PHYSICAL_QUANTITY(Temperature);

CAST(UnitSystem);
CAST(char);
CAST(UInt[3]);
CAST(int[3]);
CAST(double[3]);
CAST(double[9]);
CAST(double[4]);
CAST(double[13]);
CAST(bool[3]);
CAST(double[2]);
CAST(Operator);
CAST(Component);
CAST(int[6]);
CAST(std::vector<std::string>);
CAST(FieldType);
CAST(ARG(Vector<3u, UInt>));
CAST(ARG(Vector<2u, UInt>));
CAST(ARG(Vector<1u, UInt>));
CAST(std::vector<FieldType>);
CAST(std::string[3]);
CAST(DOFType);
CAST(std::string[2]);
CAST(Impulse::ImpulseType);

// CAST(int (&) [3]);
// CAST(double (&) [3]);
// CAST(bool (&) [3]);
// CAST(double (&) [2]);
// CAST(int (&) [6]);
// CAST(std::string (&) [3]);
// CAST(std::string (&) [2]);

#undef CAST

__END_LIBMULTISCALE__
