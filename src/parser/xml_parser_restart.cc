/**
 * @file   xml_parser_restart.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Aug 20 16:58:08 2014
 *
 * @brief  XML parser for restart files
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#include "xml_parser_restart.hh"
#include "lm_common.hh"
#include "trace_atom.hh"
#include <sstream>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

template <UInt Dim> void XMLRestartParser<Dim>::charHandler(InputStream &is) {

  switch (current_field) {
  case _position0:
    manageP0(is);
    break;
  case _displacement:
    manageField<_displacement>(is);
    break;
  case _velocity:
    manageField<_velocity>(is);
    break;
  case _acceleration:
    manageField<_acceleration>(is);
    break;
  default:
    break;
  }
}

/* -------------------------------------------------------------------------- */

template <UInt Dim>
void XMLRestartParser<Dim>::startElement(const std::string &name,
                                         const std::vector<std::string> &atts) {
  UInt j;
  cpt = 0;
  b64.reset();

  if ("SimulationData" == name) {
    for (j = 0; j < atts.size(); ++j) {
      if ("dim" == atts[j]) {
        UInt dim = std::stoi(atts[j + 1]);
        if (dim != Dim)
          LM_FATAL("reloading data of a different dimension");
        ++j;
      }
      if ("dataType" == atts[j]) {
        if ("TEXT" == atts[j + 1]) {
          current_type = TEXT;
        } else
          current_type = BINARY;
        ++j;
      }
      if ("nbDofs" == atts[j]) {
        nbDofs = std::stoi(atts[j + 1]);
        ++j;
      }
      if ("timestep" == atts[j]) {
        current_step = std::stoi(atts[j + 1]);
        DUMP("Changing current timestep from reload to " << current_step,
             DBG_WARNING);
        ++j;
      }
    }
  }
  if ("P0" == name) {
    current_field = _position0;
    DUMP("reading intial positions", DBG_INFO);
  }
  if ("U" == name) {
    current_field = _displacement;
    DUMP("reading displacements", DBG_INFO);
  }
  if ("V" == name) {
    current_field = _velocity;
    DUMP("reading velocities", DBG_INFO);
  }
  if ("A" == name) {
    current_field = _acceleration;
    DUMP("reading accelerations", DBG_INFO);
  }
}
/* -------------------------------------------------------------------------- */

template <UInt Dim>
void XMLRestartParser<Dim>::endElement(const std::string &name) {

  std::string message = name;

  switch (current_field) {
  case _position0:
    message += " : in vector P0s ";
    break;
  case _displacement:
    message += " : in vector Us ";
    break;
  case _velocity:
    message += " : in vector Vs ";
    break;
  case _acceleration:
    message += " : in vector As ";
    break;
  default:
    break;
  }
  current_field = ft_uninitialised;

  if (cpt != 0 && cpt != nbDofs) {
    for (auto &&p : mapP0) {
      std::cout << p << std::endl;
    }
    LM_FATAL(message << ": values number do not concord (read " << cpt
                     << " should be " << nbDofs << ")");
  }
}

/* -------------------------------------------------------------------------- */

template <UInt Dim> void XMLRestartParser<Dim>::manageP0(InputStream &is) {
  Triplet<Dim> C;
  while (cpt < nbDofs) {

    if (not is.canRead<Triplet<Dim>>())
      break;

    is >> C;

    mapP0.push_back(C);
    ++cpt;
  }
}

/* -------------------------------------------------------------------------- */

template <UInt Dim>
template <FieldType field_type>
void XMLRestartParser<Dim>::manageField(InputStream &is) {
  Triplet<Dim> u;
  auto &map_field = map_fields[field_type];
  while (cpt < nbDofs) {
    Triplet<Dim> index = mapP0[cpt];

    if (not is.canRead<Triplet<Dim>>())
      break;

    is >> u;

    map_field[index] = u;
    DUMP("map<" << field_type << "> size = " << map_field.size() << " : "
                << index << " " << u << " + " << cpt,
         DBG_DETAIL);

    ++cpt;
  }
}
/* -------------------------------------------------------------------------- */

template class XMLRestartParser<1>;
template class XMLRestartParser<2>;
template class XMLRestartParser<3>;
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
