/**
 * @file   lm_parsable.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Thu Sep 18 16:13:19 2014
 *
 * @brief  Common mother for parsable objects
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_LM_PARSABLE_HH__
#define __LIBMULTISCALE_LM_PARSABLE_HH__
/* -------------------------------------------------------------------------- */
#include "lm_defaults.hh"
#include "lm_macros.hh"
#include "lm_object.hh"
#include "lm_parameter.hh"
#include "parse_result.hh"
#include <memory>
#include <set>
/* -------------------------------------------------------------------------- */
namespace pybind11 {
class module_;
using module = module_;

} // namespace pybind11

__BEGIN_LIBMULTISCALE__

class Parsable : public virtual LMObject {

public:
  // Parsable(const LMID & id);
  Parsable();
  virtual ~Parsable();

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

public:
  //! parse an entire line
  virtual ParseResult parseLine(const std::string &buffer);

  //! check that all the keywords are parsed
  void checkAllKeywordsAreParsed();
  //! add a parsable to receive keywords
  void addSubParsableObject(Parsable *ptr);
  //! add a parsable to receive keywords
  void addSubParsableObject(Parsable &obj);

  //!  set parameters by direct access
  template <typename T>
  void setParam(const std::string &keyword, const T &value);
  //!  get parameters by direct access
  template <typename T> T getParam(const std::string &keyword);

  std::set<std::string> getPossibleKeywords();

  //! declare parameters
  virtual void declareParams() = 0;

protected:
  friend void declare_components(pybind11::module &);
  //! declare parameters
  template <typename T>
  void declareParam(const std::string &keyword, T &var, default_val<T> def,
                    UInt n_entries = 0);

  //! set parameters
  virtual ParseResult setParam(const std::string &keyword,
                               std::stringstream &line);

  template <typename T>
  void parseKeyword(const std::string &keyword, T &var,
                    default_val<T> def = default_val<T>());

  template <typename T>
  void parseVectorKeyword(const std::string &keyword, UInt n, T &var,
                          default_val<T> def = default_val<T>());

  template <typename T, typename V>
  void parseKeyword(const std::string &keyword, T &var, V &&def);

  template <typename T, typename V>
  void parseVectorKeyword(const std::string &keyword, UInt n, T &var, V &&def);

  void parseTag(const std::string &keyword, bool &var,
                default_val<bool> def = default_val<bool>());

  template <typename T>
  void changeDefault(const std::string &keyword, const T &val);

  void makeItOptional(const std::string &keyword);

private:
  //! check if parameter is existing
  void checkKeyword(const std::string &keyword);
  //! return a list of registered keywords
  std::string listKeywordsToStr();
  //! generate the list of registered keywords
  void generateListKeywords();
  //! simply answer if is accepting the keyword
  bool doAcceptKeyword(const std::string &keyword);

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

  std::map<std::string, std::shared_ptr<Parameter>> params;

  bool are_parameters_declared;

  std::vector<Parsable *> forward_parsable;

  std::set<std::string> list_keywords;
};

__END_LIBMULTISCALE__

#include "lm_parsable_inline_impl.hh"

#endif /* __LIBMULTISCALE_LM_PARSABLE_HH__ */
