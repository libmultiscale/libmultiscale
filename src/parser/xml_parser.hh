/**
 * @file   xml_parser.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Aug 20 16:58:08 2014
 *
 * @brief  An xml parser class for dump files in that format
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_XML_PARSER_HH__
#define __LIBMULTISCALE_XML_PARSER_HH__
/* -------------------------------------------------------------------------- */
#include "base64_reader.hh"
#include "lm_common.hh"
#include "lm_file.hh"
/* -------------------------------------------------------------------------- */
#include <expat.h>
#include <fstream>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
static void XMLCALL char_handler(void *userData, const XML_Char *s, int len);
static void XMLCALL start_element(void *userData, const char *name,
                                  const char **atts);
static void XMLCALL end_element(void *userData, const char *name);
/* -------------------------------------------------------------------------- */

class XMLParser {

public:
  enum BlockType { TEXT, BINARY };

  XMLParser();
  virtual ~XMLParser();

  virtual UInt parseFile(const std::string &name);

protected:
  friend void XMLCALL char_handler(void *userData, const XML_Char *s, int len);
  friend void XMLCALL start_element(void *userData, const char *name,
                                    const char **atts);
  friend void XMLCALL end_element(void *userData, const char *name);

  virtual void charHandler(InputStream &s) = 0;
  virtual void startElement(const std::string &name,
                            const std::vector<std::string> &atts) = 0;
  virtual void endElement(const std::string &name) = 0;

  XML_Parser parser;
  //! helper for base64
  Base64Reader b64;
  BlockType current_type;
};

/* -------------------------------------------------------------------------- */
inline XMLParser::XMLParser() {
  parser = XML_ParserCreate(NULL);
  XML_SetUserData(parser, this);
  XML_SetElementHandler(parser, start_element, end_element);
  XML_SetCharacterDataHandler(parser, char_handler);
}

/* -------------------------------------------------------------------------- */
inline XMLParser::~XMLParser() { XML_ParserFree(parser); }

/* -------------------------------------------------------------------------- */

static inline void XMLCALL char_handler(void *userData, const XML_Char *str,
                                        int len) {
  XMLParser *obj = (XMLParser *)userData;

  std::string buf;

  DUMP("read a line of size " << len, DBG_ALL);
  if (len == 0)
    return;

  std::copy(str, str + len, std::back_inserter(buf));

  if (buf[0] == '\n') {
    buf.resize(0);
    return;
  }

  if (obj->current_type == XMLParser::TEXT) {
    std::stringstream sstr(buf);
    // obj->charHandler(sstr);
  } else {
    obj->b64.str(buf);
    obj->charHandler(obj->b64);
  }
}
/* -------------------------------------------------------------------------- */

static inline void XMLCALL start_element(void *userData, const char *name,
                                         const char **atts) {
  XMLParser *obj = (XMLParser *)userData;
  std::vector<std::string> _atts;
  for (UInt i = 0; atts[i] != nullptr; ++i) {
    _atts.push_back(atts[i]);
  }
  obj->startElement(name, _atts);
}
/* -------------------------------------------------------------------------- */

static inline void XMLCALL end_element(void *userData, const char *name) {
  XMLParser *obj = (XMLParser *)userData;
  obj->endElement(name);
}

/* -------------------------------------------------------------------------- */

inline UInt XMLParser::parseFile(const std::string &name) {
  char buf[BUFSIZ];
  LMFile file(name, "rb", true);
  while (file.gets(buf, sizeof(buf)) != NULL) {
    UInt len = strlen(buf);
    UInt done = 0;
    DUMP("sending (" << buf << ") to parser", DBG_DETAIL);
    if (XML_Parse(parser, buf, len, done) == XML_STATUS_ERROR) {
      DUMP(XML_ErrorString(XML_GetErrorCode(parser))
               << " at line " << XML_GetCurrentLineNumber(parser),
           DBG_MESSAGE);
      return 1;
    }
  }
  file.close();
  return 0;
}

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

#endif /* __LIBMULTISCALE_XML_PARSER_HH__ */
