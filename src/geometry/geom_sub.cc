/**
 * @file   geom_sub.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Nov 25 15:05:56 2013
 *
 * @brief  Geometry which is the composition of two other ones by substraction
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "geom_sub.hh"
#include "cube.hh"
#include "geometry_manager.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

/* LMDESC SUB
   This geometry describe the substraction
   of two geometries. Let's consider :math:`G_1` and :math:`G_2`
   then :math:`SUB(G_1,G_2) = G_1 \setminus G_2`
*/

/* LMHERITANCE geom_composed */

/* LMEXAMPLE GEOMETRY mysub SUB IDS geom1 geom2  */

void GeomSub::declareParams() { ComposedGeom::declareParams(); }

/* -------------------------------------------------------------------------- */

Cube GeomSub::getBoundingBox() {
  return GeometryManager::getManager()
      .getGeometry(geometries[0])
      ->getBoundingBox();
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
