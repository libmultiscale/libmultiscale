/**
 * @file   chaudron.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Jan 07 16:36:30 2013
 *
 * @brief  Deprecated
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef CHAUDRON_H
#define CHAUDRON_H

#include "geometry.hh"

__BEGIN_LIBMULTISCALE__

/**
 * Class Chaudron : implementing arcs, subparts of circles and boules
 * using the spherical coordinates
 */

class Chaudron : public Geometry {

public:
  Chaudron(UInt d, LMID id) : LMObject(id), Geometry(d, CHAUDRON) {
    rmin = 0;
    rmax = 0;
    zmin = 0;
    zmax = 0;
  };

  bool contains(Real my_x, Real my_y = 0, Real my_z = 0) {
    Real x = my_x - center[0];
    Real y = my_y - center[1];
    Real z = my_z - center[2];

    Real d = x * x + y * y;

    // am I in the base cylinder ?
    if (z >= z_curv && z <= zmax && d <= rmax2)
      return true;
    // am I in the low cylinder ?
    else if (z >= zmin && z <= z_curv && d <= rmin2)
      return true;
    // am I in the curvature?
    else if (z >= zmin && z <= z_curv) {
      Real r_local = rmax - rmin;
      Real x_local = sqrt(d) - rmin;
      Real z_local = z_curv - z;
      if (z_local * z_local + x_local * x_local <= r_local * r_local)
        return true;
    }

    return false;
  }

  //! set the offset for the first spherical coordinates
  void SetRayon(Real Rmax) {
    rmax = Rmax;
    rmax2 = rmax * rmax;
  };
  //! set the zmin and zmax for cylinder coordinates
  void SetZminmax_curv(Real min, Real max, Real curv) {
    zmin = min;
    zmax = max;
    z_curv = curv;
    if (z_curv < zmin)
      LM_FATAL("object creation failed : zmin should be lower than z_curv");
    if (rmax == 0)
      LM_FATAL(
          "object creation failed : SetRayon method should first be called");
    rmin = rmax - (z_curv - zmin);
    rmin2 = rmin * rmin;
  };

  //! return rmin
  Real Rmin() { return rmin; }
  //! return rmax
  Real Rmax() { return rmax; }

  //! return zmin
  Real Zmin() { return zmin; }
  //! return zmax
  Real Zmax() { return zmax; }
  //! return z_curv
  Real Z_curv() { return z_curv; }

private:
  friend class GeomTools;
  //! minimal value for the first spherical coordinate
  Real rmin;
  //! maximal value for the first spherical coordinate
  Real rmax;
  //! minimal value for the first spherical coordinate
  Real rmin2;
  //! maximal value for the first spherical coordinate
  Real rmax2;

  //! minimal value for the cylindrical basis
  Real zmin;
  //! maximal value for the cylindrical
  Real zmax;
  //! value where curvature along z should start
  Real z_curv;
};

__END_LIBMULTISCALE__
#endif // CHAUDRON_H
