/**
 * @file   cube_surface.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Thu Feb 06 09:26:39 2014
 *
 * @brief  Cube geometry with a point-by-point defined surface attached to one
 * face
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_CUBE_SURFACE_HH__
#define __LIBMULTISCALE_CUBE_SURFACE_HH__
/* -------------------------------------------------------------------------- */
#include "cube.hh"
#include "surface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/**
 * Class CubeSurface
 *
 */

class CubeSurface : public Cube {

  /* ------------------------------------------------------------------------ */
  /* Typedefs                                                                 */
  /* ------------------------------------------------------------------------ */

public:
  enum Face {
    _face_left,   // X = xmin
    _face_right,  // X = xmax
    _face_front,  // Y = ymin
    _face_rear,   // Y = ymax
    _face_bottom, // Z = zmin
    _face_top     // Z = zmax
  };

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  CubeSurface(UInt dim, LMID id);
  ~CubeSurface();

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  virtual void declareParams();

  inline bool contains(Real my_x, Real my_y = 0, Real my_z = 0);
  template <UInt Dim>
  inline void transformCoordinates(const Vector<Dim> &X,
                                   Vector<Dim> &X_surface);
  template <UInt Dim> inline void rotateCoordinatesForSurface(Vector<Dim> &X);

  const std::string &surfaceFile();
  void setDirection(Real dir);
  void setDimensions(Real minl, Real maxl, Real minz, Real maxz);
  void init();

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

private:
  friend class GeomTools;
  MySurface *surface;
  std::string surface_file;
  UInt direction;
  int sign;

  Real height_rms_request;
  Real slope_rms_request;
  Real peak2peak_request;

  Face surface_face;
};

/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
template <UInt Dim>
inline void CubeSurface::rotateCoordinatesForSurface(Vector<Dim> &X) {
  Real X_tmp[3];
  UInt cpt = 0;

  for (UInt i = 0; i < 3; ++i) {
    if (i != direction) {
      X_tmp[cpt] = X[i];
      ++cpt;
    }
  }
  X_tmp[cpt] = X[direction];

  for (UInt i = 0; i < 3; ++i)
    X[i] = X_tmp[i];
}
/* -------------------------------------------------------------------------- */

template <UInt Dim>
inline void CubeSurface::transformCoordinates(const Vector<Dim> &X,
                                              Vector<Dim> &X_surface) {

  Quantity<Length, 3> xmin =
      this->getXmin<3>().template cast<PhysicalScalar<Length>>();
  Quantity<Length, 3> xmax =
      this->getXmax().template cast<PhysicalScalar<Length>>();

  for (UInt i = 0; i < 3; ++i) {
    if (i == direction) {
      if (sign < 0)
        X_surface[i] = xmin[i] - X[i];
      else
        X_surface[i] = X[i] - xmax[i];
    } else
      X_surface[i] = X[i] - xmin[i];
  }

  rotateCoordinatesForSurface(X_surface);
  X_surface[2] += surface->getAmplitudeMax() / 2;
}

/* -------------------------------------------------------------------------- */

inline bool CubeSurface::contains(Real my_x, Real my_y, Real my_z) {
  LM_ASSERT(dim == 3, "This geometry can be used only in 3D");

  bool test_cube = Cube::contains(my_x, my_y, my_z);
  if (!test_cube)
    return false;

  Vector<3> X = {my_x, my_y, my_z};
  Vector<3> X_surface = {0., 0., 0.};

  transformCoordinates(X, X_surface);
  bool result = surface->isUnderSurface(X_surface);

  return result;
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_CUBE_SURFACE_HH__ */
