/**
 * @file   geometry_manager.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Jan 22 17:59:17 2013
 *
 * @brief  Manager of all geometries
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_GEOMETRY_MANAGER_HH__
#define __LIBMULTISCALE_GEOMETRY_MANAGER_HH__
/* -------------------------------------------------------------------------- */
#include "geometry.hh"
#include "id_manager.hh"
/* -------------------------------------------------------------------------- */

#ifndef __LIBMULTISCALE_GEOMETRY_HH__
#error "geometry_manager should not be included directly"
#endif
__BEGIN_LIBMULTISCALE__

/** This static class manages the geometries
    avoiding duplicate geometries and insuring
    total freeing of the memory at end */

class GeometryManager : public IDManager<Geometry> {

  /* ------------------------------------------------------------------------ */
  /* Typedefs                                                                 */
  /* ------------------------------------------------------------------------ */

  friend class IDManager<Geometry>;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

protected:
  GeometryManager() = default;

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

public:
  static GeometryManager &getManager() {
    auto & res = IDManager<Geometry>::getManager<GeometryManager>();
    DUMP("GeometryManager: " << &res,DBG_INFO);
    return res;
  }

  //! access a stored geometry
  static auto getGeometry(const typeID &ID) {
    return IDManager<Geometry>::getManager<GeometryManager>().getSharedObject(
        ID);
  };
  //! access a stored geometry casted in the required type
  template <typename geom_type> geom_type *getGeometry(const typeID &ID) {
    geom_type *tmp = dynamic_cast<geom_type *>(this->getObject(ID));
    if (!tmp)
      LM_FATAL("unable to cast the geometry to required type");
    return tmp;
  }
  //! parse a config line
  UInt parseLine(std::stringstream &line, UInt Dim);
  //! create an uninitialized geometry
  std::shared_ptr<Geometry>
  createUninitializedGeometry(Geometry::GeomType type, UInt Dim = 3,
                              const typeID &ID = invalidGeom);

  typeID addGeometry(std::shared_ptr<Geometry> g);
};

/* -------------------------------------------------------------------------- */
std::ostream &operator<<(std::ostream &os, Geometry &g);
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_GEOMETRY_MANAGER_HH__ */
