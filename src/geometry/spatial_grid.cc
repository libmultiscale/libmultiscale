/**
 * @file   spatial_grid.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Mar 01 13:57:57 2013
 *
 * @brief  Spatial grid used to sort objects
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "spatial_grid.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
// neighbors coords
/* -------------------------------------------------------------------------- */

const int NeighborIndexes<3>::topologyCoords[27][3] = {
    {-1, -1, -1}, {-1, -1, 0}, {-1, -1, 1},

    {-1, 0, -1},  {-1, 0, 0},  {-1, 0, 1},

    {-1, 1, -1},  {-1, 1, 0},  {-1, 1, 1},

    {0, -1, -1},  {0, -1, 0},  {0, -1, 1},

    {0, 0, -1},   {0, 0, 0},   {0, 0, 1},

    {0, 1, -1},   {0, 1, 0},   {0, 1, 1},

    {1, -1, -1},  {1, -1, 0},  {1, -1, 1},

    {1, 0, -1},   {1, 0, 0},   {1, 0, 1},

    {1, 1, -1},   {1, 1, 0},   {1, 1, 1}};

const int NeighborIndexes<2>::topologyCoords[9][2] = {
    {-1, -1}, {-1, 0}, {-1, 1},

    {0, -1},  {0, 0},  {0, 1},

    {1, -1},  {1, 0},  {1, 1},
};

const int NeighborIndexes<1>::topologyCoords[3][1] = {
    {-1}, {0}, {1},
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
