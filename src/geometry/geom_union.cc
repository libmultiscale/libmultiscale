/**
 * @file   geom_union.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Nov 25 15:05:56 2013
 *
 * @brief  Geometry which is the composition of two other ones by union
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "geom_union.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

/* LMDESC UNION
   This geometry describe the union
   between two geometries.
   Let's consider :math:`G_1` and :math:`G_2`
   then :math:`UNION(G_1,G_2) = G_1 \cup G_2`
*/

/* LMHERITANCE geom_composed */

/* LMEXAMPLE GEOMETRY myunion UNION IDS geom1 geom2  */

void GeomUnion::declareParams() { ComposedGeom::declareParams(); }

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
