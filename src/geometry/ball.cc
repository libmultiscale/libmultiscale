/**
 * @file   ball.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  Ball geometry
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ball.hh"
#include "cube.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

Ball::Ball(UInt d, LMID id) : LMObject(id), Geometry(d, BALL) {
  for (UInt i = 0; i < 2; ++i) {
    radius_range[i] = 0.0;
    radius_range_2[i] = 0.0;
  }
}
/* -------------------------------------------------------------------------- */

Cube Ball::getBoundingBox() {

  Cube bbox = Geometry::getBoundingBox();

  Real cx = this->getCenter(0);
  Real cy = this->getCenter(1);
  Real cz = this->getCenter(2);

  bbox.setDimensions(cx - this->rMax(), cx + this->rMax(), cy - this->rMax(),
                     cy + this->rMax(), cz - this->rMax(), cz + this->rMax());
  return bbox;
}

/* -------------------------------------------------------------------------- */

void Ball::setRayons(Real rmin, Real rmax) {
  radius_range[0] = rmin;
  radius_range[1] = rmax;

  LM_ASSERT(
      rmin < rmax,
      "minimal radius should be smaller in absolute value than maximal radius "
          << rmin << " > " << rmax);

  init();
}
/* -------------------------------------------------------------------------- */
Real Ball::rMin() const { return radius_range[0]; }
/* -------------------------------------------------------------------------- */
Real Ball::rMax() const { return radius_range[1]; }
/* -------------------------------------------------------------------------- */

void Ball::init() {
  radius_range_2[1] = radius_range[1] * radius_range[1];
  radius_range_2[0] = radius_range[0] * radius_range[0];
}

/* -------------------------------------------------------------------------- */

void Ball::printself(std::ostream &stream) const {
  Geometry::printself(stream);

  std::string tabu;

  stream << tabu << "BALL geom : "
         << " Radii: " << this->rMin() << " " << this->rMax() << " ";
  stream << std::endl;
}

/* -------------------------------------------------------------------------- */

/* LMDESC BALL
   This geometry is the generalized description of a sphere.
   There is also a mechanism to generate holes
   (as described by the HOLE keyword) inside of a plain sphere/circle/segment.
*/

/* LMEXAMPLE
   .. code-block::

     GEOMETRY segment BALL CENTER 0 RADII 0 100
     GEOMETRY disk BALL CENTER 0 0 RADII 0 100
     GEOMETRY ball BALL CENTER 0 0 0 RADII 0 100
*/

void Ball::declareParams() {

  /* LMKEYWORD RADII
     Set the minimal radius and maximal radius. One example:

     .. code-block::

       GEOMETRY circle BALL CENTER 0 0 RADII 0 L0/4

     .. image:: images/geom-circle.svg
  */

  this->parseVectorKeyword("RADII", 2, radius_range);

  /* LMKEYWORD CENTER
     Set the center of the geometry
  */

  this->parseVectorKeyword("CENTER", dim, center);
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
