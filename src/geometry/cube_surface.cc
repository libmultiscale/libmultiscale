/**
 * @file   cube_surface.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Thu Feb 06 11:42:03 2014
 *
 * @brief  Cube geometry with a point-by-point defined surface attached to one
 * face
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "cube_surface.hh"
#include "lm_common.hh"
#include "lm_parsable_inline_impl.hh"
#include "lm_parser.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

template <>
inline UInt Parser::parse(CubeSurface::Face &face, std::stringstream &line,
                          UInt) {
  std::string name;
  UInt nb = strNext(name, line);
  DUMP("is it here1 ? " << nb, DBG_MESSAGE);

  if (name == "left")
    face = CubeSurface::_face_left;
  else if (name == "right")
    face = CubeSurface::_face_right;
  else if (name == "front")
    face = CubeSurface::_face_front;
  else if (name == "rear")
    face = CubeSurface::_face_rear;
  else if (name == "bottom")
    face = CubeSurface::_face_bottom;
  else if (name == "top")
    face = CubeSurface::_face_top;
  else
    LM_FATAL("unknown Face " << name);

  DUMP("is it here ?", DBG_MESSAGE);

  return nb;
}

/* -------------------------------------------------------------------------- */

/* LMDESC CUBE_SURFACE
   TODO
*/

/* LMHERITANCE cube */

/* LMEXAMPLE
   GEOMETRY fractal CUBE_SURFACE BBOX -10 10 -10 10 -10 10 SURFACE_FILE
   surface.dat
*/

void CubeSurface::declareParams() {

  Cube::declareParams();

  /* LMKEYWORD SURFACE_FILE
     The name of the file containing the surface points
  */
  this->parseKeyword("SURFACE_FILE", surface_file);

  /* LMKEYWORD RMS
     The root mean square of heights requested (perform rescaling)
  */
  this->parseKeyword("RMS", height_rms_request, 0.);

  /* LMKEYWORD SRMS
     The root mean square of slopes requested (perform rescaling)
  */
  this->parseKeyword("SRMS", slope_rms_request, 0.);

  /* LMKEYWORD PEAK2PEAK
     The peak to peak distance to be requested
  */
  this->parseKeyword("PEAK2PEAK", peak2peak_request, 0.);

  /* LMKEYWORD FACE
     The face of the cube having the surface definition
  */
  this->parseKeyword("FACE", surface_face);
}

/* -------------------------------------------------------------------------- */

CubeSurface::CubeSurface(UInt dim, LMID id)
    : LMObject(id), Geometry(dim, CUBE_SURFACE) {
  height_rms_request = 0;
  slope_rms_request = 0;
  peak2peak_request = 0;
}

/* -------------------------------------------------------------------------- */

CubeSurface::~CubeSurface() { delete surface; }

/* -------------------------------------------------------------------------- */

const std::string &CubeSurface::surfaceFile() { return surface_file; }

/* -------------------------------------------------------------------------- */
void CubeSurface::setDirection(Real dir) { direction = (int)dir; }
/* -------------------------------------------------------------------------- */

void CubeSurface::init() {
  Cube::init();

  surface = new MySurface();
  surface->loadSurfaceFromTextFile(surface_file, true);

  switch (surface_face) {
  case _face_left:
    direction = 0;
    sign = -1;
    break;
  case _face_right:
    direction = 0;
    sign = 1;
    break;
  case _face_front:
    direction = 1;
    sign = -1;
    break;
  case _face_rear:
    direction = 1;
    sign = 1;
    break;
  case _face_bottom:
    direction = 2;
    sign = -1;
    break;
  case _face_top:
    direction = 2;
    sign = 1;
    break;
  }

  Quantity<Length, 3> xmin = this->getXmin().cast<PhysicalScalar<Length>>();
  Quantity<Length, 3> xmax = this->getXmax().cast<PhysicalScalar<Length>>();

  Vector<3> scale;
  for (UInt i = 0; i < 3; ++i) {
    if (i == direction)
      scale[i] = 1.;
    else
      scale[i] = xmax[i] - xmin[i];
  }
  rotateCoordinatesForSurface(scale);
  surface->setScale(scale);
  if (height_rms_request != 0) {
    DUMPBYPROC("enforce heigh rms " << height_rms_request, DBG_MESSAGE, 0);
    surface->enforceHeightRMS(height_rms_request);
  }
  if (slope_rms_request != 0) {
    DUMPBYPROC("enforce slope rms " << slope_rms_request, DBG_MESSAGE, 0);
    surface->enforceSlopeRMS(slope_rms_request);
  }
  if (peak2peak_request != 0) {
    DUMPBYPROC("enforce peak2peak " << peak2peak_request, DBG_MESSAGE, 0);
    surface->enforcePeak2Peak(peak2peak_request);
  }
  surface->computeHeightMinMax();
  DUMPBYPROC("amplitude max is " << surface->getAmplitudeMax(), DBG_MESSAGE, 0);
  surface->computeAverageHeight();
  DUMPBYPROC("average Height is " << surface->getAverageHeight(), DBG_MESSAGE,
             0);
  surface->computeRMSSlope();
  DUMPBYPROC("RMS Slope is " << surface->getRMSSlope(), DBG_MESSAGE, 0);
  surface->computeRMSHeight();
  DUMPBYPROC("RMS Height is " << surface->getRMSHeight(), DBG_MESSAGE, 0);
  //    surface->plotSurface();
}

/* -------------------------------------------------------------------------- */
// static std::ostream& operator << (std::ostream& os,CubeSurface &g){
//   os << tabu << "CUBE geom : " << g.Lmin() << " " << g.Lmax()
//      << " " << g.Zmin() << " " << g.Zmax()
//      << " surface file " << g.surfaceFile()
//      << std::endl;
//   return os;
// }

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
