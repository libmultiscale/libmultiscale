/**
 * @file   cube.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Nov 25 12:23:57 2013
 *
 * @brief  Cubic geometry
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_CUBE_HH__
#define __LIBMULTISCALE_CUBE_HH__
/* -------------------------------------------------------------------------- */
#include "geometry.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/**
 * Class Cube
 *
 */

class Cube : public virtual Geometry {

public:
  Cube(UInt dim = 3, LMID id = "default geometry");

  virtual ~Cube();

  virtual void declareParams();

  virtual bool contains(Real my_x, Real my_y, Real my_z);
  template <typename T> inline bool contains(T &&X) {
    return Geometry::contains(X);
  }

  virtual void init();
  /// function to print the contain of the class
  virtual void printself(std::ostream &stream) const;

  /* ------------------------------------------------------------------------ */
  /* Accessors                                                                */
  /* ------------------------------------------------------------------------ */

  template <UInt Dim = 3> Vector<Dim> getXmax() const {
    return this->xmax.block<Dim, 1>(0, 0).template cast<Real>();
  }
  template <UInt Dim = 3> Vector<Dim> getXmin() const {
    return this->xmin.block<Dim, 1>(0, 0).template cast<Real>();
  }

  template <typename T> void setXmax(T &&xmax) {
    for (UInt i = 0; i < xmax.size(); ++i)
      this->ranges[2 * i + 1] = xmax[i];
    for (UInt i = xmax.size(); i < 3; ++i)
      this->ranges[2 * i + 1] = std::numeric_limits<Real>::max();
    this->init();
  }

  template <typename T> void setXmin(T &&xmin) {
    for (UInt i = 0; i < xmin.size(); ++i)
      this->ranges[2 * i + 0] = xmin[i];
    for (UInt i = xmin.size(); i < 3; ++i)
      this->ranges[2 * i + 0] = -1. * std::numeric_limits<Real>::max();
    this->init();
  }

  void setXmax(UInt i, Real r);
  void setXmin(UInt i, Real r);
  const Real &Hole(UInt i) const;
  void setHole(Real *X);

  void setDimensions(Real minx, Real maxx, Real miny, Real maxy, Real minz,
                     Real maxz);

  void setDimensions(const Real *xmin, const Real *xmax);

  void setDimensions(const Quantity<Length, 3> &xmin,
                     const Quantity<Length, 3> &xmax);

  void resetDimensions();
  void extendBoundingBox(Real x, Real y, Real z);
  void extendBoundingBox(const Real *X);

  template <UInt Dim, typename T>
  void extendBoundingBox(VectorProxy<Dim, T> &X);

  void extendBoundingBox(Vector<1> &X);
  void extendBoundingBox(Vector<2> &X);
  void extendBoundingBox(Vector<3> &X);
  void extendBoundingBox(VectorView<1> X);
  void extendBoundingBox(VectorView<2> X);
  void extendBoundingBox(VectorView<3> X);

  Vector<3> getSize();
  Real getSize(UInt i);
  Vector<3> getSizeHalf();
  Vector<3> getInvSizeHalf();

  Cube getBoundingBox();
  bool doIntersect(Geometry &geom);

  template <typename Pack> void pack(Pack &&pack) {
    for (UInt i = 0; i < 3; ++i) {
      pack << ranges[2 * i + 0];
      pack << ranges[2 * i + 1];
      pack << xmin[i];
      pack << xmax[i];
      pack << hole[i];
    }
  }

  template <typename Pack> void unpack(Pack &&pack) {
    for (UInt i = 0; i < 3; ++i) {
      pack >> ranges[2 * i + 0];
      pack >> ranges[2 * i + 1];
      pack >> xmin[i];
      pack >> xmax[i];
      pack >> hole[i];
    }
    this->init();
  }

private:
  friend class GeomTools;

  Quantity<Length, 3 * 2> ranges;
  Quantity<Length, 3> xmin;
  Quantity<Length, 3> xmax;
  Quantity<Length, 3> hole;

  bool empty;

  Vector<3> size;
  Vector<3> size_half;
  Vector<3> inv_size_half;
};

/* -------------------------------------------------------------------------- */

inline bool Cube::contains(Real my_x, Real my_y, Real my_z) {

  Real X1[3] = {my_x, my_y, my_z};

  DUMP(X1[0] << " " << getXmax() << " " << getXmin(), DBG_ALL);

  bool result = true;
  for (UInt i = 0; i < dim; ++i) {
    result &= (X1[i] <= getXmax()[i] && X1[i] >= getXmin()[i]);
  }

  if (result) {

    bool result_hole[3] = {true, true, true};

    for (UInt i = 0; i < dim; ++i) {
      if (hole[i] != 0)
        result_hole[i] =
            !(X1[i] <= center[i] + hole[i] && X1[i] >= center[i] - hole[i]);
      result &= result_hole[i];
    }
  }

  for (UInt i = 0; i < dim; ++i) {
    DUMP("result " << result << " " << X1[i] << " xmin " << getXmin()[i]
                   << " xmax " << getXmax()[i],
         DBG_ALL);
  }

  return result;
}

/* -------------------------------------------------------------------------- */

template <UInt Dim, typename T>
inline void Cube::extendBoundingBox(VectorProxy<Dim, T> &X) {
  Vector<Dim, T> v(X);
  extendBoundingBox(v);
}

inline void Cube::extendBoundingBox(Vector<1> &x) {
  Real X[3] = {x[0], 0., 0.};
  extendBoundingBox(X);
}
inline void Cube::extendBoundingBox(Vector<2> &x) {
  Real X[3] = {x[0], x[1], 0.};
  extendBoundingBox(X);
}
inline void Cube::extendBoundingBox(Vector<3> &x) {
  Real X[3] = {x[0], x[1], x[2]};
  extendBoundingBox(X);
}
inline void Cube::extendBoundingBox(VectorView<1> x) {
  Real X[3] = {x[0], 0., 0.};
  extendBoundingBox(X);
}
inline void Cube::extendBoundingBox(VectorView<2> x) {
  Real X[3] = {x[0], x[1], 0.};
  extendBoundingBox(X);
}
inline void Cube::extendBoundingBox(VectorView<3> x) {
  Real X[3] = {x[0], x[1], x[2]};
  extendBoundingBox(X);
}
/* -------------------------------------------------------------------------- */
inline void Cube::extendBoundingBox(Real x, Real y, Real z) {
  Real X[3] = {0., 0., 0.};
  X[0] = x;
  X[1] = y;
  X[2] = z;

  extendBoundingBox(X);
}

/* -------------------------------------------------------------------------- */
inline void Cube::extendBoundingBox(const Real *X) {
  if (empty) {
    setDimensions(X, X);
    empty = false;
    return;
  }

  Real new_xmin[3] = {0., 0., 0.};
  Real new_xmax[3] = {0., 0., 0.};

  for (UInt i = 0; i < dim; ++i) {
    new_xmin[i] = std::min(Real(getXmin()[i]), X[i]);
    new_xmax[i] = std::max(Real(getXmax()[i]), X[i]);
  }

  setDimensions(new_xmin, new_xmax);
}

/* -------------------------------------------------------------------------- */
inline void Cube::setDimensions(Real minx, Real maxx, Real miny, Real maxy,
                                Real minz, Real maxz) {
  Real xmax[3];
  Real xmin[3];
  xmax[0] = maxx;
  xmin[0] = minx;
  xmax[1] = maxy;
  xmin[1] = miny;
  xmax[2] = maxz;
  xmin[2] = minz;

  setDimensions(xmin, xmax);
}

/* -------------------------------------------------------------------------- */
inline void Cube::setDimensions(const Real *xmin, const Real *xmax) {

  for (UInt i = 0; i < this->dim; ++i) {
    this->setXmax(i, xmax[i]);
    this->setXmin(i, xmin[i]);
  }
  init();
}

/* -------------------------------------------------------------------------- */
inline void Cube::setDimensions(const Quantity<Length, 3> &xmin,
                                const Quantity<Length, 3> &xmax) {

  for (UInt i = 0; i < this->dim; ++i) {
    this->setXmax(i, xmax[i]);
    this->setXmin(i, xmin[i]);
  }
  init();
}

/* -------------------------------------------------------------------------- */

inline bool Cube::doIntersect(Geometry &geom) {

  if (auto *c2_ptr = dynamic_cast<Cube *>(&geom)) {
    Cube &c1 = *this;
    Cube &c2 = *c2_ptr;

    DUMP("Intersect operation  : " << c1.getXmin() << " " << c1.getXmax(),
         DBG_DETAIL);

    UInt test = true;

    for (UInt i = 0; i < dim; ++i) {
      if ((c1.getXmin()[i] <= c2.getXmin()[i]) &&
          (c2.getXmin()[i] <= c1.getXmax()[i]))
        test &= true;
      else if ((c1.getXmin()[i] <= c2.getXmax()[i]) &&
               (c2.getXmax()[i] <= c1.getXmax()[i]))
        test &= true;
      else if ((c2.getXmin()[i] <= c1.getXmin()[i]) &&
               (c1.getXmin()[i] <= c2.getXmax()[i]))
        test &= true;
      else if ((c2.getXmin()[i] <= c1.getXmax()[i]) &&
               (c1.getXmax()[i] <= c2.getXmax()[i]))
        test &= true;
    }
    return test;
  }
  LM_FATAL("No implementation of intersection of cube with other geometries");
  return false;
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_CUBE_HH__ */
