/**
 * @file   geometry_manager.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  Manager of all geometries
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "geometry.hh"
#include "geometry_list.hh"
#include "lib_geometry.hh"
#include "lm_common.hh"
#include "lm_parser.hh"
/* -------------------------------------------------------------------------- */
#include <boost/preprocessor.hpp>
/* -------------------------------------------------------------------------- */

#define BOOST_PARSE_GEOMETRY_KEY(r, data, n, x)                                \
  else if (geomType == stringify_macro(BOOST_PP_TUPLE_ELEM(2, 1, x))) geom =   \
      createUninitializedGeometry(Geometry::BOOST_PP_TUPLE_ELEM(2, 1, x), dim, \
                                  geomID);

/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

UInt GeometryManager::parseLine(std::stringstream &line, UInt dim) {

  LMID geomID;
  Parser::parse(geomID, line);
  std::string geomType;
  Parser::parse(geomType, line);

  std::shared_ptr<Geometry> geom;

  if (2 == 1) {
  }
  BOOST_PP_SEQ_FOR_EACH_I(BOOST_PARSE_GEOMETRY_KEY, f, LIST_GEOMETRY)
  else LM_THROW("Unknown geometry type "
                << geomType
                << " maybe you should compile support for this geometry ?");

  UInt pos = line.tellg();
  if (pos < line.str().length()) {
    std::string buffer = line.str().substr(pos);
    DUMPFILE(Parser::fout, "Parsing Geometry " << buffer);
    ParseResult parsed = geom->parseLine(buffer);
    if (!parsed)
      LM_FATAL("Geometry parameter "
               << line.str()
               << " was not parsed !! check config file for a syntax problem");
    Parser::shiftLine(parsed, line);
  }

  geom->checkAllKeywordsAreParsed();
  geom->init();
  this->addGeometry(geom);

  return true;
}

/* -------------------------------------------------------------------------- */

#define BOOST_CREATE_UNINITIALIZED_GEOMETRY(r, data, n, x)                     \
  case Geometry::BOOST_PP_TUPLE_ELEM(2, 1, x):                                 \
    new_geom = std::make_shared<BOOST_PP_TUPLE_ELEM(2, 0, x)>(Dim, ID);        \
    break;

/* -------------------------------------------------------------------------- */

std::shared_ptr<Geometry>
GeometryManager::createUninitializedGeometry(Geometry::GeomType type, UInt Dim,
                                             const typeID &ID) {

  std::shared_ptr<Geometry> new_geom;
  switch (type) {
    BOOST_PP_SEQ_FOR_EACH_I(BOOST_CREATE_UNINITIALIZED_GEOMETRY, f,
                            LIST_GEOMETRY)
  default:
    LM_FATAL("unknown geometry type: internal");
  }
  return new_geom;
}

/* -------------------------------------------------------------------------- */
GeometryManager::typeID
GeometryManager::addGeometry(std::shared_ptr<Geometry> g) {
  return this->addObject(g);
}
/* -------------------------------------------------------------------------- */
template <>
std::unique_ptr<IDManager<Geometry>> IDManager<Geometry>::static_pointer =
    nullptr;
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
