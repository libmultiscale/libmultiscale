/**
 * @file   cylinder.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Mon Jul 28 17:44:19 2014
 *
 * @brief  Cylinder geometry
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "cylinder.hh"
#include "cube.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

Cylinder::Cylinder(UInt d, LMID id) : LMObject(id), Geometry(d, CYLINDER) {

  for (UInt i = 0; i < 3; ++i) {
    dir[i] = 0.0;
  }

  radius_range = 0.0;
  height_range = 0.0;
}

/* -------------------------------------------------------------------------- */

Cube Cylinder::getBoundingBox() {

  Cube bbox = Geometry::getBoundingBox();

  Real cx = this->getCenter(0);
  Real cy = this->getCenter(1);
  Real cz = this->getCenter(2);

  bbox.setDimensions(cx - this->rMax(), cx + this->rMax(), cy - this->rMax(),
                     cy + this->rMax(), cz - this->rMax(), cz + this->rMax());
  return bbox;
}

/* -------------------------------------------------------------------------- */

void Cylinder::init() {

  radius_square = radius_range * radius_range;
  Real norm = 0;
  for (UInt i = 0; i < dim; ++i) {
    norm += this->dir[i] * this->dir[i];
  }
  norm = std::sqrt(norm);

  for (UInt i = 0; i < dim; ++i) {
    this->dir[i] /= norm;
    this->center[i] = this->base[i] + .5 * height_range * dir[i];
  }
}

/* -------------------------------------------------------------------------- */
Real Cylinder::rMin() { return radius_range; }
/* -------------------------------------------------------------------------- */
Real Cylinder::rMax() { return radius_range; }

/* -------------------------------------------------------------------------- */

/* LMDESC CYLINDER
   This geometry is the generalized description of a cylinder.
*/

/* LMEXAMPLE
   .. code-block::

     GEOMETRY cylinder CYLINDER CENTER 0 0 0 RADIUS 100 HEIGHT LZ/2 DIR 0 0 1
*/

void Cylinder::declareParams() {

  /* LMKEYWORD RADIUS
     Set the radius of base circle.
  */
  this->parseVectorKeyword("RADIUS", 1, radius_range);
  /* LMKEYWORD DIR
     Set the extruding direction based on the circle
  */
  this->parseVectorKeyword("DIR", 3, dir);
  /* LMKEYWORD HEIGHT
     Set the amount of extruding of the circle
  */
  this->parseVectorKeyword("HEIGHT", 1, height_range);

  /* LMKEYWORD BASE
     Set the center of the base of the geometry
  */

  this->parseVectorKeyword("BASE", dim, base);
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
