/**
 * @file   surface.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Oct 29 18:26:07 2013
 *
 * @brief  Pointwise defined surface
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "surface.hh"
#include "lm_common.hh"
#include <iomanip>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

void MySurface::plotSurface() {

  std::stringstream temp;
  temp << "surface-fractale-" << lm_my_proc_id << ".plot";
  std::ofstream file(temp.str().c_str());

  file << "set xrange [0:" << size - 1 << "]" << std::endl;
  file << "set yrange [0:" << size - 1 << "]" << std::endl;
  file << "set zrange [-5:5]" << std::endl;

  file << "unset xtics" << std::endl;
  file << "unset ytics" << std::endl;
  file << "unset ztics" << std::endl;

  file << "set pointsize 0.5" << std::endl;

  file << "splot '-' notitle w dot, '-' notitle w p pt 7 lt -1" << std::endl;

  for (UInt x = 0; x < size; x++)
    for (UInt y = 0; y < size; y++)
      file << x << " " << y << std::setprecision(15) << std::scientific
           << heights[x + size * y] << std::endl;

  file << std::endl;
}

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
