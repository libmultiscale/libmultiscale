/**
 * @file   geometry.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Jan 10 20:47:45 2014
 *
 * @brief  Common mother of all geometries
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "geometry.hh"
#include "geometry_manager.hh"
#include "lm_common.hh"

__BEGIN_LIBMULTISCALE__

Geometry::Geometry(UInt Dim, GeomType type) : type(type), dim(Dim) {
  DUMP("creation of a geometric object with dimension " << Dim, DBG_DETAIL);

  center.setZero();
}
/* -------------------------------------------------------------------------- */

Geometry::~Geometry() {}

/* -------------------------------------------------------------------------- */

Geometry::GeomType Geometry::getType() const { return type; }
/* -------------------------------------------------------------------------- */

UInt Geometry::getDim() const { return dim; }
/* -------------------------------------------------------------------------- */

void Geometry::setDim(UInt d) { dim = d; }

/* -------------------------------------------------------------------------- */

void Geometry::setCenter(Real X, Real Y, Real Z) {
  center[0] = X;
  center[1] = Y;
  center[2] = Z;
}

/* -------------------------------------------------------------------------- */

Real Geometry::getCenter(UInt i) const { return center[i]; }

/* -------------------------------------------------------------------------- */

/// function to print the contain of the class
void Geometry::printself(std::ostream &stream) const {
  stream << "Dimension " << dim << std::endl;
  stream << "type " << type << std::endl;
  stream << "center : ";
  for (UInt i = 0; i < dim; ++i)
    stream << center[i] << " ";
  stream << std::endl;
}

/* -------------------------------------------------------------------------- */

GeometryManager &Geometry::getManager() {
  return GeometryManager::getManager();
}

__END_LIBMULTISCALE__
