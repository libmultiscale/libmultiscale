/**
 * @file   ellipsoid.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Jan 07 16:36:30 2013
 *
 * @brief  Ellipsoid geometry
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_ELLIPSOID_HH__
#define __LIBMULTISCALE_ELLIPSOID_HH__
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/**
 * Class Ellipsoid
 *
 */

class Ellipsoid : public Geometry {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  Ellipsoid(UInt Dim, LMID id) : LMObject(id), Geometry(Dim, ELLIPSOID) {
    a = 1;
    b = 1;
    c = 1;
  };

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  bool contains(Real my_x, Real my_y = 0, Real my_z = 0) {

    Real x = my_x - center[0];
    Real y = my_y - center[1];
    Real z = my_z - center[2];

    Real factor = x * x / (a * a) + y * y / (b * b) + z * z / (c * c);
    DUMP(x << " " << y << " " << z << " factor=" << factor, DBG_ALL);

    if (factor <= 1)
      return true;
    return false;
  };

  //! set the minor,major and z-axis values of hte ellispoid
  void SetDimensions(Real dimx, Real dimy, Real dimz) {
    a = dimx;
    b = dimy;
    c = dimz;
  };

  //! return length axis of ellipsoid for the specified space base element
  Real GetAxes(UInt i) {
    switch (i) {
    case 0:
      return a;
    case 1:
      return b;
    case 2:
      return c;
    }
    LM_FATAL("bad requested index in ellipsoid axe objet");

    return -1;
  };

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

private:
  //! minor,major and z-axis values
  Real a, b, c;
};

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_ELLIPSOID_HH__ */
