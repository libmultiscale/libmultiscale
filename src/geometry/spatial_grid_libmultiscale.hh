/**
 * @file   spatial_grid_libmultiscale.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Nov 18 22:30:00 2013
 *
 * @brief  Spatial grid used to sort objects
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_SPATIAL_GRID_LIBMULTISCALE_HH__
#define __LIBMULTISCALE_SPATIAL_GRID_LIBMULTISCALE_HH__
/* -------------------------------------------------------------------------- */
#include "cube.hh"
#include "lm_common.hh"
#include "math_tools.hh"
#include "spatial_grid.hh"
#include "units.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

template <typename T, UInt Dim>
class SpatialGridLibMultiScale : public SpatialGrid<T, Dim> {

public:
  template <typename S, UInt d>
  SpatialGridLibMultiScale(const Cube &c, const Vector<d, S> &s);
  SpatialGridLibMultiScale(const Cube &c, const Vector<Dim> &p);
  SpatialGridLibMultiScale(const Cube &c, const Vector<Dim> &p,
                           const Vector<Dim, UInt> &s);
  SpatialGridLibMultiScale(const Cube &c, const Real p);
  ~SpatialGridLibMultiScale();

  //! function which applies a predefined operator
  T extractBoxValue(UInt index, Operator op);

private:
};

/* -------------------------------------------------------------------------- */

template <typename T, UInt Dim>
template <typename S, UInt d>
SpatialGridLibMultiScale<T, Dim>::SpatialGridLibMultiScale(
    const Cube &c, const Vector<d, S> &s)
    : LMObject("spatial_grid"),
      SpatialGrid<T, Dim>(c.getXmin<Dim>(), c.getXmax<Dim>(),
                          Vector<Dim, S>{s.template block<Dim, 1>(0, 0)}) {}
/* -------------------------------------------------------------------------- */
template <typename T, UInt Dim>
SpatialGridLibMultiScale<T, Dim>::SpatialGridLibMultiScale(const Cube &c,
                                                           const Vector<Dim> &p)
    : LMObject("spatial_grid"), SpatialGrid<T, Dim>(c.getXmin(), c.getXmax(),
                                                    p) {}
/* -------------------------------------------------------------------------- */
template <typename T, UInt Dim>
SpatialGridLibMultiScale<T, Dim>::SpatialGridLibMultiScale(
    const Cube &c, const Vector<Dim> &p, const Vector<Dim, UInt> &s)
    : LMObject("spatial_grid"), SpatialGrid<T, Dim>(c.getXmin<Dim>(),
                                                    c.getXmax<Dim>(), p, s) {}
/* -------------------------------------------------------------------------- */
template <typename T, UInt Dim>
SpatialGridLibMultiScale<T, Dim>::SpatialGridLibMultiScale(const Cube &c,
                                                           Real p)
    : LMObject("spatial_grid"), SpatialGrid<T, Dim>(c.getXmin<Dim>(),
                                                    c.getXmax<Dim>(), p) {}

/* -------------------------------------------------------------------------- */
template <typename T, UInt Dim>
SpatialGridLibMultiScale<T, Dim>::~SpatialGridLibMultiScale() {}

/* -------------------------------------------------------------------------- */

template <typename T, UInt Dim>
T SpatialGridLibMultiScale<T, Dim>::extractBoxValue(UInt index, Operator op) {
  switch (op) {
  case OP_AVERAGE: {
    MathTools::AverageOperator<T> av_op;
    return this->extractBoxValuePerOperator(index, av_op);
  } break;
  case OP_DENSITY: {
    MathTools::DensityOperator<T> ro_op;
    return this->extractBoxValuePerOperator(index, ro_op);
  } break;
  case OP_DEVIATION: {
    MathTools::DeviationOperator<T> de_op;
    return this->extractBoxValuePerOperator(index, de_op);
  } break;
  case OP_MAX: {
    MathTools::MaxOperator<T> ma_op;
    return this->extractBoxValuePerOperator(index, ma_op);
  } break;
  case OP_MIN: {
    MathTools::MinOperator<T> mi_op;
    return this->extractBoxValuePerOperator(index, mi_op);
  } break;
  case OP_SUM: {
    MathTools::SumOperator<T> su_op;
    return this->extractBoxValuePerOperator(index, su_op);
  } break;
  default:
    LM_FATAL("out of switch");
  }
  T def(0.0);
  return def;
}
/* -------------------------------------------------------------------------- */

#define SpatialGridPoint SpatialGridLibMultiScale

#define SpatialGridElem SpatialGridLibMultiScale

template <UInt Dim>
class SpatialGridPointInteger : public SpatialGridLibMultiScale<UInt, Dim> {
public:
  SpatialGridPointInteger(Cube &c, Real pas)
      : SpatialGridLibMultiScale<UInt, Dim>(c, pas){};
  SpatialGridPointInteger(Cube &c, UInt *st)
      : SpatialGridLibMultiScale<UInt, Dim>(c, st){};

private:
};

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_SPATIAL_GRID_LIBMULTISCALE_HH__ */
