/**
 * @file   geometry.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Jan 10 20:47:45 2014
 *
 * @brief  Common mother of all geometries
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_GEOMETRY_HH__
#define __LIBMULTISCALE_GEOMETRY_HH__
/* -------------------------------------------------------------------------- */
#include "lm_parsable.hh"
#include <cmath>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class Cube;
class GeometryManager;
/* -------------------------------------------------------------------------- */

/**
 * Class Geometry
 *
 */
class Geometry : public Parsable {

public:
  enum GeomType {
    BALL = 1,
    CUBE = 2,
    INTER = 3,
    SUB = 4,
    ELLIPSOID = 5,
    UNION = 6,
    CYLINDER = 7,
    CHAUDRON = 8,
    CUBE_SURFACE = 9
  };

public:
  Geometry(UInt Dim, GeomType Type);
  virtual ~Geometry();

  //! return type of geometry
  GeomType getType() const;
  //! return dimention of geometry
  UInt getDim() const;
  //! return dimention of geometry
  void setDim(UInt d);
  //! set the center of the geometry
  void setCenter(Real X, Real Y = 0, Real Z = 0);
  //! set the rotation matrix from euler parameters
  void setRotation(Real phi, Real theta, Real psi);
  //! get the i-est coordinate of the center
  Real getCenter(UInt i) const;
  //! get the coordinates of the center
  const Quantity<Length, 3> &getCenter() const { return center; };

public:
  /// function to print the contain of the class
  virtual void printself(std::ostream &stream) const;
  //! init function
  virtual void init() = 0;

  //! function that return true if geometrie contaUInt poUInt x,y,z
  virtual bool contains(Real x, Real y, Real z) = 0;
  //! generic function that call the contains from a pointer

  template <UInt Dim, typename T> inline bool contains(Vector<Dim, T> X) {
    return this->contains<Dim>(X.data());
  }

  template <UInt Dim> inline bool contains(VectorView<Dim> X) {
    return this->contains<Dim>(X.data());
  }

  template <UInt Dim, typename T> inline bool contains(VectorProxy<Dim, T> X) {
    Vector<Dim> x(X);
    return this->contains<Dim>(x.data());
  }

  template <UInt Dim> inline bool contains(Real *X) {
    if (Dim == 1) {
      return this->contains(X[0], 0., 0.);
    } else if (Dim == 2) {
      return this->contains(X[0], X[1], 0.);
    } else if (Dim == 3) {
      return this->contains(X[0], X[1], X[2]);
    } else {
      LM_FATAL("Unknown dimension '" << Dim << "'");
      return false;
    }
  }
  //! return the distance to the center
  Real distToCenter(Real X, Real Y = 0, Real Z = 0);
  //! return the distance to the center
  Real distToCenter(Real *X);
  //! return the unidimentional(X) vector to the center (see source code)
  Real vecToCenter(UInt index, Real X);
  //! return the normed vector coordinate to the center
  Real normToCenter(UInt index, Real X, Real Y = 0.0, Real Z = 0.0);
  //! return a bounding box of the current geometry
  virtual Cube getBoundingBox() = 0;

  static GeometryManager &getManager();

protected:
  friend class GeomTools;
  //! type of geom
  GeomType type;
  //! dimention
  UInt dim;
  //! center coordinates
  Quantity<Length, 3> center;
};

/* -------------------------------------------------------------------------- */

inline Real Geometry::distToCenter(Real X, Real Y, Real Z) {
  Real x[3] = {X, Y, Z};
  return distToCenter(x);
}

/* -------------------------------------------------------------------------- */

inline Real Geometry::distToCenter(Real *X) {
  Real res = 0.0;
  for (UInt i = 0; i < 3; ++i) {
    Real tmp = center[i] - X[i];
    res += tmp * tmp;
  }

  return sqrt(res);
}

/* -------------------------------------------------------------------------- */

inline Real Geometry::vecToCenter(UInt index, Real X) {
  Real res = X - center[index];
  //    LOG("vec au centre = " << res << " sur axe " << index);
  return res;
}

/* -------------------------------------------------------------------------- */

inline Real Geometry::normToCenter(UInt index, Real X, Real Y, Real Z) {
  Real res[3];
  res[0] = X - center[0];
  res[1] = Y - center[1];
  res[2] = Z - center[2];

  Real norm = sqrt(res[0] * res[0] + res[1] * res[1] + res[2] * res[2]);

  if (!norm)
    return 0.0;

  DUMP("norm au centre = " << res[index] / norm << " sur axe " << index,
       DBG_ALL);

  return res[index] / norm;
}

/* -------------------------------------------------------------------------- */
/// standard output stream operator
inline std::ostream &operator<<(std::ostream &stream, Geometry &_this) {
  _this.printself(stream);
  return stream;
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_GEOMETRY_HH__ */
