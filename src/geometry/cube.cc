/**
 * @file   cube.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Mon Nov 25 17:17:19 2013
 *
 * @brief  Cubic geometry
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "cube.hh"
#include "geometry_manager.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

Cube Geometry::getBoundingBox() {

  LMID bboxID = this->getID() + ":bbox";
  Cube bbox(dim, bboxID);
  return bbox;
}

/* -------------------------------------------------------------------------- */

Cube::Cube(UInt dim, LMID id) : LMObject(id), Geometry(dim, CUBE) {

  xmin.setZero();
  xmax.setZero();
  hole.setZero();
  ranges.setZero();
  size.setZero();
  size_half.setZero();
  inv_size_half.setZero();
  empty = true;
  DUMP("new cube " << this, DBG_INFO);
}

/* -------------------------------------------------------------------------- */

Cube::~Cube() { DUMP("delete cube " << this, DBG_INFO); }

/* -------------------------------------------------------------------------- */

void Cube::setXmin(UInt i, Real r) {
  this->xmin[i] = r;
  this->ranges[2 * i + 0] = r;
  this->init();
}
/* -------------------------------------------------------------------------- */
void Cube::setXmax(UInt i, Real r) {
  this->xmax[i] = r;
  this->ranges[2 * i + 1] = r;
  this->init();
}
/* -------------------------------------------------------------------------- */
const Real &Cube::Hole(UInt i) const { return hole[i]; }
/* -------------------------------------------------------------------------- */
void Cube::setHole(Real *X) {
  for (UInt i = 0; i < 3; ++i) {
    hole[i] = X[i];
  }
}
/* -------------------------------------------------------------------------- */
void Cube::resetDimensions() {
  for (UInt i = 0; i < 3; ++i) {
    setXmax(i, 0.0);
    setXmin(i, 0.0);
  }
  init();
}
/* -------------------------------------------------------------------------- */
Vector<3> Cube::getSize() { return size; }
/* -------------------------------------------------------------------------- */

Real Cube::getSize(UInt i) { return size[i]; }
/* -------------------------------------------------------------------------- */

Vector<3> Cube::getSizeHalf() { return size_half; }

/* -------------------------------------------------------------------------- */

Vector<3> Cube::getInvSizeHalf() { return inv_size_half; }

/* -------------------------------------------------------------------------- */

Cube Cube::getBoundingBox() { return *this; }

/* -------------------------------------------------------------------------- */

void Cube::init() {
  for (UInt i = 0; i < 3; ++i) {
    xmin[i] = this->ranges[2 * i + 0];
    xmax[i] = this->ranges[2 * i + 1];
    this->center[i] = getXmin()[i] + (getXmax()[i] - getXmin()[i]) / 2;
    this->size[i] = getXmax()[i] - getXmin()[i];
    this->size_half[i] = this->size[i] / 2.;
    this->inv_size_half[i] = 1. / this->size_half[i];
  }
}
/* -------------------------------------------------------------------------- */

void Cube::printself(std::ostream &stream) const {
  Geometry::printself(stream);

  std::string tabu;

  stream << tabu << "CUBE geom : " << std::endl
         << "xmin: " << std::endl
         << this->getXmin() << std::endl
         << "xmax: " << std::endl
         << this->getXmax() << std::endl
         << "hole: " << std::endl
         << this->hole << std::endl;
}

/* -------------------------------------------------------------------------- */

/* LMDESC CUBE
   This geometry is the generalized description of a bounding box.
   There is also a mechanism to generate holes
   (as described by the HOLE keyword) inside of a plain bounding box.
*/

/* LMEXAMPLE
   .. code-block::

     GEOMETRY segment CUBE BBOX -10 10
     GEOMETRY rectangle CUBE BBOX -10 10 -20 20
     GEOMETRY cube CUBE BBOX -10 10 -10 10 -10 10
*/

void Cube::declareParams() {

  /* LMKEYWORD BBOX
     Give the bounding box coordinates. For example:

     .. code-block::

       GEOMETRY full CUBE BBOX -L0/2 L0/2 -L1/2 L1/2

     .. image:: images/geom-cube.svg

  */
  this->parseVectorKeyword("BBOX", dim * 2, ranges);
  /* LMKEYWORD HOLE
     In the bounding box description this provides the hole values
     Below are provided a few self explanatory examples.

     .. code-block::

       GEOMETRY 1d-hole CUBE BBOX -L0/2 L0/2 -L1/2 L1/2 HOLE L0/10 0

     .. image:: images/geom-cube-hole1d.svg

     .. code-block::

       GEOMETRY 2d-hole CUBE BBOX -L0/2 L0/2 -L1/2 L1/2 HOLE L0/10 L1/5

     .. image:: images/geom-cube-hole2d.svg
  */
  this->parseVectorKeyword("HOLE", dim, hole, VEC_DEFAULTS(0., 0., 0.));
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
