/**
 * @file   geom_composed.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Nov 25 15:05:56 2013
 *
 * @brief  Geometry which is the composition of two other ones
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULISCALE_GEOM_COMPOSED_HH__
#define __LIBMULISCALE_GEOM_COMPOSED_HH__
/* -------------------------------------------------------------------------- */
#include "geometry.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/**
 * Class Composed
 *
 */

class ComposedGeom : public Geometry {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  ComposedGeom(UInt Dim, GeomType type) : Geometry(Dim, type){};

  /* ------------------------------------------------------------------------ */
  /* Accessors                                                                */
  /* ------------------------------------------------------------------------ */

  //! return the geometry from its number
  LMID getGeom(UInt i) { return geometries[i]; }

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

  void declareParams();

  void init();

protected:
  friend class GeomTools;

  //! geometries to compose
  LMID geometries[2];

  //! ptr to geometries to compose
  std::shared_ptr<Geometry> geoms[2];
};
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif /* __LIBMULISCALE_GEOM_COMPOSED_HH__ */
