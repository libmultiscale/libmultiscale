/**
 * @file   geom_composed.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Nov 25 15:05:56 2013
 *
 * @brief  Geometry which is the composition of two other ones
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "geom_composed.hh"
#include "geometry_manager.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

void ComposedGeom::init() {
  geoms[0] = GeometryManager::getManager().getGeometry(geometries[0]);
  geoms[1] = GeometryManager::getManager().getGeometry(geometries[1]);
}

/* -------------------------------------------------------------------------- */

/* LMDESC ComposedGeom
   This is the generic class that associate two geometries
*/

void ComposedGeom::declareParams() {

  /* LMKEYWORD IDS
     Give the ID of the 2 geometries to associate
  */
  this->parseVectorKeyword("IDS", 2, geometries);
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
