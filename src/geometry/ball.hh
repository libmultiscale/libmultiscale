/**
 * @file   ball.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  Ball geometry
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_BALL_HH__
#define __LIBMULTISCALE_BALL_HH__
/* -------------------------------------------------------------------------- */
#include "geometry.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/**
 * Class Ball : implementing arcs, subparts of circles and boules
 * using the spherical coordinates
 */

class Ball : public Geometry {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  Ball(UInt d, LMID id = invalidGeom);

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  inline virtual void declareParams();

  virtual void init();

  Cube getBoundingBox();

  inline bool contains(Real my_x, Real my_y = 0, Real my_z = 0);

  //! set the offset for the first spherical coordinates
  void setRayons(Real rmin, Real rmax);
  //! set the angle offset for the second spherical coordinates
  void setAngleOffset1(Real min, Real max);

  //! set the angle offset for the third spherical coordinates
  void setAngleOffset2(Real min, Real max);
  //! return rmin
  Real rMin() const;
  //! return rmax
  Real rMax() const;

  /// function to print the contain of the class
  virtual void printself(std::ostream &stream) const;

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

private:
  friend class GeomTools;
  //! minimal and maximal value for the first spherical coordinate
  Quantity<Length, 2> radius_range;
  //! minimal and maximal squared value for the first spherical coordinate
  Real radius_range_2[2];
};

/* -------------------------------------------------------------------------- */

inline bool Ball::contains(Real my_x, Real my_y, Real my_z) {
  Real x = my_x - center[0];
  Real y = my_y - center[1];
  Real z = my_z - center[2];

  Real d = x * x + y * y + z * z;

  if (d <= radius_range_2[1] && d >= radius_range_2[0]) {
    return true;
  }
  return false;
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_BALL_HH__ */
