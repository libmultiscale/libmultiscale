/**
 * @file   cylinder.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Mon Jul 28 17:44:19 2014
 *
 * @brief  Cylinder gemoetry
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_CYLINDER_HH__
#define __LIBMULTISCALE_CYLINDER_HH__
/* -------------------------------------------------------------------------- */
#include "geometry.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/**
 * Class Cylinder
 */

class Cylinder : public Geometry {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  Cylinder(UInt d, LMID id = invalidGeom);

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  void declareParams();
  void init();
  /// function to print the contain of the class
  virtual void printself(std::ostream &stream) const {
    Geometry::printself(stream);

    stream << "Cylinder geom : "
           << "Radius range: " << this->radius_range << " Height range: "
           << " " << this->height_range << " "
           << " direction: " << dir << " base: " << base << std::endl;
  };

  Cube getBoundingBox();

  inline bool contains(Real my_x, Real my_y = 0, Real my_z = 0);

  //! return rmin
  Real rMin();
  //! return rmax
  Real rMax();

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

private:
  friend class GeomTools;
  Quantity<Length> radius_range;
  Quantity<Length> height_range;
  Quantity<Length, 3> dir;
  Real radius_square;
  Quantity<Length, 3> base;
};
/* -------------------------------------------------------------------------- */

inline bool Cylinder::contains(Real my_x, Real my_y, Real my_z) {

  Real dpos[3] = {my_x - base[0], my_y - base[1], my_z - base[2]};

  Real dotp = 0;
  for (UInt i = 0; i < dim; ++i) {
    dotp += dpos[i] * this->dir[i];
  }
  if ((dotp < this->height_range) && (dotp > 0)) {
    Real proj_len_square = 0;
    for (UInt i = 0; i < dim; ++i) {
      Real comp = dpos[i] - this->dir[i] * dotp;
      proj_len_square += comp * comp;
    }
    if (proj_len_square < radius_square) {
      return true;
    }
  }
  return false;
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif // CYLINDER_H
