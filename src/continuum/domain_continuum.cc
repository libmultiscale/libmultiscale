/**
 * @file   domain_continuum.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Nov 06 20:35:11 2013
 *
 * @brief  Mother of all continuum domains
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

/* -------------------------------------------------------------------------- */
#include "domain_continuum.hh"
#include "geometry_manager.hh"
#include "lib_continuum.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
#include "xml_parser_restart.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

template <typename ContElems, typename ContNodes, UInt Dim>
DomainContinuum<ContElems, ContNodes, Dim>::DomainContinuum(CommGroup group)
    : DomainInterface(group), arlequin_flag(false),
      mesh_container(this->getID() + ":mesh"),
      prestressed_periodicity_flag(false) {
  geometries.resize(10);
  createOutput("mesh");
  getOutput("mesh") = mesh_container;
  mesh_container.setCommGroup(group);
}

/* -------------------------------------------------------------------------- */

template <typename ContElems, typename ContNodes, UInt Dim>
DomainContinuum<ContElems, ContNodes, Dim>::~DomainContinuum() {}

/* -------------------------------------------------------------------------- */
template <typename ContElems, typename ContNodes, UInt Dim>
typename DomainContinuum<ContElems, ContNodes, Dim>::ContainerPoints &
DomainContinuum<ContElems, ContNodes, Dim>::getContainer() {

  auto comm_group = getCommGroup();
  if (comm_group.amIinGroup()) {
    mesh_container.getBoundingBox() = getDomainBoundingBox();
  }
  mesh_container.setCommGroup(getCommGroup());
  return mesh_container;
}

/* -------------------------------------------------------------------------- */

template <typename ContElems, typename ContNodes, UInt Dim>
inline void DomainContinuum<ContElems, ContNodes, Dim>::readXMLFile(
    const std::string &filename) {

  XMLRestartParser<Dim> parser;

  DUMP("Setting Dummy simulation from libMultiScale XML Data format "
           << filename,
       DBG_INFO);

  if (parser.parseFile(filename)) {
    LM_FATAL("restart was not performed because file was not found"
             << filename);
    return;
  }

  DUMP("read done " << filename, DBG_INFO);

  UnitsConverter unit;
  unit.setInUnits(si_unit_system);

  UInt cpt = 0;
  UInt cpt2 = 0;
#ifndef LM_OPTIMIZED
  UInt total = getContainerNodes().size();
#endif // LM_OPTIMIZED
  DUMP("total = " << total, DBG_DETAIL);

  for (auto &&n : getContainerNodes()) {
    auto X = n.position0();

    if (cpt2 % 100 == 0)
      DUMP("passing node number " << cpt2 << "/" << total << " reloaded",
           DBG_INFO);
    ++cpt2;
    if (!parser.isDofRegistered(X))
      continue;

    Vector<Dim> U;
    parser.template assignField<_displacement>(X, U);
    n.displacement() = U;
    Vector<Dim> V;
    parser.template assignField<_velocity>(X, V);
    n.velocity() = V;

    if (cpt % 100 == 0)
      DUMP("node number " << cpt << "/" << total << " reloaded", DBG_INFO);
    ++cpt;
  }

  DUMP("associated " << cpt << " over " << total, DBG_INFO);
  //  performStep2();
}
/* -------------------------------------------------------------------------- */

template <typename ContElems, typename ContNodes, UInt Dim>
ContElems &DomainContinuum<ContElems, ContNodes, Dim>::getContainerElems() {
  auto comm_group = getCommGroup();
  auto &elems = mesh_container.getContainerElems();
  if (comm_group.amIinGroup()) {
    elems.getBoundingBox() = getDomainBoundingBox();
  }
  elems.setCommGroup(getCommGroup());
  return elems;
}

/* -------------------------------------------------------------------------- */

template <typename ContElems, typename ContNodes, UInt Dim>
ContNodes &DomainContinuum<ContElems, ContNodes, Dim>::getContainerNodes() {
  auto comm_group = getCommGroup();
  auto &nodes = mesh_container.getContainerNodes();
  if (comm_group.amIinGroup()) {
    nodes.getBoundingBox() = getDomainBoundingBox();
  }
  nodes.setCommGroup(getCommGroup());
  return nodes;
}
/* -------------------------------------------------------------------------- */
template <typename ContElems, typename ContNodes, UInt Dim>
std::vector<std::pair<UInt, UInt>> &
DomainContinuum<ContElems, ContNodes, Dim>::getPBCpairs() {
  return pbc_pairs;
}

/* -------------------------------------------------------------------------- */

/* LMDESC DomainContinuum

   This section describe the section that is associated
   with continuum mechanics model. It is the starting
   point of every Continuum Mechanics plugins.
*/

template <typename ContElems, typename ContNodes, UInt Dim>
inline void DomainContinuum<ContElems, ContNodes, Dim>::declareParams() {

  DomainInterface::declareParams();

  /* LMKEYWORD DOMAIN_GEOMETRY
     Specify the geometry/region describing the domain
   */
  this->parseKeyword("DOMAIN_GEOMETRY", geometries[0]);

  /* LMKEYWORD CONSTRAINED_GEOMETRY
     For minimization purpose (obsolete). This provided a region
     not included in the computation of the objective function and the gradient.
   */
  this->parseKeyword("CONSTRAINED_GEOMETRY", geom_constrained, invalidGeom);

  /* LMKEYWORD TIMESTEP
     Specify the explicit integration timestep
   */
  this->parseKeyword("TIMESTEP", timeStep);

  /* LMKEYWORD PRESTRESSED_PERIODICITY
     set whether the corrective displacement of slaves should be considered
   */
  this->parseKeyword("PRESTRESSED_PERIODICITY", prestressed_periodicity_flag,
                     false);
}
/* -------------------------------------------------------------------------- */

#ifdef LIBMULTISCALE_LIBMESH
template class DomainContinuum<ContainerElemsLibMesh<1>,
                               ContainerNodesLibMesh<1>, 1>;
template class DomainContinuum<ContainerElemsLibMesh<2>,
                               ContainerNodesLibMesh<2>, 2>;
template class DomainContinuum<ContainerElemsLibMesh<3>,
                               ContainerNodesLibMesh<3>, 3>;
#endif

#ifdef LIBMULTISCALE_MECA1D
template class DomainContinuum<ContainerElemsMeca1D, ContainerNodesMeca1D, 1>;
#endif

#ifdef LIBMULTISCALE_AKANTU_PLUGIN
template class DomainContinuum<ContainerElemsAkantu<1>, ContainerNodesAkantu<1>,
                               1>;
template class DomainContinuum<ContainerElemsAkantu<2>, ContainerNodesAkantu<2>,
                               2>;
template class DomainContinuum<ContainerElemsAkantu<3>, ContainerNodesAkantu<3>,
                               3>;
#endif

__END_LIBMULTISCALE__
