/**
 * @file   ref_node_continuum.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jan 22 17:41:05 2014
 *
 * @brief  This is the mother of all mesh node reference
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_REF_NODE_CONTINUUM_HH__
#define __LIBMULTISCALE_REF_NODE_CONTINUUM_HH__
/* -------------------------------------------------------------------------- */
#include "accessor_dof.hh"
#include "cube.hh"
#include "lm_common.hh"
#include "ref_point.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
template <UInt Dim, typename DaughterClass> class RefNode;
/* -------------------------------------------------------------------------- */
template <typename Ref, FieldType ftype>
struct AccessorNodalDof : public AccessorDof<Ref, ftype> {
  using AccessorDof<Ref, ftype>::AccessorDof;
  using AccessorDof<Ref, ftype>::operator=;
};
/* -------------------------------------------------------------------------- */

/**
 * Class RefNode
 * Generic class to represent nodal references
 */

template <UInt Dim, typename DaughterClass>
class RefNode : public RefPoint<Dim> {

public:
#define declare_accessor(_name)                                                \
  decltype(auto) _name() { return static_cast<DaughterClass *>(this)->_name(); }

  declare_accessor(position0);
  declare_accessor(position);
  declare_accessor(velocity);
  declare_accessor(force);
  declare_accessor(acceleration);
  declare_accessor(displacement);
  declare_accessor(mass);
  declare_accessor(id);
  declare_accessor(boundary);
  declare_accessor(internal_force);
  declare_accessor(external_force);

#undef declare_accessor

  template <FieldType ftype> AccessorNodalDof<RefNode, ftype> field() {
    AccessorNodalDof<RefNode, ftype> acc(*this);
    return acc;
  }

public:
  static constexpr UInt dim = Dim;
};

/* -------------------------------------------------------------------------- */

template <typename Ref> class AccessorNodalDof<Ref, _position> {

public:
  static constexpr FieldType ftype = _position;
  using TensorType = Vector<Ref::Dim>;
  static constexpr UInt TensorDim = Ref::Dim;
  using Scalar = Real;

  AccessorNodalDof(Ref &nd) : nd(nd) {
    position = nd.position0() + nd.displacement();
  };

  UInt size() { return Ref::Dim; }

  template <typename T>
  inline AccessorNodalDof<Ref, ftype> &operator=(const T &val) {
    position = val;
    nd.displacement() = position - nd.position0();
    return *this;
  }

  class AccessorPositionDof {

  public:
    AccessorPositionDof(Ref &nd, UInt index) : nd(nd), index(index) {}

    operator Real() { return (nd.position0() + nd.displacement())[index]; }

    template <typename T> inline AccessorPositionDof &operator=(const T &val) {
      nd.displacement()[index] = val - nd.position0()[index];
      return *this;
    }

    template <typename T> inline AccessorPositionDof &operator+=(const T &val) {
      nd.displacement()[index] += val;
      return *this;
    }

    template <typename T> inline AccessorPositionDof &operator-=(const T &val) {
      nd.displacement()[index] -= val;
      return *this;
    }

  private:
    Ref &nd;
    UInt index;
  };

  inline AccessorPositionDof operator[](UInt index) {
    return AccessorPositionDof(nd, index);
  }
  operator Vector<Ref::Dim>() { return Vector<Ref::Dim>(position); }
  operator Eigen::VectorXd() { return Eigen::VectorXd(position); }

  void printself(std::ostream &os) { os << position; }

private:
  Ref &nd;
  TensorType position;

public:
  using value_type = Real;
};
/* -------------------------------------------------------------------------- */

template <typename Ref, FieldType ftype>
std::ostream &operator<<(std::ostream &os, AccessorNodalDof<Ref, ftype> &&acc) {
  acc.printself(os);
  return os;
}

/* -------------------------------------------------------------------------- */
template <typename Ref, FieldType ftype>
std::ostream &operator<<(std::ostream &os, AccessorNodalDof<Ref, ftype> &acc) {
  acc.printself(os);
  return os;
}

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_REF_NODE_CONTINUUM_HH__ */
