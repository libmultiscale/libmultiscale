/**
 * @file   ref_elem_akantu.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Jun 24 22:58:02 2014
 *
 * @brief  This is a reference to Akantu elements
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ref_elem_akantu.hh"
#include "lm_common.hh"
#include "ref_node_akantu.hh"
#include <solid_mechanics_model.hh>
#ifdef AKANTU_HEAT_TRANSFER
#include <heat_transfer_model.hh>
#endif // AKANTU_HEAT_TRANSFER
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

template <UInt _Dim>
inline void
RefElemAkantu<_Dim>::setAkantuPointers(AkantuDOFWrapper<_Dim> &dof) {
  model = &dof.getSolidMechanicsModel();
  this->fem = &model->getFEEngineClass<MyFEM>();
  this->dofs = &dof;
  index = 0;

  nd.setAkantuPointers(dof);

#ifdef AKANTU_HEAT_TRANSFER
  // if (nd.getHeatTransferFlag())
  //   heat_transfer_model = &dof.getHeatTransferModel();
#endif
}
/* -------------------------------------------------------------------------- */

template <UInt _Dim> inline Real RefElemAkantu<_Dim>::epot() {
  return model->getEnergy("potential", this->dofs->getType(), index);
}
/* -------------------------------------------------------------------------- */

#ifdef AKANTU_HEAT_TRANSFER
template <UInt _Dim> inline Real RefElemAkantu<_Dim>::getEThermal() {
  return heat_transfer_model->getEnergy("thermal", this->dofs->getType(),
                                        index);
}
#endif
/* -------------------------------------------------------------------------- */

template <UInt _Dim> Real RefElemAkantu<_Dim>::getEKin() {
  return code_unit_system.mvv2e *
         model->getEnergy("kinetic", this->dofs->getType(), index);
}
/* -------------------------------------------------------------------------- */

template class RefElemAkantu<1u>;
template class RefElemAkantu<2u>;
template class RefElemAkantu<3u>;

__END_LIBMULTISCALE__
