/**
 * @file   akantu_dof_wrapper.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Jul 22 14:47:56 2014
 *
 * @brief  This is a helper class to manage Akantu models
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "akantu_dof_wrapper.hh"
#include "lm_common.hh"
#include <solid_mechanics_model.hh>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

template <UInt Dim>
AkantuDOFWrapper<Dim>::AkantuDOFWrapper()
    : solid_mechanics_model(NULL), material_per_element(NULL),
      material_local_numbering(NULL), connectivity(NULL) {

  solid_mechanics_model = NULL;
#ifdef AKANTU_HEAT_TRANSFER
  // heat_transfer_model = NULL;
#endif
}

/* -------------------------------------------------------------------------- */
template <UInt Dim> AkantuDOFWrapper<Dim>::~AkantuDOFWrapper() {}

/* -------------------------------------------------------------------------- */

template <UInt Dim> akantu::Array<Real> &AkantuDOFWrapper<Dim>::getForce() {
  return *this->force;
}
/* -------------------------------------------------------------------------- */

// template <UInt Dim>
// void AkantuDOFWrapper<Dim>::setForce(akantu::Array<Real> &force) {
//   this->force = &force;
// }

// /* --------------------------------------------------------------------------
// */
template <UInt Dim>
const akantu::Array<Real> &
AkantuDOFWrapper<Dim>::getGradU(UInt material_index) {
  return *this->grad_u[material_index];
}

/* -------------------------------------------------------------------------- */
template <UInt Dim>
const akantu::Array<Real> &
AkantuDOFWrapper<Dim>::getStress(UInt material_index) {
  return *this->stresses[material_index];
}

/* -------------------------------------------------------------------------- */
template <UInt Dim>
void AkantuDOFWrapper<Dim>::printself(std::ostream &stream) const {
  stream << "AkantuDOFWrapper: " << this << " " << this->nb_nodes_per_element;
}

/* -------------------------------------------------------------------------- */

template <UInt Dim>
void AkantuDOFWrapper<Dim>::setSolidMechanicsModel(
    akantu::SolidMechanicsModel &model, akantu::Array<Real> &force) {
  this->force = &force;
  this->solid_mechanics_model = &model;
  akantu::Mesh &mesh = model.getMesh();
  this->type = *mesh.elementTypes(Dim).begin();
  LM_ASSERT(mesh.elementTypes(Dim).begin() != mesh.elementTypes(Dim).end(),
            "There are no element of the right dimension in the mesh");

  if (this->type == akantu::_not_defined)
    LM_FATAL("The element type " << this->type
                                 << " coming from Akantu is not a valid type");

  this->material_per_element = &(model.getMaterialByElement(this->type));
  this->material_local_numbering =
      &(model.getMaterialLocalNumbering(this->type));
  UInt nb_mat = model.getNbMaterials();
  for (UInt mat_id = 0; mat_id < nb_mat; ++mat_id) {
    akantu::Material &current_material = model.getMaterial(mat_id);
    this->grad_u.push_back(&current_material.getGradU(this->type));
    this->stresses.push_back(&current_material.getStress(this->type));
  }
  this->connectivity = mesh.getConnectivity(this->type).storage();
  this->nb_nodes_per_element = mesh.getNbNodesPerElement(this->type);
  this->nb_quadrature_points =
      model.getFEEngine().getNbIntegrationPoints(this->type);
}

/* -------------------------------------------------------------------------- */

template <UInt Dim>
akantu::SolidMechanicsModel &AkantuDOFWrapper<Dim>::getSolidMechanicsModel() {
  LM_ASSERT(solid_mechanics_model,
            "solid mechanics model was not correctly initialized");
  return *solid_mechanics_model;
}

/* -------------------------------------------------------------------------- */

#ifdef AKANTU_HEAT_TRANSFER
// template <UInt Dim>
// akantu::HeatTransferModel &AkantuDOFWrapper<Dim>::getHeatTransferModel() {
//   LM_ASSERT(heat_transfer_model,
//             "heat transfer model was not correctly initialized");
//   return *heat_transfer_model;
// }
#endif

/* -------------------------------------------------------------------------- */

template class AkantuDOFWrapper<1u>;
template class AkantuDOFWrapper<2u>;
template class AkantuDOFWrapper<3u>;

__END_LIBMULTISCALE__
