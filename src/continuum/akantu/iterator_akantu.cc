/**
 * @file   iterator_akantu.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Tue Jul 22 14:47:56 2014
 *
 * @brief  This is the generic iterator over Akantu DOFs
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "container_akantu.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

// template <UInt Dim>
// RefElemAkantu<Dim> &ContainerElemsAkantu<Dim>::iterator::getFirst() {

//   actual_nb_elem = this->my_cont.size();
//   if (actual_nb_elem == 0)
//     this->end_flag = true;
//   else {
//     ref.setAkantuPointers(
//         static_cast<ContainerElemsAkantu<Dim> &>(this->my_cont));
//     this->end_flag = false;
//   }

//   return ref;
// }

/* -------------------------------------------------------------------------- */
template <UInt Dim>
inline ContainerElemsAkantu<Dim>::iterator::iterator(
    ContainerElemsAkantu<Dim> &c)
    : iterator_base<RefElemAkantu<Dim>>(c), actual_nb_elem(c.size()) {

  // ref.setHeatTransferFlag(c.getHeatTransferFlag());
  ref.setAkantuPointers(static_cast<ContainerElemsAkantu<Dim> &>(c));
}

/* -------------------------------------------------------------------------- */

// template <UInt Dim>
// RefNodeAkantu<Dim> &ContainerNodesAkantu<Dim>::iterator::getFirst() {

//   actual_nb_elem = this->my_cont.size(dt_all);
//   if (actual_nb_elem == 0)
//     this->end_flag = true;
//   else {
//     ref.setAkantuPointers(
//         static_cast<ContainerNodesAkantu<Dim> &>(this->my_cont));
//     ref.index = 0;
//     this->end_flag = false;
//   }

//   while (!(this->dt & ref.getDOFType())) {
//     this->getNext();
//     if (this->end_flag)
//       break;
//   }

//   return ref;
// }

/* -------------------------------------------------------------------------- */

template <UInt Dim>
ContainerNodesAkantu<Dim>::iterator::iterator(ContainerNodesAkantu<Dim> &c)
    : iterator_base<RefNodeAkantu<Dim>>(c), actual_nb_elem(c.size()) {
  // ref.setHeatTransferFlag(c.getHeatTransferFlag());
  ref.setAkantuPointers(static_cast<ContainerNodesAkantu<Dim> &>(c));
  ref.index = 0;
}
/* -------------------------------------------------------------------------- */

template class ContainerNodesAkantu<3>;
template class ContainerElemsAkantu<3>;
template class ContainerNodesAkantu<2>;
template class ContainerElemsAkantu<2>;
template class ContainerNodesAkantu<1>;
template class ContainerElemsAkantu<1>;
__END_LIBMULTISCALE__
