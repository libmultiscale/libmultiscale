/**
 * @file   akantu_dof_wrapper.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Mon Jun 16 17:13:26 2014
 *
 * @brief  This is a helper class to manage Akantu models
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_AKANTU_DOF_WRAPPER_HH__
#define __LIBMULTISCALE_AKANTU_DOF_WRAPPER_HH__
/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
#include <solid_mechanics_model.hh>
/* -------------------------------------------------------------------------- */
namespace akantu {
#ifdef AKANTU_HEAT_TRANSFER
class HeatTransferModel;
#endif
} // namespace akantu
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

template <UInt Dim> class AkantuDOFWrapper {

public:
  using MyFEM = typename akantu::SolidMechanicsModel::MyFEEngineType;

  AkantuDOFWrapper();
  ~AkantuDOFWrapper();

  //! get solidmechanics model object
  akantu::SolidMechanicsModel &getSolidMechanicsModel();
  const akantu::SolidMechanicsModel &getSolidMechanicsModel() const;
  //! set solidmechanics model object
  virtual void setSolidMechanicsModel(akantu::SolidMechanicsModel &model,
                                      akantu::Array<Real> &force);
  //! set force vector
  // virtual void setForce(akantu::Array<Real> &force);
  //! get force vector
  akantu::Array<Real> &getForce();

#ifdef AKANTU_HEAT_TRANSFER
  //! get heattransfer model object
  // akantu::HeatTransferModel &getHeatTransferModel();
  //! set heattransfer model object
  // virtual void setHeatTransferModel(akantu::HeatTransferModel &model);
#endif
  //! return the initialization state
  bool isInitialized() const;

  //! return the element type
  const inline akantu::ElementType &getType() { return this->type; }

  inline UInt getNbNodesPerElem() { return this->nb_nodes_per_element; }
  inline UInt *getConnectivity() { return this->connectivity; }

  //! return the id of the material of the element index
  inline const UInt &getMaterialID(UInt index) {
    return (*this->material_per_element)(index);
  }
  //! return the id in its material of element index
  inline const UInt &getIDInMaterial(UInt index) {
    return (*this->material_local_numbering)(index);
  }

  inline const UInt &getNbQuadraturePoints() {
    return this->nb_quadrature_points;
  }

  const akantu::Array<Real> &getGradU(UInt material_index);
  const akantu::Array<Real> &getStress(UInt material_index);

  /// function to print the contain of the class
  virtual void printself(std::ostream &stream) const;

private:
  akantu::SolidMechanicsModel *solid_mechanics_model;
#ifdef AKANTU_HEAT_TRANSFER
  // akantu::HeatTransferModel *heat_transfer_model;
#endif
  //! element type in the mesh
  akantu::ElementType type;

  //! material id per element
  const akantu::Array<UInt> *material_per_element;
  //! gives the index used in material for each element
  const akantu::Array<UInt> *material_local_numbering;

  //! pointers of strain vectors ordered by material
  std::vector<const akantu::Array<Real> *> grad_u;
  //! pointers of stress vectors ordered by material
  std::vector<const akantu::Array<Real> *> stresses;

  //! pointers of stress vectors ordered by material
  akantu::Array<Real> *force;

  //! connectivity array
  UInt *connectivity;
  //! nb nodes per element
  UInt nb_nodes_per_element;
  UInt nb_quadrature_points;
};
/* -------------------------------------------------------------------------- */

template <UInt Dim>
inline std::ostream &operator<<(std::ostream &os, AkantuDOFWrapper<Dim> &q) {
  q.printself(os);
  return os;
}

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_AKANTU_DOF_WRAPPER_HH__ */
