/**
 * @file   ref_elem_akantu.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Tue Jun 24 22:58:02 2014
 *
 * @brief  This is a reference to Akantu elements
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_REF_ELEM_AKANTU_HH__
#define __LIBMULTISCALE_REF_ELEM_AKANTU_HH__
/* -------------------------------------------------------------------------- */
#include "akantu_dof_wrapper.hh"
#include "ref_elem_fe.hh"
#include "units.hh"
/* -------------------------------------------------------------------------- */
#include <integrator_gauss.hh>
#include <shape_lagrange.hh>
#include <solid_mechanics_model.hh>

/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

template <UInt Dim> class IteratorElemsAkantu;
template <UInt Dim> class ContainerElemsAkantu;
template <UInt Dim> class RefNodeAkantu;
/* -------------------------------------------------------------------------- */

/**
 * Class RefElementAkantu
 * implementation from Akantu of the generic reference to elements
 */

template <UInt _Dim>
class RefElemAkantu : public RefElemFE<_Dim, RefElemAkantu<_Dim>> {

public:
  friend class ContainerElemsAkantu<_Dim>;
  using MyFEM = typename akantu::SolidMechanicsModel::MyFEEngineType;
  static constexpr UInt Dim = _Dim;
  using fields = field_list<_stress, _strain>;

  RefElemAkantu();
  RefElemAkantu(UInt ind);

  //! equality operator
  bool operator==(RefElemAkantu<_Dim> &ref);
  //! return normal to element when dimension of element is <= 2
  void normal(Real &x, Real &y, Real &z);
  //! get integral of potential energy density of the element
  Real epot();
  //! get integral of kinetic energy of the element
  Real getEKin();
#ifdef AKANTU_HEAT_TRANSFER
  //! get integral of thermal energy of the element
  Real getEThermal();
#endif
  //! geometric ownership test of a given space poUInt for referenced element
  bool contains(Vector<_Dim> x);
  //! give the number of nodes connected in referenced element
  UInt nNodes();
  //! return the condensed (averaged) stress tensor component coord
  Matrix<_Dim> stress();
  //! return the condensed (averaged) strain tensor component coord
  Matrix<_Dim> strain();
  //! function that computes the nodal shape values for a given element at a
  //! given space point
  template <typename Vec>
  void computeShapes(std::vector<Real> &target, Vec &&x);
  //! return the local indexes (in DOF vector) for connected nodes
  std::vector<UInt> localIndexes();
  //! return the global indexes (in DOF vector) for connected nodes
  std::vector<UInt> globalIndexes();
  //! return a ref to the i-est node
  RefNodeAkantu<_Dim> &getNode(UInt i);

  Real length() { LM_TOIMPLEMENT; };

  /// function to print the contain of the class
  virtual void printself(std::ostream &stream) const;

protected:
  //! set internal index to provide direct acess
  void setIndex(UInt ind);
  //! set pointers to akantu objects
  void setAkantuPointers(AkantuDOFWrapper<_Dim> &dof);
  //! set heat transfer flag
  void setHeatTransferFlag(bool flag);
  //! get heat transfer flag
  bool getHeatTransferFlag();

private:
  //! internal index
  UInt index;

  //! direct access to the shape object (for interpolation and other things)
  MyFEM *fem;
  //! direct acces to the dof-wrapper
  AkantuDOFWrapper<_Dim> *dofs;
  //! direct access to the shape object (for interpolation and other things)
  akantu::SolidMechanicsModel *model;
#ifdef AKANTU_HEAT_TRANSFER
  // direct access to the heat transfer model
  akantu::HeatTransferModel *heat_transfer_model;
#endif // AKANTU_HEAT_TRANSFER

  RefNodeAkantu<_Dim> nd;
};

/* -------------------------------------------------------------------------- */

template <UInt _Dim>
inline RefElemAkantu<_Dim>::RefElemAkantu()
    : index(0), dofs(nullptr), model(nullptr) {}
/* -------------------------------------------------------------------------- */

template <UInt _Dim> inline RefElemAkantu<_Dim>::RefElemAkantu(UInt) {
  LM_TOIMPLEMENT;
}

/* -------------------------------------------------------------------------- */

template <UInt _Dim>
inline bool RefElemAkantu<_Dim>::operator==(RefElemAkantu<_Dim> &) {
  LM_TOIMPLEMENT;
}
/* -------------------------------------------------------------------------- */

template <UInt _Dim>
inline void RefElemAkantu<_Dim>::normal(Real &, Real &, Real &) {
  LM_TOIMPLEMENT;
}
/* -------------------------------------------------------------------------- */

template <UInt _Dim> inline UInt RefElemAkantu<_Dim>::nNodes() {
  return this->dofs->getNbNodesPerElem();
}

/* -------------------------------------------------------------------------- */
template <UInt _Dim> inline Matrix<_Dim> RefElemAkantu<_Dim>::stress() {
  UInt mat_id = this->dofs->getMaterialID(this->index);
  UInt id_in_mat = this->dofs->getIDInMaterial(this->index);
  UInt nb_quadrature_points = this->dofs->getNbQuadraturePoints();

  const akantu::Array<Real> &stress = this->dofs->getStress(mat_id);
  akantu::Matrix<Real> retval(Dim, Dim);
  retval.zero();
  auto it = akantu::make_view(stress, Dim, Dim).begin();
  for (UInt quad_pt = 0; quad_pt < nb_quadrature_points; ++quad_pt) {
    akantu::Matrix<Real> _stress =
        it[nb_quadrature_points * id_in_mat + quad_pt];
    retval += _stress;
  }
  retval /= nb_quadrature_points;
  return MatrixView<_Dim>(retval.storage());
}

/* -------------------------------------------------------------------------- */
template <UInt _Dim> inline Matrix<_Dim> RefElemAkantu<_Dim>::strain() {
  UInt mat_id = this->dofs->getMaterialID(this->index);
  UInt id_in_mat = this->dofs->getIDInMaterial(this->index);
  UInt nb_quadrature_points = this->dofs->getNbQuadraturePoints();

  const akantu::Array<Real> &strain = this->dofs->getGradU(mat_id);
  akantu::Matrix<Real> retval(Dim, Dim);
  retval.zero();
  auto it = akantu::make_view(strain, Dim, Dim).begin();
  for (UInt quad_pt = 0; quad_pt < nb_quadrature_points; ++quad_pt) {
    akantu::Matrix<Real> _grad_u =
        it[nb_quadrature_points * id_in_mat + quad_pt];
    retval += .5 * (_grad_u + _grad_u.transpose());
  }
  retval /= nb_quadrature_points;
  return MatrixView<_Dim>(retval.storage());
}
/* -------------------------------------------------------------------------- */

template <UInt _Dim>
template <typename Vec>
inline void RefElemAkantu<_Dim>::computeShapes(std::vector<Real> &target,
                                               Vec &&X) {
  target.resize(this->dofs->getNbNodesPerElem());
  akantu::RVector shapes(&target[0], this->dofs->getNbNodesPerElem());

  akantu::RVector real_coords(_Dim);
  for (UInt i = 0; i < _Dim; ++i)
    real_coords[i] = X[i];
  return fem->computeShapes(real_coords, index, this->dofs->getType(), shapes);
}

/* -------------------------------------------------------------------------- */

template <UInt _Dim> inline bool RefElemAkantu<_Dim>::contains(Vector<_Dim> x) {
  akantu::RVector real_coords(_Dim);
  for (UInt i = 0; i < _Dim; ++i)
    real_coords[i] = x[i];

  return fem->contains(real_coords, index, this->dofs->getType());
}

/* -------------------------------------------------------------------------- */

template <UInt _Dim>
inline std::vector<UInt> RefElemAkantu<_Dim>::localIndexes() {
  LM_ASSERT(this->dofs->getNbNodesPerElem(),
            "ref to akantu element was badly initialized");

  std::vector<UInt> nodes;
  nodes.resize(this->dofs->getNbNodesPerElem());
  for (UInt i = 0; i < this->dofs->getNbNodesPerElem(); ++i) {
    nodes[i] =
        this->dofs
            ->getConnectivity()[this->dofs->getNbNodesPerElem() * index + i];
  }
  return nodes;
}
/* -------------------------------------------------------------------------- */

template <UInt _Dim>
inline std::vector<UInt> RefElemAkantu<_Dim>::globalIndexes() {
  return localIndexes();
}
/* -------------------------------------------------------------------------- */

template <UInt _Dim>
inline RefNodeAkantu<_Dim> &RefElemAkantu<_Dim>::getNode(UInt i) {
  nd.index =
      this->dofs
          ->getConnectivity()[this->dofs->getNbNodesPerElem() * index + i];
  return nd;
}

/* -------------------------------------------------------------------------- */

template <UInt _Dim>
inline void RefElemAkantu<_Dim>::setHeatTransferFlag(bool flag) {
  nd.setHeatTransferFlag(flag);
}

/* -------------------------------------------------------------------------- */

template <UInt _Dim> inline bool RefElemAkantu<_Dim>::getHeatTransferFlag() {
  return nd.getHeatTransferFlag();
}

/* -------------------------------------------------------------------------- */

template <UInt Dim>
void RefElemAkantu<Dim>::printself(std::ostream &stream) const {
  stream << "RefElemAkantu: " << this->index << " " << *(this->dofs);
}

/* -------------------------------------------------------------------------- */

template <UInt _Dim>
inline std::ostream &operator<<(std::ostream &os, RefElemAkantu<_Dim> &ref) {
  ref.printself(os);
  return os;
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_REF_ELEM_AKANTU_HH__ */
