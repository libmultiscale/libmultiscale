/**
 * @file   ref_node_akantu.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Tue Jul 22 14:47:56 2014
 *
 * @brief  This is the generic reference over Akantu nodes
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_REF_NODE_AKANTU_HH__
#define __LIBMULTISCALE_REF_NODE_AKANTU_HH__
/* -------------------------------------------------------------------------- */
#include "mesh.hh"
#include "ref_node_continuum.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
template <UInt Dim> class AkantuDOFWrapper;
/* -------------------------------------------------------------------------- */
template <UInt Dim> class IteratorNodesAkantu;
template <UInt Dim> class DomainAkantu;
template <UInt Dim> class ContainerNodesAkantu;
template <UInt Dim> class RefElemAkantu;
class ComparatorRefNodeAkantu;
/* -------------------------------------------------------------------------- */

/**
 * Class RefNodeAkantu
 */
template <UInt _Dim>
class RefNodeAkantu : public RefNode<_Dim, RefNodeAkantu<_Dim>> {

public:
  static constexpr UInt Dim = _Dim;

  //! generic container of the original model container
  using Domain = DomainAkantu<_Dim>;
  //! node comparator class to be usable in maps
  using RefComparator = ComparatorRefNodeAkantu;
  //! accessible fields
  using fields =
      field_list<_position0, _position, _displacement, _velocity, _force,
                 _internal_force, _external_force, _mass, _boundary>;

  //! iterators need to access model field
  friend class IteratorNodesAkantu<_Dim>;
  //! containers need to access index field
  friend class ContainerNodesAkantu<_Dim>;
  //! elem reference need access
  friend class RefElemAkantu<_Dim>;

  RefNodeAkantu();
  RefNodeAkantu(UInt ind);

  bool operator==(RefNodeAkantu<_Dim> &ref);
  //! return residual value for dimention coord
  VectorView<_Dim> force();
  //! return residual value for dimention coord
  VectorView<_Dim> internal_force();
  //! return residual value for dimention coord
  VectorView<_Dim> external_force();
  //! return acceleration value for dimention coord
  VectorView<_Dim> acceleration();
  //! return initial position value for dimention coord
  VectorView<_Dim> position0();
  //! return initial position value for dimention coord
  AccessorNodalDof<RefNodeAkantu<_Dim>, _position> position();
  //! return displacement value for dimention coord
  VectorView<_Dim> displacement();
  //! return velocity value for dimention coord
  VectorView<_Dim> velocity();
  //! return mass value
  Real &mass();
#ifdef AKANTU_HEAT_TRANSFER
  //! return temperature
  VectorView<_Dim> temperature();
  //! return temperature variation value
  VectorView<_Dim> temperatureVar();
  //! return the external (applied) heat rate
  VectorView<_Dim> externalHeatRate();
  //! return the heat rate (residual)
  VectorView<_Dim> heatRate();
  //! return heat capacity
  VectorView<_Dim> capacity();
#endif
  //! read or set boundary value
  VectorView<_Dim, bool> boundary();
  //! return id of the degree of freedom
  UInt id();
  //! return the pointed node index
  UInt getIndex() { return index; };
  //! set pointers to akantu objects
  void setAkantuPointers(AkantuDOFWrapper<_Dim> &dof);
  //! set heat transfer flag
  void setHeatTransferFlag(bool flag);
  //! get heat transfer flag
  bool getHeatTransferFlag();
  //! return the dof_type
  int getDOFType();

private:
  //! pointer to displacement field
  Real *field_disp;
  //! pointer to velocity field
  Real *field_vel;
  //! pointer to total force field
  Real *field_force;
  //! pointer to mass field
  Real *field_mass;
  //! pointer to accel field
  Real *field_accel;
  //! pointer to internal force
  Real *field_internal_force;
  //! pointer to external force
  Real *field_external_force;
  //! pointer to initial position field
  Real *field_pos0;

  //! pointer to temperature field
  Real *field_temperature;
  //! pointer to temperatureVar field
  Real *field_temperatureVar;
  //! pointer to external heat rate field
  Real *field_external_heat_rate;
  //! pointer to heat rate field
  Real *field_heat_rate;
  //! pointer to heat capacity field
  Real *field_capacity;
  //! pointer to boundary field
  bool *field_boundary;
  // flag for heat transfer
  bool heat_transfer_flag;

  //! internal index
  UInt index;

  //! akantu mesh
  akantu::Mesh *mesh;
};

/* -------------------------------------------------------------------------- */

template <UInt _Dim>
inline RefNodeAkantu<_Dim>::RefNodeAkantu()
    : field_disp(NULL), field_vel(NULL), field_force(NULL), field_mass(NULL),
      field_accel(NULL), field_internal_force(NULL), field_external_force(NULL),
      field_pos0(NULL), field_temperature(NULL), field_temperatureVar(NULL),
      field_external_heat_rate(NULL), field_heat_rate(NULL),
      field_capacity(NULL), field_boundary(NULL), heat_transfer_flag(false),
      mesh(NULL) {}

/* -------------------------------------------------------------------------- */

template <UInt _Dim>
inline bool RefNodeAkantu<_Dim>::operator==(RefNodeAkantu<_Dim> &ref) {
  return (ref.index == index);
}

/* -------------------------------------------------------------------------- */

template <UInt Dim> inline VectorView<Dim> RefNodeAkantu<Dim>::force() {
  return VectorView<Dim>(field_force + Dim * index);
}
/* -------------------------------------------------------------------------- */

template <UInt Dim>
inline VectorView<Dim> RefNodeAkantu<Dim>::internal_force() {
  return VectorView<Dim>(field_internal_force + Dim * index);
}
/* -------------------------------------------------------------------------- */
template <UInt Dim>
inline VectorView<Dim> RefNodeAkantu<Dim>::external_force() {
  return VectorView<Dim>(field_external_force + Dim * index);
}
/* -------------------------------------------------------------------------- */

template <UInt Dim> inline VectorView<Dim> RefNodeAkantu<Dim>::acceleration() {
  return VectorView<Dim>(field_accel + Dim * index);
}
/* -------------------------------------------------------------------------- */

template <UInt Dim> inline VectorView<Dim> RefNodeAkantu<Dim>::position0() {
  return VectorView<Dim>(field_pos0 + Dim * index);
}
/* -------------------------------------------------------------------------- */

template <UInt _Dim>
AccessorNodalDof<RefNodeAkantu<_Dim>, _position>
RefNodeAkantu<_Dim>::position() {
  return AccessorNodalDof<RefNodeAkantu<_Dim>, _position>(*this);
}
/* -------------------------------------------------------------------------- */

template <UInt Dim> inline VectorView<Dim> RefNodeAkantu<Dim>::displacement() {
  return VectorView<Dim>(field_disp + Dim * index);
}
/* -------------------------------------------------------------------------- */

template <UInt Dim> inline VectorView<Dim> RefNodeAkantu<Dim>::velocity() {
  return VectorView<Dim>(field_vel + Dim * index);
}
/* -------------------------------------------------------------------------- */

template <UInt Dim> inline Real &RefNodeAkantu<Dim>::mass() {
  return field_mass[Dim * index];
}
/* -------------------------------------------------------------------------- */

#ifdef AKANTU_HEAT_TRANSFER
template <UInt Dim> inline VectorView<Dim> RefNodeAkantu<Dim>::temperature() {
  LM_TOIMPLEMENT;
  // LM_ASSERT(field_temperature, "uninitialized field");
  // return field_temperature[index];
}
/* -------------------------------------------------------------------------- */

template <UInt Dim>
inline VectorView<Dim> RefNodeAkantu<Dim>::temperatureVar() {
  LM_TOIMPLEMENT;
  // LM_ASSERT(field_temperatureVar, "uninitialized field");
  // return field_temperatureVar[index];
}
/* -------------------------------------------------------------------------- */

template <UInt Dim>
inline VectorView<Dim> RefNodeAkantu<Dim>::externalHeatRate() {
  LM_TOIMPLEMENT;
  // LM_ASSERT(field_external_heat_rate, "uninitialized field");
  // return field_external_heat_rate[index];
}

/* -------------------------------------------------------------------------- */

template <UInt Dim> inline VectorView<Dim> RefNodeAkantu<Dim>::heatRate() {
  LM_TOIMPLEMENT;
  // LM_ASSERT(field_heat_rate, "uninitialized field");
  // return field_heat_rate[index];
}

/* -------------------------------------------------------------------------- */

template <UInt Dim> inline VectorView<Dim> RefNodeAkantu<Dim>::capacity() {
  LM_TOIMPLEMENT;
  // LM_ASSERT(field_capacity, "uninitialized field");
  // return field_capacity[index];
}
#endif
/* -------------------------------------------------------------------------- */

template <UInt Dim> inline UInt RefNodeAkantu<Dim>::id() { LM_TOIMPLEMENT; }
/* -------------------------------------------------------------------------- */

template <UInt Dim>
inline VectorView<Dim, bool> RefNodeAkantu<Dim>::boundary() {
  return VectorView<Dim, bool>(field_boundary + Dim * index);
}
/* -------------------------------------------------------------------------- */

template <UInt _Dim>
inline std::ostream &operator<<(std::ostream &os, RefNodeAkantu<_Dim> &ref) {
  UInt n = ref.getIndex();

  os << "Akantu Node reference : " << n << " [P] ";
  os << " " << ref.position();
  os << " [P0] ";
  os << " " << ref.position0();
  os << " [V] ";
  os << " " << ref.velocity();
  os << " [F] ";
  os << " " << ref.force();

  return os;
}
/* -------------------------------------------------------------------------- */

template <UInt _Dim>
inline void RefNodeAkantu<_Dim>::setHeatTransferFlag(bool flag) {
  heat_transfer_flag = flag;
}

/* -------------------------------------------------------------------------- */

template <UInt _Dim> inline bool RefNodeAkantu<_Dim>::getHeatTransferFlag() {
  return heat_transfer_flag;
}

/* -------------------------------------------------------------------------- */
template <UInt _Dim> inline int RefNodeAkantu<_Dim>::getDOFType() {
  int dt = 0;

  if (mesh->isLocalOrMasterNode(index))
    dt |= dt_master;
  if (mesh->isSlaveNode(index) || mesh->isPureGhostNode(index))
    dt |= dt_slave;
  if (mesh->isPeriodicMaster(index))
    dt |= dt_pbc_master;
  if (mesh->isPeriodicSlave(index))
    dt |= dt_pbc_slave;
  return dt;
}

/* -------------------------------------------------------------------------- */

class ComparatorRefNodeAkantu {
public:
  template <UInt _Dim>
  bool operator()(RefNodeAkantu<_Dim> &, RefNodeAkantu<_Dim> &) const {
    //    return r1.index_atome < r2.index_atome;
    LM_TOIMPLEMENT;
  }

  template <UInt _Dim>
  bool operator()(const RefNodeAkantu<_Dim> &,
                  const RefNodeAkantu<_Dim> &) const {
    LM_TOIMPLEMENT;
  }
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_REF_NODE_AKANTU_HH__ */
