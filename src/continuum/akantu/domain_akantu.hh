/**
 * @file   domain_akantu.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Mon Jul 21 10:32:33 2014
 *
 * @brief  This is the model wrapping Akantu
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_DOMAIN_AKANTU_HH__
#define __LIBMULTISCALE_DOMAIN_AKANTU_HH__
/* -------------------------------------------------------------------------- */
#include "container_akantu.hh"
#include "domain_continuum.hh"
#include "parser.hh"
#include "ref_elem_akantu.hh"
/* -------------------------------------------------------------------------- */
#include <memory>
/* -------------------------------------------------------------------------- */
namespace akantu {
class Mesh;
class SolidMechanicsModel;
class HeatTransferModel;
} // namespace akantu
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/**
 * Class DomainAkantu
 */

template <UInt Dim>
class DomainAkantu : public DomainContinuum<ContainerElemsAkantu<Dim>,
                                            ContainerNodesAkantu<Dim>, Dim> {

public:
  DomainAkantu(LMID ID, CommGroup GID);
  ~DomainAkantu();

  ArrayView getField(FieldType field_type) override;
  void init() override;
  void updateGradient() override;
  void updateAcceleration() override;
  ArrayView acceleration() override {
    return make_array_from_akantu(model->getAcceleration());
  };
  ArrayView gradient() override { return make_array_from_akantu(total_force); };
  ArrayView primal() override { return this->getField(_displacement); };
  ArrayView primalTimeDerivative() override {
    return this->getField(_velocity);
  };
  ArrayView mass() override {
    return make_array_from_akantu(model->getMass());
  };
  Real potentialEnergy() override;
  void enforceCompatibility() override;
  Cube getDomainBoundingBox() const override;
  Cube getSubDomainBoundingBox() const override;
  void setDomainBoundingBox(const Cube &) override;
  void setSubDomainBoundingBox(const Cube &) override;

  //! returns whether an md domain is perdiodic in direction dir
  virtual bool isPeriodic(UInt dir) { return this->pbc[dir]; }

protected:
  template <typename V> ArrayView make_array_from_akantu(V &v) {
    return ArrayView(v.storage(), v.size(), v.getNbComponent());
  }
  //! initialize the Akantu material objects
  void initMaterial();
  //! initialize the Akantu Mesh object
  void initMesh();
  //! scaling the mesh
  void scaleMesh();
  //! shifting the mesh
  void shiftMesh();

  //! get displacement vector
  auto &getExternalForce() { return model->getExternalForce(); };
  //! get displacement vector
  auto &getInternalForce() { return model->getInternalForce(); };

  //! \todo not yet implemented
  Real getEpot();
  //! set current step to a given value
  void setCurrentStep(UInt ts);
  //! set timestep to a given value
  void setTimeStep(Real ts);
  //! get current step to a given value
  UInt getCurrentStep();

  //! function for keyword parsing
  void declareParams();
  //! stores initial slave displacements
  // void buildSlaveDisplacements();
  void checkBoundaryInputSanity();
  void enableNeumanBndyConditions();

  inline bool haveToApplyNeumann() { return this->surface_tag != ""; }

protected:
  //! filename from which to load the mesh
  std::string mesh_filename;
  //! Akantu mesh object
  std::unique_ptr<akantu::Mesh> mesh;
  //! Akantu solidmechanics model
  std::unique_ptr<akantu::SolidMechanicsModel> model;
  //! filename from which to load the material object
  std::string material_filename;
  //! storage for pbc activation: one bool per direction
  UInt pbc[3];
  //! storage for scale factors: one real per direction
  Quantity<Length, 3> scale;
  //! storage for shift factors: one real per direction
  Quantity<Length, 3> shift;
  //! interatomic distance for the LJ linearization
  Quantity<Length> r0;
  //! cutoff radius for the LJ linearization
  Quantity<Length> rcut;
  //! epsilon energy parameter for the LJ linearization
  Quantity<Energy> epsilon;
  //! sigma distance parameter for the LJ linearization
  Quantity<Length> sigma;
  //! mass parameter for the LJ linearization
  Quantity<Mass> atomic_mass;
  //! for the 1d case parameter to control the mesh generation
  Quantity<Length> elem_size;

  /*! uniform traction or stress to be applied as traction (for traction
   *  boundary conditions)*/
  Quantity<Pressure, 3 * Dim> boundary_stress;
  /*! Surface tag for the boundary where traction should be applied (tags
   *  in the mesh have to repect gmsh convention: contiguous, starting at 1)
   */
  std::string surface_tag;
  //! starting time for application of boundary conditions
  UInt boundary_start;
  //! type of neumann boundary condition. can be neglected if not used
  bool boundary_is_traction;
  //! type of neumann boundary condition. can be neglected if not used
  bool boundary_is_stress;
  //! whether the neumann conditions have already been applied
  bool boundary_has_been_applied;

  //! akantu parser
  akantu::Parser akantu_parser;

  akantu::Array<Real> total_force;

  //-------------------------
  // old dynamic akantu
  //-------------------------

  //! Akantu heat model
  // akantu::HeatTransferModel *heat_transfer_model;
  //! filename from which to load the material object
  // std::string heat_transfer_material_filename;
  //! flag to switch heat transfer model
  // bool heat_transfer_flag;

  //! temperature
  // akantu::Array<Real> *temperature;
  //! temperature rate
  // akantu::Array<Real> *temperature_rate;
  //! heat flux
  // akantu::Array<Real> *heat_flux;

  //! dirichlet boundary geometry for heat model
  // LMID heat_boundary_geom;

  //! init heat transfer model
  // void initHeatTransfer();
  // //! get temperature
  // auto &getT() { return *temperature; };
  // //! get heat flux
  // auto &getHeatFlux() { return *heat_flux; };

  std::unique_ptr<akantu::Communicator> aka_comm;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_DOMAIN_AKANTU_HH__ */
