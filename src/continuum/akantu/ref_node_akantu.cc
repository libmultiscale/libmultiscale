/**
 * @file   ref_node_akantu.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Tue Jul 22 14:47:56 2014
 *
 * @brief  This is the generic reference over Akantu nodes
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ref_node_akantu.hh"
#include "akantu_dof_wrapper.hh"
#include "lm_common.hh"
#include <aka_common.hh>
#include <solid_mechanics_model.hh>
#ifdef AKANTU_HEAT_TRANSFER
#include <heat_transfer_model.hh>
#endif // AKANTU_HEAT_TRANSFER

/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

template <UInt _Dim>
void RefNodeAkantu<_Dim>::setAkantuPointers(AkantuDOFWrapper<_Dim> &dof) {

  akantu::SolidMechanicsModel &smodel = dof.getSolidMechanicsModel();
  this->field_disp = smodel.getDisplacement().storage();
  this->field_vel = smodel.getVelocity().storage();
  this->field_internal_force = smodel.getInternalForce().storage();
  this->field_force = dof.getForce().storage();
  if (smodel.getAnalysisMethod() == akantu::_explicit_lumped_mass) {
    smodel.assembleMassLumped();
    this->field_mass = smodel.getMass().storage();
  }
  this->field_accel = smodel.getAcceleration().storage();
  this->field_external_force = smodel.getExternalForce().storage();
  this->field_pos0 = smodel.getFEEngine().getMesh().getNodes().storage();
  this->field_boundary = smodel.getBlockedDOFs().storage();

  this->mesh = &smodel.getFEEngine().getMesh();

#ifdef AKANTU_HEAT_TRANSFER
  // if (heat_transfer_flag) {
  //   akantu::HeatTransferModel &heat_transfer_model =
  //   dof.getHeatTransferModel(); this->field_temperature =
  //   heat_transfer_model.getTemperature().storage();
  //   this->field_temperatureVar =
  //       heat_transfer_model.getTemperatureRate().storage();
  //   this->field_external_heat_rate =
  //       heat_transfer_model.getExternalHeatRate().storage();
  //   this->field_capacity = heat_transfer_model.getCapacityLumped().storage();
  //   this->field_heat_rate = heat_transfer_model.getResidual().storage();
  // }
#endif
}
/* -------------------------------------------------------------------------- */

template class RefNodeAkantu<1u>;
template class RefNodeAkantu<2u>;
template class RefNodeAkantu<3u>;

__END_LIBMULTISCALE__
