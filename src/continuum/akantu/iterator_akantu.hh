/**
 * @file   iterator_akantu.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Jul 29 17:03:26 2014
 *
 * @brief  This is the generic iterator over Akantu DOFs
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_ITERATOR_AKANTU_HH__
#define __LIBMULTISCALE_ITERATOR_AKANTU_HH__
/* -------------------------------------------------------------------------- */
#include "container_akantu.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

template <UInt Dim>
class ContainerElemsAkantu<Dim>::iterator
    : public iterator_base<RefElemAkantu<Dim>> {

public:
  friend class ContainerElemsAkantu<Dim>;
  iterator(ContainerElemsAkantu<Dim> &c);

  iterator_base<RefElemAkantu<Dim>> &operator++() {
    ++ref.index;
    return *this;
  };
  RefElemAkantu<Dim> &operator*() { return ref; };

  virtual bool
  operator!=(const iterator_base<RefElemAkantu<Dim>> &it) const override {
    return ref.index != static_cast<const iterator &>(it).ref.index;
  };
  virtual bool
  operator==(const iterator_base<RefElemAkantu<Dim>> &it) const override {
    return ref.index == static_cast<const iterator &>(it).ref.index;
  };

private:
  //! ref object that will be returned by the iterator
  RefElemAkantu<Dim> ref;
  //! actual number of nodes
  UInt actual_nb_elem;
};

/* -------------------------------------------------------------------------- */

template <UInt _Dim>
class ContainerNodesAkantu<_Dim>::iterator
    : public iterator_base<RefNodeAkantu<_Dim>> {

public:
  friend class ContainerNodesAkantu<Dim>;
  iterator(ContainerNodesAkantu<Dim> &c);

  iterator_base<RefNodeAkantu<_Dim>> &operator++() {
    ++ref.index;
    return *this;
  }
  RefNodeAkantu<_Dim> &operator*() { return ref; };

  virtual bool
  operator!=(const iterator_base<RefNodeAkantu<_Dim>> &it) const override {
    return ref.index != static_cast<const iterator &>(it).ref.index;
  };
  virtual bool
  operator==(const iterator_base<RefNodeAkantu<_Dim>> &it) const override {
    return ref.index == static_cast<const iterator &>(it).ref.index;
  };

private:
  //! reference object that will be returned during iterations
  RefNodeAkantu<Dim> ref;
  //! actual number of nodes
  UInt actual_nb_elem;
};
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_ITERATOR_AKANTU_HH__ */
