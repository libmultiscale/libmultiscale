/**
 * @file   container_akantu.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 * @author Nicolas Richart <nicolas.richart@epfl.ch>
 *
 * @date   Tue Jul 22 14:47:56 2014
 *
 * @brief  This is the generic container to Akantu DOFs
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_CONTAINER_AKANTU_HH__
#define __LIBMULTISCALE_CONTAINER_AKANTU_HH__
/* -------------------------------------------------------------------------- */
#include "container.hh"
#include "ref_elem_akantu.hh"
#include "ref_node_akantu.hh"
/* -------------------------------------------------------------------------- */
#include "akantu_dof_wrapper.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

template <UInt Dim>
inline akantu::SolidMechanicsModel &
AkantuDOFWrapper<Dim>::getSolidMechanicsModel() {
  LM_ASSERT(solid_mechanics_model,
            "solid mechanics model was not correctly initialized");
  return *solid_mechanics_model;
}
/* -------------------------------------------------------------------------- */
template <UInt Dim>
inline const akantu::SolidMechanicsModel &
AkantuDOFWrapper<Dim>::getSolidMechanicsModel() const {
  LM_ASSERT(solid_mechanics_model,
            "solid mechanics model was not correctly initialized");
  return *solid_mechanics_model;
}
/* -------------------------------------------------------------------------- */

#ifdef AKANTU_HEAT_TRANSFER
// template <UInt Dim>
// inline akantu::HeatTransferModel &
// AkantuDOFWrapper<Dim>::getHeatTransferModel() {
//   LM_ASSERT(heat_transfer_model,
//             "heat transfer model was not correctly initialized");
//   return *heat_transfer_model;
// }
#endif
/* -------------------------------------------------------------------------- */

#ifdef AKANTU_HEAT_TRANSFER
// template <UInt Dim>
// inline void
// AkantuDOFWrapper<Dim>::setHeatTransferModel(akantu::HeatTransferModel &model)
// {
//   this->heat_transfer_model = &model;
// }
#endif
/* -------------------------------------------------------------------------- */

template <UInt Dim> inline bool AkantuDOFWrapper<Dim>::isInitialized() const {
  if (solid_mechanics_model)
    return true;
#ifdef AKANTU_HEAT_TRANSFER
    // if (heat_transfer_model)
    //   return true;
#endif
  return false;
}

/* -------------------------------------------------------------------------- */

template <UInt Dim>
class ContainerElemsAkantu : public Container_base<RefElemAkantu<Dim>>,
                             public AkantuDOFWrapper<Dim> {

public:
  class iterator;
  using Ref = RefElemAkantu<Dim>;

public:
  ContainerElemsAkantu(const LMID &id);
  ~ContainerElemsAkantu();

  //! return an associated iterator
  iterator begin();
  //! return an associated iterator
  iterator end();
  //! get an item from its index
  RefElemAkantu<Dim> &get(UInt index);
  //! return the number of contained items
  UInt size() const;
  //! function that clear the container
  void empty();
  //! will initialize the local solid mechanics reference when this function is
  //! to be called
  void setSolidMechanicsModel(akantu::SolidMechanicsModel &model,
                              akantu::Array<Real> &force);
#ifdef AKANTU_HEAT_TRANSFER
  //! will initialize the local heat transfer reference when this function is to
  //! be called
  // void setHeatTransferModel(akantu::HeatTransferModel &model);
#endif
  //! will set the reference
  void setRef();
  //! set heat transfer flag
  // void setHeatTransferFlag(bool flag);
  //! return heat transfer flag
  // bool getHeatTransferFlag();

protected:
  Ref ref;
};

/* -------------------------------------------------------------------------- */
template <UInt _Dim>
class ContainerNodesAkantu : public Container_base<RefNodeAkantu<_Dim>>,
                             public AkantuDOFWrapper<_Dim> {

public:
  class iterator;

  using Ref = RefNodeAkantu<_Dim>;
  static const UInt Dim = _Dim;

  ContainerNodesAkantu(const LMID &id);
  ~ContainerNodesAkantu();

  decltype(auto) filter(int dt) {
    return Container_base<Ref>::template filter<ContainerNodesAkantu>(dt);
  }

  //! return an associated iterator
  iterator begin();
  //! return an associated iterator
  iterator end();
  //! function that clear the container
  void empty();
  //! will initialize the local reference when this function is to be called
  void setSolidMechanicsModel(akantu::SolidMechanicsModel &model,
                              akantu::Array<Real> &force);
#ifdef AKANTU_HEAT_TRANSFER
  //! will initialize the local heat transfer reference when this function is
  //! to be called
  // void setHeatTransferModel(akantu::HeatTransferModel &model);
#endif
  //! set heat transfer flag
  // void setHeatTransferFlag(bool flag);
  //! return heat transfer flag
  // bool getHeatTransferFlag();

  //! will set the reference
  // void setRef();

  //! get an item from its index
  Ref &get(UInt index);
  //! return the number of contained items
  UInt size() const;

protected:
  //! reference to be returned in the case of a get call
  Ref ref;
};

__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
#include "iterator_akantu.hh"
/* -------------------------------------------------------------------------- */
#endif /* __LIBMULTISCALE_CONTAINER_AKANTU_HH__ */
