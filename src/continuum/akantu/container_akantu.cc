/**
 * @file   container_akantu.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Tue Jul 22 14:47:56 2014
 *
 * @brief  This is the generic container to Akantu DOFs
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "container_akantu.hh"
#include "lm_common.hh"
#include <solid_mechanics_model.hh>
#ifdef AKANTU_HEAT_TRANSFER
#include <heat_transfer_model.hh>
#endif // AKANTU_HEAT_TRANSFER

/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

template <UInt _Dim>
RefElemAkantu<_Dim> &ContainerElemsAkantu<_Dim>::get(UInt index) {
  ref.index = index;
  return ref;
}
/* -------------------------------------------------------------------------- */

template <UInt _Dim> UInt ContainerElemsAkantu<_Dim>::size() const {
  if (!this->isInitialized())
    return 0;

  auto &&model = this->getSolidMechanicsModel();
  auto &&mesh = model.getFEEngine().getMesh();
  return mesh.getNbElement(_Dim);
}

/* -------------------------------------------------------------------------- */

template <UInt _Dim>
void ContainerNodesAkantu<_Dim>::setSolidMechanicsModel(
    akantu::SolidMechanicsModel &model, akantu::Array<Real> &force) {
  AkantuDOFWrapper<_Dim>::setSolidMechanicsModel(model, force);
  ref.setAkantuPointers(*this);
  ref.index = 0;
}
/* -------------------------------------------------------------------------- */
template <UInt _Dim> UInt ContainerNodesAkantu<_Dim>::size() const {
  if (!this->isInitialized())
    return 0;
  auto &&model = this->getSolidMechanicsModel();
  auto &&mesh = model.getMesh();
  return mesh.getNbNodes();

  // UInt cpt = 0;

  // for (UInt n = 0; n < mesh.getNbNodes(); ++n) {
  //   if (mesh.isPureGhostNode(n)) {
  //     if (dt & dt_ghost)
  //       ++cpt;
  //   } else if (mesh.isLocalOrMasterNode(n)) {
  //     if (dt & dt_local_master or dt & dt_local)
  //       ++cpt;
  //   } else if (dt & dt_local_slave or dt & dt_local) {
  //     ++cpt;
  //   }
  // }

  // return cpt;
}
/* -------------------------------------------------------------------------- */

template <UInt _Dim>
ContainerElemsAkantu<_Dim>::ContainerElemsAkantu(const LMID &id)
    : LMObject(id) {}

/* -------------------------------------------------------------------------- */

template <UInt _Dim> ContainerElemsAkantu<_Dim>::~ContainerElemsAkantu() {}
/* -------------------------------------------------------------------------- */

template <UInt _Dim>
void ContainerElemsAkantu<_Dim>::setSolidMechanicsModel(
    akantu::SolidMechanicsModel &model, akantu::Array<Real> &force) {
  AkantuDOFWrapper<_Dim>::setSolidMechanicsModel(model, force);
  ref.setAkantuPointers(*this);
}
/* -------------------------------------------------------------------------- */
#ifdef AKANTU_HEAT_TRANSFER
// template <UInt _Dim>
// void ContainerElemsAkantu<_Dim>::setHeatTransferModel(
//     akantu::HeatTransferModel &model) {
//   AkantuDOFWrapper<_Dim>::setHeatTransferModel(model);
// }
#endif
/* -------------------------------------------------------------------------- */

template <UInt _Dim> void ContainerElemsAkantu<_Dim>::setRef() {
  ref.setAkantuPointers(*this);
}
/* -------------------------------------------------------------------------- */

// template <UInt _Dim>
// void ContainerElemsAkantu<_Dim>::setHeatTransferFlag(bool flag) {
//   ref.setHeatTransferFlag(flag);
// }
/* -------------------------------------------------------------------------- */

// template <UInt _Dim> bool ContainerElemsAkantu<_Dim>::getHeatTransferFlag() {
//   return ref.getHeatTransferFlag();
// }
/* -------------------------------------------------------------------------- */

template <UInt _Dim>
typename ContainerElemsAkantu<_Dim>::iterator
ContainerElemsAkantu<_Dim>::begin() {

  typename ContainerElemsAkantu<_Dim>::iterator it(*this);
  return it;
}

/* -------------------------------------------------------------------------- */

template <UInt _Dim>
typename ContainerElemsAkantu<_Dim>::iterator
ContainerElemsAkantu<_Dim>::end() {

  typename ContainerElemsAkantu<_Dim>::iterator it(*this);
  it.ref.index = this->size();
  return it;
}

/* -------------------------------------------------------------------------- */

template <UInt _Dim>
ContainerNodesAkantu<_Dim>::ContainerNodesAkantu(const LMID &id)
    : LMObject(id) {
  DUMP("This function still have to be implemented", DBG_DETAIL);
}
/* -------------------------------------------------------------------------- */
template <UInt _Dim> ContainerNodesAkantu<_Dim>::~ContainerNodesAkantu() {}
/* -------------------------------------------------------------------------- */
#ifdef AKANTU_HEAT_TRANSFER
// template <UInt _Dim>
// void ContainerNodesAkantu<_Dim>::setHeatTransferModel(
//     akantu::HeatTransferModel &model) {
//   AkantuDOFWrapper<_Dim>::setHeatTransferModel(model);
//   ref.field_temperature = model.getTemperature().storage();
//   ref.field_temperatureVar = model.getTemperatureRate().storage();
//   ref.field_external_heat_rate = model.getExternalHeatRate().storage();
//   ref.field_capacity = model.getCapacityLumped().storage();
//   ref.field_heat_rate = model.getResidual().storage();
//   ref.index = 0;
// }
#endif
/* -------------------------------------------------------------------------- */

template <UInt _Dim>
RefNodeAkantu<_Dim> &ContainerNodesAkantu<_Dim>::get(UInt index) {
  ref.index = index;
  return ref;
}
/* -------------------------------------------------------------------------- */

template <UInt _Dim>
typename ContainerNodesAkantu<_Dim>::iterator
ContainerNodesAkantu<_Dim>::begin() {
  typename ContainerNodesAkantu<_Dim>::iterator it(*this);
  return it;
}
/* -------------------------------------------------------------------------- */

template <UInt _Dim>
typename ContainerNodesAkantu<_Dim>::iterator
ContainerNodesAkantu<_Dim>::end() {
  typename ContainerNodesAkantu<_Dim>::iterator it(*this);
  it.ref.index = this->size();
  return it;
}
/* -------------------------------------------------------------------------- */

// template <UInt _Dim>
// void ContainerNodesAkantu<_Dim>::setHeatTransferFlag(bool flag) {
//   ref.setHeatTransferFlag(flag);
// }
// /* --------------------------------------------------------------------------
// */ template <UInt _Dim> bool
// ContainerNodesAkantu<_Dim>::getHeatTransferFlag() {
//   return ref.getHeatTransferFlag();
// }

/* -------------------------------------------------------------------------- */

template class ContainerNodesAkantu<1u>;
template class ContainerNodesAkantu<2u>;
template class ContainerNodesAkantu<3u>;

template class ContainerElemsAkantu<1u>;
template class ContainerElemsAkantu<2u>;
template class ContainerElemsAkantu<3u>;

__END_LIBMULTISCALE__
