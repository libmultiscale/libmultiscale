/**
 * @file   domain_continuum.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Tue Oct 29 22:09:05 2013
 *
 * @brief  Mother of all continuum domains
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_DOMAIN_CONTINUUM_HH__
#define __LIBMULTISCALE_DOMAIN_CONTINUUM_HH__
/* -------------------------------------------------------------------------- */
#include "container_mesh.hh"
#include "domain_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
/**
 Class DomainContinuum
 Template class from DomainContinuumInterface
 that helps developper to implement with optimisation
 processes by template a Continuous domain object
 */

template <typename ContElems, typename ContNodes, UInt Dim>
class DomainContinuum : public DomainInterface {

public:
  //! generic container type to course over nodes
  using ContainerNodes = ContNodes;
  //! generic iterator type to course over nodes
  using IteratorNodes = typename ContainerNodes::iterator;
  //! generic reference to nodes
  using RefNode = typename ContainerNodes::Ref;
  //! generic reference to nodes
  using RefPoint = typename ContainerNodes::Ref;
  //! generic reference to container of points (contains also the mesh)
  using ContainerPoints = ContainerMesh<ContNodes, ContElems>;
  //! generic container type to course over elems
  using ContainerElems = ContElems;
  //! generic iterator type to course over elems
  using IteratorElems = typename ContainerElems::iterator;
  //! generic reference to nodes
  using RefElem = typename ContainerElems::Ref;
  //! generic reference to container of points
  using ContainerSubset = typename ContainerPoints::ContainerSubset;

  DomainContinuum(CommGroup group);
  virtual ~DomainContinuum();

  void compute_make_call(){};
  //! return the container of elements
  ContainerElems &getContainerElems();
  //! return the container of nodes
  ContainerNodes &getContainerNodes();
  //! return the container of points
  ContainerPoints &getContainer();

  void build(){};

  //! implementation of the parsing method
  virtual void declareParams();
  //! reload xml file of a saved situation
  void readXMLFile(const std::string &filename);
  //! initiation function
  virtual void init() = 0;
  //! return PBC pairs in a vector
  std::vector<std::pair<UInt, UInt>> &getPBCpairs();

protected:
  //! flag to active arlequin energy weigthing
  bool arlequin_flag;
  //! container of mesh
  ContainerPoints mesh_container;
  //! mapping from zone id to geometries
  std::vector<LMID> geometries;
  //! contrained geom
  LMID geom_constrained;
  //! flag to activate correct handling of prestressed meshes in pbc
  bool prestressed_periodicity_flag;
  //! force scalar
  Real force;
  //! PBC pairs
  std::vector<std::pair<UInt, UInt>> pbc_pairs;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_DOMAIN_CONTINUUM_HH__ */
