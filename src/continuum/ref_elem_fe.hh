/**
 * @file   ref_elem_fe.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Sun May 26 17:30:22 2013
 *
 * @brief  Mother of all reference to element
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_REF_ELEM_FE_HH__
#define __LIBMULTISCALE_REF_ELEM_FE_HH__
/* -------------------------------------------------------------------------- */
#include "ref_element.hh"
#include "ref_node_continuum.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
template <UInt Dim, typename DaughterClass> class RefElemFE;
/* -------------------------------------------------------------------------- */
template <typename Ref, FieldType ftype>
struct AccessorElementalDof : public AccessorDof<Ref, ftype> {
  using AccessorDof<Ref, ftype>::AccessorDof;
  using AccessorDof<Ref, ftype>::operator=;
};
/* -------------------------------------------------------------------------- */

/**
 * Class RefElement
 */

template <UInt Dim, typename DaughterClass>
class RefElemFE : public RefElement<RefElemFE<Dim, DaughterClass>> {

public:
  RefElemFE() { this->is_altered = false; }
  virtual ~RefElemFE(){};

#define declare_accessor(_name)                                                \
  template <typename... Args> decltype(auto) _name(Args... args) {             \
    return static_cast<DaughterClass *>(this)->_name(args...);                 \
  }

  declare_accessor(stress) declare_accessor(strain) declare_accessor(getNode)
#undef declare_accessor

      //! returns true if element contains the provided point
      virtual bool contains(Vector<Dim> x) = 0;
  //! get integral of potential energy density of the element
  virtual Real epot() { LM_TOIMPLEMENT; };
  //! get integral of kinetic energy of the element
  virtual Real getEKin() { LM_TOIMPLEMENT; };
  //! get integral of thermal energy of the element
  virtual Real getEThermal() { LM_TOIMPLEMENT; };
  //! give the number of nodes connected in referenced element
  virtual UInt nNodes() = 0;
  //! given space point
  template <typename Vec>
  void computeShapes(std::vector<Real> &target, Vec &&X) {
    static_cast<DaughterClass &>(*this).computeShapes(target, X);
  }
  //! set heat transfer flag
  virtual void setHeatTransferFlag(bool) { LM_TOIMPLEMENT; };

  template <FieldType ftype>
  AccessorElementalDof<DaughterClass, ftype> field() {
    AccessorElementalDof<DaughterClass, ftype> acc(
        static_cast<DaughterClass &>(*this));
    return acc;
  }
  template <FieldType ftype> Real field(UInt) { LM_TOIMPLEMENT; }

  void test(){};

private:
  std::vector<UInt> altered_connectivity;

  bool is_altered;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_REF_ELEM_FE_HH__ */
