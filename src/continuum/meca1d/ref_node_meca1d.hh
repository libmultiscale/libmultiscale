/**
 * @file   ref_node_meca1d.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Sun Feb 02 17:45:17 2014
 *
 * @brief  Reference to nodal DOF in MECA1D
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_REF_NODE_MECA1D_HH__
#define __LIBMULTISCALE_REF_NODE_MECA1D_HH__
/* -------------------------------------------------------------------------- */
#include "mesh_meca1d.hh"
#include "ref_node_continuum.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class DomainMeca1D;
class ComparatorRefNodeMeca1D;
/* -------------------------------------------------------------------------- */

/**
 * Class RefNodeLibMesh
 * implementation by libmesh to generic access
 * to nodes
 */

class RefNodeMeca1D : public RefNode<1, RefNodeMeca1D> {

  /* ------------------------------------------------------------------------ */
  /* Typedefs                                                                 */
  /* ------------------------------------------------------------------------ */

public:
  using Domain = DomainMeca1D;
  using RefComparator = ComparatorRefNodeMeca1D;

  static constexpr UInt Dim = 1;

  using fields = field_list<_position0, _position, _displacement, _velocity,
                            _force, _mass, _acceleration>;

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  RefNodeMeca1D() : mesh(NULL){};

  bool operator==(RefNodeMeca1D &ref) { return (ref.index == index); };
  //! return the index to node. Should be used carrefully
  UInt getIndex() { return index; };
  // //! return temperature value
  // Real &temperature() { return mesh->T(index); };
  // //! return temperature variation value
  // Real &temperatureVar() { return mesh->Tdt(index); };
  // //! return the external heat rate value
  // Real &externalHeatRate() { return mesh->external_heat_rate(index); };
  // //! return the external heat rate value
  // Real &heatRate() { return mesh->heat_rate(index); };
  //! return force value for dimention coord
  VectorView<1> force() { return VectorView<1>(&mesh->f(index)); };
  //! return acceleration value for dimention coord
  VectorView<1> acceleration() { return VectorView<1>(&mesh->a(index)); };
  //! return initial position value for dimention coord
  VectorView<1> position0() { return VectorView<1>(&mesh->p0(index)); };
  //! return displacement value for dimension coord
  VectorView<1> displacement() { return VectorView<1>(&mesh->u(index)); };
  //! return actual position for dimension coord
  AccessorNodalDof<RefNodeMeca1D, _position> position() {
    return AccessorNodalDof<RefNodeMeca1D, _position>(*this);
  };
  //! return velocity value for dimention coord
  VectorView<1> velocity() { return VectorView<1>(&mesh->v(index)); };

  //! read or set boundary value
  bool &boundary(UInt) {
    this->boundary_dummy = false;
    DUMP("MECA1D doensn't know how to deal with boundaries! This does nothing.",
         DBG_WARNING);
    return this->boundary_dummy;
  }
  //! return mass value
  Real &mass() { return mesh->m(index); };

  UInt id() { return index; };

protected:
  //! iterators need to access model field
  friend class IteratorNodesMeca1D;
  //! containers need to access index field
  friend class ContainerNodesMeca1D;
  friend class RefElemMeca1D;

  //! set the index to node. Should be used carrefully
  void setIndex(UInt i) { index = i; };

  //! set the mesh pointer used to access the data
  void setMesh(std::shared_ptr<MeshMeca1D> ptr) { mesh = ptr; };

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

  //! pointer to the mesh
  std::shared_ptr<MeshMeca1D> mesh;
  //! index refereing to the current node
  UInt index;

  //! dummy boundary value
  bool boundary_dummy;
};

/* -------------------------------------------------------------------------- */

inline std::ostream &operator<<(std::ostream &os, RefNodeMeca1D &ref) {
  UInt n = ref.getIndex();
  os << "Meca1D Node reference : " << n;
  return os;
}

/* -------------------------------------------------------------------------- */

class ComparatorRefNodeMeca1D {
public:
  bool operator()(RefNodeMeca1D &, RefNodeMeca1D &) const { LM_TOIMPLEMENT; }

  bool operator()(const RefNodeMeca1D &, const RefNodeMeca1D &) const {
    LM_TOIMPLEMENT;
  }
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_REF_NODE_MECA1D_HH__ */
