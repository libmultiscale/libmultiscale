/**
 * @file   domain_meca1d.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Fri Jul 11 15:47:44 2014
 *
 * @brief  Domain for the 1D finite element engine of LibMultiScale
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_DOMAIN_MECA1D_HH__
#define __LIBMULTISCALE_DOMAIN_MECA1D_HH__
/* -------------------------------------------------------------------------- */
#include "container_meca1d.hh"
#include "domain_continuum.hh"
#include "mesh_meca1d.hh"
#include "ref_elem_meca1d.hh"
#include "ref_node_meca1d.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/**
 * Class DomainContinuum
 * Domain implementation by our 1D implementation
 */

class DomainMeca1D
    : public DomainContinuum<ContainerElemsMeca1D, ContainerNodesMeca1D, 1u> {

public:
  DomainMeca1D(LMID ID, CommGroup group)
      : LMObject(ID),
        DomainContinuum<ContainerElemsMeca1D, ContainerNodesMeca1D, 1u>(group),
        mymesh(NULL) {
    elastic_flag = false;
  }

  ArrayView getField(FieldType field_type) override;
  void init() override;
  void updateGradient() override;
  void updateAcceleration() override;
  ArrayView acceleration() override;
  ArrayView gradient() override;
  ArrayView primal() override;
  ArrayView primalTimeDerivative() override;
  ArrayView mass() override;
  Real potentialEnergy() override;
  void enforceCompatibility() override;
  Cube getDomainBoundingBox() const override;
  Cube getSubDomainBoundingBox() const override;
  void setDomainBoundingBox(const Cube &) override;
  void setSubDomainBoundingBox(const Cube &) override;

  //! returns whether an md domain is perdiodic in direction dir
  bool isPeriodic(UInt dir) override;

protected:
  //! implementation to allow each line to be parsed
  void declareParams() override;

private:
  UInt updateNodeForce(UInt i);

  //! associated mesh
  std::shared_ptr<MeshMeca1D> mymesh;
  //! cutoff radius for the cauchy born rule
  Quantity<Length> rcut;
  //! interatomic initial distance
  Quantity<Length> r0;
  //! sigma parameter for lennard jones
  Quantity<Length> sigma;
  //! epsilon parameter for lennard jones
  Quantity<Energy> epsilon;
  //! size of elements
  Quantity<Length> element_size;
  //! flag for elastic material
  bool elastic_flag;
  //! atomic mass
  Quantity<Mass> atomic_mass;

  //! file where to laod the nodal cooedinates
  std::string mesh_file;

  // //! flag for arlequin
  // bool arlequin_flag;
  // //! flag for activating thermal computations
  // bool thermal_flag;
  // //! thermal capacity
  // Real capacity;
  // //! thermal conductivity
  // Real conductivity;
};
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_DOMAIN_MECA1D_HH__ */
