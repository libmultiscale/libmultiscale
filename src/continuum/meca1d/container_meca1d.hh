/**
 * @file   container_meca1d.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Thu Jul 24 14:21:58 2014
 *
 * @brief  Container of DOFs for MECA1D plugin
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_CONTAINER_MECA1D_HH__
#define __LIBMULTISCALE_CONTAINER_MECA1D_HH__
/* -------------------------------------------------------------------------- */
#include "container.hh"
#include "mesh_meca1d.hh"
#include "ref_elem_meca1d.hh"
#include "ref_node_meca1d.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class ContainerElemsMeca1D : public Container_base<RefElemMeca1D> {

public:
  class iterator;
  friend class IteratorElemsMeca1D;
  static constexpr UInt Dim = 1;

public:
  ContainerElemsMeca1D(const LMID &id);
  ~ContainerElemsMeca1D();

  //! set the mesh pointer correctly
  void setMesh(std::shared_ptr<MeshMeca1D> ptr);
  //! return an associated iterator
  iterator begin();
  iterator end();
  //! get an item from its index
  RefElemMeca1D &get(UInt index) override;
  //! return the number of contained items
  UInt size() const override;
  //! empty the container
  void empty() { LM_TOIMPLEMENT; };

protected:
  std::shared_ptr<MeshMeca1D> mesh;
  RefElemMeca1D ref;
};
/* -------------------------------------------------------------------------- */

class ContainerNodesMeca1D : public Container_base<RefNodeMeca1D> {

public:
  class iterator;
  static constexpr UInt Dim = 1;

  ContainerNodesMeca1D(const LMID &id);
  ~ContainerNodesMeca1D();

  decltype(auto) filter(int dt) {
    return Container_base<RefNodeMeca1D>::template filter<ContainerNodesMeca1D>(
        dt);
  }

  //! set the mesh pointer correctly
  void setMesh(std::shared_ptr<MeshMeca1D> ptr);
  //! return an associated iterator
  iterator begin();
  iterator end();
  //! get an item from its index
  RefNodeMeca1D &get(UInt index) override;
  //! return the number of contained items
  UInt size() const override;
  //! empty the container
  void empty() { LM_TOIMPLEMENT; };

protected:
  std::shared_ptr<MeshMeca1D> mesh;
  RefNodeMeca1D ref;
};

__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
#include "iterator_meca1d.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

inline ContainerElemsMeca1D::ContainerElemsMeca1D(const LMID &id)
    : LMObject(id), Container_base<RefElemMeca1D>(), mesh(NULL) {}

/* -------------------------------------------------------------------------- */

inline ContainerElemsMeca1D::~ContainerElemsMeca1D() {}

/* -------------------------------------------------------------------------- */

inline void ContainerElemsMeca1D::setMesh(std::shared_ptr<MeshMeca1D> ptr) {
  if (!ptr)
    LM_FATAL("mesh pointer not initialized");
  mesh = ptr;
  ref.setMesh(mesh);
}

/* -------------------------------------------------------------------------- */

inline ContainerElemsMeca1D::iterator ContainerElemsMeca1D::begin() {
  ContainerElemsMeca1D::iterator it(*this);
  it.current_index = 0;
  it.ref.setIndex(it.current_index);
  it.ref.setMesh(mesh);
  return it;
}

/* -------------------------------------------------------------------------- */

inline ContainerElemsMeca1D::iterator ContainerElemsMeca1D::end() {
  ContainerElemsMeca1D::iterator it(*this);
  it.current_index = this->size();
  it.ref.setIndex(it.current_index);
  it.ref.setMesh(mesh);
  return it;
}

/* -------------------------------------------------------------------------- */

inline RefElemMeca1D &ContainerElemsMeca1D::get(UInt index) {
  ref.setIndex(index);
  return ref;
}

/* -------------------------------------------------------------------------- */

inline UInt ContainerElemsMeca1D::size() const {
  if (!mesh)
    return 0;
  return mesh->nbElements();
}

/* -------------------------------------------------------------------------- */

inline ContainerNodesMeca1D::ContainerNodesMeca1D(const LMID &id)
    : LMObject(id), Container_base<RefNodeMeca1D>(), mesh(NULL) {}

/* -------------------------------------------------------------------------- */

inline ContainerNodesMeca1D::~ContainerNodesMeca1D() {}

/* -------------------------------------------------------------------------- */

inline void ContainerNodesMeca1D::setMesh(std::shared_ptr<MeshMeca1D> ptr) {
  if (ptr == NULL)
    LM_FATAL("mesh pointer not initialized");
  mesh = ptr;
  ref.setMesh(mesh);
}

/* -------------------------------------------------------------------------- */

inline ContainerNodesMeca1D::iterator ContainerNodesMeca1D::begin() {
  ContainerNodesMeca1D::iterator it(*this);
  it.current_index = 0;
  it.ref.setIndex(it.current_index);
  it.ref.setMesh(mesh);
  return it;
}

/* -------------------------------------------------------------------------- */

inline ContainerNodesMeca1D::iterator ContainerNodesMeca1D::end() {
  ContainerNodesMeca1D::iterator it(*this);
  it.current_index = this->size();
  it.ref.setIndex(it.current_index);
  it.ref.setMesh(mesh);
  return it;
}

/* -------------------------------------------------------------------------- */

inline RefNodeMeca1D &ContainerNodesMeca1D::get(UInt index) {
  ref.setIndex(index);
  return ref;
}

/* -------------------------------------------------------------------------- */

inline UInt ContainerNodesMeca1D::size() const {
  if (!mesh)
    return 0;
  return mesh->nbNodes();
}

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_CONTAINER_MECA1D_HH__ */
