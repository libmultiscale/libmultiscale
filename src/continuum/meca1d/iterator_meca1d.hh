/**
 * @file   iterator_meca1d.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Jan 07 17:07:37 2013
 *
 * @brief  Iterator over the DOFs of MECA1D plugin
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_ITERATOR_MECA1D_HH__
#define __LIBMULTISCALE_ITERATOR_MECA1D_HH__

/* -------------------------------------------------------------------------- */
#include "container_meca1d.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class ContainerElemsMeca1D::iterator : public iterator_base<RefElemMeca1D> {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  iterator(ContainerElemsMeca1D &c) : iterator_base(c){};

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  iterator_base &operator++();
  RefElemMeca1D &operator*();

  virtual bool operator!=(const iterator_base<RefElemMeca1D> &) const override;
  virtual bool operator==(const iterator_base<RefElemMeca1D> &) const override;

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

  friend class ContainerElemsMeca1D;

private:
  RefElemMeca1D ref;
  UInt current_index;
};
/* -------------------------------------------------------------------------- */

inline RefElemMeca1D &ContainerElemsMeca1D::iterator::operator*() {
  return ref;
}

/* -------------------------------------------------------------------------- */

inline auto ContainerElemsMeca1D::iterator::operator++() -> iterator_base & {
  ++current_index;
  ref.setIndex(current_index);
  return *this;
}

/* -------------------------------------------------------------------------- */
inline bool ContainerElemsMeca1D::iterator::
operator!=(const iterator_base &it) const {
  return current_index !=
         static_cast<const ContainerElemsMeca1D::iterator &>(it).current_index;
}

/* -------------------------------------------------------------------------- */
inline bool ContainerElemsMeca1D::iterator::
operator==(const iterator_base &it) const {
  return current_index ==
         static_cast<const ContainerElemsMeca1D::iterator &>(it).current_index;
}

/* -------------------------------------------------------------------------- */

class ContainerNodesMeca1D::iterator : public iterator_base<RefNodeMeca1D> {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  iterator(ContainerNodesMeca1D &c) : iterator_base(c){};

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  iterator_base &operator++();
  RefNodeMeca1D &operator*();

  virtual bool operator!=(const iterator_base<RefNodeMeca1D> &) const override;
  virtual bool operator==(const iterator_base<RefNodeMeca1D> &) const override;

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

  friend class ContainerNodesMeca1D;

private:
  UInt current_index;
  RefNodeMeca1D ref;
};

/* -------------------------------------------------------------------------- */

inline RefNodeMeca1D &ContainerNodesMeca1D::iterator::operator*() {
  return ref;
}

/* -------------------------------------------------------------------------- */

inline auto ContainerNodesMeca1D::iterator::operator++() -> iterator_base & {
  ++current_index;
  ref.setIndex(current_index);
  return *this;
}

/* -------------------------------------------------------------------------- */
inline bool ContainerNodesMeca1D::iterator::
operator!=(const iterator_base &it) const {
  return current_index !=
         static_cast<const ContainerNodesMeca1D::iterator &>(it).current_index;
}

/* -------------------------------------------------------------------------- */
inline bool ContainerNodesMeca1D::iterator::
operator==(const iterator_base &it) const {
  return current_index ==
         static_cast<const ContainerNodesMeca1D::iterator &>(it).current_index;
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_ITERATOR_MECA1D_HH__ */
