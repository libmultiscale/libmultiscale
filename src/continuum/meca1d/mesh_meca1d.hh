/**
 * @file   mesh_meca1d.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Fri Jul 11 15:47:44 2014
 *
 * @brief  The mesh data structure of MECA1D
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

/* -------------------------------------------------------------------------- */
#ifndef __LIBMULTISCALE_MESH_MECA1D_HH__
#define __LIBMULTISCALE_MESH_MECA1D_HH__
/* -------------------------------------------------------------------------- */
#include "geometry.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
class Geometry;
/* -------------------------------------------------------------------------- */

class MeshMeca1D {

public:
  using InternalArray =
      Eigen::Array<Real, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

  struct FEMeca1D {
    //! first node index
    UInt n1;
    //! second node index
    UInt n2;
  };

public:
  MeshMeca1D();
  void buildMesh(Real element_size, Geometry &b, Real mass_density);
  Real readMesh(const std::string &filename, Real mass_density);

  UInt nbElements();
  UInt nbNodes();
  Real elementSize();
  void setE(Real E);
  Real getE();
  Real getRho();

  void MeshMeca1DWithoutHole(UInt nx, Geometry &b, Real mass_density);

public:
  //! element array
  std::vector<FEMeca1D> elt;
  //! displacement array
  InternalArray u;
  //! initial position array
  InternalArray p0;
  // velocity array
  InternalArray v;
  //! force array
  InternalArray f;
  //! acceleration array
  InternalArray a;
  //! mass array
  InternalArray m;
  //! weighting function
  // InternalArray alpha;
  // //! temperature
  // InternalArray T;
  // //! temperature derivative
  // InternalArray Tdt;
  // //! external heat rate
  // InternalArray external_heat_rate;
  // //! heat rate
  // InternalArray heat_rate;

private:
  UInt n_nodes;
  UInt n_elements;
  Real e_size;
  Real density;
  Real elastic_constant;
};
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_MESH_MECA1D_HH__ */
