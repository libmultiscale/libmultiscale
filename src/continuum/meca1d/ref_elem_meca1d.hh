/**
 * @file   ref_elem_meca1d.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Tue Aug 19 16:38:14 2014
 *
 * @brief  Reference to elemental DOF in MECA1D
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_REF_ELEM_MECA1D_HH__
#define __LIBMULTISCALE_REF_ELEM_MECA1D_HH__
/* -------------------------------------------------------------------------- */
#include "mesh_meca1d.hh"
#include "ref_elem_fe.hh"
#include "ref_node_meca1d.hh"
#include "units.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

/**
 * Class RefElementLibMesh
 * implementation from libmesh of the generic reference to elements
 */

class RefElemMeca1D : public RefElemFE<1, RefElemMeca1D> {

public:
  //! iterators may need a privileged access
  friend class IteratorElemsMeca1D;
  friend class ContainerElemsMeca1D;
  static constexpr UInt Dim = 1;

public:
  RefElemMeca1D() : mesh(NULL){};
  using fields = field_list<_stress, _strain>;

  bool operator==(RefElemMeca1D &ref) { return (ref.index == index); };

  //! geometric ownership test of a given space poUInt for referenced element
  bool contains(Vector<1> x) override {
    UInt n1 = mesh->elt[index].n1;
    UInt n2 = mesh->elt[index].n2;

    if (x[0] <= mesh->p0(n2) && x[0] >= mesh->p0(n1))
      return true;
    else
      return false;
  };

  //! give the number of nodes connected in referenced element
  UInt nNodes() override { return 2; };

  //! return the condensed constraUInt tensor for coordinate coord
  VectorView<1> stress() {
    LM_TOIMPLEMENT;
    return VectorView<1>(nullptr);
  };

  //! return the condensed deformation tensor for coordinate coord
  VectorView<1> strain() {
    LM_TOIMPLEMENT;
    return VectorView<1>(nullptr);
  };

  //! function that computes the nodal shape values for a given element at a
  //! given space point
  template <typename Vec>
  void computeShapes(std::vector<Real> &target, Vec &&X) {
    target.clear();
    UInt n1 = mesh->elt[index].n1;
    UInt n2 = mesh->elt[index].n2;

    Real contrib = (X[0] - mesh->p0(n1)) / (mesh->p0(n2) - mesh->p0(n1));
    target.push_back(1 - contrib);
    target.push_back(contrib);
  }

  //! return the local indexes (in DOF vector) for connected nodes
  std::vector<UInt> localIndexes() override {
    std::vector<UInt> nodes;
    UInt n1 = mesh->elt[index].n1;
    UInt n2 = mesh->elt[index].n2;

    nodes.push_back(n1);
    nodes.push_back(n2);
    return nodes;
  }

  //! return the gloabal indexes (in DOF vector) for connected nodes
  std::vector<UInt> globalIndexes() override { return localIndexes(); }

  //! return a ref to the i-est node
  RefNodeMeca1D &getNode(UInt i);

  //! get potential energy of the element
  Real epot() override {
    UInt n1 = mesh->elt[index].n1;
    UInt n2 = mesh->elt[index].n2;
    Real disp = mesh->u(n2) - mesh->u(n1);
    return 0.5 * mesh->getE() * disp * disp / mesh->elementSize();
  }
  //! get kinetic energy of the element
  Real getEKin() override {
    UInt n1 = mesh->elt[index].n1;
    UInt n2 = mesh->elt[index].n2;
    Real h = mesh->elementSize();
    Real v1 = mesh->v(n1);
    Real v2 = mesh->v(n2);
    Real velMag = mesh->getRho() * 0.25 * (v1 * v1 + v2 * v2);
    return code_unit_system.mvv2e * h * velMag;
  }
  //! get  thermal energy of the element
  Real getEThermal() override { LM_TOIMPLEMENT; };
  inline virtual void printself(std::ostream &stream) const;

protected:
  //! set the mesh pointer
  void setMesh(std::shared_ptr<MeshMeca1D> ptr) {
    mesh = ptr;
    ref_node.setMesh(ptr);
  };

  //! set the index of referenced element
  void setIndex(UInt i) { index = i; };

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

protected:
  //! mesh pointer
  std::shared_ptr<MeshMeca1D> mesh;

  //! referenced index for accessibility
  UInt index;

  //! a reference to a node
  RefNodeMeca1D ref_node;
};
/* -------------------------------------------------------------------------- */

inline RefNodeMeca1D &RefElemMeca1D::getNode(UInt i) {

  UInt n;
  switch (i) {
  case 0:
    n = mesh->elt[index].n1;
    break;
  case 1:
    n = mesh->elt[index].n2;
    break;
  default:
    LM_FATAL("invalid index");
  }
  ref_node.setIndex(n);
  return ref_node;
}
/* -------------------------------------------------------------------------- */

inline void RefElemMeca1D::printself(std::ostream &stream) const {
  stream << "RefElemMeca1D: " << this->index;
}

/* -------------------------------------------------------------------------- */

inline std::ostream &operator<<(std::ostream &os, RefElemMeca1D &ref) {
  ref.printself(os);
  return os;
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_REF_ELEM_MECA1D_HH__ */
