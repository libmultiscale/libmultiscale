/**
 * @file   compute_centro_symmetry.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Nov 25 15:05:56 2013
 *
 * @brief  This computes the centro symmetry criterion over a set of points
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_CENTRO_SYMMETRY_HH__
#define __LIBMULTISCALE_COMPUTE_CENTRO_SYMMETRY_HH__
/* -------------------------------------------------------------------------- */
#include "compute_interface.hh"
#include "container_neighbor_atoms.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class ComputeCentroSymmetry : public ComputeInterface {

public:
  DECLARE_COMPUTE(ComputeCentroSymmetry, _or<MD>);

public:
  template <typename _Input> void build(_Input &cont);

protected:
  template <bool pbc_x, bool pbc_y, bool pbc_z, UInt Dim, typename _Input,
            typename ContNeigh>
  inline void computePerAtomCentroSymmetry(_Input &cont, ContNeigh &contNeigh);

  template <UInt Dim, typename _Input, typename ContNeigh>
  inline void computePerAtomCentroSymmetry(_Input &cont, ContNeigh &contNeigh);

  template <bool pbc_x, bool pbc_y, bool pbc_z, UInt Dim, typename _Input,
            typename ContNeigh>
  inline Real computeCentroSymmetry(typename _Input::Ref &at, _Input &cont,
                                    Real *distsq, UInt *nearest, UInt maxneigh,
                                    Real *pairs, ContNeigh &neighbors);

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */
protected:
  Quantity<Length> rcut;
  Quantity<Length> rcut2;
  bool pbc_flag[3];
  // additional tolerance to be given to the
  Quantity<Length, 3> skin;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_COMPUTE_CENTRO_SYMMETRY_HH__ */
