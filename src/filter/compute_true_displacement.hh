/**
 * @file   compute_true_displacement.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Tue Dec 10 16:00:49 2013
 *
 * @brief  This computes the real displacement when correcting for PBC
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_TRUE_DISPLACEMENT_HH__
#define __LIBMULTISCALE_COMPUTE_TRUE_DISPLACEMENT_HH__
/* -------------------------------------------------------------------------- */
#include "compute_interface.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class ComputeTrueDisplacement : public ComputeInterface {

public:
  DECLARE_COMPUTE(ComputeTrueDisplacement, _or<MD>);

public:
  template <typename _Input> void build(_Input &cont);
  template <UInt Dim, bool pbc_x, bool pbc_y, bool pbc_z, typename _Input>
  void buildWithPBC(_Input &cont);

protected:
  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */
protected:
  //! periodic boundary conditions in each direction
  bool pbc[3];
};

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_COMPUTE_TRUE_DISPLACEMENT_HH__ */
