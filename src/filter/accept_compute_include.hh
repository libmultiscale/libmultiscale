/**
 * @file   accept_compute_include.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Jan 07 16:36:30 2013
 *
 * @brief  This is a convenient file for the visitor mechanism
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "lib_filter.hh"
/* -------------------------------------------------------------------------- */

__MY_TEMPLATE_DECL__
inline void __MY_CLASS__::accept(Compute &v) {
  if (1 == 2) {
  }

#define BOOST_TYPE_CAST_COMPUTE(r, data, x)                                    \
  TYPE_CAST_COMPUTE(BOOST_PP_TUPLE_ELEM(2, 0, x))

#define TYPE_CAST_COMPUTE(filter_type)                                         \
  else if (filter_type *_ptr = dynamic_cast<filter_type *>(&v)) {              \
    __ROUTINE__                                                                \
  }

  BOOST_PP_SEQ_FOR_EACH(BOOST_TYPE_CAST_COMPUTE, obj, __LIST__)

#undef TYPE_CAST_COMPUTE
#undef BOOST_TYPE_CAST_COMPUTE

  else {
    LM_FATAL("unrecognized filter");
  };
}
