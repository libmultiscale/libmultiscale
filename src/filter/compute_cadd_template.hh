/**
 * @file   two2_template.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 *
 * @date   Wed Jan 15 17:00:43 2014
 *
 * @brief  2d template helper
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef __LIBMULTISCALE_COMPUTE_CADD_TEMPLATE_HH__
#define __LIBMULTISCALE_COMPUTE_CADD_TEMPLATE_HH__
/* ------------------------------------------------------------------------ */
#include "compute_interface.hh"
#include "container_mesh.hh"
#include "ref_elem_dd.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

/* the use_template switch turns on or off the use of the template for
   the dislocation detection, not for the coupling
 */
class ComputeCADDTemplate : public ComputeInterface {

public:
  static constexpr UInt Dim = 3;

  DECLARE_COMPUTE(ComputeCADDTemplate, _or<MD, POINT>,
                  _or<DD, RefGenericDDElem<Dim>>);

  template <typename Points, typename Segment>
  std::enable_if_t<Points::Dim == 3u> build(Points &points, Segment &segments);

  template <typename Points, typename Segment>
  std::enable_if_t<Points::Dim != 3u> build(Points &, Segment &) {
    LM_TOIMPLEMENT;
  }

  void init() override;

private:
  void loadNPZ(std::string &filename);

  template <typename Point, typename ElementType>
  std::enable_if_t<std::is_same<ElementType, RefGenericDDElem<Dim>>::value>
  computeTemplateDisplacements(Point &point, ElementType &segment);

  template <typename Point, typename ElementType>
  std::enable_if_t<!std::is_same<ElementType, RefGenericDDElem<Dim>>::value>
  computeTemplateDisplacements(Point &point, ElementType &segment);

  void coreDispAtom(Vector<Dim> &pos_core, Vector<Dim> &disp);

  template <typename IteratorAtoms>
  Real evaluateSquareError(const Real pos_core[], IteratorAtoms &it_atoms);
  Real get_r_cut() { return this->r_cut; }
  Real get_burgers() { return this->burgers; }
  Real get_theta() { return this->theta; }
  inline UInt get_search_dim() { return this->search_dim; }
  inline Real get_tol() { return this->tol; }
  Real domainSize[3];

private:
  inline int findElement(Vector<Dim> &at_coords);

  ContainerGenericMesh<2> mesh;
  std::vector<Vector<3>> corrections;

  Real center_atoms_around_core[2];
  Real r_cut;
  UInt num_nodes, num_elems;
  std::vector<Vector<3>> shape;

  // conversion between xy coordinates and element
  // coordinates l1, l2, l3
  std::vector<Vector<3>> x_l_transition;
  Vector<Dim> md_bbox_min, md_bbox_max;

  Real poisson, burgers, theta;
  dislo_orientation inversion;
  Real factor[2]; // depends entirely on inversion
  // some temp data
  // Real atom_coords[3];
  //! Optimization parameters and temps
  UInt search_dim;

  Real tol;
  // Real **vertices, *vertices_cont;

  // UInt maxiter;
  // int    dir_replica;
  // int    nb_replica;
  // Real rcut;

  std::string filename;
  Real angle_between_b_and_x;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif
