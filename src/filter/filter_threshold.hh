/**
 * @file   filter_threshold.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Fri Nov 08 12:27:05 2013
 *
 * @brief  This filter allows to select DOFs based on a threhold criterion
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_FILTER_THRESHOLD_HH__
#define __LIBMULTISCALE_FILTER_THRESHOLD_HH__
/* -------------------------------------------------------------------------- */
#include "filter_interface.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class FilterThreshold : public FilterInterface {

public:
  DECLARE_FILTER(FilterThreshold, _or<MD>);

  void init() override;

  template <bool has_min, bool has_max, typename _Input>
  void buildMinMax(_Input &cont);

  template <typename _Input> void build(_Input &cont);

private:
  //! min threshold
  Real min;
  //! max threshold
  Real max;
  //! unfiltered computed information
  LMID compute;
};

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_FILTER_THRESHOLD_HH__ */
