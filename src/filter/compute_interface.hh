/**
 * @file   compute_interface.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  This is the interface of all computes
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_INTERFACE_HH__
#define __LIBMULTISCALE_COMPUTE_INTERFACE_HH__
/* -------------------------------------------------------------------------- */
#include "container_array.hh"
#include "filter_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

class ComputeInterface : public FilterInterface {

public:
  ComputeInterface();

  virtual ~ComputeInterface();

  //! gather result routine
  ContainerArray<Real> &gatherData(UInt source_rank, UInt root_rank = 0);
  //! gather all other processor result routine
  ContainerArray<Real> &gatherAllData(UInt root_rank = 0);
  //! all gather result routine
  ContainerArray<Real> &allGatherAllData();
  //! return the total number of elements (parallel safe)
  UInt getTotalNbData(UInt root_rank = 0);

  inline virtual void compute_make_call() override { LM_TOIMPLEMENT; };

protected:
  //! vector for gather routine usage
  ContainerArray<Real> data_gather;
  //! vector for receive counts on allGatherAllData.
  std::vector<int> rcnts;
  //!  flag specifying that input data was gathered(for python)
  bool gather_flag;
  //!  processor number compute_python use to gather data
  UInt gather_root_proc;
};

__END_LIBMULTISCALE__

#define DECLARE_COMPUTE(class_name, ...)                                       \
                                                                               \
  class_name(const std::string &name);                                         \
  virtual ~class_name();                                                       \
                                                                               \
private:                                                                       \
  DECORATE_FUNCTION_DISPATCH(build, __VA_ARGS__)                               \
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW                                              \
                                                                               \
public:                                                                        \
  void declareParams() override;                                               \
  void compute_make_call() override

#endif /* __LIBMULTISCALE_COMPUTE_INTERFACE_HH__ */
