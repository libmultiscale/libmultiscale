/**
 * @file   filter.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Jul 29 17:03:26 2014
 *
 * @brief  This is the mother class of all filters
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "filter_interface.hh"
#include "lib_continuum.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

/* LMDESC FILTER
   This is a generic internal filter
*/

void FilterInterface::declareParams() {

  ActionInterface::declareParams();

  /* LMKEYWORD DOF_TYPE
     Allow to specify the dof type to iterate on: useful only for parallelism.
     The possible choices are:

     - dt_master : only the master dofs
     - dt_slave  : only the slave dofs
     - dt_pbc_master : only pbc master
     - dt_pbc_slave : only pbc slaves
     - dt_all          : all dofs
  */
  this->parseKeyword("DOF_TYPE", dt, dt_master);
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
