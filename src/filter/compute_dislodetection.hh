/**
 * @file   compute_dislocdetn.hh
 *
 * @author Max Hodapp <max.hodapp@epfl.ch>
 *
 * @date   Thu Oct 29 2015
 *
 * @brief  Dislocation detection for single crystals
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_DISLOCDETN_HH__
#define __LIBMULTISCALE_COMPUTE_DISLOCDETN_HH__
/* -------------------------------------------------------------------------- */
// LM
#include "compute_interface.hh"
#include "container_mesh.hh"

// CGAL
// --> need to undefine X,Y,Z since methods from the boost library used by CGAL
//     define them subsequently!! (see third-party/paradis/include/FM.h)
#ifdef X
#undef X
#endif
#ifdef Y
#undef Y
#endif
#ifdef Z
#undef Z
#endif

#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Fixed_alpha_shape_3.h>
#include <CGAL/Fixed_alpha_shape_cell_base_3.h>
#include <CGAL/Fixed_alpha_shape_vertex_base_3.h>
#include <CGAL/Triangulation_cell_base_with_info_3.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <CGAL/linear_least_squares_fitting_3.h>
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
// Typedef's

// CGAL
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_3<UInt, K> VB_i;
typedef CGAL::Triangulation_cell_base_with_info_3<UInt[7], K> CB_i;
typedef CGAL::Fixed_alpha_shape_vertex_base_3<K, VB_i> VBa_i;
typedef CGAL::Fixed_alpha_shape_cell_base_3<K, CB_i> CBa_i;
typedef CGAL::Triangulation_data_structure_3<VBa_i, CBa_i> T_s;
typedef CGAL::Delaunay_triangulation_3<K, T_s> T;
typedef CGAL::Fixed_alpha_shape_3<T> Ta;

typedef T::Point Pt; // Point_3
typedef K::Vector_3 Vec;
typedef K::Line_3 Line;
typedef K::Tetrahedron_3 Tet;
typedef Ta::Vertex_handle Vertex_handle;
typedef Ta::Edge Edge;
typedef Ta::Facet Face;
typedef Ta::Cell_handle Cell_handle;
/* -------------------------------------------------------------------------- */

class ComputeDisloDetection : public ComputeInterface {

public:
  DECLARE_COMPUTE(ComputeDisloDetection, _or<MD>);

  template <typename _Input> void build(_Input &cont);

private:
  // Set the reference bond vectors
  void setRefBondVec();
  // Computes the true Burgers vector of a cell
  Vec computeBurgersVec(const Cell_handle &ch, const UInt idxFace);
  // Get the remaining (distorted) face(s) of a cell which has been detected by
  // a face with index 'idxFace'
  UInt getDistortedFaces(Face &fh, const Cell_handle &ch, const UInt idxFace);

  // Detect distorted tetrahedra (--> this where the actual dislocation
  // detection happens)
  template <typename Mesh> void detect(Mesh &mesh, Ta &tet_alpha);
  // Output DD mesh
  template <typename Mesh>
  void outputDD(Mesh &mesh, std::vector<RefPointData<3>> &nodes_buffer);

  // Outputs an additional container of mesh-type elements to visualize the
  // continuum representation of the atomistic domain using, e.g. ParaView
  void outputMesh(std::list<Vertex_handle> &v_l, std::list<Cell_handle> &c_l);

  // Root process collects container arrays of Reals from all other processes
  // within the group
  void gatherAllData(ContainerArray<Real> &data_recv,
                     ContainerArray<Real> &data_send, const UInt root_rank);

protected:
  // Parsable
  UInt nodesPerSeg; // number of nodes per DD segment
  Real a0;          // lattice constant
  Real alpha;     // maximum squared circumradius for tetrahedrons (defines the
                  // alpha shape)
  Real orient[9]; // orientation of the crystal
  std::string lattice_t; // lattice type
  std::string disloc_t;  // type of dislocation

  UInt Nref;    // nbr of reference bond vectors
  Vec *bVecRef; // reference bond vectors

  // tet mesh of the atomistic domain
  // computed by CGAL --> in fact tet(alpha)!
  ContainerGenericMesh<3> meshTet;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_COMPUTE_DISLOCDETN_HH__ */
