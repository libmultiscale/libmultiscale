/**
 * @file   compute_epot.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This computes the potential energy of a set of DOFs
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "compute_epot.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

template <typename Cont> enable_if_md<Cont> ComputeEPot::build(Cont &cont) {

  Real computed_epot = 0.0;

  for (auto &&at : cont) {
    computed_epot += at.epot();
  }

  CommGroup group = cont.getCommGroup();
  group.allReduce(&computed_epot, 1, "potential energy reduce", OP_SUM);

  this->clear();
  this->getOutputAsArray().push_back(computed_epot);
}

/* -------------------------------------------------------------------------- */

template <typename Cont> enable_if_mesh<Cont> ComputeEPot::build(Cont &cont) {

  Real computed_epot = 0.0;

  for (auto &&el : cont.getContainerElems()) {
    computed_epot += el.epot();
  }

  CommGroup group = this->getCommGroup();

  group.allReduce(&computed_epot, 1, "potential energy reduce", OP_SUM);

  this->clear();

  this->getOutputAsArray().push_back(computed_epot);
}

/* -------------------------------------------------------------------------- */
/* LMDESC EPOT
   This computes the potential energy of the region provided as input
*/

/* LMEXAMPLE COMPUTE epot EPOT INPUT md */

/* -------------------------------------------------------------------------- */
void ComputeEPot::declareParams() { ComputeInterface::declareParams(); }

/* -------------------------------------------------------------------------- */

ComputeEPot::ComputeEPot(const std::string &name) : LMObject(name) {
  this->createInput("input");
  this->createArrayOutput("Epot");
}

/* -------------------------------------------------------------------------- */

ComputeEPot::~ComputeEPot() {}

/* -------------------------------------------------------------------------- */

DECLARE_COMPUTE_MAKE_CALL(ComputeEPot)

__END_LIBMULTISCALE__
