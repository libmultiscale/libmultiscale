/**
 * @file   compute_concat.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Dec 10 16:00:49 2013
 *
 * @brief  This is a compute which allows the concatenation of several computes
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_CONCAT_HH__
#define __LIBMULTISCALE_COMPUTE_CONCAT_HH__
/* -------------------------------------------------------------------------- */
#include "compute_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class ComputeConcat : public ComputeInterface {

public:
  DECLARE_COMPUTE(ComputeConcat, _or<ARRAY, COMPONENT>);

public:
  template <typename T> enable_if_not_component<T> build(T &cont);
  template <typename T> enable_if_component<T> build(T &cont);

protected:
  void register_additional_computes();
  template <typename T> void register_output(T &cont);
  template <typename T> void register_outputs(T &cont);

  std::vector<LMID> list_computes;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_COMPUTE_CONCAT_HH__ */
