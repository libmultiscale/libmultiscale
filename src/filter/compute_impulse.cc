/**
 * @file   compute_impulse.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This stimulator sets an initial impulse displacement field that will
 * generate a wave
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "compute_impulse.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

ComputeImpulse::ComputeImpulse(const std::string &name) : LMObject(name) {
  impulseType = Impulse::PLANAR;
  direction = 0;
  center.setZero();

  this->createInput("domain");

  this->createOutput("output") =
      ContainerArray<Real>(this->getID() + ":" + "output");
}

/* -------------------------------------------------------------------------- */

ComputeImpulse::~ComputeImpulse() {}

/* -------------------------------------------------------------------------- */

void ComputeImpulse::init() { wave_number = 2 * M_PI / wave_length; }

/* -------------------------------------------------------------------------- */
inline Real ComputeImpulse::computeScaleImpulse(Real d) {
  const Real k = wave_number;
  const Real res = intensity * sin(k * d) * exp(-k * k / 9 * d * d);
  DUMP("impulse computed: " << d << " " << k << " " << intensity << " => "
                            << res,
       DBG_INFO);
  return res;
}

/* -------------------------------------------------------------------------- */
template <>
inline UInt Parser::parse(Impulse::ImpulseType &type, std::stringstream &line,
                          UInt) {
  std::string name;
  UInt nb = strNext(name, line);

  if (name == "CIRCULAR")
    type = Impulse::CIRCULAR;
  else if (name == "PLANAR")
    type = Impulse::PLANAR;
  else
    LM_FATAL("unknown impulse type " << name);

  return nb;
}

/* -------------------------------------------------------------------------- */

template <typename Cont> void ComputeImpulse::build(Cont &cont) {

  constexpr UInt Dim = Cont::Dim;
  this->clear();
  auto &output = this->getOutputAsArray();

  for (auto &&at : cont) {

    if (impulseType == Impulse::PLANAR) {
      Real d = at.position0()[direction] - center[direction];

      Real impulse = computeScaleImpulse(d);
      for (UInt i = 0; i < Dim; ++i) {
        if (i == direction)
          output.push_back(impulse);
      }
    } else if (impulseType == Impulse::CIRCULAR) {

      output.resize(output.rows(), Dim);
      Vector<Dim> vec_dir =
          at.position0() - center.cast<Real>().block<Dim, 1>(0, 0);
      Real norm_vec_dir = vec_dir.norm();
      if (norm_vec_dir == 0.)
        LM_FATAL("Cannot compute a displacement at the center of the circle");

      vec_dir /= norm_vec_dir;
      Real d = norm_vec_dir - radius;

      Real impulse = computeScaleImpulse(d);
      this->getOutputAsArray().push_back(impulse * vec_dir);
    }
  }
}
/* -------------------------------------------------------------------------- */

/* LMDESC IMPULSE
   This stimulator sets an initial impulse displacement field that will generate
   a wave. It can generate planar or circular waves:

   - Planar:

     .. math::

       u(\vec{X}) += I \cdot \sin\left(k d \right)  \exp\left(-\frac{(k
       d)^2}{9} \right) \vec{e_i}

     with :math:`I` the intensity, :math:`i` the chosen direction and
     :math:`k = 2\pi/LWAVE` and :math:`d = \vec{X}_i-\vec{C}_i` as defined
     with the parameters (see below).


   - Circular:

     .. math::

       u(X) += I \cdot \sin\left(k (d - R) \right) \exp\left(-\frac{(k (d -
       R))^2}{9} \right) \frac{\vec{X} - \vec{C}}{d}

     with :math:`I` the intensity, :math:`k = 2\pi/LWAVE` and
     :math:`d = \mid \vec{X} - \vec{C} \mid` and :math:`R` a
     radius as defined with the parameters (see below).
*/

/* LMEXAMPLE COMPUTE impulse IMPULSE INPUT central_md LWAVE WaveLength*r0
 * DIRECTION 0 INTENSITY 1.1e-3 ONESHOT 0 */

void ComputeImpulse::declareParams() {

  /* LMKEYWORD LWAVE
     Set the wavelength of the wave to be generated
  */
  this->parseKeyword("LWAVE", wave_length);
  /* LMKEYWORD DIRECTION
     It can be 0,1 or 2 to specify X, Y or Z direction for planar waves
  */
  this->parseKeyword("DIRECTION", direction, 0u);
  /* LMKEYWORD RADIUS
     Radius to be used to generate the impulse
  */
  this->parseKeyword("RADIUS", radius, 0.);
  /* LMKEYWORD INTENSITY
     Intensity of the displacement
  */
  this->parseKeyword("INTENSITY", intensity);
  /* LMKEYWORD CENTER
     center of the impulse to be injected
  */
  this->parseVectorKeyword("CENTER", spatial_dimension, center,
                           VEC_DEFAULTS(0., 0., 0.));
  /* LMKEYWORD TYPE
     Flag to specify the type of impulse.
     It can be either CIRCULAR or PLANAR.
     Self-explanatory examples are:

     - COMPUTE impulse-planar   IMPULSE INPUT md LWAVE L0/10 DIRECTION 0
       INTENSITY 1e-3 CENTER 0 0

     .. image:: images/planar-impulse.png

     - COMPUTE impulse-circular IMPULSE INPUT md LWAVE L0/10 TYPE CIRCULAR
       RADIUS L0/4 INTENSITY 1e-3 CENTER 0 0

     .. image:: images/radial-impulse.png

  */
  this->parseKeyword("TYPE", impulseType, Impulse::PLANAR);
}
/* -------------------------------------------------------------------------- */

DECLARE_COMPUTE_MAKE_CALL(ComputeImpulse)

__END_LIBMULTISCALE__
