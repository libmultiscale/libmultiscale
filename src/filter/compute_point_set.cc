/**
 * @file   compute_point_set.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Thu Jul 24 14:21:58 2014
 *
 * @brief  This compute generates an independent point set from a compute which
 * contains positions
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "compute_point_set.hh"
#include "compute_interface.hh"
#include "factory_multiscale.hh"
#include "lib_dumper.hh"
#include "lib_filter.hh"
#include "lib_stimulation.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

template <typename Cont> void ComputePointSet::build(Cont &cont) {

  auto Dim = cont.getDim();

  if (Dim != spatial_dimension)
    LM_FATAL("the compute used as input is not valid: " << Dim << " != "
                                                        << spatial_dimension);

  switch (Dim) {
  case 1:
    this->build<1>(cont);
    break;
  case 2:
    this->build<2>(cont);
    break;
  case 3:
    this->build<3>(cont);
    break;
  }
}

/* -------------------------------------------------------------------------- */

template <UInt Dim, typename Cont> void ComputePointSet::build(Cont &cont) {

  auto &output = this->allocOutput<ContainerArray<RefPointData<Dim>>>("output");

  output.clear();

  int cpt = 0;
  RefPointData<Dim> ref;

  for (auto &&val : cont) {
    int k = cpt % Dim;
    ref.position()[k] = val;
    ++cpt;
    if (cpt % Dim == 0) {
      output.push_back(ref);
      ref.position().setZero();
    }
  }
}
/* -------------------------------------------------------------------------- */

/* LMDESC POINTSET
   This computes generate an independent point set from a compute
   which contains positions.
*/

/* LMEXAMPLE COMPUTE pset POINTSET INPUT coords */

inline void ComputePointSet::declareParams() {
  FilterInterface::declareParams();
}

/* -------------------------------------------------------------------------- */

ComputePointSet::ComputePointSet(const std::string &name) : LMObject(name) {

  this->createInput("input1");
  this->createOutput("output");
}

/* -------------------------------------------------------------------------- */

ComputePointSet::~ComputePointSet() {}

/* -------------------------------------------------------------------------- */

DECLARE_COMPUTE_MAKE_CALL(ComputePointSet)

__END_LIBMULTISCALE__
