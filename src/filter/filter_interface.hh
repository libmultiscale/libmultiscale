/**
 * @file   filter_interface.hh
 *
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  This is the interface to all filters
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* ---------------------------------------------------------------- */
#ifndef __LIBMULTISCALE_FILTER_INTERFACE_HH__
#define __LIBMULTISCALE_FILTER_INTERFACE_HH__
/* ---------------------------------------------------------------- */
#include "action_interface.hh"
#include "component_libmultiscale.hh"
/* ---------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* ---------------------------------------------------------------- */
class ActionInterface;
template <typename _Input> class FilterCompatibility;
/* ---------------------------------------------------------------- */

class FilterInterface : public ActionInterface {

public:
  FilterInterface(){};
  virtual ~FilterInterface(){};

  virtual void declareParams() override;

protected:
  DOFType dt;
};

/* ---------------------------------------------------------------- */

/// standard output stream operator
inline std::ostream &operator<<(std::ostream &stream,
                                const FilterInterface &_this) {
  _this.printself(stream);
  return stream;
}

__END_LIBMULTISCALE__

/* ---------------------------------------------------------------- */

#define DECLARE_FILTER(class_name, ...)                                        \
  class_name(const std::string &name);                                         \
  virtual ~class_name();                                                       \
                                                                               \
private:                                                                       \
  DECORATE_FUNCTION_DISPATCH(build, __VA_ARGS__)                               \
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW                                              \
                                                                               \
public:                                                                        \
  void declareParams() override;                                               \
  void compute_make_call() override

#define DECLARE_COMPUTE_MAKE_CALL(class_name)                                  \
  void class_name::compute_make_call() {                                       \
    DUMP(this->getID() << ": Compute", DBG_INFO);                              \
    this->build<dispatch>(this->getInput());                                   \
  }

#endif /* __LIBMULTISCALE_FILTER_INTERFACE_HH__ */
