/**
 * @file   compute_lgfm.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Max Hodapp <max.hodapp@epfl.ch>
 *
 * @brief  This compute allows to compute stresses due to force
 * and displacements at a generic interface.
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
/*
 *  TODO:
 *  - in build():
 *    > add condition if Sinclair scheme is used
 */
/* -------------------------------------------------------------------------- */

// LM
#include "compute_lgfm.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lgfm_package.h"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

ComputeLGFM::ComputeLGFM(const LMID &id) : LMObject(id) {
  this->removeInput("input1");
  this->createInput("stress_pts");
  this->createInput("lgfm");
}

/* -------------------------------------------------------------------------- */

ComputeLGFM::~ComputeLGFM() {}

/* -------------------------------------------------------------------------- */

template <typename ContainerField, typename ContainerLGFM>
std::enable_if_t<(ContainerField::Dim == 2u) || (ContainerField::Dim == 3u)>
ComputeLGFM::build( ContainerField &cont_stress_pts, // field pts where stresses are computed
                    ContainerLGFM  &cont_lgfm)       // DBEM variables at source pts
{
  
  /* Copy stress (field) points */
  
  DUMP("Check container stress_pts", DBG_MESSAGE);
  
  constexpr UInt Dim_p = ContainerField::Dim;
  
  auto nb_elem = cont_stress_pts.size();
  auto size = nb_elem * Dim_p;
  
  DUMP("Dimension: " << Dim_p, DBG_MESSAGE);
  DUMP("Elements: " << nb_elem, DBG_MESSAGE);
  DUMP("Total size: " << size, DBG_MESSAGE);
  
  std::vector< LGFM_NS::Vector<Dim_p> > stress_pts;
  
  for (auto && iter: cont_stress_pts) {
    auto pos = iter.position0();
    stress_pts.push_back(pos);
  }
  
  /* Copy sources */
  
  DUMP("Check container lgfm", DBG_MESSAGE);
  
  UInt Dim_lgfm = cont_lgfm.getDim();
  
  if (Dim_lgfm != 3u*Dim_p) {
    LM_FATAL("Dimensions of cont_stress_pts and cont_lgfm do not match");
  }
  
  auto size_lgfm = cont_lgfm.size();
  auto nb_elem_lgfm = size_lgfm / Dim_p;
  
  DUMP("Dimension: " << Dim_lgfm, DBG_MESSAGE);
  DUMP("Elements: " << nb_elem_lgfm, DBG_MESSAGE);
  DUMP("Total size: " << size_lgfm, DBG_MESSAGE);
  
  std::vector< LGFM_NS::Vector<Dim_p> > source_pts;
  std::vector< LGFM_NS::Vector<Dim_p> > source_displs;
  std::vector< LGFM_NS::Vector<Dim_p> > source_forces;
  
  for (UInt i=0u; i<nb_elem; ++i) {
    LGFM_NS::Vector<Dim_p> pos;
    LGFM_NS::Vector<Dim_p> displ;
    LGFM_NS::Vector<Dim_p> force;
    for (UInt j=0u; j<Dim_p; ++j) {
      pos[j]   = cont_lgfm[(i*Dim_lgfm)              + j];
      displ[j] = cont_lgfm[(i*Dim_lgfm) + Dim_p      + j];
      force[j] = cont_lgfm[(i*Dim_lgfm) + (2u*Dim_p) + j];
    }
    source_pts.push_back(pos);
    source_displs.push_back(displ);
    source_forces.push_back(force);
  }
  
  /* Initialize stress vector */
  
  constexpr UInt Dim_stress = 3u*(Dim_p-1u);
  
  std::vector< LGFM_NS::Vector<Dim_stress> > stresses;
  
  /* Compute stresses */
  
  LGFM_NS::computeStress<Dim_p, Dim_stress>(stresses, stress_pts, source_pts, source_displs, source_forces);
  
  /* Make result the LM output */
  
  this->clear();
  
  Real *ptr = (Real*) &stresses[0];
  for (UInt i=0u; i<nb_elem*Dim_stress; ++i)
    this->push_back(ptr[i]);
  
//   for (auto &iter: stresses) {
//     for (UInt i=0u; i<Dim_stress; ++i)
//       this->add(iter[i]);
//   }
  
  this->name_computed.clear();
  
  switch(Dim_p) {
    case 2u:
      this->name_computed.push_back("lgfm_stress11");
      this->name_computed.push_back("lgfm_stress22");
      this->name_computed.push_back("lgfm_stress12");
      break;
    case 3u:
      this->name_computed.push_back("lgfm_stress11");
      this->name_computed.push_back("lgfm_stress22");
      this->name_computed.push_back("lgfm_stress33");
      this->name_computed.push_back("lgfm_stress12");
      this->name_computed.push_back("lgfm_stress13");
      this->name_computed.push_back("lgfm_stress23");
      break;
  }
  
}

// template <typename ContainerSources, typename ContainerField>
// std::enable_if_t<ContainerSources::Dim == ContainerField::Dim>
// ComputeLGFM::build(ContainerSources &cont_sources, ContainerField &cont_stress_points) {
// 
//   // iteration over sources and copy in a contiguous array
// 
//   // example for flat array
//   std::vector<Real> flat_positions;
//   for (auto && p: cont_sources){
//     auto pos = p.position0();
//     for(UInt i = 0; i<spatial_dimension; ++i)
//       flat_positions.push_back(pos[i]);
//   }
// 
// 
//   constexpr auto Dim = ContainerSources::Dim;
//   // example for vector array
//   std::vector<Vector<Dim>> vector_positions;
//   for (auto && p: cont_sources){
//     auto pos = p.position0();
//     vector_positions.push_back(pos);
//   }
// 
//   std::vector<Vector<Dim>> stress_locations;
// 
//   // auto elems = cont_stress_points.getContainerElems();
//   
//   for (auto && p: cont_stress_points){
//     auto pos = p.position0();
//     stress_locations.push_back(pos);
//   }
// 
// 
//   // the contiguous array pointer is
//   // Real * ptr = (Real*)&vector_positions[0];
//   
//   // iteration over stress points or fetch of pointer
// 
//   // call to LGFM library
//   // LGFM::compute(ptr);
//   
//   // get the result: a pointer to a contiguous space
// 
//   // wrap a Libmultiscale container around that pointer => make it the output of the compute
// }

/* -------------------------------------------------------------------------- */
/* LMDESC LGFM
   Calculate DD displacements at fieldpoints defined by INPUT.
*/

/* LMEXAMPLE
   COMPUTE stress LGFM INPUT sources=points stress_points=where 
*/

inline void ComputeLGFM::declareParams() {
  ComputeInterface::declareParams();

};
/* -------------------------------------------------------------------------- */

void ComputeLGFM::compute_make_call() {
  DUMP(this->getID() << ": Compute", DBG_INFO);
  call_compute(*this, [&](auto &... args) { this->build(args...); },
               this->getInput("stress_pts"), this->getInput("lgfm"));
}

__END_LIBMULTISCALE__
