/**
 * @file   compute_periodic_position.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Mon Nov 25 15:05:56 2013
 *
 * @brief  This computes periodic positions by correcting for PBC
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_PERIODIC_POSITION_HH__
#define __LIBMULTISCALE_COMPUTE_PERIODIC_POSITION_HH__
/* -------------------------------------------------------------------------- */
#include "compute_true_displacement.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class ComputePeriodicPosition : public ComputeTrueDisplacement {

public:
  DECLARE_COMPUTE(ComputePeriodicPosition, _or<MD>);

public:
  template <typename _Input> void build(_Input &cont);
  template <UInt Dim, bool pbc_x, bool pbc_y, bool pbc_z, typename _Input>
  void buildWithPBC(_Input &cont);

protected:
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_COMPUTE_PERIODIC_POSITION_HH__ */
