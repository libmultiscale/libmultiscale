/**
 * @file   compute_time_delta.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Dec 10 16:00:49 2013
 *
 * @brief  This compute maintains a time history in order to compute the time
 * variation
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "compute_time_delta.hh"
#include "factory_multiscale.hh"
#include "lm_common.hh"
#include "math_tools.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
ComputeTimeDelta::ComputeTimeDelta(const std::string &name) : LMObject(name) {
  delta_size = 1;
  // ActionManager::getManager().addObject(*this);
}
/* -------------------------------------------------------------------------- */

ComputeTimeDelta::~ComputeTimeDelta() {}

/* -------------------------------------------------------------------------- */

void ComputeTimeDelta::action() { LM_TOIMPLEMENT; }

/* -------------------------------------------------------------------------- */

template <typename Cont> void ComputeTimeDelta::account(Cont &cont) {

  int k = (current_step + delta_size) % this->frequency;
  LM_ASSERT(k >= 0 || k == (int)(delta_size % this->frequency),
            "this should not happen");

  // const UInt Dim = cont.getDim();
  previous_data.resize(cont.size());
  this->getOutputAsArray().resize(cont.size());

  if (k == (int)(delta_size % this->frequency)) {
    for (UInt i = 0; i < cont.size(); ++i)
      this->getOutputAsArray()[i] = cont[i] - previous_data[i];
  }
  if (k == 0) {
    for (UInt i = 0; i < cont.size(); ++i)
      previous_data[i] = cont[i];
  }
  this->last_accounted_step = current_step;
  this->last_accounted_k = k;
}

/* -------------------------------------------------------------------------- */

template <typename Cont> void ComputeTimeDelta::build(Cont &) {

#ifndef LM_OPTIMIZED
  int k = (current_step + delta_size) % this->frequency;
#endif // LM_OPTIMIZED
  LM_ASSERT(k == (int)(delta_size % this->frequency),
            "time delta is not tallied to correct timesteps");

  LM_ASSERT(last_accounted_k == (delta_size % this->frequency),
            "time delta is not tallied to correct timesteps");

  LM_ASSERT(last_accounted_step == current_step,
            "time delta is not tallied to correct timesteps");

  // const UInt Dim = cont.getDim();

  // this->name_computed.clear();
  // for (UInt i = 0; i < Dim; ++i) {
  //   std::stringstream str;
  //   if (cont.name_computed.size() > i)
  //     str << "D-" << cont.name_computed[i];
  //   else
  //     str << "D-field" << i;
  //   this->name_computed.push_back(str.str());
  // }
}

/* -------------------------------------------------------------------------- */

/* LMDESC TIMEDELTA
   This computes a time delta from another input compute
*/

/* LMEXAMPLE
   COMPUTE time_delta TIMEDELTA INPUT md FIELD displacement\\
   COMPUTE sum TIMEAVG INPUT disp
*/

/* LMHERITANCE  action_interface*/

void ComputeTimeDelta::declareParams() {

  ActionInterface::declareParams();

  /* LMKEYWORD SIZE
     The time(multiple of timestep size)
     over which to compute the delta. The default is 1
  */

  this->parseKeyword("SIZE", delta_size);
}

/* -------------------------------------------------------------------------- */
bool ComputeTimeDelta::shouldMakeAction() {
  bool sM = this->doesStageMaskMatch();
  if (!sM)
    return false;

  this->setOneStep();

  if (this->end_step >= current_step && this->start_step <= current_step)
    if (this->frequency != UINT_MAX) {
      int k = (current_step + delta_size) % this->frequency;
      if (k == 0 || k == (int)delta_size) {
        DUMP("should proceed action "
                 << this->getID() << " current_step = " << current_step
                 << " start " << this->start_step << " end " << this->end_step,
             DBG_INFO);
        return true;
      }
    }
  return false;
}

/* -------------------------------------------------------------------------- */

DECLARE_COMPUTE_MAKE_CALL(ComputeTimeDelta)

__END_LIBMULTISCALE__
