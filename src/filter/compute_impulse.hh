/**
 * @file   compute_impulse.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Dec 04 12:14:29 2013
 *
 * @brief  This stimulator sets an initial impulse displacement field that will
 * generate a wave
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COMPUTE_IMPULSE_H
#define COMPUTE_IMPULSE_H

/* -------------------------------------------------------------------------- */
#include "compute_interface.hh"
#include "lm_parser.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

namespace Impulse {
enum ImpulseType { PLANAR = 1, CIRCULAR = 2 };
}

class ComputeImpulse : public ComputeInterface {

public:
  friend class StimulationImpulse;

  DECLARE_COMPUTE(ComputeImpulse, _or<MD, CONTINUUM>);

  //! initialization function
  void init() override;

  //! most generic stimulate function
  template <typename _Input> void build(_Input &cont);

  //! return scale deformation impulse provided a scale position
  Real computeScaleImpulse(Real d);

private:
  //! the direction for planar impulse wave
  UInt direction;
  //! wavelength of the impulse
  Quantity<Length> wave_length;
  //! wave number
  Real wave_number;
  //! intensity factor
  Real intensity;
  //! radius of the radial impulse
  Quantity<Length> radius;
  //! code for impulse
  std::string impulse;
  //! select PLANAR vs RADIAL impulse type
  Impulse::ImpulseType impulseType;
  //! center of the
  Quantity<Length, 3> center;
};

__END_LIBMULTISCALE__
#endif
