/**
 * @file   compute_addpoints.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This is a compute which allows to add point to models
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "compute_addpoints.hh"
#include "domain_multiscale.hh"
#include "factory_multiscale.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

ComputeAddPoints::ComputeAddPoints(const std::string &name) : LMObject(name) {
  // ActionManager::getManager().addObject(*this);
}

/* -------------------------------------------------------------------------- */

ComputeAddPoints::~ComputeAddPoints() {}

/* -------------------------------------------------------------------------- */

void ComputeAddPoints::action() { LM_TOIMPLEMENT; }

/* -------------------------------------------------------------------------- */
template <typename Cont> void ComputeAddPoints::build(Cont &) {

  LM_TOIMPLEMENT;
  // auto &allPositions = cont.allGatherAllData();
  // auto dom_ptr = DomainMultiScale::getManager().getSharedObject(this->dom);

  // dom_ptr->addPoints(allPositions);
}
/* -------------------------------------------------------------------------- */

/* LMDESC ADDPOINTS

   Add a points to a domain \\

*/

/* LMEXAMPLE
   COMPUTE points ADDPOINTS INPUT points \\
*/

void ComputeAddPoints::declareParams() {
  ComputeInterface::declareParams();

  /* LMKEYWORD DOMAIN
     Specify the domain ID to be used to generate the displacement data
  */
  this->parseKeyword("DOMAIN", dom);
}

/* -------------------------------------------------------------------------- */

DECLARE_COMPUTE_MAKE_CALL(ComputeAddPoints)

__END_LIBMULTISCALE__
