/**
 * @file   compute_energydensity.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This computes the spectral energy density for 1D models
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include <complex>
#include <fftw3.h>
/* -------------------------------------------------------------------------- */
#include "compute_energydensity.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
ComputeEnergyDensity::ComputeEnergyDensity(const std::string &name)
    : LMObject(name) {
  shift_fft = 1.0;
}
/* -------------------------------------------------------------------------- */
ComputeEnergyDensity::~ComputeEnergyDensity() {}

template <typename Cont> void ComputeEnergyDensity::build(Cont &cont) {

  if (cont.size() == 0)
    LM_FATAL("there is nothing to compute");

  if (Cont::Dim != 1)
    LM_FATAL("this compute operates only on 1D models");

  UInt nbDof = cont.size();

  std::vector<std::complex<Real>> dos;
  dos.resize(nbDof * 2);
  std::complex<Real> zero = 0.;
  dos.assign(nbDof * 2, zero);

  UInt cpt = 0;
  for (auto &&at : cont) {
    dos[cpt] = at.velocity()[0];
    ++cpt;
  }

  std::vector<Real> power;

  fftw_complex *in = (fftw_complex *)&dos[0];
  fftw_plan p =
      fftw_plan_dft_1d(nbDof * 2, in, in, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(p);
  power.resize(nbDof);
  for (UInt j = 0; j < nbDof; ++j) {
    Real dw = norm(dos[j]);
    power[j] = sqrt(dw) / nbDof;
  }
  fftw_destroy_plan(p);

  this->clear();
  LM_TOIMPLEMENT;
  // cast_compute(this)->setDim(2);

  this->getOutputAsArray().push_back(power[0]);
  this->getOutputAsArray().push_back(0.0);
  for (UInt i = 1; i < nbDof; ++i) {
    Real pw = 2.0 * power[i];
    Real k = i * 0.5 / nbDof / shift_fft;
    this->getOutputAsArray().push_back(pw);
    this->getOutputAsArray().push_back(k);
  }
}

/* -------------------------------------------------------------------------- */
/* LMDESC ENERGY_DENSITY
   This computes the energy per mode in the wavevector space
   Warning: Currently, this works only in 1D.
*/

/* LMEXAMPLE */
void ComputeEnergyDensity::declareParams() {

  /* LMKEYWORD SHIFT_FFT
     This keyword is used to shift the wavenumber axis
     by given value provided by the user.
  */
  this->parseKeyword("SHIFT_FFT", shift_fft);
}

/* -------------------------------------------------------------------------- */

DECLARE_COMPUTE_MAKE_CALL(ComputeEnergyDensity)

__END_LIBMULTISCALE__
