/**
 * @file   compute_extract.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  This compute extract data from references
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_EXTRACT_HH__
#define __LIBMULTISCALE_COMPUTE_EXTRACT_HH__
/* -------------------------------------------------------------------------- */
#include "compute_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class ComputeExtract : public ComputeInterface {

public:
  DECLARE_COMPUTE(ComputeExtract, _or<MD, CONTINUUM, DD>);

public:
  template <typename Cont> enable_if_md<Cont> build(Cont &cont);
  template <typename Cont> enable_if_mesh<Cont> build(Cont &cont);

  template <FieldType ftype, typename Cont>
  void buildContainerField(Cont &cont);

  template <FieldType ftype, typename Cont>
  void buildContainerNodalField(Cont &cont);

  template <typename Field> void buildContainerElementalField(Field &f);

  ContainerArray<Real> point_tags;

protected:
  FieldType field;
  int component;
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_COMPUTE_EXTRACT_HH__ */
