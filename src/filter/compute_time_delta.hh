/**
 * @file   compute_time_delta.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Dec 10 16:00:49 2013
 *
 * @brief  This compute maintains a time history in order to compute the time
 * variation
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_TIME_DELTA_HH__
#define __LIBMULTISCALE_COMPUTE_TIME_DELTA_HH__
/* -------------------------------------------------------------------------- */
#include "compute_interface.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class ComputeTimeDelta : public ComputeInterface {

public:
  DECLARE_COMPUTE(ComputeTimeDelta, _or<ARRAY>);

  template <typename _Input> void build(_Input &cont);
  template <typename _Input> void account(_Input &cont);

  //! \todo{needs to check it works alright}
  void action() override;
  //! this function decides if action should be called \todo{needs to check it
  //! works alright}
  virtual bool shouldMakeAction() override;

protected:
  //! the operator results
  std::vector<Real> previous_data;
  //! Number of steps an action's activity starts ahead of the actual action.
  UInt delta_size;

  UInt last_accounted_k;
  UInt last_accounted_step;
  bool build_phase;
};
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_COMPUTE_TIME_DELTA_HH__ */
