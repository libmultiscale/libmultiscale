/**
 * @file   compute_periodic_position.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This computes periodic positions by correcting for PBC
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define TIMER
/* -------------------------------------------------------------------------- */
#include "compute_periodic_position.hh"
#include "compute_extract.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "ref_point_data.hh"
#include <cmath>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

ComputePeriodicPosition::ComputePeriodicPosition(const std::string &name)
    : LMObject(name), ComputeTrueDisplacement(name) {}

/* -------------------------------------------------------------------------- */

ComputePeriodicPosition::~ComputePeriodicPosition() {}
/* -------------------------------------------------------------------------- */

template <typename Cont> void ComputePeriodicPosition::build(Cont &cont) {

  constexpr UInt Dim = Cont::Dim;

  if (this->pbc[0] == 0 && this->pbc[1] == 0 && this->pbc[2] == 0)
    buildWithPBC<Dim, false, false, false>(cont);

  if (this->pbc[0] == 1 && this->pbc[1] == 0 && this->pbc[2] == 0)
    buildWithPBC<Dim, true, false, false>(cont);

  if (this->pbc[0] == 1 && this->pbc[1] == 1 && this->pbc[2] == 0)
    buildWithPBC<Dim, true, true, false>(cont);

  if (this->pbc[0] == 1 && this->pbc[1] == 1 && this->pbc[2] == 1)
    buildWithPBC<Dim, true, true, true>(cont);

  if (this->pbc[0] == 1 && this->pbc[1] == 0 && this->pbc[2] == 1)
    buildWithPBC<Dim, true, false, true>(cont);

  if (this->pbc[0] == 0 && this->pbc[1] == 1 && this->pbc[2] == 0)
    buildWithPBC<Dim, false, true, false>(cont);

  if (this->pbc[0] == 0 && this->pbc[1] == 1 && this->pbc[2] == 1)
    buildWithPBC<Dim, false, true, true>(cont);

  if (this->pbc[0] == 0 && this->pbc[1] == 0 && this->pbc[2] == 1)
    buildWithPBC<Dim, false, false, true>(cont);
}

/* -------------------------------------------------------------------------- */

template <UInt Dim, bool pbc_x, bool pbc_y, bool pbc_z, typename Cont>
void ComputePeriodicPosition::buildWithPBC(Cont &cont) {

  this->clear();

  Cube &bbox = cont.getBoundingBox();
  auto d_size = bbox.getSize();
  const Vector<3> &xmin = bbox.getXmin();

  DUMP("here is my geom " << bbox, DBG_DETAIL);
  DUMP("here is interesting info " << d_size[0] << " " << d_size[1] << " "
                                   << d_size[2] << " " << xmin[0] << " "
                                   << xmin[1] << " " << xmin[2],
       DBG_DETAIL);

  UInt cpt = 0;
  for (auto &&at : cont) {

    Vector<Dim> position = at.position();
    for (UInt i = 0; i < Dim; ++i) {
      if ((pbc_x && i == 0) || (pbc_y && i == 1) || (pbc_z && i == 2)) {
        int offset = floor((position[i] - xmin[i]) / d_size[i]);
        position[i] -= offset * d_size[i];
      }
      this->getOutputAsArray().push_back(position[i]);
    }
    ++cpt;
  }
}
/* -------------------------------------------------------------------------- */

/* LMDESC PERIODICPOSITION
   TODO
*/

/* LMEXAMPLE
   COMPUTE periodicPosition PERIODICPOSITION INPUT md PBC 1 1 0
*/

/* LMHERITANCE compute_true_displacement */

void ComputePeriodicPosition::declareParams() {

  ComputeTrueDisplacement::declareParams();
}

/* -------------------------------------------------------------------------- */
DECLARE_COMPUTE_MAKE_CALL(ComputePeriodicPosition)

__END_LIBMULTISCALE__
