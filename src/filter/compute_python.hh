/**
 * @file   compute_python.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Sun Feb 02 17:45:17 2014
 *
 * @brief  This compute allows to use Python to produce computed values as input
 * to LM
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_PYTHON_HH__
#define __LIBMULTISCALE_COMPUTE_PYTHON_HH__
/* -------------------------------------------------------------------------- */
#include "compute_interface.hh"
#include "container_mesh.hh"
/* -------------------------------------------------------------------------- */
// backward declaration

namespace pybind11 {
class object;
class dict;
class module_;
using module = module_;
} // namespace pybind11

namespace py = pybind11;

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

class ComputePython : public ComputeInterface {

public:
  DECLARE_COMPUTE(ComputePython, _or<ARRAY, COMPONENT>);

  DECORATE_FUNCTION_DISPATCH(compute_with_arg, _or<ARRAY, COMPONENT>)
  template <typename T> enable_if_component<T> compute_with_arg(T &cont);
  template <typename T> enable_if_not_component<T> compute_with_arg(T &cont);

  void compute_no_arg();

  void init() override;

  void appendGeneralInfo(CommGroup group);
  void callPythonRoutine(const std::string &name,
                         std::shared_ptr<py::object> &pCompute);

protected:
  std::string filename;
  std::map<std::string, std::shared_ptr<py::object>> pComputes;

  std::map<std::string, decltype(code_unit_system.mvv2e)> units_dict;
  UInt gather_root_proc;
  bool gather_flag;

  std::vector<LMID> compute_list;

  // arguments to be passed to the python script
  std::shared_ptr<py::dict> kwargs;
  std::shared_ptr<py::module> converter;
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
#undef DL_IMPORT
#endif /* __LIBMULTISCALE_COMPUTE_PYTHON_HH__ */
