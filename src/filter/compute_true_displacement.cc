/**
 * @file   compute_true_displacement.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This computes the real displacement when correcting for PBC
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define TIMER
/* -------------------------------------------------------------------------- */
#include "compute_true_displacement.hh"
#include "compute_extract.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

ComputeTrueDisplacement::ComputeTrueDisplacement(const std::string &name)
    : LMObject(name) {
  for (UInt i = 0; i < 3; ++i) {
    pbc[i] = 0;
  }
  this->createInput("input");
  this->createArrayOutput("output");
}
/* -------------------------------------------------------------------------- */

ComputeTrueDisplacement::~ComputeTrueDisplacement() {}
/* -------------------------------------------------------------------------- */

template <typename Cont> void ComputeTrueDisplacement::build(Cont &cont) {

  constexpr UInt Dim = Cont::Dim;

  if (pbc[0] == 0 && pbc[1] == 0 && pbc[2] == 0)
    buildWithPBC<Dim, false, false, false>(cont);

  if (pbc[0] == 1 && pbc[1] == 0 && pbc[2] == 0)
    buildWithPBC<Dim, true, false, false>(cont);

  if (pbc[0] == 1 && pbc[1] == 1 && pbc[2] == 0)
    buildWithPBC<Dim, true, true, false>(cont);

  if (pbc[0] == 1 && pbc[1] == 1 && pbc[2] == 1)
    buildWithPBC<Dim, true, true, true>(cont);

  if (pbc[0] == 1 && pbc[1] == 0 && pbc[2] == 1)
    buildWithPBC<Dim, true, false, true>(cont);

  if (pbc[0] == 0 && pbc[1] == 1 && pbc[2] == 0)
    buildWithPBC<Dim, false, true, false>(cont);

  if (pbc[0] == 0 && pbc[1] == 1 && pbc[2] == 1)
    buildWithPBC<Dim, false, true, true>(cont);

  if (pbc[0] == 0 && pbc[1] == 0 && pbc[2] == 1)
    buildWithPBC<Dim, false, false, true>(cont);
}

/* -------------------------------------------------------------------------- */

template <UInt Dim, bool pbc_x, bool pbc_y, bool pbc_z, typename Cont>
void ComputeTrueDisplacement::buildWithPBC(Cont &cont) {

  this->clear();

  Cube &bbox = cont.getBoundingBox();
  DUMP(this->getID() << ": my geometry is " << bbox, DBG_DETAIL);

  auto d_size = bbox.getSize();
  auto d_inv_size_half = bbox.getInvSizeHalf();
  const Vector<3> &xmin = bbox.getXmin();
  DUMP(this->getID() << ": d_size, d_inv_size_half, xmin [" << d_size[0] << ", "
                     << d_size[1] << ", " << d_size[2] << "], ["
                     << d_inv_size_half[0] << ", " << d_inv_size_half[1] << ", "
                     << d_inv_size_half[2] << "], " << xmin,
       DBG_DETAIL);

  auto &output = this->getOutputAsArray();
  output.resize(0, Dim);

  for (auto &&at : cont) {

    Vector<Dim> position = at.position();
    auto position0 = at.position0();

    Vector<Dim> u;
    for (UInt i = 0; i < Dim; ++i) {
      if ((pbc_x && i == 0) || (pbc_y && i == 1) || (pbc_z && i == 2)) {

        int offset = floor((position[i] - xmin[i]) / d_size[i]);
        position[i] -= offset * d_size[i];
        u[i] = position[i] - position0[i];
        offset = static_cast<int>(u[i] * d_inv_size_half[i]);
        u[i] -= offset * d_size[i];
      } else
        u[i] = position[i] - position0[i];
    }
    output.push_back(u);
  }
}
/* -------------------------------------------------------------------------- */

/* LMDESC TRUEDISP
   This allow the compute the true displacement of points in a periodic
   domain, as long as none of the points moves by more than have a domain size
   in a given direction.
*/

/* LMEXAMPLE
   COMPUTE trueDisp TRUEDISP INPUT md PBC 1 1 0
*/

inline void ComputeTrueDisplacement::declareParams() {

  ComputeInterface::declareParams();

  /* LMKEYWORD PBC
     Specify if periodic boundary conditions should be considered for each
     direction
  */
  this->parseVectorKeyword("PBC", spatial_dimension, pbc);
}

/* -------------------------------------------------------------------------- */

DECLARE_COMPUTE_MAKE_CALL(ComputeTrueDisplacement)

__END_LIBMULTISCALE__
