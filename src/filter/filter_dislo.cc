/**
 * @file   filter_dislo.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  Select atoms based on centro symmetry criterion
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

/* -------------------------------------------------------------------------- */
#include "filter_dislo.hh"
#include "compute_centro_symmetry.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

template <typename Cont> void FilterDislo::build(Cont &cont) {

  auto &output =
      this->getOutput("output").get<typename Cont::ContainerSubset>();

#ifndef LM_OPTIMIZED
  constexpr UInt Dim = Cont::Dim;
  LM_ASSERT(Dim == 3, "cannot work if not in 3D!");
#endif

  output.clear();

  compute.build(cont);

  auto it = cont.begin();
  auto end = cont.end();
  auto it_centro = compute.evalArrayOutput().begin();

  Real max = 0.0;

  for (; it != end; ++it, ++it_centro) {

    auto at = *it;
    auto value = *it_centro;

    if (value > epsilon[0] && value < epsilon[1]) {
      output.push_back(at);
      if (value > max)
        max = value;
    }
  }

  UInt nb = output.size();
  DUMP("le filtre a trouve " << nb << " atoms et max = " << max
                             << " threshold = " << epsilon,
       DBG_MESSAGE);
}

/* -------------------------------------------------------------------------- */
/* LMDESC DISLO

   This filter allows to extract a set of atoms based on the
   centro-symmetry criterion (see compute \ref{computecentrosymmetry}).
*/

/* LMEXAMPLE FILTERDISLO -FEPSILON 0.5 */
/* LMHERITANCE compute_centro_symmetry */

void FilterDislo::declareParams() {
  this->addSubParsableObject(compute);

  /* LMKEYWORD EPSILON
     Specifies the lower and upper bounds for the centrosymmetry. Only values
     in the range {epsilon[0], epsilon[1]} are output by this filter
  */
  this->parseVectorKeyword("EPSILON", 2, epsilon);
}

/* -------------------------------------------------------------------------- */

FilterDislo::FilterDislo(const std::string &name)
    : LMObject(name), compute("ComputeCentroSymmetry:" + name) {
  epsilon[0] = 0.0;
  epsilon[1] = 1e300;
}

/* -------------------------------------------------------------------------- */

FilterDislo::~FilterDislo() {}

/* -------------------------------------------------------------------------- */

DECLARE_COMPUTE_MAKE_CALL(FilterDislo)

__END_LIBMULTISCALE__
