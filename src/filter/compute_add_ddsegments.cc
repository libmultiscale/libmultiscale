/**
 * @file   compute_add_ddloop.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 30 09:55:56 2014
 *
 * @brief  This allows to add a loop to a DD model from a set of points
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "compute_add_ddsegments.hh"
#include "compute_point_set.hh"
#include "domain_interface.hh"
#include "domain_multiscale.hh"
#include "factory_multiscale.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

template <typename ContIn, typename ContOut>
std::enable_if_t<ContIn::Dim == 3 and ContOut::Dim == 3>
ComputeAddDDSegments::build(ContIn &mesh, ContOut &segments) {

  auto &&nodes = mesh.getContainerNodes();
  auto &&elems = mesh.getContainerElems();
  // UInt nb_points = cont_in.size();

  using RefNode =
      std::remove_reference_t<decltype(segments.getContainerNodes().get(0))>;
  using RefElem =
      std::remove_reference_t<decltype(segments.getContainerElems().get(0))>;

  auto new_mesh =
      ContainerMesh<ContainerArray<RefNode>, ContainerArray<RefElem>>(
          this->getID() + ":created_mesh");
  auto &&new_nodes = new_mesh.getContainerNodes();
  auto &&new_elems = new_mesh.getContainerElems();

  // adding the points
  for (auto &&n : nodes) {
    // this actually creates a node in the domain (e.g. paradis)
    RefNode new_node;
    new_node.position() = n.position();
    new_node.previous_position() = n.position();
    new_nodes.push_back(new_node);
  }

  auto burgers_inverse = Quantity<Length, 3>(-1. * this->burgers);

  // adding the segments
  for (auto &&el : elems) {
    LM_ASSERT(el.conn.size() == 2,
              "we cannot add other things than segments to DD model");

    RefElem new_elem;
    auto new_connectivity = el.conn;
    new_connectivity[0] = new_nodes[el.conn[0]].getIndex();
    new_connectivity[1] = new_nodes[el.conn[1]].getIndex();
    new_elem.setConnectivity(new_connectivity);
    new_elems.push_back(new_elem);

    // setting the burgers and glide_normal
    new_elem.burgers() = this->burgers.cast<Real>();
    new_elem.normal() = this->glide_normal;

    auto mirror_elem = new_elem.getMirrorElem();
    mirror_elem.burgers() = burgers_inverse.cast<Real>();
    mirror_elem.normal() = this->glide_normal;
  }
}
/* -------------------------------------------------------------------------- */

/* LMDESC ADD_DDSEGMENTS

   Add segments to a dislocation dynamics domain
*/

/* LMEXAMPLE
   FILTER loop MESH INPUT nodes=points INPUT connectivity=conn
   COMPUTE ddLoop ADD_DDSEGMENTS INPUT mesh=loop segments=dd_part BURGERS
   0 0 1 GLIDE_PLANE 0 0 1
*/

void ComputeAddDDSegments::declareParams() {
  ComputeInterface::declareParams();

  /* LMKEYWORD BURGERS
     Specify the burgers to use for the loop segments
  */
  this->parseVectorKeyword("BURGERS", spatial_dimension, this->burgers);

  /* LMKEYWORD GLIDE_PLANE
     Specify the glide plane normal
  */
  this->parseVectorKeyword("GLIDE_PLANE", spatial_dimension,
                           this->glide_normal);
}

/* -------------------------------------------------------------------------- */

ComputeAddDDSegments::ComputeAddDDSegments(const std::string &name)
    : LMObject(name) {
  ActionManager::getManager().addObject(*this);
  this->createInput("mesh");
  this->createInput("segments");
}

/* -------------------------------------------------------------------------- */

ComputeAddDDSegments::~ComputeAddDDSegments() {}

/* -------------------------------------------------------------------------- */

void ComputeAddDDSegments::compute_make_call() {
  this->build<dispatch>(this->getInput("mesh"), this->getInput("segments"));
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
