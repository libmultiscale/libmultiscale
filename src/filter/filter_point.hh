/**
 * @file   filter_point.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Tue Jul 29 17:03:26 2014
 *
 * @brief  This allows to filter a set of DOFs by matching a list of coordinates
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the terms  of the  GNU Lesser  General Public  License as  published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public
 * License  for more details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_FILTER_POINT_HH__
#define __LIBMULTISCALE_FILTER_POINT_HH__
/* -------------------------------------------------------------------------- */
#include "domain_interface.hh"
#include "filter_interface.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class FilterPoint : public FilterInterface {

public:
  DECLARE_FILTER(FilterPoint, _or<MD, DD, CONTINUUM, MESH, POINT>);

  void init() override;

  template <typename _Input> void build(_Input &cont);

protected:
  UInt getNbPointsInFile();
  UInt getNbSharedPoints();

protected:
  std::string path;
  std::vector<Real> positions;
  Real tolerance;
  UInt dimension;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_FILTER_POINT_HH__ */
