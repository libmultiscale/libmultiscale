/**
 * @file   compute_point_set.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Thu Jul 24 14:21:58 2014
 *
 * @brief  This compute generates an independent point set from a compute which
 * contains positions
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_POINT_SET_HH__
#define __LIBMULTISCALE_COMPUTE_POINT_SET_HH__
/* -------------------------------------------------------------------------- */
#include "container_array.hh"
#include "filter_interface.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class ComputePointSet : public FilterInterface {

public:
  DECLARE_FILTER(ComputePointSet, _or<ARRAY>);

  template <typename _Input> void build(_Input &cont);
  template <UInt Dim, typename _Input> void build(_Input &cont);
};

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_COMPUTE_POINT_SET_HH__ */
