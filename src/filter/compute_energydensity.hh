/**
 * @file   compute_energydensity.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Sat Nov 30 17:14:16 2013
 *
 * @brief  This computes the spectral energy density for 1D models
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_ENERGYDENSITY_HH__
#define __LIBMULTISCALE_COMPUTE_ENERGYDENSITY_HH__
/* -------------------------------------------------------------------------- */
#include "compute_interface.hh"
#include "domain_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/** Class ComputeEnergyDensity
 * Implementation of a compute for energy density
 */

class ComputeEnergyDensity : public ComputeInterface {

public:
  DECLARE_COMPUTE(ComputeEnergyDensity, _or<MD>);

public:
  template <typename _Input> void build(_Input &cont);

protected:
  Real shift_fft;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_COMPUTE_ENERGYDENSITY_HH__ */
