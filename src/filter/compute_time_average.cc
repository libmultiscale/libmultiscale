/**
 * @file   compute_time_average.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This allows to compute time averages of other computes
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "compute_time_average.hh"
#include "factory_multiscale.hh"
#include "lm_common.hh"
#include "math_tools.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

ComputeTimeAverage::ComputeTimeAverage(const std::string &name)
    : LMObject(name) {
  window_size = 0;
  // ActionManager::getManager().addObject(*this);
}

/* -------------------------------------------------------------------------- */
ComputeTimeAverage::~ComputeTimeAverage() {}

/* -------------------------------------------------------------------------- */

template <typename Cont> void ComputeTimeAverage::account(Cont &cont) {

  int k = (current_step + window_size - 1) % this->frequency;
  LM_ASSERT(k >= 0 && k < (int)window_size, "this should not happen");

  // const UInt Dim = cont.getArray().getDim();

  if (k == 0) {
    average_data.resize(cont.size());
    for (UInt i = 0; i < cont.size(); ++i)
      average_data[i].init();
  } else {
    if ((this->getOutputAsArray().getDim() != cont.getDim()) ||
        (average_data.size() != cont.size())) {
      DUMP("inconsistency in produced data size", DBG_WARNING);
      return;
    }
  }

  for (UInt i = 0; i < cont.size(); ++i)
    average_data[i].account(cont.get(i));

  this->last_accounted_step = current_step;
  this->last_accounted_k = k;
}

/* -------------------------------------------------------------------------- */

template <typename Cont> void ComputeTimeAverage::build(Cont &cont) {

#ifndef LM_OPTIMIZED
  int k = (current_step + window_size - 1) % this->frequency;
#endif // LM_OPTIMIZED
  LM_ASSERT(k == (int)window_size - 1,
            "time average is not tallied to correct timesteps");

  LM_ASSERT(last_accounted_k == window_size - 1,
            "time average is not tallied to correct timesteps");

  LM_ASSERT(last_accounted_step == current_step,
            "time average is not tallied to correct timesteps");

  // const UInt Dim = cont.getDim();

  this->clear();
  for (UInt i = 0; i < cont.size(); ++i)
    this->getOutputAsArray().push_back(
        average_data[i].result(cont.getCommGroup()));

  // this->name_computed.clear();
  // for (UInt i = 0; i < Dim; ++i) {
  //   std::stringstream str;
  //   if (cont.name_computed.size() > i)
  //     str << cont.name_computed[i] << "-time-avg-" << i;
  //   else
  //     str << "field" << i << "-time-avg-" << i;
  //   this->name_computed.push_back(str.str());
  // }
}

/* -------------------------------------------------------------------------- */
bool ComputeTimeAverage::shouldMakeAction() {
  bool sM = this->doesStageMaskMatch();
  if (!sM)
    return false;

  this->setOneStep();

  if (this->end_step >= current_step && this->start_step <= current_step)
    if (this->frequency != UINT_MAX) {
      int k = (current_step + window_size - 1) % this->frequency;
      if (k >= 0 && k < (int)window_size) {
        DUMP("should proceed action "
                 << this->getID() << " current_step = " << current_step
                 << " start " << this->start_step << " end " << this->end_step,
             DBG_INFO);
        return true;
      }
    }
  return false;
}
/* -------------------------------------------------------------------------- */

void ComputeTimeAverage::action() { LM_TOIMPLEMENT; }

/* -------------------------------------------------------------------------- */
/* LMDESC TIMEAVG
   This computes a time averaged version of a set of reals
*/

/* LMEXAMPLE
   COMPUTE disp EXTRACT INPUT md FIELD displacement
   COMPUTE time_average TIMEAVG INPUT disp
*/

/* LMHERITANCE  action_interface*/

void ComputeTimeAverage::declareParams() {

  ActionInterface::declareParams();

  /* LMKEYWORD WINDOW
     Number of steps to be taken into account for average
     Must be smaller than FREQ
  */

  this->parseKeyword("WINDOW", window_size);
}

/* -------------------------------------------------------------------------- */
DECLARE_COMPUTE_MAKE_CALL(ComputeTimeAverage)

__END_LIBMULTISCALE__
