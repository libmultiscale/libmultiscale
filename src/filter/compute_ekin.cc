/**
 * @file   compute_ekin.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This computes the kinetic energy of a set of DOFs
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "compute_ekin.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
#include "ref_point_data.hh"
#include "units.hh"
#include "units_converter.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

ComputeEKin::ComputeEKin(const std::string &name)
    : LMObject(name), based_on_nodes_flag(false) {

  this->createInput("domain");

  auto output_names =
      std::list<std::string>{"nb_points", "Temperature", "EKin", "EKin_vector"};

  this->createArrayOutputs(output_names);
}

/* -------------------------------------------------------------------------- */
ComputeEKin::~ComputeEKin() {}

/* -------------------------------------------------------------------------- */

template <typename Cont> enable_if_md<Cont> ComputeEKin::build(Cont &cont) {

  constexpr UInt Dim = Cont::Dim;

  Array1D<Dim> computed_ekin;
  computed_ekin = 0.;

  UInt cpt = cont.size();

  for (auto &&at : cont) {
    Array1D<Dim> vel = at.velocity();
    computed_ekin += vel * vel * at.mass();
  }

  computed_ekin *= 0.5 * code_unit_system.mvv2e;

  for (UInt i = 0; i < Dim; ++i) {
    DUMP("computed ekin[" << i << "] = " << computed_ekin[i], DBG_INFO);
  }

  CommGroup group = cont.getCommGroup();

  group.allReduce(computed_ekin.data(), Dim, "kinetic energy reduce", OP_SUM);
  group.allReduce(&cpt, 1, "kinetic energy counter reduce", OP_SUM);

  Real ekin = 0.0;

  for (UInt i = 0; i < Dim; ++i) {
    DUMP("ekin[" << i << "] = " << computed_ekin[i], DBG_INFO);
    ekin += computed_ekin[i];
  }

  // compute temperature
  UnitsConverter unit;
  unit.setInUnits(code_unit_system);
  unit.setOutUnits(si_unit_system);
  unit.computeConversions();
  Real T = 2. / cpt / Dim / boltzmann * (ekin * unit.getConversion<Energy>());

  // convert the kinetic energies to the required unit systems
  // unit.setOutUnits(this->unit_system);
  // unit.computeConversions();
  // ekin *= unit.getConversion<Energy>();
  // for (UInt i = 0; i < Dim; ++i) computed_ekin[i] *=
  // unit.getConversion<Energy>();

  DUMP("nb_points = " << cpt, DBG_INFO);
  this->clear();

  Real nb_points = static_cast<Real>(cpt);
  this->getOutputAsArray("nb_points").push_back(nb_points);
  this->getOutputAsArray("Temperature").push_back(T);
  this->getOutputAsArray("EKin").push_back(ekin);
  this->getOutputAsArray("EKin_vector").resize(0, Dim);
  this->getOutputAsArray("EKin_vector").push_back(computed_ekin);
}
/* -------------------------------------------------------------------------- */

template <typename Cont> enable_if_mesh<Cont> ComputeEKin::build(Cont &cont) {

  constexpr UInt Dim = Cont::Dim;

  if (based_on_nodes_flag) {
    buildBasedOnNodes<Dim>(cont);
    return;
  }

  Real computed_ekin = 0.0;

  for (auto &&el : cont.getContainerElems()) {
    computed_ekin += el.getEKin();
  }

  CommGroup group = cont.getCommGroup();

  group.allReduce(&computed_ekin, 1, " kinetic energy reduce", OP_SUM);

  this->clear();
  this->getOutputAsArray("EKin").push_back(computed_ekin);
}
/* -------------------------------------------------------------------------- */

template <UInt Dim, typename Cont>
void ComputeEKin::buildBasedOnNodes(Cont &cont) {

  Vector<Dim> computed_ekin = Array1D<Dim>::Zero();
  UInt cpt = cont.size();

  for (auto &&at : cont.filter(~(dt_pbc_slave | dt_slave))) {
    auto vel = at.velocity();
    computed_ekin += 0.5 * at.mass() * Vector<Dim>(vel.array() * vel.array()) *
                     code_unit_system.mvv2e;
  }

  CommGroup group = cont.getCommGroup();

  group.allReduce(&computed_ekin[0], Dim, "kinetic energy reduce", OP_SUM);
  group.allReduce(&cpt, 1, "kinetic energy counter reduce", OP_SUM);

  Real ekin = 0.0;

  for (UInt i = 0; i < Dim; ++i) {
    DUMP("ekin[" << i << "] = " << computed_ekin[i], DBG_INFO);
    ekin += computed_ekin[i];
  }

  DUMP("nb_points = " << cpt, DBG_INFO);
  this->clear();

  Real nb_points = static_cast<Real>(cpt);
  this->getOutputAsArray("nb_points").push_back(nb_points);
  this->getOutputAsArray("EKin").push_back(ekin);
  this->getOutputAsArray("EKin_vector").resize(0, Dim);
  this->getOutputAsArray("EKin_vector").push_back(computed_ekin);

  DUMP("Kinetic energy based on nodes could be higher than the one computed "
       "from elements.",
       DBG_WARNING);
}

/* -------------------------------------------------------------------------- */

/* LMDESC EKIN
   This computes the kinetic energy of the region provided as input
*/

/* LMEXAMPLE COMPUTE ekin EKIN INPUT md */

inline void ComputeEKin::declareParams() {

  ComputeInterface::declareParams();

  /* LMKEYWORD BASED_ON_NODES
     Specify that the selection should be based on the
     nodes of the element. Valid for only FE.
  */
  this->parseTag("BASED_ON_NODES", based_on_nodes_flag, false);
}
/* -------------------------------------------------------------------------- */

DECLARE_COMPUTE_MAKE_CALL(ComputeEKin)

__END_LIBMULTISCALE__
