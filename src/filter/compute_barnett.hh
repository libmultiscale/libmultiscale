/**
 * @file   compute_dd_displacement.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Moseley Philip Arthur <philip.moseley@epfl.ch>
 *
 * @date   Thu Nov 21 22:11:13 2013
 *
 * @brief  This compute allows to compute the displacement field on a set of
 * points due to dislocation segments
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_BARNETT_HH__
#define __LIBMULTISCALE_COMPUTE_BARNETT_HH__
/* -------------------------------------------------------------------------- */
#include "compute_interface.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class ComputeBarnett : public ComputeInterface {

public:
  DECLARE_COMPUTE(ComputeBarnett, _or<DD>, _or<MD, POINT>);

  template <typename ContainerDD, typename ContainerPoints>
  std::enable_if_t<ContainerDD::Dim == 3 and ContainerPoints::Dim == 3>
  build(ContainerDD &cont_dd, ContainerPoints &cont_points);

  template <typename ContainerDD, typename ContainerPoints>
  std::enable_if_t<ContainerDD::Dim != 3 or ContainerPoints::Dim != 3>
  build(ContainerDD &cont_dd, ContainerPoints &cont_points);

private:
  Real poisson;

  Vector<3> slip_plane;
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_COMPUTE_BARNETT_HH__ */
