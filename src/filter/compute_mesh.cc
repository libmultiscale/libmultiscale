/**
 * @file   compute_mesh.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This computes generate an independent mesh from points and
 * connectivities provided as computes
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the
 * Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for
 * more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public
 * License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "compute_interface.hh"
#include "compute_point_set.hh"
#include "container_mesh.hh"
#include "factory_multiscale.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_dumper.hh"
#include "lib_filter.hh"
#include "lib_md.hh"
#include "lib_stimulation.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
/* ----------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* ----------------------------------------------------------------------- */

ComputeMesh::ComputeMesh(const std::string &name)
    : LMObject(name), FilterInterface() {
  this->createInput("nodes");
  this->createInput("connectivity");
  this->createOutput("mesh");
}

/* ----------------------------------------------------------------------- */

ComputeMesh::~ComputeMesh() {}

/* ----------------------------------------------------------------------- */

void ComputeMesh::build_noargument() {
  ComputePython compute(this->getID() + ":mesh_compute");
  compute.setParam("FILENAME", this->filename);
  compute.init();
  compute.compute();
  LM_TOIMPLEMENT;
}

/* ----------------------------------------------------------------------- */

template <typename Nodes, typename Connectivity>
void ComputeMesh::build(Nodes &nodes, Connectivity &connectivity) {

  constexpr UInt Dim = Nodes::Dim;
  using MeshType = ContainerGenericMesh<Dim>;

  auto &output = allocOutput<MeshType>("mesh", this->getID());

  // copy the nodes
  auto &nds = output.getContainerNodes();
  nds = nodes;
  auto &els = output.getContainerElems();
  els.clear();

  auto nodes_per_elem = connectivity.getDim();
  auto size = connectivity.size();
  auto nb_elem = size / nodes_per_elem;
  RefGenericElem<Dim> tmp_el;
  for (UInt i = 0; i < nb_elem; ++i) {
    Eigen::VectorXd conn(nodes_per_elem);
    for (UInt j = 0; j < nodes_per_elem; ++j) {
      conn[j] = UInt(connectivity[i * nodes_per_elem + j]);
      tmp_el.setConnectivity(conn);
    }
    els.push_back(tmp_el);
  }
}
/* -------------------------------------------------------------------------- */

/* LMDESC MESH
   This compute generates an independent mesh from an input
   containing points and a compute which contains connectivities.
*/

/* LMEXAMPLE COMPUTE pset MESH INPUT coords INPUT conn  */

void ComputeMesh::declareParams() {
  FilterInterface::declareParams();

  /* LMKEYWORD FILENAME
     Specify the python script to use to produce the mesh
  */
  this->parseKeyword("FILENAME", this->filename, "");
}

/* -------------------------------------------------------------------------- */

void ComputeMesh::compute_make_call() {
  try {
    this->build<dispatch>(this->getInput("nodes"),
                          this->getInput("connectivity"));
  } catch (Component::UnconnectedInput &e) {
    this->build_noargument();
  }
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
