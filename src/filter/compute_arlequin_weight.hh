/**
 * @file   compute_arlequin_weight.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_WEIGHTING_HH__
#define __LIBMULTISCALE_WEIGHTING_HH__
/* -------------------------------------------------------------------------- */
#define CONTINUFLAG 1
#define ATOMEFLAG 2
/* -------------------------------------------------------------------------- */
#include "ball.hh"
#include "compute_interface.hh"
#include "cube.hh"
#include "geom_inter.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

class ComputeArlequinWeight : public ComputeInterface {

public:
  DECLARE_COMPUTE(ComputeArlequinWeight, _or<MD, CONTINUUM>);

protected:
  template <UInt Dim> Real weight(Vector<Dim> X);
  template <UInt Dim> Real weight(Vector<Dim> X, Cube &c);
  template <UInt Dim> Real weight(Vector<Dim> X, Ball &b);
  Real computeWeight(Real x, Real R);

  template <typename Cont> enable_if_point<Cont> build(Cont &cont);
  template <typename Cont> enable_if_mesh<Cont> build(Cont &cont);

protected:
  //! geometry to base the weighting algorithm
  LMID geom;
  //! treshold to decide to impose quality quality factor
  const Real ZERO_LIMIT = 1e-3;
  //! quality factor (see anciaux, coulaud, roman, zerah publication)
  Real quality;
  //! id of compute to use to provide weights
  LMID compute_id;
};

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_WEIGHTING_HH__ */
