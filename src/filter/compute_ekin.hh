/**
 * @file   compute_ekin.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 *
 * @date   Tue Dec 03 22:43:33 2013
 *
 * @brief  This computes the kinetic energy of a set of DOFs
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_EKIN_HH__
#define __LIBMULTISCALE_COMPUTE_EKIN_HH__
/* -------------------------------------------------------------------------- */
#include "compute_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class ComputeEKin : public ComputeInterface {

public:
  DECLARE_COMPUTE(ComputeEKin, _or<MD, CONTINUUM>);

public:
  template <typename _Input> enable_if_md<_Input> build(_Input &cont);
  template <typename _Input> enable_if_mesh<_Input> build(_Input &cont);
  template <UInt Dim, typename _Input> void buildBasedOnNodes(_Input &cont);

protected:
  //! flag to select based only nodes
  bool based_on_nodes_flag;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_COMPUTE_EKIN_HH__ */
