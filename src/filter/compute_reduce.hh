/**
 * @file   compute_reduce.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Dec 10 16:00:49 2013
 *
 * @brief  This is a compute which reduce to scalar quantities
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_REDUCE_HH__
#define __LIBMULTISCALE_COMPUTE_REDUCE_HH__
/* -------------------------------------------------------------------------- */
#include "compute_interface.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class ComputeReduce : public ComputeInterface {

public:
  DECLARE_COMPUTE(ComputeReduce, _or<ARRAY>);

public:
  template <typename _Input> void build(_Input &cont);

protected:
  Operator op;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_COMPUTE_REDUCE_HH__ */
