/**
 * @file   compute_reduce.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Tue Dec 10 16:00:49 2013
 *
 * @brief  This is a compute which reduce to scalar quantities
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "compute_reduce.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "math_tools.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
ComputeReduce::ComputeReduce(const std::string &name) : LMObject(name) {
  this->createInput("input");
  this->createArrayOutput("reduced");
}
/* -------------------------------------------------------------------------- */
ComputeReduce::~ComputeReduce() {}

/* -------------------------------------------------------------------------- */

template <Operator Op, typename Cont, typename V>
std::enable_if_t<not std::is_arithmetic_v<V>> reduce(Cont &cont,
                                                     V &reduced_data) {

  MathTools::ReduceOperator<Op, V> ops;

  ops.init(reduced_data.rows(), reduced_data.cols());

  for (auto &&val : cont.rowwise()) {
    ops.account(val);
  }
  reduced_data = ops.result(cont.getCommGroup());
}
/* -------------------------------------------------------------------------- */

template <typename Cont> void ComputeReduce::build(Cont &cont) {

  const UInt Dim = cont.getDim();
  Array reduced_data(1, Dim);

  auto &array = this->getOutputAsArray();
  array.resize(array.rows(), Dim);

  switch (op) {
  case OP_MAX:
    reduce<OP_MAX>(cont, reduced_data);
    break;
  case OP_MIN:
    reduce<OP_MIN>(cont, reduced_data);
    break;
  case OP_SUM:
    reduce<OP_SUM>(cont, reduced_data);
    break;
  case OP_AVERAGE:
    reduce<OP_AVERAGE>(cont, reduced_data);
    break;
  default:
    LM_FATAL("Operator " << op << " not handled");
  }

  this->clear();
  this->getOutputAsArray().push_back(reduced_data);
}
/* -------------------------------------------------------------------------- */

/* LMDESC REDUCE
   This computes a reduced version of a set of reals
*/

/* LMEXAMPLE
   COMPUTE disp EXTRACT INPUT md FIELD displacement\\
   COMPUTE sum REDUCE INPUT disp OPERATOR MAX
*/

inline void ComputeReduce::declareParams() {
  ComputeInterface::declareParams();

  /* LMKEYWORD OPERATOR
     Specify the operator to use for the reduction. \\
     At present time the available operations are:

     - MIN
     - MAX
     - SUM
     - AVERAGE
  */
  this->parseKeyword("OPERATOR", op);
}

/* -------------------------------------------------------------------------- */

DECLARE_COMPUTE_MAKE_CALL(ComputeReduce)

__END_LIBMULTISCALE__
