/**
 * @file   compute_mesh.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  This computes generate an independent mesh from points and
 * connectivities provided as computes
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_MESH_HH__
#define __LIBMULTISCALE_COMPUTE_MESH_HH__
/* -------------------------------------------------------------------------- */
#include "compute_python.hh"
#include "container_mesh.hh"
#include "filter_interface.hh"
#include "ref_point_data.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class ComputeMesh : public FilterInterface {

public:
  DECLARE_FILTER(ComputeMesh, _or<POINT>, _or<ARRAY>);

  template <typename Nodes, typename Connectivity>
  void build(Nodes &nodes, Connectivity &connectivity);

  void build_noargument();

private:
  std::string filename;
};

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_COMPUTE_MESH_HH__ */
