/**
 * @file   compute_concat.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Jan 10 20:47:45 2014
 *
 * @brief  This is a compute which allows the concatenation of several computes
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "compute_concat.hh"
#include "factory_multiscale.hh"
#include "lib_continuum.hh"
#include "lib_dd.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include <typeinfo>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

ComputeConcat::ComputeConcat(const std::string &name) : LMObject(name) {
  this->createInput("input");
}

/* -------------------------------------------------------------------------- */

ComputeConcat::~ComputeConcat() {}

/* -------------------------------------------------------------------------- */
template <typename T> enable_if_not_component<T> ComputeConcat::build(T &cont) {

  this->register_output(cont);
  register_additional_computes();
}
/* -------------------------------------------------------------------------- */

template <typename T> enable_if_component<T> ComputeConcat::build(T &cont) {
  register_outputs(cont);
  register_additional_computes();
}
/* -------------------------------------------------------------------------- */

template <typename T> void ComputeConcat::register_outputs(T &cont) {
  for (auto &&[name, val] : cont.evalOutputs()) {
    auto &&array = cont.template evalArrayOutput(name);
    this->register_output(array);
  }
}

/* -------------------------------------------------------------------------- */

template <typename T> void ComputeConcat::register_output(T &cont) {
  auto &name = cont.getID();
  if (this->getOutputs().count(name))
    return;
  this->createOutput(name);
  this->getOutput(name) = cont;
}

/* -------------------------------------------------------------------------- */

void ComputeConcat::register_additional_computes() {

  for (LMID id : list_computes) {
    auto &my_compute = getRegisteredOutput<ComputeInterface>(id);
    this->register_outputs(my_compute);
  }
}
/* -------------------------------------------------------------------------- */

/* LMDESC CONCAT
   This computes makes a compute out of several computes.
*/

/* LMEXAMPLE COMPUTE concat CONCAT INPUT epot ADD_COMPUTE ekin */

inline void ComputeConcat::declareParams() {

  ComputeInterface::declareParams();

  /* LMKEYWORD ADD_COMPUTE
     add a compute to the input list
  */
  this->parseKeyword("ADD_COMPUTE", list_computes, std::vector<std::string>{});
}

/* -------------------------------------------------------------------------- */

DECLARE_COMPUTE_MAKE_CALL(ComputeConcat)

__END_LIBMULTISCALE__
