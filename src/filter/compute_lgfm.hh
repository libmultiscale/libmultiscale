/**
 * @file   compute_lgfm.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Max Hodapp <max.hodapp@epfl.ch>
 *
 * @brief  This compute allows to compute stresses due to force
 * and displacements at a generic interface.
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_LGFM_HH__
#define __LIBMULTISCALE_COMPUTE_LGFM_HH__
/* -------------------------------------------------------------------------- */
#include "compute_interface.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class ComputeLGFM : public ComputeInterface {
  
public:
  DECLARE_COMPUTE(ComputeLGFM);
  DECLARE_MULTIPLE_INPUT(std::tuple<DD, POINT>, std::tuple<COMPUTE>);

  template <typename ContainerField, typename ContainerLGFM>
  std::enable_if_t<(ContainerField::Dim == 2u) || (ContainerField::Dim == 3u)>
  build(ContainerField &cont_stress_pts, ContainerLGFM &cont_lgfm);
  
  template <typename ContainerField, typename ContainerLGFM>
  std::enable_if_t<(ContainerField::Dim != 2u) && (ContainerField::Dim != 3u)>
  build(ContainerField &cont_stress_pts, ContainerLGFM &cont_lgfm) {
    LM_FATAL("ContainerField::Dim must be 2 or 3");
  }
  
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_COMPUTE_LGFM_HH__ */
