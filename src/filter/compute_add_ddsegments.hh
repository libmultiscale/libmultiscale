/**
 * @file   compute_add_ddloop.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Jul 11 15:53:17 2014
 *
 * @brief  This allows to add a loop to a DD model from a set of points
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_COMPUTE_ADD_DDLOOP_HH__
#define __LIBMULTISCALE_COMPUTE_ADD_DDLOOP_HH__
/* -------------------------------------------------------------------------- */
#include "compute_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class ComputeAddDDSegments : public ComputeInterface {

public:
  DECLARE_COMPUTE(ComputeAddDDSegments, _or<MESH>, _or<DD>);

  //! most generic build function
  template <typename ContIn, typename ContOut>
  std::enable_if_t<ContIn::Dim == 3 and ContOut::Dim == 3>
  build(ContIn &cont_in, ContOut &cont_out);

  template <typename ContIn, typename ContOut>
  std::enable_if_t<ContIn::Dim != 3 or ContOut::Dim != 3> build(ContIn &,
                                                                ContOut &) {
    LM_TOIMPLEMENT;
  }

private:
  //! the domain ID to be used to insert new points
  // LMID dom;
  //! the burgers vectors
  Quantity<Length, 3> burgers;
  //! the glide plane normals
  Real glide_normal[3];
};

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_COMPUTE_ADD_DDLOOP_HH__ */
