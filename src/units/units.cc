/**
 * @file   units.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 02 16:25:38 2014
 *
 * @brief  This is the units system of LM
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "units.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
bool UnitSystem::operator==(const UnitSystem &s) const {
  if ((this->distance_standard == s.distance_standard) &&
      (this->mass_standard == s.mass_standard) &&
      (this->energy_standard == s.energy_standard) &&
      (this->time_standard == s.time_standard) &&
      (this->pressure_standard == s.pressure_standard) &&
      (this->temperature_standard == s.temperature_standard))
    return true;
  else
    return false;
}

/* -------------------------------------------------------------------------- */

bool UnitSystem::operator!=(const UnitSystem &s) const { return !((*this) == s); }

/* -------------------------------------------------------------------------- */

void UnitSystem::init() {
  velocity_standard = distance_standard / time_standard;
  acceleration_standard = velocity_standard / time_standard;
  force_standard = energy_standard / distance_standard;
  surface_standard = distance_standard * distance_standard;
  volume_standard = distance_standard * distance_standard * distance_standard;
  mass_density_standard = mass_standard / volume_standard;
  pressure_standard = force_standard / surface_standard;

  mvv2e =
      (mass_standard * velocity_standard * velocity_standard) / energy_standard;
  f_m2v_t = (force_standard / mass_standard) / acceleration_standard;
  ft_m2v = (force_standard * time_standard / mass_standard) / velocity_standard;
  mv_t2f = (mass_standard * velocity_standard / time_standard) / force_standard;
  kT2e = 1 / energy_standard;
  kT2fd = 1 / (force_standard * distance_standard);
  m_tt2f_d = (mass_standard / time_standard / time_standard) /
             (force_standard / distance_standard);
  e_dd2m_tt = (energy_standard / distance_standard / distance_standard) /
              (mass_standard / time_standard / time_standard);
  e2fd = energy_standard / (force_standard * distance_standard);
  e_m2dd_tt = (energy_standard / mass_standard) /
              (distance_standard / time_standard) /
              (distance_standard / time_standard);
  fd2e = (force_standard * distance_standard) / energy_standard;
  //    print();
}
/* -------------------------------------------------------------------------- */

void UnitSystem::print() const {
  std::stringstream sstr;
  this->printself(sstr);
  std::string clown = sstr.str();
  DUMP(clown, DBG_MESSAGE);
}

/* -------------------------------------------------------------------------- */

void UnitSystem::printself(std::ostream &stream) const {
  stream << "[d] = " << distance_standard << " meters" << std::endl;
  stream << "[m] = " << mass_standard << " Kg" << std::endl;
  stream << "[e] = " << energy_standard << " Joules" << std::endl;
  stream << "[t] = " << time_standard << " seconds" << std::endl;
  stream << "[v] = " << velocity_standard << " meters per seconds" << std::endl;
  stream << "[a] = " << acceleration_standard << " meters per seconds^2"
         << std::endl;
  stream << "[f] = " << force_standard << " Newtons" << std::endl;
  stream << "[s] = " << surface_standard << " meter^2" << std::endl;
  stream << "[V] = " << volume_standard << " meter^3" << std::endl;
  stream << "[P] = " << pressure_standard << " Pascals" << std::endl;
  stream << "[rho] = " << mass_density_standard << " Kg per meter^3"
         << std::endl;
  stream << "mvv2e = " << mvv2e << std::endl;
  stream << "f_m2v_t = " << f_m2v_t << std::endl;
  stream << "ft_m2v = " << ft_m2v << std::endl;
  stream << "mv_t2f = " << mv_t2f << std::endl;
  stream << "kT2e = " << kT2e << std::endl;
  stream << "kT2fd = " << kT2fd << std::endl;
  stream << "m_tt2f_d = " << m_tt2f_d << std::endl;
  stream << "fd2e = " << fd2e << std::endl;
}

/* -------------------------------------------------------------------------- */

UnitSystem& UnitSystem::parseUnitSystemName(const std::string& name) {
  //Since we are returning reference to global objects there
  // is no problem for dangling references
  if (name == "SIUnits")
    return si_unit_system;
  else if (name == "RealUnits")
    return real_unit_system;
  else if (name == "MetalUnits")
    return metal_unit_system;
  else
    LM_FATAL("unknown Units system " << name);
}

/* -------------------------------------------------------------------------- */

UnitSystem setRealSystemUnits() {
  UnitSystem units;
  units.distance_standard = 1e-10;
  units.mass_standard = g_mol2Kg;
  units.energy_standard = kcal_mol2Joule;
  units.time_standard = 1e-15;
  units.temperature_standard = 1.;
  units.init();
  return units;
}
/* -------------------------------------------------------------------------- */

UnitSystem setSISystemUnits() {
  UnitSystem units;
  units.distance_standard = 1.;
  units.mass_standard = 1.;
  units.energy_standard = 1.;
  units.time_standard = 1.;
  units.temperature_standard = 1.;
  units.init();
  return units;
}
/* -------------------------------------------------------------------------- */

UnitSystem setMetalSystemUnits() {
  UnitSystem units;
  units.distance_standard = 1e-10;
  units.mass_standard = g_mol2Kg;
  units.energy_standard = eV2Joule;
  units.time_standard = 1e-12;
  units.temperature_standard = 1.;
  units.init();
  return units;
}

/* -------------------------------------------------------------------------- */

void setCodeUnitSystem(const std::string& u) {
  code_unit_system = UnitSystem::parseUnitSystemName(u);
}
/* -------------------------------------------------------------------------- */

void printCodeUnitSystem(bool toStdout) {
  if(toStdout) { //printing to stdout
    code_unit_system.printself(std::cout);
  } else {
    code_unit_system.print();	//will write to dump stream.
  }
}
/* -------------------------------------------------------------------------- */

bool isCodeUnitSystemSetTo(const std::string& systemName) {
  return UnitSystem::parseUnitSystemName(systemName) == code_unit_system;
}

/* -------------------------------------------------------------------------- */

UnitSystem real_unit_system = setRealSystemUnits();
UnitSystem metal_unit_system = setMetalSystemUnits();
UnitSystem si_unit_system = setSISystemUnits();
UnitSystem code_unit_system = real_unit_system;

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
