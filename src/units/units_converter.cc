/**
 * @file   units_converter.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  This is the units converter of LM
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "units_converter.hh"
#include "lm_common.hh"
#include <limits>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

UnitsConverter::UnitsConverter() {
  conversion_length = std::numeric_limits<Real>::quiet_NaN();
  conversion_mass = std::numeric_limits<Real>::quiet_NaN();
  conversion_energy = std::numeric_limits<Real>::quiet_NaN();
  conversion_time = std::numeric_limits<Real>::quiet_NaN();
  conversion_mass_density = std::numeric_limits<Real>::quiet_NaN();
  conversion_force = std::numeric_limits<Real>::quiet_NaN();
  conversion_pressure = std::numeric_limits<Real>::quiet_NaN();
}

/* -------------------------------------------------------------------------- */

void UnitsConverter::setInUnits(UnitSystem u_in) { unit_in = u_in; }

/* -------------------------------------------------------------------------- */

void UnitsConverter::setOutUnits(UnitSystem u_out) { unit_out = u_out; }

/* -------------------------------------------------------------------------- */

void UnitsConverter::computeConversions() {
  conversion_length = unit_in.distance_standard / unit_out.distance_standard;
  conversion_mass = unit_in.mass_standard / unit_out.mass_standard;
  conversion_energy = unit_in.energy_standard / unit_out.energy_standard;
  conversion_time = unit_in.time_standard / unit_out.time_standard;

  conversion_mass_density =
      unit_in.mass_density_standard / unit_out.mass_density_standard;
  conversion_force = unit_in.force_standard / unit_out.force_standard;
  conversion_pressure = unit_in.pressure_standard / unit_out.pressure_standard;
  conversion_temperature =
      unit_in.temperature_standard / unit_out.temperature_standard;
}

/* -------------------------------------------------------------------------- */

void UnitsConverter::print() {
  unit_in.print();
  unit_out.print();
  DUMPBYPROC(" conversion_distance " << conversion_length, DBG_MESSAGE, 0);
  DUMPBYPROC(" conversion_mass " << conversion_mass, DBG_MESSAGE, 0);
  DUMPBYPROC(" conversion_energy " << conversion_energy, DBG_MESSAGE, 0);
  DUMPBYPROC(" conversion_time " << conversion_time, DBG_MESSAGE, 0);
  DUMPBYPROC(" conversion_mass_density " << conversion_mass_density,
             DBG_MESSAGE, 0);
  DUMPBYPROC(" conversion_force " << conversion_force, DBG_MESSAGE, 0);
  DUMPBYPROC(" conversion_pressure " << conversion_pressure, DBG_MESSAGE, 0);
}

/* -------------------------------------------------------------------------- */

void UnitsConverter::checkInit(Real c) {
  LM_ASSERT(!std::isnan(c),
            "unitconverter object was badly initialized"
                << " : did you call computeConversions ?");
}

/* -------------------------------------------------------------------------- */
template <> Real UnitsConverter::getConversion<Length>() {
  checkInit(conversion_length);
  return conversion_length;
}

/* -------------------------------------------------------------------------- */

template <> Real UnitsConverter::getConversion<Mass>() {
  checkInit(conversion_mass);
  return conversion_mass;
}
/* -------------------------------------------------------------------------- */

template <> Real UnitsConverter::getConversion<Energy>() {
  checkInit(conversion_energy);
  return conversion_energy;
}
/* -------------------------------------------------------------------------- */

template <> Real UnitsConverter::getConversion<Time>() {
  checkInit(conversion_time);
  return conversion_time;
}
/* -------------------------------------------------------------------------- */

template <> Real UnitsConverter::getConversion<MassDensity>() {
  checkInit(conversion_mass_density);
  return conversion_mass_density;
}
/* -------------------------------------------------------------------------- */

template <> Real UnitsConverter::getConversion<Force>() {
  checkInit(conversion_force);
  return conversion_force;
}
/* -------------------------------------------------------------------------- */

template <> Real UnitsConverter::getConversion<Pressure>() {
  checkInit(conversion_pressure);
  return conversion_pressure;
}
/* -------------------------------------------------------------------------- */

template <> Real UnitsConverter::getConversion<Temperature>() {
  checkInit(conversion_temperature);
  return conversion_temperature;
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
