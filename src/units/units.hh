/**
 * @file   units.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Jul 02 16:25:38 2014
 *
 * @brief  This is the units system of LM
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_UNITS_HH__
#define __LIBMULTISCALE_UNITS_HH__
/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
#include <cmath>
#include <sstream>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

const Real avogadro = 6.02214179e23;
const Real boltzmann = 1.3806503e-23;
/* energy conversions */
const Real cal2Joule = 4.184;
const Real kcal2Joule = 1e3 * cal2Joule;
const Real kcal_mol2Joule = kcal2Joule / avogadro;
const Real eV2Joule = 1.60217646e-19;
/* time conversions */
const Real fs2s = 1e-15;
const Real ps2s = 1e-12;
/* mass conversions */
const Real g2Kg = 1e-3;
const Real g_mol2Kg = g2Kg / avogadro;
/* distance conversions */
const Real A2meter = 1e-10;
/* -------------------------------------------------------------------------- */

/* unit system is described by the values of the standards in the SI units */

class UnitSystem {

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

public:
  Real distance_standard;
  Real mass_standard;
  Real energy_standard;
  Real time_standard;
  Real temperature_standard;

  /********** computed values ****************/
  Real velocity_standard;
  Real acceleration_standard;
  Real force_standard;
  Real surface_standard;
  Real volume_standard;
  Real mass_density_standard;
  Real pressure_standard;

  /* conversion from mv^2 to energy */
  Real mvv2e;
  /* conversion f/m to acceleration */
  Real f_m2v_t;
  /* conversion f/m*t to velocity */
  Real ft_m2v;
  /* conversion energy to temperature */
  Real mv_t2f;
  /* conversion from kbT to energy */
  Real kT2e;
  /* conversion from kbT to force.distance */
  Real kT2fd;
  /* conversion from mass/time^2 to force/distance */
  Real m_tt2f_d;
  /* conversion from energy/distance^2 to mass/time^2 */
  Real e_dd2m_tt;
  /* conversion from energy to force.distance */
  Real e2fd;
  /* conversion from energy/mass to time */
  Real e_m2dd_tt;
  /* conversion from force*distance to energy (work) */
  Real fd2e;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  //! test equality
  bool operator==(const UnitSystem &s) const;
  bool operator!=(const UnitSystem &s) const;

  //! compute values
  void init();

  virtual void print() const;

  virtual void printself(std::ostream &stream) const;

  //! Parses a string and returns the appropriate unit system.
  static UnitSystem& parseUnitSystemName(const std::string& name);
};
/* -------------------------------------------------------------------------- */

inline std::ostream &operator<<(std::ostream &os, UnitSystem usys) {
  usys.printself(os);
  return os;
}

/* -------------------------------------------------------------------------- */
//! static variable representing the atomic unit system (see real in LAMMPS)
EXTERN UnitSystem real_unit_system;
//! static variable representing the metallic unit system (see metal LAMMPS)
EXTERN UnitSystem metal_unit_system;
//! static variable representing the real unit system SI
EXTERN UnitSystem si_unit_system;
//! the variable which decides what are the internal code units
EXTERN UnitSystem code_unit_system;
/* -------------------------------------------------------------------------- */
UnitSystem setAtomicSystemUnits();
/* -------------------------------------------------------------------------- */
UnitSystem setRealSystemUnits();
/* -------------------------------------------------------------------------- */
UnitSystem setMetalSystemUnits();
/* -------------------------------------------------------------------------- */
//! Set the code unit system from a string.
void setCodeUnitSystem(const std::string& systemName);
/* -------------------------------------------------------------------------- */
//! Print the currently set code unit system.
void printCodeUnitSystem(bool toStdout = false);
/* -------------------------------------------------------------------------- */
//! Checks if the code unit system is set to the specified unit system
bool isCodeUnitSystemSetTo(const std::string& systemName);
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_UNITS_HH__ */
