/**
 * @file   units_converter.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  This is the units converter of LM
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_UNITS_CONVERTER_HH__
#define __LIBMULTISCALE_UNITS_CONVERTER_HH__
/* -------------------------------------------------------------------------- */
#include "units.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class UnitsConverter {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  UnitsConverter();

  /* ------------------------------------------------------------------------ */
  /* Accessors                                                                */
  /* ------------------------------------------------------------------------ */

  void setInUnits(UnitSystem u_in);
  void setOutUnits(UnitSystem u_out);

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

  void computeConversions();
  void print();

  template <PhysicalQuantity q> Real getConversion();

private:
  void checkInit(Real c);

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

  Real conversion_length;
  Real conversion_mass;
  Real conversion_energy;
  Real conversion_time;
  Real conversion_mass_density;
  Real conversion_force;
  Real conversion_pressure;
  Real conversion_temperature;

  UnitSystem unit_in;
  UnitSystem unit_out;
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_UNITS_CONVERTER_HH__ */
