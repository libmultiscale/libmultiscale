/**
 * @file   math_tools.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Peter Spijker <peter.spijker@epfl.ch>
 *
 * @date   Thu Feb 06 09:26:56 2014
 *
 * @brief  This provides access to a set of math functions for various usage
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_MATH_TOOLS_HH__
#define __LIBMULTISCALE_MATH_TOOLS_HH__
/* -------------------------------------------------------------------------- */
#include "lm_communicator.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

namespace MathTools {

/* series zeta(puis,ordre) = sum_{k=1}^{ordre} \frac{1}{k^{puis}} */
Real zeta(UInt puis, UInt ordre);
/* set seed for random generator */
void setSeed(long s);
/* A uniform distribution (between a and b) */
double uniformRandom(double a, double b);
/* A uniform distribution (between 0 and 1) */
Real uniform();
/* A normal (Gaussian) distribution (with mean mu and stdev sig) */
Real gaussian();
/* function to compute standard deviation of a set of values stored in an array
 */
Real stdev(Real *x, Real mu, UInt n);
Real stdev(std::vector<Real> &x, Real mu);
/* function to compute teh kurtosis of a set of values stored in an array */
Real kurtosis(Real *x, Real mu, Real std, UInt n);
/* function to compute teh skewness of a set of values stored in an array */
Real skewness(Real *x, Real mu, Real std, UInt n);
/* function to compute the average of a set of values stored in an array */
Real average(std::vector<Real> &x);
Real average(std::vector<Real> &x);

inline Real ipow(Real x, UInt y) {
  UInt i;
  Real res = 1.0;
  for (i = y; i > 0; --i) {
    res *= x;
  }
  return res;
}
/* -------------------------------------------------------------------------- */
inline Real sign(Real x) {
  if (x >= 0)
    return 1;
  else
    return -1;
}

/* -------------------------------------------------------------------------- */
template <typename T> class InterfaceOperator {
public:
  InterfaceOperator() = default;

  template <typename _T = T> std::enable_if_t<std::is_arithmetic_v<_T>> init() {
    res = 0;
    cpt = 0;
  }

  template <typename _T = T>
  std::enable_if_t<not std::is_arithmetic_v<_T>> init(UInt rows = 0,
                                                      UInt cols = 1) {
    res.resize(rows, cols);
    res.setZero();
    cpt = 0;
  }
  virtual T result(LMID) { return res; };

protected:
  //! to store the result
  T res;
  //! often one need a counter
  UInt cpt;
  //! the volume of the current box
  Real vol;
};

template <Operator Op, typename T> class ReduceOperator;
/* -------------------------------------------------------------------------- */

template <typename T>
class ReduceOperator<OP_AVERAGE, T> : public InterfaceOperator<T> {
public:
  template <typename _T> void account(_T &&el) {
    this->res += el;
    ++this->cpt;
  }
  virtual T result(CommGroup group = Communicator::getGroup("all")) {
    group.allReduce(this->res, "AverageOperator reduce res", OP_SUM);
    group.allReduce(this->cpt, "AverageOperator reduce cpt", OP_SUM);
    DUMP("make " << this->res << " / " << this->cpt, DBG_ALL);
    this->res /= this->cpt;
    return this->res;
  };
};
/* -------------------------------------------------------------------------- */
template <typename T>
class ReduceOperator<OP_SUM, T> : public InterfaceOperator<T> {
public:
  template <typename _T> void account(_T &&el) {
    this->res += el;
    ++this->cpt;
  }
  virtual T result(CommGroup group = Communicator::getGroup("all")) {
    group.allReduce(this->res, "SumOperator reduce res", OP_SUM);
    return this->res;
  };
};
/* -------------------------------------------------------------------------- */
template <typename T>
class ReduceOperator<OP_DENSITY, T> : public ReduceOperator<OP_AVERAGE, T> {
public:
  virtual T result(CommGroup group = Communicator::getGroup("all")) {
    this->res = T();
    group.allReduce(this->res, "DensityOperator reduce res", OP_SUM);
    this->res /= this->vol;
    return this->res;
  };
};
/* -------------------------------------------------------------------------- */
template <typename T>
class ReduceOperator<OP_DEVIATION, T> : public InterfaceOperator<T> {
public:
  template <typename _T = T> std::enable_if_t<std::is_arithmetic_v<_T>> init() {
    InterfaceOperator<T>::init();
    m2 = 0.;
  }
  template <typename _T = T>
  std::enable_if_t<not std::is_arithmetic_v<_T>> init(UInt rows = 0,
                                                      UInt cols = 1) {
    InterfaceOperator<T>::init(rows, cols);
    m2.resize(rows, cols);
    m2.setZero();
  }
  T result(LMID group = "group_none") {

    if (group != "group_none") {
      auto comm_group = Communicator::getGroup(group);
      comm_group.allReduce(&this->res, 1, "DeviationOperator reduce res",
                           OP_SUM);
      comm_group.allReduce(&this->m2, 1, "DeviationOperator reduce m2", OP_SUM);
      comm_group.allReduce(&this->cpt, 1, "DeviationOperator reduce cpt",
                           OP_SUM);
    }

    m2 /= this->cpt;
    this->res /= this->cpt;
    T res2 = this->res;
    res2 *= this->res;

    T res_deviation = m2;
    res_deviation -= res2;
    return res_deviation;
  }

  void account(T &el) {
    T el2 = el;
    el2 *= el;

    this->res += el;
    m2 += el2;

    ++this->cpt;
  }

  T m2;
};
/* -------------------------------------------------------------------------- */
template <typename T>
class ReduceOperator<OP_MAX, T> : public InterfaceOperator<T> {
public:
  template <typename _T>
  std::enable_if_t<std::is_same_v<std::decay_t<_T>, Real>> account(_T &&el) {
    this->res = std::max(el, this->res);
  }
  template <typename _T>
  std::enable_if_t<not std::is_same_v<std::decay_t<_T>, Real>>
  account(_T &&el) {
    this->res = el.maxCoeff();
  }

  T result(CommGroup group = Communicator::getGroup("all")) {
    group.allReduce(this->res, "MaxOperator reduce res", OP_MAX);
    return this->res;
  }
};
/* -------------------------------------------------------------------------- */
template <typename T>
class ReduceOperator<OP_MIN, T> : public InterfaceOperator<T> {
public:
  template <typename _T>
  std::enable_if_t<std::is_same_v<std::decay_t<_T>, Real>> account(_T &&el) {
    this->res = std::min(el, this->res);
  }
  template <typename _T>
  std::enable_if_t<not std::is_same_v<std::decay_t<_T>, Real>>
  account(_T &&el) {
    this->res = el.minCoeff();
  }

  T result(CommGroup group = Communicator::getGroup("all")) {
    group.allReduce(this->res, "MinOperator reduce res", OP_MIN);
    return this->res;
  }
};
/* -------------------------------------------------------------------------- */
template <typename T> using AverageOperator = ReduceOperator<OP_AVERAGE, T>;
template <typename T> using DensityOperator = ReduceOperator<OP_DENSITY, T>;
template <typename T> using DeviationOperator = ReduceOperator<OP_DEVIATION, T>;
template <typename T> using MaxOperator = ReduceOperator<OP_MAX, T>;
template <typename T> using MinOperator = ReduceOperator<OP_MIN, T>;
template <typename T> using SumOperator = ReduceOperator<OP_SUM, T>;

} // namespace MathTools
__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_MATH_TOOLS_HH__ */
