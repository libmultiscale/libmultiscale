/**
 * @file   spline_function.hh
 *
 * @author Srinivasa Babu Ramisetti <srinivasa.ramisetti@epfl.ch>
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Dec 04 12:14:29 2013
 *
 * @brief  This is a generic function using a spline interpolation
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_SPLINE_FUNCTION_HH__
#define __LIBMULTISCALE_SPLINE_FUNCTION_HH__
/* -------------------------------------------------------------------------- */
#include "cubic_spline.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/** Class SplineFunction
 *
 *  This class permits to take an analytical function and
 *  generates regular points over a domain to allow spline
 *  interpolation
 */

template <typename Function> class SplineFunction {
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  SplineFunction(Function &f) : func(f){};
  virtual ~SplineFunction(){};

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  template <UInt derivation_order> inline Real compute(Real x);

  void init(Real min, Real max, UInt n_points, UInt max_order);

  template <UInt derivation_order> void initSample(std::vector<Real> &f);

  /* ------------------------------------------------------------------------ */
  /* Accessors                                                                */
  /* ------------------------------------------------------------------------ */
public:
  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */
private:
  Function func;

  //! x coordinates
  std::vector<Real> x;
  //! per derivation order sample of the function
  std::vector<std::vector<Real>> samples;
  //! per derivation order spline objects
  std::vector<CubicSpline> splines;
};
/* -------------------------------------------------------------------------- */

template <typename Function>
template <UInt order>
inline Real SplineFunction<Function>::compute(Real x) {

  Real res = 0.0;
  splines[order].fastEvaluateSpline(x, res);
  return res;
}
/* -------------------------------------------------------------------------- */

template <typename Function>
template <UInt order>
inline void SplineFunction<Function>::initSample(std::vector<Real> &f) {

  // std::stringstream str;
  // str << "test-function-" << order << ".gplot";
  // std::ofstream file(str.str().c_str());

  UInt n_points = x.size();
  for (UInt i = 0; i < n_points; ++i) {
    Real value = func.template compute<order>(x[i]);
    f.push_back(value);
    //    file << x[i] << "\t" << value << std::endl;
  }
  //  file.close();
}
/* -------------------------------------------------------------------------- */

template <typename Function>
inline void SplineFunction<Function>::init(Real min, Real max, UInt n_points,
                                           UInt max_order) {

  // make up the x coordinates
  Real step = (max - min) / (n_points - 1);
  x.reserve(n_points);
  for (UInt i = 0; i < n_points; ++i) {
    x.push_back(min + step * i);
  }

  samples.resize(max_order);
  splines.resize(max_order);
  for (UInt order = 0; order < max_order; ++order) {
    std::vector<Real> &f = samples[order];
    switch (order) {
    case 0:
      initSample<0>(f);
      break;
    case 1:
      initSample<1>(f);
      break;
    case 2:
      initSample<2>(f);
      break;
    case 3:
      initSample<3>(f);
      break;
    default:
      LM_FATAL("switch should be extended ?");
    }
    splines[order].buildSplineCoefficients(x, f, step);
  }
}

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_SPLINE_FUNCTION_HH__ */
