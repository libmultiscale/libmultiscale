/**
 * @file   gaussian_function.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Jan 07 16:36:30 2013
 *
 * @brief  This is a gaussian function
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_GAUSSIAN_FUNCTION_HH__
#define __LIBMULTISCALE_GAUSSIAN_FUNCTION_HH__
/* -------------------------------------------------------------------------- */
#include "function_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/** Class GaussianFunction
 *
 *  This class implements a gaussian function principally for tests
 */

class GaussianFunction : public FunctionInterface {
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  GaussianFunction(Real v) {
    T = v;
    T2 = T * T;
    _1_T2 = 1. / T2;
    _1_T4 = _1_T2 * _1_T2;
  };
  virtual ~GaussianFunction(){};

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  template <UInt derivation_order> inline Real compute(Real x);

  /* ------------------------------------------------------------------------ */
  /* Accessors                                                                */
  /* ------------------------------------------------------------------------ */
public:
  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */
private:
  Real T;
  Real T2;
  Real _1_T2;
  Real _1_T4;
};
/* -------------------------------------------------------------------------- */

template <UInt derivation_order> inline Real GaussianFunction::compute(Real x) {
  LM_TOIMPLEMENT;
}
/* -------------------------------------------------------------------------- */

template <> inline Real GaussianFunction::compute<0>(Real x) {
  return exp(-1. * x * x / T2);
}
/* -------------------------------------------------------------------------- */

template <> inline Real GaussianFunction::compute<1>(Real x) {
  return -2 * _1_T2 * x * compute<0>(x);
}
/* -------------------------------------------------------------------------- */

template <> inline Real GaussianFunction::compute<2>(Real x) {
  return -2 * _1_T2 * (1 - 2 * x * x * _1_T2) * compute<0>(x);
}
/* -------------------------------------------------------------------------- */

template <> inline Real GaussianFunction::compute<3>(Real x) {
  return 4 * x * _1_T4 * (3 - 2 * x * x * _1_T2) * compute<0>(x);
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_GAUSSIAN_FUNCTION_HH__ */
