/**
 * @file   product_function.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Jan 07 16:36:30 2013
 *
 * @brief  This allows to create a function based on the product between two
 * other functions
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_PRODUCT_FUNCTION_HH__
#define __LIBMULTISCALE_PRODUCT_FUNCTION_HH__
/* -------------------------------------------------------------------------- */
#include "function_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/** Class ProductFunction
 *
 *  This class permits to multiply to functions and get the proper derivatives
 */

template <typename Function1, typename Function2>
class ProductFunction : public FunctionInterface {
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  ProductFunction(Function1 &f1, Function2 &f2) : func1(f1), func2(f2){};
  virtual ~ProductFunction(){};

  template <UInt derivation_order, bool fps = true> class myComputeFunctor {
  public:
    static Real compute(Real x, Function1 &func1, Function2 &func2);
  };

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  template <UInt derivation_order> inline Real compute(Real x);

  /* ------------------------------------------------------------------------ */
  /* Accessors                                                                */
  /* ------------------------------------------------------------------------ */
public:
  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */
private:
  Function1 func1;
  Function2 func2;
};

/* -------------------------------------------------------------------------- */

template <typename Function1, typename Function2>
template <UInt order>
inline Real ProductFunction<Function1, Function2>::compute(Real x) {
  return myComputeFunctor<order>::compute(x, func1, func2);
}
/* -------------------------------------------------------------------------- */

template <typename Function1, typename Function2>
template <UInt derivation_order, bool fps>
inline Real ProductFunction<Function1, Function2>::myComputeFunctor<
    derivation_order, fps>::compute(Real x, Function1 &func1,
                                    Function2 &func2) {
  LM_TOIMPLEMENT;
}
/* -------------------------------------------------------------------------- */

template <typename Function1, typename Function2>
template <bool fps>
class ProductFunction<Function1, Function2>::myComputeFunctor<0, fps> {
public:
  static Real compute(Real x, Function1 &func1, Function2 &func2) {
    return func1.template compute<0>(x) * func2.template compute<0>(x);
  }
};
/* -------------------------------------------------------------------------- */

template <typename Function1, typename Function2>
template <bool fps>
class ProductFunction<Function1, Function2>::myComputeFunctor<1, fps> {
public:
  static Real compute(Real x, Function1 &func1, Function2 &func2) {
    return func1.template compute<0>(x) * func2.template compute<1>(x) +
           func1.template compute<1>(x) * func2.template compute<0>(x);
  }
};
/* -------------------------------------------------------------------------- */

template <typename Function1, typename Function2>
template <bool fps>
class ProductFunction<Function1, Function2>::myComputeFunctor<2, fps> {
public:
  static Real compute(Real x, Function1 &func1, Function2 &func2) {
    return func1.template compute<0>(x) * func2.template compute<2>(x) +
           2. * func1.template compute<1>(x) * func2.template compute<1>(x) +
           func1.template compute<2>(x) * func2.template compute<0>(x);
  }
};
/* -------------------------------------------------------------------------- */

template <typename Function1, typename Function2>
template <bool fps>
class ProductFunction<Function1, Function2>::myComputeFunctor<3, fps> {
public:
  static Real compute(Real x, Function1 &func1, Function2 &func2) {
    return func1.template compute<0>(x) * func2.template compute<3>(x) +
           3. * func1.template compute<1>(x) * func2.template compute<2>(x) +
           3. * func1.template compute<2>(x) * func2.template compute<1>(x) +
           func1.template compute<3>(x) * func2.template compute<0>(x);
  }
};

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_PRODUCT_FUNCTION_HH__ */
