/**
 * @file   constant_function.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Jan 07 16:36:30 2013
 *
 * @brief  This implements a function with a constant value
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_CONSTANT_FUNCTION_HH__
#define __LIBMULTISCALE_CONSTANT_FUNCTION_HH__
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/** Class ConstantFunction
 *
 *  This class implements a constant function principally for tests
 */

class ConstantFunction {
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  ConstantFunction(Real v) { value = v; };
  virtual ~ConstantFunction(){};

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  template <UInt derivation_order> inline Real compute(Real x);

  /* ------------------------------------------------------------------------ */
  /* Accessors                                                                */
  /* ------------------------------------------------------------------------ */
public:
  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */
private:
  Real value;
};
/* -------------------------------------------------------------------------- */

template <UInt derivation_order> inline Real ConstantFunction::compute(Real x) {
  return 0.0;
}
/* -------------------------------------------------------------------------- */

template <> inline Real ConstantFunction::compute<0>(Real x) {
  if (std::abs(x) > value)
    return 0.0;
  else
    return 1.0;
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_CONSTANT_FUNCTION_HH__ */
