/**
 * @file   hamming_window.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Jan 07 16:36:30 2013
 *
 * @brief  This implements the hamming window
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_HAMMING_WINDOW_HH__
#define __LIBMULTISCALE_HAMMING_WINDOW_HH__
/* -------------------------------------------------------------------------- */
#include "function_interface.hh"
#include <gsl/gsl_sf_bessel.h>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/** Class Hammingwindow
 *
 *  This class implements access to the hamming window function
 *  @f$ F(x) = 0.54 + 0.46 \cdot cos(\frac{\pi x}{L}) @f$
 *
 */

#define coeff1 0.54
#define coeff2 0.46

class HammingWindow : public FunctionInterface {
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  HammingWindow(Real L) {
    this->L = L;
    phase = M_PI / L;
  };
  virtual ~HammingWindow(){};

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  template <UInt derivation_order> inline Real compute(Real x);

  /* ------------------------------------------------------------------------ */
  /* Accessors                                                                */
  /* ------------------------------------------------------------------------ */
public:
  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */
private:
  Real L;
  Real phase;
};
/* -------------------------------------------------------------------------- */

template <UInt derivation_order> inline Real HammingWindow::compute(Real x) {
  LM_FATAL("order " << derivation_order << "not defined yet");
  return 0.0;
}
/* -------------------------------------------------------------------------- */

template <> inline Real HammingWindow::compute<0>(Real x) {
  if (x < L) {
    Real tmp = coeff2 * cos(phase * x);
    return coeff1 + tmp;
  }
  return 0.0;
}
/* -------------------------------------------------------------------------- */

template <> inline Real HammingWindow::compute<1>(Real x) {
  if (x < L)
    return -coeff2 * sin(phase * x) * phase;
  return 0.0;
}
/* -------------------------------------------------------------------------- */

template <> inline Real HammingWindow::compute<2>(Real x) {
  if (x < L)
    return -coeff2 * cos(phase * x) * phase * phase;
  return 0.0;
}
/* -------------------------------------------------------------------------- */

template <> inline Real HammingWindow::compute<3>(Real x) {
  if (x < L)
    return coeff2 * sin(phase * x) * phase * phase * phase;
  return 0.0;
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#undef coeff1
#undef coeff2

#endif /* __LIBMULTISCALE_HAMMING_WINDOW_HH__ */
