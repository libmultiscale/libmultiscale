/**
 * @file   ref_node_dd.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 * @author Max Hodapp <max.hodapp@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 *
 * @date   Fri Nov 15 14:49:04 2013
 *
 * @brief  Generic reference to DD nodes
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_REF_ELEM_DD_HH__
#define __LIBMULTISCALE_REF_ELEM_DD_HH__
/* ----------------------------------------------------------------- */
#include "container_mesh.hh"
#include "lm_common.hh"
#include "ref_element.hh"
#include "ref_node_dd.hh"
/* ----------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* ----------------------------------------------------------------- */
template <typename Ref, FieldType ftype>
struct AccessorDDElemDof : public AccessorDof<Ref, ftype> {
  using AccessorDof<Ref, ftype>::AccessorDof;
  using AccessorDof<Ref, ftype>::operator=;
};
/* -------------------------------------------------------------------------- */

// template <FieldType ftype> struct getField {
//   template <typename Ref> static inline auto get(Ref &&ref) {
//     LM_TOIMPLEMENT;
//     return Vector<1>();
//   }
// };

// template <> struct getField<_burgers> {
//   template <typename Ref> static inline decltype(auto) get(Ref &&ref) {
//     return ref.burgers();
//   }
// };

// template <> struct getField<_normal> {
//   template <typename Ref> static inline decltype(auto) get(Ref &&ref) {
//     return ref.normal();
//   }
// };

/* ----------------------------------------------------------------- */

/**
 * Class RefDDElem
 *
 */

template <UInt _Dim, typename DaughterClass>
class RefDDElem : public RefElement<DaughterClass> {

public:
  static constexpr UInt Dim = _Dim;

#define declare_accessor(_name)                                         \
  auto _name() { return static_cast<DaughterClass *>(this)->_name(); }
  declare_accessor(burgers);
  declare_accessor(normal);

#undef declare_accessor

  std::vector<UInt> globalIndexes() override { LM_TOIMPLEMENT; };

  template <FieldType ftype>
  AccessorDDElemDof<RefDDElem<_Dim, DaughterClass>, ftype> field() {
    AccessorDDElemDof<RefDDElem<_Dim, DaughterClass>, ftype> acc(*this);
    return acc;
  }
  virtual void setConnectivity(std::vector<UInt> &) { LM_TOIMPLEMENT; }
};

/* ------------------------------------------------------------------------ */

template <UInt _Dim>
class RefGenericDDElem : public RefDDElem<_Dim, RefGenericDDElem<_Dim>> {

public:
  using fields = field_list<_burgers, _normal>;
  
  Vector<_Dim> burgers() { LM_TOIMPLEMENT; }
  Vector<_Dim> normal() { LM_TOIMPLEMENT; }
  std::vector<UInt> globalIndexes() override { LM_TOIMPLEMENT; }
  std::vector<UInt> localIndexes() override { LM_TOIMPLEMENT; }
  virtual void setConnectivity(std::vector<UInt> &) { LM_TOIMPLEMENT; }

  RefGenericDDNode<_Dim> &getNode(UInt) { LM_TOIMPLEMENT; }

  bool operator==(RefGenericDDElem<_Dim> &) { LM_TOIMPLEMENT; }

  RefGenericDDElem getMirrorElem() { LM_TOIMPLEMENT; }
};

/* ------------------------------------------------------------------------ */

template <UInt Dim>
using ContainerGenericDDMesh =
    ContainerMesh<ContainerArray<RefGenericDDNode<Dim>>,
                  ContainerArray<RefGenericDDElem<Dim>>>;

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_REF_ELEM_DD_HH__ */
