/**
 * @file   domain_dd.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Jul 11 15:47:44 2014
 *
 * @brief  Common mother for all DD domain
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "domain_dd.hh"
#include "geometry_manager.hh"
#include "lib_dd.hh"
#include "lm_common.hh"
#include "lm_communicator.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

template <typename ContNodes, typename ContElems, UInt Dim>
typename DomainDD<ContNodes, ContElems, Dim>::Container &
DomainDD<ContNodes, ContElems, Dim>::getContainer() {

  if (getCommGroup().amIinGroup()) {
    container_dd.getBoundingBox() = getDomainBoundingBox();
  }
  container_dd.setCommGroup(getCommGroup());
  return container_dd;
}

/* -------------------------------------------------------------------------- */

/* LMDESC DomainDD
   This section describe the section that is associated
   with dislocation dynamics models. It is the starting
   point of every DD plugin.
*/

/* LMHERITANCE domain_interface */

template <typename ContNodes, typename ContElems, UInt Dim>
void DomainDD<ContNodes, ContElems, Dim>::declareParams() {

  DomainInterface::declareParams();

  /* LMKEYWORD DOMAIN_GEOMETRY
     Specify the geometry/region describing the domain
  */
  this->parseKeyword("DOMAIN_GEOMETRY", geom);
}
/* -------------------------------------------------------------------------- */

#ifdef LIBMULTISCALE_USE_PARADIS
template class DomainDD<ContainerNodesParaDiS, ContainerElemsParaDiS, 3>;
#endif
__END_LIBMULTISCALE__
