/**
 * @file   ref_node_dd.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Till Junge <till.junge@epfl.ch>
 * @author Max Hodapp <max.hodapp@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 *
 * @date   Fri Nov 15 14:49:04 2013
 *
 * @brief  Generic reference to DD nodes
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_REF_NODE_DD_HH__
#define __LIBMULTISCALE_REF_NODE_DD_HH__
/* ----------------------------------------------------------------- */
#include "accessor_dof.hh"
#include "ref_point.hh"
/* ----------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* ----------------------------------------------------------------- */
template <UInt Dim, typename DaughterClass> class RefDDNode;
/* ----------------------------------------------------------------- */
template <typename Ref, FieldType ftype>
struct AccessorDDNodalDof : public AccessorDof<Ref, ftype> {
  using AccessorDof<Ref, ftype>::AccessorDof;
  using AccessorDof<Ref, ftype>::operator=;
};
/* ---------------------------------------------------------------------- */

/**
 * Class RefDDNode
 *
 */

template <UInt Dim, typename DaughterClass>
class RefDDNode : public RefPoint<Dim> {

public:
  static constexpr UInt dim = Dim;

  virtual ~RefDDNode(){};
  RefDDNode(){};

#define declare_accessor(_name)                                                \
  auto _name() { return static_cast<DaughterClass *>(this)->_name(); }

  declare_accessor(position0);
  declare_accessor(position);
  declare_accessor(velocity);
  declare_accessor(force);

#undef declare_accessor

  template <FieldType ftype>
  AccessorDDNodalDof<RefDDNode<Dim, DaughterClass>, ftype> field() {
    AccessorDDNodalDof<RefDDNode<Dim, DaughterClass>, ftype> acc(*this);
    return acc;
  }
};

/* ------------------------------------------------------------------------ */

template <UInt Dim>
class RefGenericDDNode : public RefDDNode<Dim, RefGenericDDNode<Dim>> {

public:
  RefGenericDDNode() { LM_TOIMPLEMENT; }

  using fields = field_list<_position0, _position, _velocity, _force>;

  auto &position() { return pos; }
  Vector<Dim> previous_position() { LM_TOIMPLEMENT; }
  auto &position0() { return pos0; }
  auto &velocity() { return vel; }
  auto &force() { return f; }

  UInt getIndex() { LM_TOIMPLEMENT; }
  
  inline bool operator==(RefGenericDDNode &) { LM_TOIMPLEMENT; };

private:
  Vector<Dim> pos;
  Vector<Dim> pos0;
  Vector<Dim> vel;
  Vector<Dim> f;
};

/* -------------------------------------------------------------------------- */

template <UInt Dim>
inline std::ostream &operator<<(std::ostream &os, RefGenericDDNode<Dim> &ref) {
  UInt n = ref.getIndex();

  os << "DD Node reference : " << n << " [P] " << ref.position();
  return os;
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__
#endif /* __LIBMULTISCALE_REF_NODE_DD_HH__ */
