/**
 * @file   domain_dd.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Jul 11 15:47:44 2014
 *
 * @brief  Generic DD domain
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_DOMAIN_DD_HH__
#define __LIBMULTISCALE_DOMAIN_DD_HH__
/* -------------------------------------------------------------------------- */
#include "component_libmultiscale.hh"
#include "container_mesh.hh"
#include "domain_interface.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/**
 * Class DomainDD
 *
 */
template <typename ContNodes, typename ContElems, UInt Dim>
class DomainDD : public DomainInterface {

public:
  DomainDD(LMID ID, CommGroup group)
      : DomainInterface(group), container_dd(ID) {

    createOutput("segments");
    getOutput("segments") = container_dd;
    container_dd.setCommGroup(group);
  };

  virtual ~DomainDD(){};

  void compute_make_call(){};

  using Container = ContainerMesh<ContNodes, ContElems>;
  using ContainerNodes = ContNodes;
  using ContainerElems = ContElems;
  using ContainerPoints = ContainerNodes;

  using IteratorNodes = typename ContainerNodes::iterator;
  using RefNode = typename ContainerNodes::Ref;
  using RefPoint = typename ContainerNodes::Ref;
  using RefElem = typename ContainerElems::Ref;

  using ContainerSubset = typename Container::ContainerSubset;

  //! return the global container
  Container &getContainer();

  //! return the potential energy
  Real getEpot() { return 0; };

  void build(){};

  virtual void declareParams();

  //! restart from xml file function
  void readXMLFile(const std::string &) { LM_FATAL("not yet implemented"); }

  /* ------------------------------------------------------------------------ */
  /* Functions fo AMELCG                                                      */
  /* ------------------------------------------------------------------------ */

  //! return product of force by descent direction (for AMELCG)
  Real getFdotDir() { return 0; };
  //! return max of forces (for AMELCG)
  Real getFMax() { return 0; };
  //! return max of direction vector (for AMELCG)
  Real getDirMax() { return 0; };
  //! return norm 2 of forces (for AMELCG)
  Real getFNorm2() { return 0; };
  //! return stuff for AMELCG
  Real getFdotOldF() { return 0; };
  //! update direction AMELCG
  void updateDirection(Real){};
  //! return stuff for AMELCG
  void saveForceVector(){};
  //! displace in descent direction with given magnitude
  void displaceTowardsDirection(Real){};

  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

protected:
  //! constrained geometry
  LMID geom_constrained;
  Container container_dd;
};
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_DOMAIN_DD_HH__ */
