/**
 * @file   domain_paradis.hh
 *
 * @author Till Junge <till.junge@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 * @author Max Hodapp <max.hodapp@epfl.ch>
 *
 * @date   Fri Jul 11 15:47:44 2014
 *
 * @brief  ParaDiS domain
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_DOMAIN_PARADIS_HH__
#define __LIBMULTISCALE_DOMAIN_PARADIS_HH__
/* -------------------------------------------------------------------------- */

#include "container_array.hh"
#include "container_paradis.hh"
#include "domain_dd.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/**
 * Class DomainParaDiS
 * domain to implement the plugin interface to PARADIS dislocation dynamics
 */

class DomainParaDiS
    : public DomainDD<ContainerNodesParaDiS, ContainerElemsParaDiS, 3> {

public:
  enum NodeContraint {
    _free = 0,
    _surface_node = 1,
    _fixed = 7,
    _jog = 9,
    junction = 10,
    _cross_slip = 20
  };

  DomainParaDiS(LMID ID, CommGroup group);
  virtual ~DomainParaDiS();

  //! intialisation stage
  void init() override;
  void updateGradient() override;
  void declareParams() override;
  Cube getDomainBoundingBox() const override;
  //! remesh
  void remesh() override;
  Real potentialEnergy() override { LM_TOIMPLEMENT; };
  ArrayView gradient() override { LM_TOIMPLEMENT; };
  ArrayView primal() override;
  ArrayView getField(FieldType) override { LM_TOIMPLEMENT; };
  ArrayView primalTimeDerivative() override;
  ArrayView acceleration() override;
  void enforceCompatibility() override;
  void updateAcceleration() override;

private:
  void updateVelocity();
  void updatePosition();
  ArrayView mass() override { LM_TOIMPLEMENT; };
  bool isPeriodic(UInt) override { LM_TOIMPLEMENT; };
  void imageForce(const double *position, const double *normal, double *force);
  Cube getSubDomainBoundingBox() const override;
  void setDomainBoundingBox(const Cube &) override;
  void setSubDomainBoundingBox(const Cube &) override;
  // //! parse the keywords
  UInt getNBNodes();
  bool is_it_bound();
  VectorProxy<3u> getXmin();
  VectorProxy<3u> getXmax();
  Real getShearModulus();

  void freenodearm(UInt index);
  void freenode(UInt index);

  // //! return the stress component (what is i and j ?)
  // double &get_stress_component(int i, int j) {
  //   int indices[3][3] = {{0, 5, 4}, {5, 1, 3}, {4, 3, 2}};
  //   return ParaDiSHome::paradis_ptr->param->appliedStress[indices[i][j]];
  // }

  // UInt getDim() { return 3; };
  // Real getPoisson() { return ParaDiSHome::paradis_ptr->param->pois; };
  // Real getMaxSeg() { return ParaDiSHome::paradis_ptr->param->maxSeg; };
  // Real getMinSeg() { return ParaDiSHome::paradis_ptr->param->minSeg; };
  // Real getBurgersMagnitude();
  // UInt getNBpairsInfinite();
  // bool getPairedIndex(int index0, int *index0_infinite);

  /* ------------------------------------------------------------------------ */
  /* Functions for AMELCG                                                     */
  /* ------------------------------------------------------------------------ */
  double getExternalWork() { return 0; }
  void setExternalWork(double) {}

private:
  bool boundary;
  int memSize;

  // integration timestep duration
  Quantity<Time> timestep;

  // frequency at which desired timestep should be reinforced
  UInt timestep_reset_frequency;

  UInt numDLBCycles;
  std::string control_filename;

  std::string data_filename;

  // ParaDis - Containers/Lists
  // list of ParaDis node handles of
  // "continuum" DD nodes next to the
  // artificial interface
  std::vector<Node_t *> nh_l_conInt;

  // list of ParaDis node handles confined
  // to an atomistic domain
  std::vector<Node_t *> created_nodes;

  // NOTE: vector changes every iteration depending on a dislocation detection
  // algorithm
  std::string mobility_filename;

  //! velocity field: copy from the sparse representation of ParaDiS
  Array velocities;
  //! position field: copy from the sparse representation of ParaDiS
  Array positions;
  //! acceleration field: zero
  Array accelerations;
};
/* -------------------------------------------------------------------------- */

//! Add hybrid segments to the DD model
// virtual void DomainParaDiS::addHybSegments(
//     std::vector<RefPointData<3>>
//         &positions, // DD nodes in an atomistic domain
//     std::vector<RefGenericElem<3>>
//         &conn, // connectivity between DD nodes in the atomistic domain
//     Real threshold)
//     override; // max. distance between two adjacent DD nodes possibly
//               // generating a hybrid segment

/* -------------------------------------------------------------------------- */

// bool DomainParaDiS::getPairedIndex(int index0, int *index0_infinite) {
//   UInt num_pairs = ParaDiSHome::paradis_ptr->param->numPairs;
//   for (int i = 0; i < (int)num_pairs; ++i) {
//     Pair_t pair_ind;
//     pair_ind = *ParaDiSHome::paradis_ptr->pairKeys[i];
//     if (pair_ind.index1 == index0) {
//       *index0_infinite = pair_ind.index2;
//       return true;
//     } else if (pair_ind.index2 == index0) {
//       *index0_infinite = pair_ind.index1;
//       return true;
//     }
//   }
//   return false;
// };
/* -------------------------------------------------------------------------- */

// UInt DomainParaDiS::getNBpairsInfinite() {
//   return ParaDiSHome::paradis_ptr->param->numPairs;
// };

/* -------------------------------------------------------------------------- */

// Real DomainParaDiS::getBurgersMagnitude() {
//   return ParaDiSHome::paradis_ptr->param->burgMag;
// };

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_DOMAIN_PARADIS_HH__ */
