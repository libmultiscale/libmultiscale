#ifndef __LIBMULTISCALE_PARADIS_HOME_HH__
#define __LIBMULTISCALE_PARADIS_HOME_HH__
/* -------------------------------------------------------------------------- */
#define PARALLEL
extern "C" {
#include "Home.h"
#include "Typedefs.h"
#undef X
#undef Y
}
#undef PARALLEL

/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

struct ParaDiSHome {
  static Home_t *paradis_ptr;
};

__END_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

#endif // __LIBMULTISCALE_PARADIS_HOME_HH__
