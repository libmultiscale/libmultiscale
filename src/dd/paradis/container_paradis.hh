/**
 * @file   container_paradis.hh
 *
 * @author Till Junge <till.junge@epfl.ch>
 *
 * @date   Mon Jul 28 09:52:51 2014
 *
 * @brief  ParaDiS container of DOFs
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_CONTAINER_PARADIS_HH__
#define __LIBMULTISCALE_CONTAINER_PARADIS_HH__
/* -------------------------------------------------------------------------- */
#include "container.hh"
#include "paradis_home.hh"
#include "ref_elem_paradis.hh"
#include "ref_node_paradis.hh"
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__

class ReferenceManagerParaDis {
public:
  template <typename Obj>
  void attachObject(Obj &obj, ContainerArray<RefNodeParaDiS>) {
    LM_TOIMPLEMENT;
  }
};

/* -------------------------------------------------------------------------- */

/**
 * Class ContainerParadis
 *
 */

class IteratorNodesParaDiS;

class ContainerNodesParaDiS : public virtual LMObject,
                              public Container_base<RefNodeParaDiS> {

  friend class IteratorNodesParaDiS;

public:
  static constexpr UInt Dim = 3;
  using iterator = IteratorNodesParaDiS;

  ContainerNodesParaDiS(const LMID &id)
      : LMObject(id), Container_base<RefNodeParaDiS>(), node(0){};

  ~ContainerNodesParaDiS(){};

  virtual ReferenceManagerParaDis &getRefManager() {
    LM_TOIMPLEMENT;
    return refmanager;
  };

  IteratorNodesParaDiS begin();
  IteratorNodesParaDiS end();

  RefNodeParaDiS &get(UInt index);
  inline UInt size() const;
  void init(){};

  RefNodeParaDiS node;

private:
  //! reference manager for ParaDis
  ReferenceManagerParaDis refmanager;
};

/* -------------------------------------------------------------------------- */

inline UInt ContainerNodesParaDiS::size() const {

  // return ParaDiSHome::paradis_ptr->newNodeKeyPtr -
  //        ParaDiSHome::paradis_ptr->recycledNodeHeapSize;
  if (ParaDiSHome::paradis_ptr == nullptr)
    return 0;

  unsigned int nb_elem(0);
  for (int i = 0; i < ParaDiSHome::paradis_ptr->newNodeKeyPtr; ++i) {
    if (ParaDiSHome::paradis_ptr->nodeKeys[i] != nullptr)
      ++nb_elem;
  }
  return nb_elem;
}

/* -------------------------------------------------------------------------- */
///
/// Doing elements now
///
/* -------------------------------------------------------------------------- */
class IteratorElemsParaDiS;

class ContainerElemsParaDiS : public virtual LMObject,
                              public Container_base<RefElemParaDiS> {

public:
  static constexpr UInt Dim = 3;

public:
  ContainerElemsParaDiS(const LMID &id) : LMObject(id), paradis_ptr(nullptr) {
    nodes = nullptr;
  };
  ~ContainerElemsParaDiS(){};

  using iterator = IteratorElemsParaDiS;
  friend class IteratorElemsParaDiS;

  template <typename ContMesh, typename VecBurgers, typename VecNormals>
  void add(ContMesh &segments, VecBurgers &&burgers, VecNormals &&normals);

  void setNodes(ContainerNodesParaDiS &nodes) { this->nodes = &nodes; }
  void setGrain(Home_t *home) { paradis_ptr = home; }

  //! return an associated iterator
  IteratorElemsParaDiS begin();
  IteratorElemsParaDiS end();

  //! get an item from its index
  RefElemParaDiS &get(UInt index);
  //! return the number of contained items
  inline UInt size() const;

private:
  Home_t *paradis_ptr;
  ContainerNodesParaDiS *nodes;
  RefElemParaDiS elem;
};

__END_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
#include "iterator_paradis.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

inline IteratorNodesParaDiS ContainerNodesParaDiS::begin() {
  IteratorNodesParaDiS it(*this);
  return it;
}

/* -------------------------------------------------------------------------- */

inline UInt ContainerElemsParaDiS::size() const {
  UInt cpt = 0;
  for (auto &n : *nodes) {
    cpt += n.nbArms();
  }
  return cpt / 2;
}

/* -------------------------------------------------------------------------- */

inline IteratorNodesParaDiS ContainerNodesParaDiS::end() {
  IteratorNodesParaDiS it(*this, ParaDiSHome::paradis_ptr->newNodeKeyPtr);
  return it;
}
/* -------------------------------------------------------------------------- */

inline RefNodeParaDiS &ContainerNodesParaDiS::get(unsigned int index) {
  node.setIndex(index);
  return node;
}

/* -------------------------------------------------------------------------- */

inline IteratorElemsParaDiS ContainerElemsParaDiS::begin() {
  return IteratorElemsParaDiS(*this, nodes->begin());
}
/* -------------------------------------------------------------------------- */

inline IteratorElemsParaDiS ContainerElemsParaDiS::end() {
  return IteratorElemsParaDiS(*this, nodes->end());
}
/* -------------------------------------------------------------------------- */

inline RefElemParaDiS &ContainerElemsParaDiS::get(unsigned int) { return elem; }

/* -------------------------------------------------------------------------- */

template <typename ContMesh, typename VecBurgers, typename VecNormals>
inline void ContainerElemsParaDiS::add(ContMesh &mesh, VecBurgers &&burgers,
                                       VecNormals &&normals) {

  auto &&points = mesh.getContainerNodes();
  UInt nb_points = points.size();

  std::vector<Node_t *> created_nodes;
  std::vector<UInt> numNbrs;

  // adding the points
  for (auto &&p : points) {
    Node_t *newNode = GetNewNativeNode(this->paradis_ptr);

    auto &&pos = p.position();
    newNode->x = pos[0];
    newNode->y = pos[1];
    newNode->z = pos[2];

    newNode->native = 1;
    newNode->constraint = 7;

    newNode->oldvX = 0.0;
    newNode->oldvY = 0.0;
    newNode->oldvZ = 0.0;

    AssignNodeToCell(this->paradis_ptr, newNode);

    created_nodes.push_back(newNode);
  }

  // adding the arms/elements
  numNbrs.resize(nb_points);

  auto &&elems = mesh.getContainerElems();
  for (auto &&el : elems) {
    LM_ASSERT(el.conn.size() == 2,
              "we cannot add other things than segments to DD model");
    for (UInt n = 0; n < 2; ++n) {
      UInt nd = el.conn[n];
      ++numNbrs[nd];
    }
  }

  for (UInt i = 0; i < nb_points; ++i) {
    Node_t *newNode = created_nodes[i];
    AllocNodeArms(newNode, numNbrs[i]);
    numNbrs[i] = 0;
  }

  for (auto &&el : elems) {
    UInt nd_src = el.conn[0];
    UInt nd_dst = el.conn[1];

    Node_t *srcNode = created_nodes[nd_src];
    Node_t *dstNode = created_nodes[nd_dst];

    UInt &current_arm_src = numNbrs[nd_src];
    UInt &current_arm_dst = numNbrs[nd_dst];

    srcNode->nbrTag[current_arm_src].domainID = dstNode->myTag.domainID;
    srcNode->nbrTag[current_arm_src].index = dstNode->myTag.index;

    srcNode->burgX[current_arm_src] = burgers[0];
    srcNode->burgY[current_arm_src] = burgers[1];
    srcNode->burgZ[current_arm_src] = burgers[2];

    srcNode->nx[current_arm_src] = normals[0];
    srcNode->ny[current_arm_src] = normals[1];
    srcNode->nz[current_arm_src] = normals[2];

    dstNode->nbrTag[current_arm_dst].domainID = srcNode->myTag.domainID;
    dstNode->nbrTag[current_arm_dst].index = srcNode->myTag.index;

    dstNode->burgX[current_arm_dst] = -1. * burgers[0];
    dstNode->burgY[current_arm_dst] = -1. * burgers[1];
    dstNode->burgZ[current_arm_dst] = -1. * burgers[2];

    dstNode->nx[current_arm_dst] = normals[0];
    dstNode->ny[current_arm_dst] = normals[1];
    dstNode->nz[current_arm_dst] = normals[2];

    ++current_arm_src;
    ++current_arm_dst;
  }

  // for (UInt i = 0; i < nb_points; ++i) {
  //   if (numNbrs[i] == 1) {
  //     created_nodes[i]->constraint = PINNED_NODE;
  //     // Add all pinned nodes to the list of (possible) hybrid nodes
  //     nh_l_conInt.push_back(created_nodes[i]);
  //   }
  // }
}

/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_CONTAINER_PARADIS_HH__ */
