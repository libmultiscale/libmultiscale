/* ------------------------------------------------------------------------ */
#include "ref_node_paradis.hh"
#include "domain_paradis.hh"
/* ------------------------------------------------------------------------ */

__BEGIN_LIBMULTISCALE__
/* ------------------------------------------------------------------------ */

RefNodeParaDiS::RefNodeParaDiS() {
  // Request creation of a new node
  Node_t *newNode = GetNewNativeNode(ParaDiSHome::paradis_ptr);

  newNode->oldvX = 0.0;
  newNode->oldvY = 0.0;
  newNode->oldvZ = 0.0;

  newNode->vX = 0.0;
  newNode->vY = 0.0;
  newNode->vZ = 0.0;

  newNode->fX = 0.0;
  newNode->fY = 0.0;
  newNode->fZ = 0.0;

  newNode->x = 0.0;
  newNode->y = 0.0;
  newNode->z = 0.0;

  newNode->oldx = 0.0;
  newNode->oldy = 0.0;
  newNode->oldz = 0.0;

  newNode->native = 1;
  newNode->constraint = DomainParaDiS::_free;

  AssignNodeToCell(ParaDiSHome::paradis_ptr, newNode);

  this->index_node = newNode->myTag.index;
}
/* ------------------------------------------------------------------------ */

RefNodeParaDiS::RefNodeParaDiS(int index) { index_node = index; }
/* ------------------------------------------------------------------------ */

__END_LIBMULTISCALE__
