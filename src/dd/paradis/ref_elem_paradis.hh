/**
 * @file   ref_node_paradis.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Max Hodapp <max.hodapp@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  Nodal reference of ParaDiS
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_REF_ELEM_PARADIS_HH__
#define __LIBMULTISCALE_REF_ELEM_PARADIS_HH__
/* -------------------------------------------------------------------------- */
// ParaDis includes
#define PARALLEL
#include <mpi.h>
extern "C" {
#include "Home.h"
#include "Typedefs.h"
}
#undef PARALLEL
/* -------------------------------------------------------------------------- */
#include "ref_elem_dd.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */

/**
 * Class RefElemParaDiS
 *
 */

class RefElemParaDiS : public RefDDElem<3, RefElemParaDiS> {

public:
  static const UInt Dim = 3;

  using fields = field_list<_burgers, _normal>;

  RefElemParaDiS() {}
  ~RefElemParaDiS() {}

  inline void setConnectivity(std::vector<UInt> &conn) override {

    auto node1_index = conn[0];
    auto node2_index = conn[1];
    auto *node1 = ParaDiSHome::paradis_ptr->nodeKeys[node1_index];
    auto *node2 = ParaDiSHome::paradis_ptr->nodeKeys[node2_index];
    if (node1 == nullptr or node2 == nullptr)
      LM_FATAL("invalid connectivity or nodes not created");

    auto node1_numNbrs = node1->numNbrs;
    auto node2_numNbrs = node2->numNbrs;

    this->index_node = node1_index;
    this->arm_number = node1_numNbrs;

    if (node1_index > node2_index) {
      this->index_node = node2_index;
      this->arm_number = node2_numNbrs;
    }

    auto *node1_nbrTags = node1->nbrTag;
    auto *node2_nbrTags = node2->nbrTag;

    if (node1_numNbrs > 0) {
      if (node1_nbrTags == nullptr)
        LM_FATAL("inconsistent nbrtags");
      for (int i = 0; i < node1_numNbrs; ++i)
        if (node1_nbrTags[i].index == (int)node2_index)
          LM_FATAL("inconsistent nbrtags");
    }

    if (node2_numNbrs > 0) {
      if (node2_nbrTags == nullptr)
        LM_FATAL("inconsistent nbrtags");
      for (int i = 0; i < node2_numNbrs; ++i)
        if (node2_nbrTags[i].index == (int)node1_index)
          LM_FATAL("inconsistent nbrtags");
    }

    ReallocNodeArms(node1, node1_numNbrs + 1);
    ReallocNodeArms(node2, node2_numNbrs + 1);

    node2->nbrTag[node2_numNbrs].domainID = node1->myTag.domainID;
    node2->nbrTag[node2_numNbrs].index = node1->myTag.index;

    node1->nbrTag[node1_numNbrs].domainID = node2->myTag.domainID;
    node1->nbrTag[node1_numNbrs].index = node2->myTag.index;
  };

  bool operator==(RefElemParaDiS &) { LM_TOIMPLEMENT; };

  VectorProxy<3> burgers() {
    auto *node = ParaDiSHome::paradis_ptr->nodeKeys[this->index_node];
    return VectorProxy<3>(std::forward_as_tuple(node->burgX[arm_number],
                                                node->burgY[arm_number],
                                                node->burgZ[arm_number]));
  }

  VectorProxy<3> normal() {
    auto *node = ParaDiSHome::paradis_ptr->nodeKeys[this->index_node];
    return VectorProxy<3>(std::forward_as_tuple(
        node->nx[arm_number], node->ny[arm_number], node->nz[arm_number]));
  }

  void setIndex(UInt index, UInt arm) {
    index_node = index;
    arm_number = arm;
  }

  std::vector<UInt> localIndexes() override {
    std::vector<UInt> nodes;
    auto *node1 = ParaDiSHome::paradis_ptr->nodeKeys[this->index_node];
    auto tag1 = node1->myTag.index;
    auto tag2 = node1->nbrTag[arm_number].index;

    nodes.push_back(tag1);
    nodes.push_back(tag2);
    return nodes;
  };

  RefElemParaDiS getMirrorElem() {
    auto *node1 = ParaDiSHome::paradis_ptr->nodeKeys[this->index_node];
    auto tag2 = node1->nbrTag[arm_number].index;
    auto tag1 = node1->myTag.index;

    RefElemParaDiS mirror_ref;
    mirror_ref.index_node = tag2;
    mirror_ref.arm_number = UInt(-1);

    auto *node2 = ParaDiSHome::paradis_ptr->nodeKeys[tag2];

    for (int i = 0; i < node2->numNbrs; ++i) {
      if (tag1 == node2->nbrTag[i].index) {
        mirror_ref.arm_number = i;
        break;
      }
    }
    if (mirror_ref.arm_number == UInt(-1)) {
      LM_FATAL("cannot find the other pair");
    }
    return mirror_ref;
  }

private:
  UInt index_node;
  UInt arm_number;
};
/* -------------------------------------------------------------------------- */

inline std::ostream &operator<<(std::ostream &, RefElemParaDiS &) {
  LM_TOIMPLEMENT;
}
/* -------------------------------------------------------------------------- */

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_REF_ELEM_PARADIS_HH__ */
