/**
 * @file   ref_node_paradis.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 * @author Max Hodapp <max.hodapp@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  Nodal reference of ParaDiS
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_REF_NODE_PARADIS_HH__
#define __LIBMULTISCALE_REF_NODE_PARADIS_HH__
/* -------------------------------------------------------------------------- */
// ParaDis includes
#define PARALLEL
#include <mpi.h>
extern "C" {
#include "Home.h"
#include "Typedefs.h"
#undef X
#undef Y
#undef Z
}
#undef PARALLEL
/* -------------------------------------------------------------------------- */
#include "paradis_home.hh"
#include "ref_node_dd.hh"
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

/* -------------------------------------------------------------------------- */
class DomainParaDiS;
class ComparatorRefNodeParaDiS;
/* -------------------------------------------------------------------------- */

/**
 * Class RefNodeParaDiS
 *
 */

class RefNodeParaDiS : public RefDDNode<3, RefNodeParaDiS> {

  friend class ContainerNodesParaDiS;
  friend class IteratorNodesParaDiS;
  friend class IteratorElemsParaDiS;
  friend class ContainerElemsParaDiS;

public:
  //! generic container of the original model container
  using Domain = DomainParaDiS;
  //! node comparator class to be usable in maps
  using RefComparator = ComparatorRefNodeParaDiS;

  static const UInt Dim = 3;

  using fields = field_list<_position0, _position, _velocity, _force>;

  RefNodeParaDiS();
  RefNodeParaDiS(int index);

  bool operator==(RefNodeParaDiS &ref);

  VectorProxy<3> position0();
  inline VectorProxy<3> position();
  VectorProxy<3> previous_position();
  VectorProxy<3> velocity();
  VectorProxy<3> force();
  UInt tag();
  UInt getIndex();
  //! constraint tag:
  int &getConstraint();

protected:
  UInt nbArms() const;
  int &getIndex0();
  // int &getIndex();
  void setIndex(UInt index);
  int getSlave();
  // void slaving();
  // void unslaving();
  // void constrain();
  // void unconstrain();

private:
  UInt index_node;
  UInt index0_node;
};
/* -------------------------------------------------------------------------- */

class ComparatorRefNodeParaDiS {
public:
  bool operator()(RefNodeParaDiS &, RefNodeParaDiS &) const {
    //    return r1.index_atome < r2.index_atome;
    LM_TOIMPLEMENT;
  }

  bool operator()(const RefNodeParaDiS &, const RefNodeParaDiS &) const {
    LM_TOIMPLEMENT;
  }
};

/* -------------------------------------------------------------------------- */

inline std::ostream &operator<<(std::ostream &os, RefNodeParaDiS &ref) {
  os << "ParaDiSNode reference : " << ref.getIndex();
  return os;
}
/* -------------------------------------------------------------------------- */

inline bool RefNodeParaDiS::operator==(RefNodeParaDiS &ref) {
  return (ref.index_node == index_node);
}
/* -------------------------------------------------------------------------- */

inline VectorProxy<3> RefNodeParaDiS::position0() { return this->position(); }
VectorProxy<3> RefNodeParaDiS::position() {
  return VectorProxy<3>(std::forward_as_tuple(
      ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->x,
      ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->y,
      ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->z));
}
/* -------------------------------------------------------------------------- */

inline VectorProxy<3> RefNodeParaDiS::previous_position() {
  return VectorProxy<3>(std::forward_as_tuple(
      ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->oldx,
      ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->oldy,
      ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->oldz));
}
/* -------------------------------------------------------------------------- */

inline VectorProxy<3> RefNodeParaDiS::velocity() {
  return VectorProxy<3>(std::forward_as_tuple(
      ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->vX,
      ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->vY,
      ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->vZ));
}
/* -------------------------------------------------------------------------- */

inline VectorProxy<3> RefNodeParaDiS::force() {
  return VectorProxy<3>(std::forward_as_tuple(
      ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->fX,
      ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->fY,
      ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->fZ));
}
/* -------------------------------------------------------------------------- */

inline UInt RefNodeParaDiS::nbArms() const {
  return ParaDiSHome::paradis_ptr->nodeKeys[index_node]->numNbrs;
}
/* -------------------------------------------------------------------------- */

inline UInt RefNodeParaDiS::tag() { return this->index_node; }
/* -------------------------------------------------------------------------- */

inline int &RefNodeParaDiS::getIndex0() {
  return ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->myTag.index0;
}
/* -------------------------------------------------------------------------- */

inline UInt RefNodeParaDiS::getIndex() {
  return ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->myTag.index;
}
/* -------------------------------------------------------------------------- */

inline void RefNodeParaDiS::setIndex(UInt index) { index_node = index; }
/* -------------------------------------------------------------------------- */

inline int RefNodeParaDiS::getSlave() {
  return ParaDiSHome::ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]
      ->slave;
}
/* -------------------------------------------------------------------------- */

//! constraint tag:
inline int &RefNodeParaDiS::getConstraint() {
  return ParaDiSHome::ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]
      ->constraint;
}
/* -------------------------------------------------------------------------- */

// inline void RefNodeParaDiS::slaving() {
//   ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->slave = 1;
// }
// /* --------------------------------------------------------------------------
// */

// inline void RefNodeParaDiS::unslaving() {
//   ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->slave = 0;
// }
// /* --------------------------------------------------------------------------
// */

// inline void RefNodeParaDiS::constrain() {
//   ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->constraint = 7;
// }
// /* --------------------------------------------------------------------------
// */

// inline void RefNodeParaDiS::unconstrain() {
//   ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->constraint = 0;
// }
// /* --------------------------------------------------------------------------
// */

// // Get the number of neighbors
// UInt RefNodeParaDiS::getNbrOfNeighs() {
//   return
//   UInt(ParaDiSHome::ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->numNbrs);
// }

// Real &RefNodeParaDiS::prev_position(unsigned int i) {
//   switch (i) {
//   case 0:
//     return ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->oldx;
//     break;
//   case 1:
//     return ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->oldy;
//     break;
//   case 2:
//     return ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->oldz;
//     break;
//   }
//   Real *p = NULL;
//   return *p;
// }

// Real &RefNodeParaDiS::getNeighborNodeCoord(unsigned int n, unsigned int i) {

//   UInt my_domain = ParaDiSHome::paradis_ptr->myDomain;
//   Node_t current = *ParaDiSHome::paradis_ptr->nodeKeys[this->index_node];

//   Tag_t *tag = current.nbrTag;
//   UInt tag_domain = tag[n].domainID;
//   int idx_for_neighbor = tag[n].index;

//   if (my_domain == tag_domain) {
//     switch (i) {
//     case 0:
//       return ParaDiSHome::paradis_ptr->nodeKeys[idx_for_neighbor]->x;
//       break;
//     case 1:
//       return ParaDiSHome::paradis_ptr->nodeKeys[idx_for_neighbor]->y;
//       break;
//     case 2:
//       return ParaDiSHome::paradis_ptr->nodeKeys[idx_for_neighbor]->z;
//       break;
//     }
//   } else {
//     switch (i) {
//     case 0:
//       return ParaDiSHome::paradis_ptr->remoteDomainKeys[tag_domain]
//           ->nodeKeys[idx_for_neighbor]
//           ->x;
//       break;
//     case 1:
//       return ParaDiSHome::paradis_ptr->remoteDomainKeys[tag_domain]
//           ->nodeKeys[idx_for_neighbor]
//           ->y;
//       break;
//     case 2:
//       return ParaDiSHome::paradis_ptr->remoteDomainKeys[tag_domain]
//           ->nodeKeys[idx_for_neighbor]
//           ->z;
//       break;
//     }
//   }
//   Real *p = NULL;
//   return *p;
// }

// int &RefNodeParaDiS::getNeighborNodeIndex0(unsigned int n) {
//   Node_t current = *ParaDiSHome::paradis_ptr->nodeKeys[this->index_node];
//   Tag_t *tag = current.nbrTag;
//   return tag[n].index0;
// }

// int &RefNodeParaDiS::getNeighborNodeIndex(unsigned int n) {
//   Node_t current = *ParaDiSHome::paradis_ptr->nodeKeys[this->index_node];
//   Tag_t *tag = current.nbrTag;
//   return tag[n].index;
// }

// int &RefNodeParaDiS::getNeighborNodeSlave(unsigned int n) {
//   UInt my_domain = ParaDiSHome::paradis_ptr->myDomain;
//   Node_t current = *ParaDiSHome::paradis_ptr->nodeKeys[this->index_node];

//   Tag_t *tag = current.nbrTag;
//   UInt tag_domain = tag[n].domainID;
//   int idx_for_neighbor = tag[n].index;
//   if (my_domain == tag_domain)
//     return ParaDiSHome::paradis_ptr->nodeKeys[idx_for_neighbor]->slave;
//   else
//     return ParaDiSHome::paradis_ptr->remoteDomainKeys[tag_domain]
//         ->nodeKeys[idx_for_neighbor]
//         ->slave;
// }

// int &RefNodeParaDiS::getNbrNeighborsOfNeighbor(unsigned int n) {
//   UInt my_domain = ParaDiSHome::paradis_ptr->myDomain;
//   Node_t current = *ParaDiSHome::paradis_ptr->nodeKeys[this->index_node];

//   Tag_t *tag = current.nbrTag;
//   UInt tag_domain = tag[n].domainID;
//   int idx_for_neighbor = tag[n].index;
//   if (my_domain == tag_domain)
//     return ParaDiSHome::paradis_ptr->nodeKeys[idx_for_neighbor]->numNbrs;
//   else
//     return ParaDiSHome::paradis_ptr->remoteDomainKeys[tag_domain]
//         ->nodeKeys[idx_for_neighbor]
//         ->numNbrs;
// }

// void RefNodeParaDiS::setOldVel(unsigned int i, Real oldv) {
//   switch (i) {
//   case 0:
//     ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->oldvX = oldv;
//     return;
//   case 1:
//     ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->oldvY = oldv;
//     return;
//   case 2:
//     ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->oldvZ = oldv;
//     return;
//   }
//   return;
// }

// void RefNodeParaDiS::release(UInt nbr_index) {
//   ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->constraint = 0;

//   UInt index_for_neighbor =
//       ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->nbrTag[0].index;
//   ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->oldvX =
//       0.5 * (ParaDiSHome::paradis_ptr->nodeKeys[index_for_neighbor]->oldvX
//       +
//              ParaDiSHome::paradis_ptr->nodeKeys[nbr_index]->oldvX);
//   ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->oldvY =
//       0.5 * (ParaDiSHome::paradis_ptr->nodeKeys[index_for_neighbor]->oldvY
//       +
//              ParaDiSHome::paradis_ptr->nodeKeys[nbr_index]->oldvY);
//   ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->oldvZ =
//       0.5 * (ParaDiSHome::paradis_ptr->nodeKeys[index_for_neighbor]->oldvZ
//       +
//              ParaDiSHome::paradis_ptr->nodeKeys[nbr_index]->oldvZ);

//   InsertArm(ParaDiSHome::paradis_ptr,
//   ParaDiSHome::paradis_ptr->nodeKeys[this->index_node],
//             &ParaDiSHome::paradis_ptr->nodeKeys[nbr_index]->myTag,
//             -1.0 *
//             (ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->burgX[0]),
//             -1.0 *
//             (ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->burgY[0]),
//             -1.0 *
//             (ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->burgZ[0]),
//             ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->nx[0],
//             ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->ny[0],
//             ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->nz[0], 0,
//             1);
//   ParaDiSHome::paradis_ptr->nodeKeys[this->index_node]->numNbrs = 2;
// }

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_REF_NODE_ParaDiS_HH__ */
