/**
 * @file   iterator_paradis.hh
 *
 * @author Till Junge <till.junge@epfl.ch>
 * @author Jaehyun Cho <jaehyun.cho@epfl.ch>
 *
 * @date   Wed Jul 09 21:59:47 2014
 *
 * @brief  ParaDiS iterator over DOFs
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_ITERATOR_PARADIS_HH__
#define __LIBMULTISCALE_ITERATOR_PARADIS_HH__
/* -------------------------------------------------------------------------- */
#include "container_paradis.hh"
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
// ParaDiS includes
#define PARALLEL
extern "C" {
#include <Home.h>
#include <Typedefs.h>
}
#undef PARALLEL
/* -------------------------------------------------------------------------- */
__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

/**
 * Class IteratorNodeParaDiS
 *
 */

class IteratorNodesParaDiS : public iterator_base<RefNodeParaDiS> {

public:
  IteratorNodesParaDiS(ContainerNodesParaDiS &c, UInt index_node = 0)
      : iterator_base(c), node(index_node), index_node(index_node) {

    auto &nodeKeys = ParaDiSHome::paradis_ptr->nodeKeys;
    if (nodeKeys == nullptr)
      return;

    auto &newNodeKeyPtr = ParaDiSHome::paradis_ptr->newNodeKeyPtr;
    if (index_node < (UInt)newNodeKeyPtr and nodeKeys[index_node] == nullptr)
      ++(*this);

    if (nodeKeys[node.index_node] != nullptr) {
      nodeKeys[node.index_node]->myTag.node_number = 0;
    }
  };

  iterator_base &operator++();
  RefNodeParaDiS &operator*();
  const RefNodeParaDiS &operator*() const;

  bool operator!=(const iterator_base &) const;
  bool operator==(const iterator_base &) const;

  RefNodeParaDiS node;

private:
  UInt index_node;
};

/* -------------------------------------------------------------------------- */

inline RefNodeParaDiS &IteratorNodesParaDiS::operator*() { return node; }

/* -------------------------------------------------------------------------- */

inline const RefNodeParaDiS &IteratorNodesParaDiS::operator*() const {
  return node;
}

/* -------------------------------------------------------------------------- */

inline iterator_base<RefNodeParaDiS> &IteratorNodesParaDiS::operator++() {

  UInt current_node_number = -1;
  if (ParaDiSHome::paradis_ptr->nodeKeys[node.index_node] != nullptr) {
    current_node_number =
        ParaDiSHome::paradis_ptr->nodeKeys[node.index_node]->myTag.node_number;
  }

  ++index_node;
  while (index_node < (UInt)ParaDiSHome::paradis_ptr->newNodeKeyPtr and
         ParaDiSHome::paradis_ptr->nodeKeys[index_node] == nullptr) {
    ++index_node;
    if (index_node == (UInt)ParaDiSHome::paradis_ptr->newNodeKeyPtr)
      break;
  }
  node.index_node = index_node;
  if (ParaDiSHome::paradis_ptr->nodeKeys[node.index_node] != nullptr) {
    ParaDiSHome::paradis_ptr->nodeKeys[node.index_node]->myTag.node_number =
        current_node_number + 1;
  }
  return *this;
}

/* -------------------------------------------------------------------------- */

inline bool IteratorNodesParaDiS::operator!=(const iterator_base &it) const {
  return !(*this == it);
}

/* -------------------------------------------------------------------------- */

inline bool IteratorNodesParaDiS::operator==(const iterator_base &it) const {
  return index_node == static_cast<const IteratorNodesParaDiS &>(it).index_node;
}

/* -------------------------------------------------------------------------- */
///
/// Doing elements now
///
/* -------------------------------------------------------------------------- */

class IteratorElemsParaDiS : public iterator_base<RefElemParaDiS> {

public:
  IteratorElemsParaDiS(ContainerElemsParaDiS &,
                       IteratorNodesParaDiS iterator_nodes)
      : it_nodes(iterator_nodes), current_arm(0) {}

  iterator_base &operator++() {
    ++this->current_arm;
    if (this->current_arm == (*it_nodes).nbArms()) {
      current_arm = 0;
      ++it_nodes;
    }
    auto &node = *it_nodes;
    int tag1 = node.index_node;
    if (tag1 < ParaDiSHome::paradis_ptr->newNodeKeyPtr) {
      int tag2 = ParaDiSHome::paradis_ptr->nodeKeys[node.getIndex()]
                     ->nbrTag[this->current_arm]
                     .index;
      if (tag1 > tag2)
        ++(*this);
    }
    return *this;
  };

  RefElemParaDiS &operator*() {
    auto &node1 = *it_nodes;
    elem.setIndex(node1.getIndex(), current_arm);
    auto connectivity = elem.localIndexes();

    connectivity[0] =
        ParaDiSHome::paradis_ptr->nodeKeys[connectivity[0]]->myTag.node_number;
    connectivity[1] =
        ParaDiSHome::paradis_ptr->nodeKeys[connectivity[1]]->myTag.node_number;
    elem.setAlteredConnectivity(connectivity);
    return elem;
  };

  bool operator!=(const iterator_base &other) const {
    return !((*this) == other);
  };

  bool operator==(const iterator_base &other) const {

    if (static_cast<const IteratorElemsParaDiS &>(other).it_nodes !=
        this->it_nodes)
      return false;

    return current_arm ==
           static_cast<const IteratorElemsParaDiS &>(other).current_arm;
  };

private:
  IteratorNodesParaDiS it_nodes;
  UInt current_arm;
  RefElemParaDiS elem;
};

__END_LIBMULTISCALE__

#endif /* __LIBMULTISCALE_ITERATOR_PARADIS_HH__ */
