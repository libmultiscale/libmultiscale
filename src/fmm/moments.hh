/**
 * @file   moments.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Aug 10 18:08:12 2012
 *
 * @brief  Calculation of moments to be used by fast multipole algorithms
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it
 * under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but
 * WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_MOMENTS_HH__
#define __LIBMULTISCALE_MOMENTS_HH__
/* -------------------------------------------------------------------------- */

template <UInt T, UInt Dim, UInt order> class Moments {

public:
  Moments();
  virtual ~Moments();
};

/* -------------------------------------------------------------------------- */

template <typename T, UInt order> class Moments<T, 3, order> {

  /* ------------------------------------------------------------------------ */
  /* Typedefs                                                                 */
  /* ------------------------------------------------------------------------ */

public:
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

  Moments();
  virtual ~Moments();

public:
  T moments[n_moments_3D[order]];
};

/* -------------------------------------------------------------------------- */

#endif /* __LIBMULTISCALE_MOMENTS_HH__ */
