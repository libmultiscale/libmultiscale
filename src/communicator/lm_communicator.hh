/**
 * @file   communicator.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Fri Nov 08 12:44:53 2013
 *
 * @brief  Mother class of LM communicators
 *
 * @section LICENSE
 *
 * Copyright INRIA and CEA
 *
 * The LibMultiScale is a C++ parallel framework for the multiscale
 * coupling methods dedicated to material simulations. This framework
 * provides an API which makes it possible to program coupled simulations
 * and integration of already existing codes.
 *
 * This Project was initiated in a collaboration between INRIA Futurs Bordeaux
 * within ScAlApplix team and CEA/DPTA Ile de France.
 * The project is now continued at the Ecole Polytechnique Fédérale de Lausanne
 * within the LSMS/ENAC laboratory.
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 *
 */

#ifndef __LIBMULTISCALE_COMMUNICATOR_HH__
#define __LIBMULTISCALE_COMMUNICATOR_HH__
/* -------------------------------------------------------------------------- */
#include "comm_group.hh"
#include "id_manager.hh"
#include "tensor.hh"
#include <memory>
#include <mpi.h>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */
class Geometry;
/* -------------------------------------------------------------------------- */

class Communicator : public IDManager<CommGroup> {

public:
  Communicator();
  virtual ~Communicator();

  //! return the communicator used
  static Communicator &getCommunicator();

  //! create the communicator
  static void createCommunicator();

  //! reset the communicator
  void reset();

  //! add a group of nb_proc processors
  void addGroup(const LMID &id, UInt nb_procs);

  //! return the number of registered objects
  static UInt getNbGroups();

  //! get the groups
  static decltype(objects) &getGroups();

  //! get a group by its name
  static CommGroup getGroup(const LMID &id);

  //! return the number of still free processors
  UInt getNumberFreeProcs();
  //! wait for unsynchronisated comunications
  void waitForPendingComs();

  //! datatypes to be sent through MPI coms
  static MPI_Datatype mpi_type_processor;
};

/* -------------------------------------------------------------------------- */
__END_LIBMULTISCALE__
/* -------------------------------------------------------------------------- */

#include "comm_group_inline_impl.hh"

#endif /* __LIBMULTISCALE_COMMUNICATOR_HH__ */
