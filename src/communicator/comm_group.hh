#ifndef __LIBMULTISCALE_COMM_GROUP_HH__
#define __LIBMULTISCALE_COMM_GROUP_HH__
/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
/* -------------------------------------------------------------------------- */
#include <memory>
#include <mpi.h>
/* -------------------------------------------------------------------------- */

__BEGIN_LIBMULTISCALE__

class Communicator;
/* -------------------------------------------------------------------------- */
struct Request {
  MPI_Request request;
};
/* -------------------------------------------------------------------------- */

struct MPIProc {

  MPIProc() : sequence_number_recv(0), sequence_number_send(0){};

  UInt getAbsoluteRank() { return absolute_mpi_rank; };

  //! number in the container list of the group
  UInt id;
  //! mpirank of the processor in the group
  UInt mpi_rank;
  //! mpirank ofthe processor in MPI_COMM_WORLD
  UInt absolute_mpi_rank;
  //! current sequence number in recv
  UInt sequence_number_recv;
  //! current sequence number in send
  UInt sequence_number_send;
};
/* -------------------------------------------------------------------------- */

inline std::ostream &operator<<(std::ostream &os, const MPIProc &proc) {
  os << "proc: " << proc.id << " mpi_rank: " << proc.mpi_rank
     << " sequence_number_recv: " << proc.sequence_number_recv
     << " sequence_number_send: " << proc.sequence_number_send
     << " absolute_mpi_rank: " << proc.absolute_mpi_rank;

  return os;
}
/* -------------------------------------------------------------------------- */

struct CommGroup {

  bool operator==(CommGroup grp) const {
    return (grp.getID() == this->getID());
  }
  bool operator!=(CommGroup grp) const { return !(grp == *this); }

  auto getMPIComm() const { return mpi_comm; };
  UInt getMyRank() const {
    if (mpi_comm == MPI_COMM_NULL)
      LM_FATAL("No member of the group: this call should be secured");
    int rank_in_group;
    MPI_Comm_rank(mpi_comm, &rank_in_group);
    return rank_in_group;
  }

  // UInt real2GroupRank(UInt real_rank);

  const LMID &getID() const {
    if (id == "invalid")
      LM_FATAL("Communication group is invalid");
    return id;
  };

  template <typename Vec>
  void allReduce(Vec &&contrib, const std::string &comment, Operator op);

  template <typename T>
  void allReduce(T *contrib, UInt nb, const std::string &comment, Operator op);

  template <typename T>
  void reduce(T *contrib, UInt nb, const std::string &comment, Operator op,
              UInt root_rank = 0);

  template <typename Vec>
  void reduce(Vec &&contrib, const std::string &comment, Operator op,
              UInt root_rank = 0);

  template <typename T>
  void send(T *d, UInt nb, UInt to, const std::string &comment);

  template <typename T>
  Request isend(T *d, UInt nb, UInt to, const std::string &comment);

  template <typename T>
  Request issend(T *d, UInt nb, UInt to, const std::string &comment);

  void testall();

  template <typename T>
  inline void receive(T *d, UInt nb, UInt from, const std::string &comment);

  template <typename BufType>
  inline void broadcast(BufType &&buf, UInt root, const std::string &comment);

  template <typename T>
  inline void broadcast(T *buf, UInt nb, UInt root, const std::string &comment);

  template <typename T>
  inline void gather(T *sendbuf, UInt nb, T *recvbuf, UInt to,
                     const std::string &comment);

  template <typename BufType>
  inline void gather(BufType &&sendbuf, BufType &&recvbuf, UInt to,
                     const std::string &comment);

  template <typename T>
  inline void scatter(T *sendbuf, T *recvbuf, UInt nb_recv, UInt root,
                      const std::string &comment);

  template <typename BufType>
  inline void scatter(BufType &&sendbuf, BufType &&recvbuf, UInt root,
                      const std::string &comment);

  template <typename T>
  inline void allGatherv(T *send_buffer, UInt send_nb, T *recv_buffer,
                         std::vector<UInt> &recv_counts,
                         const std::string &comment);

  template <typename BufType>
  inline void allGatherv(BufType &&send_buffer, BufType &&recv_buffer,
                         std::vector<UInt> &recv_counts,
                         const std::string &comment);

  template <typename T>
  inline void allGather(T *send_buffer, UInt nb, T *recv_buffer,
                        const std::string &comment);

  template <typename BufType>
  inline void allGather(BufType &&send_buffer, BufType &&recv_buffer,
                        const std::string &comment);

  auto size() const { return processors->size(); };
  auto begin() { return processors->begin(); };
  auto end() { return processors->end(); };

  auto &operator[](UInt i) { return (*processors)[i]; }

  inline bool isInGroup(UInt mpi_rank) const;

  bool amIinGroup() const { return is_current_proc_in_group; };
  void barrier();
  Request ibarrier();
  void wait(Request &req);

  void printself(std::ostream &os) const;

  template <typename Vec>
  inline void send(Vec &&d, UInt to, const std::string &comment);

  template <typename Vec>
  inline void receive(Vec &&d, UInt from, const std::string &comment);

  template <typename T> inline int probe(UInt from, const std::string &comment);
  template <typename T>
  inline int iprobe(UInt from, const std::string &comment);

protected:
  //! the intra communicator of the group
  MPI_Comm mpi_comm;
  //! the mpi group
  MPI_Group mpi_group;
  //! vector of processors
  std::shared_ptr<std::vector<MPIProc>> processors;
  //! stores the color used at creation of the communicator
  bool is_current_proc_in_group;

  friend Communicator;
  inline CommGroup(const LMID &id, int color, UInt nb_procs);

private:
  //! general id
  LMID id;
};
/* -------------------------------------------------------------------------- */

inline std::ostream &operator<<(std::ostream &os, const CommGroup group) {
  group.printself(os);
  return os;
}
/* -------------------------------------------------------------------------- */

struct SelfGroup : public CommGroup {
  inline SelfGroup();
};

struct AllGroup : public CommGroup {
  inline AllGroup();
};

__END_LIBMULTISCALE__

#endif //__LIBMULTISCALE_COMM_GROUP_HH__
