import subprocess
import numpy as np

# lattice and atomic constants
A = 4.04525975883
BX = np.sqrt(2)
BY = np.sqrt(3)
BZ = np.sqrt(6)
AX = BX*A
AY = BY*A
AZ = BZ*A
mass = 26.9815400000000

# geometry constants
WIDTH = 10
HEIGHT = 100
THICKNESS = 5
LX = WIDTH*AX
LY = HEIGHT*AY
LZ = THICKNESS*AZ

# MD geometry constants
MDHEIGHT = 20
MDLY = MDHEIGHT*AY

# FEM geometry constants
FEMHEIGHT = 10
FEMLY = FEMHEIGHT*AY

# bridging size
BRIDGE = 5*AY
PAD = 1*AY


geo = f'''
h = 15;
LX = {LX};
LY = {FEMLY};
LZ = {LZ};

Point(1) = {{0.0, 0.0, 0.0, h}};
Point(2) = {{LX,   0.0, 0.0, h}};
Point(3) = {{0.0, LY,   0.0, h}};
Point(4) = {{LX,   LY,   0.0, h}};

Point(5) = {{0.0, 0.0, LZ, h}};
Point(6) = {{LX,   0.0, LZ, h}};
Point(7) = {{0.0, LY,   LZ, h}};
Point(8) = {{LX,   LY,   LZ, h}};

Line(27) = {{2, 4}};
Line(28) = {{4, 3}};
Line(29) = {{3, 1}};
Line(30) = {{1, 2}};
Line(31) = {{7, 5}};
Line(32) = {{5, 6}};
Line(33) = {{6, 8}};
Line(34) = {{8, 7}};
Line(35) = {{1, 5}};
Line(36) = {{3, 7}};
Line(37) = {{8, 4}};
Line(38) = {{6, 2}};
Curve Loop(31) = {{27, 28, 29, 30}};
Plane Surface(32) = {{31}};
Curve Loop(32) = {{36, 31, -35, -29}};
Plane Surface(33) = {{32}};
Curve Loop(33) = {{31, 32, 33, 34}};
Plane Surface(34) = {{33}};
Curve Loop(34) = {{38, 27, -37, -33}};
Plane Surface(35) = {{34}};
Curve Loop(35) = {{28, 36, -34, 37}};
Plane Surface(36) = {{35}};
Curve Loop(36) = {{32, 38, -30, 35}};
Plane Surface(37) = {{36}};

Transfinite Surface "*";

Surface Loop(1) = {{34, 33, 36, 32, 35, 37}};
Volume(1) = {{1}};

Transfinite Volume "*";
'''


with open('mesh.geo', 'w') as f:
    f.write(geo)

subprocess.call("gmsh -3 mesh.geo -o mesh.msh", shell=True)
# subprocess.call("gmsh mesh.geo", shell=True)
