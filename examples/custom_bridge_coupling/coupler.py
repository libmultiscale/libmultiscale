#!/usr/bin/python3

import pylibmultiscale as lm
import numpy as np


class MyCoupler(lm.ArlequinPython):

    def __init__(self, name):
        super().__init__(name)
        self.centroid_flag = False
        self.A = lm.ContainerArrayReal()
        self.rhs = lm.ContainerArrayReal()

    def coupling(self, stage):
        if not self.is_in_continuum() and not self.is_in_atomic():
            return

        if stage == lm.COUPLING_STEP2:
            lm.DUMP("Zero boundary condition", lm.DBG_INFO)
            lm.STARTTIMER("ZERO_BC")
            self.boundary_zone.setPointField(
                lm.force, 0., lm.spatial_dimension.fget())
            lm.STOPTIMER("ZERO_BC")

        if (stage != lm.COUPLING_STEP3):
            return

        lm.STARTTIMER("BuildRHS")
        lm.DUMP("Build RHS", lm.DBG_INFO)
        self.buildRHS(lm.velocity)
        lm.STOPTIMER("BuildRHS")

        lm.DUMP("Synch with migration bridging", lm.DBG_INFO)
        lm.STARTTIMER("updatesForMigrations")
        self.bridging_zone.updateForMigration()
        lm.DUMP("Synch with migration boundary", lm.DBG_INFO)
        self.boundary_zone.updateForMigration()
        lm.STOPTIMER("updatesForMigrations")

        lm.DUMP("Correcting surface effect", lm.DBG_INFO)
        lm.STARTTIMER("projectAtomicVelocitiesOnMesh")
        self.boundary_zone.projectAtomicFieldOnMesh(lm.velocity)
        self.boundary_zone.projectAtomicFieldOnMesh(lm.displacement)
        lm.STOPTIMER("projectAtomicVelocitiesOnMesh")

        lm.STARTTIMER("BuildRHS")
        lm.DUMP("Build RHS", lm.DBG_INFO)
        self.buildRHS(lm.velocity)

        lm.STOPTIMER("BuildRHS")

        lm.DUMP("solving constraint", lm.DBG_INFO)
        lm.STARTTIMER("Solving constraint")
        self.solveConstraint()
        lm.STOPTIMER("Solving constraint")

        lm.DUMP("Applying constraint", lm.DBG_INFO)
        lm.STARTTIMER("Correcting")
        self.applyCorrection(lm.velocity)
        lm.STOPTIMER("Correcting")

        lm.DUMP(self.getID() + " : coupling stage all done", lm.DBG_INFO)

    def solveConstraint(self):
        for r, a in zip(self.rhs.array(), self.A.array()):
            r /= a

    def applyCorrection(self, field):
        if self.is_in_continuum():
            correction = self.bridging_zone.buffer_for_nodes
            correction.resize(
                self.bridging_zone.smatrix.shape[0], self.rhs.cols())

            correction.array()[:] = -(self.bridging_zone.smatrix @
                                      self.rhs.array())
            for corr, lbda in zip(correction.array(), self.lambdas_mesh.array()):
                corr /= lbda

            self.bridging_zone.setPBCSlaveFromMaster(correction)
            self.bridging_zone.addMeshField(field, correction)
        if self.is_in_atomic():
            correction = self.bridging_zone.buffer_for_points
            correction.resize(self.rhs.rows(), self.rhs.cols())
            for cor, r, lbda in zip(correction.array(),
                                    self.rhs.array(),
                                    self.lambdas_point.array()):
                # "weight associated with atom is zero : abort"
                assert(lbda != 0)
                cor[:] = r / lbda
            self.bridging_zone.addPointField(field, correction)

    def buildRHS(self, field_type):
        lm.DUMP("Clean RHS", lm.DBG_INFO)
        if self.rhs.array().shape[0] == 0:
            self.rhs.resize(0, lm.spatial_dimension.fget())
        self.rhs.setZero()

        if self.is_in_continuum():
            self.bridging_zone.mesh2Point(field_type, self.rhs)

        if (self.is_in_atomic()):
            field = lm.ComputeExtract(self.getID())
            field.setParam("FIELD", field_type)
            field.compute(self.bridging_zone.pointList)
            if (not self.is_in_continuum()):
                self.rhs = -1 * field.evalArrayOutput().array()
            else:
                tmp = self.rhs.array()
                tmp -= field.evalArrayOutput().array()

        self.bridging_zone.synchronizeVectorBySum("rhs", self.rhs)

    def buildConstraintMatrix(self):
        if self.is_in_continuum():
            shp = self.bridging_zone.smatrix
            print(type(shp))
            print(shp.shape)
            lumped_shape = np.zeros((shp.shape[0], 1))
            print(lumped_shape.shape)
            for i in range(shp.shape[0]):
                lumped_shape[i] = shp[i, :].sum()

            self.bridging_zone.setPBCSlaveFromMaster(lumped_shape)
            A = (shp.transpose() *
                 (1. / self.lambdas_mesh.array() * lumped_shape))

        if self.is_in_atomic():
            if not self.is_in_continuum():
                A = np.zeros(self.lambdas_point.rows(),
                             self.lambdas_point.cols())

            A += 1. / self.lambdas_point.array()
        # synch constraint matrix */
        self.bridging_zone.synchronizeVectorBySum("A", self.A)

    def init(self, domA, domC):
        print(domA, domC)
        # super().init(domA, domC)

        # allocate the bridging zone(parallel version)
        self.bridging_zone.params.geometry = self.bridging_geom
        self.bridging_zone.params.check_coherency = False
        self.bridging_zone.params.centroid = self.centroid_flag

        # set the pbc pairs
        self.bridging_zone.setPBCPairs(domC.getPBCpairs())
        # initialize the bridging_zone object
        self.bridging_zone.init(domA.getContainer(), domC.getContainer())
        # allocate the vectors necessary to continue
        if (self.is_in_atomic()):  # or bridging_zone.getNumberLocalMatchedPoints()
            self.bridging_zone.attachVector(self.A)
            # self.allocate(bridging_zone.getNumberLocalMatchedPoints())

        # build weights
        if (self.is_in_atomic()):
            self.computeAtomicWeights(self.bridging_zone.pointList)

        if (self.is_in_continuum()):
            self.computeContinuumWeights(self.bridging_zone.meshList)

        # build constraint matrix
        self.buildConstraintMatrix()

        # now treat the boundary zone
        self.boundary_zone.params.geometry = self.boundary_geom
        self.boundary_zone.params.check_coherency = False
        self.boundary_zone.params.centroid = self.centroid_flag
        # set the pbc pairs
        self.boundary_zone.setPBCPairs(domC.getPBCpairs())
        # initialize the bridging_zone object
        self.boundary_zone.init(domA.getContainer(), domC.getContainer())
        # self.boundary_zone.projectAtomicDOFsOnMesh(_displacement)
        self.boundary_zone.projectAtomicFieldOnMesh(lm.velocity)
        self.boundary_zone.setPointField(
            lm.force, 0., lm.spatial_dimension.fget())
