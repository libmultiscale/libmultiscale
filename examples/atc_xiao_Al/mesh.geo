
h = 15;
LX = 57.20861214259503;
LY = 70.06595432107383;
LZ = 49.54411143073819;

Point(1) = {0.0, 0.0, 0.0, h};
Point(2) = {LX,   0.0, 0.0, h};
Point(3) = {0.0, LY,   0.0, h};
Point(4) = {LX,   LY,   0.0, h};

Point(5) = {0.0, 0.0, LZ, h};
Point(6) = {LX,   0.0, LZ, h};
Point(7) = {0.0, LY,   LZ, h};
Point(8) = {LX,   LY,   LZ, h};

Line(27) = {2, 4};
Line(28) = {4, 3};
Line(29) = {3, 1};
Line(30) = {1, 2};
Line(31) = {7, 5};
Line(32) = {5, 6};
Line(33) = {6, 8};
Line(34) = {8, 7};
Line(35) = {1, 5};
Line(36) = {3, 7};
Line(37) = {8, 4};
Line(38) = {6, 2};
Curve Loop(31) = {27, 28, 29, 30};
Plane Surface(32) = {31};
Curve Loop(32) = {36, 31, -35, -29};
Plane Surface(33) = {32};
Curve Loop(33) = {31, 32, 33, 34};
Plane Surface(34) = {33};
Curve Loop(34) = {38, 27, -37, -33};
Plane Surface(35) = {34};
Curve Loop(35) = {28, 36, -34, 37};
Plane Surface(36) = {35};
Curve Loop(36) = {32, 38, -30, 35};
Plane Surface(37) = {36};

Transfinite Surface "*";

Surface Loop(1) = {34, 33, 36, 32, 35, 37};
Volume(1) = {1};

Transfinite Volume "*";
