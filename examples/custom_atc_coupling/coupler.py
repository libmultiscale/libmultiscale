#!/usr/bin/python3

import pylibmultiscale as lm
import numpy as np


class MyCoupler(lm.CouplingAtomicContinuum):

    def __init__(self, name):
        super().__init__(name)
        self.domA = None
        self.domC = None
        self.geometry = None

    def coupling(self, stage):
        if stage != lm.COUPLING_STEP2:
            return
        print(stage)
        # example of getting atom positions
        pos = self.domA.getField(lm.position)
        print(pos)

        # example of extracting the position (carefull lots of copies involved)
        filter = lm.FilterGeometry(self.getID()+":filter")
        if self.geometry is None:
            raise RuntimeError("geometry member must be set")
        filter.params.geometry = self.geometry

        filter.compute(self.domA.getContainer())
        extractor = lm.ComputeExtract(self.getID()+":extract")
        extractor.params.field = lm.position
        extractor.compute(filter.evalOutput())
        filtered_positions = extractor.evalArrayOutput()
        # print(filtered_positions.array())
        # modify them as you wish
        # ....
        # apply the changes
        stimulator = lm.StimulationField(self.getID()+":stimulator")
        stimulator.params.field = lm.position
        stimulator.inputs.field = filtered_positions
        stimulator.inputs.dofs = filter.evalOutput()
        stimulator.stimulate()

    def init(self, domA, domC):
        super().init(domA, domC)
        self.domA = domA
        self.domC = domC
