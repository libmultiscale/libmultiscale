#!/usr/bin/python3

import pylibmultiscale as lm
import numpy as np
from time_integration import loop
from coupler import MyCoupler

lm.loadModules()
Dim = 3
lm.spatial_dimension.fset(Dim)

# create a communicator and the groups
comm = lm.Communicator.getCommunicator()
all_group = lm.Communicator.getGroup('all')

# lattice and atomic constants
A = 4.04525975883
BX = np.sqrt(2)
BY = np.sqrt(3)
BZ = np.sqrt(6)
AX = BX*A
AY = BY*A
AZ = BZ*A
mass = 26.9815400000000

# geometry constants
WIDTH = 10
HEIGHT = 100
THICKNESS = 5
LX = WIDTH*AX
LY = HEIGHT*AY
LZ = THICKNESS*AZ

# MD geometry constants
MDHEIGHT = 30
HALF_MDHEIGHT = MDHEIGHT/2
MDLY = MDHEIGHT*AY

# FEM geometry constants
FEMHEIGHT = 10
FEMLY = FEMHEIGHT*AY

# bridging size
BRIDGE = 5*AY
PAD = 1.5*AY

# little epsilon to match dimensions
eps = A*0.1

names = list(globals().keys())
for name in names:
    v = globals()[name]
    if isinstance(v, float) or isinstance(v, int):
        lm.Parser.setAlgebraicVariable(name, float(v))

_vars = lm.Parser.getAlgebraicVariables()

# define explicitely the geometries
gManager = lm.GeometryManager.getManager()


md_geom = lm.Cube(Dim, 'md')
md_geom.params.bbox = [0, LX, -MDLY/2, MDLY/2, 0, LZ]

fe1_geom = lm.Cube(Dim, 'fe1')
fe1_geom.params.bbox = [0, LX, -LY/2, -MDLY/2+BRIDGE, 0, LZ]

fe2_geom = lm.Cube(Dim, 'fe2')
fe2_geom.params.bbox = [0, LX, MDLY/2-BRIDGE, LY/2, 0, LZ]

bridge1 = lm.Cube(Dim, 'bridge1')
bridge1.params.bbox = [0, LX+eps, -MDLY/2+PAD, -MDLY/2+BRIDGE, 0, LZ+eps]

pad1 = lm.Cube(Dim, 'pad1')
pad1.params.bbox = [0, LX+eps, -MDLY/2, -MDLY/2+PAD+eps, 0, LZ+eps]

bridge2 = lm.Cube(Dim, 'bridge2')
bridge2.params.bbox = [0, LX+eps, MDLY/2-BRIDGE, MDLY/2-PAD,  0, LZ+eps]

pad2 = lm.Cube(Dim, 'pad2')
pad2.params.bbox = [0, LX+eps, MDLY/2-PAD-eps, MDLY/2, 0, LZ+eps]

# create akantu domain #1
dom_fe1 = lm.DomainAkantu3('fe1', all_group)
dom_fe1.params.material_filename = "material.dat"
dom_fe1.params.mesh_filename = "mesh.msh"
dom_fe1.params.pbc = [1, 0, 1]
dom_fe1.params.domain_geometry = fe1_geom
dom_fe1.params.timestep = 1e-3
dom_fe1.params.shift = [0, -MDLY/2-FEMLY+BRIDGE, 0]
dom_fe1.init()

# create akantu domain #2
dom_fe2 = lm.DomainAkantu3('fe2', all_group)
dom_fe2.params.material_filename = "material.dat"
dom_fe2.params.mesh_filename = "mesh.msh"
dom_fe2.params.pbc = [1, 0, 1]
dom_fe2.params.domain_geometry = fe2_geom
dom_fe2.params.timestep = 1e-3
dom_fe2.params.shift = [0, MDLY/2-BRIDGE, 0]
dom_fe2.init()

# create lammps domain
dom_md = lm.DomainLammps3('md', all_group)
dom_md.params.lammps_file = "lammps.conf"
dom_md.params.domain_geometry = md_geom
dom_md.params.timestep = 1e-3
dom_md.init()

arlequin1 = MyCoupler("coupler1")
arlequin1.geometry = bridge1
arlequin1.init(dom_md, dom_fe1)

arlequin2 = MyCoupler("coupler2")
arlequin2.geometry = bridge2
arlequin2.init(dom_md, dom_fe2)

dom = lm.DomainMultiScale.getManager()
dom.addObject(dom_md)
dom.addObject(dom_fe1)
dom.addObject(dom_fe2)
dom.for_each(lambda d: print(d))
couplers = lm.CouplerManager.getManager()
couplers.addObject(arlequin1)
couplers.addObject(arlequin2)
print(dom)

loop(10)
