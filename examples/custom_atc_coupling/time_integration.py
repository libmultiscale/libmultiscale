#!/usr/bin/python3
################################################################
import time
import datetime
import argparse
import pylibmultiscale as lm
from mpi4py import MPI
################################################################
lastdump = -1e30
lastStep = 0
################################################################


def printState(nb_step):

    if lm.lm_my_proc_id.fget() != 0:
        return

    global lastdump, lastStep
    gtime = time.time()
    if (gtime - lastdump) < 2:
        return

    current_step = lm.current_step.fget()
    nstep_done = - lastStep + current_step

    info_steps = "{0:>2d} % - passing step {1:>5d}/{2:<5d}".format(
        int(100*current_step/nb_step), current_step, nb_step)

    if current_step > 0:
        step_per_seconds = nstep_done/(gtime - lastdump)
        time_per_step = datetime.timedelta(
            seconds=(gtime - lastdump)/nstep_done)
        remaining_time = time_per_step * (nb_step-current_step)
        step_per_seconds = "{0:.2f}".format(step_per_seconds)
        info_steps += " {0: >5s} steps/seconds".format(step_per_seconds)
        info_steps += " remaining {0}".format(remaining_time)

    print(info_steps)

    lastStep = current_step
    lastdump = gtime

################################################################


time_step = 1e300


def get_min_time_step(d):
    ts = d.getParam("TIMESTEP")
    global time_step
    time_step = min(time_step, ts)


def loop(nb_step):

    lm.loadModules()

    comm = MPI.COMM_WORLD
    comm.Barrier()

    dom = lm.DomainMultiScale.getManager()
    actions = lm.ActionManager.getManager()
    nb_step += lm.current_step.fget()

    comm.Barrier()

    shouldPrintState = True

    dom.for_each(get_min_time_step)
    print("Multiscale timestep:", time_step)

    current_time = lm.current_step.fget()*time_step

    for current_step in range(lm.current_step.fget(), nb_step):
        lm.current_step.fset(current_step)
        current_time += time_step

        if shouldPrintState:
            printState(nb_step)

        lm.current_stage.fset(lm.PRE_DUMP)
        actions.action()
        lm.current_stage.fset(lm.PRE_STEP1)
        actions.action()

        def newmark_predictor(d):
            v = d.primalTimeDerivative()
            p = d.primal()
            acc = d.acceleration()

            v += 0.5 * time_step * acc
            p += time_step * v

            d.changeRelease()

        dom.for_each(newmark_predictor)

        dom.for_each(lambda d:  d.enforceCompatibility())

        dom.coupling(lm.COUPLING_STEP1)

        lm.current_stage.fset(lm.PRE_STEP2)
        actions.action()

        dom.for_each(lambda d: d.updateGradient())

        dom.coupling(lm.COUPLING_STEP2)

        lm.current_stage.fset(lm.PRE_STEP3)
        actions.action()

        dom.for_each(lambda d: d.updateAcceleration())

        def newmark_corrector(d):
            v = d.primalTimeDerivative()
            acc = d.acceleration()

            v += 0.5 * time_step * acc

            d.changeRelease()

        dom.for_each(newmark_corrector)

        dom.coupling(lm.COUPLING_STEP3)

        lm.current_stage.fset(lm.PRE_STEP4)
        actions.action()

        dom.coupling(lm.COUPLING_STEP4)
        comm.Barrier()

    lm.current_stage.fset(lm.PRE_DUMP)
    actions.action()
    lm.current_stage.fset(lm.PRE_STEP1)
    actions.action()

    lm.closeModules()
    print("Done")

################################################################
