.. |lm| replace:: *LibMultiScale*
		  
Known limitations in |lm|
=========================

#. When using Akantu, only meshes with a single element type can be used. |lm| assumes
   that there is only one type for simplifications in the wiring of elements to be provided
   by Akantu. This is not a strong requirement, but rather a missing feature to be developped
   once the need from users shows up.

   
