Configuration files
-------------------

Sections
````````
Configuration files are divided in sections which admit the syntax

.. code-block::

 Section NAME UNITS
  ...
 endSection


The user can place commands in the section with the simple structure

.. code-block::

 Section NAME UNITS
  ...
  KEYWORD arg1 arg2 ...
  ...
 endSection

where units can be

- RealUnits: SI units
  
  - length = meters
  - mass = kilograms
  - energy = Joule
  - time = seconds
  - temperature = Kelvins
- RealUnits
  
  - length = angstroms
  - mass = grams per mol
  - energy = kilo calory per mol
  - time = femto seconds
  - temperature = Kelvins
- MetalUnits
  
  - length = angstroms
  - mass = grams per mol
  - energy = eV
  - time = pico seconds
  - temperature = Kelvins

The unit system specify, for all values in the described section,
in what units are expressed the numerical values.


The parser allows multiline arguments or block reported arguments such as
the examples below

.. code-block::

 Section NAME UNITS
  ...
  KEYWORD arg1 \
	  arg2 \
	  arg3
  ...
  KEYWORD arg1 BLOCK_PARAMS section2

 endSection

 #second section called by the BLOCK_PARAM
 Section section2 UNITS
   arg1 arg2
   arg3
   arg4
 endSection


\subsection{Variables}
Variables can be defined using the LET command

.. code-block::

   LET varname = cos(pi/2)

As you can see an algebraic parser is included and allows to
compute values within scripts and benefit from common unary functions.
all variable names can be recalled within arguments like

.. code-block::
   
 Section NAME UNITS
  ...
  LET varname = cos(pi/2)
  KEYWORD varname
  ...
 endSection

The user can also construct variable names with other variables using
the ${} dereferencing operator

.. code-block::
   
 Section NAME UNITS
  ...
  LET ANGLE = pi/2
  LET cos${ANGLE} = cos(ANGLE)
  KEYWORD cos${ANGLE} arg2 arg3
  PRINT cos${ANGLE}
  ...
 endSection

Environment variables can also be defined and used using $() operator

.. code-block::

 Section NAME UNITS
  ...
  LET ANGLE = $(myangle)
  LET cos${ANGLE} = cos(ANGLE)
  KEYWORD cos${ANGLE} arg2 arg3
  PRINT cos${ANGLE}
  ...
 endSection
 
In the example, &myangle& must be defined in the environment context.


Control blocks
``````````````

It is possible to use loops within the configuration files.
The iteration is done following the syntax

.. code-block::

 FOR k in a b c
    LET name = k
 endFOR

or to explore a range of numerical values

.. code-block::

 LET index = 0
 FOR k in range start end increment
    LET index = index + k
 endFOR


General keywords
````````````````

A *LibMultiScale* configuration file needs a **MultiScale** section.
It is the entry point for all configuration files.

.. code-block::

 Section MultiScale Units
  ...
 endSection


First of all, the **MultiScale** section should start by defining what
is the unit system used **internally** by the code. It can differ
from the unit system of the section since the *LibMultiScale* parser
will handle the conversion for the user.
Secondly, the dimension of the space that is going to be simulated
needs to be given. For instance, these two things can be provided like

.. code-block::
   
 Section MultiScale RealUnits
  ...
  UNITCODE RealUnits
  DIMENSION 3
  ...
 endSection

Then you can construct some objects between three kinds

- MODELS: It is the storage of degrees of freedom and can be of nature:

  - :ref:`md`
  - :ref:`continuum`
  - :ref:`dd`

- :ref:`dumper`: action that generate output files of various forms.
- :ref:`stimulation`: action that alter a set of degrees of freedom, for instance to impose thermostats or initial conditions.
- :ref:`filter`: special container which select a subset of another container based on varisous criteria
- :ref:`filter`: special container which contains real value from dedicated operations.
- :ref:`coupling`: request creation of a coupler

In general these object are created following the syntax

.. code-block::

   FAMILY nameID TYPE INPUT input KEY1 param1 KEY2

And a full description is provided in the lexicon presented in chapter \ref{chapter:lexicon}.
