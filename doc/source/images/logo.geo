l = 1;
fx = .2;
fy = 1;
w = 0.22;
space = 0.02;

Point(1) =  {fx*0,   fy*l/4, 0, w};
Point(2) =  {fx*l,   fy*l/4, 0, w};
Point(3) =  {fx*l,   fy*2*l/3, 0, w};
Point(4) =  {fx*2*l, fy*l/3, 0, w};
Point(5) =  {fx*3*l, fy*2*l/3, 0, w};
Point(6) =  {fx*3*l, fy*0, 0, w};
Point(7) =  {fx*4*l, fy*0, 0, w};
Point(8) =  {fx*4*l, fy*l, 0, w};
Point(9) =  {fx*3*l, fy*l, 0, w};
Point(10) = {fx*2*l, fy*   2*l/3, 0, w};
Point(11) = {fx*l,   fy*   l, 0, w};
Point(12) = {fx*0,   fy*   l, 0, w};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};
Line(6) = {6, 7};
Line(7) = {7, 8};
Line(8) = {9, 8};
Line(9) = {9, 10};
Line(10) = {10, 11};
Line(11) = {11, 12};
Line(12) = {12, 1};
Curve Loop(1) = {12, 1, 2, 3, 4, 5, 6, 7, -8, 9, 10, 11};
Plane Surface(1) = {1};


// the L
Point(13) =  {fx*l,    fy*0, 0, w};
Point(14) =  {fx*l,    fy*l/4-space, 0, w};
Point(15) =  {-space,   fy*l/4-space, 0, w};
Point(16) =  {-space,   fy*l, 0, w};
Point(17) =  {fx*-l,   fy*l, 0, w};
Point(18) =  {fx*-l,   fy*0, 0, w};

Line(13) = {18, 13};
Line(14) = {13, 14};
Line(15) = {14, 15};
Line(16) = {15, 16};
Line(17) = {16, 17};
Line(18) = {17, 18};
