.. image:: images/logo.svg
   :width: 200
   :target: https://gitlab.com/libmultiscale/libmultiscale

LibMultiScale manual
====================

In simulations, particle approaches can be helpful when the
discreteness of matter needs to be taken into account. Multiscale
coupling methods allow to reduce the prohibitive computational costs
of discrete methods. For instance, with these approaches one can
couple an atomic description with a macroscopic model of continuum
mechanics.

The LibMultiScale is a C++ parallel framework (MPI-based only) for the multiscale
coupling methods dedicated to material simulations. This framework is
designed as a library providing an API which makes it possible to
program coupled simulations. The coupled parts can be provided by
existing projects. In such a manner, the API gives C++ templated
interfaces to reduce to the maximum the cost of integration taking the
form of plugins or alike. LAMMPS (Sandia laboratories) and Akantu
(LSMS) have been integrated to provide a functional framework.

The LibMultiScale is now distributed with a joint :ref:`CECILL-C` and :ref:`LGPL`
open-source licence with a shared copyrights between INRIA, CEA and
EPFL. 

Table of content
================

.. toctree::
   :maxdepth: 2

   licences	      
   Installation
   Conffile
   Python
   keywords
   api
   development
   limitations


