Developer Guide
===============

Component
---------

Component types
~~~~~~~~~~~~~~~

A component (STIMULATION, COMPUTE, and FILTER) is an object which takes an arbitrary number of inputs and return an arbitrary number of outputs. A STIMULATION component modifies the input, a COMPUTE component execute a calcul specific to the input, and a FILTER component filters some part of the input. FILTER and COMPUTE components are located in the src/filter folder, while STIMULATION components are located in the src/stimulation folder. 


Component creation
~~~~~~~~~~~~~~~~~~

Some specific steps must be executed in order to create a component in LibMultiscale.

- In the header file, the component must be declared calling a DECLARE function. The first argument to declare a component is the component name followed by the type of the input. The type of the input must be specified inside `_or<>`, and several types can be specified. All the possible input types are mentionned in the src/common/possible_types.hh file. 

.. code-block:: c++

   DECLARE_COMPUTE(ComputeName, _or<ObjectType> );
   DECLARE_FILTER(FilterName _or<ObjectType>);
   DECLARE_STIMULATION(StimulationName, _or<ObjectType>);

- In the source file, the objects must be built calling the MAKE_CALL function.

.. code-block:: c++
		
   DECLARE_COMPUTE_MAKE_CALL(ComputeName)
   DECLARE_FILTER_MAKE_CALL(ComputeName)
   DECLARE_SIMULATION_MAKE_CALL(ComputeName)

- Outputs must be created in the constructor

.. code-block:: c++

   this->createOutput("output");		
   this->createArrayOutput("ArrayOutput");

- To complete the creation of a new component it must be declared in the packages/filters.cmake file for the COMPUTE and FILTER components, and in the packages/stiulation.cmake file for the STIMULATION component. 		


Component exceptions
~~~~~~~~~~~~~~~~~~~~

- If the component includes several types of inputs, the user must declare himself the make_call_function.

.. code-block:: c++

   void ComponentName::compute_make_call() {
     try {
       this->build<dispatch>(this->getInput("FirstInput"), this->getInput("SecondInput")); (COMPUTE/FILTER component)
       this->stimulate<dispatch>(this->getInput("FirstInput"), this->getInput("SecondInput")); (STIMULATION component)
     } catch (Component::UnconnectedInput &e) {
       LM_FATAL("Unconnected input for component " << this->getID() << std::endl
                                                   << e.what());
     }
   }

Additionally, the user must specify in the constructor the new inputs, while deleting the original input `input1`.

.. code-block:: c++
		
  this->createInput("FirstInput");
  this->createInput("SecondInput");
  this->removeInput("input1");

   
- If the component does not specify any types of object, the user must also declare himself the make_call_function.

.. code-block:: c++

   void ComponentName::compute_make_call() { this->build(); }
