.. |lm| replace:: *LibMultiScale*
		  
Python interface to |lm|
========================

Installation
------------

After a successful compilation, the easiest to install the
python modules is to proceed with a *pip* installation

.. code-block:: bash

  cd build/python
  pip install --user -e .

Getting started
---------------

Module Import
`````````````

A typical import sequence would be:


.. code-block:: python

  import pylibmultiscale as lm
  lm.loadModules()
  Dim = 3
  lm.spatial_dimension.fset(Dim)

Where the dimension must be set for your problem




Naming convention
`````````````````

The python interface follows the `C++ API <doxygen/classes.html>`_
thanks to the binding written with `pybind11 <https://github.com/pybind>`_.
For instance, all domains, stimulators, dumpers, and computes are
accessible in python.

However, since templates are not included in the features of the python
language, a naming convention allows to translate C++ class names into python
class names: the explicit types are concatenated and added to the python class
name.

For instance, considering the following templated class

.. code-block:: cpp

   template <typename A, typename B, UInt dim> class MyDomain;

instanciated as

.. code-block:: cpp

   MyDomain<Real, int, 3>;


would have an equivalent python class name as

.. code-block:: python

   class MyDomainRealint3

Creating a domain
`````````````````

The current description is based on the example
which can be found in the directory *examples/single_scale_continuum_akantu*.

Creating a domain is done by instanciating a class in python style:

.. code-block:: python

   # getting a communicator
   comm = lm.Communicator.getCommunicator()
   all_group = comm.getGroup('all')
   # actual creation of the domain
   dom = lm.DomainAkantu3('fe', all_group)

   
**Important information** *: At present time, the python interface is only sequential.*


Setting the configuration is done by accessing the *params* member:

.. code-block:: python

 dom.params.material_filename = "material.dat"
 dom.params.mesh_filename = "mesh.msh"
 dom.params.pbc = [1, 0, 1]
 dom.params.domain_geometry = "fe_geom"
 dom.params.timestep = 1.

which is equivalent to the following configuration

.. code-block::

 Section AKANTU:fe RealUnits
 DOMAIN_GEOMETRY fe_geom
 MESH_FILENAME mesh.msh
 MATERIAL_FILENAME material.dat
 TIMESTEP 1
 PBC 1 0 1
 endSection

Then the domain needs to be initialized

.. code-block:: python

 dom.init()

The needed geometries can be created with

.. code-block:: python

 # create the geometries
 cube = lm.Cube(Dim, 'fe_geom')
 cube.params.bbox = [0, 1, 0, 1, 0, 1]
 cube.init()

 gManager = lm.GeometryManager.getManager()
 gManager.addGeometry(cube)


Connecting to other components
``````````````````````````````

The domains are valid inputs for many other
components. For instance, one can use the paraview
dumper to visualize the domain.

This can be done with:

.. code-block:: python

   dumper = lm.DumperParaview("aka")
   # configure
   dumper.params.disp = True
   dumper.params.vel = True
   dumper.params.force = True

   # connect to domain
   dumper.params.input = dom

   # initialize
   dumper.init()

Then it is possible to call the pipeline chain with:

.. code-block:: python

   dumper.dump()

which will produce praview files.

Time evolution
``````````````

It is possible to implement your own time integrator
by claiming for the proper fields and by modifying them.

For instance a *velocity-verlet* integration step can
be implemented as:


.. code-block:: python

   u = dom.primal()  # displacement   
   v = dom.primalTimeDerivative()
   acc = dom.acceleration()

   # predictor
   v += 0.5 * time_step * acc
   u += time_step * v

   # treats syncing for parallelism
   dom.enforceCompatibility();

   # update force&acceleration
   dom.updateGradient()
   dom.updateAcceleration()

   # corrector
   v += 0.5 * time_step * acc;
