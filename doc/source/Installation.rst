Getting Started
===============

.. |lm| replace:: *LibMultiScale*

Dependencies
------------

For `Debian bullseye <https://www.debian.org/releases/bullseye/>`_ ,
`Ubuntu 19.10 (Eoan Ermine) <https://releases.ubuntu.com/19.10/>`_ ,
and
`Ubuntu 20.04 (Focal Fossa) <https://releases.ubuntu.com/20.04/>`_ 
the necessary dependencies can be installed with

.. code-block:: bash

  sudo apt-get -qq update && sudo apt-get install -y cmake build-essential python3-dev python3-numpy libboost-dev libfftw3-dev libgsl-dev gfortran liblapack-dev libopenmpi-dev sharutils git doxygen graphviz python3-pip texlive-latex-base libmumps-dev libscotch-dev

In order to be able to generate the documentation the following may be issued

.. code-block:: bash

  sudo apt-get install -y git doxygen graphviz python3-pip texlive-latex-base
  pip3 install --user sphinx breathe sphinx-rtd-theme

Getting source code
-------------------

|lm| source code can be downloaded from the gitlab repository:

.. code-block:: bash

   git clone https://gitlab.com/libmultiscale/libmultiscale.git


Fetching the submodules
-----------------------

In order to compile the ParaDiS plugin, a submodule download is required:

.. code-block:: bash

  # ParaDiS discrete dislocation dynamics support
  git submodule update --init third-party/paradis


Compiling
---------

|lm| is a *cmake* project. Therefore to configure it you can either follow the usual way:

.. code-block:: bash

   cd libmultiscale
   mkdir build
   cd build
   ccmake <path to libmultiscale sources>

*[ Set the options that you need ]*

Setting the option at the command line is possible by specifying the activated options

.. code-block:: bash

   cmake -DCMAKE_BUILD_TYPE:STRING=Release -DLIBMULTISCALE_MD1D:BOOL=ON -DLIBMULTISCALE_MECA1D:BOOL=ON -DLIBMULTISCALE_LAMMPS_PLUGIN:BOOL=ON -DLIBMULTISCALE_AKANTU_PLUGIN:BOOL=ON -DLIBMULTISCALE_PARADIS_PLUGIN:BOOL=ON -DLIBMULTISCALE_DOCUMENTATION:BOOL=ON ..

.. code-block:: bash
		
   make
   make install

