#===============================================================================
# @file   CMakeLists.txt
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
#
# @date   Mon Jul 28 12:20:03 2014
#
# @brief  Principal CMamke for the documentation
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

find_package(Doxygen REQUIRED)

if(NOT DOXYGEN_FOUND)
  error("Doxygen is necessary to build the documentation")
endif(NOT DOXYGEN_FOUND)
  
set(DOXYGEN_WARNINGS NO)
set(DOXYGEN_QUIET "YES" CACHE STRING "switch to make doxygen quiet or not")
mark_as_advanced(DOXYGEN_QUIET)
if(CMAKE_VERBOSE_MAKEFILE)
  set(DOXYGEN_WARNINGS YES)
  set(DOXYGEN_QUIET "NO" CACHE STRING "switch to make doxygen quiet or not" FORCE)
endif(CMAKE_VERBOSE_MAKEFILE)
add_subdirectory(doxygen)

set(file_list_source_files "${CMAKE_CURRENT_BINARY_DIR}/list_source_files")
file(WRITE "${file_list_source_files}" "")

package_get_all_source_files(
  LIBMULTISCALE_SRCS
  LIBMULTISCALE_PUBLIC_HDRS
  LIBMULTISCALE_PRIVATE_HDRS
  )

set(LIBMULTISCALE_DEPEND_FILES ${LIBMULTISCALE_SRCS} ${LIBMULTISCALE_PUBLIC_HDRS} ${LIBMULTISCALE_PRIVATE_HDRS})
foreach(_file ${LIBMULTISCALE_DEPEND_FILES})
  file(APPEND "${file_list_source_files}"
    "${_file}
")
endforeach()

add_custom_target(link_doc_source
  COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_CURRENT_SOURCE_DIR}/source ${CMAKE_CURRENT_BINARY_DIR}/source)

add_custom_command(
  COMMENT "Parse keyowrds from inline doc"
  OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/source/keywords.rst
  COMMAND python3 ${CMAKE_CURRENT_SOURCE_DIR}/keywords/generate_doc.py ${file_list_source_files} ${CMAKE_CURRENT_BINARY_DIR}/source/keywords.rst
  DEPENDS ${LIBMULTISCALE_DEPEND_FILES}
  )


add_custom_command(
  COMMENT "Launch sphinx"
  OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/build/toto
  COMMAND sphinx-build -M html ${CMAKE_CURRENT_BINARY_DIR}/source ${CMAKE_CURRENT_BINARY_DIR}/build
  DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/source/keywords.rst libmultiscale-doc-doxygen
  )

add_custom_target(libmultiscale-doc DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/build/toto libmultiscale-doc-doxygen link_doc_source)

