/**
 * @file   lm_doc_data.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Oct 23 16:09:08 2013
 *
 * @brief  Manual parser data
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_LM_DOC_DATA_HH__
#define __LIBMULTISCALE_LM_DOC_DATA_HH__
/* -------------------------------------------------------------------------- */
#include <iostream>
#include <vector>
#include <map>
#include <boost/filesystem.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
/* -------------------------------------------------------------------------- */

struct LMDocData {
  std::string name;                           // item name
  std::string type;                           // item type
  std::string description;                    // description 
  std::vector<LMDocData> children;            // sub items
  std::string var_name;                       // code variable name
  std::string c_type;                         // item type from the code
  boost::filesystem::path filename;           // filename where the thing is stored
  std::vector<std::string> heritance;         // list of heritance
  std::string example;                        // list of examples
  std::string defaults;                       // default values
};

BOOST_FUSION_ADAPT_STRUCT(
			  LMDocData,
			  (std::string, name)
			  (std::string, type)
			  (std::string, description)
			  (std::vector<LMDocData>, children)
			  (std::string, var_name)
			  (std::string, c_type)
			  (boost::filesystem::path, filename)                       
			  (std::vector<std::string>, heritance)                       
			  (std::string, example)                       
			  (std::string, defaults)                       
			  )

/* -------------------------------------------------------------------------- */
struct LMDataNode {
  std::string name;
  std::map<std::string,LMDataNode> children;
  std::vector<LMDocData> data; 
};

/* -------------------------------------------------------------------------- */


#endif /* __LIBMULTISCALE_LM_DOC_DATA_HH__ */
