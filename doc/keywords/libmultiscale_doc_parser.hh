/**
 * @file   libmultiscale_doc_parser.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Mon Oct 28 19:23:14 2013
 *
 * @brief  Manual doc parser
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_LIBMULTISCALE_DOC_PARSER_HH__
#define __LIBMULTISCALE_LIBMULTISCALE_DOC_PARSER_HH__
/* -------------------------------------------------------------------------- */
#include <limits>
#include <boost/spirit/include/qi.hpp>
#include "lm_doc_data.hh"
#include <set>
/* -------------------------------------------------------------------------- */

template <typename Iterator, typename Skipper = boost::spirit::ascii::blank_type>
struct LibMultiScaleDocParser : 
  boost::spirit::qi::grammar<Iterator, LMDocData(), Skipper >{
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  
  LibMultiScaleDocParser();

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

public:

  
  //! parse a single file (filename is stored in the structure)
  bool parse(LMDocData & result,std::set<std::string> & internal_key_list);
  //! call back function for error handling
  void errorHandlerMain(const std::string::const_iterator & a, 
			const std::string::const_iterator& b, 
			const std::string::const_iterator & c, 
			const boost::spirit::info & what);

  //! call back function for error handling
  void errorHandlerType(const std::string::const_iterator & a, 
			const std::string::const_iterator& b, 
			const std::string::const_iterator & c, 
			const boost::spirit::info & what);

  //! call back function for error handling
  void errorHandlerTodo(const std::string::const_iterator & a, 
			const std::string::const_iterator& b, 
			const std::string::const_iterator & c, 
			const boost::spirit::info & what);

  //! call back function for error handling
  void errorHandlerSyntax(const std::string::const_iterator & a, 
			  const std::string::const_iterator& b, 
			  const std::string::const_iterator & c, 
			  const boost::spirit::info & what);


  //! get the line number and last extract last line
  int getLineNumber(const std::string & text, 
		    std::string & now_parsing);


  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */

private:

  //! grammar rules
  boost::spirit::qi::rule<Iterator, LMDocData(), Skipper > start;
  boost::spirit::qi::rule<Iterator, std::string() > name;
  boost::spirit::qi::rule<Iterator, std::string(std::string) > text;
  boost::spirit::qi::rule<Iterator, std::string() > c_type;
  boost::spirit::qi::rule<Iterator, std::string() > brackets;
  boost::spirit::qi::rule<Iterator, std::string(std::string) > innertext;
  boost::spirit::qi::rule<Iterator, LMDocData() > description;
  boost::spirit::qi::rule<Iterator, LMDocData(), Skipper > keyword;
  boost::spirit::qi::rule<Iterator, std::vector<std::string>() > heritance;
  boost::spirit::qi::rule<Iterator, std::string(std::string) > lookup_type;
  boost::spirit::qi::rule<Iterator, std::string() > lookup_todo;
  boost::spirit::qi::rule<Iterator, std::string() > example;
  boost::spirit::qi::rule<Iterator, std::string(std::string) > declaration;
  boost::spirit::qi::rule<Iterator, std::string() > lookup_syntax;
  boost::spirit::qi::rule<Iterator, std::string() > expr;
  boost::spirit::qi::rule<Iterator, std::string() > defaults;
  //boost::spirit::qi::rule<Iterator, std::string() > quantity_type;
  boost::spirit::qi::rule<Iterator, std::string() > vec_quantity_type;

  std::string fname;

  std::string::const_iterator iter;
  std::string::const_iterator end;

};

/* -------------------------------------------------------------------------- */
typedef LibMultiScaleDocParser<std::string::const_iterator> LibMultiScaleParser;
/* -------------------------------------------------------------------------- */

#endif /* __LIBMULTISCALE_LIBMULTISCALE_DOC_PARSER_HH__ */
