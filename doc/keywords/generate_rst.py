import os

################################################################


def makeTypeString(doc):

    if 'c_type' not in doc:
        return "NOT YET ACCESSIBLE"
    return doc['c_type']

    #     boost:: smatch what
    #
    #     const boost:: regex my_filter_quantity_plus_brackets("Quantity<([^,]+),?(.*)>\\[(.*)\\]")
    #
    #     bool res = boost:: regex_match(doc.c_type, what, my_filter_quantity_plus_brackets)
    #
    #     if (res){
    #         std: : cerr << doc.filename.native() << ":0:Error the type "
    #         << doc.c_type << " is invalid" << std: : endl
    #         exit(EXIT_FAILURE)
    #     }
    #
    #     const boost:: regex my_filter_quantity("Quantity<([^,]+),([^,]+)>")
    #     res = boost:: regex_match(doc.c_type, what, my_filter_quantity)
    #
    #     if (res) {
    #         //    std: : cerr << "AAAAAAA"
    #         const boost: : regex my_filter2("Keyword\\s\\[(.*)\\]")
    #         boost:: smatch what2
    #         res = boost: : regex_match(doc.type, what2, my_filter2)
    #         if (!res) return doc.c_type
    #
    #         //    std: : cerr << "AAAAAAA " + doc.type << std: : endl
    #         std: : string ret = what2.str(1)
    #         if (what.str(2) != "") ret += " x "
    #         ret += what.str(1)
    #         return ret
    #     }
    #
    #     const boost:: regex my_filter_single_quantity("Quantity<(.*)>")
    #     res = boost:: regex_match(doc.c_type, what, my_filter_single_quantity)
    #     if (res) {
    #         //    std: : cerr << "BBBBBBBBB"
    #         return what.str(1)
    #     }
    #
    #     const boost:: regex my_filter_brackets("(.*)\\[(.*)\\]")
    #     res = boost: : regex_match(doc.c_type, what, my_filter_brackets)
    #     if (res) {
    #         //    std: : cerr << "CCCCCCCC"
    #         const boost: : regex my_filter2("Keyword\\s\\[(.*)\\]")
    #         boost:: smatch what2
    #         res = boost: : regex_match(doc.type, what2, my_filter2)
    #         if (!res) return doc.c_type
    #
    #         std: : string ret = what2.str(1)
    #         if (what.str(2) != "") ret += " x "
    #         ret += what.str(1)
    #         return ret
    #     }
    #
    #     return doc.c_type
    # }
    raise

################################################################


def getSubPath(_file):

    dirs = _file.split('/')
    i = 0
    for i in range(0, len(dirs)):
        if dirs[i] == 'src':
            break

    if i == len(dirs):
        raise RuntimeError("problem in the file structure")

    return os.path.join(*dirs[i:])

################################################################


def getMainSection(_file):

    dirs = _file.split('/')
    i = 0
    for i in range(0, len(dirs)):
        if dirs[i] == 'src':
            break

    if i == len(dirs):
        raise RuntimeError("problem in the file structure")

    return dirs[i+1]

################################################################


def generateSyntax(fout, doc, level):
    func_name = "syntax " + getMainSection(doc['filename'])
    params = [doc['name']]
    # fout.write(f"{func_name} {params}\n\n")
    generateSection(fout, "**Syntax**", level+1)
    fout.write("TODO\n\n")

################################################################


def generateSection(fout,  title, level):

    if level == 0:
        sep = '='
    elif level == 1:
        sep = '-'
    elif level == 2:
        sep = '`'
    elif level == 3:
        sep = '~'
    elif level == 4:
        sep = '~'
    else:
        raise RuntimeError("what the fuck ", level)

    fout.write(f'''
{title}
{sep*len(title)}

''')

################################################################


def generateKeywordsBloc(fout, keywords, level):

    if not keywords:
        return

    key_list = ", ".join([k['name'] for k in keywords])

    generateSection(fout, "**Keywords** " + f"({key_list})", level+1)
    fout.write("\n\n\n")

    for i, keyword in enumerate(keywords):

        name = keyword['name']
        description = keyword['description'].lstrip('\n')

        default = "*MANDATORY*"
        # params = [keyword['name'], makeTypeString(keyword),
        #           # keyword['var_name'],
        #           ]

        if 'defaults' not in keyword:
            default = "Default: (**MANDATORY**)"
        else:
            default = f"**Default**: {keyword['defaults']}"

        fout.write(f"""
**{name}**:
{description}
     {default}


""")

################################################################


def generateHeritanceList(doc,  hlist, sorted_by_name):

    heritance = doc['heritance']
    # print("heritance:", heritance)
    for i, class_name in enumerate(heritance):
        class_name = class_name.strip()
        if class_name not in sorted_by_name:
            raise RuntimeError(doc['filename'] + ":0:" +
                               "Error: Unknown heritance " + class_name +
                               "\nDeclared classes are:\n" +
                               ", ".join(sorted_by_name.keys())
                               )

        if class_name not in hlist:
            hlist[class_name] = 0
        hlist[class_name] += 1
        # print(f"hlist[{class_name}] = {hlist[class_name]}")
        herit = sorted_by_name[class_name]
        generateHeritanceList(herit, hlist, sorted_by_name)

################################################################


def generateHeritance(fout, doc, hlist, sorted_by_name, level):

    heritance = doc['heritance']
    if len(heritance):
        print(f"\tinherit:{', '.join(heritance)}")
    for i, class_name in enumerate(heritance):
        class_name = class_name.strip()
        if class_name not in sorted_by_name:
            print(doc['filename'] + ":0:" +
                  "Error: Unknown heritance " + class_name)
            continue
        if class_name not in hlist:
            print("internal error")
            continue
        if hlist[class_name] > 1:
            hlist[class_name] -= 1
            print("not outputing " + class_name,
                  "since hlist[" + class_name + "] = " + hlist[class_name])
        herit = sorted_by_name[class_name]
        generateSection(fout, "Inherits from :ref:`" +
                        class_name + "`", level+1)

        generateKeywordsBloc(fout, herit['children'], level)
        # fout.write("herit.children:" +
        #            ", ".join([e['name'] for e in herit['children']]) + "\n\n")
        # fout.write("herit:" + str(herit) + "\n\n")
        # fout.write("hlist:" + str(hlist) + "\n\n")

################################################################


def generateClassInfo(fout, doc, sorted_by_name, level):

    print("GeneratingClassInfo:", doc['name'])
    # get heritance info
    hlist = {}

    generateHeritanceList(doc, hlist, sorted_by_name)

    generateSection(
        fout, "**File**: " + f"{getSubPath(doc['filename'])}", level+1)

    description = doc['description']

    generateSection(fout, "**Description**", level+1)
    if description != "":
        fout.write(f"{description}\n\n")
    generateSyntax(fout, doc, level)
    if doc['example']:
        generateSection(fout, "**Examples**", level+1)
        examples = doc['example']
        if isinstance(examples, list):
            examples = examples[0]
        fout.write(examples + '\n')

    generateKeywordsBloc(fout, doc['children'], level)
    generateHeritance(fout, doc, hlist, sorted_by_name, level)

################################################################


def generateRST(fout, sorted_by_section, sorted_by_name, level=0):

    if sorted_by_section['children']:

        if level != 0:
            print("GeneratingSortedSection:", sorted_by_section['name'])
            fout.write(".. _" + sorted_by_section['name'] + ":\n\n")

            generateSection(
                fout, sorted_by_section['name'], level)
            fout.write("\n------------\n\n")

        for name, child in sorted(sorted_by_section['children'].items()):
            generateRST(fout, child, sorted_by_name, level+1)
    else:
        for data in sorted(sorted_by_section['data'], key=lambda x: x['name']):
            fout.write(".. _" + data['class_name'] + ":\n\n")
            generateSection(fout, data['name'], level)
            generateClassInfo(fout, data, sorted_by_name, level)
            fout.write("\n------------\n\n")
