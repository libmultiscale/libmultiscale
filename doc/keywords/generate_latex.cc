/**
 * @file   generate_latex.cc
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Thu Oct 24 12:01:19 2013
 *
 * @brief  Manual generator
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include <fstream>
#include <vector>
#include <sstream>
#include <algorithm>
#include "generate_latex.hh"
#include <boost/regex.hpp>
/* -------------------------------------------------------------------------- */

GenerateLatex::GenerateLatex(){}
/* -------------------------------------------------------------------------- */


GenerateLatex::~GenerateLatex(){}
/* -------------------------------------------------------------------------- */

std::string makeTypeString(const LMDocData & doc){

  //  std::cerr << "doing " << doc.c_type << std::endl;
  boost::smatch what;  

  const boost::regex my_filter_quantity_plus_brackets("Quantity<([^,]+),?(.*)>\\[(.*)\\]");

  bool res = boost::regex_match(doc.c_type,what,my_filter_quantity_plus_brackets);  

  if (res){
    std::cerr << doc.filename.native() << ":0:Error the type " 
	      << doc.c_type << " is invalid" << std::endl;
    exit(EXIT_FAILURE);    
  }

  const boost::regex my_filter_quantity("Quantity<([^,]+),([^,]+)>");
  res = boost::regex_match(doc.c_type,what,my_filter_quantity);

  if (res) {
    //    std::cerr << "AAAAAAA";
    const boost::regex my_filter2("Keyword\\s\\[(.*)\\]");
    boost::smatch what2;  
    res = boost::regex_match(doc.type,what2,my_filter2);
    if (!res) return doc.c_type;
    
    //    std::cerr << "AAAAAAA " + doc.type << std::endl;
    std::string ret = what2.str(1);
    if (what.str(2) != "") ret += " x ";
    ret += what.str(1);
    return ret;
  }

  const boost::regex my_filter_single_quantity("Quantity<(.*)>");
  res = boost::regex_match(doc.c_type,what,my_filter_single_quantity);
  if (res) {
    //    std::cerr << "BBBBBBBBB";
    return  what.str(1);
  }

  const boost::regex my_filter_brackets("(.*)\\[(.*)\\]");
  res = boost::regex_match(doc.c_type,what,my_filter_brackets);  
  if (res) {
    //    std::cerr << "CCCCCCCC";
    const boost::regex my_filter2("Keyword\\s\\[(.*)\\]");
    boost::smatch what2;  
    res = boost::regex_match(doc.type,what2,my_filter2);
    if (!res) return doc.c_type;
    
    std::string ret = what2.str(1);
    if (what.str(2) != "") ret += " x ";
    ret += what.str(1);
    return ret;
  }  

  return doc.c_type;
}

/* -------------------------------------------------------------------------- */

std::string removeUnderscore(const std::string & str){

  const boost::regex my_filter("\\\\_");

  std::string copy_str = 
    boost::regex_replace(str,my_filter,"",
			 boost::match_default | boost::format_all);

  return copy_str;
}

/* -------------------------------------------------------------------------- */


std::string getSubPath(const boost::filesystem::path & file){

  boost::filesystem::path::iterator end    = file.end();
  boost::filesystem::path::iterator begin  = file.begin();

  for (; end != begin ; --end) {
    if (end->native() == "src"){
      break;
    }
  }
  if (end == file.end()){
    std::cerr << "problem in the file structure" << std::endl;
    exit(EXIT_FAILURE);
  }

  begin = end;
  end = file.end();

  boost::filesystem::path p;

  for (; begin != end ; ++begin) {
    p /= *begin;
  }
  return protectString(p.native());
}
/* -------------------------------------------------------------------------- */

std::string getMainSection(const boost::filesystem::path & file){

  boost::filesystem::path::iterator end    = file.end();
  boost::filesystem::path::iterator begin  = file.begin();

  for (; end != begin ; --end) {
    if (end->native() == "src"){
      break;
    }
  }
  if (end == file.end()){
    std::cerr << "problem in the file structure" << std::endl;
    exit(EXIT_FAILURE);
  }
  ++end;

  return end->native();
}
/* -------------------------------------------------------------------------- */
void makeLatexFunction(std::ofstream & fout,const std::string & func_name,
		       const std::vector<std::string> & params){

  fout << "\\" << func_name;
  for (UInt i = 0; i < params.size(); ++i) {
    fout << "{" << params[i] << "}";
  }
  fout << std::endl << std::endl;
}
/* -------------------------------------------------------------------------- */
void generateSyntax(std::ofstream & fout,const LMDocData & doc){
  std::string func_name = removeUnderscore(std::string("syntax") + getMainSection(doc.filename));
  
  std::vector<std::string> params;
  params.push_back(doc.name);
  makeLatexFunction(fout,func_name,params);
}
/* -------------------------------------------------------------------------- */
void GenerateLatex::generateLatexCall(std::ofstream & fout,
				      const std::string & func) {

  std::vector<std::string> params;
  makeLatexFunction(fout,func,params);
}

/* -------------------------------------------------------------------------- */
void GenerateLatex::generateLatexCall(std::ofstream & fout,
				      const std::string & func,
				      const std::string & arg1) {

  std::vector<std::string> params;
  params.push_back(arg1);
  makeLatexFunction(fout,func,params);
}

/* -------------------------------------------------------------------------- */

void GenerateLatex::generateLatexCall(std::ofstream & fout,
				      const std::string & func,
				      const std::string & arg1,
				      const std::string & arg2) {

  std::vector<std::string> params;
  params.push_back(arg1);
  params.push_back(arg2);
  makeLatexFunction(fout,func,params);
}

/* -------------------------------------------------------------------------- */

void GenerateLatex::generateLatexCall(std::ofstream & fout,
				      const std::string & func,
				      const std::string & arg1,
				      const std::string & arg2,
				      const std::string & arg3) {

  std::vector<std::string> params;
  params.push_back(arg1);
  params.push_back(arg2);
  params.push_back(arg3);
  makeLatexFunction(fout,func,params);
}

/* -------------------------------------------------------------------------- */


void GenerateLatex::generateLatexSection(std::ofstream & fout,
					 const std::string & title,
					 const std::string & label,
					 const std::string level){

  std::vector<std::string> params;
  params.push_back(removeUnderscore(label));
  params.push_back(title);
  params.push_back(label);

  makeLatexFunction(fout,"my"+level,params);
}

/* -------------------------------------------------------------------------- */

void GenerateLatex::generateKeywordsLatexBloc(std::ofstream & fout,
					      std::vector<LMDocData> & keywords){
  
  if (keywords.size() == 0) return;

  std::string key_list = keywords[0].name;
  for(UInt i = 1 ; i < keywords.size(); ++i)
    key_list += ", " + keywords[i].name;

  generateLatexCall(fout,"keywordlist",key_list);

  for (UInt i = 0 ; i < keywords.size() ; ++i){
    LMDocData & keyword = keywords[i];

    std::vector<std::string> params;
    params.push_back(keyword.name);
    params.push_back(makeTypeString(keyword));
    params.push_back(keyword.c_type);
    //    std::cerr << keyword.name << " " << keyword.c_type << std::endl;
    params.push_back(keyword.var_name);
    params.push_back(keyword.description);
    if (keyword.defaults == "")
      params.push_back("\\textbf{MANDATORY}");
    else
      params.push_back("Default: " + keyword.defaults);
    makeLatexFunction(fout,"keyword",params);
  }
}

/* -------------------------------------------------------------------------- */

void 
GenerateLatex::generateHeritanceList(LMDocData & doc,
				     std::map<std::string,UInt> & hlist){

  std::vector<std::string> & heritance = doc.heritance;
  for (UInt i = 0; i < heritance.size(); ++i) {
    std::string class_name = heritance[i];
    if (sorted_by_name.count(class_name) == 0){
      std::cout << doc.filename.native() << ":0:" 
		<< "Unknown heritance " << class_name << std::endl;
      exit(EXIT_FAILURE);
    }  
    hlist[class_name] += 1;
    //    std::cerr << "hlist[" << class_name << "] = " << hlist[class_name] << std::endl;
    LMDocData & herit = sorted_by_name[class_name]; 
    std::stringstream sstr;
    generateHeritanceList(herit,hlist);
  }
}

/* -------------------------------------------------------------------------- */


void GenerateLatex::generateHeritance(std::ofstream & fout,
				      LMDocData & doc,
				      std::map<std::string,UInt> & hlist){

  std::vector<std::string> & heritance = doc.heritance;
  for (UInt i = 0; i < heritance.size(); ++i) {
    std::string class_name = heritance[i];
    if (sorted_by_name.count(class_name) == 0){
      std::cout << doc.filename.native() << ":0:" 
		<< "Unknown heritance " << class_name << std::endl;
      exit(EXIT_FAILURE);
    }
    if (!hlist.count(class_name)) {
      std::cerr << "internal error" << std::endl;
      exit(EXIT_FAILURE);
    }
    if (hlist[class_name] > 1){
      hlist[class_name] -=1;
      // std::cerr << "not outputing " << class_name 
      // 		<< " since hlist[" << class_name << "] = " << hlist[class_name] << std::endl;
      continue;
    }
    // std::cerr << "outputing " << class_name 
    // 	      << " since hlist[" << class_name << "] = " << hlist[class_name] << std::endl;
    
    LMDocData & herit = sorted_by_name[class_name]; 
    std::stringstream sstr;
    sstr << class_name << "[\\ref{" <<  
      removeUnderscore(class_name) << "}]";
    generateLatexCall(fout,"heritance",sstr.str());
    generateKeywordsLatexBloc(fout,herit.children);
    generateHeritance(fout,herit,hlist);
  }
}

/* -------------------------------------------------------------------------- */

void GenerateLatex::generateClassInfo(std::ofstream & fout,
				      LMDocData & doc){
  
  
  //get heritance info
  std::map<std::string,UInt> hlist;
  generateHeritanceList(doc,hlist);
  // std::map<std::string,UInt>::iterator it  = hlist.begin();
  // std::map<std::string,UInt>::iterator end = hlist.end();

  // std::cerr << "4 " << doc.name << " heritance is " << std::endl;
  // while(it != end){
  //   std::cerr << it->first << ": " << it->second << std::endl;
  //   ++it;
  // }
  std::string & description = doc.description;

  if (description != "") generateLatexCall(fout,"description",description);
  generateSyntax(fout,doc);
  if (doc.example != "" && doc.example.find("NONE") == std::string::npos)    
    generateLatexCall(fout,"example",doc.example);
  generateLatexCall(fout,"file",getSubPath(doc.filename));
  
  generateKeywordsLatexBloc(fout,doc.children);
  generateHeritance(fout,doc,hlist);
}

/* -------------------------------------------------------------------------- */
void GenerateLatex::generateLatex(std::ofstream & fout, 
				  LMDataNode & sorted_by_section,
				  std::map<std::string,LMDocData> & sorted_by_name,
				  std::string level){

  this -> sorted_by_name = sorted_by_name;

  if (sorted_by_section.children.size()){
    if (level != "root") {
      generateLatexSection(fout,sorted_by_section.name,
			   sorted_by_section.name,level);
      level = "sub" + level;
    }
    else level = "section";
    
    std::map<std::string,LMDataNode>::iterator it = sorted_by_section.children.begin();
    std::map<std::string,LMDataNode>::iterator end = sorted_by_section.children.end();
    
    while(it != end){
      
      generateLatex(fout,it->second,sorted_by_name,level);
      
      ++it;
    }
  }
  else {

    std::vector<LMDocData>::iterator it = sorted_by_section.data.begin();
    std::vector<LMDocData>::iterator end = sorted_by_section.data.end();
  
    while (it != end){
    
      std::string title = it->name;
      generateLatexSection(fout,title,protectString(it->filename.stem().native()),level);
      generateClassInfo(fout,*it);
      ++it;
    }
  }
}
/* -------------------------------------------------------------------------- */
