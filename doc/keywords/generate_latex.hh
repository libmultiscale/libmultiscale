/**
 * @file   generate_latex.hh
 *
 * @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
 *
 * @date   Wed Oct 23 16:09:08 2013
 *
 * @brief  Manual generator
 *
 * @section LICENSE
 *
 * Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * LibMultiScale is free  software: you can redistribute it and/or  modify it under the
 * terms  of the  GNU Lesser  General Public  License as  published by  the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
 * details.
 *
 * You should  have received  a copy  of the GNU  Lesser General  Public License
 * along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBMULTISCALE_GENERATE_LATEX_HH__
#define __LIBMULTISCALE_GENERATE_LATEX_HH__
/* -------------------------------------------------------------------------- */
#include "lm_doc_data.hh"
#include <map>
#include <boost/regex.hpp>
/* -------------------------------------------------------------------------- */
typedef unsigned int UInt;
/* -------------------------------------------------------------------------- */


inline std::string protectString(const std::string & str){


  const boost::regex my_filter("_");

  std::string copy_str = 
    boost::regex_replace(str,my_filter,"\\\\_",
			 boost::match_default | boost::format_all);

  const boost::regex my_filter2("\\\\\\\\_");

  copy_str = 
    boost::regex_replace(copy_str,my_filter2,"\\\\_",
  			 boost::match_default | boost::format_all);


  const boost::regex my_filter3("#");

  copy_str = 
    boost::regex_replace(copy_str,my_filter3,"diese",
  			 boost::match_default | boost::format_all);
  
  return copy_str;
}

/* -------------------------------------------------------------------------- */



class GenerateLatex {
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  
  GenerateLatex();
  virtual ~GenerateLatex();
  
  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:

  void generateLatexCall(std::ofstream & fout,
			 const std::string & func);
  void generateLatexCall(std::ofstream & fout,
			 const std::string & func,
			 const std::string & arg1);
  void generateLatexCall(std::ofstream & fout,
			 const std::string & func,
			 const std::string & arg1,
			 const std::string & arg2);
  void generateLatexCall(std::ofstream & fout,
			 const std::string & func,
			 const std::string & arg1,
			 const std::string & arg2,
			 const std::string & arg3);

  void generateLatexSection(std::ofstream & fout,
			    const std::string & title,
			    const std::string & label,
			    const std::string level = "root");
  void generateKeywordsLatexBloc(std::ofstream & fout,
				 std::vector<LMDocData> & keywords);
  void generateClassInfo(std::ofstream & fout,
			 LMDocData & doc);
  void generateLatex(std::ofstream & fout, 
		     LMDataNode & sorted_by_section,
		     std::map<std::string,LMDocData> & sorted_by_name,
		     std::string level = "root");

  void generateHeritance(std::ofstream & fout,
			 LMDocData & doc,
			 std::map<std::string,UInt> & hlist);
  void generateHeritanceList(LMDocData & doc,
			     std::map<std::string,UInt> & hlist);

  
  /* ------------------------------------------------------------------------ */
  /* Accessors                                                                */
  /* ------------------------------------------------------------------------ */
public:
  
  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */
private:

  std::map<std::string,LMDocData> sorted_by_name;
  
};

#endif /* __LIBMULTISCALE_GENERATE_LATEX_HH__ */
