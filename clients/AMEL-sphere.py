#!/usr/bin/python3
# -*- coding: utf-8 -*-
################################################################
# ./python/AMEL.py
################################################################
# author : Guillaume ANCIAUX (guillaume.anciaux@epfl.ch, g.anciaux@laposte.net)
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
#
# @date   Mon Sep 08 23:40:22 2014
#
# @brief  This describe the root objects to be combined
#         into valid LM components
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
################################################################
import time
import datetime
import argparse
import pylibmultiscale as lm
from mpi4py import MPI
################################################################
lastdump = -1e30
lastStep = 0
nb_step = 0
################################################################


def printState():

    if lm.lm_my_proc_id.fget() != 0:
        return

    global lastdump, lastStep, nb_step
    gtime = time.time()
    if (gtime - lastdump) < 2:
        return

    current_step = lm.current_step.fget()
    nstep_done = - lastStep + current_step

    info_steps = "{0:>2d} % - passing step {1:>5d}/{2:<5d}".format(
        int(100*current_step/nb_step), current_step, nb_step)

    if current_step > 0:
        step_per_seconds = nstep_done/(gtime - lastdump)
        time_per_step = datetime.timedelta(
            seconds=(gtime - lastdump)/nstep_done)
        remaining_time = time_per_step * (nb_step-current_step)
        step_per_seconds = "{0:.2f}".format(step_per_seconds)
        info_steps += " {0: >5s} steps/seconds".format(step_per_seconds)
        info_steps += " remaining {0}".format(remaining_time)

    print(info_steps)

    lastStep = current_step
    lastdump = gtime

################################################################


def Usage():

    parser = argparse.ArgumentParser(
        description='AMEL: LibMultiScale basic client, Python version')
    parser.add_argument('input_file',
                        help='global config file of the simulation')
    parser.add_argument('nsteps', type=int,
                        help='Number of step wanted to be run (>=0)')

    try:
        args = parser.parse_args()
    except SystemExit as e:
        print("\nGenerated release file: release.info")
        open('release.info', 'w').write(lm.release_info)
        raise e

    return args.input_file, args.nsteps


################################################################
time_step = 1e300


def get_min_time_step(d):
    ts = d.getParam("TIMESTEP")
    global time_step
    time_step = min(time_step, ts)


def main():

    lm.loadModules()

    global nb_step
    conf, nb_step = Usage()

    comm = MPI.COMM_WORLD
    comm.Barrier()

    dom = lm.DomainMultiScale.getManager()
    dom.build(conf)

    actions = lm.ActionManager.getManager()
    nb_step += lm.current_step.fget()

    comm.Barrier()

    shouldPrintState = True

    dom.for_each(get_min_time_step)
    print("Multiscale timestep:", time_step)

    current_time = lm.current_step.fget()*time_step

    for current_step in range(lm.current_step.fget(), nb_step):
        lm.current_step.fset(current_step)
        current_time += time_step

        if shouldPrintState:
            printState()

        lm.current_stage.fset(lm.PRE_DUMP)
        actions.action()
        lm.current_stage.fset(lm.PRE_STEP1)
        actions.action()

        def newmark_predictor(d):
            v = d.primalTimeDerivative()
            p = d.primal()
            acc = d.acceleration()

            v += 0.5 * time_step * acc
            p += time_step * v

            if isinstance(d, lm.DomainLammps3):
                angular_vel = d.getField(lm.angular_velocity)
                angular_acc = d.getField(lm.angular_acceleration)
                angular_vel += 0.5 * time_step * angular_acc
            d.changeRelease()

        dom.for_each(newmark_predictor)

        dom.for_each(lambda d:  d.enforceCompatibility())

        dom.coupling(lm.COUPLING_STEP1)

        lm.current_stage.fset(lm.PRE_STEP2)
        actions.action()

        dom.for_each(lambda d: d.updateGradient())

        dom.coupling(lm.COUPLING_STEP2)

        lm.current_stage.fset(lm.PRE_STEP3)
        actions.action()

        dom.for_each(lambda d: d.updateAcceleration())

        def newmark_corrector(d):
            v = d.primalTimeDerivative()
            acc = d.acceleration()

            v += 0.5 * time_step * acc

            if isinstance(d, lm.DomainLammps3):
                angular_vel = d.getField(lm.angular_velocity)
                angular_acc = d.getField(lm.angular_acceleration)
                angular_vel += 0.5 * time_step * angular_acc

            d.changeRelease()

        dom.for_each(newmark_corrector)

        dom.coupling(lm.COUPLING_STEP3)

        lm.current_stage.fset(lm.PRE_STEP4)
        actions.action()

        dom.coupling(lm.COUPLING_STEP4)
        comm.Barrier()

    lm.current_stage.fset(lm.PRE_DUMP)
    actions.action()
    lm.current_stage.fset(lm.PRE_STEP1)
    actions.action()

    lm.closeModules()
    print("Done")

################################################################


if __name__ == "__main__":
    main()
