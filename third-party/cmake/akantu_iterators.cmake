set(AKANTU_ITERATORS_TARGETS_EXPORT LibMultiScaleLibraryDepends)

if (NOT TARGET akantu_iterators AND NOT LIBMULTISCALE_AKANTU_PLUGIN)
  package_add_to_export_list(akantu_iterators akantu_iterators)
  add_subdirectory(${PROJECT_SOURCE_DIR}/third-party/akantu_iterators)
else()
  package_remove_from_export_list(akantu_iterators akantu_iterators)
endif()

set(AKANTU_ITERATORS_VERSION "master" CACHE INTERNAL "")
set(AKANTU_ITERATORS_LIBRARIES "akantu_iterators" CACHE INTERNAL "")


