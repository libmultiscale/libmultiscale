set(_working_dir ${PROJECT_BINARY_DIR}/third-party/src/akantu-download)
configure_file(${PROJECT_SOURCE_DIR}/third-party/akantu.cmake.in ${_working_dir}/CMakeLists.txt)

if(NOT EXISTS ${PROJECT_SOURCE_DIR}/third-party/akantu/CMakeLists.txt)
  message(STATUS "Downloading akantu")
  execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
    RESULT_VARIABLE result WORKING_DIRECTORY ${_working_dir}
    OUTPUT_FILE ${_working_dir}/configure-out.log
    ERROR_FILE ${_working_dir}/configure-error.log)

  execute_process(COMMAND "${CMAKE_COMMAND}" --build .
    WORKING_DIRECTORY ${_working_dir}
    OUTPUT_FILE ${_working_dir}/build-out.log
    ERROR_FILE ${_working_dir}/build-error.log)
endif()

set(AKANTU_TARGETS_EXPORT LibMultiScaleLibraryDepends)
set(AKANTU_ITERATORS_TARGETS_EXPORT LibMultiScaleLibraryDepends)
set(AKANTU_USE_IOHELPER OFF CACHE BOOL "iohelper option in Akantu" FORCE)
set(AKANTU_PARALLEL ON CACHE BOOL "Add parallel support in Akantu" FORCE)
set(AKANTU_USE_LAPACK ON CACHE BOOL "Add lapack support in Akantu" FORCE)
set(AKANTU_CORE_CXX11 ON CACHE BOOL "core package for Akantu" FORCE)
set(AKANTU_IMPLICIT ON CACHE BOOL "Add the implicit support for Akantu" FORCE)

add_subdirectory(third-party/akantu)
      
get_cmake_property(_cache_variables CACHE_VARIABLES)

# Set all akantu options as off and mark them as advanced
mark_as_advanced(Akantu_DIR)
mask_package_options(Akantu)
     
mark_as_advanced(SCOTCH_LIBRARY_METIS)
package_add_to_export_list(Akantu akantu)
      
if(AKANTU_EXCLUDE_SOURCE_FILES)
  set(_tmp "third-party/akantu/src/${AKANTU_EXCLUDE_SOURCE_FILES}")
  string(REPLACE ";" ";third-party/akantu/src/" _tmp "${_tmp}")
  list(APPEND _tmp ${LIBMULTISCALE_EXCLUDE_SOURCE_FILES})
  set(LIBMULTISCALE_EXCLUDE_SOURCE_FILES ${_tmp} CACHE INTERNAL "")
endif()
      
list(APPEND LIBMULTISCALE_TESTS_EXCLUDE_FILES ${AKANTU_TESTS_EXCLUDE_FILES})
list(APPEND LIBMULTISCALE_DOC_EXCLUDE_FILES ${AKANTU_DOC_EXCLUDE_FILES})
    
set(AKANTU_LIBRARIES "akantu" CACHE INTERNAL "")
package_add_to_export_list(Akantu akantu_iterators)
