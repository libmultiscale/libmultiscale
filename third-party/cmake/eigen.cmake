set(_working_dir ${PROJECT_BINARY_DIR}/third-party/src/eigen-download)
configure_file(${PROJECT_SOURCE_DIR}/third-party/eigen.cmake.in ${_working_dir}/CMakeLists.txt)

if(NOT EXISTS ${PROJECT_SOURCE_DIR}/third-party/eigen/CMakeLists.txt)
  message(STATUS "Downloading eigen")
  execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
    RESULT_VARIABLE result WORKING_DIRECTORY ${_working_dir}
    OUTPUT_FILE ${_working_dir}/configure-out.log
    ERROR_FILE ${_working_dir}/configure-error.log)

  execute_process(COMMAND "${CMAKE_COMMAND}" --build .
    WORKING_DIRECTORY ${_working_dir}
    OUTPUT_FILE ${_working_dir}/build-out.log
    ERROR_FILE ${_working_dir}/build-error.log)
endif()

set(BUILD_TESTING OFF CACHE BOOL "" FORCE)

add_subdirectory(third-party/eigen)

set(EIGEN3_LIBRARIES Eigen3::Eigen CACHE INTERNAL "")

get_cmake_property(_cache_variables CACHE_VARIABLES)
foreach(_var ${_cache_variables})
  if(_var MATCHES "^EIGEN_")
    mark_as_advanced(${_var})
  endif()
  if(_var MATCHES "^CUDA_")
    mark_as_advanced(${_var})
  endif()
  if(_var MATCHES "^PASTIX_")
    mark_as_advanced(${_var})
  endif()
  if(_var MATCHES "^QT_")
    mark_as_advanced(${_var})
  endif()
  if(_var MATCHES "^RT_")
    mark_as_advanced(${_var})
  endif()        

endforeach()
