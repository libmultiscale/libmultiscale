#===============================================================================
# @file   paradis.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Till Junge <till.junge@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
# @author Moseley Philip Arthur <philip.moseley@epfl.ch>
#
# @date   Fri Jul 11 15:47:44 2014
#
# @brief  ParaDiS package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(PARADIS EXTERNAL
  DESCRIPTION "ParaDiS support"
  SYSTEM OFF
  DEFAULT OFF
  COMPILE_FLAGS "-DPARADIS_DISPLACEMENTS -DPARADIS_IN_LIBMULTISCALE"
  )

package_get_name(PARADIS _pkg_name)
_package_use_system(${_pkg_name} _use_system)

if(NOT ${_use_system})
  _package_get_option_name(${_pkg_name} _option_name)
  if(${_option_name})

    set(PARADIS_TARGETS_EXPORT LibMultiScaleLibraryDepends)
    set(PARADIS_IN_LIBMULTISCALE ON CACHE BOOL
      "Specify paradis that he is used inside of libmultiscale" FORCE)
    
    set(PARADIS_DISPLACEMENTS ON CACHE BOOL
      "Enable calculation of displacements in paradis" FORCE)
    
    add_subdirectory(third-party/paradis)
    set_target_properties(ParaDiS PROPERTIES COMPILE_FLAGS "-w")

    foreach(_var ${_cache_variables})
      if(_var MATCHES "^PARADIS_")
        mark_as_advanced(${_var})
      endif()
    endforeach()

    _package_set_libraries(${_pkg_name} ParaDiS)
    _package_set_include_dir(${_pkg_name} ${PARADIS_INCLUDE_DIRS})

    package_add_to_export_list(PARADIS ParaDiS)
  endif()
else()
  add_optional_external_package(ParaDiS "Plugin for PARADIS as DD code" OFF)
endif()
