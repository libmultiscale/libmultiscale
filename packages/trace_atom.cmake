#===============================================================================
# @file   trace_atom.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
#
# @date   Mon Jul 28 12:20:03 2014
#
# @brief  Debugging tool for tracing packages
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(TRACE_ATOM
  DESCRIPTION "Enable system for tracing of atoms"
  DEFAULT OFF
  ADVANCED
  )


package_get_name(LAMMPS _pkg_name)
package_is_activated(${_pkg_name} _is_activated)

if (${_is_activated})
  add_definitions(-DTRACE_ATOM)
endif()

#mark_as_advanced(LIBMULTISCALE_TRACE_ATOM)