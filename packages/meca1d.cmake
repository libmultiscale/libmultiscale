#===============================================================================
# @file   meca1d.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
#
# @date   Mon Oct 28 19:23:14 2013
#
# @brief  FE1D package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(MECA1D
  DESCRIPTION "Plugin for FE 1D code"
  DEFAULT OFF
  )

set(LIBMULTISCALE_CONTINUUM_HDRS_LIST_MECA1D
  continuum/meca1d/domain_meca1d.hh
  )

set(LIBMULTISCALE_MECA1D_FILES
  ${LIBMULTISCALE_CONTINUUM_HDRS_LIST_MECA1D}
  continuum/meca1d/domain_meca1d.cc
  continuum/meca1d/ref_node_meca1d.hh
  continuum/meca1d/mesh_meca1d.hh
  continuum/meca1d/mesh_meca1d.cc
  continuum/meca1d/container_meca1d.hh
  continuum/meca1d/iterator_meca1d.hh
  continuum/meca1d/ref_elem_meca1d.hh
  )

set(LIBMULTISCALE_CONTINUUM_MODEL_LIST_MECA1D
  "((DomainMeca1D,1,MECA1D))"
  )

package_declare_sources(meca1d ${LIBMULTISCALE_MECA1D_FILES})
set_module_headers_from_package(continuum meca1d ${LIBMULTISCALE_CONTINUUM_HDRS_LIST_MECA1D})
declare_boost_list_from_package(continuum_model meca1d ${LIBMULTISCALE_CONTINUUM_MODEL_LIST_MECA1D})
