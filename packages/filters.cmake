#===============================================================================
# @file   filters.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
#
# @date   Thu Sep 18 16:13:26 2014
#
# @brief  Filter package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(filters NOT_OPTIONAL DESCRIPTION "filters/computes package for LibMultiScale")

set(LIBMULTISCALE_FILTERS TRUE)

set(LIBMULTISCALE_FILTER_HDRS_LIST_FILTERS
  filter/compute_centro_symmetry.hh
  filter/compute_ekin.hh
  filter/compute_energydensity.hh
  filter/compute_epot.hh
  filter/compute_extract.hh
  filter/compute_python.hh
  filter/compute_reduce.hh
  filter/compute_impulse.hh
  filter/compute_concat.hh
  filter/compute_true_displacement.hh
  filter/compute_point_set.hh
  filter/compute_mesh.hh
  filter/compute_time_average.hh
  filter/compute_time_delta.hh
  filter/filter_dislo.hh
  filter/filter_geometry.hh
  filter/compute_periodic_position.hh
  filter/filter_threshold.hh
  filter/compute_addpoints.hh
  filter/compute_add_ddsegments.hh
  filter/compute_barnett.hh
  filter/filter_point.hh
  filter/compute_arlequin_weight.hh
  )

set(LIBMULTISCALE_FILTERS_FILES
  ${LIBMULTISCALE_FILTER_HDRS_LIST_FILTERS}
  filter/accept_compute_include.hh
  filter/accept_filter_include.hh
  filter/accept_real_compute.hh
  filter/accept_ref_compute.hh
  filter/accept_ref_filter.hh
  filter/compute_interface.cc
  filter/compute_interface.hh
  filter/compute_centro_symmetry.cc
  filter/compute_ekin.cc
  filter/compute_energydensity.cc
  filter/compute_epot.cc
  filter/compute_extract.cc
  filter/compute_python.cc
  filter/compute_reduce.cc
  filter/compute_impulse.cc
  filter/compute_concat.cc
  filter/compute_true_displacement.cc
  filter/compute_point_set.cc
  filter/compute_mesh.cc
  filter/filter_interface.cc
  filter/filter_dislo.cc
  filter/filter_geometry.cc
  filter/filter_interface.hh
  filter/compute_time_average.cc
  filter/compute_time_delta.cc
  filter/compute_periodic_position.cc
  filter/filter_threshold.cc
  filter/compute_addpoints.cc
  filter/compute_add_ddsegments.cc
  filter/compute_barnett.cc
  filter/filter_point.cc
  filter/compute_arlequin_weight.cc
  )

set(LIBMULTISCALE_FILTER_LIST_FILTERS
  "((FilterGeometry,GEOM))"
  "((FilterDislo,DISLO))"
  "((FilterPoint,POINT))"    
  "((FilterThreshold, THRESHOLD))"
  "((ComputeMesh,MESH))"
  "((ComputePointSet,POINTSET))"
  )

set(LIBMULTISCALE_COMPUTE_LIST_FILTERS
  "((ComputeCentroSymmetry,CENTRO))"
  "((ComputeEKin,EKIN))"
  "((ComputeEnergyDensity,ENERGY_DENSITY))"
  "((ComputeExtract,EXTRACT))"
  "((ComputeEPot,EPOT))"
  "((ComputeImpulse,IMPULSE))"
  "((ComputeTrueDisplacement,TRUEDISP))"
  "((ComputePeriodicPosition,PERIODICPOSITION))"
  "((ComputeReduce,REDUCE))"
  "((ComputePython,PYTHON))"
  "((ComputeConcat,CONCAT))"
  "((ComputeTimeAverage,TIMEAVG))"
  "((ComputeTimeDelta,TIMEDELTA))"
  "((ComputeAddPoints,ADDPOINTS))"
  "((ComputeAddDDSegments,ADD_DDSEGMENTS))"
  "((ComputeBarnett,BARNETT))"
  "((ComputeArlequinWeight,ARLEQUINWEIGHT))"
  )

package_declare_sources(filters ${LIBMULTISCALE_FILTERS_FILES})
set_module_headers_from_package(filter filters ${LIBMULTISCALE_FILTER_HDRS_LIST_FILTERS})
declare_boost_list_from_package(compute filters ${LIBMULTISCALE_COMPUTE_LIST_FILTERS})
declare_boost_list_from_package(filter filters ${LIBMULTISCALE_FILTER_LIST_FILTERS})

