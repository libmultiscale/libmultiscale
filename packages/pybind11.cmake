set(PYBIND11_PYTHON_VERSION ${LIBMULTISCALE_PREFERRED_PYTHON_VERSION} CACHE INTERNAL "")

package_declare(pybind11 EXTERNAL
  EXTRA_PACKAGE_OPTIONS ARGS "2.2;CONFIG" LINK_LIBRARIES pybind11::embed PREFIX pybind11
  DESCRIPTION "LibMultiScale's pybind11 interface"
  SYSTEM AUTO third-party/cmake/pybind11.cmake
  FOUND pybind11_FOUND
  )

set(PYBIND11_CPP_STANDARD -std=c++17)

package_add_third_party_script_variable(pybind11
  PYBIND11_VERSION "master")
package_add_third_party_script_variable(pybind11
  PYBIND11_GIT "https://github.com/pybind/pybind11.git")

