#===============================================================================
# @file   md1d.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
#
# @date   Sun Jan 06 01:16:34 2013
#
# @brief  1D moleculardynamics package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(MD1D 
  DESCRIPTION "Plugin for MD 1D code"
  DEFAULT OFF
  )

set(LIBMULTISCALE_MD_HDRS_LIST_MD1D
  md/md1d/domain_md1d.hh
  )

set(LIBMULTISCALE_MD1D_FILES
  ${LIBMULTISCALE_MD_HDRS_LIST_MD1D}
  md/md1d/container_md1d.hh
  md/md1d/domain_md1d.cc
  md/md1d/iterator_md1d.hh
  md/md1d/md_builder.cc
  md/md1d/md_builder.hh
  md/md1d/ref_atom_md1d.hh
  md/md1d/ref_atom_md1d.cc
  md/md1d/reference_manager_md1d.hh
  )

set(LIBMULTISCALE_ATOM_MODEL_LIST_MD1D
  "((DomainMD1D,1,MD1D))"
  )


package_declare_sources(md1d ${LIBMULTISCALE_MD1D_FILES})
set_module_headers_from_package(md md1d ${LIBMULTISCALE_MD_HDRS_LIST_MD1D})
declare_boost_list_from_package(atom_model md1d ${LIBMULTISCALE_ATOM_MODEL_LIST_MD1D})
