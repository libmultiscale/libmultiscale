#===============================================================================
# @file   blackdynamite.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
#
# @date   Fri Nov 08 11:28:32 2013
#
# @brief  Blackdynamite parametric studies packages
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(BlackDynamite EXTERNAL
  DESCRIPTION "Activate BlackDynamite features"
  SYSTEM OFF
  DEFAULT OFF)

set(LIBMULTISCALE_DUMPER_HDRS_LIST_BLACKDYNAMITE
  dumper/dumper_blackdynamite.hh
  )

set(LIBMULTISCALE_BLACKDYNAMITE_FILES
  ${LIBMULTISCALE_DUMPER_HDRS_LIST_BLACKDYNAMITE}
  dumper/dumper_blackdynamite.cc
  )

set(LIBMULTISCALE_DUMPER_REAL_INPUT_LIST_BLACKDYNAMITE
  "((DumperBlackDynamite,BLACKDYNAMITE))"
  )


package_declare_sources(BlackDynamite ${LIBMULTISCALE_BLACKDYNAMITE_FILES})
