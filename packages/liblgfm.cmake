#===============================================================================
# @file   liblgfm.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
#
# @brief  liblgfm package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(LIBLGFM EXTERNAL
  DESCRIPTION "LIBLGFM support"
  SYSTEM OFF
  DEFAULT OFF
  )

package_get_name(LIBLGFM _pkg_name)
_package_use_system(${_pkg_name} _use_system)

if(NOT ${_use_system})
  _package_get_option_name(${_pkg_name} _option_name)
  if(${_option_name})

    set(LIBLGFM_TARGETS_EXPORT LibMultiScaleLibraryDepends)
    
    add_subdirectory(third-party/liblgfm)

    foreach(_var ${_cache_variables})
      if(_var MATCHES "^LIBLGFM_")
        mark_as_advanced(${_var})
      endif()
    endforeach()

    _package_set_libraries(${_pkg_name} lgfm)
    _package_set_include_dir(${_pkg_name} ${LIBLGFM_INCLUDE_DIRS})

    set(LIBLGFM_PARALLEL ON CACHE BOOL "Activate the Parallel version of LibLGFM" FORCE)
    set(LAMMPS_LGFM ON CACHE BOOL "Activate the LGFM part for lammps" FORCE)
  endif()
else()
  add_optional_external_package(LIBLGFM "Plugin LIBLGFM" FALSE FOUND LIBLGFM)
endif()
