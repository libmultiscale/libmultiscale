#===============================================================================
# @file   mandatory_packages.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
#
# @date   Wed Mar 06 12:17:42 2013
#
# @brief  All mandatory packages
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(mandatory_packages NOT_OPTIONAL
  DESCRIPTION "meta package for mandatory external packages"
  DEPENDS pybind11 PythonLibs lapack mpi expat fftw gsl zlib boost
  )

#add_external_package(MPI)
#
#add_external_package(Boost PREFIX Boost)
#list(APPEND LIBMULTISCALE_EXTERNAL_LIB_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/src)
#list(APPEND LIBMULTISCALE_BOOST_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/src/boost)
#
##Not using the macro because the python module is shity
#find_package(PythonLibs REQUIRED 2.7)
#if(PYTHONLIBS_FOUND)
#  list(APPEND LIBMULTISCALE_DEFINITIONS LIBMULTISCALE_USE_PYTHON)
#  list(APPEND LIBMULTISCALE_EXTERNAL_LIB_INCLUDE_DIR ${PYTHON_INCLUDE_DIR})
#  list(APPEND LIBMULTISCALE_EXTERNAL_LIBRARIES ${PYTHON_LIBRARIES})
#
#  set(LIBMULTISCALE_PYTHON_INCLUDE_DIR ${PYTHON_INCLUDE_DIR})
#  set(LIBMULTISCALE_PYTHON_LIBRARIES   ${PYTHON_LIBRARIES})
#  set(LIBMULTISCALE_PYTHON ON)
#
#  list(APPEND LIBMULTISCALE_OPTION_LIST PYTHON)
#endif()
#
#add_external_package(FFTW)
#add_external_package(GSL)
#add_external_package(LAPACK)
#add_external_package(EXPAT)
#add_optional_external_package(QVIEW "Activate the QView monitoring feature" OFF)
#if (LIBMULTISCALE_USE_QVIEW)
#  add_definitions(-DUSING_QVIEW)
#endif(LIBMULTISCALE_USE_QVIEW)
#mark_as_advanced(LIBMULTISCALE_USE_QVIEW)
#
#add_optional_external_package(ZLIB "Activate the ZLIB compression feature" OFF)
#if (LIBMULTISCALE_USE_ZLIB)
#  add_definitions(-DLIBMULTISCALE_USE_ZLIB)
#endif(LIBMULTISCALE_USE_ZLIB)
#
