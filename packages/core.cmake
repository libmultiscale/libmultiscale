#===============================================================================
# @file   core.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
#
# @date   Mon Oct 13 10:50:54 2014
#
# @brief  Core package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(core NOT_OPTIONAL DESCRIPTION "core package for LibMultiScale")

set(LIBMULTISCALE_CONTINUUM_HDRS_LIST_CORE
  common/container_mesh.hh
)

set(LIBMULTISCALE_DD_HDRS_LIST_CORE
  dd/ref_node_dd.hh
  dd/ref_elem_dd.hh
)

set(LIBMULTISCALE_GEOMETRY_HDRS_LIST_CORE
  geometry/geom_inter.hh
  geometry/cylinder.hh
  geometry/geometry.hh
  geometry/geom_sub.hh
  geometry/geom_composed.hh
  geometry/chaudron.hh
  geometry/ellipsoid.hh
  geometry/cube.hh
  geometry/geom_union.hh
  geometry/ball.hh
  geometry/cube_surface.hh
  )

set(LIBMULTISCALE_CORE_FILES

  common/lm_pack_buffer.hh
  common/lm_type_caster.hh
  common/lm_python_bindings.hh
  common/lm_python_actions.hh
  common/lm_python_components.hh
  common/lm_python_containers.hh
  common/lm_python_couplers.hh
  common/lm_python_domains.hh
  common/lm_python_geometries.hh
  common/lm_python_types.hh
  common/lm_file.hh
  common/lm_file.cc
  common/lm_fwd.hh
  common/lm_libc.cc
  common/action_interface.cc
  common/action_interface.hh
  common/visitor_libmultiscale.hh
  common/model_list.hh
  common/id_manager.hh
  common/lm_log.hh
  common/lm_log.cc
  common/lm_ostream_types.hh
  common/lm_macros.hh
  common/lm_types.hh
  common/lm_globals.hh
  common/lm_globals.cc
  common/lm_common.hh
  common/domain_interface.hh
  common/domain_interface.cc
  common/ref_subset.cc
  common/ref_subset.hh
  common/reference_manager.cc
  common/reference_manager.hh
  common/reference_manager_interface.hh
  common/attached_object.hh
  common/ref_point.hh
  common/ref_point_data.hh
  common/ref_element.hh
  common/component_libmultiscale.hh
  common/component_libmultiscale_inline_impl.hh
  common/component_libmultiscale.cc
  common/lm_functions.cc
  common/lm_functions.hh
  common/lm_timer.cc
  common/lm_timer.hh
  common/lm_object.hh
  common/lm_object.cc
  common/accessor_dof.hh

  parser/lm_parser.cc
  parser/lm_parser_inline_impl.hh
  parser/parse_result.hh
  parser/xml_parser_restart.cc
  parser/algebraic_parser.cc
  parser/lm_parsable.cc
  parser/xml_parser_restart.hh
  parser/lm_parsable.hh
  parser/lm_parsable_inline_impl.hh
  parser/algebraic_parser.hh
  parser/xml_parser.hh
  parser/lm_parser.hh
  parser/lm_parameter.hh
  parser/lm_parameter.cc
  parser/lm_defaults.hh
  parser/lm_defaults.cc

#  ${LIBMULTISCALE_MD_HDRS_LIST_CORE}
  md/spatial_filter_atomic.cc
  md/domain_md.cc
  md/trace_atom.cc
  md/container_neighbor_atoms.hh
  md/trace_atom.hh
  md/domain_md.hh
  md/spatial_filter_atomic.hh
  md/ref_atom.hh
  md/container_neighbor_atoms_interface.hh

  ${LIBMULTISCALE_GEOMETRY_HDRS_LIST_CORE}
  geometry/geometry_manager.cc
  geometry/spatial_grid_libmultiscale.hh
  geometry/geometry_manager.hh
  geometry/spatial_grid.hh
  geometry/spatial_grid.cc
  geometry/geometry.cc
  geometry/cube.cc
  geometry/ball.cc
  geometry/cylinder.cc
  geometry/geom_inter.cc
  geometry/geom_sub.cc
  geometry/geom_union.cc
  geometry/geom_composed.cc
  geometry/surface.hh
  geometry/surface.cc
  geometry/cube_surface.cc

  ${LIBMULTISCALE_CONTINUUM_HDRS_LIST_CORE}
  continuum/domain_continuum.cc
  continuum/ref_node_continuum.hh
  continuum/ref_elem_fe.hh
  continuum/domain_continuum.hh

  ${LIBMULTISCALE_DD_HDRS_LIST_CORE}
  dd/domain_dd.hh
  dd/domain_dd.cc

  factory/domain_multiscale.cc
  factory/factory_multiscale.cc
  factory/factory_multiscale.hh
  factory/domain_multiscale.hh

  units/units.hh
  units/units.cc
  units/units_converter.hh
  units/units_converter.cc
  units/quantity.hh

  communicator/lm_communicator.hh
  communicator/lm_communicator.cc
  communicator/comm_group.hh
  communicator/comm_group_inline_impl.hh

  container/container_array.hh
  container/iterator_array_base_type.hh
  container/container.hh
  container/container.cc
  container/iterator_array.hh
  container/field.hh

  fmm/fmm_multigrid.hh
  fmm/fmm_interface.hh
  fmm/moments.hh
  fmm/fmm.hh
  fmm/radial_function.hh

  math/low_pass_filter.hh
  math/cubic_spline.hh
  math/function_interface.hh
  math/hanning_window.hh
  math/constant_function.hh
  math/gaussian_function.hh
  math/product_function.hh
  math/dispersion_relation.hh
  math/spline_function.hh
  math/tensor.hh
  math/hamming_window.hh
  math/math_tools.hh
  math/math_tools.cc

  common/auto_argument.hh
  common/auto_arguments.hh
  common/possible_types.hh
  )

set(LIBMULTISCALE_GEOMETRY_LIST_CORE 
  "((Cylinder,CYLINDER))"		    
  "((Cube,CUBE))"		    
  "((Ball,BALL))"		    
  "((GeomSub,SUB))"		    
  "((GeomInter,INTER))"		    
  "((GeomUnion,UNION))"		    
  "((CubeSurface,CUBE_SURFACE))"		    
  )

package_declare_sources(core ${LIBMULTISCALE_CORE_FILES})
set_module_headers_from_package(geometry core ${LIBMULTISCALE_GEOMETRY_HDRS_LIST_CORE})
set_module_headers_from_package(continuum core ${LIBMULTISCALE_CONTINUUM_HDRS_LIST_CORE})
set_module_headers_from_package(dd core ${LIBMULTISCALE_DD_HDRS_LIST_CORE})
declare_boost_list_from_package(geometry core ${LIBMULTISCALE_GEOMETRY_LIST_CORE})
