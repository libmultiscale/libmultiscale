#===============================================================================
# @file   akantu_plugin.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Till Junge <till.junge@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
#
# @date   Tue Jul 22 14:47:56 2014
#
# @brief  Akantu Plugin package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(AKANTU_PLUGIN
  DESCRIPTION "Plugin Akantu for LM"
  DEFAULT OFF
  DEPENDS Akantu
  )

set(LIBMULTISCALE_CONTINUUM_HDRS_LIST_AKANTU
  continuum/akantu/domain_akantu.hh
  )

set(LIBMULTISCALE_AKANTU_FILES
  ${LIBMULTISCALE_CONTINUUM_HDRS_LIST_AKANTU}
  continuum/akantu/domain_akantu.cc
  continuum/akantu/iterator_akantu.cc
  continuum/akantu/akantu_dof_wrapper.hh
  continuum/akantu/akantu_dof_wrapper.cc
  continuum/akantu/ref_elem_akantu.hh
  continuum/akantu/ref_elem_akantu.cc
  continuum/akantu/ref_node_akantu.hh
  continuum/akantu/ref_node_akantu.cc
  continuum/akantu/container_akantu.hh
  continuum/akantu/container_akantu.cc
  continuum/akantu/iterator_akantu.hh
  )

set(LIBMULTISCALE_CONTINUUM_MODEL_LIST_AKANTU
  "((DomainAkantu<1>,1,AKANTU))"
  "((DomainAkantu<2>,2,AKANTU))"
  "((DomainAkantu<3>,3,AKANTU))"
  )

package_declare_sources(akantu_plugin ${LIBMULTISCALE_AKANTU_FILES})
set_module_headers_from_package(continuum akantu_plugin ${LIBMULTISCALE_CONTINUUM_HDRS_LIST_AKANTU})
declare_boost_list_from_package(continuum_model akantu_plugin ${LIBMULTISCALE_CONTINUUM_MODEL_LIST_AKANTU})
