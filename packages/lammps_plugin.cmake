#===============================================================================
# @file   lammps_plugin.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
#
# @date   Mon Oct 28 19:23:14 2013
#
# @brief  LAMMPS Plugin package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(lammps_plugin
  DESCRIPTION "Plugin LAMMPS for LM"
  DEFAULT OFF
  DEPENDS lammps
  )

set(LIBMULTISCALE_MD_HDRS_LIST_LAMMPS
  md/lammps/domain_lammps.hh
  )

set(LIBMULTISCALE_LAMMPS_FILES
  ${LIBMULTISCALE_MD_HDRS_LIST_LAMMPS}
  ${LIBMULTISCALE_FILTER_HDRS_LIST_LAMMPS}
  md/lammps/container_lammps.hh
  md/lammps/domain_lammps.cc
  md/lammps/domain_lammps.hh
  md/lammps/epot_hook.hh
  md/lammps/epot_hook.cc
  md/lammps/iterator_lammps.hh
  md/lammps/lammps_common.hh
  md/lammps/ref_atom_lammps.hh
  md/lammps/reference_manager_lammps.cc
  md/lammps/reference_manager_lammps.hh
  md/lammps/stress_hook.hh
  md/lammps/stress_hook.cc
  md/lammps/compute_lammps.cc
  md/lammps/compute_lammps.hh
  md/lammps/atom_vec_lm.hh
  md/lammps/atom_lm.hh
  md/lammps/lammps_force_compute.hh
)


set(LIBMULTISCALE_ATOM_MODEL_LIST_LAMMPS
  "((DomainLammps<2>,2,LAMMPS))"
  "((DomainLammps<3>,3,LAMMPS))"
  )

package_declare_sources(lammps_plugin ${LIBMULTISCALE_LAMMPS_FILES})
set_module_headers_from_package(md lammps_plugin ${LIBMULTISCALE_MD_HDRS_LIST_LAMMPS})
declare_boost_list_from_package(atom_model lammps_plugin ${LIBMULTISCALE_ATOM_MODEL_LIST_LAMMPS})
