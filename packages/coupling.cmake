#===============================================================================
# @file   coupling.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
#
# @date   Tue Feb 05 10:37:52 2013
#
# @brief  Coupling package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(coupling NOT_OPTIONAL DESCRIPTION "coupling package for LibMultiScale")

set(LIBMULTISCALE_COUPLER_HDRS_LIST_COUPLING
  coupling/xiao.hh  
  coupling/kobayashi.hh
  coupling/weak_coupling.hh
  coupling/arlequin_python.hh
  coupling/coupling_atomic_continuum_python.hh
  )


set(LIBMULTISCALE_COUPLING_FILES
  ${LIBMULTISCALE_COUPLER_HDRS_LIST_COUPLING}
  coupling/dof_association.cc
  coupling/kobayashi.cc
  coupling/xiao.cc
  coupling/weak_coupling.cc
  coupling/bridging_atomic_continuum.cc
  coupling/duo_distributed_vector.cc
  coupling/coupler_manager.cc
  coupling/arlequin_template.cc
  coupling/bridging_inline_impl.hh
  coupling/bridging.cc
  coupling/coupler_manager.hh
  coupling/arlequin_template.hh
  coupling/shape_matrix.hh
  coupling/bridging_atomic_continuum.hh
  coupling/dof_association.hh
  coupling/point_association.hh
  coupling/point_association_inline_impl.hh
  coupling/point_association.cc
  coupling/bridging.hh
  coupling/coupling_interface.hh
  coupling/coupling_atomic_continuum.hh
  coupling/coupling_atomic_continuum.cc
  coupling/coupling_dd_continuum.hh
  coupling/coupling_dd_md.hh
  coupling/duo_distributed_vector.hh
  coupling/coupling_dd_continuum.cc
  coupling/coupling_dd_md.cc
  )

set(LIBMULTISCALE_COUPLER_LIST_COUPLING
  "((Xiao,XIAO,ATOMTYPE,CONTINUUMTYPE))"
  "((WeakCoupling,WEAKCOUPLING,ATOMTYPE,CONTINUUMTYPE))"
  "((Kobayashi,KOBAYASHI,ATOMTYPE,CONTINUUMTYPE))"
  )

#  "((Thermal,THERMAL,ATOMTYPE,CONTINUUMTYPE))"
#  "((ThermalPar,THERMALPAR,ATOMTYPE,CONTINUUMTYPE))"
#  "((Adaptive,ADAPTIVE,ATOMTYPE,CONTINUUMTYPE))"

package_declare_sources(coupling ${LIBMULTISCALE_COUPLING_FILES})
set_module_headers_from_package(coupler coupling ${LIBMULTISCALE_COUPLER_HDRS_LIST_COUPLING})
declare_boost_list_from_package(coupler coupling ${LIBMULTISCALE_COUPLER_LIST_COUPLING})
