#===============================================================================
# @file   akantu.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Till Junge <till.junge@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
#
# @date   Tue Jul 22 14:47:56 2014
#
# @brief  Akantu package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(Akantu EXTERNAL
  DESCRIPTION "Akantu support"
  EXTRA_PACKAGE_OPTIONS PREFIX AKANTU FOUND Akantu_FOUND
  SYSTEM AUTO third-party/cmake/akantu.cmake
  DEFAULT OFF
  )

package_add_third_party_script_variable(Akantu
  AKANTU_GIT "https://gitlab.com/akantu/akantu.git")

package_add_third_party_script_variable(Akantu
  AKANTU_VERSION "master")


