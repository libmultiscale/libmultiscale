#===============================================================================
# @file   liblgfm_plugin.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
#
# @brief  LibLGFM Plugin package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(liblgfm_plugin
  DESCRIPTION "Plugin LibLGFM for LM"
  DEFAULT OFF
  DEPENDS liblgfm
  )

set(LIBMULTISCALE_HDRS_LIST_LIBLGFM
  filter/compute_lgfm.hh
  )

set(LIBMULTISCALE_LIBLGFM_FILES
  ${LIBMULTISCALE_HDRS_LIST_LIBLGFM}
  filter/compute_lgfm.cc
)

set(LIBMULTISCALE_LGFM_COMPUTE_LIST_FILTERS
  "((ComputeLGFM,LGFM))"
  )

package_declare_sources(liblgfm_plugin ${LIBMULTISCALE_LIBLGFM_FILES})
set_module_headers_from_package(filter liblgfm_plugin ${LIBMULTISCALE_HDRS_LIST_LIBLGFM})
declare_boost_list_from_package(compute liblgfm ${LIBMULTISCALE_LGFM_COMPUTE_LIST_FILTERS})
