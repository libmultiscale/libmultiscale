#===============================================================================
# @file   lammps.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
#
# @date   Mon Oct 28 19:23:14 2013
#
# @brief  LAMMPS package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(LAMMPS EXTERNAL
  DESCRIPTION "LibMultiscale's LAMMPS support"
  SYSTEM AUTO third-party/cmake/lammps.cmake
  FOUND LAMMPS_FOUND
  DEFAULT OFF
  )

package_add_third_party_script_variable(LAMMPS
  LAMMPS_GIT "https://gitlab.com/libmultiscale/lammps.git")

package_add_third_party_script_variable(LAMMPS
  LAMMPS_VERSION "new_lammps")
