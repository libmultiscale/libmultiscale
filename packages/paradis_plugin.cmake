#===============================================================================
# @file   paradis_plugin.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Till Junge <till.junge@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
# @author Moseley Philip Arthur <philip.moseley@epfl.ch>
#
# @date   Fri Jul 11 15:47:44 2014
#
# @brief  ParaDiS package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(paradis_plugin
  DESCRIPTION "Plugin ParaDiS for LM"
  SYSTEM OFF
  DEFAULT OFF
  DEPENDS PARADIS
  )

set(LIBMULTISCALE_DD_HDRS_LIST_PARADIS
  dd/paradis/domain_paradis.hh
  dd/paradis/paradis_home.hh
  )

set(LIBMULTISCALE_PARADIS_FILES
  ${LIBMULTISCALE_DD_HDRS_LIST_PARADIS}
  dd/paradis/domain_paradis.cc
  dd/paradis/iterator_paradis.hh
  dd/paradis/container_paradis.hh
  dd/paradis/ref_node_paradis.hh
  dd/paradis/ref_node_paradis.cc
  dd/paradis/ref_elem_paradis.hh
  dd/paradis/paradis_home.cc
  )

set(LIBMULTISCALE_DD_MODEL_LIST_PARADIS
  "((DomainParaDiS,3,PARADIS))"
  )

package_declare_sources(paradis_plugin ${LIBMULTISCALE_PARADIS_FILES})
set_module_headers_from_package(dd paradis_plugin ${LIBMULTISCALE_DD_HDRS_LIST_PARADIS})
declare_boost_list_from_package(dd_model paradis_plugin ${LIBMULTISCALE_DD_MODEL_LIST_PARADIS})
