#===============================================================================
# @file   cadd_coupling.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
#
# @date   Thu Sep 18 10:34:22 2014
#
# @brief  CADD coupling package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(CADD
  DESCRIPTION "CADD features"
  DEFAULT OFF
  DEPENDS CGAL
  )

set(LIBMULTISCALE_COUPLER_HDRS_LIST_CADD
  coupling/template_cadd.hh
  coupling/quasi_continuum.hh
  coupling/needleman.hh
  )

set(LIBMULTISCALE_FILTER_HDRS_LIST_CADD
#  filter/filter_point.hh
#  filter/compute_add_ddsegments.hh
  filter/compute_dislodetection.hh
  filter/compute_cadd_template.hh
  )

set(LIBMULTISCALE_CADD_FILES
  ${LIBMULTISCALE_COUPLER_HDRS_LIST_CADD}
  # coupling/point_association.cc
  # coupling/point_association_inline_impl.hh
  # coupling/point_association.hh

  coupling/quasi_continuum.cc
  coupling/template_cadd.cc
  coupling/needleman.cc

  ${LIBMULTISCALE_FILTER_HDRS_LIST_CADD}
  # filter/filter_point.cc
  # filter/compute_add_ddsegments.cc
  filter/compute_dislodetection.cc
  filter/compute_cadd_template.cc
  )


set(LIBMULTISCALE_COUPLER_LIST_CADD 
  "((CADD_template,CADDTEMPLATE,DDTYPE,ATOMTYPE))"
  "((QuasiContinuum, QUASICONTINUUM, ATOMTYPE, CONTINUUMTYPE))"
  "((Needleman,NEEDLEMAN,DDTYPE,CONTINUUMTYPE))"
  )

set(LIBMULTISCALE_FILTER_LIST_CADD
  "((FilterPoint,POINT))"
  "((ComputeDisloDetection,DISLOCDETECTION))"
  "((ComputeCADDTemplate,CADD_TEMPLATE))"  
  )

set(LIBMULTISCALE_COMPUTE_LIST_CADD
  # "((ComputeAddDDSegments,ADD_DDSEGMENTS))"
  )

package_declare_sources(cadd ${LIBMULTISCALE_CADD_FILES})

set_module_headers_from_package(coupler cadd ${LIBMULTISCALE_COUPLER_HDRS_LIST_CADD})
declare_boost_list_from_package(coupler cadd ${LIBMULTISCALE_COUPLER_LIST_CADD})

set_module_headers_from_package(filter cadd ${LIBMULTISCALE_FILTER_HDRS_LIST_CADD})
declare_boost_list_from_package(filter cadd ${LIBMULTISCALE_FILTER_LIST_CADD})
declare_boost_list_from_package(compute cadd ${LIBMULTISCALE_COMPUTE_LIST_CADD})


