#===============================================================================
# @file   stimulations.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
#
# @date   Thu Sep 18 16:13:26 2014
#
# @brief  Stimulations package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(stimulations NOT_OPTIONAL DESCRIPTION "stimulations package for LibMultiScale")

set(LIBMULTISCALE_STIMULATION_HDRS_LIST_STIMULATIONS
  stimulation/stimulation_interface.hh
  stimulation/stimulation_addforce.hh
  stimulation/stimulation_berendsen.hh
  stimulation/stimulation_dislo.hh
  stimulation/stimulation_volterra.hh
  stimulation/stimulation_field.hh
  stimulation/stimulation_global_timestep.hh
  stimulation/stimulation_next_event.hh

  stimulation/stimulation_impulse.hh
  stimulation/stimulation_langevin.hh
  stimulation/stimulation_linearmomentum.hh
  stimulation/stimulation_zero.hh
  stimulation/stimulation_rigidify.hh
  stimulation/stimulation_temperature.hh
  stimulation/stimulation_veluni.hh
  stimulation/stimulation_block.hh
  stimulation/stimulation_enforce_compatibility.hh
  )

set(LIBMULTISCALE_STIMULATIONS_FILES
  ${LIBMULTISCALE_STIMULATION_HDRS_LIST_STIMULATIONS}
  stimulation/stimulation_next_event.cc
  stimulation/accept_ref_stimulation.hh
  stimulation/accept_real_stimulation.hh
  stimulation/accept_stimulation_include.hh
  stimulation/stimulation_interface.cc
  stimulation/stimulation_global_timestep.cc
  stimulation/stimulation_berendsen.cc
  stimulation/stimulation_dislo.cc
  stimulation/stimulation_volterra.cc
  stimulation/stimulation_field.cc
  stimulation/stimulation_impulse.cc
  stimulation/stimulation_langevin.cc
  stimulation/stimulation_linearmomentum.cc
  stimulation/stimulation_zero.cc
  stimulation/stimulation_rigidify.cc
  stimulation/stimulation_temperature.cc
  stimulation/stimulation_veluni.cc
  stimulation/stimulation_addforce.cc
  stimulation/stimulation_block.cc
  stimulation/stimulation_enforce_compatibility.cc 
  )

set(LIBMULTISCALE_STIMULATION_LIST_STIMULATIONS
  "((StimulationBerendsen,BERENDSEN))"
  "((StimulationDislo,DISLO))"
  "((StimulationVolterra,VOLTERRA))"
  "((StimulationImpulse,IMPULSE))"
  "((StimulationZero,ZERO))"
  "((StimulationTemperature,TEMPERATURE))"
  "((StimulationField,FIELD))"
  "((StimulationLangevin,LANGEVIN))"
  "((StimulationLinearMomentum, LINEAR_MOMENTUM))"
  "((StimulationAddForce,ADDFORCE))"
  "((StimulationVelUni,VELUNI))"
  "((StimulationGlobalTimeStep,GLOBAL_TIMESTEP))"
  "((StimulationRigidify,RIGIDIFY))"
  "((StimulationBlock, BLOCK))"
  "((StimulationNextEvent,NEXT_EVENT))"
  "((StimulationEnforceCompatibility,ENFORCE_COMPATIBILITY))"
)

package_declare_sources(stimulations ${LIBMULTISCALE_STIMULATIONS_FILES})
set_module_headers_from_package(stimulation stimulations ${LIBMULTISCALE_STIMULATION_HDRS_LIST_STIMULATIONS})
declare_boost_list_from_package(stimulation stimulations ${LIBMULTISCALE_STIMULATION_LIST_STIMULATIONS})
