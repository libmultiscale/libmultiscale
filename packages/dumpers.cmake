#===============================================================================
# @file   dumpers.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
#
# @date   Thu Sep 18 16:13:26 2014
#
# @brief  Dumper package
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

package_declare(dumpers NOT_OPTIONAL DESCRIPTION "dumpers package for LibMultiScale")

set(LIBMULTISCALE_DUMPER_HDRS_LIST_DUMPERS
  dumper/dumper_interface.hh
  dumper/dumper_1d.hh
  dumper/dumper_ekin.hh
  dumper/dumper_epot.hh
  dumper/dumper_grid.hh
  dumper/dumper_lammps.hh
  dumper/dumper_paraview.hh
  dumper/dumper_restart.hh
  dumper/dumper_text.hh
  )

set(LIBMULTISCALE_DUMPERS_FILES
  ${LIBMULTISCALE_DUMPER_HDRS_LIST_DUMPERS}

  dumper/dumper_interface.cc
  dumper/dumper_paraview.cc
  
  dumper/dumper_1d.cc
  dumper/dumper_ekin.cc
  dumper/dumper_epot.cc
  dumper/dumper_grid.cc
  dumper/dumper_lammps.cc
  dumper/dumper_restart.cc
  dumper/dumper_text.cc

  dumper/paraview_helper.hh
  dumper/base64_reader.hh
  dumper/base64_writer.hh
)

set(LIBMULTISCALE_DUMPER_LIST_DUMPERS
  "((DumperParaview,PARAVIEW))"
  "((Dumper1D,DUMPER1D))"
  "((DumperEKin,EKIN))"
  "((DumperEPot,EPOT))"
  "((DumperGrid,GRID))"
  "((DumperLammps,LAMMPS))"
  "((DumperRestart,RESTART))"
  "((DumperText,TEXT))"
  )

#  ((DumperXYZ,XYZ))
#  ((DumperVGroup,VGROUPE))
#  ((DumperReduce,REDUCE))
#  ((DumperReduce,REDUCE))
#  ((DumperUser,USER))
#  ((DumperGlobalStat,GLOBALSTAT))
#  ((DumperContact,CONTACT))
#  ((DumperNeighbor,NEIGHBOR))
#  ((DumperCountPoints,COUNTPOINTS))
#LIBMULTISCALE_USE_FIG -> DumperFig,DUMPERFIG
#LIBMULTISCALE_USE_EPSN -> DumperEpsn,EPSN

package_declare_sources(dumpers ${LIBMULTISCALE_DUMPERS_FILES})
set_module_headers_from_package(dumper dumpers ${LIBMULTISCALE_DUMPER_HDRS_LIST_DUMPERS})
declare_boost_list_from_package(dumper dumpers ${LIBMULTISCALE_DUMPER_LIST_DUMPERS})
