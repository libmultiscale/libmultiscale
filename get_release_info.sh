#!/bin/bash

uuencode=$1
git diff > $2/release.patch
echo "#include <string>" 
echo "std::string lm_release_info =  \"\\n\\" 
branch=$(git rev-parse --abbrev-ref --verify HEAD)
echo "$branch \\n\\"
git rev-parse --verify $branch | sed 's/$/\\n\\/'
git remote -v | sed 's/$/ \\n\\/'
echo "\\n\\"
$uuencode -m $2/release.patch release.patch | sed 's/$/\\n\\/'
echo "\\"
$uuencode -m $2/CMakeCache.txt CMakeCache.txt | sed 's/$/\\n\\/'
echo "\";"

git rev-parse --verify $branch | sed 's/$/\\n\\/' > $2/python/commit_hash

    
