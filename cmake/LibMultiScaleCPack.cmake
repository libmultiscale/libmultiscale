#===============================================================================
# @file   LibMultiScaleCPack.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
#
# @date   Mon Jul 28 12:20:03 2014
#
# @brief  Configure the packaging system
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

set(PACKAGE_FILE_NAME "libmultiscale" CACHE STRING "Name of package to be generated")
mark_as_advanced(PACKAGE_FILE_NAME)

set(CPACK_GENERATOR "DEB;TGZ;TBZ2;STGZ")

# General configuration
set(CPACK_PACKAGE_VENDOR "LSMS")
set(CPACK_PACKAGE_FILE_NAME "${PACKAGE_FILE_NAME}-${LIBMULTISCALE_VERSION}-${CPACK_DEBIAN_PACKAGE_ARCHITECTURE}")
set(CPACK_PACKAGE_VERSION "${LIBMULTISCALE_VERSION}")

# Debian config package
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "guillaume.anciaux@epfl.ch, nicolas.richart@epfl.ch")
if(CMAKE_SIZEOF_VOID_P EQUAL 8)
  set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "amd64" CACHE STRING "Architecture of debian package generation")
else()
  set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "i386" CACHE STRING "Architecture of debian package generation")
endif()
mark_as_advanced(CPACK_DEBIAN_PACKAGE_ARCHITECTURE)
set(CPACK_DEBIAN_PACKAGE_DESCRIPTION "LibMultiScale library")
set(CPACK_DEBIAN_PACKAGE_DEPENDS "${PACKAGE_SYSTEM_DEBIAN_PACKAGE_DEPENDS}")

set(CPACK_DEB_COMPONENT_INSTALL ON)
set(CPACK_TGZ_COMPONENT_INSTALL ON)
set(CPACK_STGZ_COMPONENT_INSTALL ON)
#set(CPACK_COMPONENTS_GROUPING "ALL_COMPONENTS_IN_ONE")
set(CPACK_COMPONENTS_ALL client lib)
set(CPACK_COMPONENT_CLIENT_DISPLAY_NAME "Clients")
set(CPACK_COMPONENT_LIB_DISPLAY_NAME "Libraries")
set(CPACK_COMPONENT_DEV_DISPLAY_NAME "C++ Headers")
set(CPACK_COMPONENT_DEV_DEPENDS lib)
set(CPACK_COMPONENT_LIB_DESCRIPTION "LibMultiScale clients as AMEL/LMPOST")
set(CPACK_COMPONENT_LIB_DESCRIPTION "LibMultiScale libraries")
set(CPACK_COMPONENT_DEV_DESCRIPTION "LibMultiScale C/C++ header files")
set(CPACK_COMPONENT_CLIENT_GROUP "Applications")
set(CPACK_COMPONENT_LIB_GROUP "Applications")
set(CPACK_COMPONENT_DEV_GROUP "Development")

set(CPACK_SOURCE_PACKAGE_FILE_NAME "${PACKAGE_FILE_NAME}-${LIBMULTISCALE_VERSION}-src")
#set(CPACK_RESOURCE_FILE_LICENSE "${PROJECT_SOURCE_DIR}/COPYING")

list(APPEND CPACK_SOURCE_IGNORE_FILES ${LIBMULTISCALE_EXCLUDE_SOURCE_FILES} ${LIBMULTISCALE_TESTS_EXCLUDE_FILES} ${LIBMULTISCALE_DOC_EXCLUDE_FILES})
foreach(_pkg ${PACKAGE_SYSTEM_PACKAGES_OFF})
  list(APPEND CPACK_SOURCE_IGNORE_FILES ${CMAKE_SOURCE_DIR}/packages/${_pkg}.cmake)
endforeach()
list(APPEND CPACK_SOURCE_IGNORE_FILES "/doc/manual/;/.*build.*/;/CVS/;/\\\\.svn/;/\\\\.bzr/;/\\\\.hg/;/\\\\.git/;\\\\.swp$;\\\\.#;/#;~" )

include(CPack)
