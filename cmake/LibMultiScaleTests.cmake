#===============================================================================
# @file   LibMultiScaleTests.cmake
#
# @author Guillaume Anciaux <guillaume.anciaux@epfl.ch>
# @author Nicolas Richart <nicolas.richart@epfl.ch>
#
# @date   Mon Feb 04 23:50:04 2013
#
# @brief  Test system for LM
#
# @section LICENSE
#
# Copyright (©) 2010-2011 EPFL (Ecole Polytechnique Fédérale de Lausanne)
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# LibMultiScale is free  software: you can redistribute it and/or  modify it under the
# terms  of the  GNU Lesser  General Public  License as  published by  the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# LibMultiScale is  distributed in the  hope that it  will be useful, but  WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A  PARTICULAR PURPOSE. See  the GNU  Lesser General  Public License  for more
# details.
#
# You should  have received  a copy  of the GNU  Lesser General  Public License
# along with LibMultiScale. If not, see <http://www.gnu.org/licenses/>.
#
#===============================================================================

include(CMakeDebugMessages)
cmake_register_debug_message_module(TestSystem)

macro(list_sub_directories list_of_dirs path pattern)
  cmake_debug_message(TestSystem "list_sub_directories: Path ${path} - Pattern ${pattern}")
  file(GLOB sub-dir RELATIVE ${path} ${path}/${pattern})
  cmake_debug_message(TestSystem "list_sub_directories: ${path}/${pattern} - ${sub-dir}")
  set(${list_of_dirs} "")
  foreach (dir ${sub-dir})
    if (IS_DIRECTORY ${path}/${dir})# AND NOT ${dir} MATCHES "[.].*")
      list(APPEND ${list_of_dirs} ${dir})
    endif()
  endforeach()
  cmake_debug_message(TestSystem "list_sub_directories: ${${list_of_dirs}}")
endmacro()

################################################################
function(create_test path)
  cmake_debug_message(TestSystem "create_test: path ${path} - global dependencies: ${ARGN}")
  set(list_of_dirs "")
  list_sub_directories(list_of_dirs ${CMAKE_CURRENT_SOURCE_DIR}/${path} test_*)

  list(APPEND _dependencies ${ARGN})

  set(_default_test_enable TRUE)
  set(_base_dependencies_str "")
  foreach(_dep ${_dependencies})
    set(_base_dependencies_str "${_base_dependencies_str}_${_dep}")
    package_get_name(${_dep} _pkg_name)
    _package_get_option_name(${_pkg_name} _option_name)
    if(NOT ${_option_name})
      set(_default_test_enable FALSE)
      cmake_debug_message(TestSystem "create_test: test dependencies ${_dep}: NOT OK")
    else()
      cmake_debug_message(TestSystem "create_test: test dependencies ${_dep}: OK")
    endif()
  endforeach()

  set(_tests_dir ${CMAKE_CURRENT_SOURCE_DIR}/${path})

  if(_default_test_enable)
    cmake_debug_message(TestSystem "create_test: list of dirs ${list_of_dirs}")
    foreach(dir ${list_of_dirs})
      cmake_debug_message(TestSystem "create_test: registering test ${test_name}")

      set(_test_enable TRUE)
      set(_extra_dependencies "")
      set(_dependencies_str "${_base_dependencies_str}")
      if(EXISTS ${_tests_dir}/${dir}/.depends)
        file(STRINGS ${_tests_dir}/${dir}/.depends _extra_dependencies)
        foreach(_dep ${_extra_dependencies})
	  package_get_name(${_dep} _pkg_name)
	  package_is_activated(${_dep} is_activated)
	  if(NOT ${is_activated})
	   # message(BBBBBBBBB " " ${_pkg_name} " " ${_dep} " " ${dir})
            set(_test_enable FALSE)
            cmake_debug_message(TestSystem "create_test: test extra dependencies ${_dep}: NOT OK")
          else()
	    #message(AAAAAAAAAA " " ${_pkg_name} " " ${_dep} " " ${dir})
            cmake_debug_message(TestSystem "create_test: test extra dependencies ${_dep}: OK")
          endif()
        endforeach()
      endif()
      if(EXISTS ${_tests_dir}/${dir}/.timesteps)
        file(STRINGS ${_tests_dir}/${dir}/.timesteps test_timesteps)
      else()
	set(test_timesteps 10)
      endif()
      
      cmake_debug_message(TestSystem "create_test: dependency string ${_base_dependencies_str} ${_dependencies_str}")

      set(test_name ${dir}${_dependencies_str})
      if(_test_enable)
	SET(${test_name}_timesteps ${test_timesteps} CACHE STRING "number of timestep to run for test ${test_name}")
	mark_as_advanced(${test_name}_timesteps)
        add_test(${test_name} ${CMAKE_BINARY_DIR}/test/run_test.sh ${test_name} ${_tests_dir} ${dir} ${${test_name}_timesteps} )
        cmake_debug_message(TestSystem "create_test: ${test_name} REGISTRED")
        set_tests_properties(${test_name} PROPERTIES DEPENDS AMEL)
        if(LIBMULTISCALE_TESTS_CHECK_REFERENCE)
          add_test(${test_name}_check_reference ${CMAKE_SOURCE_DIR}/test/check_reference.sh output_${test_name}
            ${CMAKE_BINARY_DIR}/test/untar_test_reference/output_${test_name})
          set_tests_properties(${test_name}_check_reference PROPERTIES DEPENDS AMEL ${test_name})
        endif()
      else()
        cmake_debug_message(TestSystem "create_test: ${test_name} NOT REGISTRED")
      endif()
    endforeach()
  endif()
endfunction()

################################################################
function(register_tests path)
  list_sub_directories(list_of_dirs ${path} *)
  cmake_debug_message(TestSystem "register_tests: ${path}")
  cmake_debug_message(TestSystem "register_tests: list_of_dirs: ${list_of_dirs}")
  foreach(dir ${list_of_dirs})
    string(REPLACE "_" ";" plugins ${dir})
    create_test(${dir} ${plugins})
  endforeach()
endfunction()

################################################################
