#!/bin/bash

find . -type f | grep -v untar_test_reference | grep "output" > generated-list-reference.txt

if [ -e reference.tar.gz ]
then
    tar tfz reference.tar.gz > list-reference.txt
    diff ./generated-list-reference.txt ./list-reference.txt | grep "<" | cut -d ' ' -f 2 > list-to-append.txt
    gunzip reference.tar.gz
    tar rvf reference.tar `cat list-to-append.txt`
    gzip reference.tar
else
    tar cfz reference.tar.gz `cat generated-list-reference.txt`
fi