#!/bin/bash

set -x

if [ $# -ne 2 ]
then
    echo "wrong usage $#"
    exit -1
fi

job_dir=$1
reference_job_dir=$2
diff -r $job_dir $reference_job_dir
