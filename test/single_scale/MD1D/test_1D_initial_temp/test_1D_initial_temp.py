# import matplotlib.pyplot as plt
import numpy as np

my_data = np.genfromtxt('dumper_energy.txt', delimiter='\t',
                        names=True, deletechars='')

ekin = my_data["ekin:EKin"][100:]
epot = my_data["epot:Epot"][100:]

etot = (ekin+epot)
print(etot.min(), etot.max(), (etot.max()-etot.min())/etot.max())
print(ekin.min(), ekin.max(), (ekin.max()-ekin.min())/ekin.max())
print(epot.min(), epot.max(), (epot.max()-epot.min())/epot.max())

# plt.plot(epot, label='epot')
# plt.plot(ekin, label='ekin')
# plt.plot(etot, label='etot')
# plt.legend()
# plt.show()

if np.abs((etot.max()-etot.min())/etot.max()) > 1e-2:
    raise RuntimeError('energy is not conserved')


temperatures = my_data["ekin:Temperature"]
temp_avg = temperatures.sum() / temperatures.shape[0]
if np.abs(temp_avg - 50.) > 10.:
    raise RuntimeError(
        'injected temperature is not respected: {0}'.format(temp_avg))
