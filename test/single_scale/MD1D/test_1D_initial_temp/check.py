

def compute(input=None, constants=None, **kwargs):
    component = input
    print(component)
    T = component.evalOutput("ekin:Temperature").array()[0, 0]
    targetT = constants['targetT']
    max_admissible_T = targetT*4
    if T > max_admissible_T or T < 0.:
        raise RuntimeError('invalid temperature {0}'.format(T))
    return component
