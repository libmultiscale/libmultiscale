import numpy as np


def compute(**kwargs):

    print(kwargs)

    kwargs_ref = {
        'units':
        {'e2fd': 1.0,
         'e_dd2m_tt': 1.0,
         'e_m2dd_tt': 1.0,
         'f_m2v_t': 1.0,
         'fd2e': 1.0,
         'ft_m2v': 1.0,
         'kT2e': 1.0,
         'kT2fd': 1.0,
         'm_tt2f_d': 1.0,
         'mv_t2f': 1.0,
         'mvv2e': 1.0},
        'constants': {
            'INF': 1.7976931348623157e+308,
            'avogadro': 6.02214179e+23,
            'boltzmann': 1.3806503e-23,
            'pi': 3.141592653589793},
        'step': 0,
        'current_stage': 'PRE_STEP1',
        'compute_name': 'pos',
        'rank': 0}

    assert kwargs_ref == kwargs

    return np.linspace(-10., 10., 1000)


if __name__ == "__main__":
    print(compute())
