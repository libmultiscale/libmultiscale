import numpy as np


def compute(ekin=None, epot=None, **kwargs):
    return np.array(ekin["EKin"] + epot["Epot"])
