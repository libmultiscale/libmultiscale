mpirun -np 1 ../../../clients/AMEL test_paraview.config 1

if [ $? != 0 ]
then
    echo "Execution of libmultiscale failed"
    exit -1
fi


python3 test_paraview.py
