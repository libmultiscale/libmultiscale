#!/usr/bin/env python

import xmltodict

filename = './para-VTUs/para_pvf0000.proc0000.vtu'
dico = xmltodict.parse(open(filename).read())
vtkfile = dico['VTKFile']
UnstructuredGrid = vtkfile['UnstructuredGrid']
Piece = UnstructuredGrid['Piece']
nb_points = Piece['@NumberOfPoints']
if int(nb_points) != 4999:
    raise RuntimeError('nb_points')

nb_cells = Piece['@NumberOfCells']
points = Piece['Points']['DataArray']

point_data = Piece['PointData']['DataArray']
if not isinstance(point_data, list):
    raise RuntimeError('not enough outputed fields')

if (len(point_data) != 3):
    raise RuntimeError('not enough outputed fields')

for data in point_data:
    if int(data['@NumberOfComponents']) != 1:
        raise RuntimeError('wrong number of components for point field')
