import csv 

stress_lammps_xx = []
stress_lammps_yy = []
stress_lammps_xy = []
epot_lammps = []
open_lammps = "dump_test/dump.70"
f_lammps = open(open_lammps, "r")
A_lammps = []
A_lammps = [ line.split() for line in f_lammps]
del A_lammps[0:9]

for i in range(len(A_lammps)):
    stress_lammps_xx.append(float(A_lammps[i][11]))
    stress_lammps_yy.append(float(A_lammps[i][12]))
    stress_lammps_xy.append(float(A_lammps[i][14]))
    epot_lammps.append(float(A_lammps[i][17]))
    
stress_libmultiscale_xx = []
stress_libmultiscale_yy = []
stress_libmultiscale_xy = []
epot_libmultiscale = []
open_libmultiscale = 'paramd_0007.csv'

err_stress = 0
err_epot = 0

with open(open_libmultiscale, 'r') as file:
    reader = csv.reader(file)
    for line in reader:
        if line[0] == 'displacement:0':
            for i in range(len(line)):
                if line[i] == 'stress:0' or line[i] == 'stress:1' or line[i] == 'stress:3':
                    err_stress += 1
                if line[i] == 'epot':
                    err_epot += 1
            if err_stress != 3:
                raise RuntimeError("LM: stress not computed")    
            if err_epot == 0:
                raise RuntimeError("LM: epot not computed")    

        if line[0] != 'displacement:0':
            stress_libmultiscale_xx.append(float(line[2]))
            stress_libmultiscale_yy.append(float(line[3]))
            stress_libmultiscale_xy.append(float(line[5]))
            epot_libmultiscale.append(float(line[8]))

for i in range(len(stress_lammps_xx)):
    if stress_lammps_xx[i] != stress_libmultiscale_xx[i]:
        raise RuntimeError("LM: problem stress xx")  
    if stress_lammps_yy[i] != stress_libmultiscale_yy[i]:
        raise RuntimeError("LM: problem stress yy")  
    if stress_lammps_xy[i] != stress_libmultiscale_xy[i]:
        raise RuntimeError("LM: problem stress xy")  
    if epot_lammps[i] != epot_libmultiscale[i]:
        raise RuntimeError("LM: problem epot")  
