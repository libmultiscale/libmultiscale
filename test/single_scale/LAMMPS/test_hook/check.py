import numpy as np


def compute(stress=None, epot=None, **kwargs):

    step = kwargs["step"]
    stress = stress["stress"]
    epot_libmultiscale = epot['pe/atom']

    stress_libmultiscale_xx = []
    stress_libmultiscale_yy = []
    stress_libmultiscale_xy = []

    for i in range(len(stress)):
        stress_libmultiscale_xx.append(stress[i][0])
        stress_libmultiscale_yy.append(stress[i][1])
        stress_libmultiscale_xy.append(stress[i][3])

    stress_lammps_xx = []
    stress_lammps_yy = []
    stress_lammps_xy = []
    epot_lammps = []

    open_lammps = "dump_test/dump." + str(step)

    f_lammps = open(open_lammps, "r")
    A_lammps = []
    A_lammps = [line.split() for line in f_lammps]
    del A_lammps[0:9]

    for i in range(len(A_lammps)):
        stress_lammps_xx.append(float(A_lammps[i][11]))
        stress_lammps_yy.append(float(A_lammps[i][12]))
        stress_lammps_xy.append(float(A_lammps[i][14]))
        epot_lammps.append(float(A_lammps[i][17]))

    stress_lammps_xx = np.array(stress_lammps_xx)
    stress_lammps_yy = np.array(stress_lammps_yy)
    stress_lammps_xy = np.array(stress_lammps_xy)
    epot_lammps = np.array(epot_lammps)
    natom = epot_lammps.shape[0]

    err_xx = np.linalg.norm(stress_lammps_xx - stress_libmultiscale_xx)
    err_yy = np.linalg.norm(stress_lammps_yy - stress_libmultiscale_yy)
    err_xy = np.linalg.norm(stress_lammps_xy - stress_libmultiscale_xy)
    err_epot = np.linalg.norm(epot_lammps - epot_libmultiscale)

    err_xx = err_xx/natom
    err_yy = err_yy/natom
    err_xy = err_xy/natom
    err_epot = err_epot/natom

    if err_xx > 1e-10:
        print('diff', stress_lammps_xx-stress_libmultiscale_xx)
        raise RuntimeError(
            "Test failed in stress_xx: error is now " + str(err_xx))

    if err_yy > 1e-10:
        print('diff', stress_lammps_yy-stress_libmultiscale_yy)
        raise RuntimeError(
            "Test failed in stress_yy: error is now " + str(err_yy))

    if err_xy > 1e-10:
        print('diff', stress_lammps_xy-stress_libmultiscale_xy)
        raise RuntimeError(
            "Test failed in stress_xy: error is now " + str(err_xy))

    if err_epot > 1e-5:
        print('diff', epot_lammps-epot_libmultiscale)
        raise RuntimeError(
            "Test failed in epot: error is now " + str(err_epot))

    return stress
