from __future__ import print_function

import numpy as np


def compute(p0=None, input=None, **kwargs):
    p0 = input
    vec = 1e-4*p0**2
    return vec


if __name__ == "__main__":
    a = np.array([[2, 2], [3, 3]])
    print(compute(a, 1))
