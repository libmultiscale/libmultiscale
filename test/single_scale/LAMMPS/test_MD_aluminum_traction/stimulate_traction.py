#!/usr/bin/env python
import numpy as np


def compute(input=None, constants=None, **kwargs):
    pos0 = input
    disp = np.zeros(pos0.shape)
    disp[:, 1] = pos0[:, 1]*constants["strain"]
    print(
        "additional displacement for {0}: sheared by tau = {1}".format(
            'disp_md', constants["strain"]))
    print(
        "  {0:e} <= disp <= {1:e} and {2:e} <= position <= {3:e} ".format(
            disp[:, 0].min(), disp[:, 0].max(),
            pos0[:, 1].min(), pos0[:, 1].max()))
    return disp
