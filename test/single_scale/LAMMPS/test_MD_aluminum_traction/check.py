import numpy as np


def compute(input, **kwargs):
    T = input.evalOutput("Temperature").array()[0, 0]
    if T > 1e-6:
        raise RuntimeError("temperature too high: no equilibrium: T = " +
                           str(T))
    if T < 0:
        raise RuntimeError("negative temperature ????")
    return input
