import numpy as np

def compute(**kwargs):
    
#    print kwargs.keys()
    lX = kwargs['wavelengthX']
    lY = kwargs['wavelengthY']
    r0 = kwargs['r0']
    r1 = kwargs['r1']
    print lX,lY
    

    res = np.sin(kwargs['pos'][:,0]/lX/r0*2.*np.pi)*np.sin(kwargs['pos'][:,1]/lY/r1*2.*np.pi)
    res2 = np.zeros((res.shape[0],2))
    res2[:,0] = res*1e-4
    return res2


