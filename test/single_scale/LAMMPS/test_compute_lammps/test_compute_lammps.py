file_ekin_lammps = open("ekin_lammps.txt", "r")
ekin_lammps = [(line.strip()).split() for line in file_ekin_lammps]
del ekin_lammps[0]

file_ekin_libmultiscale = open("ekin_libmultiscale.txt", "r")
ekin_libmultiscale = [(line.strip()).split() for line in file_ekin_libmultiscale]
del ekin_libmultiscale[0]

for i in range(len(ekin_lammps)):
    err = abs( float(ekin_lammps[i][2]) - float(ekin_libmultiscale[i][2]) )
    if err != 0:
        raise RuntimeError("compute lammps failed")
