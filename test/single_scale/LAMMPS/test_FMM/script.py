
import numpy as np

def compute(a):
    vec = np.zeros(a.shape)
    vec[:,0] = 1.0e-5
    return vec
    

if __name__ == "__main__":
    a = np.array([[2,2], [3,3]])
    print compute(a)
