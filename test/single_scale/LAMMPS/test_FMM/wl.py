
import numpy as np

def compute(a,ts):
    vec = np.copy(kwargs["amplitudeMax"])
    ts = kwargs["step"]
#    r0 = 4.032*np.sqrt(2)/2
    r0 = 1
    r1 = r0*np.sqrt(3)
    wlx = 160/(2.*(ts+1))*r0
    wly = 90/(2.*(ts+1))*r1
    wl_sq = 1./np.sqrt((1./wlx)**2+(1./wly)**2)
    vec.resize(vec.shape[0],vec.shape[1]+1)
    vec[:,vec.shape[1]-1] = wl_sq*np.ones(vec.shape[0])
    return vec
    

if __name__ == "__main__":
    a = np.array([[2,2], [3,3]])
    print compute(a,1)
