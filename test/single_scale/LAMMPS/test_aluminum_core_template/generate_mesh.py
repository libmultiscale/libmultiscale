import numpy as np


def generate_nodes():
    npoints = 100
    pos = np.zeros((npoints, 3))
    pos[:, 0] = np.linspace(-10., 10., npoints)
    return pos


def generate_connectivity(nodes):
    n_nodes = nodes.shape[0]
    conn = np.zeros((n_nodes, 2))
    conn[:, 0] = np.arange(0, n_nodes)
    conn[:-1, 1] = np.arange(1, n_nodes)
    conn[-1, 1] = 0
    return conn


def compute(**kwargs):
    nodes = generate_nodes()
    conn = generate_connectivity(nodes)
    return {'nodes': nodes, 'connectivity': conn}
