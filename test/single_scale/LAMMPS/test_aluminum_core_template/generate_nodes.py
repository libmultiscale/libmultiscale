import numpy as np


def compute(**kwargs):
    npoints = 100

    pos = np.zeros((npoints, 3))
    pos[:, 0] = np.linspace(-10., 10., npoints)
    return pos
