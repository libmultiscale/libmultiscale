export NPROC=1
mpirun -np $NPROC ../../../clients/AMEL test_Langevin_thermostat.pconfig 100
python3 test_Langevin_thermostat.py

export NPROC=4
mpirun -np $NPROC ../../../clients/AMEL test_Langevin_thermostat.pconfig 100
python3 test_Langevin_thermostat.py

export NPROC=5
mpirun -np $NPROC ../../../clients/AMEL test_Langevin_thermostat.pconfig 100
python3 test_Langevin_thermostat.py

export NPROC=8
mpirun -np $NPROC ../../../clients/AMEL test_Langevin_thermostat.pconfig 100
python3 test_Langevin_thermostat.py

