import numpy as np

my_data = np.genfromtxt('dumper_ekin.txt', delimiter='\t',
                        names=True, deletechars='')

temperatures = my_data["ekin:Temperature"]
sz = temperatures.shape[0]
temperatures = temperatures[sz-15:]
print(temperatures)
temp_avg = temperatures.sum() / temperatures.shape[0]
if np.abs(temp_avg - 50.) > 10.:
    raise RuntimeError(
        'injected temperature is not respected: {0}'.format(temp_avg))
