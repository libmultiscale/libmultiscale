import numpy as np
import scipy.optimize as opt


values = []


def compute(step=None, position0=None, disp=None, **kwargs):
    disp = disp["displacement"]
    position0 = position0["position0"]
    index = np.argmax(disp[:, 0])
    position0 = position0[index, 0]
    pmax = np.abs(np.array([position0]))
    global values
    values.append(pmax)

    if step == 5000:

        values = np.array(values)
        values = values.reshape(values.shape[0])
        steps = np.arange(values.shape[0], dtype=float)*100.

        def f(x, a, b):
            return a*x+b
        popt, pcov = opt.curve_fit(f, steps, values)
        # print(popt)
        # import matplotlib.pyplot as plt
        # plt.plot(steps, values)
        # plt.plot(steps, f(steps, *p))
        # plt.show()
        error = np.abs(popt[0] - 0.0364484912035075)
        if error > 1e-3:
            raise RuntimeError(
                "push wave speed is not the one expected: error = " +
                str(error))

    return pmax
