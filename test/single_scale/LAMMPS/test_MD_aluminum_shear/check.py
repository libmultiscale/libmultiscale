

def compute(input=None, **kwargs):
    T = input.evalOutput("Temperature").array()[0, 0]
    if T > 5e-5:
        raise RuntimeError("temperature too high: no equilibrium: T = " +
                           str(T))
    if T < 0:
        raise RuntimeError("negative temperature ????")
    return input
