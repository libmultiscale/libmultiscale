#!/usr/bin/env sh

export NPROC=1
mkdir -p output

mpirun -np $NPROC ../../../clients/AMEL $1 2000


if [ $? != 0 ]
then
    echo "Libmultiscale failed"
    exit -1
else
    echo "Libmultiscale successful"
fi
