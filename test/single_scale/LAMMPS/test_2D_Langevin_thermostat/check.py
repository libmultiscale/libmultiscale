
def temperature(input=None, **kwargs):
    ekin = kwargs['ekin']
    T = ekin['Temperature'][0, 0]
    max_admissible_T = 300
    if T > max_admissible_T or T < 0.:
        raise RuntimeError('invalid temperature {0}'.format(T))

    ekin = input
    T = ekin.evalOutput('Temperature').array()[0, 0]
    max_admissible_T = 300
    if T > max_admissible_T or T < 0.:
        raise RuntimeError('invalid temperature {0}'.format(T))
    return input
