h = 33.0;
a = 100.0;

Point(1) = {0,  0, 0, h};
Point(2) = {0, -a, 0, h};
Point(3) = {a, -a, 0, h};
Point(4) = {a,  0, 0, h};

Point(5) = {0,  0, a, h};
Point(6) = {0, -a, a, h};
Point(7) = {a, -a, a, h};
Point(8) = {a,  0, a, h};

Line(1) = {1, 5};
Line(2) = {5, 8};
Line(3) = {8, 4};
Line(4) = {4, 1};
Line(5) = {6, 2};
Line(6) = {2, 3};
Line(7) = {3, 7};
Line(8) = {7, 6};
Line(9) = {5, 6};
Line(10) = {1, 2};
Line(11) = {4, 3};
Line(12) = {8, 7};

Line Loop(26) = {4, 1, 2, 3};
Plane Surface(26) = {26};
Line Loop(28) = {9, -8, -12, -2};
Plane Surface(28) = {28};
Line Loop(30) = {-7, -11, -3, 12};
Plane Surface(30) = {30};
Line Loop(32) = {-10, -4, 11, -6};
Plane Surface(32) = {32};
Line Loop(34) = {10, -5, -9, -1};
Plane Surface(34) = {34};
Line Loop(36) = {8, 5, 6, 7};
Plane Surface(36) = {36};

Surface Loop(50) = {26, 28, 30, 32, 34, 36};
Volume(50) = {50};

Physical Surface ("shear") = {26, 36};
Physical Volume ("vol") = {50};

Transfinite Surface "*";
Transfinite Volume "*";

