#!/usr/bin/env python3
import numpy as np
import sys


theoretical_strain = float(sys.argv[1])
with open("strain_dump.txt") as fh:
    for line in fh:
        pass

grad_u = np.matrix([float(val) for val in line.split()[2:]]).reshape(3, 3)
print(grad_u, grad_u.shape)
epsilon = 0.5*(grad_u + grad_u.T)
vol_change = 1/3*np.diag(epsilon).sum()

error_code = 0
if abs(vol_change) > 1e-10:
    print("The linearised change in volume should be zero but is {0}".format(
        vol_change))
    error_code -= 1
else:
    print("the change in volume is correct")
if (abs(epsilon[0, 1]/theoretical_strain - 1) > 1e-3):
    print("the x-y shear strain should be pretty exactly "
          "{0} but it is {1}".format(
              theoretical_strain, epsilon[0, 1]))
    print("This corresponds to an error of {0}".format(
        abs(epsilon[0, 1]/theoretical_strain - 1)))
    error_code -= 2
else:
    print("the mean x-y strain is correct")
if abs(epsilon[0, 2]) > 1e-10:
    print("the x-z shear strain should be zero but is {0}".format(
        epsilon[0, 2]))
    error_code -= 4
else:
    print("the mean x-z strain is correct")
if abs(epsilon[1, 2]) > 1e-10:
    print("the y-z shear strain should be zero but is {0}".format(
        epsilon[1, 2]))
    error_code -= 4
else:
    print("the mean y-z strain is correct")

sys.exit(error_code)
