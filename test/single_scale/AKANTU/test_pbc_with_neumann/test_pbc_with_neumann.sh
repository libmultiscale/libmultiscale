#!/usr/bin/env sh

export ELSTRAINO=1.92177e-3

# generate the mesh

gmsh cube_3d.geo -3

if [ $? != 0 ]
then
    echo "Generation of mesh failed"
    exit -1
else
    echo "Generation of mesh successful"
fi


#run the sim
../../../clients/AMEL $1 4000

if [ $? != 0 ]
then
    echo "Libmultiscale failed"
    exit -1
else
    echo "Libmultiscale successful"
fi

#run the test
./check_result.py $ELSTRAINO

if [ $? != 0 ]
then
    echo "Results wrong"
    exit -1
else
    echo "Check of results successful"
fi


exit 0
