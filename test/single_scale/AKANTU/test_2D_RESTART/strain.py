import numpy as np


def compute(input=None, constants=None, **kwargs):

    pos0 = input
    disp = np.zeros(pos0.shape)
    # step = kwargs["step"]

    dmax = 0.02
    # FEMLX = 0.20
    FEMLY = 0.20

    for i in range(len(pos0)):
        disp[:, 0] = dmax/FEMLY * pos0[:, 1]

    return disp
