#!/usr/bin/env sh

# strain computed as follows:
# in doubt check that this value still corresponds to the value in
# anisotropic_mendelev_aluminum.dat
# the C66 component of the stiffness tensor is 
export C66=0.169372865037
export STRESS=624.15096e-6

# generate the mesh

gmsh cube_3d.geo -3

if [ $? != 0 ]
then
    echo "Generation of mesh failed"
    exit -1
else
    echo "Generation of mesh successful"
fi


#run the sim
../../../clients/AMEL $1 4000

if [ $? != 0 ]
then
    echo "Libmultiscale failed"
    exit -1
else
    echo "Libmultiscale successful"
fi

#run the test
./check_result.py $STRESS $C66

if [ $? != 0 ]
then
    echo "Results wrong"
    exit -1
else
    echo "Check of results successful"
fi


exit 0
