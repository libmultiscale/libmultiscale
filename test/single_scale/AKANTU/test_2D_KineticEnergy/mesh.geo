h = 2;

sqrt3 = 1.73205080756887729353;
sqrt2 = 1.4142135623730950488;
r0 = 4.0320*sqrt2/2;
r1 = r0*sqrt3;

L0 = r0*10;
L1 = r1*10;

Point(1) = {-L0/2, -L1/2, 0.0, h};
Point(2) = {L0/2, -L1/2, 0.0, h};
Point(3) = {-L0/2, L1/2, 0.0, h};
Point(4) = {L0/2, L1/2, 0.0, h};


Line(27) = {2, 4};
Line(28) = {4, 3};
Line(29) = {3, 1};
Line(30) = {1, 2};
Line Loop(31) = {27, 28, 29, 30};
Plane Surface(32) = {31};

Transfinite Surface "*";

