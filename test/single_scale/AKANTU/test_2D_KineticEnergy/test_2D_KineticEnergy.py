import numpy as np
import sys

# kinetic energy computation analytical equation
# "KE = 0.5*rho*area*vel^2"
rho = 3.8328782866680342684
r0 = 4.0320*np.sqrt(2)/2
r1 = r0*np.sqrt(3)
Lx = 10*r0
Ly = 10*r1
print(Lx, Ly)
area = Lx*Ly
velx = 2.0
vely = 2.0
mvv2e = 2390.0574
keAnalytic = 0.5*rho*area*(velx*velx+vely*vely)*mvv2e


my_data = np.genfromtxt('libMS-energy.txt', delimiter='\t',
                        names=True, deletechars='')

keElem = my_data["tKE_Elem:EKin"]
keNode = my_data["tKE_Node:EKin"]


print("1) Actual kinetic energy = ", keAnalytic)
print("2) Kinetic energy based on elements = ", np.average(keElem))
print("3) Kinetic energy based on nodes = ", np.average(keNode))
print("4) Normalized Energy difference (1-2) = ",
      np.average(keAnalytic - keElem)/keAnalytic)
print("5) Normalized Energy difference (1-3) = ",
      np.average(keAnalytic - keNode)/keAnalytic)

error = np.abs(keElem - keAnalytic)/keAnalytic
if (error >= 1e-5).any():
    raise RuntimeError("bad kinetic computation from elems")

error = np.abs(keNode - keAnalytic)/keAnalytic
if (error >= 1e-5).any():
    raise RuntimeError("bad kinetic computation from nodes")
