#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import sys

if len(sys.argv) > 1:
    theoretical_strain = float(sys.argv[1])
    have_strain = True
    print(theoretical_strain)
else:
    have_strain = False
strains = list()
time = list()
with open("strain_dump.txt") as fh:
    for line in fh:
        if line.strip().startswith("#"):
            continue
        tokens = line.split()
        time.append(int(tokens[0]))
        strains.append([float(strain) for strain in tokens[2:]])

strains = np.array(strains)
dirs = ('x', 'y', 'z')
fig = plt.figure()
ax = fig.add_subplot(111)
for i in range(3):
    for j in range(3):
        if have_strain:
            if i == 1 and j == 0:
                ax.plot(time, abs(.5*strains[:, 3*i+j]/theoretical_strain-1),
                        label=r"$\epsilon_{{"+dirs[i]+dirs[j]+"}}$")
                ax.set_yscale("log")
        else:
            if i in [0, 1] and i != j and j in [0, 1]:
                ax.plot(time, strains[:, 3*i+j],
                        label=r"$\epsilon_{{"+dirs[i]+dirs[j]+"}}$")
ax.legend()
ax.grid()
plt.show()
