
import numpy as np

def compute():
    positions = kwargs["positions0"]
    min_val = positions.min()
    max_val = positions.max()
    vec = (positions - min_val)/(max_val-min_val)*100
    return vec


if __name__ == "__main__":
    a = np.array([[2.,3.,9.]])
    print compute(a,1)
