mpirun -np 1 ../../../clients/AMEL $1 $2

#kinetic energy computation analytical equation
#"KE = 0.5*rho*Lx*vel^2"
r0=1.23157195294235
rho=$(echo " 39.95/$r0" | bc -l)
Lx=$(echo "2000*$r0" | bc -l)
velx=2.0
unit_conversion=2390.06
keAnalytic=$(echo "0.5*$rho*$Lx*($velx*$velx)" | bc -l)
keElem=$(awk -v unit=$unit_conversion 'FNR == 3 {printf "%.4f", ($4/unit)}' energy.txt)
keNode=$(awk -v unit=$unit_conversion 'FNR == 3 {printf "%.4f", ($6/unit)}' energy.txt)

diffe1=$(awk -v a=$keAnalytic -v b=$keElem 'BEGIN{printf "%.4f", (a-b)}')
diffe2=$(awk -v a=$keAnalytic -v b=$keNode 'BEGIN{printf "%.4f", (a-b)}')
echo "1) Actual kinetic energy = " $keAnalytic
echo "2) Kinetic energy based on elements = " $keElem
echo "3) Kinetic energy based on nodes = " $keNode
echo "4) Energy difference (1-2) = " $diffe1
echo "5) Energy difference (1-3) = " $diffe2

reference=0.2460
error=$(awk -v n1=$diffe1 -v ref=$reference  'BEGIN{if(n1-ref>=0) printf "%.4f", (n1-ref); else printf "%.4f", -(n1-ref);}')
flag=$(awk -v err=$error 'BEGIN{if(err <= 1e-3) print 0; else print 1;}')
if [ $flag == "0" ]; then 
    exit 0;
else
    exit 1;
fi