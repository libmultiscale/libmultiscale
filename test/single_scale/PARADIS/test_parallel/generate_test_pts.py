import numpy as np

def create_grid(pts,minCoords,maxCoords,endpts):
    delta = (maxCoords-minCoords)/(pts-endpts)
    pos = np.empty((pts[0]*pts[1]*pts[2],3))
    p = 0
    for i in range(pts[0]):
        for j in range(pts[1]):
            for k in range(pts[2]):
                pos[p,:] = minCoords + delta*[i,j,k]
                p = p+1
    return pos

def compute(**kwargs):
    if kwargs["rank"] == 0:
        pts = np.array([2, 6, 6])
        minCoords = np.array([-1.75e4, -1.75e4, -1.75e4])
        maxCoords = np.array([ 0.00e4,  1.75e4,  1.75e4])
        endpts = np.array([0,1,1])
    elif kwargs["rank"] == 1:
        pts = np.array([3, 6, 6])
        minCoords = np.array([ 0.00e4, -1.75e4, -1.75e4])
        maxCoords = np.array([ 1.75e4,  1.75e4,  1.75e4])
        endpts = np.array([1,1,1])
    else: return -1
    return create_grid(pts,minCoords,maxCoords,endpts)

if __name__ == "__main__":
    a = np.array([[0]])
    print compute(a,1)
    a = np.array([[1]])
    print compute(a,1)
