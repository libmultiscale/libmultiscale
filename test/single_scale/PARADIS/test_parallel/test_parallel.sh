#!/usr/bin/env bash

mkdir -p dumps
rm dumps/pv_dd-VTUs/pv_dd_pvf0001.proc0000.vtu
rm dumps/disp-VTUs/disp_pvf0001.proc0000.vtu
touch dumps/pv_dd-VTUs/pv_dd_pvf0001.proc0000.vtu
touch dumps/disp-VTUs/disp_pvf0001.proc0000.vtu
echo "nothing happened" >> dumps/pv_dd-VTUs/pv_dd_pvf0001.proc0000.vtu
echo "nothing happened" >> dumps/disp-VTUs/disp_pvf0001.proc0000.vtu

EXECUTABLE=../../../clients/AMEL

mpirun -np 2 $EXECUTABLE test_parallel.conf 10

if [ $? != 0 ]
then
    echo "Execution of libmultiscale failed"
    exit -1
fi

REFERENCE_CHECKSUM0="5ab688958de2350a305a41a7bc92add2  dumps/pv_dd-VTUs/pv_dd_pvf0001.proc0000.vtu
8e8c08c7a25c9370f3ae220034021676  dumps/pv_dd-VTUs/pv_dd_pvf0001.proc0001.vtu"
REFERENCE_CHECKSUM1="1af385e771e30f8dd5779d1d4d40207e  dumps/disp-VTUs/disp_pvf0001.proc0000.vtu
f246275a4fd07725107e7565cd098d61  dumps/disp-VTUs/disp_pvf0001.proc0001.vtu"

if [ "$(md5sum dumps/pv_dd-VTUs/pv_dd_pvf0001.proc000*.vtu)" != "$REFERENCE_CHECKSUM0" ];
then
    echo "FAILED! File0 has different md5sums:"
    echo "checksum  = $(md5sum dumps/pv_dd-VTUs/pv_dd_pvf0001.proc000*.vtu)"
    echo "reference = $REFERENCE_CHECKSUM0"
    exit -1
fi

if [ "$(md5sum dumps/disp-VTUs/disp_pvf0001.proc000*.vtu)" != "$REFERENCE_CHECKSUM1" ];
then
    echo "FAILED! File1 has different md5sums:"
    echo "checksum  = $(md5sum dumps/disp-VTUs/disp_pvf0001.proc000*.vtu)"
    echo "reference = $REFERENCE_CHECKSUM1"
    exit -1
fi
