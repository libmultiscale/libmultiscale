#!/usr/bin/python

from ParaDisTools.generate_paradis_config import generateDataFileContent
from ParaDisTools.generate_paradis_config import generateCtrlFileContent
from ParaDisTools.generate_paradis_config import generateRIJMFile
from ParaDisTools.generate_franck_read_loop import generateFRLoopNodes
import numpy as np
import os
import matplotlib.pyplot as plt

################################################################
radius = 100.
center = radius*np.array((-.5, -.5))
# center = radius*np.array((0, 0))
p1 = radius*np.array((0., 1.)) + center
# p1 = radius*np.array((0.05, 1.)) + center
# p2 = radius*np.array((0.05, 1.)) + center
a = 4.04527
r0 = a*np.sqrt(2)/2.
burgers = np.array((1., 0, 0))*r0
normal = (0., 0., 1.)

# nodes = generateFRLoopNodes(center, burgers, normal, p1, p1)
nodes = generateFRLoopNodes(center, burgers, normal, p1, p1, npoints=100)

for (i, j), node in nodes.items():
    node.constraint = 0

positions = []
for (i, j), node in nodes.items():
    print(node.pos)
    print(node.constraint)
    positions.append(node.pos)

positions = np.array(positions)
plt.scatter(positions[:, 0], positions[:, 1])
plt.show()
################################################################
conf_data = dict()
conf_data["file_version"] = 4
conf_data["num_file_segments"] = 1
conf_data["xmin"] = (-1e+05,   -1e+05,   -1e+05)
conf_data["xmax"] = (1e+05,    1e+05,    1e+05)

conf_data["node_count"] = 3
conf_data["data_decomp_type"] = 1
conf_data["data_decomp_geometry"] = (1, 1, 1)

conf_ctrl = dict()
conf_ctrl['num_doms'] = (1, 1, 1)
conf_ctrl['num_cells'] = (1, 1, 1)
conf_ctrl['min_seg'] = 4
conf_ctrl['max_seg'] = 400
conf_ctrl['time_integrator'] = "trapezoidtimestep"
conf_ctrl['burgers'] = r0 * 1e-10
conf_ctrl['temperature'] = 20
conf_ctrl['mobility_law'] = "FCC_0"
conf_ctrl['rijm_file'] = "Rijm.cube.out"
conf_ctrl['rijm_pbc_file'] = "RijmPBC.cube.out"

################
# DOABLE

conf_ctrl['shear_modulus'] = 2.6e+10
conf_ctrl['poisson_ratio'] = 3.000000e-01

################
# adhoc

conf_ctrl['e_core'] = 5.502262e+10
#conf_ctrl['e_core'] = 0.
conf_ctrl['core_radius'] = 2.041241e+01
conf_ctrl['screw_mobility'] = 1.000000e+01
conf_ctrl['edge_mobility'] = 1.000000e+01
conf_ctrl['climb_mobility'] = 1.000000e-04


################################################################
sizes = (np.array(conf_data["xmax"])-np.array(conf_data["xmin"]))
sizes = list(sizes)

print(sizes)

filename = "test"

base_filename, ext = os.path.splitext(filename)
print(base_filename)

ctrl_filename = base_filename + '.ctrl'
data_filename = base_filename + '.data'

ctrl = open(ctrl_filename, 'w')
data = open(data_filename, 'w')

data.write(generateDataFileContent(conf_data, nodes))
ctrl.write(generateCtrlFileContent(conf_ctrl))

generateRIJMFile(
    os.path.expanduser(os.path.join(
        "~/repositories/libmultiscale/build/third-party/paradis/",
        "stresstablegen")),
    sizes, conf_ctrl)
