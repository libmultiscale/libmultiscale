import numpy as np


def compute(dd_pos=None, **kwargs):
    dd_pos = dd_pos['position']
    if dd_pos.shape[0] == 0:
        return np.array([[0, 0]])
    if np.sqrt((dd_pos[:, 2]**2).sum()) > 1e-10:
        raise RuntimeError('loop going out of plane')
    _max = dd_pos[:, :2].max(axis=0)
    _min = dd_pos[:, :2].min(axis=0)
    radius = (_max-_min)/2.
    # print(radius)
    return np.array([radius])
