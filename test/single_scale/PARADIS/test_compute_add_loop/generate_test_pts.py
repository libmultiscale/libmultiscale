
import numpy as np


def compute(**kwargs):
    pts = np.array([5, 5, 5])
    sz = 1.75e4
    minCoords = sz*np.array([-1., -1., -1.])
    maxCoords = sz*np.array([1.,  1.,  1.])

    delta = (maxCoords-minCoords)/(pts-1)
    pos = np.zeros((pts[0]*pts[1]*pts[2], 3))
    p = 0
    for i in range(pts[0]):
        for j in range(pts[1]):
            for k in range(pts[2]):
                pos[p, :] = minCoords + delta*[i, j, k]
                pos[p, 2] += .5
                p += 1
    # print(pos)
    return pos
