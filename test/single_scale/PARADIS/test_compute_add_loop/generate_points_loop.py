import numpy as np


def points_loop(**kwargs):
    radius = 100.
    npoints = 100

    delta = 2.*np.pi/npoints
    pos = np.zeros((npoints, 3))
    for i in range(npoints):
        pos[i, 0] = radius*np.cos(i*delta)
        pos[i, 1] = radius*np.sin(i*delta)

    print(pos)
    return pos
