import numpy as np


def connectivity(pos=None, **kwargs):
    nodes = pos['points_loop']
    n_nodes = nodes.shape[0]
    conn = np.zeros((n_nodes, 2))
    conn[:, 0] = np.arange(0, n_nodes)
    conn[:-1, 1] = np.arange(1, n_nodes)
    conn[-1, 1] = 0
    return conn
