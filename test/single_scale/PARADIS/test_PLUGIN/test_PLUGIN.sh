#!/usr/bin/env bash

../../../clients/AMEL test_PLUGIN.conf 10

if [ $? != 0 ]
then
    echo "Execution of libmultiscale failed"
    exit -1
fi

python3 test_paraview.py
