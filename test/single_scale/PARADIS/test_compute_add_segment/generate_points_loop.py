import numpy as np


def compute(**kwargs):
    pts = 5
    min_pos = -200
    max_pos = 200

    delta = (max_pos-min_pos)/(pts-1)
    print(delta)
    pos = np.empty((pts, 3))
    for i in range(pts):
        pos[i, 0] = min_pos + delta*i
        pos[i, 1] = 0.
        pos[i, 2] = 0.

    return pos
