import numpy as np


def compute(**kwargs):
    pts = 5

    con = np.empty((pts-1, 2))
    for i in range(pts-1):
        con[i, 0] = i
        con[i, 1] = i+1

    print(con.shape)
    return con
