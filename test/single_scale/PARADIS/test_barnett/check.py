import numpy as np
import barnett


def compute(computed_disp=None, dd_nodes=None, pos_disp=None, **kwargs):
    computed_disp = computed_disp['displacement']
    dd_nodes = dd_nodes['position']
    pos_disp = pos_disp['compute']
    a = 4.04
    r0 = a*np.sqrt(2)/2.
    burg = np.array([1., 0., 0.])*r0
    slip = np.array([0., 1., 0.])
    # create segments from node positions knowing it is a loop
    elems = np.zeros((dd_nodes.shape[0], 2), dtype=int)
    elems[:, 0] = np.arange(0, dd_nodes.shape[0], dtype=int)
    elems[:, 1] = np.arange(1, dd_nodes.shape[0]+1, dtype=int)
    elems[-1, 1] = 0

    python_computed_disp = barnett.computeDisloDisplacements(
        dd_nodes, elems, pos_disp, burg, slip_plane=slip)
    error = np.linalg.norm(computed_disp - python_computed_disp)
    if error > 1e-4:
        raise RuntimeError("barnett field was not well computed: {0}".format(
            error))

    print(f"barnett field was computed with an error {error}")

    # print(computed_disp[:10])
    # print(python_computed_disp[:10])
    # print((computed_disp/python_computed_disp)[:10])

    return python_computed_disp
