
import numpy as np


def compute(**kwargs):
    pts = np.array([6, 6, 6])
    R = 100
    loop_center = np.array([-50., -50., 0.])
    centerGrid = R*np.array([1., 1., 0.])/np.sqrt(2) + loop_center

    pos = np.zeros((pts[0]*pts[1]*pts[2], 3))
    p = 0
    Nx, Ny, Nz = pts
    L = 20/Nx

    # print(pts, centerGrid)
    for i in range(Nx):
        for j in range(Ny):
            for k in range(Nz):
                pos[p, :] = L*np.array([i-Nx/2, j-Ny/2, k-Nz/2])
                pos[p, :] += centerGrid
                pos[p, :] += L/2
                p += 1
    return pos
