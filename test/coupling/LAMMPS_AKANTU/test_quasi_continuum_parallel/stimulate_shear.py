#!/usr/bin/env python
import numpy as np

def compute(**kwargs):
    keys = kwargs.keys()
    names = ["disp_md", "disp_upper", "disp_lower"]
    for name in names:
        if name in keys:
            key = name
    try:
        pos0 = kwargs[key]
    except UnboundLocalError as err:
        raise UnboundLocalError("{0}\nThe only available keys are {1}".format(err, keys))
    disp = np.zeros(pos0.shape)
    disp[:,0] = pos0[:,1]*kwargs["shearstrain"]
    print "additional displacement for {0}: sheared by tau = {1}".format(key, kwargs["shearstrain"])
    print "  {0:e} <= disp <= {1:e} and {2:e} <= position <= {3:e} ".format(
        disp[:,0].min(), disp[:,0].max(), pos0[:,1].min(), pos0[:,1].max())
    return disp

if __name__ == "__main__":
    pass
