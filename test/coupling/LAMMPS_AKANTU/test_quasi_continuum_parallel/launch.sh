#!/bin/bash

export NPROC=$1
export NPROCMD=$2
export NPROCUPPERFEM=$3
export NPROCLOWERFEM=$4
export RUN_FOLDER=run_nproc_$NPROC

mkdir $RUN_FOLDER

cd $RUN_FOLDER

ln -s ../stimulate_shear.py

mkdir output4

# run the sim
time mpirun -np $NPROC ../$LMPATH ../$CONF_FILE $NSTEPS

if [ $? != 0 ]
then
    echo "Libmultiscale failed for nproc = $NPROC"
    exit -1
else
    echo "Libmultiscale success for nproc = $NPROC"
fi

# check the results
echo "Results of diff:---------------------------------------------------------"
diff output4/ekin_upper.txt ../reference_result.txt
echo "End results of diff:-----------------------------------------------------"

if [ $? != 0 ]
then
    echo "Results differ from reference for nproc = $NPROC"
    echo "This may mean a regression of libmultiscale in general or may indicate that parallelization now influences the result. please compare the outputs of all runs"
    exit -2
else
    echo "Results ok"
fi
