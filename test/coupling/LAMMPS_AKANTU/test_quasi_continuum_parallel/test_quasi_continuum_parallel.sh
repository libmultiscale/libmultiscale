#!/bin/bash


set -x
export LMPATH="../../../clients/AMEL"
export CONF_FILE=amel.config
export NSTEPS=201

python3 GenerateMesh.py --md_size 8                        --height 30.0                        --thickness 6                        --width 12.0


# sequential run
# it's launch.sh nproc nproc_md nproc_upper_fem nproc_lower_fem
./launch.sh 1 1 1 1
if [ $? != 0 ]
then
    exit -1
fi

# one proc per domain
# it's launch.sh nproc nproc_md nproc_upper_fem nproc_lower_fem
./launch.sh  3 1 1 1
if [ $? != 0 ]
then
    exit -1
fi

# one proc per FEM, MD parallel
# it's launch.sh nproc nproc_md nproc_upper_fem nproc_lower_fem
./launch.sh  8 6 1 1
if [ $? != 0 ]
then
    exit -1
fi


# everything parallel
# it's launch.sh nproc nproc_md nproc_upper_fem nproc_lower_fem
# UNCOMMENT THIS, ŠNOM!
#./launch.sh  8 4 2 2
if [ $? != 0 ]
then
    exit -1
fi
