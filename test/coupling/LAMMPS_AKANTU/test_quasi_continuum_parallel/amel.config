################################################################################
#section la plus generale
################################################################################
Section MultiScale MetalUnits

UNITCODE MetalUnits
DIMENSION 3

INCLUDE ../common.config VARIABLES


################################################################################
## geometry
INCLUDE ../common.config GEOMETRIES

################################################################################
## model declarations
COM DISTRIBUTED
PROCESSORS md_part nprocmd
PROCESSORS lower   nproclowerfem
PROCESSORS upper   nprocupperfem
MD_CODE LAMMPS md_part
ELAST_CODE AKANTU lower
ELAST_CODE AKANTU upper

################################################################################
## filters
FILTER upper_nodes GEOM INPUT upper GEOMETRY upper_bound BASED_ON_NODES
FILTER lower_nodes GEOM INPUT lower GEOMETRY lower_bound BASED_ON_NODES

COMPUTE disp_upper EXTRACT INPUT upper FIELD position
COMPUTE shear_disp_upper PYTHON INPUT disp_upper FILENAME stimulate_shear

COMPUTE disp_lower EXTRACT INPUT lower FIELD position
COMPUTE shear_disp_lower PYTHON INPUT disp_lower FILENAME stimulate_shear

COMPUTE ekin_upper EKIN INPUT upper BASED_ON_NODES

INCLUDE ../common.config FILTERS

################################################################################
## couplings
COUPLING_CODE lower_coupling md_part lower QUASICONTINUUM BLOCK_PARAMS lower_coupling_params
COUPLING_CODE upper_coupling md_part upper QUASICONTINUUM BLOCK_PARAMS upper_coupling_params
#COUPLING_CODE template dd_part md_part CADDTEMPLATE BLOCK_PARAMS dd_coupling_params


################################################################################
## stimulators

STIMULATION thermostat LANGEVIN INPUT thermo_geo TEMP 0 SEED 12 DAMP relaxation TIMESTEP timestep START shearpoint WORK

STIMULATION stim_shear_md    FIELD INPUT md_part FIELD displacement COMPUTE shear_disp_md    ADDITIVE ONESHOT shearpoint
STIMULATION stim_shear_upper FIELD INPUT upper   FIELD displacement COMPUTE shear_disp_upper ADDITIVE ONESHOT shearpoint
STIMULATION stim_shear_lower FIELD INPUT lower   FIELD displacement COMPUTE shear_disp_lower ADDITIVE ONESHOT shearpoint

## STIMULATION upper_boundary RAZ INPUT upper_nodes FIELD force STAGE PRE_STEP3
STIMULATION lower_boundary_vel RAZ INPUT lower_nodes FIELD velocity STAGE PRE_STEP1
STIMULATION lower_boundary_for RAZ INPUT lower_nodes FIELD force STAGE PRE_STEP1

################################################################################
## dumpers
#DUMPER para_md PARAVIEW INPUT md_part DISP VEL FORCE P0 PREFIX output4  FREQ paraviewdumpfreq ADDFIELD truedisp ADDFIELD centrosymmetry
#DUMPER para_upper PARAVIEW INPUT upper DISP VEL FORCE P0 PREFIX output4 FREQ paraviewdumpfreq
#DUMPER para_lower PARAVIEW INPUT lower DISP VEL FORCE P0 PREFIX output4 FREQ paraviewdumpfreq

#DUMPER ekin TEXT INPUT ekin FREQ dumpfreq PREFIX output4
#DUMPER thermowork TEXT INPUT friction_restitution:thermostat FREQ dumpfreq PREFIX output4

#DUMPER para_thermo_atoms PARAVIEW INPUT thermo_geo P0 PREFIX output4 ONESHOT 0
#DUMPER dislocated_atoms_dump PARAVIEW INPUT dislocated_atoms ADDFIELD centrosymmetry:dislocated_atoms PREFIX output4 FREQ dumpfreq

#DUMPER kinetic_atoms EKIN INPUT free_geometry UNITS RealUnits PREFIX output4
##DUMPER para_dd PARAVIEW INPUT dd_part DISP VEL FORCE P0 PREFIX output2 FREQ 1

DUMPER ekin_upper TEXT INPUT ekin_upper ONESHOT $NSTEPS-1 PREFIX output4

endSection


################################################################################
## lower coupling parameters
Section lower_coupling_params MetalUnits
PAD_PATH ../input_files/lower_pad_atoms.txt
INTERFACE_PATH ../input_files/lower_interface_atoms.txt
GRID_DIVISION 5 2 3
GEOMETRY lower_pad
TOL 1e-4
endSection

################################################################################
## upper coupling parameters
Section upper_coupling_params MetalUnits
PAD_PATH ../input_files/upper_pad_atoms.txt
INTERFACE_PATH ../input_files/upper_interface_atoms.txt
GRID_DIVISION 5 2 3
GEOMETRY upper_pad
TOL 1e-4
endSection

################################################################################
## dd coupling parameters
Section dd_coupling_params MetalUnits
DISLO_ORIENTATION 0
SEARCH_DIM 1 # CHECK THIS
TOL 1e-12
SHEAR 0
SHEAR_START 20
INITIAL_X 9*2*burgers
SHEAR_STRESS -1
DETECTGEOM detection
PADGEOM dd_pad
MDGEOM 1
endSection


################################################################################
#Lammps section
Section LAMMPS:md_part MetalUnits
LAMMPS_FILE ../lammps.conf
DOMAIN_GEOMETRY 1
endSection

################################################################################
## lower FEM section
Section AKANTU:lower MetalUnits
DOMAIN_GEOMETRY 1
MESH_FILENAME ../input_files/lower_domain_akantu.msh
MATERIAL_FILENAME ../aniso_aluminum.dat
TIMESTEP timestep # (metalunits)
PBC 1 0 1
PRESTRESSED_PERIODICITY
TRACTION_BNDY_TAG 20
TRACTION_BNDY_TENSOR 0 stress 0 stress 0 0 0 0 0
TRACTION_BNDY_START shearpoint
endSection

################################################################################
## upper FEM section
Section AKANTU:upper MetalUnits
DOMAIN_GEOMETRY 1
MESH_FILENAME ../input_files/upper_domain_akantu.msh
MATERIAL_FILENAME ../aniso_aluminum.dat
TIMESTEP timestep
PBC 1 0 1
PRESTRESSED_PERIODICITY
TRACTION_BNDY_TAG 20
TRACTION_BNDY_TENSOR 0 stress 0 stress 0 0 0 0 0
TRACTION_BNDY_START shearpoint
endSection
