Section MultiScale RealUnits

UNITCODE RealUnits
DIMENSION 2

INCLUDE geometries.config Geometries

COM DISTRIBUTED 

LET stiff=36*1.58740105196819947475*(1.3641211104312093062*0.92)/2.5400008365880643002/2.5400008365880643002
LET density=r0*r1/2
LET initT=2*20
LET finalT=20
LET dampCoeff=1000
LET restCoeffx=1
LET restCoeffy=1

###### model declaration ######
MD_CODE LAMMPS md
ELAST_CODE AKANTU fe-left
ELAST_CODE AKANTU fe-right

FILTER mdGeom GEOM INPUT md GEOMETRY md
FILTER mdInnerGeom GEOM INPUT md GEOMETRY inner-md
###### stimulations ######
# STIMULATION thermo-filter THERMOSTAT_FILTERING INPUT mdInnerGeom VELOCITYBASED TEMPERATURE 40 \
#  	       		  SCALING_STRENGTH 1 DOMAIN_LENGTH Lx Ly \
#  			  STIFFNESS stiff stiff  MAX_LAMBDA 10*r0 2*r1 \
#  			  NB_FREQUENCY_MODES 81 1 MODESPACING 1 1 SEED 32 ONESHOT 0

# STIMULATION thermo-filter THERMOSTAT_FILTERING INPUT mdInnerGeom FILTER_SIZE 100*r0 \
# 	    		  CUTOFF_WAVELENGTH 10*r0 NEIGHBORS md REPLICA 0 1 FILTER_NPOINT 1000 \
# 			  DENSITY density FUNCTION highpass RCUT 100*r0 ONESHOT 0

STIMULATION impulse   IMPULSE     INPUT mdInnerGeom LWAVE WaveLength*r0 DIRECTION 0 INTENSITY 1 STAGE PRE_DUMP ONESHOT 0

###### couplings ######
# least square coupling
COUPLING_CODE couplingLeft md fe-left THERMALFILTER MECHANICAL_BOUNDARY coupling-boundary-left MECHANICAL_GEOM coupling-bridging-left INTERFACE_GEOM coupling-bridging-left BLOCK_GEOM coupling-bridging-left GRID_DIVISION 2 2
# BLOCK_PARAMS LSFDZone
COUPLING_CODE couplingRight md fe-right THERMALFILTER MECHANICAL_BOUNDARY coupling-boundary-right MECHANICAL_GEOM coupling-bridging-right INTERFACE_GEOM coupling-bridging-right BLOCK_GEOM coupling-bridging-right GRID_DIVISION 2 2
# BLOCK_PARAMS LSFDZone

###### dumpers ######
DUMPER dumpermd       PARAVIEW INPUT md       P0 DISP VEL FORCE MASS FREQ 1000 PREFIX ./
DUMPER dumperfe-left  PARAVIEW INPUT fe-left  P0 DISP VEL FORCE MASS FREQ 1000 PREFIX ./
DUMPER dumperfe-right PARAVIEW INPUT fe-right P0 DISP VEL FORCE MASS FREQ 1000 PREFIX ./

# DUMPER unmatched-fe-boundaryL PARAVIEW INPUT unmatched-fe-boundary_zone:couplingLeft DISP VEL FORCE ONESHOT 0 PREFIX ./
# DUMPER unmatched-fe-bridgingL PARAVIEW INPUT unmatched-fe-bridging_zone:couplingLeft DISP VEL FORCE ONESHOT 0 PREFIX ./
# DUMPER matched-fe-boundaryL PARAVIEW INPUT matched-fe-boundary_zone:couplingLeft DISP VEL FORCE ONESHOT 0 PREFIX ./
# DUMPER matched-fe-bridgingL PARAVIEW INPUT matched-fe-bridging_zone:couplingLeft DISP VEL FORCE ONESHOT 0 PREFIX ./

# DUMPER unmatched-fe-boundaryR PARAVIEW INPUT unmatched-fe-boundary_zone:couplingRight DISP VEL FORCE ONESHOT 0 PREFIX ./
# DUMPER unmatched-fe-bridgingR PARAVIEW INPUT unmatched-fe-bridging_zone:couplingRight DISP VEL FORCE ONESHOT 0 PREFIX ./
# DUMPER matched-fe-boundaryR PARAVIEW INPUT matched-fe-boundary_zone:couplingRight DISP VEL FORCE ONESHOT 0 PREFIX ./
# DUMPER matched-fe-bridgingR PARAVIEW INPUT matched-fe-bridging_zone:couplingRight DISP VEL FORCE ONESHOT 0 PREFIX ./

BLOCK_PARAMS ENERGY

endSection

################################################################
#MD section
################################################################
Section LAMMPS:md RealUnits
LAMMPS_FILE lammps-hp.in
DOMAIN_GEOMETRY md
endSection


################################################################
#FE left section
################################################################
Section AKANTU:fe-left RealUnits
DOMAIN_GEOMETRY fe-left
MESH_FILENAME meshTri.msh
MATERIAL_FILENAME material.dat
TIMESTEP 1
SCALE LFEMx/NBELEMX LFEMy/NBELEMY
SHIFT -LFEMx-Lx/2+bridgingSize+epsilonX -Ly/2
PBC 0 1
endSection

################################################################
#FE right section
################################################################
Section AKANTU:fe-right RealUnits
DOMAIN_GEOMETRY fe-right
MESH_FILENAME meshTri.msh
MATERIAL_FILENAME material.dat
TIMESTEP 1
SCALE LFEMx/NBELEMX LFEMy/NBELEMY
SHIFT Lx/2-bridgingSize-epsilonX -Ly/2
PBC 0 1
endSection

################################################################
# Spatial filter
################################################################
Section SFsection RealUnits

FILTER_SIZE 40*r0 
FILTER_NPOINT 1000 
DENSITY density
FUNCTION highpass 
RCUT 40*r0 
CUTOFF_WAVELENGTH 20*r0
REPLICA 0 4

endSection

################################################################
#Coupling LeastSquare with filtering
################################################################
Section LSFZone RealUnits

BLOCK_PARAMS SFsection
FILTERING
endSection

################################################################
#Coupling LeastSquare with damping
################################################################
Section LSFDZone RealUnits

BLOCK_PARAMS SFsection
GLE_DAMPING 
DAMPING dampCoeff
endSection

################################################################
#Coupling LeastSquare with damping and restitution
################################################################
Section LSFDRZone RealUnits

BLOCK_PARAMS SFsection
GLE_DAMPING
DAMPING dampCoeff
#GLE_RESTITUTION
RESTITUTION restCoeffx restCoeffy
STIFFNESS stiff stiff
DOMAIN_LENGTH Lx Ly
MAX_LAMBDA 8*r0 4*r1
MODESPACING 1 1
R0 r0 r1
NB_FREQUENCY_MODES 31 1
LATTICE_TYPE hex_2d
ATOM_MASS 26.98153860
TEMPERATURE finalT
SEED 23
endSection

################################################################
# Filters and Computes
################################################################

Section ENERGY RealUnits

FILTER feLeftGeom GEOM INPUT fe-left GEOMETRY fe-left
FILTER feLeftNonOverlapGeom GEOM INPUT fe-left GEOMETRY non-overlap-fe-left
FILTER feLeftBoundGeom GEOM INPUT fe-left GEOMETRY coupling-boundary-left
FILTER feLeftBrigGeom GEOM INPUT fe-left GEOMETRY coupling-bridging-left

FILTER feRightGeom GEOM INPUT fe-right GEOMETRY fe-right
FILTER feRightNonOverlapGeom GEOM INPUT fe-right GEOMETRY non-overlap-fe-right
FILTER feRightBoundGeom GEOM INPUT fe-right GEOMETRY coupling-boundary-right
FILTER feRightBrigGeom GEOM INPUT fe-right GEOMETRY coupling-bridging-right

COMPUTE ekin-feL EKIN INPUT feLeftGeom
COMPUTE ekin-feLNOG EKIN INPUT feLeftNonOverlapGeom
COMPUTE ekin-feLBOUND EKIN INPUT feLeftBoundGeom
COMPUTE ekin-feLBRIG EKIN INPUT feLeftBrigGeom

COMPUTE ekin-feR EKIN INPUT feRightGeom
COMPUTE ekin-feRNOG EKIN INPUT feRightNonOverlapGeom
COMPUTE ekin-feRBOUND EKIN INPUT feRightBoundGeom
COMPUTE ekin-feRBRIG EKIN INPUT feRightBrigGeom

COMPUTE epot-feL EPOT INPUT feLeftGeom
COMPUTE epot-feLNOG EPOT INPUT feLeftNonOverlapGeom
COMPUTE epot-feLBOUND EPOT INPUT feLeftBoundGeom
COMPUTE epot-feLBRIG EPOT INPUT feLeftBrigGeom

COMPUTE epot-feR EPOT INPUT feRightGeom
COMPUTE epot-feRNOG EPOT INPUT feRightNonOverlapGeom
COMPUTE epot-feRBOUND EPOT INPUT feRightBoundGeom
COMPUTE epot-feRBRIG EPOT INPUT feRightBrigGeom

DUMPER energy-mesh TEXT INPUT ekin-feL 	  ADD_COMPUTE epot-feL \
		   ADD_COMPUTE ekin-feLNOG   ADD_COMPUTE epot-feLNOG \
		   ADD_COMPUTE ekin-feLBOUND ADD_COMPUTE epot-feLBOUND \
		   ADD_COMPUTE ekin-feLBRIG  ADD_COMPUTE epot-feLBRIG \
		   ADD_COMPUTE ekin-feR 	  ADD_COMPUTE epot-feR \
		   ADD_COMPUTE ekin-feRNOG	  ADD_COMPUTE epot-feRNOG \
		   ADD_COMPUTE ekin-feRBOUND ADD_COMPUTE epot-feRBOUND \
		   ADD_COMPUTE ekin-feRBRIG  ADD_COMPUTE epot-feRBRIG \
		   FREQ 100 PREFIX ./
endSection