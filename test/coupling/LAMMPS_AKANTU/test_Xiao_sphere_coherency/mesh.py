import subprocess
import numpy as np

def mesh(LX, LY, Nelement_x, Nelement_y):

    geo = f'''
    LX = {LX};
    LY = {LY};
  
    Point(1) = {{0.0, 0.0, 0.0}};
    Point(2) = {{LX,   0.0, 0.0}};
    Point(3) = {{LX,   LY,   0.0}};
    Point(4) = {{0.0, LY,   0.0}};
    
    Line(1) = {{1, 2}};
    Line(2) = {{2, 3}};
    Line(3) = {{3, 4}};
    Line(4) = {{4, 1}};

    Line Loop(5) = {{1, 2, 3, 4}};
    Plane Surface(6) = {{5}};
    Physical Volume("internal") = {{1}};

    Transfinite Line {{2, 4}} = {Nelement_y} Using Progression 1;  

    Transfinite Line {{1, 3}} = {Nelement_x} Using Progression 1;  

    Transfinite Surface {6};

    Recombine Surface {6};

    '''

    with open('mesh.geo', 'w') as f:
        f.write(geo)

    subprocess.call("gmsh -2 " + "mesh.geo -o " + "mesh.msh", shell=True)

d0 = 0.00275
Lx = 0.1831713080185027
Ly = 4*20*d0
mesh(Lx, Ly, 3+1, 4+1)
