#!/usr/bin/env bash
module load CADDMesher

cd mesh

python3 GenerateMesh.py

cd ..

if [ $? != 0 ]
then
    echo "Generation of mesh failed"
    exit -1
else
    echo "Generation of mesh successful"
fi

exit 0
