#!/usr/bin/env bash
module load CADDMesher

cd mesh

python3 GenerateMesh.py

if [ $? != 0 ]
then
    echo "Generation of mesh failed"
    exit -1
else
    echo "Generation of mesh successful"
fi

cd ..

mpirun -np 4 ../../../clients/AMEL $1 $2

if [ $? != 0 ]
then
    echo "Libmultiscale failed"
    exit -1
else
    echo "Libmultiscale successful"
fi

exit 0
