#!/usr/bin/env python
import numpy as np
import sys

from stimulate_strain import grad_u, solid_body_disp, names

names = ((names[0], "disp_md_ext"), (names[1], "disp_fe_ext"))

tol_max = .1
tol_mean = .01

def compute(**kwargs):
    keys = kwargs.keys()
    for pos_name, disp_name in names:
        if pos_name in keys:
            pos_key, disp_key = pos_name, disp_name
    try:
        pos0 = kwargs[pos_key]
    except UnboundLocalError as err:
        raise UnboundLocalError(
            "{0}\nThe only available keys are {1}".format(err, keys))
    try:
        real_disp = kwargs[disp_key]
    except UnboundLocalError as err:
        raise UnboundLocalError(
            "{0}\nThe only available keys are {1}".format(err, keys))


    theoretical_disp = np.array(np.matrix(pos0) * grad_u) + solid_body_disp

    real_diff = theoretical_disp - real_disp
    dists =  (real_diff**2).sum(1)**.5
    max_err = dists.max()
    mean_err = dists.mean()
    print ("\n")
    print ("the tolerances on mean and max displacement "
           "are {0} and {1}, respectively".format(mean_err, max_err))
    def symbol(val, tol):
        if val < tol:
            return "<"
        elif val == tol:
            return "="
        else:
            return ">"
    mean_ok = mean_err < tol_mean
    max_ok = max_err < tol_max
    print ("the maximum error on displacement = {0} {1} tol: {2}".format(
        max_err, symbol(max_err, tol_max),
        "passed" if max_ok else "FAILED"))
    print ("the mean error on displacement = {0} {1} tol: {2}".format(
        mean_err, symbol(mean_err, tol_mean),
        "passed" if mean_ok else "FAILED"))

    if max_ok and mean_ok:
        print("Test succesful")
        sys.exit(0)
    print("Test failed")
    sys.exit(2)
    return disp
