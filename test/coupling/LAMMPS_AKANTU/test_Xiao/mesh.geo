h = 1;
M = 120;
N = 4;

Point(1) = {0.0, 0.0, 0.0, h};
Point(2) = {M,   0.0, 0.0, h};
Point(3) = {0.0, N,   0.0, h};
Point(4) = {M,   N,   0.0, h};


Line(27) = {2, 4};
Line(28) = {4, 3};
Line(29) = {3, 1};
Line(30) = {1, 2};
Line Loop(31) = {27, 28, 29, 30};
Plane Surface(32) = {31};

Transfinite Line {30, 28} = 100 Using Progression 1;

Transfinite Surface "*";

