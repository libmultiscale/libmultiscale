#!/usr/bin/env bash
module load CADDMesher

python3 mesh_gen.py

export NBTIMESTEP=$2

export NPMD=1
export NPFE=1
mpirun -np 1 ../../../clients/AMEL $1 $NBTIMESTEP
#export NPMD=1
#mpirun -np 2 ../../../clients/AMEL $1 $NBTIMESTEP
#export NPMD=2
#mpirun -np 3 ../../../clients/AMEL $1 $NBTIMESTEP
#export NPMD=3
#mpirun -np 4 ../../../clients/AMEL $1 $NBTIMESTEP
