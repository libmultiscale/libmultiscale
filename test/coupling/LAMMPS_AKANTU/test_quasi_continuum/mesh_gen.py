#!/usr/bin/env python
from __future__ import print_function

import numpy as np

#this is the cadd mesher python wrap. You need to compile this first
from CADDMesher import *

## constants
latt_const = 4.032

def buildDomain(width, height):
    print("Building Domain... ", end="")
    ## aluminium lattice:
    consts = vecReal([latt_const, latt_const, latt_const])
    ## standard orienation
    miller = vecReal([ 1., 0., 0.,
                       0., 1., 0.,
                       0., 0., 1])
    lattice = FccLattice3d(consts, miller)


    ## domain geometries
    min_max = vecReal(
        [lim*latt_const for lim in [0, width, 0, width, 0, height]])
    overall = Block3d(min_max, "overall")

    min_max = vecReal(
        [lim*latt_const for lim in [0, width, 0, width, height-7, height]])
    refined = Block3d(min_max, "refined")

    min_max = vecReal(
        [lim*latt_const for lim in [0, width, 0, width, height-4, height]])
    atomistic = Block3d(min_max, "atomistic")

    min_max = vecReal(
        [lim*latt_const for lim in [0, width, 0, width, height-3.9, height]])
    atomistic_skinned = Block3d(min_max, "atomistic_skinned")

    ## correction for periodic boundary conditions
    centre = PointRef3d(vecReal([0, 0, height*latt_const]))

    ## creation of domain
    boundary_special_treatment = False
    domain = Domain3d(overall, refined, atomistic, atomistic_skinned, lattice,
                      boundary_special_treatment, centre)
    print("done")
    return domain

def buildAuxMesh(domain, width, height):
    print("Building auxiliary mesh... ", end="")
    coords = -.1 * width * latt_const, 1.1 * width * latt_const
    refinement_point = PointRef3d();
    for x in coords:
        refinement_point[0] = x
        for y in coords:
            refinement_point[1] = y

            ## base
            refinement_point[2] = -.1*height * latt_const
            refinement = 30.
            domain.setPointRefinement(refinement_point, refinement)

            ## intermediate
            refinement_point[2] = (height - 8) * latt_const
            refinement = 7.9
            domain.setPointRefinement(refinement_point, refinement)

            ## intermediate
            refinement_point[2] = (height - 14) * latt_const
            refinement = 8.
            domain.setPointRefinement(refinement_point, refinement)

            ## interface
            refinement_point[2] = (height - 6) * latt_const
            refinement = 0.
            domain.setPointRefinement(refinement_point, refinement)

            ## top
            refinement_point[2] = (1.1*height) * latt_const
            domain.setPointRefinement(refinement_point, refinement)
    print("done")

def output(domain, width, height):
    mesh = domain.getMesh()

    ## print("Writing paraview")
    ## mesh.dump_paraview("domain")

    print("Writing gmsh")
    mesh.dump_msh("domain_akantu.msh")

    print("Writing atoms")
    atoms = PointContainer3d(domain.getAtoms())
    bounds = atoms.computeBounds()
    atom_geom = Block3d(bounds, "atomic geometry")
    atom_geom.shift(PointRef3d(vecReal([latt_const/4, latt_const/4, latt_const/4])))
    atoms.filter(atom_geom, 1e-4)
    #atoms.dump_paraview("all_atoms")
    atoms.dump_lammps("all_atoms.dat", atom_geom.bounds())

    print("Writing interface and pad atoms")
    min_max = vecReal(
        [lim * latt_const for lim in
         [0, width, 0, width, height-4.1, height-3.9]])
    interface_box = Block3d(min_max, "interface_box")
    interface_atoms = PointContainer3d("interface_atoms")
    pad_atoms = PointContainer3d("pad_atoms")
    domain.compute_interface_atoms(interface_atoms, pad_atoms, interface_box)
    interface_atoms.filter(atom_geom, 1e-4)
    interface_atoms.dump_text("interface_atoms.txt")
    interface_atoms.dump_paraview("interface_atoms")
    pad_atoms.filter(atom_geom, 1e-4)
    pad_atoms.dump_text ("pad_atoms.txt")
    pad_atoms.dump_paraview ("pad_atoms.txt")


def main():
    width, height = 16, 64
    domain = buildDomain(width, height)
    buildAuxMesh(domain, width, height)
    domain.fill_points()
    output(domain, width, height)
    print("done, exit")



if __name__ == "__main__":
    main()
