#!/usr/bin/env python
import numpy as np
import sys

from stimulate_strain import grad_u, solid_body_disp, names

names = ((names[0], "disp_md_ext"), (names[1], "disp_fe_ext"))

tol_max = .1
tol_mean = .01

def compute(**kwargs):
    T_md = kwargs["ekin_md"][0][1]
    if T_md > 140000:
        print("Test failed: temperature is now " + str(T_md))
        sys.exit(-1)

    return kwargs["ekin_md"]
