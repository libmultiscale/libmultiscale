#!/usr/bin/env python
import numpy as np
import sys

def compute(**kwargs):
#    print kwargs
    positions = kwargs['ComputeExtract:PADAssociation:coupling']
    positionsy = positions[:,1]
    ymin = np.amin(positionsy)
    ymax = np.amax(positionsy)

    weights = [(i-ymin)/(ymax-ymin) for i in positions[:,1] ]
    weights = np.array(weights)
#    print weights
    return weights
