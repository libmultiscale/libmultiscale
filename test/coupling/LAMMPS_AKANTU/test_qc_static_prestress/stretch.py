#!/usr/bin/env python
import numpy as np

def compute(**kwargs):
    keys = kwargs.keys()
    names = ["fem_pos", "md_pos"]
    for name in names:
        if name in keys:
            key = name
    try:
        pos0 = kwargs[key]
    except UnboundLocalError as err:
        raise UnboundLocalError("{0}\nThe only available keys are {1}".format(err, keys))
    pos_n = pos0.copy()
    pos_n[:,0] *= 1.01
    return pos_n

if __name__ == "__main__":
    pass
