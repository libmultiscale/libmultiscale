#!/usr/bin/env python
import numpy as np

grad_u = np.matrix([[0.02, 0.03, 0.04 ],
                    [0.06, 0.07, 0.08 ],
                    [0.10, 0.11, 0.12 ]]) * 1e-3
solid_body_disp = np.array([1, 2, 3])*0
#strain = 0.5(grad_u + grad_u.T)
names = ("pos_md", "pos_fe")

def compute(**kwargs):
    keys = kwargs.keys()
    for name in names:
        if name in keys:
            key = name
    try:
        pos0 = kwargs[key]
    except UnboundLocalError as err:
        raise UnboundLocalError(
            "{0}\nThe only available keys are {1}".format(err, keys))
    disp = np.array(np.matrix(pos0) * grad_u) + solid_body_disp

    print("When dealing with {0}, computed displacement within the ranges:".format(key))
    print("   u_x_min = {0}, \tu_x_max = {1}".format(disp.min(0)[0], disp.max(0)[0]))
    print("   u_y_min = {0}, \tu_y_max = {1}".format(disp.min(0)[1], disp.max(0)[1]))
    print("   u_z_min = {0}, \tu_z_max = {1}".format(disp.min(0)[2], disp.max(0)[2]))

    return disp
