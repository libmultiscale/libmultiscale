################################################################################
#section la plus generale
################################################################################
Section MultiScale RealUnits

UNITCODE MetalUnits
DIMENSION 3

LET A = 4.04
LET AX = 5.7134227919873046
LET AY = 6.9974852625782642
LET AZ = 9.8959385608440389
##----------------------------------------------------------------------------##
## these following params come from GenerateMesh.py
LET WIDTH = 105.69832165176514
LET HEIGHT = 279.89941050313058
LET THICKNESS = 59.375631365064237
LET ATOMISTICSLIMIT = 20.992455787734791
LET PADLIMIT = 34.987426312891316
##----------------------------------------------------------------------------##
LET sqrt2 = 1.4142135623730951
LET burgers = sqrt2*.5*A
LET stress = 0.001
LET dumpfreq = 100

################################################################################
## geometry
GEOMETRY 1 CUBE BBOX -1000 1000 -1000 1000 -1000 1000
GEOMETRY lower_pad CUBE BBOX -5 WIDTH+5 -PADLIMIT-5 -ATOMISTICSLIMIT+5 -5 THICKNESS+5
GEOMETRY upper_pad CUBE BBOX -5 WIDTH+5 ATOMISTICSLIMIT-5  PADLIMIT+5 -5 THICKNESS+5
GEOMETRY upper_bound CUBE BBOX -5 WIDTH+5 HEIGHT-1 HEIGHT+1 -5 THICKNESS+5
GEOMETRY lower_bound CUBE BBOX -5 WIDTH+5 -HEIGHT-1 -HEIGHT+1 -5 THICKNESS+5
# detection geometry
GEOMETRY detection CUBE BBOX 0 WIDTH -5 +5 1*AZ 2*AZ
# free atoms geometry
GEOMETRY free_atoms CUBE BBOX -5 WIDTH+5 -ATOMISTICSLIMIT ATOMISTICSLIMIT 1*AZ 5*AZ
# dd_pad geometry
GEOMETRY dd_pad SUB IDS 1 free_atoms

################################################################################
## model declarations
MD_CODE LAMMPS md_part
ELAST_CODE AKANTU lower
ELAST_CODE AKANTU upper
#DD_CODE PARADIS dd_part

################################################################################
## filters
FILTER upper_nodes GEOM INPUT upper GEOMETRY upper_bound
FILTER lower_nodes GEOM INPUT lower GEOMETRY lower_bound
COMPUTE truedisp TRUEDISP INPUT md_part PBC 1 0 1
COMPUTE centrosymmetry CENTRO INPUT md_part RCUT 3.5 PBC 1 0 1

################################################################################
## couplings
COUPLING_CODE lower_coupling md_part lower QUASICONTINUUM BLOCK_PARAMS lower_coupling_params
COUPLING_CODE upper_coupling md_part upper QUASICONTINUUM BLOCK_PARAMS upper_coupling_params
#COUPLING_CODE template dd_part md_part CADDTEMPLATE BLOCK_PARAMS dd_coupling_params


################################################################################
## stimulators
STIMULATION md_dislo DISLO INPUT md_part BLOCK_PARAMS dislo_params
STIMULATION fem_dislo_upper DISLO INPUT upper BLOCK_PARAMS dislo_params
STIMULATION fem_dislo_lower DISLO INPUT lower BLOCK_PARAMS dislo_params
#STIMULATION dd_dislo DISLO INPUT dd_part BLOCK_PARAMS dislo_params
STIMULATION contain_dislo RAZ INPUT md_part FIELD velocity COEF 0.9 END 500
STIMULATION c_d2 RAZ INPUT md_part FIELD velocity COEF 0.95 START 501 END 10000
STIMULATION c_d3 RAZ INPUT md_part FIELD velocity COEF 0.95 START 10001 END 100000


## STIMULATION upper_boundary RAZ INPUT upper_nodes FIELD force STAGE PRE_STEP3
## STIMULATION lower_boundary RAZ INPUT lower_nodes FIELD force STAGE PRE_STEP3

################################################################################
## dumpers
DUMPER para_md PARAVIEW INPUT md_part DISP VEL FORCE P0 PREFIX output  FREQ dumpfreq ADDFIELD truedisp ADDFIELD centrosymmetry
DUMPER para_upper PARAVIEW INPUT upper DISP VEL FORCE P0 PREFIX output FREQ dumpfreq
DUMPER para_lower PARAVIEW INPUT lower DISP VEL FORCE P0 PREFIX output FREQ dumpfreq
##DUMPER para_dd PARAVIEW INPUT dd_part DISP VEL FORCE P0 PREFIX output FREQ 1

endSection


################################################################################
## lower coupling parameters
Section lower_coupling_params RealUnits
PAD_PATH mesh/lower_pad_atoms.txt
INTERFACE_PATH mesh/lower_interface_atoms.txt
GRID_DIVISION 5 2 3
GEOMETRY lower_pad
TOL 1e-4
endSection

################################################################################
## upper coupling parameters
Section upper_coupling_params RealUnits
PAD_PATH mesh/upper_pad_atoms.txt
INTERFACE_PATH mesh/upper_interface_atoms.txt
GRID_DIVISION 5 2 3
GEOMETRY upper_pad
TOL 1e-4
endSection

################################################################################
## dd coupling parameters
Section dd_coupling_params RealUnits
DISLO_ORIENTATION 0
SEARCH_DIM 1 # CHECK THIS 
TOL 1e-12
SHEAR 0
SHEAR_START 20
INITIAL_X 9*2*burgers
SHEAR_STRESS -1
DETECTGEOM detection
PADGEOM dd_pad
MDGEOM 1
endSection


################################################################################
#Lammps section
Section LAMMPS:md_part RealUnits
LAMMPS_FILE lammps.conf
DOMAIN_GEOMETRY 1
endSection

################################################################################
Section PARADIS:dd_part RealUnits
NUMDLBSTEPS 0
CONTROLFILE frank_read_src.ctrl
DATAFILE frank_read_src.data
TIMESTEP 1
DOMAIN_GEOMETRY 1
endSection

################################################################################
## lower FEM section
Section AKANTU:lower RealUnits
DOMAIN_GEOMETRY 1
MESH_FILENAME mesh/lower_domain_akantu.msh
MATERIAL_FILENAME material.dat
TIMESTEP 1
PBC 1 0 1
PRESTRESSED_PERIODICITY
TRACTION_BNDY_TAG 0
TRACTION_BNDY_TENSOR 0 stress 0 stress 0 0 0 0 0
endSection

################################################################################
## upper FEM section
Section AKANTU:upper RealUnits
DOMAIN_GEOMETRY 1
MESH_FILENAME mesh/upper_domain_akantu.msh
MATERIAL_FILENAME material.dat
TIMESTEP 1
PBC 1 0 1
PRESTRESSED_PERIODICITY
TRACTION_BNDY_TAG 0
TRACTION_BNDY_TENSOR 0 stress 0 stress 0 0 0 0 0
endSection


## dislocation parameters
Section dislo_params MetalUnits
STAGE PRE_DUMP
BURGERS burgers
POS 9*2*burgers -A*sqrt(3)/4 0
POISSON 0.3250621346085783
ONESHOT 0
THETA 90
NB_REPLICA 10
LINE_DIR 0 0 1
SLIP_PLANE 0 1 0
endSection
