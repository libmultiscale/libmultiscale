
    LX = 0.1831713080185027;
    LY = 0.132;
  
    Point(1) = {0.0, 0.0, 0.0};
    Point(2) = {LX,   0.0, 0.0};
    Point(3) = {LX,   LY,   0.0};
    Point(4) = {0.0, LY,   0.0};
    
    Line(1) = {1, 2};
    Line(2) = {2, 3};
    Line(3) = {3, 4};
    Line(4) = {4, 1};

    Line Loop(5) = {1, 2, 3, 4};
    Plane Surface(6) = {5};
    Physical Volume("internal") = {1};

    Transfinite Line {2, 4} = 7 Using Progression 1;  

    Transfinite Line {1, 3} = 9 Using Progression 1;  

    Transfinite Surface 6;

    Recombine Surface 6;

    