import numpy as np
import math

def compute(input=None, constants=None, **kwargs):

    pos0 = input
    disp = np.zeros(pos0.shape)
    step = kwargs["step"]

    d0 = 0.00275
    dmax = 10*d0
    number_of_step = 100
    D = np.linspace(0, dmax, number_of_step)
    
    if step < number_of_step:
        d = D[step]
    else:
        d = dmax
        
    PAD = 3*d0
    BZ = 20*d0
    BRIDGE = PAD+BZ
    DEMLX = 0.1831713080185027
    DEMLY = 0.09158280806287362 + 0.09159999132898411
    DEMLZ = 0.001
    FEMLX = 0.1831713080185027
    FEMLY = 3*20*d0
    FEMLZ = 0
    
    fe1_bot = -DEMLY/2-FEMLY+BRIDGE
    fe1_top = -DEMLY/2+BRIDGE 
    fe2_bot = DEMLY/2-BRIDGE
    fe2_top = DEMLY/2-BRIDGE+FEMLY
    md_bot = -DEMLY/2
    md_top = DEMLY/2
    L = abs(fe2_top-fe1_bot)/2

    
    for i in range(len(pos0)):
        disp[:, 0] = d/L * pos0[:, 1]
            
    return disp

if __name__ == "__main__":
    pass
    
    
    
