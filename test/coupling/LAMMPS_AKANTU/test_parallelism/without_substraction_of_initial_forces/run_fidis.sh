#!/bin/bash 
#SBATCH --job-name 1
#SBATCH 
#SBATCH -o out.%j 
#SBATCH -e err.%j 
#SBATCH --nodes 1 
# "Task" is a process, "CPUs per task" means threads per process 
#SBATCH --ntasks-per-node 5
#SBATCH --cpus-per-task 1 
#SBATCH --mem 100G 
# 
#SBATCH --time 72:00:00 

# Auto module load 

srun -n 5 /home/mvoisin/libmultiscale/build/clients/AMEL-sphere lm.config 2500000

