h = 0.1;
L = 1.0;
N = 4;

Point(1) = {0.0, 0.0, 0.0, h};
Point(2) = {N*L, 0.0, 0.0, h};
Point(3) = {0.0, L, 0.0, h};
Point(4) = {N*L, L, 0.0, h};


Line(27) = {2, 4};
Line(28) = {4, 3};
Line(29) = {3, 1};
Line(30) = {1, 2};
Line Loop(31) = {27, 28, 29, 30};
Plane Surface(32) = {31};

Transfinite Surface "*";

