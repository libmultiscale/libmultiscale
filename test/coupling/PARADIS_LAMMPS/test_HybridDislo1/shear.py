import numpy as np

def compute(**kwargs):
    shear = .01
    output_array = np.zeros(kwargs["position"].shape)
    output_array[:,0] = shear*kwargs["position"][:,1]
    return output_array


if __name__ == "__main__":
    input_array = np.random.rand(4,3)
    print input_array
    print compute(input_array)
