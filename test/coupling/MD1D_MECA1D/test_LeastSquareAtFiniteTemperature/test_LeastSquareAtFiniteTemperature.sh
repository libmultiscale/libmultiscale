mpirun -np 1 ../../../clients/AMEL $1 $2

# perl ./analysePVF.pl simu.info -1 1 -800 800 displacement
# mkdir -p displacement
# mv *png displacement/
# mv *avi displacement/

# perl ./analysePVF.pl simu.info -0.2 0.2 -800 800 displacement
# mkdir -p zoomdisplacement
# mv *png zoomdisplacement/
# mv *avi zoomdisplacement/

# perl ./analysePVF.pl simu.info -0.05 0.05 -800 -400 displacement
# mkdir -p zoomzoomdisplacement
# mv *png zoomzoomdisplacement/
# mv *avi zoomzoomdisplacement/

# perl ./analysePVF.pl simu1.info 1e-20 1e-3 1e-4 1 energydensity