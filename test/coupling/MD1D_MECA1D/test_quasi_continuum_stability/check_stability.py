#!/usr/bin/env python
import numpy as np
import sys

def compute(**kwargs):
    T_md = kwargs["ekin_md"][0][1]
    if T_md > 100:
        print("Test failed: temperature is now " + str(T_md))
        sys.exit(-1)

    return kwargs["ekin_md"]
