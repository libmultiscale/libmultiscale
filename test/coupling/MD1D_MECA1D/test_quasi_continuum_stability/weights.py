#!/usr/bin/env python
import numpy as np
import sys

def compute(**kwargs):
#    print kwargs
    positions = kwargs['ComputeExtract:PADAssociation:coupling']
    xmin = np.amin(positions)
    xmax = np.amax(positions)

    weights = [1. - (i-xmin)/(xmax-xmin) for i in positions[:,0] ]
    weights = np.array(weights)
    print weights
    return weights
