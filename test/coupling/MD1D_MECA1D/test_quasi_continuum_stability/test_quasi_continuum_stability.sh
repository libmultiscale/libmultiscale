#!/usr/bin/env bash

cd mesh

python3 cadd1d.py

cd -


../../../clients/AMEL $1 $2

if [ $? != 0 ]
then
    echo "Libmultiscale failed"
    exit -1
else
    echo "Libmultiscale successful"
fi

exit 0
