mpirun -np 1 ../../../clients/AMEL $1 200000

perl ./analysePVF.pl simu1.info -1 1 -1000 1000 displacement

gnuplot -p -e "set term pngcairo;set output 'energyconservation.png';p 'thermo-md.txt' u 1:(\$3+\$6) w lp t 'MD - Total energy', '< paste thermo-left.txt thermo-right.txt' u 1:(\$4+\$9) w lp t 'FE - Only Kinetic Energy'"

mkdir -p displacement
mv *png displacement/
mv *avi displacement/

perl ./analysePVF.pl simu1.info -0.1 0.1 -1000 1000 displacement
mkdir -p zoomdisplacement
mv *png zoomdisplacement/
mv *avi zoomdisplacement/

perl ./analysePVF.pl simu1.info -0.05 0.05 -800 -400 displacement
mkdir -p zoomzoomdisplacement
mv *png zoomzoomdisplacement/
mv *avi zoomzoomdisplacement/

# perl ./analysePVF.pl simu1.info 1e-20 1e-3 1e-4 1 energydensity