#!/usr/bin/perl

`rm -f *.png`;

#use Curses;

$index{"displacement"} = 2;
$index{"velocity"} = 3;
$index{"force"} = 4;
$index{"energydensity"} = 5;
$index{"linecolor"} = 6;
$index{"pointtype"} = 7;

if ((@ARGV[0] ne "")&&(@ARGV[1] ne "")&&(@ARGV[2] ne "")&&(@ARGV[3] ne "")){
    $file_list = @ARGV[0];
    $ymin = @ARGV[1];
    $ymax = @ARGV[2];
    $xmin = @ARGV[3];
    $xmax = @ARGV[4];
}
else{
    print "Usage : ./analysePVF.pl file_list ymin ymax xmin xmax OPTION";
    print "\t where OPTION = displacement/velocity/force/energydensity\n";
    exit();
}

$option = @ARGV[5];

# read the file_list
@models=read_file($file_list);

$n_models = @models;
for ($k = 0 ; $k < $n_models ; $k  = $k+1)
{
    @line = split / / , @models[$k];

    $tab_name = "file" . @line[0];
    push @modelkey,$line[0];
    $hash_model{@line[0]} = $tab_name;
    $gmode_model{@line[0]} = @line[1];
    if (@line[6] ne ""){
	$line_color{@line[0]} = trim(@line[6]);
    }
    if (@line[7] ne ""){
	$point_type{@line[0]} = trim(@line[7]);
    }

    open(FOUT,@line[$index{$option}]) or die "Cannot open file @line[$index{$option}]";
    @$tab_name = <FOUT>;
    close(FOUT);

    if ($k > 0 && $n_lines != @$tab_name)
    {
	print "Files do not correspond to the same simulation\n";
	exit();
    }
    else
    {
	$n_lines = @$tab_name;
    }
}


#initscr();
#getmaxyx($row, $col);

$j = 0;
for ($k = 0 ; $k < $n_lines ; $k  = $k+1,$j = $j + 1)
{
    $fig_handle = sprintf "$option%.4d.png", $j+1;

    Dump(3,"Generating $fig_handle [$j/$n_lines]");

    $commande = "";

    #foreach $key (keys (%hash_model)) 
    foreach (@modelkey)
    {
	$key=  $_;
	$temp=  $hash_model{$key};

	@val = split /\t/, @$temp[$k];

	$nbr_champs = @val;

	if ($nbr_champs >= 2) {

	    @tab = @val;
	    $file = "temp_$temp";
	    &Compute;

	    if ($commande ne ""){
		$commande = $commande . ",";
	    }

	    if (($line_color{$key} && $point_type{$key}) ne ""){
		$lcolor = $line_color{$key};
		$ptype = $point_type{$key};
		$commande = $commande . "\"temp_$temp\" using 1:2 title '$key' with $gmode_model{$key} lc $lcolor pt $ptype";
	    }
	    elsif ($line_color{$key} ne ""){
	    	$lcolor = $line_color{$key};		
	    	$commande = $commande . "\"temp_$temp\" using 1:2 title '$key' with $gmode_model{$key} lc $lcolor";
	    }
	    elsif ($point_type{$key} ne ""){
	    	$ptype = $point_type{$key};
	    	$commande = $commande . "\"temp_$temp\" using 1:2 title '$key' with $gmode_model{$key} pt $ptype";
	    }
	    else{
	    #$commande = $commande . "\"temp_$temp\" using 1:2 title '$key' with lp";
	    $commande = $commande . "\"temp_$temp\" using 1:2 title '$key' with $gmode_model{$key}";
	    #$commande = $commande . "\"temp_$temp\" using 1:2 title '$key' with $gmode_model{$key}" . ",\"temp_$temp\" using 1:3 title '$key' with $gmode_model{$key}" ;
	    }
	}	
    }
    
    if ($option eq "energydensity"){
	#$commande = "set logscale x\n" . $commande;
	$commande = "set title \"frame $k\"\nset xlabel \"wave number (k)\"\n set logscale x\n set ylabel \"$option\" \nset yrange [$ymin:$ymax] \nset xrange[$xmin:$xmax] \nset terminal pngcairo enhanced size 800,600\nset output '$fig_handle'\nplot " . $commande;
	#$commande = "set title \"frame $k\"\nset xlabel \"wave number (k)\"\n set ylabel \"$option\" \nset yrange [$ymin:$ymax] \nset xrange[$xmin:$xmax] \nset terminal pngcairo\nset output '$fig_handle'\nplot " . $commande;
	#$commande = "set title \"frame $k\"\nset xlabel \"wavelength\"\n set ylabel \"$option\"  \nset terminal pngcairo\nset output '$fig_handle'\nplot " . $commande;
    }
    else{
	$commande = "set title \"frame $k\"\nset xlabel \"position\"\nset ylabel \"$option\"\nset yrange [$ymin:$ymax]\nset xrange[$xmin:$xmax]\nset terminal pngcairo enhanced size 800,600\nset output '$fig_handle'\nplot " . $commande;
    }
    
#    $commande = "set title \"frame $k\"\nset xlabel \"position\"\nset ylabel \"$option\"\nset yrange [$ymin:$ymax]\nset xrange[$xmin:$xmax]
#\nset arrow 1 from -400,-1e-4 to 400,-1e-4\nset arrow 2 to -400,-1e-4 from 400,-1e-4\nset label 1 \"MD zone\" at 0,-2e-4 center
#\nset arrow 3 from 340,-5e-4 to 400,-5e-4\nset arrow 4 to 340,-5e-4 from 400,-5e-4\nset label 2 \"Thermal zone\" at 390,-8e-4 center
#\nset terminal png\nset output '$fig_handle'\nplot " . $commande;

    open(FIN,">commande") or die "Unable to create temp file\n";

    print FIN $commande;

    close(FIN);

    `gnuplot commande`;

}

Dump(3,"figs phase over");

#$str_fig = `ls $option*.fig`;
#@list_fig = split(/\n/,$str_fig);

#foreach my $file_jpg (@list_fig)
#{
#    Dump(2,"Converting $file_jpg to $file_jpg.jpg");
#    $log = `fig2dev -L jpeg $file_jpg $file_jpg.jpg`;
#    print $log;
#}

#Dump(2,"Convert to jpg phase over");
Dump(1,"Generating video (this may take a while...)");

print `mencoder mf://\$option*.png -mf w=800:h=600:type=png:fps=15 -o output$option.avi -ovc lavc`;

#`convert -loop 0 -size 225x150 *.jpg outputd.gif`;

#endwin();
#`rm -f temp* commande *.jpg *.fig`;

################################################################
# This subroutine extracts displacement or other fields 
# as requested by the user using the OPTION keyword.
# It creates the x,y data required for plotting.
################################################################
sub Compute{
    $val = @tab;
    $output = "";
   
    for ($i=0; $i < $val/2-1 ; $i = $i+1) 
    {
	$str_tmp = sprintf  "%.15e\t%.15e\n",
	@tab[2*$i+1], @tab[2*$i];
	
	$output = $output . $str_tmp;
    }

    # write the data to a temp file
    open(FIN,">$file") or die "Unable to create temp file\n";
    print FIN $output;
    close(FIN);
}

sub Dump{
($diff, $text) = @_; 
#addstr($row-$diff,0,$text);
#refresh(); 
print $text . "\n";
}

# Declare the subroutines for string manipulations
# Perl trim function to remove whitespace from the start and end of the string
sub trim($)
{
    my $string = shift;
    $string =~ s/^\s+//;
    $string =~ s/\s+$//;
    return $string;
}
# Left trim function to remove leading whitespace
sub ltrim($)
{
    my $string = shift;
    $string =~ s/^\s+//;
    return $string;
}
# Right trim function to remove trailing whitespace
sub rtrim($)
{
    my $string = shift;
    $string =~ s/\s+$//;
    return $string;
}

# subroutine to read the file_list
# returns an array with model information
sub read_file
{
    my( $filename ) = shift;
    my @lines;
    
    open( FILE, "< $filename" ) or die "Can't open $filename : $!";
    
    while( <FILE> ) {	
	s/#.*//;            # ignore comments by erasing them
	next if /^(\s)*$/;  # skip blank lines
	
	chomp;              # remove trailing newline characters	
	push @lines, $_;    # push the data line onto the array
    }
    
    close FILE;
    
    return @lines;  # return array
}
