Section MultiScale RealUnits

UNITCODE RealUnits
DIMENSION 1

INCLUDE geometries.config Geometries

COM DISTRIBUTED
#PROCESSORS md 1
#PROCESSORS fe-left 1
#PROCESSORS fe-right 1

LET stiff=36*1.58740105196819947475*(1.6567944e-21/6.94769e-21)/1.1/1.1
LET density=r0
LET DLS=2.0*(L-boundarySize)
LET initT=100
LET dampCoeff=1000

#MD model configuration
MD_CODE MD1D md
#FE model configuration for left side
ELAST_CODE MECA1D fe-left
#FE model configuration for right side
ELAST_CODE MECA1D fe-right

FILTER mdGeom GEOM INPUT md GEOMETRY mdGeom
FILTER mdTemperatureGeom GEOM INPUT md GEOMETRY mdGeomBoundaryExcluded
#FILTER mdImpulseGeom GEOM INPUT md GEOMETRY mdImpulseGeom

#stimulator to intitalize 100k temperature in the middle of MD region
# STIMULATION thermo-filter THERMOSTAT_FILTERING INPUT mdTemperatureGeom VELOCITYBASED TEMPERATURE 2.0*initT \
#  	       		  SCALING_STRENGTH 1 DOMAIN_LENGTH DLS \
#  			  STIFFNESS stiff MAX_LAMBDA 5.0*r0 \
#  			  NB_FREQUENCY_MODES 161 MODESPACING 1 SEED 32 ONESHOT 0

STIMULATION initTemp TEMPERATURE INPUT mdGeom TEMP 220 SEED 32 ONESHOT 0
#STIMULATION initTemp TEMPERATURE TEMP 120 SEED 32 INPUT mdTemperatureGeom ONESHOT 0
STIMULATION thermo-filter THERMOSTAT_FILTERING INPUT mdGeom FILTER_SIZE 40*r0 \
	    		  CUTOFF_WAVELENGTH 16*r0 NEIGHBORS md REPLICA 0 FILTER_NPOINT 1000 \
			  DENSITY density FUNCTION highpass RCUT 40*r0 ONESHOT 1 TEMPERATURE 2.0*initT

#STIMULATION impulseMD IMPULSE INPUT md LWAVE 100*r0 INTENSITY 1 ONESHOT 2

FILTER mdInterfaceBoundary GEOM INPUT md GEOMETRY total_boundary
FILTER feleftInterfaceBoundary GEOM INPUT fe-left GEOMETRY total_boundary
FILTER ferightInterfaceBoundary GEOM INPUT fe-right GEOMETRY total_boundary

# STIMULATION razmd RAZ INPUT mdInterfaceBoundary FIELD displacement FIELD velocity FIELD force START 0
STIMULATION razfeleft RAZ INPUT feleftInterfaceBoundary FIELD displacement FIELD velocity FIELD force END 1001
STIMULATION razferight RAZ INPUT ferightInterfaceBoundary FIELD displacement FIELD velocity FIELD force END 1001

FILTER feleftsubpart  GEOM INPUT fe-left  GEOMETRY SZ-left
FILTER ferightsubpart GEOM INPUT fe-right GEOMETRY SZ-right

STIMULATION razleft  RAZ INPUT feleftsubpart  FIELD displacement FIELD velocity FIELD force END 8000
STIMULATION razright RAZ INPUT ferightsubpart FIELD displacement FIELD velocity FIELD force END 8000

# least square coupling
COUPLING_CODE couplingLeft md fe-left THERMALFILTER MECHANICAL_BOUNDARY BZboundary-left MECHANICAL_GEOM BZ-left \  
	      		       	       INTERFACE_GEOM BZ-leftinterface BLOCK_GEOM BZ-leftblock \
	      	 	 	       GRID_DIVISION 2 2 GRID_DIVISIONZ 2 \
				       START 1
# BLOCK_PARAMS LSFDZone

COUPLING_CODE couplingRight md fe-right THERMALFILTER MECHANICAL_BOUNDARY BZboundary-right MECHANICAL_GEOM BZ-right \
	      		     	       INTERFACE_GEOM BZ-rightinterface BLOCK_GEOM BZ-rightblock \
	      	 	  	       GRID_DIVISION 2 2 GRID_DIVISIONZ 2 \
				       START 1
# BLOCK_PARAMS LSFDZone

FILTER mdGeomBoundary GEOM INPUT md GEOMETRY mdGeomBoundaryExcluded
COMPUTE ekinMD EKIN INPUT mdGeomBoundary
COMPUTE epotMD EPOT INPUT mdGeomBoundary
DUMPER thermo-md TEXT INPUT epotMD ADD_COMPUTE ekinMD FREQ 100

FILTER fe-leftWoutBZGeom GEOM INPUT fe-left GEOMETRY fe-leftGeom
COMPUTE ekinfe-l EKIN INPUT fe-leftWoutBZGeom BASED_ON_NODES
COMPUTE epotfe-l EPOT INPUT fe-leftWoutBZGeom
DUMPER thermo-left TEXT INPUT epotfe-l ADD_COMPUTE ekinfe-l FREQ 100

FILTER fe-rightWoutBZGeom GEOM INPUT fe-right GEOMETRY fe-rightGeom
COMPUTE ekinfe-r EKIN INPUT fe-rightWoutBZGeom BASED_ON_NODES
COMPUTE epotfe-r EPOT INPUT fe-rightWoutBZGeom
DUMPER thermo-right TEXT INPUT epotfe-r ADD_COMPUTE ekinfe-r FREQ 100

LET freq=5000
DUMPER md-dis DUMPER1D INPUT md FREQ freq PREFIX ./ FIELD position FIELD displacement
DUMPER md-vel DUMPER1D INPUT md FREQ freq PREFIX ./ FIELD position FIELD velocity
DUMPER fel-dis DUMPER1D INPUT fe-left FREQ freq PREFIX ./ FIELD position FIELD displacement
DUMPER fel-vel DUMPER1D INPUT fe-left FREQ freq PREFIX ./ FIELD position FIELD velocity
DUMPER fer-dis DUMPER1D INPUT fe-right FREQ freq PREFIX ./ FIELD position FIELD displacement
DUMPER fer-vel DUMPER1D INPUT fe-right FREQ freq PREFIX ./ FIELD position FIELD velocity

# DUMPER unmatched-fe-boundaryL PARAVIEW INPUT unmatched-fe-boundary_zone:couplingLeft DISP VEL FORCE ONESHOT 0 PREFIX ./
# DUMPER unmatched-fe-bridgingL PARAVIEW INPUT unmatched-fe-bridging_zone:couplingLeft DISP VEL FORCE ONESHOT 0 PREFIX ./
# DUMPER matched-fe-boundaryL PARAVIEW INPUT matched-fe-boundary_zone:couplingLeft DISP VEL FORCE ONESHOT 0 PREFIX ./
# DUMPER matched-fe-bridgingL PARAVIEW INPUT matched-fe-bridging_zone:couplingLeft DISP VEL FORCE ONESHOT 0 PREFIX ./

# DUMPER unmatched-fe-boundaryR PARAVIEW INPUT unmatched-fe-boundary_zone:couplingRight DISP VEL FORCE ONESHOT 0 PREFIX ./
# DUMPER unmatched-fe-bridgingR PARAVIEW INPUT unmatched-fe-bridging_zone:couplingRight DISP VEL FORCE ONESHOT 0 PREFIX ./
# DUMPER matched-fe-boundaryR PARAVIEW INPUT matched-fe-boundary_zone:couplingRight DISP VEL FORCE ONESHOT 0 PREFIX ./
# DUMPER matched-fe-bridgingR PARAVIEW INPUT matched-fe-bridging_zone:couplingRight DISP VEL FORCE ONESHOT 0 PREFIX ./

# DUMPER md PARAVIEW INPUT md DISP VEL FORCE FREQ 1000
# DUMPER fel PARAVIEW INPUT fe-left DISP VEL FORCE FREQ 1000
# DUMPER fer PARAVIEW INPUT fe-right DISP VEL FORCE FREQ 1000
endSection

################################################################
#MD section
################################################################
Section MD1D:md RealUnits
DOMAIN_GEOMETRY mdGeom
ELASTIC
RCUT rcut
R0 r0
MASS 39.95
TIMESTEP 1
EPSILON 1.6567944e-21/6.94769e-21
SIGMA 1.1
#SHIFT r0/10
endSection

################################################################
#FE section left
################################################################
Section MECA1D:fe-left RealUnits
ELASTIC
ELEM_SIZE sizeFE*r0
MASS 39.95
R0 r0
RCUT rcut
EPSILON 1.6567944e-21/6.94769e-21
SIGMA 1.1
TIMESTEP 1
#CONDUCTIVITY 0.02849643140563710967
#CAPACITY 4.98770199362e-05
#THERMAL
DOMAIN_GEOMETRY fe-leftGeom
endSection
################################################################

################################################################
#FE section right
################################################################
Section MECA1D:fe-right RealUnits
ELASTIC
ELEM_SIZE sizeFE*r0
MASS 39.95
R0 r0
RCUT rcut
EPSILON 1.6567944e-21/6.94769e-21
SIGMA 1.1
TIMESTEP 1
#CONDUCTIVITY 0.02849643140563710967
#CAPACITY 4.98770199362e-05
#THERMAL
DOMAIN_GEOMETRY fe-rightGeom
endSection
################################################################

################################################################
#Coupling LeastSquare with filtering
################################################################
Section LSFZone RealUnits

FILTERING 
FILTER_SIZE 40*r0 
FILTER_NPOINT 1000 
DENSITY r0 
FUNCTION highpass 
RCUT 40*r0 
CUTOFF_WAVELENGTH 20*r0
REPLICA 0
endSection

################################################################
#Coupling LeastSquare with damping
################################################################
Section LSFDZone RealUnits

GLE_DAMPING 
FILTER_SIZE 40*r0 
FILTER_NPOINT 1000 
DENSITY r0 
FUNCTION highpass
RCUT 40*r0 
CUTOFF_WAVELENGTH 20*r0
DAMPING dampCoeff
endSection

################################################################
#Coupling LeastSquare with damping and restitution
################################################################
Section LSFDRZone RealUnits

GLE_DAMPING
GLE_RESTITUTION
FILTER_SIZE 40*r0 
FILTER_NPOINT 1000 
DENSITY r0
FUNCTION highpass
RCUT 40*r0 
CUTOFF_WAVELENGTH 20*r0
DAMPING dampCoeff
STIFFNESS stiff
DOMAIN_LENGTH DLS
MAX_LAMBDA 4*r0
MODESPACING 1
NB_FREQUENCY_MODES 61
R0 r0
LATTICE_TYPE mono_atom_1d
ATOM_MASS 39.95
TEMPERATURE initT
SEED 32

endSection
