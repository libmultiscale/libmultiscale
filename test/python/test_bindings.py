#!/usr/bin/env python3

import os
import numpy as np
print(os.getcwd())

try:
    import test_python
except Exception:
    print("cannot load test_python")


def test_length():
    test_python.Length1Real(1.)
    test_python.Length2Real(np.random.rand(2))
    test_python.Length3Real(np.random.rand(3))
    test_python.Length4Real(np.random.rand(4))
    test_python.Length5Real(np.random.rand(5))
