#!/usr/bin/python3

import pylibmultiscale as lm
import pytest
import numpy as np
import os

lm.loadModules()
Dim = 3


comm = lm.Communicator.getCommunicator()
nb_proc = comm.getNumberFreeProcs()
comm.addGroup('md', nb_proc)
md_group = lm.Communicator.getGroup('md')

A = 4.047971240341177
L = 5

cube = lm.Cube(Dim, 'md_geom')
cube.params.bbox = [-L*A, L*A, -L*A, L*A, -L*A, L*A]
cube.init()

gManager = lm.GeometryManager.getManager()
gManager.reset()
gManager.addGeometry(cube)


@pytest.fixture
def managers():
    yield
    lm.FilterManager.destroy()
    lm.ActionManager.destroy()
    lm.DomainMultiScale.destroy()


def test_create_domain(managers):
    dom = lm.DomainLammps3('md', md_group)
    open("lammps.in", 'w').write("""
mass            * 26.98153860
pair_style	lj/cut 6.03863042319
pair_coeff      * * 2.4621 2.596037
fix		2 all nve
""")
    dom.params.lammps_file = "lammps.in"
    dom.params.create_header = True
    dom.params.boundary = ['p', 'p', 'p']
    dom.params.lattice = 'fcc'
    dom.params.lattice_size = A
    dom.params.domain_geometry = "md_geom"
    dom.params.replica = [-L, L, -L, L, -L, L]
    dom.params.lattice_origin = [.1, .1, .1]
    dom.params.timestep = 1.
    dom.init()
    lm.DomainMultiScale.getManager().addObject(dom)
    return dom


@pytest.fixture
def lammps_domain(managers):
    return test_create_domain(managers)


def test_stimulate_temperature(lammps_domain):
    dom = lammps_domain
    assert dom is not None

    initial_temp = lm.StimulationTemperature('initial_temp')
    initial_temp.params.input = 'md'
    initial_temp.params.seed = 40
    initial_temp.params.temp = 100
    initial_temp.params.oneshot = True
    initial_temp.init()
    initial_temp.stimulate()
    lm.ActionManager.getManager().addObject(initial_temp)
    thermostat = lm.StimulationLangevin('thermostat')
    thermostat.params.input = 'md'
    thermostat.params.freq = 1
    thermostat.params.damp = 10
    thermostat.params.temp = 50
    thermostat.params.seed = 40
    thermostat.params.timestep = 1
    thermostat.params.work = True
    thermostat.init()
    lm.ActionManager.getManager().addObject(thermostat)
    ekin = lm.ComputeEKin('ekin')
    ekin.params.input = 'md'
    ekin.init()
    temperature = ekin.evalOutput("Temperature")
    print(temperature.size())
    print(temperature.getDim())
    array = ekin.evalOutput('EKin').array()
    array = ekin.evalOutput('EKin_vector').array()
    print(array.shape)
    print(array)

    assert np.abs(temperature.array() - 100) < 1e-10

    dumper = lm.DumperParaview('dumper')
    dumper.params.input = 'md'
    dumper.init()
    dumper.dump()


@pytest.fixture
def lammps_domain_100K(lammps_domain):
    dom = lammps_domain
    test_stimulate_temperature(dom)
    return dom


def test_compute_python():
    check = lm.ComputePython('check')
    v = np.array([[1, 2, 3]])

    def toto(units=None, **kwargs):
        # print(units)
        print(kwargs)
        return v
    check.params.func = {'toto': toto}
    check.init()
    check.compute()
    assert (check.evalOutput('toto').array() == v).all()
    assert (check.evalOutput('toto').array().shape == (1, 3))


def test_compute_python2(managers):
    data = lm.ComputePython('data')
    check = lm.ComputePython('check')
    v = np.array([[1, 2, 3]])

    def toto(units=None, **kwargs):
        # print(units)
        print(kwargs)
        return v
    data.params.func = {'toto': toto}
    data.init()
    lm.FilterManager.getManager().addObject(data)
    check.params.input = 'data.toto'
    check.params.func = {'toto': toto}
    check.init()
    check.compute()
    assert (check.evalOutput('toto').array() == v).all()
    assert (check.evalOutput('toto').array().shape == (1, 3))


def test_compute_python3(managers):
    data = lm.ComputePython('data')

    print(os.getcwd())
    with open('tmp.py', 'w') as f:
        code = """
import numpy as np
v = np.array([[1, 2, 3]])

def toto(units=None, **kwargs):
    # print(units)
    print(kwargs)
    return v
"""
        f.write(code)

    import sys
    print(os.getcwd())
    print(os.listdir(os.getcwd()))
    sys.path.append(os.getcwd())
    data.params.filename = 'tmp'
    data.init()
    data.compute()
    v = np.array([[1, 2, 3]])

    assert (data.evalOutput('toto').array() == v).all()
    assert (data.evalOutput('toto').array().shape == (1, 3))
