#!/usr/bin/python3
import pylibmultiscale as lm
import os

time_step = 0


def get_min_time_step(d):
    ts = d.getParam("TIMESTEP")
    global time_step
    time_step = min(time_step, ts)


def test_create_domain():

    os.chdir('test_dem_fem')
    lm.loadModules()

    dom = lm.DomainMultiScale.getManager()
    dom.build('lm.config')

    actions = lm.ActionManager.getManager()
    nb_step = lm.current_step.fget()

    dom.for_each(get_min_time_step)
    print("Multiscale timestep:", time_step)

    current_time = lm.current_step.fget()*time_step

    for current_step in range(lm.current_step.fget(), nb_step):
        lm.current_step.fset(current_step)
        current_time += time_step

        lm.current_stage.fset(lm.PRE_DUMP)
        actions.action()
        lm.current_stage.fset(lm.PRE_STEP1)
        actions.action()

        def newmark_predictor(d):
            v = d.primalTimeDerivative()
            p = d.primal()
            acc = d.acceleration()

            v += 0.5 * time_step * acc
            p += time_step * v

            if isinstance(d, lm.DomainLammps3):
                angular_vel = d.getField(lm.angular_velocity)
                angular_acc = d.getField(lm.angular_acceleration)
                angular_vel += 0.5 * time_step * angular_acc
            d.changeRelease()

        dom.for_each(newmark_predictor)

        dom.for_each(lambda d:  d.enforceCompatibility())

        dom.coupling(lm.COUPLING_STEP1)

        lm.current_stage.fset(lm.PRE_STEP2)
        actions.action()

        dom.for_each(lambda d: d.updateGradient())

        dom.coupling(lm.COUPLING_STEP2)

        lm.current_stage.fset(lm.PRE_STEP3)
        actions.action()

        dom.for_each(lambda d: d.updateAcceleration())

        def newmark_corrector(d):
            v = d.primalTimeDerivative()
            acc = d.acceleration()

            v += 0.5 * time_step * acc

            if isinstance(d, lm.DomainLammps3):
                angular_vel = d.getField(lm.angular_velocity)
                angular_acc = d.getField(lm.angular_acceleration)
                angular_vel += 0.5 * time_step * angular_acc

            d.changeRelease()

        dom.for_each(newmark_corrector)

        dom.coupling(lm.COUPLING_STEP3)

        lm.current_stage.fset(lm.PRE_STEP4)
        actions.action()

        dom.coupling(lm.COUPLING_STEP4)

    lm.current_stage.fset(lm.PRE_DUMP)
    actions.action()
    lm.current_stage.fset(lm.PRE_STEP1)
    actions.action()

    lm.closeModules()
    print("Done")

################################################################


if __name__ == '__main__':
    test_create_domain()
