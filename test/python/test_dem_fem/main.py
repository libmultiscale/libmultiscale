import os
import numpy as np
import create_generate
import create_confine
import create_lammps_conf
import create_lm_config
import create_lammps_in

# Parameters

kn = 50e9
kt = 60e9
gamma_n = 1000
gamma_t = 'NULL'
xmu = 0.5
dampflag = 1
density = 2500


d1 = 0.00353
dt = 0.000000005

P = [5e6]
Size = [0.00706]
Seed = [15000]


# Code

path = "8_particles"
    
try:
    os.mkdir(path)
except OSError:
    print ("Creation of the directory %s failed" % path)
else:
    print ("Successfully created the directory %s" % path)

Lx = Size[0]
Ly = Size[0]
Lz = Size[0]
create_generate.create_generate(Lx, Ly, Lz, kn, kt, gamma_n, gamma_t, xmu, dampflag, density, dt, d1, path)

for k in range(len(P)):

    pressure = P[k]

    path2 = path + "/pressure_" + str(pressure*10**-6)

    try:
        os.mkdir(path2)
    except OSError:
        print ("Creation of the directory %s failed" % path2)
    else:
        print ("Successfully created the directory %s" % path2)
        
    create_confine.create_confine(Lx, Ly, Lz, kn, kt, gamma_n, gamma_t, xmu, dampflag, density, dt, pressure, path, path2)
    create_lammps_conf.create_lammps_conf(kn, kt, gamma_n, gamma_t, xmu, dampflag, density, dt, pressure, path2)
    create_lammps_in.create_lammps_in(kn, kt, gamma_n, gamma_t, xmu, dampflag, density, dt, pressure, path2)
    create_lm_config.create_lm_config(Lx, Ly, Lz, dt, path2)

