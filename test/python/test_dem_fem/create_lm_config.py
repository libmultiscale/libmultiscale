def create_lm_config( Lx, Ly, Lz, dt, path2):
    template = open("lm.config","r")
    outname = path2 + "/lm.config"
    out = open(outname, "w")
    
    for line in template:
        line = line.replace("{Lx}", str(Lx))
        line = line.replace("{Ly}", str(Ly))
        line = line.replace("{Lz}", str(Lz))
        line = line.replace("{dt}", str(dt))
        line = line.replace("{path2}", str(path2))
        out.write(line)
        
    template.close()
    out.close()
    return(print("create confine file: Done"))
