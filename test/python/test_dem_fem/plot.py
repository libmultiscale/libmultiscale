import matplotlib.pyplot as plt
import numpy as np
import math 
import os
from numpy.linalg import inv
from import_atoms import import_atoms
from Atom_class import Atom


def plot_velocity(path2):

    path = path2 + "/image"
    try:
        os.mkdir(path)
    except OSError:
        print ("Creation of the directory %s failed" % path)
    else:
        print ("Successfully created the directory %s" % path)
    
    


            # C44

            open10 =  "box_" + str(SizeX) + "/seed_" + str(seed) + "/pressure_" + str(pressure*10**-6) + "/Stress.txt"
            f10 = open(open10, "r")
            A10 = []
            A10 = [ line.split() for line in f10]
            del A10[0:1]

            atoms, boundaries = import_atoms("box_" + str(SizeX) + "/seed_" + str(seed) + "/pressure_" + str(pressure*10**-6) + "/confined_" + str(pressure*10**-6) + "_MPa.data")
            L = boundaries[1] - boundaries[0]
            

    fig11 = plt.figure()
    plt.subplot(1,1,1)
    ax11 = plt.gca()
    
    for i in range(len(Anisotropy_coefficient_all[0])):
        ax11.plot(Size_box_all[0][i]/dmean, Anisotropy_coefficient_all[0][i],'r*')
        ax11.plot(Size_box_all[1][i]/dmean, Anisotropy_coefficient_all[1][i], 'mo')
        ax11.plot(Size_box_all[2][i]/dmean, Anisotropy_coefficient_all[2][i], 'bs')
        ax11.plot(Size_box_all[3][i]/dmean, Anisotropy_coefficient_all[3][i], 'g^')
        ax11.plot(Size_box_all[4][i]/dmean, Anisotropy_coefficient_all[4][i], 'k+')
        ax11.plot(Size_box_all[5][i]/dmean, Anisotropy_coefficient_all[5][i], 'c*')
        ax11.plot(Size_box_all[6][i]/dmean, Anisotropy_coefficient_all[6][i], 'o', color = 'orange')
          
                
    ax11.axhline(y = 1, color = 'k', linestyle='--')
 
    ax11.set_xlabel('box size / d0', fontsize = 16)
    ax11.set_ylabel('Anisotropic coefficient', fontsize = 16)
    
    #Anisotropic coefficient error bar
    fig1 = plt.figure()
    plt.subplot(1,1,1)
    ax1 = plt.gca()

    xerr = [2*Ecart_b[0][0]/dmean]
    yerr = [2*Ecart_a[0][0]]
    ax1.errorbar(Size_box[0][0]/dmean, Anisotropy_coefficient[0], xerr=xerr, yerr=yerr, fmt='r*')

    xerr = [2*Ecart_b[1][0]/dmean]
    yerr = [2*Ecart_a[1][0]]
    ax1.errorbar(Size_box[1][0]/dmean, Anisotropy_coefficient[1], xerr=xerr, yerr=yerr, fmt='mo')

    xerr = [2*Ecart_b[2][0]/dmean]
    yerr = [2*Ecart_a[2][0]]
    ax1.errorbar(Size_box[2][0]/dmean, Anisotropy_coefficient[2], xerr=xerr, yerr=yerr, fmt='bs')

    xerr = [2*Ecart_b[3][0]/dmean]
    yerr = [2*Ecart_a[3][0]]
    ax1.errorbar(Size_box[3][0]/dmean, Anisotropy_coefficient[3], xerr=xerr, yerr=yerr, fmt='g^')

    xerr = [2*Ecart_b[4][0]/dmean]
    yerr = [2*Ecart_a[4][0]]
    ax1.errorbar(Size_box[4][0]/dmean, Anisotropy_coefficient[4], xerr=xerr, yerr=yerr, fmt='k+')

    xerr = [2*Ecart_b[5][0]/dmean]
    yerr = [2*Ecart_a[5][0]]
    ax1.errorbar(Size_box[5][0]/dmean, Anisotropy_coefficient[5], xerr=xerr, yerr=yerr, fmt='c*')
    
    xerr = [2*Ecart_b[6][0]/dmean]
    yerr = [2*Ecart_a[6][0]]
    ax1.errorbar(Size_box[6][0]/dmean, Anisotropy_coefficient[6], xerr=xerr, yerr=yerr, fmt='o', color = 'orange')
    
    ax1.axhline(y = 1, color = 'k', linestyle='--')

    ax1.set_xlabel('box size / d0', fontsize = 16)
    ax1.set_ylabel('Anisotropic coefficient', fontsize = 16)
    

    #Young Modulus
    fig22 = plt.figure()
    plt.subplot(1,1,1)
    ax22 = plt.gca()

    for i in range(len(Young_modulus_all[0])):
        ax22.plot(Size_box_all[0][i]/dmean, Young_modulus_all[0][i],'r*')
        ax22.plot(Size_box_all[1][i]/dmean, Young_modulus_all[1][i], 'mo')
        ax22.plot(Size_box_all[2][i]/dmean, Young_modulus_all[2][i], 'bs')
        ax22.plot(Size_box_all[3][i]/dmean, Young_modulus_all[3][i], 'g^')
        ax22.plot(Size_box_all[4][i]/dmean, Young_modulus_all[4][i], 'k+')
        ax22.plot(Size_box_all[5][i]/dmean, Young_modulus_all[5][i], 'c*')
        ax22.plot(Size_box_all[6][i]/dmean, Young_modulus_all[6][i], 'o', color = 'orange')

    ax22.axhline(y = Young_modulus[6], color = 'k', linestyle='--')

    ax22.set_xlabel('box size / d0', fontsize = 16)
    ax22.set_ylabel('Young modulus (Pa)', fontsize = 16)

    
    #Young Modulus error bar
    fig2 = plt.figure()

    plt.subplot(1,1,1)
    ax2 = plt.gca()

    xerr = [2*Ecart_b[0][0]/dmean]
    yerr = [2*Ecart_e[0][0]]
    ax2.errorbar(Size_box[0][0]/dmean, Young_modulus[0], xerr=xerr, yerr=yerr, fmt='r*')

    xerr = [2*Ecart_b[1][0]/dmean]
    yerr = [2*Ecart_e[1][0]]
    ax2.errorbar(Size_box[1][0]/dmean, Young_modulus[1], xerr=xerr, yerr=yerr, fmt='mo')

    xerr = [2*Ecart_b[2][0]/dmean]
    yerr = [2*Ecart_e[2][0]]
    ax2.errorbar(Size_box[2][0]/dmean, Young_modulus[2], xerr=xerr, yerr=yerr, fmt='bs')

    xerr = [2*Ecart_b[3][0]/dmean]
    yerr = [2*Ecart_e[3][0]]
    ax2.errorbar(Size_box[3][0]/dmean, Young_modulus[3], xerr=xerr, yerr=yerr, fmt='g^')

    xerr = [2*Ecart_b[4][0]/dmean]
    yerr = [2*Ecart_e[4][0]]
    ax2.errorbar(Size_box[4][0]/dmean, Young_modulus[4], xerr=xerr, yerr=yerr, fmt='k+')

    xerr = [2*Ecart_b[5][0]/dmean]
    yerr = [2*Ecart_e[5][0]]
    ax2.errorbar(Size_box[5][0]/dmean, Young_modulus[5], xerr=xerr, yerr=yerr, fmt='c*')
    
    xerr = [2*Ecart_b[6][0]/dmean]
    yerr = [2*Ecart_e[6][0]]
    ax2.errorbar(Size_box[6][0]/dmean, Young_modulus[6], xerr=xerr, yerr=yerr, fmt='o', color = 'orange')
    
    ax2.axhline(y = Young_modulus[6], color = 'k', linestyle='--')
    
    ax2.set_xlabel('box size / d0', fontsize = 16)
    ax2.set_ylabel('Young modulus (Pa)', fontsize = 16)


    #Poisson
    fig33 = plt.figure()
    plt.subplot(1,1,1)
    ax33 = plt.gca()

    for i in range(len(Poisson_all[0])):
        ax33.plot(Size_box_all[0][i]/dmean, Poisson_all[0][i],'r*')
        ax33.plot(Size_box_all[1][i]/dmean, Poisson_all[1][i], 'mo')
        ax33.plot(Size_box_all[2][i]/dmean, Poisson_all[2][i], 'bs')
        ax33.plot(Size_box_all[3][i]/dmean, Poisson_all[3][i], 'g^')
        ax33.plot(Size_box_all[4][i]/dmean, Poisson_all[4][i], 'k+')
        ax33.plot(Size_box_all[5][i]/dmean, Poisson_all[5][i], 'c*')
        ax33.plot(Size_box_all[6][i]/dmean, Poisson_all[6][i], 'o', color = 'orange')
        
    ax33.axhline(y = Poisson[6], color = 'k', linestyle='--')
    ax33.set_xlabel('box size / d0', fontsize = 16)
    ax33.set_ylabel('Poisson ratio', fontsize = 16)


    #Poisson error bar
    fig3 = plt.figure()

    plt.subplot(1,1,1)
    ax3 = plt.gca()

    
    xerr = [2*Ecart_b[0][0]/dmean]
    yerr = [2*Ecart_p[0][0]]
    ax3.errorbar(Size_box[0][0]/dmean, Poisson[0], xerr=xerr, yerr=yerr, fmt='r*')

    xerr = [2*Ecart_b[1][0]/dmean]
    yerr = [2*Ecart_p[1][0]]
    ax3.errorbar(Size_box[1][0]/dmean, Poisson[1], xerr=xerr, yerr=yerr, fmt='mo')

    xerr = [2*Ecart_b[2][0]/dmean]
    yerr = [2*Ecart_p[2][0]]
    ax3.errorbar(Size_box[2][0]/dmean, Poisson[2], xerr=xerr, yerr=yerr, fmt='bs')

    xerr = [2*Ecart_b[3][0]/dmean]
    yerr = [2*Ecart_p[3][0]]
    ax3.errorbar(Size_box[3][0]/dmean, Poisson[3], xerr=xerr, yerr=yerr, fmt='g^')

    xerr = [2*Ecart_b[4][0]/dmean]
    yerr = [2*Ecart_p[4][0]]
    ax3.errorbar(Size_box[4][0]/dmean, Poisson[4], xerr=xerr, yerr=yerr, fmt='k+')

    xerr = [2*Ecart_b[5][0]/dmean]
    yerr = [2*Ecart_p[5][0]]
    ax3.errorbar(Size_box[5][0]/dmean, Poisson[5], xerr=xerr, yerr=yerr, fmt='c*')
    
    xerr = [2*Ecart_b[6][0]/dmean]
    yerr = [2*Ecart_p[6][0]]
    ax3.errorbar(Size_box[6][0]/dmean, Poisson[6], xerr=xerr, yerr=yerr, fmt='o', color = 'orange')
    
    ax3.axhline(y = Poisson[6], color = 'k', linestyle='--')
    
    ax3.set_xlabel('box size / d0', fontsize = 16)
    ax3.set_ylabel('Poisson ratio', fontsize = 16)

    #Shear modulus
    fig44 = plt.figure()
    plt.subplot(1,1,1)
    ax44 = plt.gca()

    for i in range(len(Shear_modulus_all[0])):
        ax44.plot(Size_box_all[0][i]/dmean, Shear_modulus_all[0][i],'r*')
        ax44.plot(Size_box_all[1][i]/dmean, Shear_modulus_all[1][i], 'mo')
        ax44.plot(Size_box_all[2][i]/dmean, Shear_modulus_all[2][i], 'bs')
        ax44.plot(Size_box_all[3][i]/dmean, Shear_modulus_all[3][i], 'g^')
        ax44.plot(Size_box_all[4][i]/dmean, Shear_modulus_all[4][i], 'k+')
        ax44.plot(Size_box_all[5][i]/dmean, Shear_modulus_all[5][i], 'c*')
        ax44.plot(Size_box_all[6][i]/dmean, Shear_modulus_all[6][i], 'o', color = 'orange')
        
    ax44.axhline(y = Shear_modulus[6], color = 'k', linestyle='--')
    
    ax44.set_xlabel('box size / d0', fontsize = 16)
    ax44.set_ylabel('Shear modulus (Pa)', fontsize = 16)

    
    #Shear modulus error bar
    fig4 = plt.figure()

    plt.subplot(1,1,1)
    ax4 = plt.gca()

    xerr = [2*Ecart_b[0][0]/dmean]
    yerr = [2*Ecart_s[0][0]]
    ax4.errorbar(Size_box[0][0]/dmean, Shear_modulus[0], xerr=xerr, yerr=yerr, fmt='r*')

    xerr = [2*Ecart_b[1][0]/dmean]
    yerr = [2*Ecart_s[1][0]]
    ax4.errorbar(Size_box[1][0]/dmean, Shear_modulus[1], xerr=xerr, yerr=yerr, fmt='mo')

    xerr = [2*Ecart_b[2][0]/dmean]
    yerr = [2*Ecart_s[2][0]]
    ax4.errorbar(Size_box[2][0]/dmean, Shear_modulus[2], xerr=xerr, yerr=yerr, fmt='bs')

    xerr = [2*Ecart_b[3][0]/dmean]
    yerr = [2*Ecart_s[3][0]]
    ax4.errorbar(Size_box[3][0]/dmean, Shear_modulus[3], xerr=xerr, yerr=yerr, fmt='g^')

    xerr = [2*Ecart_b[4][0]/dmean]
    yerr = [2*Ecart_s[4][0]]
    ax4.errorbar(Size_box[4][0]/dmean, Shear_modulus[4], xerr=xerr, yerr=yerr, fmt='k+')

    xerr = [2*Ecart_b[5][0]/dmean]
    yerr = [2*Ecart_s[5][0]]
    ax4.errorbar(Size_box[5][0]/dmean, Shear_modulus[5], xerr=xerr, yerr=yerr, fmt='c*')
    
    xerr = [2*Ecart_b[6][0]/dmean]
    yerr = [2*Ecart_s[6][0]]
    ax4.errorbar(Size_box[6][0]/dmean, Shear_modulus[6], xerr=xerr, yerr=yerr, fmt='o', color = 'orange')
    
    ax4.axhline(y = Shear_modulus[6], color = 'k', linestyle='--')
    
    ax4.set_xlabel('box size / d0', fontsize = 16)
    ax4.set_ylabel('Shear modulus (Pa)', fontsize = 16)

    #Measured pressure
    fig55 = plt.figure()
    plt.subplot(1,1,1)
    ax55 = plt.gca()

    for i in range(len(Measured_pressure_all[0])):
        ax55.plot(Size_box_all[0][i]/dmean, Measured_pressure_all[0][i],'r*')
        ax55.plot(Size_box_all[1][i]/dmean, Measured_pressure_all[1][i], 'mo')
        ax55.plot(Size_box_all[2][i]/dmean, Measured_pressure_all[2][i], 'bs')
        ax55.plot(Size_box_all[3][i]/dmean, Measured_pressure_all[3][i], 'g^')
        ax55.plot(Size_box_all[4][i]/dmean, Measured_pressure_all[4][i], 'k+')
        ax55.plot(Size_box_all[5][i]/dmean, Measured_pressure_all[5][i], 'c*')
        ax55.plot(Size_box_all[6][i]/dmean, Measured_pressure_all[6][i], 'o', color = 'orange')
        
    ax55.axhline(y = Measured_pressure[6], color = 'k', linestyle='--')
    
    ax55.set_xlabel('box size / d0', fontsize = 16)
    ax55.set_ylabel('Shear modulus (Pa)', fontsize = 16)

    
