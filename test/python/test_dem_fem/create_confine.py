def create_confine(Lx, Ly, Lz, kn, kt, gamma_n, gamma_t, xmu, dampflag, density, dt, pressure, path, path2):
    template = open("confine.in","r")
    outname = path2 + "/confine.in"
    out = open(outname, "w")
    press = pressure *10**-6
    for line in template:
        line = line.replace("{Lx}", str(Lx))
        line = line.replace("{Ly}", str(Ly))
        line = line.replace("{Lz}", str(Lz))
        line = line.replace("{kn}", str(kn))
        line = line.replace("{kt}", str(kt))
        line = line.replace("{gamma_n}", str(gamma_n))
        line = line.replace("{gamma_t}", str(gamma_t))
        line = line.replace("{xmu}", str(xmu))
        line = line.replace("{dampflag}", str(dampflag))
        line = line.replace("{density}", str(density))
        line = line.replace("{dt}", str(dt))
        line = line.replace("{pressure}", str(pressure))
        line = line.replace("{path}", str(path))
        line = line.replace("{path2}", str(path2))
        line = line.replace("{press}", str(press))
        out.write(line)
        
    template.close()
    out.close()
    return(print("create confine file: Done"))
