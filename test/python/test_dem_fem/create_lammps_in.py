def create_lammps_in( kn, kt, gamma_n, gamma_t, xmu, dampflag, density, dt, pressure, path2):
    template = open("lammps.in","r")
    outname = path2 + "/lammps.in"
    out = open(outname, "w")
    
    press = pressure *10**-6
    for line in template:
        line = line.replace("{kn}", str(kn))
        line = line.replace("{kt}", str(kt))
        line = line.replace("{gamma_n}", str(gamma_n))
        line = line.replace("{gamma_t}", str(gamma_t))
        line = line.replace("{xmu}", str(xmu))
        line = line.replace("{dampflag}", str(dampflag))
        line = line.replace("{density}", str(density))
        line = line.replace("{dt}", str(dt))
        line = line.replace("{press}", str(press))
        out.write(line)
        
    template.close()
    out.close()
    return(print("create confine file: Done"))
