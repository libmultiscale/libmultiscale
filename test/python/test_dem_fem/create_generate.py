import os

def create_generate(Lx, Ly, Lz, kn, kt, gamma_n, gamma_t, xmu, dampflag, density, dt, d1, path):

    template = open("generate.in","r")
    outname = path + "/generate.in"
    out = open(outname, "w")

    pos1_x = Lx/4
    pos1_y = Ly/4
    pos1_z = Lz/4
    
    pos2_x = 3*Lx/4
    pos2_y = Ly/4
    pos2_z = Lz/4
    
    pos3_x = Lx/4
    pos3_y = 3*Ly/4
    pos3_z = Lz/4
    
    pos4_x = 3*Lx/4
    pos4_y = 3*Ly/4
    pos4_z = Lz/4
    
    pos5_x = Lx/4
    pos5_y = Ly/4
    pos5_z = 3*Lz/4
    
    pos6_x = 3*Lx/4
    pos6_y = Ly/4
    pos6_z = 3*Lz/4
    
    pos7_x = Lx/4
    pos7_y = 3*Ly/4
    pos7_z = 3*Lz/4
    
    pos8_x = 3*Lx/4
    pos8_y = 3*Ly/4
    pos8_z = 3*Lz/4
    
    for line in template:
        line = line.replace("{Lx}", str(Lx))
        line = line.replace("{Ly}", str(Ly))
        line = line.replace("{Lz}", str(Lz))
        line = line.replace("{kn}", str(kn))
        line = line.replace("{kt}", str(kt))
        line = line.replace("{gamma_n}", str(gamma_n))
        line = line.replace("{gamma_t}", str(gamma_t))
        line = line.replace("{xmu}", str(xmu))
        line = line.replace("{dampflag}", str(dampflag))
        line = line.replace("{density}", str(density))
        line = line.replace("{dt}", str(dt))
        line = line.replace("{d1}", str(d1))
        line = line.replace("{path}", str(path))
        line = line.replace("{pos1_x}", str(pos1_x))
        line = line.replace("{pos1_y}", str(pos1_y))
        line = line.replace("{pos1_z}", str(pos1_z))
        line = line.replace("{pos2_x}", str(pos2_x))
        line = line.replace("{pos2_y}", str(pos2_y))
        line = line.replace("{pos2_z}", str(pos2_z))
        line = line.replace("{pos3_x}", str(pos3_x))
        line = line.replace("{pos3_y}", str(pos3_y))
        line = line.replace("{pos3_z}", str(pos3_z))
        line = line.replace("{pos4_x}", str(pos4_x))
        line = line.replace("{pos4_y}", str(pos4_y))
        line = line.replace("{pos4_z}", str(pos4_z))
        line = line.replace("{pos5_x}", str(pos5_x))
        line = line.replace("{pos5_y}", str(pos5_y))
        line = line.replace("{pos5_z}", str(pos5_z))
        line = line.replace("{pos6_x}", str(pos6_x))
        line = line.replace("{pos6_y}", str(pos6_y))
        line = line.replace("{pos6_z}", str(pos6_z))
        line = line.replace("{pos7_x}", str(pos7_x))
        line = line.replace("{pos7_y}", str(pos7_y))
        line = line.replace("{pos7_z}", str(pos7_z))
        line = line.replace("{pos8_x}", str(pos8_x))
        line = line.replace("{pos8_y}", str(pos8_y))
        line = line.replace("{pos8_z}", str(pos8_z))
        out.write(line)

    template.close()
    out.close()
    return(print("create generate file: Done"))

