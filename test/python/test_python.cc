#include "lm_type_caster.hh"
#include "quantity.hh"
#include <boost/preprocessor/stringize.hpp>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py = pybind11;
using namespace libmultiscale;

#define FUNC_NAME(quant, size, type) quant##size##type
#define FUNC_DEF(quant, size, type)                                            \
  void FUNC_NAME(quant, size, type)(Quantity<quant, size, type>)

FUNC_DEF(Length, 1, Real) {}
FUNC_DEF(Length, 2, Real) {}
FUNC_DEF(Length, 3, Real) {}
FUNC_DEF(Length, 4, Real) {}
FUNC_DEF(Length, 5, Real) {}

#define PYTHON_FUNC_DEF(quant, size, type)                                     \
  m.def(BOOST_PP_STRINGIZE(FUNC_NAME(quant, size, type)),                      \
        &FUNC_NAME(quant, size, type))

PYBIND11_MODULE(test_python, m) {
  m.doc() = "LibMultiScale python bindings test module";

  PYTHON_FUNC_DEF(Length, 1, Real);
  PYTHON_FUNC_DEF(Length, 2, Real);
  PYTHON_FUNC_DEF(Length, 3, Real);
  PYTHON_FUNC_DEF(Length, 4, Real);
  PYTHON_FUNC_DEF(Length, 5, Real);
}
