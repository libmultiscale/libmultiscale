#!/usr/bin/python3

import pylibmultiscale as lm
import pytest
import numpy as np
import barnett as barnett_helper

lm.loadModules()
Dim = 3
lm.spatial_dimension.fset(Dim)
# lm.print_trace.fset(True)

comm = lm.Communicator.getCommunicator()

A = 4.047971240341177
L = 5

cube = lm.Cube(Dim, 'md_geom')
cube.params.bbox = [-L*A, L*A, -L*A, L*A, -L*A, L*A]
cube.init()
gManager = lm.GeometryManager.getManager()
gManager.reset()
gManager.addGeometry(cube)
all_group = lm.Communicator.getGroup('all')


has_paradis = hasattr(lm, "DomainParaDiS")


@pytest.fixture
def managers():
    yield
    lm.FilterManager.destroy()
    lm.ActionManager.destroy()
    lm.DomainMultiScale.destroy()


@pytest.fixture
def lammps_domain(managers):
    dom = lm.DomainLammpsDynamic3('md', all_group)
    open("lammps.in", 'w').write("""
mass            * 26.98153860
pair_style	lj/cut 6.03863042319
pair_coeff      * * 2.4621 2.596037
fix		2 all nve
""")
    dom.params.lammps_file = "lammps.in"
    dom.params.create_header = True
    dom.params.boundary = ['p', 'p', 'p']
    dom.params.lattice = 'fcc'
    dom.params.lattice_size = A
    dom.params.domain_geometry = "md_geom"
    dom.params.replica = [-L, L, -L, L, -L, L]
    dom.params.lattice_origin = [.1, .1, .1]
    dom.init()
    lm.DomainMultiScale.getManager().addObject(dom)
    return dom


test_ctrl = """
numXdoms           = 1
numYdoms           = 1
numZdoms           = 1

numXcells          = 4
numYcells          = 4
numZcells          = 4

#maxstep           = 500
#remeshRule        = 2
minSeg             = 4.00000e+00
maxSeg             = 4.00000e+02
timestepIntegrator = "trapezoidtimestep"
rTol               = 5.000000e+00
rc                 = 20.41241
Ecore              = 5.502262e+10
shearModulus       = 2.600000e+10
pois               = 3.000e-01
burgMag            = 2.85671e-10
TempK              = 2.000000e+01
MobScrew           = 1.000000e+01
MobEdge            = 1.000000e+01
MobClimb           = 1.000000e-04
mobilityLaw        = "FCC_0"
Rijmfile           = "Rijm.cube.out"
RijmPBCfile        = "RijmPBC.cube.out"
loadType =   0
#edotdir = [
#  1.949730e-05
#  2.755186e-06
#  1.000000e+00
#  ]
#eRate =   1.000000e+04
#appliedStress = [
#  0.000000e+00
#  0.000000e+00
#  5.000000e+08
#  0.000000e+00
#  0.000000e+00
#  0.000000e+00
#  ]
"""

test_data = """
dataFileVersion =   4
numFileSegments =   1
minCoordinates = [
  -17500.0
  -17500.0
  -17500.0
  ]
maxCoordinates = [
  17500.0
  17500.0
  17500.0
  ]

nodeCount =   29
dataDecompType =   1

dataDecompGeometry = [
  1
  1
  1
]

#
#  END OF DATA FILE PARAMETERS
#
domainDecomposition =
 -17500.0
     -17500.0
         -17500.0
         17500.0
     17500.0
  17500.0

nodalData =
#  Primary lines: node_tag, x, y, z, num_arms, constraint
#  Secondary lines: arm_tag, burgx, burgy, burgz, nx, ny, nz
0,0     5.00000e+01     -5.00000e+01     0.00000e+00     2     7
     0,1     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,28     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,1     4.76621e+01     -2.85030e+01     0.00000e+00     2     7
     0,0     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,2     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,2     4.07575e+01     -8.01109e+00     0.00000e+00     2     7
     0,1     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,3     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,3     2.96093e+01     1.05174e+01     0.00000e+00     2     7
     0,2     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,4     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,4     1.47386e+01     2.62162e+01     0.00000e+00     2     7
     0,3     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,5     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,5     -3.15916e+00     3.83512e+01     0.00000e+00     2     7
     0,4     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,6     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,6     -2.32472e+01     4.63550e+01     0.00000e+00     2     7
     0,5     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,7     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,7     -4.45861e+01     4.98533e+01     0.00000e+00     2     7
     0,6     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,8     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,8     -6.61782e+01     4.86827e+01     0.00000e+00     2     7
     0,7     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,9     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,9     -8.70138e+01     4.28977e+01     0.00000e+00     2     7
     0,8     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,10     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,10     -1.06119e+02     3.27689e+01     0.00000e+00     2     7
     0,9     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,11     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,11     -1.22600e+02     1.87699e+01     0.00000e+00     2     7
     0,10     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,12     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,12     -1.35686e+02     1.55539e+00     0.00000e+00     2     7
     0,11     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,13     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,13     -1.44765e+02     -1.80698e+01     0.00000e+00     2     7
     0,12     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,14     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,14     -1.49414e+02     -3.91881e+01     0.00000e+00     2     7
     0,13     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,15     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,15     -1.49414e+02     -6.08119e+01     0.00000e+00     2     7
     0,14     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,16     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,16     -1.44765e+02     -8.19302e+01     0.00000e+00     2     7
     0,15     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,17     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,17     -1.35686e+02     -1.01555e+02     0.00000e+00     2     7
     0,16     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,18     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,18     -1.22600e+02     -1.18770e+02     0.00000e+00     2     7
     0,17     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,19     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,19     -1.06119e+02     -1.32769e+02     0.00000e+00     2     7
     0,18     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,20     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,20     -8.70138e+01     -1.42898e+02     0.00000e+00     2     7
     0,19     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,21     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,21     -6.61782e+01     -1.48683e+02     0.00000e+00     2     7
     0,20     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,22     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,22     -4.45861e+01     -1.49853e+02     0.00000e+00     2     7
     0,21     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,23     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,23     -2.32472e+01     -1.46355e+02     0.00000e+00     2     7
     0,22     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,24     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,24     -3.15916e+00     -1.38351e+02     0.00000e+00     2     7
     0,23     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,25     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,25     1.47386e+01     -1.26216e+02     0.00000e+00     2     7
     0,24     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,26     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,26     2.96093e+01     -1.10517e+02     0.00000e+00     2     7
     0,25     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,27     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,27     4.07575e+01     -9.19889e+01     0.00000e+00     2     7
     0,26     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,28     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
0,28     4.76621e+01     -7.14970e+01     0.00000e+00     2     7
     0,27     -2.85671e+00     -0.00000e+00     -0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
     0,0     2.85671e+00     0.00000e+00     0.00000e+00
          0.00000e+00     1.00000e+00     0.00000e+00
"""


@pytest.fixture
def paradis_domain(managers):
    if not has_paradis:
        return
    dom = lm.DomainParaDiS('paradis', all_group)
    dom.params.numdlbsteps = 0
    dom.params.controlfile = 'test.ctrl'
    dom.params.datafile = 'test.data'
    dom.params.timestep = 1e-7
    open('test.data', 'w').write(test_data)
    open('test.ctrl', 'w').write(test_ctrl)
    print('init paradis domain')
    dom.init()
    return dom


def grid_point():
    pts = np.array([2, 2, 2])
    sz = 110
    minCoords = sz*np.array([0., 0., -.1])
    maxCoords = sz*np.array([1.,  1.,  .1])

    delta = (maxCoords-minCoords)/(pts-1)
    pos = np.zeros((pts[0]*pts[1]*pts[2], 3))
    p = 0
    for i in range(pts[0]):
        for j in range(pts[1]):
            for k in range(pts[2]):
                pos[p, :] = minCoords + delta*[i, j, k]
                pos[p, 2] += .5
                p += 1
    return pos


def test_compute_python():
    toto = lm.ComputePython('toto')
    try:
        toto.init()
        toto.build()
        raise RuntimeError('should raise an exception')
    except lm.LibMultiScaleException:
        pass


def test_barnett(paradis_domain):
    if not has_paradis:
        return

    dom_dd = paradis_domain

    pset = lm.ComputePointSet('pset')
    grid_points = grid_point()
    pset.params.input = grid_points
    pset.init()
    pset.build()

    barnett = lm.ComputeBarnett('computed_disp')
    barnett.inputs.segments = dom_dd
    barnett.inputs.points = pset
    barnett.params.slip_plane = [0., 1., 0.]
    barnett.init()
    barnett.build()
    computed_disp = barnett.getArray().to_array()

    dd_nodes = lm.ComputeExtract('dd_nodes')
    dd_nodes.inputs.input = dom_dd
    dd_nodes.params.field = lm.position
    dd_nodes.init()
    dd_nodes.build()

    node_positions = dd_nodes.getArray().to_array()
    a = 4.04
    r0 = a*np.sqrt(2)/2.
    burg = np.array([1., 0., 0.])*r0
    slip = np.array([0., 1., 0.])
    # create segments from node positions knowing it is a loop
    elems = np.zeros((node_positions.shape[0], 2), dtype=int)
    elems[:, 0] = np.arange(0, node_positions.shape[0], dtype=int)
    elems[:, 1] = np.arange(1, node_positions.shape[0]+1, dtype=int)
    elems[-1, 1] = 0

    python_computed_disp = barnett_helper.computeDisloDisplacements(
        node_positions, elems, grid_points, burg, slip_plane=slip)
    error = np.linalg.norm(computed_disp - python_computed_disp)
    if error > 1e-5:
        raise RuntimeError("barnett field was not well computed: {0}".format(
            error))


# @pytest.fixture
# def lammps_domain_100K(lammps_domain):
#     dom = lammps_domain
#     test_stimulate_temperature(dom)
#     return dom
#
#
# def test_compute_python():
#     check = lm.ComputePython('check')
#     v = np.array([[1, 2, 3]])
#
#     def toto(units=None, **kwargs):
#         # print(units)
#         print(kwargs)
#         return v
#     check.params.func = {'toto': toto}
#     check.init()
#     check.compute()
#     assert (check.getArray('toto').to_array() == v).all()
#     assert (check.getArray('toto').to_array().shape == (1, 3))
#
#
# def test_compute_python2(managers):
#     data = lm.ComputePython('data')
#     check = lm.ComputePython('check')
#     v = np.array([[1, 2, 3]])
#
#     def toto(units=None, **kwargs):
#         # print(units)
#         print(kwargs)
#         return v
#     data.params.func = {'toto': toto}
#     data.init()
#     lm.FilterManager.getManager().addObject(data)
#     check.params.input = 'data.toto'
#     check.params.func = {'toto': toto}
#     check.init()
#     check.compute()
#     assert (check.getArray('toto').to_array() == v).all()
#     assert (check.getArray('toto').to_array().shape == (1, 3))
#
#
# def test_compute_python3(managers):
#     data = lm.ComputePython('data')
#     code = """
# import numpy as np
# v = np.array([[1, 2, 3]])
#
# def toto(units=None, **kwargs):
#     # print(units)
#     print(kwargs)
#     return v
# """
#     f = open('tmp.py', 'w')
#     f.write(code)
#     f.close()
#
#     data.params.filename = 'tmp'
#     data.init()
#     data.compute()
#     v = np.array([[1, 2, 3]])
#
#     assert (data.getArray('toto').to_array() == v).all()
#     assert (data.getArray('toto').to_array().shape == (1, 3))
#
