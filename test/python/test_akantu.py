#!/usr/bin/python3

import pylibmultiscale as lm
import pytest
import numpy as np
import gc

lm.loadModules()
Dim = 3


comm = lm.Communicator.getCommunicator()
comm.reset()
nb_proc = comm.getNumberFreeProcs()
comm.addGroup('md', nb_proc)
fe_group = lm.Communicator.getGroup('fe')

cube = lm.Cube(Dim, 'fe_geom')
cube.params.bbox = [0, 1, 0, 1, 0, 1]
print(cube.params.bbox)
cube.init()

gManager = lm.GeometryManager.getManager()
gManager.reset()
gManager.addGeometry(cube)

print("loaded")


@pytest.fixture
def managers():
    yield
    lm.FilterManager.destroy()
    lm.ActionManager.destroy()
    lm.DomainMultiScale.destroy()


def test_create_domain(managers):
    dom = lm.DomainAkantu3('fe', fe_group)
    with open("material.dat", 'w') as f:
        f.write("""
material elastic [
         name = steel
         rho = 1   # density
         E   = 1 # young's modulus
         nu  = 0.3    # poisson's ratio
]
""")

    with open("mesh.msh", 'w') as f:
        f.write("""
$MeshFormat
4.1 0 8
$EndMeshFormat
$Entities
0 0 0 1
1 0 0 0 1 1 1 0 0
$EndEntities
$Nodes
1 4 1 4
3 1 0 4
1
2
3
4
0 0 0
0 1 0
0 0 1
1 0 0
$EndNodes
$Elements
1 1 1 1
3 1 4 1
1 1 2 3 4
$EndElements
""")
    dom.params.material_filename = "material.dat"
    dom.params.mesh_filename = "mesh.msh"
    dom.params.pbc = [1, 0, 1]
    dom.params.domain_geometry = "fe_geom"
    dom.params.timestep = 1.
    dom.init()
    lm.DomainMultiScale.getManager().addObject(dom)


@pytest.fixture
def akantu_domain(managers):
    return test_create_domain(managers)


def test_contain_tetrahedron(akantu_domain):
    dom = lm.DomainMultiScale.getManager().getObject("fe")
    cont = dom.getContainer()

    e = cont.getContainerElems().__iter__().__next__()
    print(e, e.contains(np.array([1, 0, 0])))
    print(e, e.contains(np.array([0.02, 0.02, 0.02])))
    print(e, e.contains(np.array([0.5, 0.5, 0.5])))
    print(e, e.contains(np.array([0., 0., 0.])))
