#define MAIN_SOURCE
/* -------------------------------------------------------------------------- */
#include "algebraic_parser.hh"
#include "lm_common.hh"
#include <fstream>
#include <gtest/gtest.h>
#include <iostream>
#include <limits>
/* -------------------------------------------------------------------------- */

using namespace libmultiscale;
TEST(PARSER, simple_reading) {
  AlgebraicExpressionParser p;

  std::string str;
  double result;
  auto expressions =
      std::vector<std::pair<std::string, Real>>{{"3", 3},
                                                {"-3", -3},
                                                {"L = 3", 3},
                                                {"L2 = 2*3", 6},
                                                {"L2 = -2*3", -6},
                                                {"L2 = -L+2*3", 3},
                                                {"L5 = (-5+2)*3", -9},
                                                {"L5 = (-L+2)*3", -3},
                                                {"L2", 3},
                                                {"L3 = sqrt(2)", std::sqrt(2)}};

  for (auto &&kv : expressions) {
    auto l = kv.first;
    auto val = kv.second;
    //std::cout << "Parsing " << l << "\n";
    bool r = p.parse(l, result);
    //std::cout << "result " << result << "\n";    
    EXPECT_EQ(r, true);
    EXPECT_EQ(val, result);
  }
}
