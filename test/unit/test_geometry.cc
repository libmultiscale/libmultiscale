/* ./test/migration/test_migration.cc
**********************************
author : Guillaume ANCIAUX (guillaume.anciaux@epfl.ch, g.anciaux@laposte.net)

The LibMultiScale is a C++ parallel framework for the multiscale
coupling methods dedicated to material simulations. This framework
provides an API which makes it possible to program coupled simulations
and integration of already existing codes.

This Project is done in a collaboration between
EPFL within ENAC-LSMS (http://lsms.epfl.ch/) and
INRIA Bordeaux, ScAlApplix (http://www.labri.fr/projet/scalapplix/).

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
***********************************/

/* -------------------------------------------------------------------------- */

#define TIMER
#define MAIN_SOURCE
#include "cube.hh"
#include "lm_parsable_inline_impl.hh"
#include <gtest/gtest.h>

/* -------------------------------------------------------------------------- */
using namespace libmultiscale;
/* -------------------------------------------------------------------------- */

TEST(CUBE, test_cube) {
  Cube cube(3);
  Vector<3u> xmin({-1, -1, -1});
  Vector<3u> xmax = xmin * -1;
  cube.setXmax(xmax);
  cube.setXmin(xmin);

  Vector<3> point = {0., 0., 0.};

  static_cast<Geometry &>(cube).contains(point);

  cube.contains(point);

  auto c = cube.getBoundingBox();

  __attribute__((unused)) Vector<2> v = c.getXmax<2>();
}

/* -------------------------------------------------------------------------- */

struct TestParsable : Parsable {

  TestParsable() : LMObject("testparsable") {}

  void declareParams() {

    this->parseKeyword("SCALAR", scalar);
    this->parseKeyword("VEC_1", vector_1);
    this->parseVectorKeyword("VEC_1_VEC", 1, vector_1);
    this->parseKeyword("VEC_3", vector);
    this->parseVectorKeyword("VEC_3_VEC", 3, vector);
    this->parseKeyword("QUANTITY_1", quantity_1);
    this->parseVectorKeyword("QUANTITY_1_VEC", 1, quantity_1);
    this->parseKeyword("QUANTITY_3", quantity);
    this->parseVectorKeyword("QUANTITY_3_VEC", 3, quantity);

    this->parseKeyword("SCALAR_DEF", scalar, 1.);
    this->parseKeyword("VEC_1_DEF", vector_1, 0.);
    this->parseKeyword("VEC_3_DEF", vector, VEC_DEFAULTS(0., 1., 2.));
    this->parseKeyword("QUANTITY_1_DEF", quantity_1, 0.);
    this->parseKeyword("QUANTITY_3_DEF", quantity, VEC_DEFAULTS(0., 1., 2.));

    this->parseVectorKeyword("UVEC_1_VEC_DEF", 1, uvector_1,
                             VEC_DEFAULTS(10u, 10u, 10u));
    this->parseVectorKeyword("UVEC_2_VEC_DEF", 2, uvector,
                             VEC_DEFAULTS(10u, 10u, 10u));

    this->parseKeyword("STRING", string);
    this->parseKeyword("STRING_DEF", string, std::string("toto"));
    this->parseKeyword("STRING_DEF_CHAR", string, "toto");

    this->parseKeyword("VEC_STRING", strings);
    this->parseKeyword("VEC_STRING_DEF", strings,
                       VEC_DEFAULTS(std::string("toto"), std::string("tata"),
                                    std::string("toto")));
    this->parseKeyword("VEC_STRING_DEF_CHAR", strings,
                       VEC_DEFAULTS("toto", "tata", "toto"));

    this->parseVectorKeyword("VEC_STRING_2_VEC", 2, strings);
    this->parseVectorKeyword("VEC_STRING_2_VEC_DEF", 2, strings,
                             VEC_DEFAULTS(std::string("toto"),
                                          std::string("tata"),
                                          std::string("toto")));
    this->parseVectorKeyword("VEC_STRING_2_VEC_DEF_CHAR", 2, strings,
                             VEC_DEFAULTS("toto", "tata", "toto"));
  };

  Real scalar;
  Vector<1> vector_1;
  Vector<3> vector;

  Vector<1, UInt> uvector_1;
  Vector<2, UInt> uvector;

  Quantity<Length, 1> quantity_1;
  Quantity<Length, 3> quantity;

  std::string string;
  std::vector<std::string> strings;
};

TEST(PARSABLE, divers) {
  TestParsable p;
  p.declareParams();
}

/* -------------------------------------------------------------------------- */

