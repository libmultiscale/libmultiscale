/* ./test/migration/test_migration.cc
**********************************
author : Guillaume ANCIAUX (guillaume.anciaux@epfl.ch, g.anciaux@laposte.net)

The LibMultiScale is a C++ parallel framework for the multiscale
coupling methods dedicated to material simulations. This framework
provides an API which makes it possible to program coupled simulations
and integration of already existing codes.

This Project is done in a collaboration between
EPFL within ENAC-LSMS (http://lsms.epfl.ch/) and
INRIA Bordeaux, ScAlApplix (http://www.labri.fr/projet/scalapplix/).

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
***********************************/

/* -------------------------------------------------------------------------- */
#include <gtest/gtest.h>
/* -------------------------------------------------------------------------- */

#define TIMER
#define MAIN_SOURCE

#define LM_FATAL(x)                                                            \
  {                                                                            \
    std::stringstream sstr;                                                    \
    sstr << x;                                                                 \
    throw std::runtime_error(sstr.str());                                      \
  }

#include "component_libmultiscale.hh"
#include <cxxabi.h>
#include <memory>
#include <tuple>
#include <typeinfo>
#include <utility>

namespace libmultiscale {
std::string demangle(const char *name) {

  int status = -4; // some arbitrary value to eliminate the compiler warning
  std::unique_ptr<char, void (*)(void *)> res{
      abi::__cxa_demangle(name, NULL, NULL, &status), std::free};
  return (status == 0) ? res.get() : name;
}
} // namespace libmultiscale

/* -------------------------------------------------------------------------- */
using namespace libmultiscale;
/* -------------------------------------------------------------------------- */

struct Obj : public AutoDispatch::Argument {
  virtual ~Obj(){};

  int operator+(Obj &) { return 0; };
};
struct A : public Obj {};
struct B : public Obj {};

// code of the function
DECORATE_FUNCTION_DISPATCH(foo, _or<A, B>, _or<B>)
template <typename T1, typename T2> void foo(T1 &a, T2 &b) {
  static_assert(!std::is_same<T1, Obj>::value, "Automatic cast failed");
  auto c [[maybe_unused]] = a + b;
}

TEST(STATIC_DISPATCH, function) {
  A a;
  B b;

  foo<dispatch>(static_cast<Obj &>(a), static_cast<Obj &>(b));
}

/* -------------------------------------------------------------------------- */

TEST(COMPONENT, make_argument) {
  AutoDispatch::ArgumentAny out;
  out = 18;

  int a = out.get<int>();
  EXPECT_EQ(a, 18);
}

/* -------------------------------------------------------------------------- */

class TestComponent : public Component {
public:
  using subtypes = std::tuple<int, Real, bool, std::string>;
  using possible_types = std::tuple<subtypes, subtypes>;

  TestComponent() : LMObject("TestComponent") {
    createInput("input_1");
    createInput("input_2");

    createOutput("out_1");
    createOutput("out_2");
  }

  DECORATE_FUNCTION_DISPATCH(doIt, _or<int>, _or<bool, double, std::string>)
  template <typename T1, typename T2> void doIt(T1 &cont, T2 &cont2) {
    getOutput("out_1") = cont;
    getOutput("out_2") = cont2;
    // std::cout << "out_1 " << cont << std::endl;
    // std::cout << "out_2 " << cont2 << std::endl;
  }

private:
  virtual void compute_make_call() override {
    this->doIt<dispatch>(this->getInput("input_1"), this->getInput("input_2"));
  };
};

/* -------------------------------------------------------------------------- */

TEST(COMPONENT, connect_and_get) {
  TestComponent comp;

  int a = 2;
  bool b = true;

  comp.connect("input_1", a);
  comp.connect("input_2", b);

  comp.compute();
  int res_a = comp.evalOutput<int>("out_1");
  bool res_b = comp.evalOutput("out_2").get<bool>();
  EXPECT_EQ(res_a, 2);
  EXPECT_EQ(res_b, true);
}

/* ---------------------------------------------------------------------- */

class TestComponentNoInput : public Component {
public:
  using possible_types = std::tuple<std::tuple<>>;

  TestComponentNoInput() : LMObject("TestComponentNoInput") {
    createOutput("out_1");
    createOutput("out_2");
  }

private:
  virtual void compute_make_call() override {
    getOutput("out_1") = 1;
    getOutput("out_2") = 10.;
  };
};

/* -------------------------------------------------------------------------- */

TEST(COMPONENT, argument_container) {
  InputContainer cont;

  cont = 18;
  EXPECT_EQ(cont.get<int>(), 18);

  int a = 124;
  std::map<std::string, InputContainer> map_test;
  map_test["test"] = 23;
  map_test["test2"] = a;

  EXPECT_EQ(map_test["test"].get<int>(), 23);
  EXPECT_EQ(map_test["test2"].get<int>(), 124);

  for (auto &pair : map_test) {
    if (pair.first == "test") {
      EXPECT_EQ(pair.second.get<int>(), 23);
    }
    if (pair.first == "test2") {
      EXPECT_EQ(pair.second.get<int>(), 124);
    }
  }

  TestComponentNoInput comp;

  comp.compute();
  EXPECT_EQ(comp.evalOutput<int>("out_1"), 1);
  EXPECT_EQ(comp.evalOutput<Real>("out_2"), 10.);
}

/* -------------------------------------------------------------------------- */

TEST(COMPONENT, chain_components) {

  TestComponent comp;
  TestComponent comp2;
  TestComponentNoInput comp3;

  comp.connect("input_1", comp2, "out_1");
  comp.connect("input_2", comp2, "out_2");
  comp2.connect("input_1", comp3, "out_1");
  comp2.connect("input_2", comp3, "out_2");

  comp.compute();

  EXPECT_EQ(comp.evalOutput<int>("out_1"), 1);
  EXPECT_EQ(comp.evalOutput<double>("out_2"), 10.);
}
/* -------------------------------------------------------------------------- */

TEST(COMPONENT, chain_components2) {

  TestComponent comp;
  TestComponent comp2;
  TestComponent comp3;

  comp.connect("input_1", comp2, "out_1");
  comp.connect("input_2", comp2, "out_2");
  comp2.connect("input_1", comp3, "out_1");
  comp2.connect("input_2", comp3, "out_2");
  comp3.connect("input_1", 145);
  comp3.connect("input_2", std::string("toto"));

  comp.compute();

  EXPECT_EQ(comp.evalOutput<int>("out_1"), 145);
  EXPECT_EQ(comp.evalOutput<std::string>("out_2"), "toto");
}
/* -------------------------------------------------------------------------- */
struct TestObject : public LMObject {

  TestObject() : LMObject("TestObject") {}
};

class ComponentCallCounter : public Component {
public:
  using subtypes = std::tuple<int, Real, bool, std::string>;
  using possible_types = std::tuple<subtypes, subtypes>;

  ComponentCallCounter(const std::string &name) : LMObject(name) {
    createInput("input_1");
    createOutput("out_1");
  }

  DECORATE_FUNCTION_DISPATCH(doIt, _or<TestObject>)
  template <typename T1> void doIt(T1 &cont) {
    getOutput("out_1") = TestObject{cont};
    ++counter;
  }
  int counter = 0;

private:
  virtual void compute_make_call() override {
    this->doIt<dispatch>(this->getInput("input_1"));
  };
};

TEST(COMPONENT, double_eval) {

  ComponentCallCounter comp("comp");
  ComponentCallCounter comp2("comp2");

  comp.connect("input_1", comp2, "out_1");
  comp2.connect("input_1", TestObject{});

  comp.compute();

  EXPECT_EQ(comp.counter, 1);
  EXPECT_EQ(comp2.counter, 1);

  comp.compute();

  EXPECT_EQ(comp.counter, 1);
  EXPECT_EQ(comp2.counter, 1);
}
/* -------------------------------------------------------------------------- */
int main(int argc, char *argv[]) {
  int result = 0;

  ::testing::InitGoogleTest(&argc, argv);
  loadModules(argc, argv);
  result = RUN_ALL_TESTS();
  closeModules();

  return result;
}
