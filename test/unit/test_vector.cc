/* ./test/migration/test_migration.cc
**********************************
author : Guillaume ANCIAUX (guillaume.anciaux@epfl.ch, g.anciaux@laposte.net)

The LibMultiScale is a C++ parallel framework for the multiscale
coupling methods dedicated to material simulations. This framework
provides an API which makes it possible to program coupled simulations
and integration of already existing codes.

This Project is done in a collaboration between
EPFL within ENAC-LSMS (http://lsms.epfl.ch/) and
INRIA Bordeaux, ScAlApplix (http://www.labri.fr/projet/scalapplix/).

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
***********************************/

/* -------------------------------------------------------------------------- */

#define TIMER
#define MAIN_SOURCE
#include "lm_types.hh"
#include "quantity.hh"
#include <gtest/gtest.h>
/* -------------------------------------------------------------------------- */
using namespace libmultiscale;
/* -------------------------------------------------------------------------- */

template <UInt Dim>
void foo(UInt, const Vector<Dim> &, const Vector<Dim> &,
         const Vector<Dim, UInt> &) {}

template <UInt Dim>
void foo(UInt, const Vector<Dim> &, const Vector<Dim> &,
         const Vector<Dim, Real> &) {}

TEST(LINALG, Vector) {

  Vector<3, Real> v;
  Vector<3, UInt> v2;

  foo(3, v, v, v);
  foo(3, v, v, v2);

  Matrix<3> m;
  Vector<3> v3;
  __attribute__((unused)) Vector<3> v4 = m * v3;

  Matrix<1> m2;
  Vector<1> v5;
  __attribute__((unused)) Vector<1> v6 = m2 * v5;
  __attribute__((unused)) Real v7 = m2 * v5;
}
/* -------------------------------------------------------------------------- */
VectorProxy<3> wrap_vector(std::array<Real, 3> &v) {
  return VectorProxy<3>(std::forward_as_tuple(v[0], v[1], v[2]));
}

TEST(LINALG, VectorProxy) {
  std::array<Real, 3> pos{0, 0, 0};
  VectorProxy<3> position(std::forward_as_tuple(pos[0], pos[1], pos[2]));

  Vector<3> v1 = Vector<3>::Random();
  position = v1;
  for (UInt i = 0; i < 3; ++i)
    EXPECT_EQ(pos[i], v1[i]);
  Quantity<Length, 3> v2 = Quantity<Length, 3>::Random();
  position = v2;
  for (UInt i = 0; i < 3; ++i)
    EXPECT_EQ(pos[i], v2[i]);

  Vector<3> v3 = Vector<3>::Random();
  wrap_vector(pos) = v3;
  for (UInt i = 0; i < 3; ++i)
    EXPECT_EQ(pos[i], v3[i]);

  Quantity<Length, 3> v4 = Quantity<Length, 3>::Random();
  wrap_vector(pos) = v4;
  for (UInt i = 0; i < 3; ++i)
    EXPECT_EQ(pos[i], v4[i]);
}

/* -------------------------------------------------------------------------- */

TEST(UNITS, various) {
  PhysicalScalar<Length> dist = 2;
  Quantity<Length> dist2 = dist;
  PhysicalScalar<Length> dist3 [[gnu::unused]] = dist2;
}
