# 3d CU with eam reading file

units		metal
dimension	3
atom_style	atomic
boundary 	p p s
atom_style	atomic
#at 100 lattice constant is a=3.620390310181612
lattice		fcc 3.620390310181612 origin 0.1 0.1 0.1

region		box block -16 16 -16 16 -300 102

create_box	2 box

region		substrate block -16 16 -16 16 -300 0
create_atoms	1 region substrate
lattice		fcc 3.620390310181612 origin 0.98 0.2 0.98
#lattice		fcc 3.620390310181612 origin 0.1 0.1 0.1
region		rigid block -16 16 -16 16 0 102
create_atoms	2 region rigid


mass		* 63.550
pair_style	hybrid lj/cut 2.556191013989369301 eam/alloy
pair_coeff	* * eam/alloy Cu_mishin1.eam.alloy Cu NULL
pair_coeff	1 2 lj/cut 0.1630771062 2.27730728218
pair_coeff	2 2 lj/cut 0.1630771062 2.27730728218

neighbor	0.3 bin
neigh_modify	delay 1

fix		1 all nve

