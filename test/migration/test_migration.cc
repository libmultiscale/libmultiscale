/* ./test/migration/test_migration.cc
**********************************
author : Guillaume ANCIAUX (guillaume.anciaux@epfl.ch, g.anciaux@laposte.net)

The LibMultiScale is a C++ parallel framework for the multiscale
coupling methods dedicated to material simulations. This framework
provides an API which makes it possible to program coupled simulations
and integration of already existing codes.

This Project is done in a collaboration between
EPFL within ENAC-LSMS (http://lsms.epfl.ch/) and
INRIA Bordeaux, ScAlApplix (http://www.labri.fr/projet/scalapplix/).

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
***********************************/

/* -------------------------------------------------------------------------- */

#define TIMER
#define MAIN_SOURCE
#include "algebraic_parser.hh"
#include "domain_multiscale.hh"
#include "factory_multiscale.hh"
#include "lib_md.hh"
#include "lm_common.hh"
#include "reference_manager_interface.hh"
#include <iomanip>
#include <math.h>
#include <stdlib.h>
/* -------------------------------------------------------------------------- */
using namespace libmultiscale;
/* -------------------------------------------------------------------------- */

Real GaussianRand() {
  Real x1, x2, w;
  // UInt *seed = &randSeed;
  do {
    x1 = 2.0 * drand48() - 1.0;
    x2 = 2.0 * drand48() - 1.0;
    // x1 = RANDOM_abs1( seed );
    // x2 = RANDOM_abs1( seed );
    w = x1 * x1 + x2 * x2;
  } while (w >= 1.0);

  w = sqrt((-2.0 * log(w)) / w);

  return x1 * w;
}

/* -------------------------------------------------------------------------- */

int main(int argc, char **argv) {

  loadModules(argc, argv); // loading the modules macro

#ifdef LIBMULTISCALE_USE_LAMMPS
  MPI_Barrier(MPI_COMM_WORLD);

  DomainMultiScale &dom = DomainMultiScale::getManager();

  DOWAIT_AT_STARTUP;

  dom.build(argv[1]);

  STOPTIMER("Init");

  Real time_step = 1e300;
  dom.for_each([&time_step](auto &d) {
    Real ts = d.template getParam<Quantity<Time>>("TIMESTEP");
    time_step = std::min(time_step, ts);
  });
  DUMP("Multiscale timestep:" << time_step, DBG_MESSAGE);

  DomainLammps<3> &dL =
      static_cast<DomainLammps<3> &>(*dom.getModelByNumber(0));

  ContainerLammps<3> &c = dL.getContainer();

  ContainerArray<RefLammps<3>> subset("everything-subset");
  dL.getContainer().getRefManager()->addSubset(subset);

  // build the subset to be attached to reference manager
  for (auto at : c) {
    subset.push_back(at);
  }

  ContainerArray<RefLammps<3>> subset2("half-subset");
  dL.getContainer().getRefManager()->addSubset(subset2);

  // build the subset to be attached to reference manager
  UInt cpt = 0;
  for (auto at : c) {
    if (cpt % 2 == 0)
      subset2.push_back(at);
    ++cpt;
  }

  ContainerArray<Real> v_p0("v_p0", 0, 3);
  ContainerArray<Real> v2_p0("v2_p0", 0, 2);
  ContainerArray<Real> v3_p0("v3_p0", 0, 2);

  // build a vector to be migrated with the atoms
  cpt = 0;
  for (auto at : c) {
    Vector<3> pos0 = at.position0();
    v_p0.push_back(pos0);
    Vector<2> pos1 = pos0.block<2, 1>(0, 0) * 2;
    v2_p0.push_back(pos1);
    if (cpt % 2 == 0) {
      v3_p0.push_back(pos1);
    }
    ++cpt;
  }
  dL.getContainer().getRefManager()->attachObject(v_p0, subset);
  dL.getContainer().getRefManager()->attachObject(v2_p0, subset);
  dL.getContainer().getRefManager()->attachObject(v3_p0, subset2);

  srand48(345);
  //  force random migrations

  for (auto at : c) {
    at.position()[0] += GaussianRand();
    at.position()[1] += GaussianRand();
    at.position()[2] += GaussianRand();
  }

  // UInt index_pb = 4;
  // if (lm_my_proc_id == 0){
  //   at = subset.Get(index_pb);
  //   DUMP("atome " << index_pb << " coords "
  // 	 << at.position0(0) << " "
  // 	 << at.position0(1) << " "
  // 	 << at.position0(2) << " "
  // 	 << v_p0[3*index_pb],DBG_ERROR);
  // }

  dom.coupling(COUPLING_STEP3);

  dom.for_each([time_step](auto &d) {
    auto &&v = d.primalTimeDerivative();
    auto &&p = d.primal();
    auto &&acc = d.acceleration();

    v += 0.5 * time_step * acc;
    p += time_step * v;
    d.changeRelease();
    d.enforceCompatibility();
  });
  ++current_step;
  dom.coupling(COUPLING_STEP3);

  // if (lm_my_proc_id == 0){
  //   at = subset.Get(index_pb);
  //   DUMP("atome " << index_pb << " coords "
  // 	 << at.position0(0) << " "
  // 	 << at.position0(1) << " "
  // 	 << at.position0(2) << " "
  // 	 << v_p0[3*index_pb],DBG_ERROR);
  // }

  MPI_Barrier(MPI_COMM_WORLD);

  // test that all attached initial positions are still correct in v_p0
  Eigen::IOFormat fmt(Eigen::FullPrecision, 0, ", ", "", "", "", "[", "]");

  if (lm_my_proc_id == 0) {
    for (auto [i, at] : enumerate(subset)) {

      if (v_p0(i, 0) != at.position0()[0])
        LM_FATAL("fail@x " << subset.getID() << "[" << i << "] = data"
                           << v_p0.row(i).format(fmt) << " != ref"
                           << at.position0().format(fmt));
      if (v_p0(i, 1) != at.position0()[1])
        LM_FATAL("fail@y " << subset.getID() << "[" << i << "] = data"
                           << v_p0.row(i).format(fmt) << " != ref"
                           << at.position0().format(fmt));
      if (v_p0(i, 2) != at.position0()[2])
        LM_FATAL("fail@z " << subset.getID() << "[" << i << "] = data"
                           << v_p0.row(i).format(fmt) << " != ref"
                           << at.position0().format(fmt));
      if (v2_p0(i, 0) != at.position0()[0] * 2)
        LM_FATAL("fail@x " << subset.getID() << "[" << i << "] = data"
                           << v2_p0.row(i).format(fmt) << " != ref"
                           << (at.position0() * 2).format(fmt));
      if (v2_p0(i, 1) != at.position0()[1] * 2)
        LM_FATAL("fail@y " << subset.getID() << "[" << i << "] = data"
                           << v2_p0.row(i).format(fmt) << " != ref"
                           << (at.position0() * 2).format(fmt));
    }
    for (auto [i, at] : enumerate(subset2)) {
      if (v3_p0(i, 0) != at.position0()[0] * 2)
        LM_FATAL("fail@ x " << subset.getID() << "[" << i << "] = data"
                            << v3_p0.row(i).format(fmt) << " != ref"
                            << (at.position0() * 2).format(fmt));
      if (v3_p0(i, 1) != at.position0()[1] * 2)
        LM_FATAL("fail@y " << subset.getID() << "[" << i << "] = data"
                           << v3_p0.row(i).format(fmt) << " != ref"
                           << (at.position0()).format(fmt));
    }
  }
#endif
  closeModules(); // closing the modules macro

  return 0;
}
