#Copyright EPFL and INRIA
#
#author : Guillaume ANCIAUX (guillaume.anciaux@epfl.fr, g.anciaux@laposte.net)
#
#The LibMultiScale is a C++ parallel framework for the multiscale
#coupling methods dedicated to material simulations. This framework
#provides an API which makes it possible to program coupled simulations
#and integration of already existing codes.
#
#This Project is done in a collaboration between 
#EPFL within ENAC-LSMS (http://lsms.epfl.ch/) and 
#INRIA Bordeaux, ScAlApplix (http://www.labri.fr/projet/scalapplix/). 
#
#This software is governed by the CeCILL-C license under French law and
#abiding by the rules of distribution of free software.  You can  use, 
#modify and/ or redistribute the software under the terms of the CeCILL-C
#license as circulated by CEA, CNRS and INRIA at the following URL
#"http://www.cecill.info". 
#
#As a counterpart to the access to the source code and  rights to copy,
#modify and redistribute granted by the license, users are provided only
#with a limited warranty  and the software's author,  the holder of the
#economic rights,  and the successive licensors  have only  limited
#liability. 
#
#In this respect, the user's attention is drawn to the risks associated
#with loading,  using,  modifying and/or developing or reproducing the
#software by the user in light of its specific status of free software,
#that may mean  that it is complicated to manipulate,  and  that  also
#therefore means  that it is reserved for developers  and  experienced
#professionals having in-depth computer knowledge. Users are therefore
#encouraged to load and test the software's suitability as regards their
#requirements in conditions enabling the security of their systems and/or 
#data to be ensured and,  more generally, to use and operate it in the 
#same conditions as regards security. 
#
#The fact that you are presently reading this means that you have had
#knowledge of the CeCILL-C license and that you accept its terms.

package_get_all_external_informations(
  PRIVATE_INCLUDE LIBMULTISCALE_EXTERNAL_INCLUDE_DIR
  INTERFACE_INCLUDE LIBMULTISCALE_INTERFACE_EXTERNAL_LIBRARIES
  LIBRARIES LIBMULTISCALE_EXTERNAL_LIBRARIES
  )
include_directories(${LIBMULTISCALE_EXTERNAL_INCLUDE_DIR})

ADD_EXECUTABLE(TEST_MIGRATION test_migration.cc)

package_get_all_definitions(LIBMULTISCALE_DEFINITIONS)
foreach(_dep ${LIBMULTISCALE_DEFINITIONS})
  add_definitions(-D${_dep})
endforeach()

TARGET_LINK_LIBRARIES(TEST_MIGRATION ${MY_LIBRARIES} multiscale)

file(COPY migration.sh Cu_mishin1.eam.alloy dense-crystal.in dense-crystal.config materials.dat coupled-crystal.config DESTINATION .)
add_test(TEST_MIGRATION ${CMAKE_CURRENT_BINARY_DIR}/migration.sh)




