NPROC=1 ./TEST_MIGRATION dense-crystal.config
NPROC=2 mpirun -np 2 ./TEST_MIGRATION dense-crystal.config
NPROC=4 mpirun -np 4 ./TEST_MIGRATION dense-crystal.config
NPROC=6 mpirun -np 6 ./TEST_MIGRATION dense-crystal.config
NPROC=8 mpirun -np 8 ./TEST_MIGRATION dense-crystal.config
