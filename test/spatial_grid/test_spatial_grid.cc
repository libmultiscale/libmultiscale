/* ./test/spatial_grid/test_spatial_grid.cc
**********************************
author : Guillaume ANCIAUX (guillaume.anciaux@epfl.ch, g.anciaux@laposte.net)

The LibMultiScale is a C++ parallel framework for the multiscale
coupling methods dedicated to material simulations. This framework
provides an API which makes it possible to program coupled simulations
and integration of already existing codes.

This Project is done in a collaboration between
EPFL within ENAC-LSMS (http://lsms.epfl.ch/) and
INRIA Bordeaux, ScAlApplix (http://www.labri.fr/projet/scalapplix/).

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
***********************************/

/* -------------------------------------------------------------------------- */

#define TIMER
#define MAIN_SOURCE
/* -------------------------------------------------------------------------- */
#include "lm_common.hh"
#include "lm_communicator.hh"
#include "spatial_grid_libmultiscale.hh"
/* -------------------------------------------------------------------------- */
#include <algorithm>
#include <math.h>
#include <stdlib.h>
/* -------------------------------------------------------------------------- */
using namespace libmultiscale;
/* -------------------------------------------------------------------------- */

#define NCELL 10

template <UInt Dim>
void addPoints(UInt NPOINT, SpatialGridLibMultiScale<Real, Dim> &grid) {
  LM_FATAL("bipbip");
}

template <>
void addPoints<1>(UInt NPOINT, SpatialGridLibMultiScale<Real, 1> &grid) {
  Real x[1];
  for (UInt i = 0; i < NPOINT + 1; ++i) {
    x[0] = i + 0.5;
    grid.addElement(x[0], x);
  }
}

template <>
void addPoints<2>(UInt NPOINT, SpatialGridLibMultiScale<Real, 2> &grid) {
  Real x[2];

  for (UInt i = 0; i < NPOINT + 1; ++i) {
    x[0] = i + 0.5;
    for (UInt j = 0; j < NPOINT + 1; ++j) {
      x[1] = j + 0.5;
      grid.addElement(x[0], x);
    }
  }
}

template <>
void addPoints<3>(UInt NPOINT, SpatialGridLibMultiScale<Real, 3> &grid) {
  Real x[3];

  for (UInt i = 0; i < NPOINT + 1; ++i) {
    x[0] = i + 0.5;
    //    std::cout << i << "/" << NPOINT << std::endl;
    for (UInt j = 0; j < NPOINT + 1; ++j) {
      x[1] = j + 0.5;
      for (UInt k = 0; k < NPOINT + 1; ++k) {
        x[2] = k + 0.5;
        grid.addElement(x[0], x);
      }
    }
  }
}
/* -------------------------------------------------------------------------- */
template <UInt Dim>
void printNeighbors(Vector<Dim> &x, std::vector<UInt> &trusted_neighbors,
                    std::vector<UInt> &neighbors) {

  UInt s1 = trusted_neighbors.size();
  UInt s2 = neighbors.size();
  UInt s = std::min(s1, s2);

  std::cerr << " for point ";
  for (UInt i = 0; i < Dim; ++i) {
    std::cerr << x[i] << "\t";
  }
  std::cerr << std::endl;

  std::cerr << "#\ttrusted_neighbors(" << s1 << ")\tneighbors(" << s2 << ")"
            << std::endl;
  for (UInt i = 0; i < s; ++i) {
    std::cerr << i << "\t" << trusted_neighbors[i] << "\t" << neighbors[i]
              << std::endl;
  }

  if (s1 > s)
    for (UInt i = s; i < s1; ++i) {
      std::cerr << i << "\t" << trusted_neighbors[i] << std::endl;
    }
  if (s2 > s)
    for (UInt i = s; i < s2; ++i) {
      std::cerr << i << "\t  \t" << neighbors[i] << std::endl;
    }
}
/* -------------------------------------------------------------------------- */

template <UInt Dim>
std::vector<UInt>
buildTrustedNeighbors(Vector<Dim, UInt> &indexes,
                      SpatialGridLibMultiScale<Real, Dim> &grid,
                      UInt pbc_flag[Dim], UInt cell_number[Dim]) {

  LM_FATAL("wrong dimension");
}

template <>
std::vector<UInt> buildTrustedNeighbors(Vector<3, UInt> &indexes,
                                        SpatialGridLibMultiScale<Real, 3> &grid,
                                        UInt pbc_flag[3], UInt cell_number[3]) {

  Vector<3, UInt> ii;
  std::vector<UInt> trusted_neighbors;

  for (int i = -1; i < 2; ++i) {
    if (indexes[0] == 0 && i == -1) {
      if (pbc_flag[0])
        ii[0] = cell_number[0] - 1;
      else
        continue;
    } else if (indexes[0] + i == cell_number[0]) {
      if (pbc_flag[0])
        ii[0] = 0;
      else
        continue;
    } else
      ii[0] = indexes[0] + i;
    for (int j = -1; j < 2; ++j) {
      if (indexes[1] == 0 && j == -1) {
        if (pbc_flag[1])
          ii[1] = cell_number[1] - 1;
        else
          continue;
      } else if (indexes[1] + j == cell_number[1]) {
        if (pbc_flag[1])
          ii[1] = 0;
        else
          continue;
      } else
        ii[1] = indexes[1] + j;

      for (int k = -1; k < 2; ++k) {
        if (indexes[2] == 0 && k == -1) {
          if (pbc_flag[2])
            ii[2] = cell_number[2] - 1;
          else
            continue;
        } else if (indexes[2] + k == cell_number[2]) {
          if (pbc_flag[2])
            ii[2] = 0;
          else
            continue;
        } else
          ii[2] = indexes[2] + k;

        auto index = grid.cartesianIndex2Index(ii);
        LM_ASSERT(index != UINT_MAX, "invalid neighbor block number "
                                         << index << " " << indexes[0] << " "
                                         << indexes[1] << " " << indexes[2]
                                         << std::endl);
        trusted_neighbors.push_back(index);
      }
    }
  }
  return trusted_neighbors;
}

/* -------------------------------------------------------------------------- */

template <>
std::vector<UInt> buildTrustedNeighbors(Vector<2, UInt> &indexes,
                                        SpatialGridLibMultiScale<Real, 2> &grid,
                                        UInt pbc_flag[2], UInt cell_number[2]) {

  Vector<2, UInt> ii;
  std::vector<UInt> trusted_neighbors;

  for (int i = -1; i < 2; ++i) {
    if (indexes[0] == 0 && i == -1) {
      if (pbc_flag[0])
        ii[0] = cell_number[0] - 1;
      else
        continue;
    } else if (indexes[0] + i == cell_number[0]) {
      if (pbc_flag[0])
        ii[0] = 0;
      else
        continue;
    } else
      ii[0] = indexes[0] + i;
    for (int j = -1; j < 2; ++j) {
      if (indexes[1] == 0 && j == -1) {
        if (pbc_flag[1])
          ii[1] = cell_number[1] - 1;
        else
          continue;
      } else if (indexes[1] + j == cell_number[1]) {
        if (pbc_flag[1])
          ii[1] = 0;
        else
          continue;
      } else
        ii[1] = indexes[1] + j;
      UInt index = grid.cartesianIndex2Index(ii);
      trusted_neighbors.push_back(index);
    }
  }
  return trusted_neighbors;
}

/* -------------------------------------------------------------------------- */

template <>
std::vector<UInt> buildTrustedNeighbors(Vector<1, UInt> &indexes,
                                        SpatialGridLibMultiScale<Real, 1> &grid,
                                        UInt pbc_flag[1], UInt cell_number[1]) {

  Vector<1, UInt> ii;
  std::vector<UInt> trusted_neighbors;

  for (int i = -1; i < 2; ++i) {
    if (indexes[0] == 0 && i == -1) {
      if (pbc_flag[0])
        ii[0] = cell_number[0] - 1;
      else
        continue;
    } else if (indexes[0] + i == cell_number[0]) {
      if (pbc_flag[0])
        ii[0] = 0;
      else
        continue;
    } else
      ii[0] = indexes[0] + i;
    UInt index = grid.cartesianIndex2Index(ii);
    trusted_neighbors.push_back(index);
  }
  return trusted_neighbors;
}

/* -------------------------------------------------------------------------- */

template <UInt Dim>
void testNeighbors(Vector<Dim> &x, SpatialGridLibMultiScale<Real, Dim> &grid,
                   UInt *pbc = NULL) {

  UInt pbc_flag[Dim];
  if (pbc == NULL)
    for (UInt i = 0; i < Dim; ++i)
      pbc_flag[i] = 0;
  else
    for (UInt i = 0; i < Dim; ++i)
      pbc_flag[i] = pbc[i];

  auto indexes = grid.coordinates2CartesianIndex(x);

  UInt cell_number[3];
  for (UInt i = 0; i < Dim; ++i)
    cell_number[i] = grid.getCellNumber(i);

  std::vector<UInt> trusted_neighbors =
      buildTrustedNeighbors(indexes, grid, pbc_flag, cell_number);

  std::sort(trusted_neighbors.begin(), trusted_neighbors.end());

  std::vector<UInt> neighbors;
  grid.getNeighborhood(x, neighbors);

  std::sort(neighbors.begin(), neighbors.end());

  if (trusted_neighbors.size() != neighbors.size()) {
    printNeighbors<Dim>(x, trusted_neighbors, neighbors);
    LM_FATAL("did not compute the same number of neighbors: error");
  }
  for (UInt i = 0; i < trusted_neighbors.size(); ++i) {
    if (trusted_neighbors[i] != neighbors[i]) {
      printNeighbors<Dim>(x, trusted_neighbors, neighbors);
      LM_FATAL("error in computing neighbor(" << i
                                              << ") : " << trusted_neighbors[i]
                                              << " != " << neighbors[i]);
    }
  }
}

/* -------------------------------------------------------------------------- */

template <UInt Dim> void test(UInt NPOINT) {
  Cube c(Dim);

  c.setDimensions(0, NPOINT, 0, NPOINT, 0, NPOINT);

  Vector<Dim, UInt> s;
  for (UInt i = 0; i < Dim; ++i)
    s[i] = NCELL;

  SpatialGridLibMultiScale<Real, Dim> grid(c, s);

  addPoints<Dim>(NPOINT, grid);

  std::vector<std::vector<Real> *> &blocks = grid.getBlocks();
  for (UInt i = 0; i < grid.getSize(); ++i)
    if (blocks[i]) {
      auto x = grid.index2Coordinates(i);
      Real v = grid.extractBoxValue(i, OP_AVERAGE);
      Real v_std = grid.extractBoxValue(i, OP_DEVIATION);

      if (fabs(v - x[0])) {
        std::cout << "failed on computing average " << x << " " << v << " "
                  << v_std << " " << std::endl;

        exit(EXIT_FAILURE);
      }
      Real tmp = (NPOINT / NCELL);
      Real predicted_std =
          (tmp - 1) * (2 * tmp - 1) / 6. - (tmp - 1) * (tmp - 1) / 4.;
      if (fabs(v_std - predicted_std)) {
        std::cout << "failed on computing std " << predicted_std << " " << x
                  << " " << v << " " << v_std << " " << std::endl;

        exit(EXIT_FAILURE);
      }
    }

  Real p[3] = {0.5, Real(NPOINT) / 2, Real(NPOINT) - .5};
  Vector<Dim> x;
  x.Zero();
  // test the neighbors of the corner points
  for (UInt i = 0; i < 3; ++i) {
    x[0] = p[i];
    if (Dim > 1) {
      for (UInt j = 0; j < 3; ++j) {
        x[1] = p[j];
        if (Dim == 3) {
          for (UInt k = 0; k < 3; ++k) {
            x[2] = p[k];
            testNeighbors(x, grid);
          }
        } else
          testNeighbors(x, grid);
      }
    } else
      testNeighbors(x, grid);
  }
}
/* -------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  loadModules(argc, argv); // loading the modules macro

  UInt NPOINT = 100;
  if (argc == 2) {
    NPOINT = atoi(argv[1]);
  }

  DUMP("test 1D", DBG_MESSAGE);
  test<1u>(NPOINT);
  DUMP("test 2D", DBG_MESSAGE);
  test<2u>(NPOINT);
  DUMP("test 3D", DBG_MESSAGE);
  test<3u>(NPOINT);

  closeModules(); // closing the modules macro

  return EXIT_SUCCESS;
}
